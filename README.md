Этот проект - часть ПО "Димео.Подписка", которое предназначено для издателей периодических печатных и цифровых изданий и автоматизирует процессы подписки и коммуникаций, обеспечивая непрерывное, сквозное, двухстороннее взаимодействие редакции издания с подписчиком.

This project is part of the Dimeo.Podpiska software, which is intended for publishers of print and digital periodicals and automates subscription and communication processes, providing continuous, end-to-end, two-way interaction between the editorial team and the subscriber.
