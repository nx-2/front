<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

// Создается уведомление если при отправке пдф фтп-подписчикам произошли ошибки
chdir(dirname(dirname(dirname(__DIR__))));

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

include 'app_init.php';

$db = $serviceManager->get('nx\Db\Adapter');

$sql           = new Sql($db);
$select        = $sql->select()
	->from(['L'=>'magazinesubscribe_ftp_log'])->columns(['id', 'comment'])
	->where(['L.status' => 0])
	->where('L.created >= SUBDATE(NOW(), INTERVAL 4 HOUR)');
$selectString  = $sql->getSqlStringForSqlObject($select);
$result        = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
$items         = $result->toArray();

if(!empty($items)) {
	$data = ['message' => 'ошибки при отправке pdf для ftp подписок', 'type' => 2, 'created' => date('Y-m-d H:i:s')];
	$insert      = $sql->insert('nx_notification')->values($data);
	$queryString = $sql->getSqlStringForSqlObject($insert);
	$result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
}

