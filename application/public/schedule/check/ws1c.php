<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

// проверка доступности веб-сервисов 1c и создание уведомления если выявлена недоступность
chdir(dirname(dirname(dirname(__DIR__))));
include 'app_init.php';

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use nx\Service\Ws1c;

$db = $serviceManager->get('nx\Db\Adapter');
$publisherModel = $serviceManager->get('nx\Model\Publisher');

$queryString = "SELECT p.id FROM publisher p LEFT JOIN integration1c_verify iv ON p.id = iv.publisher_id WHERE p.enabled = 1 AND iv.is_verified = 1";
$publisherIds = $db->query($queryString)->execute();
$pIds = [];
while ($c = $publisherIds->current()) {
    if (isset($c['id']))
        $pIds [] = $c['id'];
    $publisherIds->next();
}

foreach ($pIds as $publisherId) {
    $publisher = $publisherModel->getOne($publisherId);
    $options = isset($publisher['integration_1c']) ? $publisher['integration_1c'] : false;

    try {
        $client = @new Ws1c(2,$options);
    } catch(\Exception $e) {
        //ini_set('default_socket_timeout', $old);

        $data = ['message' => 'ошибка при подключении к 1с сервису по адресу ' . $options['url'], 'type' => 4, 'created' => date('Y-m-d H:i:s')];
        $sql         = new Sql($db);
        $insert      = $sql->insert('nx_notification')->values($data);
        $queryString = $sql->getSqlStringForSqlObject($insert);
        $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
        continue;
    } //finally {
    //ini_set('default_socket_timeout', $old);
}
return;

