<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

// Создается уведомление если за прошедшие N дней не было ни одной покупки ПДФ
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

chdir(dirname(dirname(dirname(__DIR__))));
include 'app_init.php';

$db = $serviceManager->get('nx\Db\Adapter');
$sql           = new Sql($db);
$select  = $sql->select()->from(['P' => 'publisher'])->columns(['id'])->where(['enabled' => 1]);
$queryString = $sql->buildSqlString($select);
$result = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
$publishers = $result->toArray();

$days = 3;

foreach ( $publishers as $publisher ) {
    $publisherId = $publisher['id'];

    $select        = $sql->select()
        ->from(['O'=>'nx_order'])->columns(['id'])
        ->join(['OI' => 'nx_order_item'], 'OI.order_id = O.id', [])
        ->join(['P' => 'nx_price'], 'O.price_id = P.id', [])
        ->where(['OI.item_type' => 'Issue'])
        ->where(['O.order_state_id' => 3])
        ->where(['P.publisher_id' => $publisherId])
        ->where('O.created >= SUBDATE(NOW(),' . $days . ')');
    $selectString  = $sql->getSqlStringForSqlObject($select);
    $result        = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
    $items         = $result->toArray();

    if(empty($items)) {
        $data = ['message' => 'за последние ' . $days .  ' суток не найдены оплаченные покупки pdf', 'type' => 3, 'created' => date('Y-m-d H:i:s'), 'publisher_id' => $publisherId];
        $insert      = $sql->insert('nx_notification')->values($data);
        $queryString = $sql->getSqlStringForSqlObject($insert);
        $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
    }
}


