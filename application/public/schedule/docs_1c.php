<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

chdir(dirname(dirname(__DIR__)));
require 'init_autoloader.php';
//echo getcwd() . "\n";die;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

$configuration = include 'config/application.config.php';

$serviceManager = new ServiceManager(new ServiceManagerConfig());
$serviceManager->setService('ApplicationConfig', $configuration);


// load modules -- which will provide services, configuration, and more
$serviceManager->get('ModuleManager')->loadModules();

$application = $serviceManager->get('Application');
$application->bootstrap();

//$transactionModel  = $serviceManager->get('nx\Model\Transaction');
//$paymentModel      = $serviceManager->get('nx\Model\Payment');
$subscriptionModel = $serviceManager->get('nx\Model\Subscription');
$orderService      = $serviceManager->get('nx\Service\Order');
$releaseService    = $serviceManager->get('nx\Service\SubscriptionItemRelease');

$shippings = $subscriptionModel->itemReleaseModel->getShippingsBy(['is_doc_wait' => 1, 'limit' => 20]);

if (!empty($shippings)) {
	$ids = [];
	$idsByOrder = [];

	foreach ($shippings as $shipping ) {
	    if (!isset($idsByOrder[$shipping['order_id']]))
	        $idsByOrder[$shipping['order_id']] = [];

	    $idsByOrder[ $shipping['order_id'] ] [] = $shipping['id'];
    }

//	foreach ($shippings as $shipping) {
//		$ids[] = $shipping['id'];
//	}

	foreach ($idsByOrder as $orderId => $ids) {

        $sales_data  = [];
        $data        = $releaseService->get1CDocsData_Release(['ids' => $ids, 'prefix' => 'release']);
        $sales_data  = $data['sales_data'];

        if ( !empty($sales_data) ) {
            $results = $orderService->request1CDocs(
                [
                    'selling' => true,
                    'invoice' => true,
                    'type' => 'order_item_release',
                    'prefix' => 'release',
                    'data' => $sales_data,
                    'order_id' => $orderId
                ]
            );
            if (!empty($results)) {
                foreach ($results as $release_id => $result) {
                    $release_data = ['last_request_date' => date('Y-m-d H:i:s')];
                    $release_data['last_request'] = serialize($result['request']);
                    if (!empty($result['error'])) {
                        $subscriptionModel->fileStorageModel->delete(['entity_id' => $release_id, 'entity_type' => 'order_item_release']);
                        $release_data['doc_result'] = $result['error'];
                        //$subscriptionModel->itemReleaseModel->update(array('doc_result' => $result['error'], 'last_request' => serialize($result['request'])), array('id' => $release_id));
                        foreach ( $sales_data as $i => $sale_data ) {
                            $sale_release_id = str_replace('release','',$sale_data['SaleId']);
                            if ( $sale_release_id == $release_id ) {
                                unset($sales_data[$i]);
                            }
                        }
                    } else {
                        $release_data['doc_result'] = '';
                    }
                    $subscriptionModel->itemReleaseModel->update($release_data, ['id' => $release_id]);
                }
                if ( !empty($sales_data) ) {
                    $sales_data = array_values($sales_data);
                    $orderService->request1CDocs(
                        [
                            'selling' => true,
                            'invoice' => true,
                            'signed' => true,
                            'type' => 'order_item_release',
                            'prefix' => 'release',
                            'data' => $sales_data,
                            'order_id' => $orderId,
                        ]
                    );
                }
            }
        }
        $subscriptionModel->itemReleaseModel->update(['is_doc_wait' => 0], ['id' => $ids]);

    }
}
//$paymentModel->sendNdsTo1c();
