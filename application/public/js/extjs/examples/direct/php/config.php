<?php
$API = [
    'TestAction'=> [
        'methods'=> [
            'doEcho'=> [
                'len'=>1
            ],
            'multiply'=> [
                'len'=>1
            ],
            'getTree'=> [
                'len'=>1
            ],
            'getGrid'=> [
                'len'=>1
            ],
            'showDetails'=> [
                'params'=> [
                    'firstName',
                    'lastName',
                    'age'
                ]
            ]
        ]
    ],

    'Profile'=> [
        'methods'=> [
            'getBasicInfo'=> [
                'len'=>2
            ],
            'getPhoneInfo'=> [
                'len'=>1
            ],
            'getLocationInfo'=> [
                'len'=>1
            ],
            'updateBasicInfo'=> [
                'len'=>0,
                'formHandler'=>true
            ]
        ]
    ]
];
