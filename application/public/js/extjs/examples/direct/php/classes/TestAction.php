<?php
class TestAction {
    function doEcho($data){
        return $data;
    }

    function multiply($num){
        if(!is_numeric($num)){
            throw new Exception('Call to multiply with a value that is not a number');
        }
        return $num*8;
    }

    function getTree($id){
        $out = [];
        if($id == "root"){
        	for($i = 1; $i <= 5; ++$i){
        	    array_push($out, [
        	    	'id'=>'n' . $i,
        	    	'text'=>'Node ' . $i,
        	    	'leaf'=>false
                ]);
        	}
        }else if(strlen($id) == 2){
        	$num = substr($id, 1);
        	for($i = 1; $i <= 5; ++$i){
        	    array_push($out, [
        	    	'id'=>$id . $i,
        	    	'text'=>'Node ' . $num . '.' . $i,
        	    	'leaf'=>true
                ]);
        	}
        }
        return $out;
    }

    function getGrid($params){
        $sort = $params->sort[0];
        $field = $sort->property;
        $direction = $sort->direction;

        /*
         * Here we would apply a proper sort from the DB, but since
         * it's such a small dataset we will just sort by hand here.
         */

        if ($field == 'name') {
            $data = [[
                'name'=>'ABC Accounting',
                'turnover'=>50000
            ], [
                'name'=>'Ezy Video Rental',
                'turnover'=>106300
            ], [
                'name'=>'Greens Fruit Grocery',
                'turnover'=>120000
            ], [
                'name'=>'Icecream Express',
                'turnover'=>73000
            ], [
                'name'=>'Ripped Gym',
                'turnover'=>88400
            ], [
                'name'=>'Smith Auto Mechanic',
                'turnover'=>222980
            ]];
        } else {
            $data = [[
                'name'=>'ABC Accounting',
                'turnover'=>50000
            ], [
                'name'=>'Icecream Express',
                'turnover'=>73000
            ], [
                'name'=>'Ripped Gym',
                'turnover'=>88400
            ], [
                'name'=>'Ezy Video Rental',
                'turnover'=>106300
            ], [
                'name'=>'Greens Fruit Grocery',
                'turnover'=>120000
            ], [
                'name'=>'Smith Auto Mechanic',
                'turnover'=>222980
            ]];
        }
        if ($direction == 'DESC') {
            $data = array_reverse($data);
        }
        return $data;
    }

    function showDetails($data){
        $first = $data->firstName;
        $last = $data->lastName;
        $age = $data->age;
        return "Hi $first $last, you are $age years old.";
    }
}
