<?php

include '../connection.php';

try {
    $statement = $db->prepare('select * from task');

    if(!$statement->execute()) {
        throw new Exception(implode(', ', $statement->errorInfo()));
    }
    $jsonResult = [
        'success' => true,
        'tasks' => $statement->fetchAll(PDO::FETCH_ASSOC)
    ];
} catch(Exception $e) {
    $jsonResult = [
        'success' => false,
        'message' => $e->getMessage()
    ];
}

echo json_encode($jsonResult);

?>
