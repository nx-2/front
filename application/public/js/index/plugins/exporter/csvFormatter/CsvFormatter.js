/**
 * @class Ext.ux.Exporter.CSVFormatter
 * @extends Ext.ux.Exporter.Formatter
 * Specialised Format class for outputting .csv files
 */
Ext.define("Ext.ux.exporter.csvFormatter.CsvFormatter", {
    extend: "Ext.ux.exporter.Formatter",
    contentType: 'data:text/csv;base64,',
    separator: ";",
    extension: "csv",

    format: function(store, config) {
        this.columns = config.columns || (store.fields ? store.fields.items : store.model.prototype.fields.items);
        this.grid = config.grid || {};
        return this.getHeaders() + "\n" + this.getRows(store);
    },
    getHeaders: function(store) {
        var columns = [], title;
        Ext.each(this.columns, function(col) {
          var title;
          if (col.text != undefined) {
            title = col.text;
          } else if(col.name) {
            title = col.name.replace(/_/g, " ");
            title = Ext.String.capitalize(title);
          }

          columns.push(title);
        }, this);

        return columns.join(this.separator);
    },
    getRows: function(store) {
        var rows = [];
        store.each(function(record, index) {
          rows.push(this.geCell(store, record, index));
        }, this);

        return rows.join("\n");
    },
    geCell: function(store, record, index) {
        var cells = [];
        Ext.each(this.columns, function(col, colIndex) {
            var name = col.name || col.dataIndex;
            if(name) {
                if (Ext.isFunction(col.renderer)) {
                  var value = col.renderer(record.get(name), {exporter: true}, record, index, colIndex, store, this.grid.getView());
                } else {
                  var value = record.get(name);
                }
                value = Ext.util.Format.stripTags(value);
                cells.push(value);
            }
        }, this);

        return cells.join(this.separator);
    }
});