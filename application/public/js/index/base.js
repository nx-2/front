/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 *
 */

/**
 * Открывает окно с сообщением системы
 * @param string infoName
 * @param string infoMessage
 * @param string infoType (error/info/question/warning)
 */
var infoReport = function(infoName, infoMessage, infoType) {
    infoName    = infoName || '';
    infoMessage = infoMessage || '';
    Ext.Msg.show({
        title   : 'Сообщение системы',
        msg     : '<b>' + infoName + '</b><br/>' + infoMessage,
        buttons : Ext.Msg.OK,
        icon    : Ext.Msg.ERROR//,
        //iconCls : 'settings-ico'
    });
    throw new Error(infoName + " " + infoMessage); //die
}

var page_size = 25;

var ProcessLocation = function(app, token, opts) 
{
    var currentToken = Ext.History.getToken();
    //if(currentToke != token)
    //{
    //  Ext.History.add(componentToken);
    //}
    var controller = 'subscription';
    var action     = 'index';

    var segments = token ? token.split('/') : {};

    if(segments[0])
    {
        controller = segments[0];
    }

    if(segments[1])
    {
        action = segments[1];
    }

    var params = {opts: opts};
    if(segments[2])
    {
        for(i = 2; i < segments.length; i++)
        {
            if(i%2==1)
            {
                params[segments[i-1]] = segments[i];
            }
        }
    }

    action = action + 'Action';

    app.getController(controller)[action](params);
}

var ProcessView = function(controller, view, params)
{
    var contentPanel = Ext.ComponentQuery.query('#NXContentPanel');
    if(contentPanel[0].items && contentPanel[0].items.getCount() && contentPanel[0].items.getAt(0).store) {
        contentPanel[0].items.getAt(0).store.clearFilter(true);
    }
    var view = controller.getView(view).create({nxparams: params});
    //controller.mainview = view;
    //view.controller = controller;
    if(view.store && (view.store.autoLoad==undefined || view.store.autoLoad)) 
    {
        view.store.pageSize = page_size;
        view.store.getProxy().extraParams = params;
        //view.store.clearFilter(true);
        view.store.loadPage(1);//load();
    }
    if(params.state)//view.isXType('gridpanel') && )
    {
        ProcessViewState(view, params.state);
    }
    contentPanel[0].removeAll(true);
    contentPanel[0].add(view);
    return view;
}

var ProcessViewState = function(view, state_id)
{
    Ext.Ajax.request({
        url     : '/view-state/get',
        method  : 'GET',
        params  : { id: state_id },
        scope   : this,
        success : function(response) {
            var response = Ext.JSON.decode(response.responseText);
            if(response.data) {
                if(view.itemId == response.data.viewid)
                {
                    if(response.data.data) {
                        var switcher = Ext.create('nx.ux.view_state_switcher');
                        switcher.applyState(view, response.data);
                    }
                }
            }
        }
    });
}

var submitFakeForm = function (url, data, filename) 
{
    var form = Ext.get('fakeForm');
    form.set({
        action : url
    });
    if(data!=undefined)
    {
        form.set({
            method : 'post'
        }).first().set({
            value : data
        }).next().set({
            value : filename
        });
    }
    document.forms.fakeForm.submit();
    return false;
}

var convertDataToTree = function (data)
{
    var results = [];
    if(data instanceof Array || Object.prototype.toString.call(data) === '[object Array]')
    {
        for(var i = 0, len = data.length; i < len; i++)
        {
            var value   = data[i];
            if(Object.prototype.toString.call(value) === '[object Object]' || Object.prototype.toString.call(value) === '[object Array]')
            {
                var children = this.convertDataToTree(value);
                results.push({
                    'text'     : i,
                    'children' : children,
                    //'expanded' : true,
                    'value'    : '',
                    'leaf'     : 0
                });
            }
            else
            {
                results.push({
                    'text'  : i,// + ': ' + value,
                    'value' : value,
                    'leaf'  : 1
                });
            }
        }
    }
    else if(Object.prototype.toString.call(data) === '[object Object]')
    {
        for(var prop in data)
        {
            var value = data[prop];
            if(Object.prototype.toString.call(value) === '[object Object]' || Object.prototype.toString.call(value) === '[object Array]')
            {
                var children = this.convertDataToTree(value);
                results.push({
                    'text'     : prop,
                    'children' : children,
                    //'expanded' : true,
                    'value'    : '',
                    'leaf'     : 0
                });
            }
            else
            {
                results.push({
                    'text'  : prop,// + ': ' + value,
                    'value' : value,
                    'leaf'  : 1
                });
            }
        }
    }
    else
    {
        for(var prop in data)
        {
            var value = data[prop];
            results.push({
                'text'  : prop,// + ': ' + value,
                'value' : value,
                'leaf'  : 1
            });
        }
    }
    return results;
}

var showDataAsTree = function(data, params)
{
    params = params || {};
    var buttons = [
        {
            text: 'Закрыть',
            handler: function(btn)
            {
                btn.up('window').close();
                //Ext.ComponentQuery.query('#dataTreeWindow')[0].close();
            }
        }
    ];
    if(params.buttons) {
        buttons = Ext.Array.push(buttons, params.buttons);
    }
    new Ext.window.Window({
        title     : params.title ? params.title : 'Данные',
        itemId    : 'dataTreeWindow',
        iconCls   : 'watch-ico',
        layout    : 'fit',
        //width     : 700,
        //height    : 600,
        border    : false,
        resizable : false,
        //modal     : true,
        autoShow  : true, 
        items     : [
            {
                xtype       : 'treepanel',
                width       : 400,
                height      : 500,
                autoShow    : true,
                rootVisible : false,
                rowLines    : true,
                bodyCls     : 'x-tree-noicon',
                store       : new Ext.data.TreeStore({
                    autoLoad : true,
                    fields   : [
                        {name: 'text'},
                        {name: 'value'}
                    ],
                    root: {
                        text: 'Данные',
                        expanded: true,
                        children: convertDataToTree(data)
                    }
                }),
                columns: [
                    {xtype: 'treecolumn', dataIndex: 'text', text: 'свойство', width:190},
                    {dataIndex: 'value', text: 'значение', flex:1, tdCls: 'nx-multiline', renderer: 'htmlEncode'}
                ],
                listeners: {
                    'render' : function() {
                        if(params.expand == 'first') {
                            this.getRootNode().getChildAt(0).expandChildren(true);
                            this.getRootNode().getChildAt(0).expand();
                        }
                        else {
                            this.expandAll();
                        }
                        //this.getRootNode().getChildAt(2).collapse();
                    }
                },
                viewConfig : { 
                    enableTextSelection: true
                }
            }
        ],
        buttons : buttons
    });
}

var callWithSuspendEvent = function(el, func, args, event_name)
{
    el.suspendEvent(event_name);
    el[func](args);
    el.resumeEvent(event_name);
}

/*var showByXtype =  function(xtype, params)
{
    new Ext.window.Window({
        //title     : '',
        autoShow  : true,
        width     : 800,
        maxHeight : 600,
        layout    : 'fit',
        y         : 100,
        items     : [
            {
                xtype: xtype,
                nxparams: params
            }
        ]
    });
}*/