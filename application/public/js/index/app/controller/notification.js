/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.notification', {
    extend: 'nx.controller.base',
    views: [
        'notification.index'
    ],

    init: function() 
    {
        this.control({
            'nx_notification_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
                'nx_switch_field' : function(grid, rec, field)
                {
                    grid.el.mask();
                    this.update_field(rec.get('id'), field, rec.get(field)=='1' ? 0 : 1, grid);
                },
            }
        });
    }
});