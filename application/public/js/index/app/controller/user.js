/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.user', {
    extend: 'nx.controller.base',
    views: [
        'user.index',
        'user.edit'
    ],

    stores: [
        'user'
    ],

    models: ['user'],

    init: function() 
    {   
        this.control({
            'nx_user_list' : {
                nx_user_edit: function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id')});
                },
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                }
            }
        });
    }
});