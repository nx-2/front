/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.person', {
    extend: 'nx.controller.base',
    views: [
        'person.index',
        'person.edit',
        'person.add'
    ],

    stores: [
        'person'
    ],

    models: ['person'],

    init: function() 
    {
        this.control({
            'nx_person_add button[action=add]': {
                click: function(btn) {
                    this.add_db_record({view: btn.up('window')});
                }
            },
            'nx_person_edit button[action=save]': {
                click: function(btn) {
                    this.update_db_record({view: btn.up('window')});
                }
            },
            'nx_person_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) 
                {
                    grid.getStore().loadPage(1);
                },
                'nx_person_add' : function(grid)
                {
                    this.open_add_form({grid: grid});
                },
                'nx_person_edit' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id')});
                }
            }
        });
    }
});