/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.company', {
    extend: 'nx.controller.base',
    views: [
        'company.index',
        'company.edit',
        'company.add',
        'company.ws1c'
    ],

    stores: [
        'company',
        'person'
    ],

    models: ['company'],

    init: function() 
    {
        this.control({
            'nx_company_edit button[action=save]': {
                click: function(btn) {
                    this.update_db_record({view: btn.up('window')});
                }
            },
            'nx_company_edit button[action=clone]': {
                click: function(btn) {
                    this.add_db_record({view: btn.up('window')});
                }
            },
            'nx_company_add button[action=add]': {
                click: function(btn) {
                    this.add_db_record({view: btn.up('window')});
                }
            },
            'nx_company_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {

                    /*var params = {};
                    filters.each(function(filter){
                        params[filter.property] = filter.value
                        //filter.exactMatch = true;
                    });

                    grid.store.filters = filters;
                    grid.getStore().reload({
                        params : params
                    });*/
                    grid.getStore().loadPage(1);
                    //console.log(grid.store.filters);
                    //grid.getStore().filter({property: 'name', value: /test/});
                    //grid.getStore().filter();
                    //console.log(grid, grid.store.filters, filters, last_filters, active);
                    //grid.applyHeaderFilters();
                },
                'nx_company_edit' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id')});
                },
                'nx_company_add' : function(grid)
                {
                    this.open_add_form({grid: grid});
                },
                'nx_company_clone' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id'), clone: 1});
                },
                'nx_company_send_ws1c' : function(button)
                {
                    var grid = button.up('grid');
                    var selection = grid.getSelectionModel().getSelection();  
                    if(selection.length)
                    {
                        var ids = [];
                        Ext.each(selection, function(row) {
                            ids.push(row.get('id'));
                        });
                        this.sendTo1C(ids, grid, button);         
                    }
                    else {
                        Ext.Msg.alert('Ошибка', 'Не выбраны организации');
                    }
                }
            },
            'nx_company_edit' : {
                'nx_person_edit' : function(grid, rec)
                {
                    this.getController('person').open_edit_form({id: rec.get('id')});
                }
            },
            'nx_company_ws1c' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                }
            }
        });
    },
    ws1cAction : function(params)
    {
        ProcessView(this, this.id + '.ws1c', params);
    },
    sendTo1C: function(ids, grid, el)
    {
        Ext.Msg.confirm('Подтверждение','Отправить в 1С (' + ids.length + ')?', function(btn){
            if(btn=='yes')
            {
                if(el) {
                    el.setLoading(true);
                }
                Ext.Ajax.request({
                    url     : '/company/send-ws1c',
                    method  : 'POST',
                    scope   : this,
                    params  : {
                        ids : ids.join(',')
                    },
                    success : function(response) {
                        if(el) {
                            el.setLoading(false);
                        }
                        var response = Ext.JSON.decode(response.responseText);
                        this.show1CResult(response.result);
                        if(grid) {
                            grid.getStore().reload();
                        }
                    }
                });
            }
        },this);
    },
    show1CResult: function(data){
        new Ext.window.Window({
            title     : 'Передача организаций в 1С',
            //iconCls   : 'payment-ico',
            layout    : 'fit',
            border    : false,
            resizable : true,
            autoShow  : true, 
            items     : [
                {
                    xtype : 'grid',
                    width : 500,
                    height: 500,
                    autoShow: true,
                    store: new Ext.data.JsonStore({
                        proxy: {
                            type: 'memory',
                            reader: {
                                type: 'json',
                                root: 'items'
                            }
                        },
                        data         : {'items' : data},
                        pageSize     : 9999,
                        autoLoad     : true,
                        fields       : [
                            {name: 'id'},
                            {name: 'result'}
                        ]
                    }),
                    columns    : [
                        {xtype: 'rownumberer', width: 50},
                        {text: 'id', dataIndex: 'id', width: 70},
                        {text: 'комментарий', dataIndex: 'result', flex: 1, tdCls: 'nx-multiline'},
                    ],
                }
            ], 
            buttons : [
                {
                    text: 'Закрыть',
                    handler: function() {
                        this.up('window').close();
                    }
                }
            ]
        });
    }
});