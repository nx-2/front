/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.maillog', {
    extend: 'nx.controller.base',
    views: [
        'maillog.index',
        'maillog.archive'
    ],

    stores: [
        'maillog'
    ],

    init: function() 
    {
        this.control({
            'nx_maillog_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                }
            },
            'nx_maillog_archive' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                }
            }
        });
    },
    archiveAction : function(params)
    {
        ProcessView(this, this.id + '.archive', {'from': 'archive'});
    }
});