/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.payment', {
    extend: 'nx.controller.base',
    views: [
        //'payment.index',
        'payment.edit',
        'payment.add',
        'payment.order',
        'payment.transaction'
    ],

    stores: [
        'payment'
    ],

    init: function() 
    {
        this.control({
            'nx_payment_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                }
            },
            'nx_payment_order' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
                'nx_switch_field' : function(grid, rec, field)
                {
                    this.update_field(rec.get('id'), field, rec.get(field)=='1' ? 0 : 1);
                },
                'nx_open_order_edit_form': function(grid, order_id) {
                    //this.getController('subscription').open_edit_form({id: order_id});
                    this.getController('order').open_form({id: order_id});
                }
            },
            'nx_payment_transaction' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                }
            }
        });
    },
    /*indexAction : function(params)
    {
        var view = ProcessView(this, this.id + '.index', params);
        //view.store.filters.removeAtKey('label');
        //view.store.load();
    },*/
    orderAction : function(params)
    {
        ProcessView(this, this.id + '.order', params);
    },
    transactionAction : function(params)
    {
        ProcessView(this, this.id + '.transaction', params);
    }
});