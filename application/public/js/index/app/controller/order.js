/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.order', {
    extend: 'nx.controller.base',
    views: [
        'order.index',
        'order.edit',
        'order.add',
        'order.stats',
        'order.stattable'
        //'order.message'
    ],

    stores: [
        'order'
    ],

    init: function() 
    {
        this.control({
            'nx_order_edit button[action=save]': {
                click: function(button) 
                {
                    var view = button.up('nx_order_edit');
                    var form = view.down('form').getForm();
                    var old_state_id = form.findField('order_state_id_old').getValue();
                    var new_state_id = form.findField('order_state_id').getValue();
                    var order_id     = form.findField('id').getValue()
                    var email        = form.findField('email').getValue()
                    if(old_state_id!=3 && new_state_id==3)
                    {
                        Ext.Msg.confirm('Подтверждение', 'Выслать письмо об оплате заказа?', function(btn)
                        {
                            this.update_db_record({view: view});
                            if (btn == 'yes')
                            {
                                //Ext.widget('nx_order_message', {order_id: order_id});
                                Ext.widget('nx_ux_mailform', {
                                    entity_id       : order_id, 
                                    log_message     : 'Отправка письма для заказа #' + order_id,
                                    get_message_url : '/order/get-message-after-pay',
                                    email           : email,
                                    templates       : [
                                        {id: 1, name: 'Шаблон покупки/отправки PDF'},
                                    ]
                                });
                            }
                        }, this);
                    }
                    else
                    {
                        this.update_db_record({view: view});
                    }
                }
            },
            'nx_order_add button[action=add]': {
                click: function() {
                    this.add_db_record();
                }
            },
            'nx_order_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
                'nx_order_edit' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id')});
                },
                'nx_order_add' : function(grid)
                {
                    this.open_add_form();
                },
                'nx_order_switch_field' : function(grid, rec, field)
                {
                    this.update_field(rec.get('id'), field, rec.get(field)=='1' ? 0 : 1);
                }
            }
        });
    },
    statAction : function(params)
    {
        ProcessView(this, this.id + '.stattable', params);
    },
    statsAction : function(params)
    {
        ProcessView(this, this.id + '.stats', params);
    },
    open_form: function(order_id)
    {
        this.get_action({id: order_id, success: function(response) 
            {
                var response = Ext.JSON.decode(response.responseText);
                var shop_id = response.data.shop_id;
                if(shop_id == 2)
                {
                    this.open_edit_form({id: order_id});
                }
                else
                {
                    this.getController('subscription').open_edit_form({id: order_id});
                }
            }
        });
    }
});