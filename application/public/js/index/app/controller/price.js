/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.price', {
    extend: 'nx.controller.base',
    views: [
        'price.index',
        'price.magazine',
        //'price.zone_country',
        //'price.zone_country_form',
        //'price.zone_delivery',
        //'price.zone_delivery_form',
        'price.zone',
        'price.zone_form'
    ],

    init: function() 
    {
        this.control({
            'nx_price_magazine button[action=add], nx_price_magazine button[action=save]': {
                click: function(btn)
                {
                    var view = btn.up('window');
                    var form = view.down('form').getForm(); 
                    if (form.isValid()) 
                    {
                        var store = view.down('grid').store;
                        //store.commitChanges();
                        var rows  = store.getRange();
                        var data  = [];
                        Ext.each(rows, function(row){
                            data[data.length] = row.getData();
                        });
                        var values = form.getValues();
                        values.data = Ext.encode(data);
                        if(btn.action == 'save') {
                            values.id = view.price_id; 
                        }
                        Ext.Ajax.request({
                            url    : '/price/save-from-grid',
                            method : 'POST',
                            scope  : this,
                            params : values,
                            success : function(response) {
                                var response = Ext.JSON.decode(response.responseText);
                                view.grid.store.reload();
                                view.close();
                            }
                        }, this);
                    }
                }
            },
            'nx_price_list' : {
                nx_price_edit: function(grid, rec)
                {
                    Ext.Ajax.request({
                        url    : '/price/get',
                        method : 'POST',
                        scope  : this,
                        params : { 
                            id : rec.get('id'),
                        },
                        success : function(response) {
                            var response = Ext.JSON.decode(response.responseText);
                            var view = Ext.widget('nx_' + this.id + '_magazine', {nxdata: response, grid: grid, mode: 'edit', price_id: rec.get('id')});
                        }
                    }, this);
                    
                },
                'nx_price_add' : function(grid)
                {
                    Ext.Ajax.request({
                        url    : '/price/get',
                        method : 'POST',
                        scope  : this,
                        success : function(response) {
                            var response = Ext.JSON.decode(response.responseText);
                            var view = Ext.widget('nx_' + this.id + '_magazine', {nxdata: response, grid: grid, mode: 'add'});
                        }
                    }, this);
                },
            },
            /*'nx_price_zone_country': {
                'nx_zone_country_add' : function(grid)
                {
                    Ext.widget('nx_' + this.id + '_zone_country_form', {nxparams: {grid: grid}});
                },
                'nx_zone_country_edit' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id'), view: 'nx_price_zone_country_form', url: '/zone/zone-country-get', grid: grid});
                },
                'nx_zone_country_delete': function(grid, rec)
                {
                    Ext.Msg.confirm('Подтверждение', 'Удалить привязку?', function(btn){
                        if (btn == 'yes')
                        {   
                            Ext.Ajax.request({
                                url     : '/zone/zone-country-delete',
                                method  : 'POST',
                                waitMsg : 'Отправка данных',
                                scope   : this,
                                params  : {
                                    id: rec.get('id')
                                },
                                success: function(form, action) 
                                {
                                    grid.getStore().reload();
                                }
                            });
                        }
                    }, this);
                }
            },
            'nx_price_zone_country_form button[action=add]': {
                click: function(btn)
                {
                    this.add_db_record({view: btn.up('window'), url: '/zone/zone-country-save', redirect: 'price/zonecountry'});
                }
            },
            'nx_price_zone_country_form button[action=save]': {
                click: function(btn)
                {
                    this.update_db_record({view: btn.up('window'), url: '/zone/zone-country-save', redirect: 'price/zonecountry'});
                }
            },
            'nx_price_zone_delivery': {
                'nx_zone_delivery_edit' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id'), view: 'nx_price_zone_delivery_form', url: '/zone/zone-delivery-get', grid: grid});
                },
            },
            'nx_price_zone_delivery_form button[action=save]': {
                click: function(btn)
                {
                    this.update_db_record({view: btn.up('window'), url: '/zone/zone-delivery-save', redirect: 'price/zonedelivery'});
                }
            },*/
            'nx_price_zone': {
                'nx_zone_add' : function(grid)
                {
                    Ext.widget('nx_price_zone_form', {nxparams: {grid: grid}});
                },
                'nx_zone_edit' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id'), view: 'nx_price_zone_form', url: '/zone/get', grid: grid});
                },
                'nx_zone_delete': function(grid, rec)
                {
                    Ext.Msg.confirm('Подтверждение', 'Удалить?', function(btn){
                        if (btn == 'yes')
                        {   
                            Ext.Ajax.request({
                                url     : '/zone/delete',
                                method  : 'POST',
                                waitMsg : 'Отправка данных',
                                scope   : this,
                                params  : {
                                    id: rec.get('id')
                                },
                                success: function(form, action) 
                                {
                                    grid.getStore().reload();
                                }
                            });
                        }
                    }, this);
                }
            },
            'nx_price_zone_form button[action=add]': {
                click: function(btn)
                {
                    this.add_db_record({view: btn.up('window'), url: '/zone/save', redirect: 'price/zone'});
                }
            },
            'nx_price_zone_form button[action=save]': {
                click: function(btn)
                {
                    this.update_db_record({view: btn.up('window'), url: '/zone/save', redirect: 'price/zone'});
                }
            },
        });
    },
    zoneAction: function(params)
    {
        ProcessView(this, this.id + '.zone', params);
    },
    zonecountryAction: function(params)
    {
        ProcessView(this, this.id + '.zone_country', params);
    },
    zonedeliveryAction: function(params)
    {
        ProcessView(this, this.id + '.zone_delivery', params);
    },
});