/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.tag', {
    extend: 'nx.controller.base',
    views: [
        'tag.index',
        'tag.add',
        'tag.edit'
    ],

    init: function() 
    {
        this.control({
            'nx_tag_add button[action=add]': {
                click: this.add_db_record
            },
            'nx_tag_edit button[action=save]': {
                click: function(btn) { 
                    this.update_db_record({view: btn.up('window')}); 
                }
            },
            'nx_tag_list' : {
                nx_tag_edit: function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id'), grid: grid});
                },
                'nx_tag_add' : function(grid)
                {
                    this.open_add_form({grid: grid});
                },
                headerfilterchange : function(grid) {
                    grid.getStore().loadPage(1);
                },
            }
        });
    }
});