/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

/*
базовый контроллер налагает некоторые стандарты наименования
пример
обращение по this.id (допустим = company) работает, если выполнены условия:
zend2 контроллер - companyController
extjs контроллер - nx.controller.company
extjs view - nx.view.company.index
extjs store - nx.store.company
extjs model - nx.model.company
*/
Ext.define('nx.controller.base', 
{
    extend: 'Ext.app.Controller',

    redirect_to_index: function()
    {
        var hash = '';
        hash = this.id + '/index';
        window.location.hash = hash;
    },

    indexAction : function(params)
    {
        ProcessView(this, this.id + '.index', params);
    },

    editAction: function(params) 
    {
        if(params.id)
        {
            var view = ProcessView(this, this.id + '.edit', params);
            this.load_edit_form({view: view, id: params.id});
        }
        else
        {
            this.redirect_to_index();
        }
    },

    get_action: function(params)
    {
        Ext.Ajax.request({
            url     : '/' + this.id + '/get',
            method  : 'POST',
            waitMsg : 'Отправка данных',
            scope   : this,
            params  : {id: params.id},
            success : params.success
        });
    },

    load_edit_form: function(params)//view, id)
    {
        var view = params.view;
        var id   = params.id;
        view.el.mask('Загрузка');
        view.down('form').getForm().load(
        {
            url     : params.url ? params.url : '/' + this.id + '/get',
            //waitMsg : 'Загрузка',
            scope   : view,
            params  : 
            {
                id : id
            },
            success : function() 
            {
                this.el.unmask();
            }
        });
    },

    open_edit_form: function(params)
    {
        var view = Ext.widget(params.view ? params.view : 'nx_' + this.id + '_edit', {nxparams: params});
        this.load_edit_form({view: view, id: params.id, url: params.url});
    },

    open_add_form: function(params)
    {
        Ext.widget('nx_' + this.id + '_add', {nxparams: params});
    },

    update_db_record: function(params)
    {
        var params = params || {};
        var view   = params.view ? params.view : Ext.ComponentQuery.query('nx_' + this.id + '_edit')[0];
        var form   = view.down('form');//Ext.ComponentQuery.query('nx_' + this.id + '_edit form')[0];
        form       = form.getForm();

        if (form.isValid()) 
        {
            //view.el.mask('Отправка данных');
            form.submit(
            {
                url     : params.url ? params.url : '/' + this.id + '/update',
                waitMsg : 'Отправка данных',
                scope   : this,
                params  : 
                {
                    id : view.nxparams.id,
                    nx_ui_confirmed: params.confirmed == undefined ? 0 : params.confirmed
                },
                success: function(form, action) 
                {
                    //view.el.unmask();
                    if(!action.result.systemMessage && !action.result.redirect && action.result.confirmMessage)
                    {
                        Ext.Msg.confirm('Подтверждение', action.result.confirmMessage, function(btn){
                            if (btn == 'yes') {
                                this.update_db_record({'confirmed': 1});
                            }
                        }, this);
                    }
                    if (!action.result.systemMessage && !action.result.redirect && !action.result.confirmMessage) 
                    {
                        view.close();
                        params.redirect ? window.location.hash = params.redirect : this.redirect_to_index();
                        //this.getStore(this.id).reload();
                        if(view.nxparams.grid) {
                            view.nxparams.grid.store.reload();
                        }
                        else {
                            var store = Ext.data.StoreManager.lookup(this.id);
                            if(store) {
                                store.reload();
                            }
                        }
                    }
                }
            });
        }
    },

    add_db_record: function(params)
    {
        var params = params || {};
        var view   = params.view ? params.view : Ext.ComponentQuery.query('nx_' + this.id + '_add')[0];
        var form   = view.down('form');
        form       = form.getForm();

        if (form.isValid()) 
        {
            //view.el.mask('Отправка данных');
            form.submit(
            {
                url     : params.url ? params.url : '/' + this.id + '/add',
                waitMsg : 'Отправка данных',
                scope   : this,
                params  : {
                    nx_ui_confirmed: params.confirmed == undefined ? 0 : params.confirmed
                },
                success: function(form, action) 
                {
                    //view.el.unmask();
                    if(!action.result.systemMessage && !action.result.redirect && action.result.confirmMessage)
                    {
                        Ext.Msg.confirm('Подтверждение', action.result.confirmMessage, function(btn){
                            if (btn == 'yes') {
                                this.add_db_record(Ext.apply(params, {'confirmed': 1}));
                            }
                        }, this);
                    }
                    if (!action.result.systemMessage && !action.result.redirect && !action.result.confirmMessage) 
                    {
                        view.close();
                        params.redirect ? window.location.hash = params.redirect : this.redirect_to_index();
                        //this.getStore(this.id).reload();
                        if(view.nxparams.grid) {
                            view.nxparams.grid.store.reload();
                        }
                        else {
                            var store = Ext.data.StoreManager.lookup(this.id);
                            if(store) {
                                store.reload();
                            }
                        }
                    }
                }
            });
        }
    },

    update_field: function(rec_id, field, value, grid)
    {
        var params = {'id': rec_id};
        params[field] = value;
        Ext.Ajax.request({
            url: '/' + this.id + '/update',
            method  : 'POST',
            waitMsg : 'Отправка данных',
            scope   : this,
            params  : params,
            success: function(form, action) 
            {
                if(grid) {
                    grid.store.reload();
                }
                else {
                    this.getStore(this.id).reload();
                }
            }
        });
    }
});