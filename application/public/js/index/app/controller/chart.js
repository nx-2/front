/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.chart', {
    extend: 'nx.controller.base',
    views: [
        'chart.index',
        'chart.edit',
        'chart.add'
    ],

    stores: [
        'chart'
    ],

    //models: ['chart'],

    init: function() 
    {   
        this.control({
            'nx_chart_add button[action=add]': {
                click: this.add_db_record
            },
            'nx_chart_edit button[action=save]': {
                click: function(btn) { 
                    this.update_db_record({view: btn.up('window')}); 
                }
            },
            'nx_chart_list' : {
                nx_chart_edit: function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id')});
                },
                'nx_chart_add' : function()
                {
                    this.open_add_form();
                },
                //headerfilterchange : function(grid, filters, last_filters, active) {
                //    grid.getStore().loadPage(1);
                //}
            }
        });
    }
});