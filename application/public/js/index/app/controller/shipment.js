/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.shipment', {
    extend: 'nx.controller.base',
    views: [
        'shipment.index',
        'shipment.reclamation'
    ],

    init: function() 
    {
        this.control({
            'nx_shipment_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
            },
            'nx_shipment_reclamation' : {
                headerfilterchange : function(grid) {
                    grid.getStore().loadPage(1);
                },
            }
        });
    },

    reclamationAction: function(params)
    {
        ProcessView(this, this.id + '.reclamation', params);
    }
});