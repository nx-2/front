/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.role', {
    extend: 'nx.controller.base',
    views: [
        'role.index',
        'role.add',
        'role.edit'
    ],

    stores: [
        'role'
    ],

    init: function() 
    {   
        this.control({
            'nx_role_add button[action=add]': {
                click: this.add_db_record
            },
            'nx_role_edit button[action=save]': {
                click: function(btn) {
                    this.update_db_record({view: btn.up('window')});
                }
            },
            'nx_role_list': {
                'nx_role_add' : function(grid)
                {
                    this.open_add_form();
                },
                'nx_role_edit' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id')});
                }
            }
        });  
    },
});