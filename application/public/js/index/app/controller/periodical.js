/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.periodical', {
    extend: 'Ext.app.Controller',

    views: [
        'periodical.index'
    ],

    stores: [
        'periodical'
    ],

    models: ['periodical'],

    init: function() 
    {
        this.control({

        });
    },
    index: function() 
    {
        ProcessView(this, 'periodical.index');
    },
    redirectToIndex: function()
    {
         window.location.hash = '';
    }
});