/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.transaction', {
    extend: 'nx.controller.base',
    views: [
        'transaction.index'
    ],
    init: function() 
    {
        this.control({
            'nx_transaction_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
                'nx_transaction_send_to_payment': function(button) {
                    var grid = button.up('grid');
                    var selection = grid.getSelectionModel().getSelection();  
                    if(selection.length)
                    {
                        var ids = [];
                        Ext.each(selection, function(row) {
                            ids.push(row.get('id'));
                        });
                        Ext.Msg.confirm('Подтверждение','Сохранить транзакции в nx онлайн платежи?', function(btn){
                            if(btn=='yes')
                            {
                                this.sendToPayment(ids, grid);     
                            }
                        }, this);    
                    }
                    else {
                        Ext.Msg.alert('Ошибка', 'Не выбраны позиции');
                    }
                }
            }
        });
    },
    sendToPayment: function(ids)
    {
        Ext.Ajax.request({
            url     : '/transaction/send-to-payment',
            method  : 'POST',
            scope   : this,
            params  : { 
                'ids[]' : ids//.join(',')
            },
            success : function(response) {
                //view.setLoading(false);
                var response = Ext.JSON.decode(response.responseText);
                this.showResult(response.result);
            }
        }, this);
    },
    showResult: function(data){
        new Ext.window.Window({
            title     : 'Сохранение транзакций в nx онлайн платежи',
            iconCls   : 'payment-ico',
            layout    : 'fit',
            border    : false,
            resizable : true,
            autoShow  : true, 
            items     : [
                {
                    xtype : 'grid',
                    width : 500,
                    height: 500,
                    autoShow: true,
                    store: new Ext.data.JsonStore({
                        proxy: {
                            type: 'memory',
                            reader: {
                                type: 'json',
                                root: 'items'
                            }
                        },
                        data         : {'items' : data},
                        pageSize     : 9999,
                        autoLoad     : true,
                        fields       : [
                            {name: 'id'},
                            {name: 'result'}
                        ]
                    }),
                    columns    : [
                        {xtype: 'rownumberer', width: 50},
                        {text: 'id', dataIndex: 'id', width: 70},
                        {text: 'комментарий', dataIndex: 'result', flex: 1, tdCls: 'nx-multiline'},
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    },
                    allowDeselect: true,
                }
            ], 
            buttons : [
                {
                    text: 'Закрыть',
                    handler: function() {
                        this.up('window').close();
                    }
                }
            ]
        });
    },
});