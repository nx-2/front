/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.controller.subscription', {
    //extend: 'Ext.app.Controller',
    extend: 'nx.controller.base',
    views: [
        'subscription.index',
        'subscription.edit',
        'subscription.add',
        'subscription.pdflog',
        'subscription.pdflogbyissue',
        //'subscription.remind',
        'subscription.xpress',
        'subscription.ship',
        'subscription.shiptable',
        'subscription.items',
        'subscription.shiptomap',
        'subscription.shipcounts',
        'subscription.action',
        'subscription.add_csv',
        'comment.list'
    ],

    stores: [
        'subscription',
        'subscription_item',
        'file_storage',
        'action',
        'tag'
    ],

    models: ['subscription'],

    init: function() 
    {
        this.control({
            'nx_subscription_edit button[action=save]': {
                click: function(button) 
                {
                    var view             = button.up('window');
                    var map_addr_changed = 0;
                    var map_addr_empty   = 0;
                    var check_fields     = ['map_apartment', 'map_house', 'map_street', 'map_city', 'map_zipcode'];
                    var empty_fields     = [];
                    Ext.each(view.down('#addressRightForm').items.items, function(item){
                        if(item.isDirty()) {
                            map_addr_changed = 1;
                        }
                        if(check_fields.indexOf(item.name)!=-1 && !item.value)
                        {
                            empty_fields[empty_fields.length] = item.fieldLabel;
                        }
                    });
                    if(map_addr_changed && empty_fields.length)
                    {
                        Ext.Msg.confirm('Подтверждение', 'Адрес по КЛАДР был изменен, но поля (' + empty_fields.join(', ') + ') пустые, вы уверены?', function(btn)
                        {
                            if (btn == 'yes')
                            {
                                this.update_db_record({view: view});
                            }
                        }, this);
                    }
                    else
                    {
                        this.update_db_record({view: view});
                    }
                }
            },
            'nx_subscription_add button[action=add]': {
                click: function(button) {
                    this.add_db_record({view: button.up('window')});
                }
            },
            'nx_subscription_edit button[action=extend]': {
                click: function(button) {
                    this.add_db_record({view: button.up('window')});
                }
            },
            'nx_subscription_edit': {
                'nx_get_ship_doc': function(view, ids, signed) {
                    this.getShipDoc(view, ids, signed, false);
                },
                'nx_order_form_refresh_ui': function(view, data) {
                    this.refresh_form_ui(view, data);
                },
                'nx_get_items_ship_doc': function(view, ids, signed) {
                    this.getShipDoc(view, ids, signed, true);
                }
            },
            'nx_subscription_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
                'nx_subscription_edit' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id')});
                },
                'nx_subscription_add' : function(grid)
                {
                    this.open_add_form({grid: grid});
                },
                'nx_subscription_switch_field' : function(grid, rec, field)
                {
                    grid.el.mask();
                    this.update_field(rec.get('id'), field, rec.get(field)=='1' ? 0 : 1);
                },
                'nx_subscription_extend' : function(grid, rec)
                {
                    this.open_edit_form({id: rec.get('id'), extend: 1});
                },
                'nx_subscription_company_send_ws1c': function(button, rec)
                {
                    var ids = [rec.get('company_id')];
                    this.getController('company').sendTo1C(ids, null, button);
                },
                'nx_subscription_register' : function(rec)
                {
                    Ext.Ajax.request({
                        url    : '/subscription/register',
                        method : 'POST',
                        scope  : this,
                        params : { 
                            id : rec.get('id'),
                        },
                        success : function(response) {
                            //var response = Ext.JSON.decode(response.responseText);
                        }
                    }, this);
                }
            },
            'nx_pdflog_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                }
            },
            'nx_pdflogbyissue_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
                'nx_pdf_log_details': function(issue_id, send) {
                    this.showPdfLogDetails(issue_id, send);
                }
            },
            'nx_subscription_xpress' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                }
            },
            'nx_subscription_ship_list' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
                'nx_get_ship_doc': function(view, ids) {
                    this.getShipDoc(view, ids);
                }
            },
            'nx_subscription_items' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
                'nx_open_order_edit_form': function(grid, order_id) {
                    this.open_edit_form({id: order_id});
                },
                'nx_subscription_re_prolong': function(button) {
                    var grid = button.up('grid');
                    var selection = grid.getSelectionModel().getSelection();  
                    if(!selection.length) {
                        selection = grid.store.data.items;
                    }
                    if(selection.length)
                    {
                        var ids = [];
                        Ext.each(selection, function(row) {
                            ids.push(row.get('id'));
                        });
                        this.updateProlong(ids, grid);         
                    }
                    else {
                        Ext.Msg.alert('Ошибка', 'Не выбраны позиции');
                    }
                }
            },
            'nx_subscription_shipcounts' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                },
                'nx_pdf_log_details' : function(issue_id) {
                    this.showPdfLogDetails(issue_id, 0);
                }
            },
            'nx_subscription_action' : {
                headerfilterchange : function(grid, filters, last_filters, active) {
                    grid.getStore().loadPage(1);
                }
            },
            'nx_subscription_add_csv button[action=add]': {
                click: function(button) {
                    var view = button.up('window')
                    var form = view.down('form');
                    form     = form.getForm();
                    if (form.isValid()) 
                    {
                        form.submit(
                        {
                            url     : '/subscription/get-data-from-csv',
                            waitMsg : 'Отправка данных',
                            scope   : this,
                            timeout : 600000,
                            success : function(form, action) 
                            {
                                if (!action.result.systemMessage && !action.result.redirect && !action.result.confirmMessage) 
                                {
                                    showDataAsTree(action.result.data, {title: 'Проверьте данные', expand: 'first', buttons: [
                                        {
                                            text   : 'Импортировать',
                                            handler: function(button) {
                                                Ext.Msg.confirm('Подтверждение','Импортировать ' + Ext.Object.getSize(action.result.data) + ' подписок?', function(btn){
                                                    if(btn=='yes')
                                                    {
                                                        button.setLoading(true);
                                                        Ext.Ajax.request({
                                                            url    : '/subscription/add-by-data',
                                                            method : 'POST',
                                                            scope  : this,
                                                            timeout: 600000,
                                                            params : { 
                                                                data : Ext.JSON.encode(action.result.data),
                                                                //step : 2
                                                            },
                                                            success : function(response) {
                                                                button.setLoading(false);
                                                                var response = Ext.JSON.decode(response.responseText);
                                                                button.up('window').close();
                                                                view.grid.store.reload();
                                                            }
                                                        }, this);
                                                    }
                                                },this);
                                            }
                                        }
                                    ]});
                                }
                            }
                        });
                    }



                }
            },
        });
    },
    pdflogAction : function(params)
    {
        ProcessView(this, this.id + '.pdflog', params);
    },
    pdflogbyissueAction : function(params)
    {
        ProcessView(this, this.id + '.pdflogbyissue', params);
    },
    xpressAction: function(params)
    {
        ProcessView(this, this.id + '.xpress', params);
    },
    shipAction: function(params)
    {
        ProcessView(this, this.id + '.ship', params);
    },
    shiptableAction: function(params)
    {
        ProcessView(this, this.id + '.shiptable', params);
    },
    itemsAction: function(params)
    {
        ProcessView(this, this.id + '.items', params);
    },
    shiptomapAction: function(params)
    {
        ProcessView(this, this.id + '.shiptomap', params);
    },
    shipcountsAction: function(params)
    {
        ProcessView(this, this.id + '.shipcounts', params);
    },
    actionAction: function(params)
    {
        ProcessView(this, this.id + '.action', params);
    },
    getShipDoc: function(view, ids, signed, is_item) {
        view.setLoading('Загрузка');
        Ext.Ajax.request({
            url     : is_item ? '/order-item/get-ship-doc' : '/ship/get-ship-doc',
            method  : 'POST',
            scope   : this,
            params  : { 
                ids   : ids.join(','),
                signed: signed || false
            },
            success : function(response) {
                view.setLoading(false);
                var response = Ext.JSON.decode(response.responseText);
                this.showGetDocResult(view, ids, signed, response.result, is_item);//, signed);
            }
        }, this);
    },
    showGetDocResult: function(view, ids, signed, data, is_item){//, signed) {
        var file_ids = [];
        Ext.each(data, function(row) {
            //console.log(row);
            for(var i=0;i<row.files.length;i++) {
                file_ids.push(row.files[i]);
            }
        });
        //console.log(file_ids);
        new Ext.window.Window({
            title     : 'Получение документов из 1С',
            iconCls   : 'payment-ico',
            layout    : 'fit',
            border    : false,
            resizable : true,
            autoShow  : true, 
            items     : [
                {
                    xtype : 'grid',
                    width : 500,
                    height: 500,
                    autoShow: true,
                    store: new Ext.data.JsonStore({
                        proxy: {
                            type: 'memory',
                            reader: {
                                type: 'json',
                                root: 'items'
                            }
                        },
                        data         : {'items' : data},
                        pageSize     : 9999,
                        autoLoad     : true,
                        fields       : [
                            {name: 'id'},
                            {name: 'result'}
                        ]
                    }),
                    columns    : [
                        {xtype: 'rownumberer', width: 50},
                        {text: 'id', dataIndex: 'id', width: 70},
                        {text: 'комментарий', dataIndex: 'result', flex: 1, tdCls: 'nx-multiline'},
                        {
                            xtype        : 'actioncolumn',
                            width        : 30,
                            menuDisabled : true,
                            hidden       : is_item,
                            items        : [
                                {
                                    icon: '/images/extjs/icons/watch.png',
                                    tooltip: 'Показать тех. данные',
                                    scope: this,
                                    handler: function(grid, rowIndex, colIndex, item, e, rec) 
                                    {
                                        var id = rec.get('id');
                                        Ext.Ajax.request({
                                            url     : '/ship/get',
                                            method  : 'POST',
                                            params  : {
                                                id : id
                                            },
                                            scope   : this,
                                            success : function(response) {
                                                var response = Ext.JSON.decode(response.responseText);
                                                showDataAsTree(response.data);
                                            }
                                        });
                                    }
                                },
                            ]
                        }
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    },
                    allowDeselect: true,
                    selModel: {
                        mode: 'MULTI'
                    },
                }
            ], 
            buttons : [
                {
                    text   : 'Обновить',
                    scope  : this,
                    handler: function(btn) {
                        btn.up('window').close();
                        this.getShipDoc(view, ids, signed, is_item);
                    }
                },
                {
                    xtype   : 'splitbutton',
                    text    : 'Запросить из 1С',
                    hidden  : !nx.app.isperm('ship_manager'),//ids.length ? false : true,
                    scope   : this,
                    handler : function(btn) {
                        var win  = btn.up('window');
                        var grid = win.down('grid');
                        var selection = grid.getSelectionModel().getSelection();
                        if(!selection.length) {
                            selection = grid.store.data.items;
                        }
                        if(selection.length)
                        {
                            Ext.Msg.confirm('Подтверждение','Запросить документы для ' + selection.length + ' отгрузок?', function(key){
                                if(key=='yes')
                                {
                                    var select_ids = [];
                                    Ext.each(selection, function(row) {
                                        select_ids.push(row.get('id'));
                                    });
                                    btn.setLoading(true);
                                    Ext.Ajax.request({
                                        url     : is_item ? '/order-item/update-ship-doc' : '/ship/update-ship-doc',
                                        method  : 'POST',
                                        scope   : this,
                                        params  : { 
                                            ids   : select_ids.join(','),
                                            signed: signed || false
                                        },
                                        success : function(response) {
                                            btn.setLoading(false);
                                            var response = Ext.JSON.decode(response.responseText);
                                            win.close();
                                            this.getShipDoc(view, ids, signed, is_item);
                                        }
                                    }, this);
                                }
                            }, this);
                            
                        }
                    },
                    menu: {
                        showSeparator : false,
                        cls           : 'no-icon-menu',
                        items         : [
                            {
                                text    : 'Проверить связь с 1С',
                                scope   : this,
                                handler : function(btn) {
                                    this.check1CConnection(btn);
                                }
                            }
                        ]
                    }
                },
                {
                    text    : 'Скачать',
                    hidden  : file_ids.length ? false : true,
                    scope   : this,
                    handler : function() {
                        //this.setLoading(true);
                        Ext.Ajax.request({
                            url     : '/file-storage/create-zip',
                            method  : 'POST',
                            scope   : this,
                            params  : { 
                                ids : file_ids.join(',')
                            },
                            success : function(response) {
                                //this.setLoading(false);
                                var response = Ext.JSON.decode(response.responseText);
                                if(response.result) {
                                    window.location = response.result.url;
                                }
                                else {
                                    Ext.Msg.alert('Сообщение', 'Не удалось создать архив!');
                                }
                            }
                        }, this);
                    }
                },
                {
                    text: 'Закрыть',
                    handler: function() {
                        this.up('window').close();
                    }
                }
            ]
        });
    },
    showPdfLogDetails: function(issue_id, send) {
        ExtWindow = new Ext.window.Window({
            title: send ? 'Отправлено' : 'Всего',
            autoShow: true,
            width: 400,
            maxHeight:500,
            layout: 'fit',
            y:100,
            //modal: true,
            items: [
                {
                    xtype: 'grid',
                    store: new Ext.data.JsonStore({
                        proxy: {
                            type: 'ajax',
                            url : '/subscription/pdflog',
                            reader: {
                                type: 'json',
                                root: 'items',
                                successProperty: 'success',
                                totalProperty: 'total'
                            },
                            extraParams: {
                                filter: '[{"property":"issue_id","value":"'+issue_id+'"}' + (send ? ', {"property":"send","value":"'+send+'"}' : '') + ']'
                            }
                        },
                        pageSize: 999,
                        remoteFilter: true,
                        autoLoad : true,
                        fields        : [
                            {name: 'email'},
                            {name: 'send'},
                            {name: send ? 'sendtime' : 'created'},
                            {name: 'subscription_id'}
                        ]
                    }),
                    columns    : [
                        {xtype: 'rownumberer', width: 40},
                        {header: 'Email', dataIndex: 'email', flex: 1},
                        {header: 'Статус', dataIndex: 'send', flex: 1, renderer: function(value) { if(value=='1') { return 'отправлено'; } else { return value; } }},
                        {header: 'Дата', dataIndex: send ? 'sendtime' : 'created', flex: 1},
                        {header: 'id подписки', dataIndex: 'subscription_id', flex: 1}
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    } 
                }
            ]
        });
    },
    updateProlong: function(ids, grid, el) 
    {
        if(el) {
            el.setLoading(true);
        }
        Ext.Ajax.request({
            url     : '/order-item/update-prolong-state',
            method  : 'POST',
            scope   : this,
            params  : {
                ids : ids.join(',')
            },
            success : function(response) {
                if(el) {
                    el.setLoading(false);
                }
                var response = Ext.JSON.decode(response.responseText);
                if(grid) {
                    grid.getStore().reload();
                }
            }
        });
    },
    check1CConnection: function(el)
    {
        el.setLoading(true);
        Ext.Ajax.request({
            url     : '/index/check1C',
            method  : 'GET',
            scope   : this,
            success : function(response) {
                el.setLoading(false);
                var response = Ext.JSON.decode(response.responseText);
                var msg = response.check ? 'Успешно' : 'Ошибка';
                var icon = response.check ? Ext.Msg.INFO : Ext.Msg.ERROR;
                Ext.Msg.show({title: 'Проверка соединения с 1С', msg: msg, icon: icon, buttons: Ext.Msg.OK});
            }
        });
    },
    refresh_form_ui: function(view, data) {
        if(data.subscriber_type_id == 1 || data.company_id == 0) {
            // view.setTitle(view.init_title + ' (физ. лицо)');
            view.setTitle(view.init_title);
            view.getEl().setStyle({'backgroundColor': '#CAD7E6'});
            view.getHeader().getEl().setStyle({'backgroundColor': '#CAD7E6'});
        }
        if(data.subscriber_type_id == 2 || data.company_id != 0) {
            // view.setTitle(view.init_title + ' (юр. лицо)');
            view.setTitle(view.init_title);
            view.getEl().setStyle({'backgroundColor': '#F5DEB3'});
            view.getHeader().getEl().setStyle({'backgroundColor': '#F5DEB3'});
        }
    }
});