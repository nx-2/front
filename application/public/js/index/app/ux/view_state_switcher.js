/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.ux.view_state_switcher', {
    extend      : 'Ext.button.Button',
    xtype       : 'view_state_switcher',
    filter_menu : true,
    nx_data     : {},
    initComponent : function(arguments) {
        Ext.apply(this,{
            text    : '',
            iconCls : 'filter-ico',
            scope   : this,
            //hidden  : nx.config.perm.is_admin ? false : true, 
        });
        this.callParent(arguments);
    },
    listeners: {
        'render' : function() {
            this.reloadMenu();
        }
    },
    onFilterChange: function(store, filters, eopts) {
        this.resetTitle(eopts.view);
    },
    onColumnsChanged: function(ct, eopts) {
        this.resetTitle(eopts.view);
    },
    resetTitle: function(view) {
        if(view.nx_data && view.nx_data.old_title) {
            view.setTitle(view.nx_data.old_title);
            delete view.nx_data.old_title;
        }
    },
    reloadMenu: function() {
        var grid  = this.up('grid');
        if(!grid.itemId) {
            return;
        }
        if(this.menu) {
            this.menu.destroy();
        }
        this.menu = Ext.create('Ext.menu.Menu',{plain: true});
        this.menu.add({
            text: 'Запомнить',
            plain: true,
            scope: this,
            padding: 5,
            handler: function(btn) {
                this.saveState(grid);
            }
        });
        if(grid.xfilters && this.filter_menu) {
            this.menu.add('-',{
                text    : 'Фильтры',
                plain   : true,
                scope   : this,
                padding : 5,
                handler : function(btn) {
                    var cmpid = grid.id + '_filters';
                    if(!Ext.getCmp(cmpid)) {
                        ExtWindow = new Ext.window.Window({
                            id        : cmpid,
                            title     : 'Фильтры',
                            autoShow  : true,
                            width     : 700,
                            maxHeight : 500,
                            layout    : 'fit',
                            y         : 100,
                            renderTo  : grid.id,
                            constrain : true,
                            items: [
                                {
                                    xtype          : 'grid_filters',
                                    grid           : grid,
                                    state_switcher : this
                                }
                            ],
                            listeners: {
                                'render': function() {
                                    grid.on('destroy', function(grid, eopts){
                                        eopts.filters_window.destroy();
                                    }, this, {single: true, filters_window: this});
                                }
                            }
                        });
                    }
                }
            });
        }
        this.menu.add('-',{
            xtype   : 'buttongroup',
            columns : this.filter_menu ? 4 : 3,
            plain   : true,
            itemId  : 'switcher_filters',
            border  : 0,
            frame   : false
            //scope   : this,
            //padding : 5
        });
        Ext.Ajax.request({
            url     : '/view-state/index',
            method  : 'GET',
            params  : { viewid: grid.itemId },
            scope   : this,
            success : function(response) {
                var response = Ext.JSON.decode(response.responseText);
                if(response.items) {
                    //this.menu.add('-');
                    for(var i = 0; i<response.items.length; i++)
                    {
                        var item = response.items[i];
                        this.menu.down('#switcher_filters').add({
                            text    : item.name, 
                            nx_data : item,
                            plain   : true, 
                            padding : 5,
                            scope   : this,
                            //url     : 'http://www.google.com/search',
                            handler: function(btn) 
                            {
                                //window.open(window.location.href + '/state/' + btn.nx_data.id ,'_blank');
                                if(btn.nx_data) {
                                    this.applyState(grid, btn.nx_data);
                                }
                            }
                        },{
                            width   : 16,
                            height  : 16,
                            nx_data : item,
                            tooltip : 'открыть в новом окне',
                            icon    : '/images/extjs/icons/extlink.png',
                            handler : function(btn) {
                                window.open(window.location.href.replace(/\/state\/\d+/gi,'') + '/state/' + btn.nx_data.id ,'_blank');
                            }
                        },{
                            width   : 16,
                            height  : 16,
                            nx_data : item,
                            tooltip : 'посмотреть',
                            icon    : '/images/extjs/icons/edit_10.png',
                            scope   : this,
                            hidden  : !this.filter_menu,
                            handler : function(button) {
                                var cmpid = grid.id + '_watch_filter';
                                if(Ext.getCmp(cmpid)) {
                                    Ext.getCmp(cmpid).destroy();
                                }
                                ExtWindow = new Ext.window.Window({
                                    id        : cmpid,
                                    title     : 'Фильтр ' + button.nx_data.name,
                                    autoShow  : true,
                                    width     : 700,
                                    maxHeight : 500,
                                    layout    : 'fit',
                                    y         : 100,
                                    items: [
                                        {
                                            xtype          : 'grid_filters',
                                            grid           : grid,
                                            state_switcher : this,
                                            view_state     : button.nx_data
                                        }
                                    ],
                                    listeners: {
                                        'render': function() {
                                            grid.on('destroy', function(grid, eopts){
                                                eopts.filters_window.destroy();
                                            }, this, {single: true, filters_window: this});
                                        }
                                    }
                                });
                            }
                        },{
                            width   : 16,
                            height  : 16,
                            nx_data : item,
                            tooltip : 'удалить',
                            icon    : '/images/extjs/icons/delete_10.png',
                            scope   : this,
                            handler : function(button) {
                                Ext.Msg.confirm('Подтверждение', 'Удалить?', function(btn){
                                    if(btn=='yes')
                                    {
                                        Ext.Ajax.request({
                                            url     : '/view-state/delete',
                                            method  : 'POST',
                                            params  : {
                                                id : button.nx_data.id
                                            },
                                            scope   : this,
                                            success : function(response) {
                                                this.reloadMenu();
                                            }
                                        });
                                    }
                                },this);
                            }
                        });
                    }
                }
            }
        });
    },
    getGridColumnsData: function(view) {
        //var grid  = this.up('grid');
        var headerCt = view.headerCt;
        var columns = headerCt.getGridColumns();
        var data = [];
        Ext.each(columns, function(col) {
            if(col.dataIndex) {
                var cur = {
                    dataIndex : col.dataIndex, 
                    hidden    : col.isHidden(), 
                    index     : headerCt.items.findIndex('dataIndex', col.dataIndex),//col.getIndex(),
                    width     : col.getWidth(),
                };
                data[data.length] = cur;
            }
        });
        return data;
    },
    applyColumnsCfg: function(view, cols) {
        var headerCt = view.headerCt;
        var columns = headerCt.getGridColumns();
        Ext.each(columns, function(column){
            Ext.each(cols, function(col){
                if(column.dataIndex == col.dataIndex) {
                    if(col.hidden) {
                        column.hide();
                    }
                    else {
                        column.show();
                    }
                    var idx = headerCt.items.findIndex('dataIndex', column.dataIndex);
                    if(col['index'] && col['index']!=idx) {
                        //headerCt.move(column.getIndex(),col['index']);
                        headerCt.move(idx,col['index']);
                    }
                    if(col['width']) {
                        column.setWidth(col['width']);
                    }
                }
            });
        });
        view.getView().refresh();
    },
    applyState: function(view, state) {
        var data = state.data;
        if(data.filter) {
            var filters = data.filter;
            if(view.resetHeaderFilters!=undefined) {
                view.resetHeaderFilters(false);
            }
            view.store.clearFilter(true);
            if(view.setHeaderFilters!=undefined) {
                view.setHeaderFilters(filters, false);
            }
            view.store.filter(filters);
        }
        if(data.columns) {
            this.applyColumnsCfg(view, data.columns);
        }
        if(data.sort) {
            view.store.sort(data.sort);
        }
        else {
            view.store.sorters.clear();
        }
        view.nx_data = view.nx_data || {};
        Ext.apply(view.nx_data, {old_title: view.title});
        view.setTitle(view.title + ' - ' + state.name);
        view.store.on('filterchange', this.onFilterChange, this, {view: view, single: true});
        view.on('columnschanged', this.onColumnsChanged, this, {view: view, single: true});
    },
    saveState: function(view) {
        var store   = view.getStore();
        var columns = this.getGridColumnsData(view);
        if(!view.itemId) {
            console.log('неизвестный itemId!');
            return false;
        }
        //if(!store.filters.getCount())
        //{
        //    Ext.Msg.alert('Ошибка', 'Не выбраны фильтры!');
        //    return false;
        //}
        Ext.Msg.prompt('Подтверждение','Введите название', function(btn, text){
            if(btn=='ok' && text)
            {
                var options = {
                    action  : 'read',
                    filters : store.filters.items,
                    sorters : store.sorters.items
                };
                var operation   = new Ext.data.Operation(options);
                var fakeRequest = store.getProxy().buildRequest(operation);
                var params      = fakeRequest.params;
                Ext.Ajax.request({
                    url     : '/view-state/add',
                    method  : 'POST',
                    scope   : this,
                    params  : { 
                        name    : text,
                        filter  : params.filter,//Ext.encode(grid.filters.getFilterData());
                        sort    : params.sort,
                        columns : Ext.encode(columns),
                        viewid  : view.itemId
                    },
                    success : function(response) {
                        var response = Ext.JSON.decode(response.responseText);
                        if(response.result) {
                            Ext.Msg.alert('Сообщение', 'Состояние успешно сохранено');
                            this.reloadMenu();
                        }
                        else {
                            Ext.Msg.alert('Сообщение', 'Состояние не удалось сохранить, либо оно уже было сохранено ранее');
                        }
                    }
                });
            }
        }, this);
    },
    /*updateState: function(view, data) {
        var store   = view.getStore();
        var columns = this.getGridColumnsData(view);
        if(!view.itemId) {
            console.log('неизвестный itemId!');
            return false;
        }
        if(!data.filters)
        {
            Ext.Msg.alert('Ошибка', 'Не выбраны фильтры!');
            return false;
        }
        Ext.Ajax.request({
            url     : '/view-state/update',
            method  : 'POST',
            scope   : this,
            params  : { 
                name    : text,
                filter  : Ext.encode(data.filters),
                columns : Ext.encode(columns),
                viewid  : view.itemId
            },
            success : function(response) {
                var response = Ext.JSON.decode(response.responseText);
                if(response.result) {
                    Ext.Msg.alert('Сообщение', 'Состояние успешно сохранено');
                    this.reloadMenu();
                }
                else {
                    Ext.Msg.alert('Сообщение', 'Состояние не удалось сохранить');
                }
            }
        });
    }*/
});