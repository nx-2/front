/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.ux.LinkButton', {
    extend: 'Ext.Component',
    alias: 'widget.linkbutton',
    
    baseCls: Ext.baseCSSPrefix + 'linkbutton',
    autoEl: {
        tag: 'a',
        href: '#'
    },
    renderTpl: '{text}',
    
    initComponent: function() {
        this.renderData = {
            text: this.text
        };
        
        this.callParent(arguments);
    },
    
    afterRender: function() {
        this.mon(this.getEl(), 'click', this.handler, this);
    },
    
    handler: Ext.emptyFn
});