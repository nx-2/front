/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.ux.JsonCombo', {
    extend         : 'Ext.form.ComboBox',
    xtype          : 'nx.ux.JsonCombo',
    emptyText      : 'выбрать',
    minListWidth   : 180,
    triggerAction  : 'all',
    //fixValue       : false,
    selectOnFocus  : true,
    displayField   : 'name',
    valueField     : 'id',
    minChars       : 2, 
    url            : '',
    baseParams     : {},
    storeAutoload  : true,
    storeSorters   : [],
    initComponent  : function() {
        Ext.apply(this, {
            store : new Ext.data.JsonStore({
                sorters: this.storeSorters,
                proxy: {
                    type: 'ajax',
                    url : this.url,
                    reader: {
                        type: 'json',
                        root: 'objects',
                        successProperty: 'success',
                        totalProperty: 'total'
                    },
                    extraParams: this.baseParams
                },
                autoLoad   : this.storeAutoload,
                root       : 'objects',
                fields     : [
                    {name : 'id',   type : 'int'},
                    {name : 'name', type : 'string'}
                ]/*,
                listeners: {
                    scope : this,
                    load  : function(store, records, options){
                        if (this.fixValue) {
                            this.setValue(this.value)
                        } 
                    }
                }*/
            })
        });
        nx.ux.JsonCombo.superclass.initComponent.call(this, arguments);
    }/*,
    onRender : function() {
        nx.ux.JsonComboAbstract.superclass.onRender.apply(this, arguments);
    }*/
});