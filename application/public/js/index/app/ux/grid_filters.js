/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

/*
nx uses filters as gridheaderfilters plugin and as grid.store.filters directly if certain columns absent
gridheaderfilters uses grid.store.filters but only for grid columns
*/
Ext.define('nx.ux.grid_filters', {
    extend    : 'Ext.panel.Panel',
    xtype     : 'grid_filters',
    overflowY : 'scroll',
    layout    : {type : 'hbox'},
    initComponent: function(arguments) {
        var grid = this.grid;
        grid.store.on('load', this.fill, this);
        var xfilters = grid.xfilters;
        var fstore = [];
        Ext.each(xfilters, function(filter) {
            if(!filter.onlyheader && filter.fieldLabel && filter.name) {
                fstore[fstore.length] = [filter.name, filter.fieldLabel];
            }
        });

        this.items = [
            {
                xtype: 'form',
                fieldDefaults: {
                    msgTarget: 'side',
                    labelWidth: 100,
                    width: 350
                },
                itemId: 'filter_form',
                //layout: 'form',
                border: 0,
                style: 'padding:5px;',   
            },{
                xtype:'tbspacer',
                flex:1
            },
            {
                xtype    : 'combo',
                style    : 'margin: 5px 25px 5px 0;',
                editable : false,
                emptyText: 'добавить фильтр',
                listeners: {
                    scope: this,
                    'select' : function(combo, recs) {
                        this.addFilterRowByName(recs[0].get('field1'));
                        combo.reset();
                    }
                },
                store: fstore
            }
        ];

        Ext.apply(this,{
            buttonAlign: 'left',
            buttons: [
                { 
                    text: 'Применить',
                    scope: this,
                    handler: function() {
                        this.applyFilters();
                    }
                },
                '->',
                { 
                    text: 'Запомнить',
                    hidden: this.view_state ? true : false,
                    scope: this,
                    handler: function() {
                        this.state_switcher.saveState(this.grid);
                    }
                }
            ]
        });

        if(this.view_state) {
            var filters = new Ext.util.MixedCollection();
            filters.addAll(this.view_state.data.filter);
            this.view_state_filters = filters;
        }
            
        this.callParent(arguments);

        this.fill();
    },
    fill: function() {
        var form = this.down('#filter_form');
        if(form) {
            this.down('#filter_form').removeAll(true);
            var grid = this.grid;
            var xfilters = grid.xfilters;
            if(this.view_state_filters) {
                var filters = this.view_state_filters;
            }
            else {
                var filters = grid.store.filters;
            }
            filters.each(function(filter) {
                this.addFilterRowByName(filter.property);
            },this);
            /*var grouped = [];
            var groups = {};
            Ext.each(xfilters, function(filter) {
                if(filter.onlyheader) {
                    return;
                }
                var filter = Ext.clone(filter);
                var index = filters.findIndex('property', filter.name);//attention! findIndex second arg is - a string that the property values should start with 
                if(index!=-1) {
                    filter.value = filters.getAt(index).value;
                }
                if(filter.group) {
                    if(groups[filter.group]) {
                        Ext.Array.push(grouped[groups[filter.group]], filter);
                    }
                    else {
                        groups[filter.group] = grouped.length;
                        grouped[grouped.length] = [filter];
                    }
                }
                else {
                    grouped[grouped.length] = filter;
                }
            },this);
            Ext.each(grouped, function(group){
                this.addFilterRow(group);  
            },this);*/
        }
    },
    addFilterRowByName: function(name) {
        var fields   = this.down('#filter_form').getForm().getFields();
        if(fields.findIndex('name', new RegExp('^' + name + '$')) != -1) {
            return;
        }
        var grid     = this.grid;
        var xfilters = grid.xfilters;
        if(this.view_state_filters) {
            var filters = this.view_state_filters;
        }
        else {
            var filters = grid.store.filters;
        }
        var filter = false;
        Ext.each(xfilters, function(xfilter) {
            if(xfilter.onlyheader) {
                return;//continue
            }
            if(xfilter.name == name) {
                filter = Ext.clone(xfilter);
                var index = filters.findIndex('property', new RegExp('^' + filter.name + '$'));
                if(index!=-1) {
                    filter.value = filters.getAt(index).value;
                }
                return false;//break
            }
        },this);
        if(filter.group) {
            var grouped = [];
            Ext.each(xfilters, function(xfilter) {
                if(xfilter.onlyheader) {
                    return;
                }
                if(xfilter.group == filter.group) {// && xfilter.name != filter.name) {
                    var gfilter = Ext.clone(xfilter);
                    var index = filters.findIndex('property', new RegExp('^' + gfilter.name + '$'));
                    if(index!=-1) {
                        gfilter.value = filters.getAt(index).value;
                    }
                    grouped[grouped.length] = gfilter;
                }
            },this);
            if(grouped.length>1) {
                filter = grouped;
            }
        }
        if(filter.ops) {
            var ops = [];
            Ext.each(filter.ops, function(op){
                ops[ops.length] = [op, op];
            });
            var combo = {
                xtype : 'combo',
                name  : filter.name + '_operator',
                store : ops,
                width : 50,
                value : ops[0][0]
            }
            var index = filters.findIndex('property', new RegExp('^' + filter.name + '$'));
            if(index!=-1 && filters.getAt(index).xoperator) {
                Ext.apply(combo, {value: filters.getAt(index).xoperator});
            }
            if(Ext.isArray(filter)) {
                filter = Ext.Array.push(filter, combo);
            }
            else {
                filter = [filter, combo];
            }
        }
        if(filter) {
            this.addFilterRow(filter);
        }
    },
    addFilterRow: function(filter) {
        var del = {
            xtype  : 'button', 
            icon   : '/images/extjs/icons/delete_10.png', 
            border : 0,
            style  : 'background: none;',
            handler: function(btn) {
                this.up().removeAll(true);
            }
        };
        if(Ext.isArray(filter)) {
            filter = Ext.Array.merge([del],filter);
        }
        else {
            filter = [del, filter];
            //this.down('#filter_form').add(filter);
        }
        this.down('#filter_form').add({
            border    : 0,
            layout    : {
                type : 'hbox'
            },
            defaults: {
                style: {
                    marginRight: '2px',
                    marginBottom: '2px'
                }
            },
            items: filter
        });
    },
    applyFilters: function() {
        var grid = this.grid;
        if(grid.resetHeaderFilters!=undefined) {
            grid.resetHeaderFilters(false);
        }
        grid.store.clearFilter(true);
        /*var fields = this.down('#filter_form').getForm().getValues();
        for(var name in fields) {
            //if(fields[name] && fields[name]!='') {
                grid.setHeaderFilter(name, fields[name], false);
                grid.store.filters.add(name, new Ext.util.Filter({
                    property: name,
                    value   : fields[name],
                    operator: op
                }));
            //}
        }*/
        var fields = this.down('#filter_form').getForm().getFields();
        fields.each(function(field) {
            var value = field.getValue();
            if(field.xtype=='datefield') {
                value = field.getRawValue();
            }
            if(field.allowSubmitBlank === false && (!value || value=='')) {
                return;
            }
            var name  = field.getName();
            if(name.indexOf('_operator') != -1) {
                return;//continue
            }
            var op = '';
            var index = fields.findIndex('name', name + '_operator');
            if(index != -1) {
                op = fields.getAt(index).getValue();
            }
            var util = {property: name, value: value};
            if(op!='' && op!='=') {
                Ext.apply(util, {xoperator: op});
            }
            else if(grid.setHeaderFilter!=undefined) {
                grid.setHeaderFilter(name, value, false);
            }
            grid.store.filters.add(new Ext.util.Filter(util));
        });
        grid.store.filter();
    }
});