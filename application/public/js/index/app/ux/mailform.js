/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.ux.mailform', {
    extend          : 'Ext.window.Window',
    alias           : 'widget.nx_ux_mailform',
    title           : 'Отправить письмо',
    autoShow        : true,
    width           : 800,
    entity_name     : '',
    entity_id       : 0, //id передается по урл get_message_url
    email           : '',//email отправки письма
    email_cc        : '',//email копии
    log_message     : '',//сообщение для лога
    get_message_url : '',//урл получения темы и текста письма
    templates       : [],//шаблоны {id, name, url, subject}
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        border  : 0,
                        padding : 5,
                        items   : [
                            {
                                xtype      : 'textfield',
                                name       : 'subject',
                                fieldLabel : 'Тема',
                                labelWidth : 70,
                                inputWidth : 700,
                                value      : this.subject || ''
                            }
                        ]
                    },
                    {
                        xtype : 'htmleditor',
                        width : '100%',
                        height: 500,
                        //grow  : true,
                        //growMax : 400,
                        name  : 'message',
                        value : this.message || ''
                    }
                ]
            }
        ];

        var items = [];
        for(var len = this.templates.length, i=0; i<len; i++) {
            var template = this.templates[i];
            item = {
                //xtype   : 'button', 
                text    : template['name'],
                scope   : this,
                nx_data : template,
                handler : function(btn) {
                    if(btn.nx_data['id']) {
                        this.loadMailFormByTemplateID(btn.nx_data['id']);
                    }
                    else if(btn.nx_data['url']) {
                        this.loadMailFormByUrl(btn.nx_data['url'], btn.nx_data['subject']);
                    }
                }
            }
            items[items.length] = item;
        }
        Ext.apply(this, { 
            dockedItems: [
                {
                    xtype : 'toolbar',
                    hidden: items.length == 1 ? true : false,
                    items : items/*[
                        {
                            text    : 'Шаблон напоминания',
                            scope   : this,
                            handler : function() {
                                this.loadMailFormByTemplateID(this.subscription.get('label') == 'lvrach_subscription' ? 2 : 1);
                            }
                        },
                        {
                            text    : 'Шаблон продления из личного кабинета',
                            scope   : this,
                            handler : function() {
                                this.loadMailFormByTemplateID(9);
                            }
                        },
                        {
                            text    : 'Шаблон продления',
                            scope   : this,
                            handler : function() {
                                this.loadMailFormByTemplateID(14);
                            }
                        },
                        {
                            text    : 'Квитанция/Счет',
                            scope   : this,
                            handler : function() {
                                this.loadMailFormByUrl(nx.config.services.paydoc.url + '?hash=' + this.subscription.get('hash'), 'Оформление подписки на сайте Издательства Открытые системы');
                            }
                        }
                    ]*/
                }
            ]
        });


        this.buttons = [
            {
                text: 'Отправить',
                scope: this,
                handler: function(){
                    this.down('form').getForm().submit({
                        url     : '/mail-log/send-mail-message',//'/subscription/send-mail-message',
                        waitMsg : 'Отправка данных',
                        scope   : this,
                        params  : {
                            entity_id   : this.entity_id,
                            entity_name : this.entity_name,
                            email       : this.email,
                            log_message : this.log_message,
                            email_cc    : this.email_cc
                        },
                        success : function(form, response) {
                            this.close();
                        }
                    });
                }
            },
            {
                text: 'Отправить себе',
                scope: this,
                handler: function(){
                    this.down('form').getForm().submit({
                        url     : '/mail-log/send-mail-message',// '/subscription/send-mail-message',
                        waitMsg : 'Отправка данных',
                        scope   : this,
                        params  : {
                            debug       : true,
                            //entity_id   : this.entity_id,
                            //entity_name : this.entity_name,
                            email       : this.email,
                            log_message : this.log_message
                        },
                        success : function(form, response) {
                            this.close();
                        }
                    });
                }
            },
            {
                text    : 'Закрыть',
                action  : 'close',
                scope   : this,
                handler : this.close
            }
        ];

        this.callParent(arguments);
    },
    onRender: function() 
    {
        this.callParent(arguments);
        //console.log(this.down('toolbar').items.getAt(0));
        //this.down('toolbar').items.getAt(0).fireEvent('click', this.down('toolbar').items.getAt(0));
        var btn = this.down('toolbar').items.getAt(0);
        if(btn) {
            btn.handler.call(this, btn);
        }
    },
    loadMailFormByTemplateID: function(template_id)
    {
        this.down('form').getForm().load(
        {
            url     : this.get_message_url,//'/subscription/get-mail-message',
            waitMsg : 'Загрузка',
            scope   : this,
            params  : 
            {
                entity_id   : this.entity_id,
                template_id : template_id
            },
            success : function() 
            {
            }
        });
    },
    loadMailFormByUrl: function(url, subject)
    {
        Ext.Ajax.request({
            url     : '/index/get-url-content',
            waitMsg : 'Загрузка',
            method  : 'POST',
            params  : {
                //subscription_id : this.subscription.get('id'),
                url             : url
            },
            scope   : this,
            success : function(response) {
                var response = Ext.JSON.decode(response.responseText);
                this.down('htmleditor[name="message"]').setValue(response.data);
                if(subject) {
                    this.down('textfield[name="subject"]').setValue(subject);
                }
            }
        });
    }
});