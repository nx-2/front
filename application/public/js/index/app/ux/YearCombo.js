/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.ux.YearCombo', {
    extend : 'Ext.form.field.ComboBox',
    xtype  : 'yearcombo',
    alias  : 'widget.yearcombo',
    //emptyText : 'Выберите год',
    editable  : false,
    start     : 2001,
    end       : 'now',
    years     : [],
    queryMode : 'local',
    initComponent  : function() 
    {
        this.value = this.getYear();
        this.createStorage();
        Ext.apply(this, {
            store : this.years.reverse()
        });
        this.superclass.initComponent.call(this);
    },
    createStorage : function() 
    {
        var year;
        this.years   = [];
        if (this.end == 'now') 
        {
            this.end = new Date().getFullYear() + 1;
        }
        for (year = this.start; year <= this.end; year++) 
        {
            value = [year,year];
            this.years.push(value);
        }
        //year = ['','-'];
        //this.years.push(year);
    },
    getYear: function() 
    {
        var now = new Date(),
        year = now.getFullYear();
        return year;
    }
});