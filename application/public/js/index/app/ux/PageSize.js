/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

/**
* Ext.ux.grid.PageSize
*/
Ext.define('nx.ux.PageSize', 
{
    extend      : 'Ext.form.field.ComboBox',
    alias       : 'plugin.pagesize',
    beforeText  : 'Показать',
    afterText   : 'строк/страницу',
    mode        : 'local',
    displayField: 'text',
    valueField  : 'value',
    allowBlank  : false,
    triggerAction: 'all',
    width       : 100,
    maskRe      : /[0-9]/,    

    init: function(paging) 
    {
        paging.on('afterrender', this.onInitView, this);
    },
    store: new Ext.data.SimpleStore(
    {
        fields: ['text', 'value'],
        data: [['25', 25], ['50', 50], ['100', 100], ['200', 200], ['500', 500], ['3000', 3000]]
    }),    
    onInitView: function(paging) 
    {
        this.setValue(paging.store.pageSize); 
        paging.add('-', this.beforeText, this, this.afterText);
        this.on('select', this.onPageSizeChanged, paging);
        this.on('specialkey', function(combo, e) {
            if(13 === e.getKey()) {
                this.onPageSizeChanged.call(paging, this);        
            }
        });
    },
    onPageSizeChanged: function(combo) 
    {
        this.store.pageSize = parseInt(combo.getValue(), 10);
        this.doRefresh();
    }
}); 