/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.ux.notification_handler', {
    extend : 'Ext.button.Button',
    xtype  : 'notification_handler',
    interval_min: 10,
    initComponent: function(arguments) {
        Ext.apply(this,{
            icon       :'images/extjs/icons/notification.png',
            href       : '/#notification/index',
            hrefTarget : '_self',
            tooltip    : 'системные сообщения'
            //hidden     : !nx.config.perm['is_developer']
        });

        var task = { 
            scope: this,
            run: function(){
                this.requestState();
            }, 
            interval: 1000 * 60 * this.interval_min
        }

        //if(nx.app.isperm('admin')) {
            Ext.TaskManager.start(task);
        //}

        this.callParent(arguments);
    },
    requestState: function() {
        Ext.Ajax.request({
            url     : '/notification/index',
            method  : 'GET',
            params  : {
                filter : '[{"property":"checked","value":"0"}]'
            },
            scope   : this,
            success : function(response) {
                var response = Ext.JSON.decode(response.responseText);
                this.setState(parseInt(response.total) ? true : false);
            }
        });
    },
    setState: function(has_new_msg) {
        if(has_new_msg) {
            this.setIcon('images/extjs/icons/notification_new.png');
            this.el.highlight("ffff9c", {
                attr       : "backgroundColor",
                endColor   : "ffaa00",
                easing     : 'ease',
                duration   : 2000,
                iterations : 30 * this.interval_min,
                //alternate  : true
            });
        }
        else {
            this.setIcon('images/extjs/icons/notification.png');
        }
    }
});