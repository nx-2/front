/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.ux.csv_to_db_editor', {
    extend     : 'Ext.panel.Panel',
    xtype      : 'csv_to_db_editor',
    count_rows : 0,
    initComponent: function() {
        this.items = [
            {
                xtype : 'button',
                text  : 'Добавить колонку',
                style : {
                    marginTop: '5px'
                },
                scope : this,
                handler: function() {
                    this.addNewRow();
                }
            },
            {
                itemId : 'fields_table',
                border : 0,
                items  : [
                    {
                        layout: 'hbox',
                        border: 0,
                        defaults:{
                            border:0
                        },
                        items: [
                            {
                                html  : '',
                                width : 25
                            },
                            {
                                html  : 'номер колонки в csv',
                                width : 150,
                            },
                            {
                                html  : 'название в базе',
                                width : 150,
                            },
                        ]
                    }
                ]
            }
        ];
        this.callParent(arguments);
        this.addNewRow();
    },
    addNewRow: function() 
    {
        this.down('#fields_table').add(
            {
                itemId    : 'row' + this.count_rows,
                cls       : 'nx_row',
                row_index : this.count_rows,
                border    : 0,
                layout    : {
                    type : 'hbox'
                },
                defaults: {
                    style: {
                        marginRight  : '2px',
                        marginBottom : '2px'
                    }
                },
                items: [
                    {
                        xtype     : 'button',
                        itemId    : 'magazines_' + this.count_rows + '_delete_row',
                        icon      : '/images/extjs/icons/delete.png',
                        tooltip   : 'Удалить',
                        row_index : this.count_rows,
                        scope     : this,
                        handler   : function(button, event) {
                            this.down('#fields_table').remove('row' + button.row_index);
                            this.count_rows--;
                        }
                    },
                    {
                        xtype      : 'numberfield',
                        name       : 'csv_map[' + this.count_rows + '][column_num]',
                        //value    : 0,
                        width      : 150,
                        allowBlank : false,
                        minValue   : 1
                        //vtype : 'Numeric'
                    },
                    {
                        xtype     : 'combobox',
                        store     : this.fields,
                        //emptyText : 'Колонка в базе',
                        editable  : false,
                        queryMode : 'local',
                        name      : 'csv_map[' + this.count_rows + '][column_db]',
                        width     : 150,
                        allowBlank : false,
                    },
                ]
            }
        );
        this.count_rows++;
    }
});