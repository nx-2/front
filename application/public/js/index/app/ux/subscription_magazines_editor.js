/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.ux.subscription_magazines_editor', {
    extend : 'Ext.panel.Panel',
    xtype  : 'subscription_magazines_editor',
    count_magazines : 0,
    initComponent: function() {
        this.items = [
            {
                xtype : 'textfield',
                name  : 'is_change_magazines',
                value : '0',
                hidden: true
            },
            {
                itemId : 'magazines_table',
                border : 0,
                items  : [
                    {
                        layout: 'hbox',
                        border: 0,
                        defaults:{
                            border:0
                        },
                        items: [
                            {
                                html: '',
                                width: 25
                            },
                            {
                                html: 'издание',
                                width : 100,
                            },
                            {
                                html: 'тип доставки',
                                width : 155,
                            },
                            {
                                html: 'дата начала',
                                width: 90
                            },
                            {
                                html: 'период',
                                width: 75
                            },
                            {
                                html   : 'год',
                                width  : 60
                            },
                            {
                                html   : 'нач.выпуск',
                                width  : 75
                            },
                            {
                                html   : 'номеров',
                                width  : 70
                            },
                            {
                                html: '',
                                width: 25
                            },
                            {
                                html  : 'кол-во',
                                width : 50
                            },
                            {
                                html   : 'цена',
                                width  : 70
                            },
                            {
                                html   : 'скидка%',
                                width  : 50
                            },
                            {
                                html: '',
                                width: 25
                            },
                        ]
                    }
                ]
            },
            {
                xtype : 'combobox',
                store : new Ext.data.ArrayStore({
                    fields : ['id', 'name'],
                    data   : subscribleMagazinesStore
                }),
                displayField   : 'name',
                valueField     : 'id',
                emptyText      : 'Выберите издание',
                editable       : false,
                queryMode      : 'local',
                submitValue    : false,
                forceSelection : true,
                width          :300,
                style          : {
                    marginTop: '5px'
                },
                listeners:{
                    scope: this,
                    'select': function(combo, record){
                        this.addNewMagazineRow({'magazine_id' : combo.getValue()}, 'add');
                        combo.setValue('-');
                    }
                }
            }
        ];
        this.callParent(arguments);
    },
    addNewMagazineRow: function(data, action) 
    {
        if(!data.magazine_id) {
            return false;
        }
        if(action!='fill')
        {
            this.down('textfield[name="is_change_magazines"]').setValue('1');
        }
        this.down('#magazines_table').add(
            {
                itemId    : 'magazines_table_row' + this.count_magazines,
                cls       : 'nx_magazines_table_row',
                row_index : this.count_magazines,
                border    : 0,
                layout    : {
                    type : 'hbox'
                },
                defaults: {
                    style: {
                        marginRight: '2px',
                        marginBottom: '2px'
                    }
                },
                listeners: {
                    scope: this,
                    'added' : function(row) {
                        if(!data.id) {
                            this.calcMainData(row.row_index, action);
                        }
                    }
                },
                items: [
                    {
                        xtype     : 'button',
                        itemId    : 'magazines_' + this.count_magazines + '_delete_row',
                        icon      : '/images/extjs/icons/delete.png',
                        tooltip   : 'Удалить',
                        row_index : this.count_magazines,
                        scope     : this,
                        handler   : function(button, event) {
                            this.down('#magazines_table').remove('magazines_table_row' + button.row_index);
                            this.count_magazines--;
                            this.down('textfield[name="is_change_magazines"]').setValue('1');
                        }
                    },
                    {
                        xtype : 'textfield',
                        itemId: 'magazines_' + this.count_magazines + '_id',
                        name  : 'magazines[' + this.count_magazines + '][id]',
                        value : data.id ? data.id : 0,
                        hidden: true
                    },
                    {
                        xtype     : 'combobox',
                        id        : 'magazines_' + this.count_magazines + '_periodical_id' + this.id,
                        store     : magazines_store,
                        emptyText : 'Выберите издание',
                        editable  : false,
                        queryMode : 'local',
                        name      : 'magazines[' + this.count_magazines + '][periodical_id]',
                        value     : data.magazine_id,
                        width     : 100,
                        readOnly  : true,
                        listeners : {
                            scope: this,
                            'change' : function(field) {
                                this.down('textfield[name="is_change_magazines"]').setValue('1');
                                this.calcMainData(field.up('[cls="nx_magazines_table_row"]').row_index);
                            }
                        }
                    },
                    {
                        xtype     : 'combobox',
                        id        : 'magazines_' + this.count_magazines + '_delivery_type_id' + this.id,
                        store     : delivery_types_store2,
                        emptyText : 'Выберите способ доставки',
                        editable  : false,
                        queryMode : 'local',
                        name      : 'magazines[' + this.count_magazines + '][delivery_type_id]',
                        value     : data.delivery_type_id ? data.delivery_type_id : "1",
                        width     : 150,
                        tpl       : new Ext.XTemplate(
                            '<tpl for="."><tpl if="this.isAllowedDelivery(field1)"><div class="x-boundlist-item">{field2}</div><tpl else><div class="x-boundlist-item x-hidden">{field2}</div></tpl></tpl>',
                            {
                                isAllowedDelivery: function(value) 
                                {
                                    var allowed  = [5,3,2,1,9];
                                    var is_allow = 0;
                                    for (var i = allowed.length - 1; i >= 0; i--) {
                                        if(value == allowed[i]) {
                                            is_allow = 1;
                                        }
                                    };
                                    return is_allow;
                                }
                            }
                        ),
                        listeners : {
                            scope: this,
                            'change' : function(field) {
                                this.down('textfield[name="is_change_magazines"]').setValue('1');
                                this.calcMainData(field.up('[cls="nx_magazines_table_row"]').row_index, action);
                            }
                        }
                    },
                    {
                        xtype      : 'datefield',
                        id         : 'magazines_' + this.count_magazines + '_date_start' + this.id,
                        name       : 'magazines[' + this.count_magazines + '][date_start]',
                        width      : 90,
                        value      : data.date_start ? data.date_start : Ext.Date.format(new Date(), 'Y-m-01'),
                        editable   : false,
                        format     : 'Y-m-d',
                        //allowBlank : false,
                        listeners  : {
                            scope: this,
                            'change' : function(field) {
                                this.down('textfield[name="is_change_magazines"]').setValue('1');
                                this.calcMainData(field.up('[cls="nx_magazines_table_row"]').row_index);
                            }
                        }
                    },
                    {
                        xtype     : 'combo',
                        id        : 'magazines_' + this.count_magazines + '_period' + this.id,
                        name      : 'magazines[' + this.count_magazines + '][period]',
                        store     : (function(){ var result = []; for(var i=0; i<48;i++) {result[i] = i+1;} return result; })(),
                        value     : data.period ? data.period : 6,
                        width     : 70 ,
                        listeners : {
                            scope: this,
                            'change' : function(field) {
                                this.down('textfield[name="is_change_magazines"]').setValue('1');
                                this.calcMainData(field.up('[cls="nx_magazines_table_row"]').row_index);
                            }
                        }
                    },
                    {
                        xtype       : 'textfield',
                        id          : 'magazines_' + this.count_magazines + '_byear' + this.id,
                        name        : 'magazines[' + this.count_magazines + '][byear]',
                        value       : data.byear ? data.byear : 0,
                        width       : 60,
                        submitValue : false,
                        readOnly    : true,
                        //hidden      : data.id ? false : true,
                        fieldStyle  : 'border: 0; background: none;',
                        vtype       : 'Numeric'
                    },
                    {
                        xtype       : 'textfield',
                        id          : 'magazines_' + this.count_magazines + '_bnum' + this.id,
                        name        : 'magazines[' + this.count_magazines + '][bnum]',
                        value       : data.bnum,
                        width       : 70,
                        submitValue : false,
                        readOnly    : true,
                        //hidden      : data.id ? false : true,
                        fieldStyle  : 'border: 0; background: none;',
                        vtype       : 'Numeric'
                    },
                    {
                        xtype       : 'textfield',
                        id          : 'magazines_' + this.count_magazines + '_rqty' + this.id,
                        name        : 'magazines[' + this.count_magazines + '][rqty]',
                        value       : data.rqty,
                        width       : 70,
                        submitValue : false,
                        readOnly    : true,
                        //hidden      : data.id ? false : true,
                        fieldStyle  : 'border: 0; background: none;',
                        vtype       : 'Numeric'
                    },
                    {
                        xtype     : 'button',
                        itemId    : 'magazines_' + this.count_magazines + '_issues_btn',
                        icon      : '/images/extjs/icons/subscription-item-issues.png',
                        tooltip   : 'раскладка по выпускам',
                        scope     : this,
                        //hidden    : data.id ? false : true,
                        row_index : this.count_magazines,
                        handler   : function(button, event) 
                        {
                            this.showIssues(button.row_index);
                        }
                    },
                    {
                        xtype  : 'textfield',
                        itemId : 'magazines_' + this.count_magazines + '_quantity',
                        name   : 'magazines[' + this.count_magazines + '][quantity]',
                        value  : data.quantity ? data.quantity : 1,
                        width  : 50,
                        vtype  : 'Numeric',
                        listeners : {
                            scope: this,
                            'change' : function(field) {
                                this.down('textfield[name="is_change_magazines"]').setValue('1');
                                this.calcMainData(field.up('[cls="nx_magazines_table_row"]').row_index);
                            }
                        }
                    },
                    {
                        xtype  : 'textfield',
                        itemId : 'magazines_' + this.count_magazines + '_price',
                        name   : 'magazines[' + this.count_magazines + '][price]',
                        value  : data.price ? data.price : 0,
                        width  : 70,
                        vtype  : 'Numeric',
                        listeners : {
                            scope: this,
                            'change' : function(field) {
                                this.down('textfield[name="is_change_magazines"]').setValue('1');
                                this.calcMainData(field.up('[cls="nx_magazines_table_row"]').row_index);
                            }
                        }
                    },
                    {
                        xtype  : 'textfield',
                        itemId : 'magazines_' + this.count_magazines + '_discount',
                        name   : 'magazines[' + this.count_magazines + '][discount]',
                        value  : data.discount ? data.discount : 0,
                        width  : 50,
                        vtype  : 'Numeric', 
                        listeners : {
                            scope: this,
                            'change' : function(field) {
                                this.down('textfield[name="is_change_magazines"]').setValue('1');
                                this.calcMainData(field.up('[cls="nx_magazines_table_row"]').row_index);
                            }
                        }
                    },
                    {
                        xtype     : 'button',
                        itemId    : 'magazines_' + this.count_magazines + '_price_btn',
                        icon      : '/images/extjs/icons/refresh.png',
                        tooltip   : 'взять цену из прайса',
                        scope     : this,
                        //hidden    : data.id ? false : true,
                        row_index : this.count_magazines,
                        handler   : function(button, event) 
                        {
                            this.updatePrice(button.row_index);
                        }
                    },
                ]
            }
        );
        this.count_magazines++;
    },
    fill: function(items) 
    {
        Ext.each(items, function(item) {
            this.addNewMagazineRow({
                'id'                 : item.id,
                'magazine_id'        : item.periodical_id, 
                'date_start'         : item.date_start,//Ext.Date.format(Ext.Date.parse(item.date_start,'Y-m-d'),'Y-m-d'), 
                'period'             : item.period,
                'price'              : item.price,
                'discount'           : item.discount,
                'byear'              : item.byear,
                'bnum'               : item.bnum,
                'rqty'               : item.rqty,
                'quantity'           : item.quantity,
                //'type'               : item.type,
                //'delivery_type_name' : item.delivery_type_name,
                'delivery_type_id'   : item.delivery_type_id
            }, 'fill');
        }, this);
    },
    showIssues: function(row_index) 
    {
        var item_id       = this.down('#magazines_' + row_index + '_id').getValue();
        var periodical_id = Ext.getCmp('magazines_' + row_index + '_periodical_id' + this.id).getValue();
        var byear         = Ext.getCmp('magazines_' + row_index + '_byear' + this.id).getValue();
        var bnum          = Ext.getCmp('magazines_' + row_index + '_bnum' + this.id).getValue();
        var rqty          = Ext.getCmp('magazines_' + row_index + '_rqty' + this.id).getValue();

        if(parseInt(periodical_id) && parseInt(byear) && parseInt(bnum) && parseInt(rqty))
        {
            ExtWindow = new Ext.window.Window({
                title: 'Раскладка по выпускам',
                autoShow: true,
                width: 430,
                maxHeight:500,
                layout: 'fit',
                //y:100,
                //modal: true,
                items: [
                    {
                        xtype: 'grid',
                        store: new Ext.data.JsonStore({
                            proxy: {
                                type: 'ajax',
                                url : '/subscription/get-issues',
                                reader: {
                                    type: 'json',
                                    root: 'items',
                                    successProperty: 'success',
                                    totalProperty: 'total'
                                },
                                extraParams: {
                                    ship_item_id  : item_id,
                                    periodical_id : periodical_id,
                                    byear         : byear,
                                    bnum          : bnum,
                                    rqty          : rqty
                                }
                            },
                            pageSize: 999,
                            remoteFilter: true,
                            autoLoad : true,
                            fields        : [
                                {name: 'shipping_id'},
                                {name: 'name'},
                                {name: 'release_date'},
                                //{name: 'shipment_info'},
                                {name: 'channel_name'},
                                {name: 'shipment_date'},
                                {name: 'shipment_id'},
                                {name: 'ship_count'}
                            ]
                        }),
                        columns    : [
                            {text: 'Название', dataIndex: 'name', flex: 1},
                            {text: 'Дата выхода', dataIndex: 'release_date', width: 85},
                            //{text: 'Отгружено', dataIndex: 'shipment_info',  width: 150},
                            {text: 'Канал', dataIndex: 'channel_name',  width: 70},
                            {text: 'Отгрузка', dataIndex: 'shipment_date',  width: 120},
                            {
                                xtype        : 'actioncolumn',
                                width        : 30,
                                menuDisabled : true,
                                hidden       : !nx.app.isperm('ship_manager'),
                                items        : [{
                                    icon    : '/images/extjs/icons/refresh.png',
                                    tooltip : 'Сбросить отгрузку (повторная отгрузка)',
                                    scope   : this,
                                    handler : function(view, rowIndex, colIndex, item, e, rec) 
                                    {
                                        Ext.Msg.confirm('Подтверждение', 'Сбросить статус отгрузки (повторная отгрузка)?', function(btn){
                                            if(btn=='yes')
                                            {
                                                var id = rec.get('id');
                                                Ext.Ajax.request({
                                                    url     : '/ship/reset-shipment',
                                                    method  : 'POST',
                                                    params  : {
                                                        id : rec.get('shipping_id')
                                                    },
                                                    scope   : this,
                                                    success : function(response) {
                                                        var response = Ext.JSON.decode(response.responseText);
                                                        view.up('grid').store.reload();
                                                    }
                                                });
                                            }
                                        },this);
                                    },
                                    getClass: function(value, meta, rec, rowindex)
                                    {
                                        if( rec.get('shipment_id') && rec.get('shipment_id') != 0 ) {
                                            return '';
                                        }
                                        return 'x-hide-display';
                                    }
                                }]
                            }
                        ],
                        viewConfig : { 
                            enableTextSelection: true,
                            getRowClass: function(rec, rowIdx, params, store) {
                                return rec.get('ship_count') >= 1 && rec.get('shipment_id') == 0 ? 'nx-fail-row' : (rec.get('ship_count') > 1 ? 'nx-row-warn' : '');
                            }
                        } 
                    }
                ]
            });
        }
    },
    calcMainData: function(row_index, action) 
    {
        var periodical_id    = Ext.getCmp('magazines_' + row_index + '_periodical_id' + this.id).getValue();
        var delivery_type_id = Ext.getCmp('magazines_' + row_index + '_delivery_type_id' + this.id).getValue();
        var date_start       = Ext.getCmp('magazines_' + row_index + '_date_start' + this.id).getValue();
        var period           = Ext.getCmp('magazines_' + row_index + '_period' + this.id).getValue();

        if(periodical_id!='0' && delivery_type_id!='0' && date_start && period!='0')
        {   
            var row = this.down('#magazines_table_row' + row_index);
            row.disable();
            Ext.Ajax.request(
            {
                url     : '/subscription/get-data-for-new-item',
                method  : 'GET',
                params  : {
                    periodical_id    : periodical_id,
                    delivery_type_id : delivery_type_id,
                    date_start       : date_start,
                    period           : period,
                    company_id       : this.view ? this.view.down('form').getForm().findField('company_id').getValue() : 0,
                    country_id       : this.view ? this.view.down('form').getForm().findField('country_id').getValue() : 0,
                },
                scope   : this,
                success : function(response) 
                {
                    //Ext.ComponentQuery.query('#magazines_table_row' + row_index)[0].unmask();
                    row.enable();
                    var response = Ext.JSON.decode(response.responseText);
                    Ext.getCmp('magazines_' + row_index + '_byear' + this.id).setValue(response.data.byear);
                    Ext.getCmp('magazines_' + row_index + '_bnum' + this.id).setValue(response.data.bnum);
                    Ext.getCmp('magazines_' + row_index + '_rqty' + this.id).setValue(response.data.rqty);
                    if(action=='add')
                    {
                        //this.down('#magazines_' + row_index + '_price').setValue(response.data.price);
                        callWithSuspendEvent(this.down('#magazines_' + row_index + '_price'), 'setValue', response.data.price, 'change');
                    }
                }
            });
        }
    },
    extendRow: function(row_index)
    {
        var date_start  = Ext.getCmp('magazines_' + row_index + '_date_start' + this.id).getValue();
        var period      = Ext.getCmp('magazines_' + row_index + '_period' + this.id).getValue();
        var extend_date = Ext.Date.add(date_start, Ext.Date.MONTH, period);

        callWithSuspendEvent(Ext.getCmp('magazines_' + row_index + '_date_start' + this.id), 'setValue', extend_date, 'change');

        this.calcMainData(row_index, 'add');
    },
    extendAll: function()
    {
        for(var i=0;i<this.count_magazines;i++)
        {
            this.extendRow(i);
        }
    },
    updatePrice: function(row_index) 
    {
        this.calcMainData(row_index, 'add');
        this.down('textfield[name="is_change_magazines"]').setValue('1');
    }
    /*setEditable: function()
    {
        for(var i=0;i<this.count_magazines;i++)
        {
            var items = this.down('#magazines_table_row' + i).items;
            items.each(function(item){
                item.disable();
            });
            this.down('#magazines_' + i + '_issues_btn').enable();
        }
    }*/
});