/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.model.address', {
    extend: 'Ext.data.Model',
    fields: [
    	{
    		name    : 'id',
    		mapping : 'id'
    	},
        {
            name    : 'address',
            mapping : 'address'
        }, 
    	{
    		name    : 'phone',
    		mapping : 'phone'
    	}, 
    	{
    		name    : 'fax',
    		mapping : 'fax'
    	}
    ]
});