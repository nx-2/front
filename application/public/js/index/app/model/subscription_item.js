/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.model.subscription_item', {
    extend: 'Ext.data.Model',
    fields: [
        {name : 'id'},
        {name : 'subscription_id'},
        {name : 'order_id'},
        {name : 'email'},
        {name : 'discount'},
        {name : 'quantity'},
        {name : 'completed'},
        {name : 'date_start'}, 
        {name : 'date_end'}, 
        {name : 'period'}, 
        {name : 'rqty'}, 
        {name : 'bnum'}, 
        {name : 'byear'}, 
        {name : 'created'},  
        {name : 'periodical_name'},  
        {name : 'periodical_id'},  
        {name : 'delivery_type_id'},  
        {name : 'delivery_type_name'}, 
        {name : 'price', type : 'float'},
        {name : 'enabled'}, 
        {name : 'type'}, 
        {name : 'order_state_id'},
        {name : 'order_state_name'}, 
        {name : 'order_type'}, 
        {name : 'name'},
        {name : 'order_price'},
        {name : 'payment_date', type: 'date'},
        {name : 'payment_type_name'},
        {name : 'order_label'},
        {name : 'is_present'},
        {name : 'prolong_state'}
    ]
});