/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.model.subscription', {
    extend: 'Ext.data.Model',
    fields: [
    	{
    		name    : 'id',
            type    : 'int',
    		mapping : 'id'
    	},
        {
            name    : 'xpress_id',
            type    : 'int',
            mapping : 'xpress_id'
        }, 
    	{
    		name    : 'name',
    		mapping : 'name'
    	}, 
        {
            name    : 'order_state_id',
            mapping : 'order_state_id'
        },
        {
            name    : 'order_state_name',
            mapping : 'order_state_name'
        },  
        {
            name    : 'periodical_names',
            mapping : 'periodical_names'
        }, 
        {
            name    : 'price',
            type    : 'float',
            mapping : 'price'
        }, 
        {
            name    : 'subscriber_type_id',
            mapping : 'subscriber_type_id'
        }, 
        {
            name    : 'payment_type_id',
            mapping : 'payment_type_id'
        }, 
        {
            name    : 'payment_type_name',
            mapping : 'payment_type_name'
        }, 
        {
            name    : 'payment_agent_id',
            mapping : 'payment_agent_id'
        }, 
        {
            name    : 'delivery_types',
            mapping : 'delivery_types'
        }, 
        {
            name    : 'delivery_type_names',
            mapping : 'delivery_type_names'
        }, 
        {
            name    : 'created',
            //type    : 'date',
            mapping : 'created'
        }, 
        {
            name    : 'label',
            mapping : 'label'
        }, 
        {
            name    : 'enabled',
            mapping : 'enabled'
        }, 
        //{name    : 'checked'}, 
        {
            name    : 'payment_date',
            type    : 'date',
            mapping : 'payment_date'
        },
        {
            name    : 'date_start_sub',
            type    : 'date',
            mapping : 'date_start_sub'
        },
        {
            name    : 'date',
            type    : 'date',
            mapping : 'date'
        },
        {
            name    : 'is_present',
            mapping : 'is_present'
        },
        {
            name    : 'type',
            mapping : 'type'
        },
        {
            name    : 'hash',
            mapping : 'hash'
        }, 
        {
            name    : 'company_inn',
            mapping : 'company_inn'
        }, 
        {
            name    : 'company_kpp',
            mapping : 'company_kpp'
        },
        {name: 'email'},
        {name: 'last_updated'},
        {name: 'company_id'},
        {name: 'phone'},
        {name: 'full_map_address'},
        {name: 'map_zipcode'},
        {name: 'comment'}
    ]
});