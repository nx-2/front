/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.model.maillog', {
    extend: 'Ext.data.Model',
    fields: [
    	{
    		name    : 'id',
    		mapping : 'id'
    	},
        {
            name    : 'email',
            mapping : 'email'
        },
        {
            name    : 'from',
            mapping : 'from'
        },
        {
            name    : 'subject',
            mapping : 'subject'
        },
        {
            name    : 'message',
            mapping : 'message'
        },
        {
            name    : 'created',
            mapping : 'created'
        }
    ]
});