/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.model.payment', {
    extend: 'Ext.data.Model',
    fields: [
    	{
    		name    : 'id',
    		mapping : 'id'
    	},
        {
            name    : 'order_id',
            mapping : 'order_id'
        },
        {
            name    : 'ws1c_id',
            mapping : 'ws1c_id'
        },
        {
            name    : 'docno',
            mapping : 'docno'
        },
        {
            name    : 'sum',
            mapping : 'sum'
        },
        {
            name    : 'contractor_id',
            mapping : 'contractor_id'
        },
        {
            name    : 'contractor_name_inn',
            mapping : 'contractor_name_inn'
        },
        {
            name    : 'contractor_match_comment',
            mapping : 'contractor_match_comment'
        },
        {
            name    : 'payment_agent_id',
            mapping : 'payment_agent_id'
        },
        {
            name    : 'payment_agent_name',
            mapping : 'payment_agent_name'
        },
        {
            name    : 'status',
            mapping : 'status'
        },
        {
            name    : 'result',
            mapping : 'result'
        },
        {
            name    : 'status_comment',
            mapping : 'status_comment'
        },
        {
            name    : 'request_data',
            mapping : 'request_data'
        },
        {
            name    : 'date',
            mapping : 'date'
        },
        {
            name    : 'year',
            mapping : 'year'
        },
        {
            name    : 'purpose',
            mapping : 'purpose'
        },
        {
            name    : 'comment',
            mapping : 'comment'
        },
        {
            name    : 'label',
            mapping : 'label'
        },
        {
            name    : 'enabled',
            mapping : 'enabled'
        },
        {
            name    : 'created',
            mapping : 'created'
        },
        {
            name    : 'last_updated',
            mapping : 'last_updated'
        },
        {
            name    : 'transaction_id',
            mapping : 'transaction_id'
        },
        {
            name    : 'contractor_id',
            mapping : 'contractor_id'
        }
    ]
});