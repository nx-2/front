/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.model.user', {
    extend: 'Ext.data.Model',
    fields: [
    	{
    		name    : 'id',
    		mapping : 'id'
    	},
        {
            name    : 'name',
            mapping : 'full_name'
        }, 
    	{
    		name    : 'login',
    		mapping : 'login'
    	}, 
    	{
    		name    : 'enabled',
    		mapping : 'checked'
    	}, 
    	{
    		name    : 'email',
    		mapping : 'email'
    	}, 
        {
            name    : 'roles',
            mapping : 'roles'
        }
    ]
});