/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.model.company', {
    extend: 'Ext.data.Model',
    fields: [
        {name : 'id'},
        {name : 'checked'},
        {name : 'enabled'},
        {name : 'name'},
        {name : 'inn'},
        {name : 'kpp'}, 
        {name : 'shortname'}, 
        {name : 'othernames'}, 
        {name : 'wrongnames'}, 
        {name : 'email'}, 
        {name : 'url'}, 
        {name : 'org_form'},
        {name : 'created'},
        {name : 'ws1c_synced'}
    ]
});