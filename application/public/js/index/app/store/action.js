/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.store.action', {
    extend: 'Ext.data.Store',
    remoteFilter: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/action/index'
        },
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
            totalProperty: 'total'
        }
    },
    fields      : [
        {name: 'id'},
        {name: 'name'},
        {name: 'email'},
        {name: 'discount'},
        {name: 'date_begin'},
        {name: 'date_finish'},
        {name: 'checked'},
        {name: 'created'},
        {name: 'count'},
        {name: 'count_pay'},
        {name: 'page_id'},
        {name: 'code'}
    ]
});