/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.store.order', {
    extend: 'Ext.data.Store',
    //model: 'nx.model.order',
    remoteFilter: true,
    proxy: {
        type: 'ajax',
        api: {
	        read: '/order/index',
	    },
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
            totalProperty: 'total'
        }
    },
    fields      : [
        {name: 'id'},
        {name: 'email'},
        {name: 'old_order_id'},
        {name: 'created'},
        {name: 'name'},
        {name: 'item_name'},
        {name: 'periodical_name'},
        {name: 'issue_name'},
        {name: 'issue_title'},
        {name: 'order_state_id'},
        {name: 'order_state_name'},
        {name: 'price'},
        {name: 'payment_type_id'},
        {name: 'payment_type_name'},
        {name: 'type'},
        {name: 'payment_date', type: 'date'}
    ]
});