/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.store.maillog', {
    extend       : 'Ext.data.Store',
    model        : 'nx.model.maillog',
    alias        : 'store.maillog',
    remoteFilter : true,
    proxy        : {
        type: 'ajax',
        api: {
	        read: '/mail-log/index',
	    },
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
            totalProperty: 'total'
        }
    },
});