/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.store.user', {
    extend: 'Ext.data.Store',
    //fields: ['name', 'email'],
    model: 'nx.model.user',
    //autoLoad: true,
    /*data: [
        {name: 'Ed',    email: 'ed@sencha.com'},
        {name: 'Tommy', email: 'tommy@sencha.com'}
    ],*/
    /*proxy: {
        type: 'ajax',
        api: {
	        read: 'js/index/data/users.json',
	        update: 'js/index/data/updateUsers.json'
	    },
        reader: {
            type: 'json',
            root: 'users',
            successProperty: 'success'
        }
    }*/
    //pageSize: page_size,
    groupField: 'roles',
    remoteFilter: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/user/index',
        },
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
            totalProperty: 'total'
        }
    }
});