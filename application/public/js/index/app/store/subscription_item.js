/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.store.subscription_item', {
    extend       : 'Ext.data.Store',
    model        : 'nx.model.subscription_item',
    alias        : 'store.subscription_item',
    remoteFilter : true,
    autoSync     : true,
    proxy        : {
        type: 'ajax',
        api: {
	        read: '/subscription/items',
            update: '/order-item/update',
	    },
        writer: {
            writeRecords: function(request, data) {
                if(data[0])
                {
                    request.params = data[0];
                }
                return request;
            }
        },
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
            totalProperty: 'total'
        }
    },
});