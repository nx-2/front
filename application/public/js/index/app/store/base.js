/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.store.base', {
    extend: 'Ext.data.Store',
    constructor: function(args) {
        nx.store.base.superclass.constructor.call(this,args);
        this.getProxy().encodeFilters = this.encodeFilters;
    },
    encodeFilters: function(filters) {//add 'xoperator' property into request ( 'operator' already exist )
        var min = [],
            length = filters.length,
            i = 0;

        for (; i < length; i++) {
            min[i] = {
                property: filters[i].property,
                value   : filters[i].value
            };
            if(filters[i].xoperator) {
                Ext.apply(min[i], {xoperator: filters[i].xoperator});
            }
        }
        return this.applyEncoding(min);
    },
    getFilterValueFor:function(property) {
        var filters = this.filters;
        return filters.getAt(filters.findIndex('property', new RegExp('^' + property + '$'))).value;
    },
});