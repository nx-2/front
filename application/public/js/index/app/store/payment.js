/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.store.payment', {
    extend       : 'Ext.data.Store',
    model        : 'nx.model.payment',
    alias        : 'store.payment',
    remoteFilter : true,
    autoSync     : true,
    proxy        : {
        type: 'ajax',
        //actionMethods : {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
        api: {
	        read: '/payment/index',
            update: '/payment/update',
	    },
        writer: {
            writeRecords: function(request, data) {
                if(data[0])
                {
                    request.params = data[0];
                }
                return request;
            }
        },
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
            totalProperty: 'total'
        }
    },
    listeners: {
        write : function(store, op) 
        {
            var response = Ext.JSON.decode(op.response.responseText);
            if(response.confirmMessage)
            {
                Ext.Msg.confirm('Подтверждение', response.confirmMessage, function(btn){
                    if (btn == 'yes') {
                        //repeat last operation
                        store.getProxy()[op.action](new Ext.data.Operation({action: op.action, params: Ext.applyIf(op.request.params, {nx_ui_confirmed : '1'})}));
                    }
                }, this);
            }
        },
    }
});