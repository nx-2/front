/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.store.subscription', {
    extend: 'nx.store.base',
    model: 'nx.model.subscription',
    alias: 'store.subscription',
    //autoLoad: true,
    remoteFilter: true,
    remoteSort:true,
    //buffered: true,
    proxy: {
        type: 'ajax',
        api: {
	        read: '/subscription/index',
	        //update: 'js/index/data/updateUsers.json'
	    },
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
            totalProperty: 'total'
        },
        /*encodeFilters: function(filters) {//add 'operator' property into request
            var min = [],
                length = filters.length,
                i = 0;

            for (; i < length; i++) {
                min[i] = {
                    property: filters[i].property,
                    value   : filters[i].value
                };
                if(filters[i].operator) {
                    Ext.apply(min[i], {operator: filters[i].operator});
                }
            }
            return this.applyEncoding(min);
        }*/
    },
    /*listeners: {
        'beforeload': function(store, op) {
            console.log(op);
        }
    }*/
});