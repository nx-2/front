/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.store.chart', {
    extend: 'Ext.data.Store',
    remoteFilter: true,
    proxy: {
        type: 'ajax',
        api: {
            read: '/chart/index'
        },
        reader: {
            type: 'json',
            root: 'items',
            successProperty: 'success',
            totalProperty: 'total'
        }
    },
    fields      : [
        {name: 'id'},
        {name: 'name'},
    ]
});