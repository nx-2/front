/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.periodical.index' ,{
    extend: 'Ext.grid.Panel',

    title: 'Издания',

    store: 'periodical',

    //autoScroll: true,
    //scroll: true,
    //loadMask   : {msg : 'Загрузка...'},
    //height: '100%',

    initComponent: function() {
        /*var my_filters = Ext.create('Ext.ux.grid.GridFilters', {
            filters: [{
                type: 'numeric',
                dataIndex: 'id'
            }]
        });*/
        

        this.columns = 
        [
            {
                xtype:'actioncolumn',
                width:50,
                items: [
                {
                    icon: '/images/extjs/icons/edit.png',
                    tooltip: 'Редактировать',
                    handler: function(grid, rowIndex, colIndex) 
                    {
                        var rec = grid.getStore().getAt(rowIndex);
                        //alert("Edit " + rec.get('firstname'));
                        window.location.hash = '#periodical/edit/id/' + rec.get('id');
                    }
                }]
            },
            {header: 'ID',  dataIndex: 'id',  flex: 1},// filter: {type: 'numeric'}, filterable: true},
            {header: 'Код', dataIndex: 'code', flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Название', dataIndex: 'name', flex: 1}
        ];

        var filtersCfg = {
            ftype: 'filters',
            menuFilterText: 'Фильтровать',
            autoReload: false, //don't reload automatically
            local: true, //only filter locally
            // filters may be configured through the plugin,
            // or in the column definition within the headers configuration
            filters: [{
                active : true,
                type: 'numeric',
                dataIndex: 'id'
            }, {
                type: 'string',
                dataIndex: 'code'
            }, {
                type: 'string',
                dataIndex: 'name'
            }]
        };

        Ext.apply(this, {
            features: [filtersCfg]
        });

        /*var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                //highlightOnFilter     : false, 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : true
        });*/

        /*Ext.apply(this, {
            //plugins: [{ptype: 'gridheaderfilters', ensureFilteredVisible: false, stateful: false}]
            plugins: [gridheaderfilters]
        });*/

        Ext.apply(this, {
            
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            }],
        });

        this.callParent(arguments);
    }
    /*,onRender: function()
    {
        
        //console.log('test');
        
    }*/

});