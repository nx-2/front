/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.chart.index.grid' ,{
    extend : 'Ext.grid.Panel',
    alias  : 'widget.nx_chart_list',
    store  : 'chart',
    itemId : 'nx_chart_list',

    initComponent: function() {

        this.columns = [
            {
                xtype  : 'actioncolumn',
                hidden : !nx.app.isperm('admin'),
                width  : 40,
                items  : [{
                    icon    : '/images/extjs/icons/edit.png',
                    tooltip : 'Редактировать',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_chart_edit', this, rec);
                    }
                },{
                    icon    : '/images/extjs/icons/external.png',
                    tooltip : 'Ссылка',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        window.open('/#chart/index/show/' + rec.get('id'),'_blank');
                    }
                }]
            },
            {text: 'id',  dataIndex: 'id',  width: 40, filter: {xtype: 'textfield'}},
            {text: 'Название',  dataIndex: 'name', width: 190, filter: {xtype: 'textfield'}},
            {
                xtype  : 'actioncolumn',
                hidden : !nx.app.isperm('admin'),
                width  : 25,
                items: [
                    {
                        width   : 16,
                        height  : 16,
                        tooltip : 'удалить',
                        icon    : '/images/extjs/icons/disabled.png',
                        scope   : this,
                        handler : function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Подтверждение', 'Удалить?', function(btn){
                                if(btn=='yes')
                                {
                                    Ext.Ajax.request({
                                        url     : '/chart/delete',
                                        method  : 'POST',
                                        params  : {
                                            id : rec.get('id')
                                        },
                                        scope   : this,
                                        success : function(response) {
                                            grid.store.reload();
                                        }
                                    });
                                }
                            },this);
                        }
                    }
                ]
            }
        ];

        Ext.apply(this, {
            viewConfig : { 
                enableTextSelection: true
            },
            //allowDeselect: true,
            /*selModel: {
                mode: 'MULTI'
            }*/
        });

        Ext.apply(this, {
            dockedItems: [
            {
                xtype  : 'toolbar',
                hidden : !nx.app.isperm('admin'),
                items  : [
                    '->',
                    {
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        handler : function() {
                            this.fireEvent('nx_chart_add', this);
                        }
                    }
                ]
            }]
        });

        this.callParent(arguments);
    },
    onRender: function() {
        this.callParent(arguments);
        this.store.load();
        //console.log(this);
        if(this.nxparams.show) {
            var id = this.nxparams.show;
            //this.collapse();
            this.showChart(id, {maximize: 1});
        }
    },
    listeners: {
        'select' : function(sm, rec) {
            this.showChart(rec.get('id'));
            sm.deselectAll();
        },
    },
    addChart: function(data) {
        if(Ext.ComponentQuery.query('#chart_' + data['id']).length) {
            return false;
        }
        var board   = this.up('#nx_chart_index').down('#nx_chart_board');
        
        var chart = this.getChartByType(data);
        var tools = [];
        if(data['type']!=0) {
            tools[tools.length] = {
                type    : 'gear',
                tooltip : 'данные',
                scope   : this,
                handler : function(event, toolEl, owner, tool) {
                    new Ext.window.Window({
                        title     : data['name'],
                        layout    : 'fit',
                        border    : false,
                        resizable : true,
                        autoShow  : true, 
                        items     : [this.getChartByType(data, 0)]
                    });
                }
            }
        }
        //console.log(chart, board, data);
        var chartWindow = {
            xtype       : 'window',
            itemId      : 'chart_' + data['id'],
            title       : data['name'],
            width       : 620,
            height      : 430,
            autoShow    : true,
            constrain   : true,
            layout      : 'fit',
            maximizable : true,
            items       : [chart],
            tools       : tools,
            listeners: {
                'close' : function(panel) {
                    board.remove(panel, true);
                }
            }
        }
        return board.add(chartWindow);
    },
    getChartByType: function(data, type) {
        //0 - grid
        //1 - line
        //2 - bar
        var chart_type = type!=undefined ? type : data['type'];
        var chart      = {}
        var fields     = [];
        var xfields    = [];
        var yfields    = [];
        var columns    = [{xtype: 'rownumberer', width: 50}];
        if(data['data'][0]) {
            for(var prop in data['data'][0]) {
                if(!fields.length) {
                    fields[fields.length]   = {name: prop};
                    xfields[xfields.length] = prop;
                }
                else {
                    fields[fields.length]   = {name: prop, type: 'float'};
                    yfields[yfields.length] = prop;
                }
                columns[columns.length] = {text: prop, dataIndex: prop, flex:1};
            }
        }
        var store = new Ext.data.Store({
            data  : data['data'],
            fields: fields
        });

        if(chart_type==0) {
            chart = {
                xtype      : 'grid',
                width      : 500,
                height     : 500,
                store      : store,
                columns    : columns,
                viewConfig : { 
                    enableTextSelection: true
                },
                dockedItems: [
                {
                    xtype: 'toolbar',
                    items : [
                        {
                            text   : 'Экспорт CSV',
                            iconCls: 'export-csv-ico',
                            handler: function() 
                            {
                                var grid = this.up('window').down('grid');
                                Ext.Msg.confirm('Подтверждение','Экспортировать ' + grid.store.getCount() + ' строк?', function(btn){
                                    if(btn=='yes')
                                    {
                                        var data = Ext.ux.exporter.Exporter.exportGrid(grid, 'csv');
                                        submitFakeForm('/index/export-data', data, 'Данные');
                                    }
                                },grid);  
                            }
                        }
                    ]
                }]
            }
        }
        if(chart_type==1 || chart_type==2) {
            var series = [];
            if(chart_type==1) {
                for(var i = 0; i<yfields.length; i++)
                {
                    s = {
                        type      : 'line',
                        axis      : ['left'],
                        xField    : xfields[0],
                        yField    : yfields[i],
                        highlight : true,
                        tips      : {
                            trackMouse : true,
                            width      : 140,
                            height     : 28,
                            renderer   : function(storeItem, item) {//console.log(item);
                                this.setTitle(storeItem.get(item.series.xField) + ': ' + storeItem.get(item.series.yField));
                            }
                        }
                    }
                    series[series.length] = s;
                }
            }
            if(chart_type==2) {
                series[0] = {
                    type      : 'column',
                    axis      : ['left'],
                    xField    : xfields[0],
                    yField    : yfields,
                    highlight : true,
                    tips      : {
                        trackMouse : true,
                        width      : 140,
                        height     : 28,
                        renderer   : function(storeItem, item) {//console.log(item);
                            this.setTitle(storeItem.get(item.series.xField) + ': ' + storeItem.get(item.yField));
                        }
                    }
                }
            }
            //console.log(fields,xfields,yfields,series);

            chart = {
                width   : 600,
                height  : 400,
                animate : false,
                xtype   : 'chart',
                title   : 'chart',
                legend  : true,
                store   : store,
                series  : series,
                axes    : [
                    {
                        type     : 'Category',
                        position : 'bottom',
                        fields   : xfields,
                        title    : xfields[0],
                        grid     : true,
                    }, 
                    {
                        type     : 'Numeric',
                        position : 'left',
                        fields   : yfields,
                        grid     : true,
                    }
                ]  
            }
        }

        return chart;
    },
    showChart: function(id, params){
        Ext.Ajax.request({
            url     : '/chart/load',
            method  : 'POST',
            params  : {
                id : id,
            },
            scope   : this,
            success : function(response) {
                var response = Ext.JSON.decode(response.responseText);
                var chart    = this.addChart(response.data);
                if(params && params.maximize) {
                    this.hide();//collapse(null, false);
                    chart.maximize();
                }
            }
        });
    }
});

Ext.define('nx.view.chart.index' ,{
    extend : 'Ext.panel.Panel',
    itemId : 'nx_chart_index',
    title  : 'Панель графиков',
    layout : {
        type:'border',
        //align:'stretch'
    },
    initComponent: function() {
        Ext.apply(this, {
            items: [
                {
                    region : 'center',
                    //flex   : 1,
                    //title  : 'board',
                    itemId : 'nx_chart_board',
                    items  : [
                    ]
                },
                {
                    xtype       : 'nx_chart_list',
                    width       : 300,
                    region      : 'east',
                    collapsible : true,
                    title       : 'Список графиков',
                    split       : true,
                    nxparams    : this.nxparams,
                }
            ]
        });

        this.callParent(arguments);
    }
});