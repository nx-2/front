/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.chart.add', {
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_chart_add',
    title    : 'Добавление графика',
    autoShow : true,
    width    : 700,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title: 'свойства',
                                layout: 'hbox',
                                padding: 5,
                                items: 
                                [
                                    {
                                        layout: 'form',
                                        border: 0,
                                        flex: 10,
                                        items: [
                                            {
                                                xtype      : 'textfield',
                                                name       : 'name',
                                                fieldLabel : 'Название',
                                                allowBlank : false
                                            },
                                            {
                                                xtype      : 'textarea',
                                                name       : 'query',
                                                fieldLabel : 'SQL запрос',
                                                allowBlank : false,
                                                grow       : true,
                                                growMin    : 0,
                                                growMax    : 200
                                            },
                                            {
                                                xtype         : 'combo',
                                                name          : 'type',
                                                store         : [['0','grid'],['1','line'], ['2','column']],
                                                fieldLabel    : 'тип',
                                                editable      : false,
                                                queryMode     : 'local',
                                                triggerAction : 'all',
                                                value         : '0'
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Добавить',
                action: 'add'
            },
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});