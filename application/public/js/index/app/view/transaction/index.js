/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.transaction.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_transaction_list',

    title: 'Транзакции',

    initComponent: function() {

        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/transaction/index'
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields        : [
                    {name: 'id'},
                    {name: 'date'},
                    {name: 'shop'},
                    {name: 'transactionid'},
                    {name: 'sum'},
                    {name: 'system_name'},
                    {name: 'description'},
                    {name: 'person'},
                    {name: 'sys_status'},
                    {name: 'shop_status'},
                    {name: 'buh_status'}
                ]
            })
        });

        this.columns = [
            {header: 'id',  dataIndex: 'id',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Дата',  dataIndex: 'date',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Магазин',  dataIndex: 'shop',  flex: 1, filter: {xtype: 'combo', editable: false, filterName: 'shop_id', store: [['4','Подписка'],['8','Продажа']]}, renderer: function(value) { 
                    if(value=='OSsP.Subs') return 'Подписка';
                    if(value=='OSP.Shop') return '<span class="transaction-shop">Продажа</span>';
                    return value;
                }
            },
            {header: 'tr_ID',  dataIndex: 'transactionid',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Система',  dataIndex: 'system_name',  flex: 1, filter: {xtype: 'textfield'},},
            {header: 'Сумма',  dataIndex: 'sum',  flex: 1, filter: {xtype: 'textfield'}, renderer: function(v) { return v.toString().replace('.',','); }},
            {header: 'Описание',  dataIndex: 'description',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Плательщик',  dataIndex: 'person',  flex: 1, filter: {xtype: 'textfield'}},
            {
                header: 'Статус платеж. системы',  
                dataIndex: 'sys_status',  
                flex: 1,
                filter: {xtype: 'combo', editable : false, store: [['-2','ошибка суммы'],['-1','fail'],['0','неизвестно'],['1','подтверждено'],['2','оплачено']]},
                renderer: function(value) 
                {
                    if(value=='-2') 
                    {
                        return 'ошибка суммы';
                    }
                    else if(value=='-1') 
                    {
                        return 'fail';
                    }
                    else if(value=='0') 
                    {
                        return 'неизвестно';
                    }
                    else if(value=='1') 
                    {
                        return 'подтверждено';
                    }
                    else if(value=='2') 
                    {
                        return 'оплачено';
                    }
                }
            },
            {
                header: 'Статус передачи',  
                dataIndex: 'shop_status',  
                flex: 1,
                filter: {xtype: 'combo', editable : false, store: [['0','не передано'],['1','подтверждено'],['2','завершено сразу'],['3','завершено по запросу']]},
                renderer: function(value) 
                {
                    if(value=='0') 
                    {
                        return 'не передано';
                    }
                    else if(value=='1') 
                    {
                        return 'подтверждено';
                    }
                    else if(value=='2') 
                    {
                        return 'завершено сразу';
                    }
                    else if(value=='3') 
                    {
                        return 'завершено по запросу';
                    }
                }
            }
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    return rec.get('sys_status') == 2 ? 'transaction-row-payed' : '';
                } 
            },
            allowDeselect: true,
            selModel: {
                mode: 'MULTI'
            },
        });

        Ext.apply(this, {
            dockedItems: [{
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных',
                plugins     : [{ptype: 'pagesize'}]
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },
                    ,'-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Транзакции');
                                }
                            },this);   
                        }
                    },
                    '->',
                    {
                        text   : 'Сохранить в платежи',
                        scope: this,
                        handler: function(btn) {
                            this.fireEvent('nx_transaction_send_to_payment', btn);
                        }
                    }
                ]
            }]
        });

        this.callParent(arguments);
    }
});