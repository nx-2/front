/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.model.comment', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name   : 'id',
            type   : 'int',
            useNull: true
        },
        {
            name : 'comment',
        }, 
        {
            name : 'entity_id',
            type : 'int',
        },
        {
            name : 'row_id',
            type : 'int',
        },
        {
            name: 'created'
        }, 'tag_name', 'tag_id'
    ],
    validations: [{
        type  : 'length',
        field : 'comment',
        min   : 1
    }]
});

Ext.define('nx.view.comment.list', {
    extend      : 'Ext.grid.Panel',
    alias       : 'widget.nx_comment_list',
    xtype       : 'nx_comment_list', 
    header      : false,
    hideHeaders : true,
    //height : 100, 

    initComponent: function(){
        this.store = new Ext.create('Ext.data.Store', {
            model    : 'nx.model.comment',
            autoLoad : true,
            autoSync : true,
            proxy    : {
                type: 'ajax',
                api: {
                    read    : '/comment/index',
                    create  : '/comment/add',
                    update  : '/comment/update',
                    destroy : '/comment/delete'
                },
                reader: {
                    type: 'json',
                    root: 'items',
                    successProperty: 'success',
                    totalProperty: 'total'
                    //messageProperty: 'message'
                },
                writer: {
                    writeRecords: function(request, data) {
                        if(data[0])
                        {
                            request.params = data[0];
                        }
                        return request;
                    }
                //    type: 'json',
                //    writeAllFields: false,
                //    root: 'data'
                },
                //listeners: {
                //    exception: function(proxy, response, operation){
                //        Ext.MessageBox.show({
                //            title: 'REMOTE EXCEPTION',
                //            msg: operation.getError(),
                //            icon: Ext.MessageBox.ERROR,
                //            buttons: Ext.Msg.OK
                //        });
                //    }
                //}
            },
            listeners: {
                scope: this,
                'beforeload' : function(store) {
                    store.getProxy().extraParams = {
                        entity_id: this.entity_id,
                        row_id   : this.row_id
                    }
                    /*var filters = this.store.filters;
                    filters.add(new Ext.util.Filter({
                        property: 'row_id',
                        value   : this.row_id
                    }));*/
                    //filters.add(new Ext.util.Filter({
                    //    property: 'entity_id',
                    //    value   : this.entity_id
                    //}));
                },
                write: function(proxy, operation){
                    if (operation.action == 'create') {
                        this.store.reload();
                    }
                    //if (operation.action == 'destroy') {
                    //    main.child('#form').setActiveRecord(null);
                    //}
                    //Ext.example.msg(operation.action, operation.resultSet.message);
                }
            }
        });
        //this.editing = Ext.create('Ext.grid.plugin.CellEditing');
        this.editing = Ext.create('Ext.grid.plugin.RowEditing', {
            saveBtnText  : 'Обновить',
            cancelBtnText: 'Отменить',
        });

        Ext.apply(this, {
            viewConfig : { 
                enableTextSelection: true,
            },
            allowDeselect : true,
            //iconCls       : 'icon-grid',
            //frame         : true,
            plugins       : [this.editing],
            dockedItems   : [{
                xtype: 'toolbar',
                items: [{
                    html: 'Комментарии',
                },
                '->',
                {
                    iconCls : 'add-ico',
                    text    : 'Добавить',
                    scope   : this,
                    handler : this.onAddClick
                }, {
                    icon     : '/images/extjs/icons/disabled.png',
                    text     : 'Удалить',
                    disabled : true,
                    itemId   : 'delete',
                    scope    : this,
                    handler  : this.onDeleteClick
                }]
            }],
            columns: [
            {
                //text         : 'id',
                width        : 30,
                //sortable     : true,
                //resizable    : false,
                //draggable    : false,
                //hideable     : false,
                //menuDisabled : true,
                dataIndex    : 'id'
            }, 
            {
                dataIndex: 'created',
                width    : 120
            },
            {
                dataIndex: 'tag_name',
                editor: {
                    name            : 'tag_id',
                    xtype           : 'nx.ux.JsonCombo',
                    emptyText       : 'выбрать тег',
                    url             : '/tag/get-objects-for-combo',
                    matchFieldWidth : false,
                    listeners: {
                        scope: this,
                        'select' : function(combo) {
                            var selection = this.getView().getSelectionModel().getSelection()[0];
                            selection.set('tag_name', combo.getRawValue());
                        }
                    }
                }
                //width           : 100
            },
            {
                //text      : 'comment',
                flex      : 1,
                //sortable  : true,
                dataIndex : 'comment',
                editor    : {
                    type: 'textfield'
                }
            }]
        });
        this.callParent();
        this.getSelectionModel().on('selectionchange', this.onSelectChange, this);
    },
    
    onSelectChange: function(selModel, selections){
        this.down('#delete').setDisabled(selections.length === 0);
    },

    onDeleteClick: function(){
        var selection = this.getView().getSelectionModel().getSelection()[0];
        if (selection) {
            Ext.Msg.confirm('Подтверждение','Удалить?', function(btn){
                if(btn=='yes')
                {
                    this.store.remove(selection);
                }
            },this);
        }
    },

    onAddClick: function(){
        var rec = new nx.model.comment({
            comment   : '',
            entity_id : this.entity_id,
            row_id    : this.row_id
        }), edit = this.editing;

        edit.cancelEdit();
        this.store.insert(0, rec);
        edit.startEdit(0, 2);
        /*edit.startEditByPosition({
            row: 0,
            column: 2
        });*/
    }
});

