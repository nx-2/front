/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.log.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_log_list',

    title: 'Логирование',

    //store: 'log',

    initComponent: function() {

        Ext.apply(this, {     
            store: new Ext.data.Store({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/log/index',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields : [
                    {name: 'id'},
                    {name: 'title'},
                    {name: 'user_name'},
                    {name: 'created'}
                ]
            })
        });

        this.columns = [
            {header: 'Событие',  dataIndex: 'title',  flex: 3, filter: {xtype: 'textfield'}},
            {header: 'Пользователь',  dataIndex: 'user_name',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Дата', dataIndex: 'created', width: 120, filter: {xtype: 'textfield'}},
            {
                xtype        : 'actioncolumn',
                width        : 30,
                menuDisabled : true,
                items        : [{
                    icon: '/images/extjs/icons/watch.png',
                    tooltip: 'Показать тех. данные',
                    scope: this,
                    handler: function(grid, rowIndex, colIndex, item, e, rec) 
                    {
                        var log_id = rec.get('id');
                        Ext.Ajax.request({
                            url     : '/log/get',
                            method  : 'GET',
                            params  : {
                                id : log_id
                            },
                            scope   : this,
                            success : function(response) {
                                var response = Ext.JSON.decode(response.responseText);
                                showDataAsTree(response.data);
                            }
                        });
                    }
                }]
            }
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            } 
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);
    }
});