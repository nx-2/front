/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.address.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_address_list',

    title: 'Адреса',

    store: 'address',

    initComponent: function() {

        this.columns = [
            {
                xtype:'actioncolumn',
                width:50,
                items: [
                {
                    icon: '/images/extjs/icons/edit.png',
                    tooltip: 'Редактировать',
                    scope: this,
                    handler: function(grid, rowIndex, colIndex) 
                    {
                        var rec = grid.getStore().getAt(rowIndex);
                        //alert("Edit " + rec.get('firstname'));
                        window.location.hash = '#address/edit/id/' + rec.get('id');
                        //this.fireEvent('nx_user_edit', this, rec);
                        //Ext.History.fireEvent('change', 'users/edit/id/' + rec.get('id'), {rec: rec});
                        //Ext.History.add('users/edit/id/' + rec.get('id'));
                    }
                }]
            },
            {header: 'id',  dataIndex: 'id',  flex: 1},
            {header: 'Адрес',  dataIndex: 'address',  flex: 1},
            {header: 'Телефон',  dataIndex: 'phone',  flex: 1},
            {header: 'Факс',  dataIndex: 'fax',  flex: 1}
        ];

        Ext.apply(this, {
            
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            }],
        });

        this.callParent(arguments);
    }
});