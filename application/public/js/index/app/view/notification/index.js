/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.notification.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_notification_list',

    title: 'Системные сообщения',

    initComponent: function() {

        this.title += ' - <a href="/notification/rss">rss feed</a>';

        Ext.apply(this, {     
            store: new Ext.data.Store({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/notification/index',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields : [
                    {name: 'id'},
                    {name: 'message'},
                    {name: 'type'},
                    {name: 'checked'},
                    {name: 'created'}
                ]
            })
        });

        this.columns = [
            {
                xtype:'actioncolumn',
                width:20,
                items: [{
                    icon: '/images/extjs/icons/bullet_black.png',
                    tooltip: 'Отметить',
                    scope: this,
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_switch_field', grid, rec, 'checked');
                    },
                    getClass: function(value, meta, rec, rowindex)
                    {
                        if(rec.get('checked')==1) {
                            return 'nx-transparent';
                        }
                    }
                }]
            },
            {text: 'id', dataIndex: 'id', width: 50, hidden:true, filter: {xtype: 'textfield'}},
            {text: 'Дата', dataIndex: 'created', width: 120, filter: {xtype: 'textfield'}},
            {text: 'Сообщение', dataIndex: 'message', flex: 1, filter: {xtype: 'textfield'}},
            {text: 'Тип', dataIndex: 'type', width: 30, hidden:true, filter: {xtype: 'textfield'}},
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    var css_class = '';
                    css_class += rec.get('checked') == 0 ? 'nx-row-warn' : '';
                    return css_class;
                } 
            } 
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);
    },
    onRender: function() {
        this.callParent(arguments);
        if(this.nxparams.show) {
            var id = this.nxparams.show;
            this.showNotification(id);
        }
    },
    showNotification: function(id)
    {
        Ext.Ajax.request({
            url     : '/notification/index',
            method  : 'GET',
            params  : {
                filter : '[{"property":"id","value":"' + id + '"}]'
            },
            scope   : this,
            success : function(response) {
                var response = Ext.JSON.decode(response.responseText);
                if(parseInt(response.total))
                {
                    Ext.Msg.alert('Сообщение', response.items[0].message);
                }
            }
        });
    }
});