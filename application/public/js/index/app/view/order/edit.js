/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.order.edit', {
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_order_edit',
    title    : 'Редактирование заказа',
    autoShow : true,
    width    : 900,

    initComponent: function() {
        var viewid = this.id;
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title: 'свойства',
                                layout: 'hbox',
                                padding: 5,
                                items: 
                                [
                                    {
                                        flex: 10,
                                        border: 0,
                                        items: [
                                            {
                                                layout: 'form',
                                                border: 0,
                                                items: [
                                                    {
                                                        xtype       : 'textfield',
                                                        name        : 'id',
                                                        fieldLabel  : 'ID',
                                                        fieldStyle  : 'border: 0; background: none;',
                                                        submitValue : false,
                                                        readOnly    : true
                                                    },
                                                    {
                                                        xtype  : 'textfield',
                                                        name   : 'order_state_id_old',
                                                        hidden : true
                                                    },
                                                    {
                                                        xtype         : 'combo',
                                                        name          : 'type',
                                                        store         : subscription_type_store,
                                                        fieldLabel    : 'тип',
                                                        editable      : false,
                                                        queryMode     : 'local',
                                                        triggerAction : 'all'
                                                    },
                                                    {
                                                        xtype          : 'combobox',
                                                        name           : 'order_state_id',
                                                        store          : orderStatesStore,
                                                        fieldLabel     : 'Статус',
                                                        editable       : false,
                                                        queryMode      : 'local',
                                                        forceSelection : true,
                                                        triggerAction  : 'all'
                                                    },
                                                    {
                                                        xtype          : 'combobox',
                                                        name           : 'payment_type_id',
                                                        store          : payment_types_store,
                                                        fieldLabel     : 'Способ оплаты',
                                                        editable       : false,
                                                        queryMode      : 'local',
                                                        triggerAction  : 'all',
                                                        forceSelection : true
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'comment',
                                                        fieldLabel : 'Комментарий'
                                                    },
                                                    {
                                                        xtype          : 'checkbox',
                                                        name           : 'enabled',
                                                        fieldLabel     : 'Включен?',
                                                        inputValue     : '1',
                                                        uncheckedValue : '0'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'email',
                                                        fieldLabel : 'Email',
                                                        vtype      : 'email'
                                                    },
                                                    {
                                                        xtype      : 'datefield',
                                                        name       : 'payment_date',
                                                        fieldLabel : 'Дата оплаты',
                                                        format     : 'Y-m-d',
                                                        editable   : false
                                                    },
                                                    {
                                                        border: 0,
                                                        layout: 'hbox',
                                                        items: [
                                                            {
                                                                xtype  : 'textfield',
                                                                hidden : true,
                                                                name   : 'person_id',
                                                                id     : 'nx_edit_person_id' + viewid
                                                            },
                                                            {
                                                                xtype      : 'textfield',
                                                                fieldLabel : 'Физ. лицо',
                                                                name       : 'person_id_name',
                                                                id         : 'nx_edit_person_name' + viewid,
                                                                readOnly   : true,
                                                                flex       : 4
                                                            },
                                                            {
                                                                xtype           : 'nx.ux.JsonCombo',
                                                                flex            : 2,
                                                                emptyText       : 'выбрать физ. лицо',
                                                                url             : '/person/get-objects-for-combo',
                                                                matchFieldWidth : false,
                                                                listeners       : {
                                                                    scope: this,
                                                                    'select' : function(combo) {
                                                                        Ext.getCmp('nx_edit_person_id' + viewid).setValue(combo.getValue());
                                                                        Ext.getCmp('nx_edit_person_name' + viewid).setValue(combo.getRawValue());
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        border: 0,
                                                        layout: 'hbox',
                                                        items: [
                                                            {
                                                                xtype  : 'textfield',
                                                                hidden : true,
                                                                name   : 'user_id',
                                                                id     : 'nx_edit_user_id' + viewid
                                                            },
                                                            {
                                                                xtype      : 'textfield',
                                                                fieldLabel : 'OSP юзер',
                                                                name       : 'user_name',
                                                                id         : 'nx_edit_user_name' + viewid,
                                                                readOnly   : true,
                                                                flex       : 4
                                                            },
                                                            {
                                                                xtype           : 'nx.ux.JsonCombo',
                                                                flex            : 2,
                                                                emptyText       : 'выбрать OSP юзера',
                                                                url             : '/user/get-objects-for-combo',
                                                                matchFieldWidth : false,
                                                                listeners       : {
                                                                    scope: this,
                                                                    'select' : function(combo) {
                                                                        Ext.getCmp('nx_edit_user_id' + viewid).setValue(combo.getValue());
                                                                        Ext.getCmp('nx_edit_user_name' + viewid).setValue(combo.getRawValue());
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                    }    
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        layout   : 'form',
                                        border   : 0,
                                        flex     : 6,
                                        margins  : '0 0 0 25',
                                        defaults : {
                                            labelAlign: 'right'
                                        },
                                        items: [
                                            {
                                                xtype       : 'textfield',
                                                name        : 'price',
                                                fieldLabel  : 'стоимость',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'create_user_name',
                                                fieldLabel  : 'завел',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'created',
                                                fieldLabel  : 'внесение',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_user_name',
                                                fieldLabel  : 'изменил',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_updated',
                                                fieldLabel  : 'правка',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ],
                listeners: {
                    scope: this,
                    'actioncomplete' : function(form, action) {
                        this.down('form').getForm().findField('order_state_id_old').setValue( this.down('form').getForm().findField('order_state_id').getValue() );
                    }
                }
            }
        ];

        this.buttons = [
            {
                text   : 'Сохранить',
                action : 'save'
            },
            {
                text    : 'Закрыть',
                action  : 'close',
                scope   : this,
                handler : this.close
            }
        ];

        this.callParent(arguments);
    }
});