/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.order.stattable' ,{
    extend: 'Ext.grid.Panel',
    itemId: 'nx_order_stattable',

    title: 'Покупки pdf - статистика таблицей',
    nx_data: {
        show_count : true,
        show_sum   : true
    },
    initComponent: function() 
    {
        var fields = [
            {name: 'yearmonth'}
        ];
        Ext.each(subscribleMagazinesStore, function(magazine){
            if(magazine[0]) {
                fields[fields.length] = {name: magazine[1]};
            }
        });

        var columns = [
            {text: 'год-месяц', dataIndex: 'yearmonth', width: 100}
        ];
        Ext.each(subscribleMagazinesStore, function(magazine){
            if(magazine[0]) {
                columns[columns.length] = {
                    text            : magazine[1], 
                    dataIndex       : magazine[1], 
                    flex            : 1, 
                    renderer        : this.cellRenderer, 
                    summaryType     : this.summaryType, 
                    summaryRenderer : this.summaryRenderer
                };
            }
        }, this);

        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/order/stattable',
                    },
                    reader: {
                        type            : 'json',
                        root            : 'items',
                        successProperty : 'success',
                        totalProperty   : 'total'
                    }
                },
                remoteFilter : true,
                fields       : fields,
                autoLoad     : false
            })
        });

        Ext.apply(this, {
            columns : columns
        });

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            },
            xfilters : [
                //{fieldLabel: 'метка', name: 'label', xtype: 'textfield', ops: ['=','!=']},
                //{fieldLabel: 'тип подписчика', name: 'subscriber_type_id', xtype: 'combo', store: subscriber_type_store},
                //{fieldLabel: 'тип', name: 'order_type', xtype: 'combo', store: subscription_type_store},
                //{fieldLabel: 'id акции', name: 'action_id', xtype: 'textfield'},
                //{fieldLabel: 'год-месяц начало', name: 'yearmonth_start', xtype: 'textfield'},
                //{fieldLabel: 'год-месяц конец', name: 'yearmonth_end', xtype: 'textfield'},
            ],
            features: [{
                ftype: 'summary'
            }],
        });
    
        Ext.apply(this, {     
            dockedItems: [
            {
                xtype: 'toolbar',
                items : [
                    //{
                    //    xtype: 'view_state_switcher',
                    //},
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            //this.resetHeaderFilters(false);
                            this.store.clearFilter(true);
                            this.down('#period-switcher-quarter').handler.call(this.down('#period-switcher-quarter'));
                        }
                    },
                    '-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Статистика продаж PDF');
                                }
                            },this);   
                        }
                    },
                    {
                        icon: '/images/extjs/icons/list-prev.png',
                        handler: function() {
                            var step      = Ext.ComponentQuery.query('button[cls=period-switcher][pressed=true]')[0].step;
                            var grid      = this.up('grid');
                            var filters   = grid.store.filters;
                            var cur_start = filters.getAt(filters.findIndex('property', new RegExp('^c_yearmonth_start$'))).value;
                            var cur_end   = filters.getAt(filters.findIndex('property', new RegExp('^c_yearmonth_end$'))).value;
                            var cur_start_month = cur_start.slice(4,6);
                            grid.applyYearMonthFilters(
                                Ext.Date.format(Ext.Date.add(new Date(cur_start.slice(0,4) + '-' + cur_start.slice(4,6) + '-01'), Ext.Date.MONTH, -step+(cur_start_month - step == 0 ? 1 : 0 ) ), 'Ym'),
                                Ext.Date.format(Ext.Date.add(new Date(cur_end.slice(0,4) + '-' + cur_end.slice(4,6) + '-01'), Ext.Date.MONTH, -step), 'Ym')
                            );
                        }
                    },
                    {
                        text: 'Квартал',
                        enableToggle : true,
                        cls: 'period-switcher',
                        step: 3,
                        itemId: 'period-switcher-quarter',
                        handler: function() {
                            Ext.each(Ext.ComponentQuery.query('button[cls=period-switcher]'), function(btn) {
                                btn.toggle(0);
                                this.toggle(1);
                            }, this);
                            var cur_date    = new Date();
                            var cur_month   = cur_date.getMonth();
                            var start_month = '01';
                            var end_month   = '03'; 
                            if(cur_month>3 && cur_month<=6) {
                                start_month = '04';
                                end_month   = '06'; 
                            }
                            if(cur_month>6 && cur_month<=9) {
                                start_month = '07';
                                end_month   = '09'; 
                            }
                            if(cur_month>9) {
                                start_month = '10';
                                end_month   = '12'; 
                            }
                            this.up('grid').applyYearMonthFilters(
                                Ext.Date.format(new Date(), 'Y' + start_month),
                                Ext.Date.format(new Date(), 'Y' + end_month)
                            );
                        }
                    },
                    {
                        text: 'Полгода',
                        enableToggle : true,
                        cls: 'period-switcher',
                        step: 6,
                        itemId: 'period-switcher-halfyear',
                        handler: function() {
                            Ext.each(Ext.ComponentQuery.query('button[cls=period-switcher]'), function(btn) {
                                btn.toggle(0);
                                this.toggle(1);
                            }, this);
                            var cur_date    = new Date();
                            var start_month = '01';
                            var end_month   = '06'; 
                            if(cur_date.getMonth()>5) {
                                start_month = '07';
                                end_month   = '12'; 
                            }
                            this.up('grid').applyYearMonthFilters(
                                Ext.Date.format(new Date(), 'Y' + start_month),
                                Ext.Date.format(new Date(), 'Y' + end_month)
                            );
                        }
                    },
                    {
                        icon: '/images/extjs/icons/list-next.png',
                        handler: function() {
                            var step      = Ext.ComponentQuery.query('button[cls=period-switcher][pressed=true]')[0].step;
                            var grid      = this.up('grid');
                            var filters   = grid.store.filters;
                            var cur_start = filters.getAt(filters.findIndex('property', new RegExp('^c_yearmonth_start$'))).value;
                            var cur_end   = filters.getAt(filters.findIndex('property', new RegExp('^c_yearmonth_end$'))).value;
                            grid.applyYearMonthFilters(
                                Ext.Date.format(Ext.Date.add(new Date(cur_start.slice(0,4) + '-' + cur_start.slice(4,6) + '-01'), Ext.Date.MONTH, step), 'Ym'),
                                Ext.Date.format(Ext.Date.add(new Date(cur_end.slice(0,4) + '-' + cur_end.slice(4,6) + '-01'), Ext.Date.MONTH, step), 'Ym')
                            );
                        }
                    },
                    {
                        text         : 'кол-во',
                        scope        : this,
                        enableToggle : true,
                        pressed      : this.nx_data.show_count,
                        handler      : function(btn) {
                            this.nx_data.show_count = btn.pressed;
                            this.reconfigure();
                        }
                    },
                    {
                        text         : 'сумма',
                        scope        : this,
                        enableToggle : true,
                        pressed      : this.nx_data.show_sum,
                        handler      : function(btn) {
                            this.nx_data.show_sum = btn.pressed;
                            this.reconfigure();
                        }
                    }
                ]
            }]
        });

        this.callParent(arguments);
        this.down('#period-switcher-quarter').handler.call(this.down('#period-switcher-quarter'));
    },
    applyYearMonthFilters:function(start, end) 
    {
        var filters = this.store.filters;
        var index   = filters.findIndex('property', 'c_yearmonth_start');
        if(index!=-1) {
            filters.removeAt(index);
        }
        var index = filters.findIndex('property', 'c_yearmonth_end');
        if(index!=-1) {
            filters.removeAt(index);
        }
        filters.add(new Ext.util.Filter({
            property: 'c_yearmonth_start',
            value   : start
        }));
        filters.add(new Ext.util.Filter({
            property: 'c_yearmonth_end',
            value   : end
        }));
        this.store.load();
    },
    cellRenderer: function(value, meta, rec, rowIndex, colIndex, store, view) 
    {
        if(!value) 
        {
            return 0;
        }
        var is_export = meta['exporter'] ? true : false;
        var grid      = view.up('grid');
        var cfg       = grid.nx_data;
        var content   = '';

        var count       = value['count'];
        var total_price = value['total_price'];
        total_price     = parseFloat(total_price).toFixed(2);
        content += cfg.show_count ? count : '';
        content += cfg.show_sum && cfg.show_count ? (is_export ? " " : '<br/>') : '';
        content += cfg.show_sum ? total_price.toString().replace('.',',') : '';

        return content;
    },
    summaryType: function(rows, col) {
        var sum   = 0;
        var count = 0;
        Ext.each(rows, function(row){
            if(row.get(col)) {
                sum+=parseFloat(row.get(col)['total_price']);
                count+=parseInt(row.get(col)['count']);
            }
        });
        var res = {'total_price' : sum, 'count': count};
        return res;
    },
    summaryRenderer: function(value) {
        var cfg         = this.nx_data;
        var content     = '';
        var count       = value['count'];
        var total_price = value['total_price'];
        total_price     = parseFloat(total_price).toFixed(2);
        content += cfg.show_count ? count.toString() : '';
        content += cfg.show_sum && cfg.show_count ? '<br/>' : '';
        content += cfg.show_sum ? total_price.toString().replace('.',',') : '';
        return content;
    }
});