/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.order.stats' ,{
    extend : 'Ext.grid.Panel',
    alias  : 'widget.stats',
    itemId : 'nx_order_stats',
    title  : 'Статистика списком',
    initComponent: function() {
        var view = this;

        Ext.apply(this, {
            as_links: true,
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/order-item/stats',
                    },
                    reader: {
                        type            : 'json',
                        root            : 'items',
                        successProperty : 'success',
                        totalProperty   : 'total'
                    }
                },
                remoteFilter: true,
                fields        : [
                    {name: 'item_id'},
                    {name: 'count'},
                    {name: 'count_qty'},
                    {name: 'item_type'},
                    {name: 'name'},
                    {name: 'issue_title'},
                    {name: 'sum_total_price'},
                ],
                listeners : {
                    scope: this,
                    'beforeload': function(store)
                    {
                        var filters   = this.store.filters;
                        var f         = [];
                        var allowed   = ['month', 'year', 'issue_num', 'issue_title', 'periodical_id'];
                        filters.each(function(filter){
                            f[filter.property] = 1;
                        }, this);
                        if(!(
                            (f['payment_date_start'] && f['payment_date_end']) || 
                            f['item_id']
                        )) {
                            return false;
                        }
                    }
                }
            })
        });

        Ext.apply(this, {
            columns    : [
                {text: 'ID товара', dataIndex: 'item_id', width: 100, xfilter: true},
                {text: 'Тип товара', dataIndex: 'item_type', width: 120, hidden: true},
                {text: 'Название', dataIndex: 'name', width: 150},
                {text: 'Заголовок', dataIndex: 'issue_title', width: 100},
                {text: 'Всего', dataIndex: 'count_qty', width: 70},
                {text: 'Сумма', dataIndex: 'sum_total_price', width: 120, 
                    summaryType: function(rows) {
                        var sum = 0;
                        Ext.each(rows, function(row){
                            sum+=parseFloat(row.get('sum_total_price'));
                        });
                        return sum.toFixed(2);
                    }
                },
            ]
        });

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            //reloadOnChange        : false,
            layout                : 'hbox'
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            },
            features: [{
                ftype: 'summary'
            }],
            xfilters : [
                {fieldLabel: 'ID товара', name: 'item_id', xtype: 'textfield'},
                {fieldLabel: 'дата оплаты', name: 'payment_date_start', xtype: 'datefield', group: 'payment_date', width: 230, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                    {name: 'payment_date_end', xtype: 'datefield', group: 'payment_date', width: 120, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
            ]
        });
    
        Ext.apply(this, {     
            dockedItems: [
            {
                xtype : 'toolbar',
                items : [
                    {
                        xtype: 'view_state_switcher',
                    },
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetFilters();
                        }
                    },
                    {
                        icon    : '/images/extjs/icons/list-prev.png',
                        tooltip : 'предыдущий месяц',
                        scope   : this,
                        handler : function() {
                            var date  = this.getFilterValueBy('payment_date_start');
                            var d     = new Date(date);
                            var month = d.getMonth();
                            var year  = d.getFullYear();
                            month     = month - 1;
                            if(month == -1) {
                                month = 11;
                                year  = year - 1;
                            }
                            var new_start_date = new Date(year, month, 1);
                            var new_start      = Ext.Date.format(new_start_date, 'Y-m-01');
                            var new_end        = Ext.Date.format(Ext.Date.getLastDateOfMonth(new_start_date), 'Y-m-d')
                            this.applyPaymentDateFilters(new_start, new_end);
                        }
                    },
                    {
                        icon    : '/images/extjs/icons/list-next.png',
                        tooltip : 'следующий месяц',
                        scope   : this,
                        handler : function() {
                            var date  = this.getFilterValueBy('payment_date_start');
                            var d     = new Date(date);
                            var month = d.getMonth();
                            var year  = d.getFullYear();
                            month     = month + 1;
                            if(month == 12) {
                                month = 0;
                                year  = year + 1;
                            }
                            var new_start_date = new Date(year, month, 1);
                            var new_start      = Ext.Date.format(new_start_date, 'Y-m-01');
                            var new_end        = Ext.Date.format(Ext.Date.getLastDateOfMonth(new_start_date), 'Y-m-d')
                            this.applyPaymentDateFilters(new_start, new_end);
                        }
                    },
                    '-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Продажи - статистика');
                                }
                            },this);   
                        }
                    },
                ]
            }],
        });

        this.callParent(arguments);

        var cur_date = new Date();
        this.store.filters.add('payment_date_start', new Ext.util.Filter({
            property: 'payment_date_start',
            value   : Ext.Date.format(cur_date, 'Y-m-01')
        }));
        this.store.filters.add('payment_date_end', new Ext.util.Filter({
            property: 'payment_date_end',
            value   : Ext.Date.format(Ext.Date.getLastDateOfMonth(cur_date), 'Y-m-d')
        }));
    },
    resetFilters: function() {
        this.resetHeaderFilters(false);
        this.store.clearFilter(true);
        var cur_date = new Date();
        this.applyPaymentDateFilters(Ext.Date.format(cur_date, 'Y-m-01'), Ext.Date.format(Ext.Date.getLastDateOfMonth(cur_date), 'Y-m-d'));
    },
    applyPaymentDateFilters:function(start, end) {
        var filters = this.store.filters;
        var index   = filters.findIndex('property', 'payment_date_start');
        if(index!=-1) {
            filters.removeAt(index);
        }
        var index = filters.findIndex('property', 'payment_date_end');
        if(index!=-1) {
            filters.removeAt(index);
        }
        filters.add(new Ext.util.Filter({
            property: 'payment_date_start',
            value   : start
        }));
        filters.add(new Ext.util.Filter({
            property: 'payment_date_end',
            value   : end
        }));
        this.store.load();
    },
    getFilterValueBy:function(property) {
        var filters = this.store.filters;
        var index   = filters.findIndex('property', new RegExp('^' + property + '$'));
        return index==-1 ? null : filters.getAt(index).value;
    },
});