/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

/*Ext.define('nx.view.order.message', {
    extend: 'Ext.window.Window',
    alias: 'widget.nx_order_message',

    title: 'Отправить сообщение об оплате со ссылкой на pdf',
    autoShow: true,
    width: 800,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        border  : 0,
                        padding : 5,
                        items   : [
                            {
                                xtype      : 'textfield',
                                name       : 'subject',
                                fieldLabel : 'Тема',
                                labelWidth : 70,
                                inputWidth : 700
                            }
                        ]
                    },
                    {
                        xtype : 'htmleditor',
                        width : '100%',
                        height: 500,
                        //grow  : true,
                        //growMax : 400,
                        name  : 'message'
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Отправить',
                scope: this,
                handler: function(){
                    this.down('form').getForm().submit({
                        url     : '/order/send-message-after-pay',
                        waitMsg : 'Отправка данных',
                        scope   : this,
                        params  : {
                            order_id : this.order_id
                        },
                        success : function(form, response) {
                            this.close();
                        }
                    });
                }
            },
            {
                text: 'Отправить себе',
                scope: this,
                handler: function(){
                    this.down('form').getForm().submit({
                        url     : '/order/send-message-after-pay',
                        waitMsg : 'Отправка данных',
                        scope   : this,
                        params  : {
                            debug : true,
                            order_id : this.order_id
                        },
                        success : function(form, response) {
                            this.close();
                        }
                    });
                }
            },
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    },
    onRender: function() {
        this.callParent(arguments);
        this.down('form').getForm().load(
        {
            url     : '/order/get-message-after-pay',
            waitMsg : 'Загрузка',
            scope   : this,
            params  : 
            {
                order_id : this.order_id
            },
            success : function() 
            {
            }
        });
    }
});*/