/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.order.index.grid' ,{
    extend : 'Ext.grid.Panel',
    alias  : 'widget.nx_order_list',
    store  : 'order',
    itemId : 'nx_order_issue_list',

    initComponent: function() {

        this.columns = [
            {
                xtype:'actioncolumn',
                width:50,
                items: [{
                    icon    : '/images/extjs/icons/edit.png',
                    tooltip : 'Редактировать',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_order_edit', this, rec);
                    }
                },{
                    icon    : '/images/extjs/icons/order-message.gif',
                    tooltip : 'Письмо об оплате',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        var templates = [];
                        if(rec.get('order_state_id')=='3' || rec.get('type')!='0')
                        {
                            templates[templates.length] = {id: 10, name: 'Шаблон покупки/отправки PDF'};
                        }
                        templates[templates.length] = {id: 21, name: 'Шаблон из асурц №21'};
                        //view = Ext.widget('nx_order_message', {order_id: rec.get('id')});
                        Ext.widget('nx_ux_mailform', {
                            entity_id       : rec.get('id'), 
                            //entity_name     : 'Подписка', 
                            log_message     : 'Отправка письма для заказа #' + rec.get('id'),
                            get_message_url : '/order/get-message-after-pay',
                            email           : rec.get('email'),
                            templates       : templates
                        });
                        //}
                    },
                    /*getClass: function(value, meta, rec, rowindex)
                    {
                        if((rec.get('order_state_id')=='3' || rec.get('type')!='0') && rec.get('old_order_id')=='0')
                        {
                            return '';
                        }
                        return 'x-hide-display';
                    }*/
                }]
            },
            {text: 'id',  dataIndex: 'id',  width: 60, xfilter: true},
            {text: 'ФИО',  dataIndex: 'name',  flex: 1, xfilter: true},
            {text: 'Email', dataIndex: 'email', width: 150, xfilter: true},
            {text: 'Издание', dataIndex: 'periodical_name', width: 100, xfilter: 'periodical_id'},
            {text: 'Выпуск', dataIndex: 'issue_title',  flex: 1, xfilter: true},
            {text: 'Состояние', dataIndex: 'order_state_name', width: 70, xfilter: 'order_state_id'},
            {text: 'Сумма', dataIndex: 'price', width: 100, xfilter: true, renderer: function(v) { return v.toString().replace('.',','); },
                summaryType: function(rows) {
                    var sum = 0;
                    Ext.each(rows, function(row){
                        sum+=parseFloat(row.get('price'));
                    });
                    return sum.toFixed(2);
                }
            },
            {text: 'Способ оплаты', dataIndex: 'payment_type_name', width: 90, xfilter: 'payment_type_id'},
            {text: 'Дата создания',  dataIndex: 'created', width: 120, xfilter: true},
            {text: 'Дата оплаты', dataIndex: 'payment_date',  width: 90, renderer: Ext.util.Format.dateRenderer('Y-m-d'), xfilter: true},
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    return rec.get('order_state_id') == 3 ? 'transaction-row-payed' : '';
                } 
            }, 
            features: [{
                ftype: 'summary'
            }],
            xfilters : [
                {fieldLabel: 'id', name: 'id', xtype: 'textfield'},
                {fieldLabel: 'фио', name: 'name', xtype: 'textfield'},
                {fieldLabel: 'email', name: 'email', xtype: 'textfield'},
                {fieldLabel: 'состояние', name: 'order_state_id', xtype: 'combo', store: orderStatesStore},
                {fieldLabel: 'издание', name: 'periodical_id', xtype: 'combo', store: subscribleMagazinesStore, editable : false, multiSelect: true, invalidCls: ''},
                {fieldLabel: 'сумма', name: 'price', xtype: 'textfield'},
                {fieldLabel: 'способ оплаты', name: 'payment_type_id', xtype: 'combo', store: payment_types_store},
                {fieldLabel: 'выпуск', name: 'issue_title', xtype: 'combo', store: delivery_types_store},
                {fieldLabel: 'дата создания', name: 'created', xtype: 'textfield'},
                {fieldLabel: 'интервал созд.', name: 'created_start', xtype: 'datefield', group: 'created', width: 230, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                    {name: 'created_end', xtype: 'datefield', group: 'created', width: 120, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                {fieldLabel: 'дата оплаты', name: 'payment_date', xtype: 'textfield'},
                {fieldLabel: 'интервал опл.', name: 'payment_date_start', xtype: 'datefield', group: 'payment_date', width: 230, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                    {name: 'payment_date_end', xtype: 'datefield', group: 'payment_date', width: 120, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
            ]
        });

        Ext.apply(this, {
            dockedItems: [{
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных',
                plugins     : [{ptype: 'pagesize'}]
            },
            {
                xtype: 'toolbar',
                items : [
                    // {
                    //     xtype: 'view_state_switcher',
                    // },
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters(false);
                            this.store.clearFilter();
                        }
                    },
                    ,'-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Продажи_журналов');
                                }
                            },this);   
                        }
                    },
                    '-',
                    {
                        text    : 'Статистика списком',
                        //iconCls : 'ship-ico',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'order/stats';
                        }
                    },
                    {
                        text    : 'Статистика таблицей',
                        //iconCls : 'ship-ico',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'order/stat';
                        }
                    },
                    '->',
                    {
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        handler : function() {
                            this.fireEvent('nx_order_add', this);
                        }
                    }
                ]
            }]
        });

        this.addEvents('nx_order_add', 'nx_order_edit', 'nx_order_switch_field');
        this.callParent(arguments);
    },
    onRender: function() {
        this.callParent(arguments);
        this.store.pageSize = page_size-5;
        this.store.getProxy().extraParams = this.nxparams;
        this.store.load();
    }
});

Ext.define('nx.view.order.index' ,{
    extend : 'Ext.panel.Panel',
    title  : 'Продажа журналов',
    layout : {
        type:'vbox',
        align:'stretch'
    },
    initComponent: function() {
        var viewid = this.id;
        var curDate = new Date();
        Ext.apply(this, {
            items: [
                {
                    xtype   : 'form',
                    padding : '5 5 0 5',
                    border  : 0,
                    //flex:1
                    items   : [
                        {
                            xtype       : 'textfield',
                            id          : 'order_last_moth_income' + viewid,
                            labelWidth  : 120,
                            fieldLabel  : 'итого за ' + Ext.Date.format(curDate, 'F').toLowerCase(),
                            fieldStyle  : 'border: 0; background: none;',
                            submitValue : false,
                            readOnly    : true
                        },
                        {
                            xtype       : 'textfield',
                            id          : 'order_prev_moth_income' + viewid,
                            labelWidth  : 120,
                            fieldLabel  : 'итого за ' + Ext.Date.format(new Date(curDate.getFullYear(), curDate.getMonth()-1, 1), 'F').toLowerCase(),
                            fieldStyle  : 'border: 0; background: none;',
                            submitValue : false,
                            readOnly    : true
                        }
                    ],
                    listeners: {
                        render: function() {
                            Ext.Ajax.request({
                                url     : '/order/get-last-month-income',
                                method  : 'POST',
                                scope   : this,
                                success : function(response) {
                                    var response = Ext.JSON.decode(response.responseText);
                                    Ext.getCmp('order_last_moth_income' + viewid).setValue(response.data.income_last_month);
                                    Ext.getCmp('order_prev_moth_income' + viewid).setValue(response.data.income_prev_month);
                                }
                            });
                        }
                    }
                },
                {
                    xtype    : 'nx_order_list',
                    nxparams : this.nxparams,
                    flex     : 1
                }
            ]
        });

        this.callParent(arguments);
    }
});