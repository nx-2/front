/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.person.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_person_list',

    title: 'Физ. лица',

    store: 'person',

    initComponent: function() {

        this.columns = [
            {
                xtype:'actioncolumn',
                width:50,
                items: [
                {
                    icon: '/images/extjs/icons/edit.png',
                    tooltip: 'Редактировать',
                    scope: this,
                    handler: function(grid, rowIndex, colIndex) 
                    {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_person_edit', this, rec);
                    }
                }]
            },
            {header: 'id',  dataIndex: 'id',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Имя',  dataIndex: 'name',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Email',  dataIndex: 'email',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Телефон',  dataIndex: 'phone',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Должность',  dataIndex: 'position',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Дата', dataIndex: 'created', flex: 1, filter: {xtype: 'textfield'}}
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    var css_class = '';
                    css_class += rec.get('enabled') == 0 ? ' nx-row-disabled' : ''; 
                    return css_class;
                } 
            } 
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },
                    '->',
                    {
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        handler : function() {
                            this.fireEvent('nx_person_add', this);
                        }
                    }
                ]
            }],
        });

        this.addEvents('nx_person_edit');
        this.addEvents('nx_person_add');
        this.callParent(arguments);
    }
});