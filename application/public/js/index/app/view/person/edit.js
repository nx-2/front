/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.person.edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.nx_person_edit',

    title: 'Редактирование физ. лица',
    autoShow: true,
    width: 900,
    //height: 500,

    initComponent: function() {
        var viewid = this.id;
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title: 'свойства',
                                layout: 'hbox',
                                padding: 5,
                                items: 
                                [
                                    {
                                        layout: 'form',
                                        border: 0,
                                        flex: 10,
                                        items: [
                                            {
                                                xtype       : 'textfield',
                                                name        : 'id',
                                                fieldLabel  : 'ID',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype          : 'checkbox',
                                                name           : 'checked',
                                                fieldLabel     : 'Проверено?',
                                                inputValue     : '1',
                                                uncheckedValue : '0'
                                            },
                                            {
                                                xtype          : 'checkbox',
                                                name           : 'enabled',
                                                fieldLabel     : 'Включено?',
                                                inputValue     : '1',
                                                uncheckedValue : '0'
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'name',
                                                fieldLabel  : 'ФИО',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'f',
                                                fieldLabel : 'Фамилия'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'i',
                                                fieldLabel : 'Имя',
                                                //allowBlank : false,
                                                vtype      : 'alpharu'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'o',
                                                fieldLabel : 'Отчество'
                                            },
                                            {
                                                xtype      : 'combo',
                                                name       : 'gender',
                                                fieldLabel : 'Пол',
                                                store      : [['0', 'жен.'],['1', 'муж.']],
                                                editable   : false
                                            },
                                            {
                                                border: 0,
                                                layout: 'hbox',
                                                items: [
                                                    {
                                                        xtype  : 'textfield',
                                                        name   : 'company_id',
                                                        id     : 'nx_edit_company_id' + viewid,
                                                        hidden : true,
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        fieldLabel : 'Организация',
                                                        name       : 'company_name',
                                                        id         : 'nx_edit_company_name' + viewid,
                                                        readOnly   : true,
                                                        flex       : 4
                                                    },
                                                    {
                                                        xtype           : 'nx.ux.JsonCombo',
                                                        emptyText       : 'выбрать организацию',
                                                        url             : '/company/get-objects-for-combo',
                                                        matchFieldWidth : false,
                                                        flex            : 2,
                                                        listeners       : {
                                                            scope: this,
                                                            'select' : function(combo) {
                                                                Ext.getCmp('nx_edit_company_id' + viewid).setValue(combo.getValue());
                                                                Ext.getCmp('nx_edit_company_name' + viewid).setValue(combo.getRawValue());
                                                            }
                                                        }
                                                    } 
                                                ]
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'position',
                                                fieldLabel : 'Должность'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'speciality',
                                                fieldLabel : 'Специальность'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'phone',
                                                fieldLabel : 'Телефон'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'email',
                                                fieldLabel : 'Email',
                                                vtype      : 'email'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'comment',
                                                fieldLabel : 'Комментарий'
                                            }
                                        ]
                                    },
                                    {
                                        layout: 'form',
                                        border: 0,
                                        flex: 6,
                                        margins: '0 0 0 25',
                                        defaults: {
                                            labelAlign: 'right'
                                        },
                                        items: [
                                            {
                                                xtype       : 'textfield',
                                                name        : 'create_user_name',
                                                fieldLabel  : 'завел',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'created',
                                                fieldLabel  : 'внесение',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_user_name',
                                                fieldLabel  : 'изменил',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_updated',
                                                fieldLabel  : 'правка',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title: 'адрес',
                                layout: 'form',
                                padding: 5,
                                items: [
                                    {
                                        xtype         : 'combo',
                                        name          : 'country_id',
                                        store         : countries_store,
                                        fieldLabel    : 'Страна',
                                        editable      : false,
                                        queryMode     : 'local',
                                        triggerAction : 'all'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'area',
                                        fieldLabel : 'Область'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'region',
                                        fieldLabel : 'Район'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'city',
                                        fieldLabel : 'Город, село'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'zipcode',
                                        fieldLabel : 'Индекс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'address',
                                        fieldLabel : 'Адрес'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'telcode',
                                        fieldLabel : 'Тел. код'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'address_phone',
                                        fieldLabel : 'Телефон'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'fax',
                                        fieldLabel : 'Факс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'address_comment',
                                        fieldLabel : 'Комментарий'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Сохранить',
                action: 'save'
            },
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});