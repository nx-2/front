/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.person.add', {
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_person_add',
    title    : 'Добавление физ. лица',
    autoShow : true,
    width    : 700,

    initComponent: function() {
        var viewid = this.id;
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title: 'свойства',
                                padding: 5,
                                layout: 'form',
                                border: 0,
                                items: [
                                    {
                                        xtype          : 'checkbox',
                                        name           : 'checked',
                                        fieldLabel     : 'Проверено?',
                                        inputValue     : '1',
                                        uncheckedValue : '0',
                                        checked        : true
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'f',
                                        fieldLabel : 'Фамилия'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'i',
                                        fieldLabel : 'Имя',
                                        allowBlank : false,
                                        vtype      : 'alpharu'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'o',
                                        fieldLabel : 'Отчество'
                                    },
                                    {
                                        xtype      : 'combo',
                                        name       : 'gender',
                                        fieldLabel : 'Пол',
                                        store      : [['0', 'жен.'],['1', 'муж.']],
                                        editable   : false
                                    },
                                    {
                                        border: 0,
                                        layout: 'hbox',
                                        items: [
                                            {
                                                xtype  : 'textfield',
                                                name   : 'company_id',
                                                id     : 'nx_add_company_id' + viewid,
                                                hidden : true,
                                            },
                                            {
                                                xtype      : 'textfield',
                                                fieldLabel : 'Организация',
                                                name       : 'company_name',
                                                id         : 'nx_add_company_name' + viewid,
                                                readOnly   : true,
                                                flex       : 4
                                            },
                                            {
                                                xtype           : 'nx.ux.JsonCombo',
                                                emptyText       : 'выбрать организацию',
                                                url             : '/company/get-objects-for-combo',
                                                matchFieldWidth : false,
                                                flex            : 2,
                                                listeners       : {
                                                    scope: this,
                                                    'select' : function(combo) {
                                                        Ext.getCmp('nx_add_company_id' + viewid).setValue(combo.getValue());
                                                        Ext.getCmp('nx_add_company_name' + viewid).setValue(combo.getRawValue());
                                                    }
                                                }
                                            } 
                                        ]
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'position',
                                        fieldLabel : 'Должность'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'speciality',
                                        fieldLabel : 'Специальность'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'phone',
                                        fieldLabel : 'Телефон'
                                    },
                                    {
                                        xtype: 'textfield',
                                        name : 'email',
                                        fieldLabel: 'Email',
                                        vtype: 'email'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'comment',
                                        fieldLabel : 'Комментарий'
                                    }
                                ]
                            },
                            {
                                title: 'адрес',
                                layout: 'form',
                                padding: 5,
                                items: [
                                    {
                                        xtype         : 'combo',
                                        name          : 'country_id',
                                        store         : countries_store,
                                        fieldLabel    : 'Страна',
                                        editable      : false,
                                        queryMode     : 'local',
                                        triggerAction : 'all'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'area',
                                        fieldLabel : 'Область'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'region',
                                        fieldLabel : 'Район'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'city',
                                        fieldLabel : 'Город, село'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'zipcode',
                                        fieldLabel : 'Индекс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'address',
                                        fieldLabel : 'Адрес'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'telcode',
                                        fieldLabel : 'Тел. код'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'phone',
                                        fieldLabel : 'Телефон'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'fax',
                                        fieldLabel : 'Факс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'comment',
                                        fieldLabel : 'Комментарий'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Добавить',
                action: 'add'
            },
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});