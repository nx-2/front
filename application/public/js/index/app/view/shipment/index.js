/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.shipment.index' ,{
    extend : 'Ext.grid.Panel',
    alias  : 'widget.nx_shipment_list',
    title  : 'Лог отгрузок',
    initComponent: function() {
        Ext.apply(this, {     
            store: new Ext.data.Store({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/ship/shipment',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields : [
                    {name: 'id'},
                    {name: 'channel_name'},
                    {name: 'create_user_name'},
                    {name: 'date'},
                    {name: 'created'},
                    //{name: 'file_hash'}
                ]
            })
        });

        this.columns = [
            {text: 'id', dataIndex: 'id', width: 50, filter: {xtype: 'textfield'}},
            {text: 'Менеджер', dataIndex: 'create_user_name', width: 200, filter: {xtype: 'textfield'}},
            {text: 'Канал', dataIndex: 'channel_name', width: 100, filter: {xtype: 'textfield'}},
            {text: 'Дата отгрузки', dataIndex: 'date', width: 120, filter: {xtype: 'textfield'}},
            {text: 'Дата создания', dataIndex: 'created', width: 120, filter: {xtype: 'textfield'}},
            {
                xtype        : 'actioncolumn',
                width        : 20,
                menuDisabled : true,
                items        : [
                    {
                        icon    : '/images/extjs/icons/external.png',
                        tooltip : 'Отгрузки',
                        scope   : this,
                        handler : function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            window.open('/#subscription/ship/shipment_id/' + rec.get('id'),'_blank');
                        }
                    },
                    /*{
                        icon    : '/images/extjs/icons/download.png',
                        tooltip : 'Загрузить csv',
                        scope   : this,
                        handler : function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            console.log(rec.get('file_hash'));
                            window.open('/file-storage/download/hash/' + rec.get('file_hash'),'_blank');
                        },
                        getClass: function(value, meta, rec, rowindex)
                        {
                            return rec.get('file_hash') ? '' : 'x-hide-display';
                        }
                    }*/
                ]
            }
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
            } 
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);
    }
});