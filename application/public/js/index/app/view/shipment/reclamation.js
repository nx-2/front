/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.shipment.reclamation' ,{
    extend : 'Ext.grid.Panel',
    alias  : 'widget.nx_shipment_reclamation',
    title  : 'Рекламации',
    initComponent: function() {
        Ext.apply(this, {     
            store: new Ext.data.Store({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/ship-item/reclamation',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    },
                },
                remoteFilter: true,
                fields        : [
                    {name: 'id'},
                    {name: 'shipment_created'},
                    {name: 'channel_name'},
                    {name: 'release_date'},
                    {name: 'issue_name'},
                    {name: 'cancel_date'},
                    {name: 'cancel_user_name'},
                    {name: 'create_user_name'},
                    {name: 'item_release_id'},
                    {name: 'order_id'},
                    {name: 'periodical_name'},
                    {name: 'issue_title'},
                    {name: 'customer_name'},
                    {name: 'zipcode'},
                    {name: 'email'},
                ],
                //autoLoad: true,
            })
        });

        this.columns = [
            {text: 'id', dataIndex: 'id', width: 50, xfilter: true},
            {text: 'id отгр.', dataIndex: 'item_release_id', width: 50, xfilter: true},
            {text: 'Издание', dataIndex: 'periodical_name', width: 70, xfilter: 'periodical_id'},
                {text: 'Выпуск', dataIndex: 'issue_title', width: 60, xfilter: true},
                {text: 'Подписчик', dataIndex: 'customer_name', flex: 1, tdCls: 'nx-multiline', xfilter: true},
                {text: 'Индекс', dataIndex: 'zipcode', width: 50, filter: {xtype: 'textfield'}},  
                {text: 'Email', dataIndex: 'email', width: 150, xfilter: true},  
                {text: 'id заказа', dataIndex: 'order_id', width: 60, xfilter: true},
            {text: 'Дата выхода', dataIndex: 'release_date', width: 120, xfilter: true},
            {text: 'Создал', dataIndex: 'create_user_name', width: 100},
            {text: 'Дата создания', dataIndex: 'shipment_created', width: 120, xfilter: true},
            {text: 'Отменил', dataIndex: 'cancel_user_name', width: 100},
            {text: 'Дата отмены', dataIndex: 'cancel_date', width: 120, xfilter: true},
            {text: 'Канал', dataIndex: 'channel_name', width: 80, xfilter: 'channel_id'},
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
            },
            xfilters : [//filters property already taken up by filters feature 
                {fieldLabel: 'id', name: 'id', xtype: 'textfield'},
                {fieldLabel: 'id отгр.', name: 'item_release_id', xtype: 'textfield'},
                {fieldLabel: 'издание', name: 'periodical_id', xtype: 'combo', store: subscribleMagazinesStore},
                {fieldLabel: 'выпуск', name: 'issue_title', xtype: 'textfield'},
                {fieldLabel: 'подписчик', name: 'customer_name', xtype: 'textfield', minLength: 3},
                {fieldLabel: 'индекс', name: 'zipcode', xtype: 'textfield'},
                {fieldLabel: 'email', name: 'email', xtype: 'textfield', minLength: 3},
                {fieldLabel: 'id заказа', name: 'order_id', xtype: 'textfield'},
                {fieldLabel: 'дата выхода', name: 'release_date', xtype: 'textfield'},
                {fieldLabel: 'создал', name: 'create_user_name', xtype: 'textfield'},
                {fieldLabel: 'дата создания', name: 'shipment_created', xtype: 'textfield'},
                {fieldLabel: 'отменил', name: 'cancel_user_name', xtype: 'textfield'},
                {fieldLabel: 'дата отмены', name: 'cancel_date', xtype: 'textfield'},
                {fieldLabel: 'канал', name: 'channel_id', xtype: 'combo', store: ship_channels_store},
            ]
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);
    }
});