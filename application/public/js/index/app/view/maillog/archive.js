/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.maillog.archive' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_maillog_archive',

    title: 'Архив электронных писем',

    store: 'maillog',/*{
        type      : 'maillog',
        listeners : {
            'beforeload' : function() {
                this.getProxy().extraParams = {
                    from: 'archive'
                }
            }
        }
    },*/

    initComponent: function() {

        this.columns = [
            {header: 'ID',  dataIndex: 'id',  width: 100, filter: {xtype: 'textfield'}},
            {header: 'Email', dataIndex: 'email',  width: 250, filter: {xtype: 'textfield'}},
            {header: 'От', dataIndex: 'from',  width: 250},
            {
                xtype:'actioncolumn',
                width:30,
                items: [
                {
                    icon: '/images/extjs/icons/maillog.gif',
                    tooltip: 'Письмо',
                    scope: this,
                    handler: function(grid, rowIndex, colIndex) 
                    {
                        var rec = grid.getStore().getAt(rowIndex);
                        var mailMessageWindow = new Ext.window.Window(
                        {
                            title    : 'Текст письма',
                            autoShow : true,
                            width    : 800,
                            items    : [
                                {
                                    xtype: 'form',
                                    items: 
                                    [
                                        {
                                            xtype : 'htmleditor',
                                            width : '100%',
                                            height: 500,
                                            name  : 'message'
                                        }
                                    ],
                                    listeners: 
                                    {
                                        'render': function() 
                                        {
                                            this.getForm().load(
                                            {
                                                url     : '/mail-log/get',
                                                waitMsg : 'Загрузка',
                                                scope   : this,
                                                params  : 
                                                {
                                                    id   : rec.get('id'),
                                                    from : 'archive'
                                                }
                                            });
                                        }
                                    }
                                }
                            ]
                        });
                    }
                }]
            },
            {header: 'Тема', dataIndex: 'subject',  flex: 1},
            {header: 'Дата', dataIndex: 'created', width: 120, filter: {xtype: 'textfield'}}
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            } 
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);
    }
});