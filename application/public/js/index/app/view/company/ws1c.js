/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.company.ws1c' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_company_ws1c',
    title: 'Компании 1C',

    initComponent: function() {

        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/company/ws1c',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                //autoLoad : true,
                fields        : [
                    {name: 'id'},
                    {name: 'name'},
                    {name: 'inn'},
                    {name: 'kpp'},
                ]
            })
        });

        this.columns = [
            //{header: 'id', dataIndex: 'id',  width: 50, filter: {xtype: 'textfield'}},
            {xtype:'rownumberer', width: 40},
            {header: 'Название', dataIndex: 'name',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'ИНН', dataIndex: 'inn', width: 120, filter: {xtype: 'textfield'}},
            {header: 'КПП', dataIndex: 'kpp',  width: 120, filter: {xtype: 'textfield'}},
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            } 
        });
    
        Ext.apply(this, {        
            dockedItems: [
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },
                    '->',
                    {
                        text   : 'Обновить',
                        iconCls: 'refresh-ico',
                        scope  : this,
                        handler: function() {
                            Ext.Msg.confirm('Подтверждение', 'Обновить список организаций из 1С?', function(btn){
                                if(btn=='yes')
                                {
                                    this.setLoading(true);
                                    Ext.Ajax.request({
                                        url     : '/company/re-ws1c',
                                        method  : 'POST',
                                        scope   : this,
                                        success : function(response) {
                                            this.setLoading(false);
                                            var response = Ext.JSON.decode(response.responseText);
                                            this.store.reload();
                                        }
                                    });
                                }
                            },this);
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);
    }
});