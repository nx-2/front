/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.company.edit', {
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_company_edit',
    title    : 'Редактирование организации',
    autoShow : true,
    width    : 900,

    initComponent: function() {
        var viewid = this.id;
        if(this.nxparams.clone) {
            this.title =  'Клонирование компании';
        }
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title: 'свойства',
                                layout: 'hbox',
                                padding: 5,
                                items: 
                                [
                                    {
                                        flex: 10,
                                        border: 0,
                                        items: [
                                            {
                                                layout: 'form',
                                                border: 0,
                                                //flex: 10,
                                                items: [
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'id',
                                                        fieldLabel : 'ID',
                                                        submitValue : false,
                                                        readOnly    : true
                                                    },
                                                    {
                                                        xtype          : 'checkbox',
                                                        name           : 'checked',
                                                        fieldLabel     : 'Проверена?',
                                                        inputValue     : '1',
                                                        uncheckedValue : '0'
                                                    },
                                                    {
                                                        xtype          : 'checkbox',
                                                        name           : 'enabled',
                                                        fieldLabel     : 'Включена?',
                                                        inputValue     : '1',
                                                        uncheckedValue : '0'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'org_form',
                                                        fieldLabel : 'Орг.-правовая форма'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'name',
                                                        fieldLabel : 'Название',
                                                        allowBlank : false
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'shortname',
                                                        fieldLabel : 'Короткое название'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'othernames',
                                                        fieldLabel : 'Другие названия'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'wrongnames',
                                                        fieldLabel : 'Неправильные названия'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'email',
                                                        fieldLabel : 'Email',
                                                        vtype      : 'email'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'url',
                                                        fieldLabel : 'Сайт'
                                                    },
                                                    {
                                                        border: 0,
                                                        layout: 'hbox',
                                                        items: [
                                                            {
                                                                xtype  : 'textfield',
                                                                name   : 'contact_person_id',
                                                                id     : 'nx_edit_person_id' + viewid,
                                                                hidden : true,
                                                            },
                                                            {
                                                                xtype      : 'textfield',
                                                                fieldLabel : 'Контактное лицо',
                                                                name       : 'contact_person_name',
                                                                id         : 'nx_edit_person_name' + viewid,
                                                                readOnly   : true,
                                                                flex       : 4
                                                            },
                                                            {
                                                                xtype           : 'nx.ux.JsonCombo',
                                                                emptyText       : 'выбрать физ. лицо',
                                                                url             : '/person/get-objects-for-combo',
                                                                matchFieldWidth : false,
                                                                flex            : 2,
                                                                listeners       : {
                                                                    scope: this,
                                                                    'select' : function(combo) {
                                                                        Ext.getCmp('nx_edit_person_id' + viewid).setValue(combo.getValue());
                                                                        Ext.getCmp('nx_edit_person_name' + viewid).setValue(combo.getRawValue());
                                                                    }
                                                                }
                                                            } 
                                                        ]
                                                    },
                                                    {
                                                        xtype      : 'textareafield',
                                                        name       : 'description',
                                                        fieldLabel : 'Описание',
                                                        growMax    : 200,
                                                        grow       : true
                                                    }
                                                ]
                                            }/*,
                                            {
                                                xtype: 'grid',
                                                title: 'сотрудники',
                                                id: 'company_persons',
                                                store: 'person',
                                                columns: [
                                                    {
                                                        xtype:'actioncolumn',
                                                        width:50,
                                                        items: [
                                                        {
                                                            icon: '/images/extjs/icons/edit.png',
                                                            tooltip: 'Редактировать',
                                                            scope: this,
                                                            handler: function(grid, rowIndex, colIndex) 
                                                            {
                                                                var rec = grid.getStore().getAt(rowIndex);
                                                                this.fireEvent('nx_person_edit', this, rec);
                                                            }
                                                        }]
                                                    },
                                                    {header: 'id',  dataIndex: 'id',  flex: 1},
                                                    {header: 'Имя',  dataIndex: 'name',  flex: 1}//,
                                                    //{
                                                    //    header: 'контакт. лицо?', flex: 1, renderer: function(val)
                                                    //   {
                                                    //        return Ext.String.format("<input type='radio' name='rowSel' value={0} />", val);
                                                    //    }
                                                    //}
                                                ],
                                                listeners: {
                                                    'itemclick': function()
                                                    {
                                                        console.log('test');
                                                    }
                                                }     
                                            }*/
                                        ]
                                    },
                                    {
                                        layout   : 'form',
                                        border   : 0,
                                        flex     : 6,
                                        margins  : '0 0 0 25',
                                        defaults : {
                                            labelAlign: 'right'
                                        },
                                        items    : [
                                            {
                                                xtype       : 'textfield',
                                                name        : 'create_user_name',
                                                fieldLabel  : 'завел',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'created',
                                                fieldLabel  : 'внесение',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_user_name',
                                                fieldLabel  : 'изменил',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_updated',
                                                fieldLabel  : 'правка',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title   : 'реквизиты',
                                layout  : 'form',
                                padding : 5,
                                items   : [
                                    {
                                        xtype      : 'textfield',
                                        name       : 'inn',
                                        fieldLabel : 'ИНН'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'kpp',
                                        fieldLabel : 'КПП'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'okonh',
                                        fieldLabel : 'ОКОНХ'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'okpo',
                                        fieldLabel : 'ОКПО'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'pay_account',
                                        fieldLabel : 'Рассчетный счет'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'bank',
                                        fieldLabel : 'Банк'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'corr_account',
                                        fieldLabel : 'Корр. счет'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'bik',
                                        fieldLabel : 'БИК'
                                    }
                                ]
                            },
                            {
                                title   : 'адрес',
                                layout  : 'form',
                                padding : 5,
                                items   : [
                                    {
                                        xtype          : 'combo',
                                        name           : 'country_id',
                                        store          : countries_store,
                                        fieldLabel     : 'Страна',
                                        forceSelection : true,
                                        editable       : false,
                                        queryMode      : 'local',
                                        triggerAction  : 'all'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'area',
                                        fieldLabel : 'Область'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'region',
                                        fieldLabel : 'Район'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'city',
                                        fieldLabel : 'Город, село'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'zipcode',
                                        fieldLabel : 'Индекс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'address',
                                        fieldLabel : 'Адрес'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'telcode',
                                        fieldLabel : 'Тел. код'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'phone',
                                        fieldLabel : 'Телефон'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'fax',
                                        fieldLabel : 'Факс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'comment',
                                        fieldLabel : 'Комментарий'
                                    }
                                ]
                            },
                            {
                                title   : 'юридический адрес',
                                layout  : 'form',
                                padding : 5,
                                items   : [
                                    {
                                        xtype          : 'combo',
                                        name           : 'la_country_id',
                                        store          : countries_store,
                                        fieldLabel     : 'Страна',
                                        forceSelection : true,
                                        editable       : false,
                                        queryMode      : 'local',
                                        triggerAction  : 'all'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_area',
                                        fieldLabel : 'Область'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_region',
                                        fieldLabel : 'Район'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_city',
                                        fieldLabel : 'Город, село'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_zipcode',
                                        fieldLabel : 'Индекс',
                                        maxLength  : 6,
                                        minLength  : 6
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_address',
                                        fieldLabel : 'Адрес'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_telcode',
                                        fieldLabel : 'Тел. код'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_address_phone',
                                        fieldLabel : 'Телефон'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_fax',
                                        fieldLabel : 'Факс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_address_comment',
                                        fieldLabel : 'Комментарий'
                                    }
                                ]
                            }
                        ]
                    }
                ],
                listeners: {
                    scope: this,
                    'actioncomplete' : function(form, action) {
                        if(action.result.data)
                        {
                            if(this.nxparams.clone) {
                                this.down('textfield[name="name"]').setValue('Копия ' + this.down('textfield[name="name"]').getValue());
                            }
                        }
                    }
                }
            }
        ];

        this.buttons = [
            {
                text   : this.nxparams.clone ? 'Клонировать' : 'Сохранить',
                action : this.nxparams.clone ? 'clone' : 'save'
            },
            {
                text    : 'Закрыть',
                action  : 'close',
                scope   : this,
                handler : this.close
            }
        ];

        this.callParent(arguments);
    }
});