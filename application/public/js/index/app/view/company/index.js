/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.company.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_company_list',

    title: 'Компании',

    store: 'company',

    initComponent: function() {

        this.actionsmenu = Ext.create('Ext.menu.Menu', {
            showSeparator : false,
            cls           : 'no-icon-menu',
            items         : [
                {
                    //iconCls : 'x-hide-display',
                    text    : 'Клонировать',
                    tooltip : 'клонировать',
                    scope   : this,
                    handler : function(btn) {
                        this.fireEvent('nx_company_clone', this, this.actionsmenu.rec);
                    }
                },
            ]
        });

        this.columns = [
            {
                xtype:'actioncolumn',
                width:50,
                items: [
                    {
                        icon: '/images/extjs/icons/edit.png',
                        tooltip: 'Редактировать',
                        scope: this,
                        handler: function(grid, rowIndex, colIndex) 
                        {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.fireEvent('nx_company_edit', this, rec);
                            //Ext.History.fireEvent('change', 'users/edit/id/' + rec.get('id'), {rec: rec});
                            //Ext.History.add('company/edit/id/' + rec.get('id'));
                        }
                    },
                    {
                        icon    : '/images/extjs/icons/expand.png',
                        tooltip : 'действия',
                        scope   : this,
                        handler : function(grid, rowIndex, colIndex, item, e) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.actionsmenu.rec = rec;
                            this.actionsmenu.showAt(e.getXY());
                        }
                    }]
            },
            {text: 'id', dataIndex: 'id',  width: 50, filter: {xtype: 'textfield'}},
            {text: 'Название', dataIndex: 'name', flex: 1, filter: {xtype: 'textfield'}},
            {text: 'ОПФ', dataIndex: 'org_form', width: 70, filter: {xtype: 'textfield'}},
            {text: 'ИНН', dataIndex: 'inn', width: 100, filter: {xtype: 'textfield'}},
            {text: 'КПП', dataIndex: 'kpp', width: 100, filter: {xtype: 'textfield'}},
            {text: 'Коротко', dataIndex: 'shortname', width: 100, filter: {xtype: 'textfield'}},
            {text: 'Email', dataIndex: 'email', width: 200, filter: {xtype: 'textfield'}},
            {text: 'Сайт', dataIndex: 'url', width: 200, filter: {xtype: 'textfield'}},
            {text: 'Проверена', dataIndex: 'checked', hidden: true, width: 70, filter: {xtype: 'textfield'}},
            {text: '1C статус', dataIndex: 'ws1c_synced', hidden: true, width: 70, filter: {xtype: 'textfield'}},
            {text: 'Дата создания', dataIndex: 'created', width: 120, filter: {xtype: 'textfield'}}
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    var css_class = '';
                    css_class += rec.get('enabled') == 0 ? ' nx-row-disabled' : ''; 
                    return css_class;
                } 
            },
            allowDeselect: true,
            selModel: {
                mode: 'MULTI'
            },
        });
    
        Ext.apply(this, {
                   
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },
                    '-',
                    {
                        text    : 'Сбросить 1C',
                        icon    : '/images/extjs/icons/refresh.png',
                        scope   : this,
                        hidden  : !nx.app.isperm('ship_manager') && !nx.app.isperm('subscribe_manager'),
                        handler : function(button) {
                            var selection = this.getSelectionModel().getSelection();  
                            if(selection.length)
                            {
                                var ids = [];
                                Ext.each(selection, function(row) {
                                    ids.push(row.get('id'));
                                });
                                Ext.Msg.confirm('Подтверждение','Сбросить статус отправки в 1С (' + ids.length + ')?', function(btn){
                                    if(btn=='yes')
                                    {
                                        button.setLoading(true);
                                        Ext.Ajax.request({
                                            url     : '/company/reset-ws1c',
                                            method  : 'POST',
                                            scope   : this,
                                            params  : {
                                                id : ids.join(',')
                                            },
                                            success : function(response) {
                                                button.setLoading(false);
                                                this.getStore().reload();
                                            }
                                        });
                                    }
                                },this);  
                            }
                            else {
                                Ext.Msg.alert('Ошибка', 'Не выбраны организации');
                            }
                        }
                    },
                    {
                        text    : 'Отправить в 1С',
                        icon    : '/images/extjs/icons/1c.gif',
                        scope   : this,
                        hidden  : !nx.app.isperm('ship_manager') && !nx.app.isperm('subscribe_manager'),
                        handler : function(button) {
                            this.fireEvent('nx_company_send_ws1c', button);
                        }
                    },
                    '->',
                    {
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        handler : function() {
                            this.fireEvent('nx_company_add', this);
                        }
                    }
                ]
            }],
        });

        this.addEvents('nx_company_edit');
        this.addEvents('nx_company_add');
        this.callParent(arguments);
    },
    /*show1CResult: function(data){
        new Ext.window.Window({
            title     : 'Передача организаций в 1С',
            //iconCls   : 'payment-ico',
            layout    : 'fit',
            border    : false,
            resizable : true,
            autoShow  : true, 
            items     : [
                {
                    xtype : 'grid',
                    width : 500,
                    height: 500,
                    autoShow: true,
                    store: new Ext.data.JsonStore({
                        proxy: {
                            type: 'memory',
                            reader: {
                                type: 'json',
                                root: 'items'
                            }
                        },
                        data         : {'items' : data},
                        pageSize     : 9999,
                        autoLoad     : true,
                        fields       : [
                            {name: 'id'},
                            {name: 'result'}
                        ]
                    }),
                    columns    : [
                        {xtype: 'rownumberer', width: 50},
                        {text: 'id', dataIndex: 'id', width: 70},
                        {text: 'комментарий', dataIndex: 'result', flex: 1, tdCls: 'nx-multiline'},
                    ],
                }
            ], 
            buttons : [
                {
                    text: 'Закрыть',
                    handler: function() {
                        this.up('window').close();
                    }
                }
            ]
        });
    },*/
});