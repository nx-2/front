/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.company.add', {
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_company_add',
    title    : 'Добавление организации',
    autoShow : true,
    width    : 700,

    initComponent: function() {
        var viewid = this.id;
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title   : 'свойства',
                                padding : 5,
                                layout  : 'form',
                                border  : 0,
                                items   : [
                                    {
                                        xtype          : 'checkbox',
                                        name           : 'checked',
                                        fieldLabel     : 'Проверена?',
                                        inputValue     : '1',
                                        uncheckedValue : '0'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'org_form',
                                        fieldLabel : 'Орг.-правовая форма'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'name',
                                        fieldLabel : 'Название',
                                        allowBlank : false
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'shortname',
                                        fieldLabel : 'Короткое название'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'othernames',
                                        fieldLabel : 'Другие названия'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'wrongnames',
                                        fieldLabel : 'Неправильные названия'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'email',
                                        fieldLabel : 'Email',
                                        vtype      : 'email'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'url',
                                        fieldLabel : 'Сайт'
                                    },
                                    {
                                        border : 0,
                                        layout : 'hbox',
                                        items  : [
                                            {
                                                xtype  : 'textfield',
                                                name   : 'contact_person_id',
                                                id     : 'nx_add_person_id' + viewid,
                                                hidden : true,
                                            },
                                            {
                                                xtype      : 'textfield',
                                                fieldLabel : 'Контактное лицо',
                                                name       : 'contact_person_name',
                                                id         : 'nx_add_person_name' + viewid,
                                                readOnly   : true,
                                                flex       : 4
                                            },
                                            {
                                                xtype           : 'nx.ux.JsonCombo',
                                                emptyText       : 'выбрать физ. лицо',
                                                url             : '/person/get-objects-for-combo',
                                                matchFieldWidth : false,
                                                flex            : 2,
                                                listeners       : {
                                                    scope: this,
                                                    'select' : function(combo) {
                                                        Ext.getCmp('nx_add_person_id' + viewid).setValue(combo.getValue());
                                                        Ext.getCmp('nx_add_person_name' + viewid).setValue(combo.getRawValue());
                                                    }
                                                }
                                            } 
                                        ]
                                    },
                                    {
                                        xtype      : 'textareafield',
                                        name       : 'description',
                                        fieldLabel : 'Описание',
                                        growMax    : 200,
                                        grow       : true
                                    }
                                ]
                            },
                            {
                                title   : 'реквизиты',
                                layout  : 'form',
                                padding : 5,
                                items   : [
                                    {
                                        xtype      : 'textfield',
                                        name       : 'inn',
                                        fieldLabel : 'ИНН'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'kpp',
                                        fieldLabel : 'КПП'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'okonh',
                                        fieldLabel : 'ОКОНХ'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'okpo',
                                        fieldLabel : 'ОКПО'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'pay_account',
                                        fieldLabel : 'Рассчетный счет'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'bank',
                                        fieldLabel : 'Банк'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'corr_account',
                                        fieldLabel : 'Корр. счет'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'bik',
                                        fieldLabel : 'БИК'
                                    }
                                ]
                            },
                            {
                                title   : 'адрес',
                                layout  : 'form',
                                padding : 5,
                                items   : [
                                    {
                                        xtype          : 'combo',
                                        name           : 'country_id',
                                        store          : countries_store,
                                        fieldLabel     : 'Страна',
                                        forceSelection : true,
                                        editable       : false,
                                        queryMode      : 'local',
                                        triggerAction  : 'all'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'area',
                                        fieldLabel : 'Область'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'region',
                                        fieldLabel : 'Район'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'city',
                                        fieldLabel : 'Город, село'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'zipcode',
                                        fieldLabel : 'Индекс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'address',
                                        fieldLabel : 'Адрес'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'telcode',
                                        fieldLabel : 'Тел. код'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'phone',
                                        fieldLabel : 'Телефон'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'fax',
                                        fieldLabel : 'Факс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'comment',
                                        fieldLabel : 'Комментарий'
                                    }
                                ]
                            },
                            {
                                title   : 'юридический адрес',
                                layout  : 'form',
                                padding : 5,
                                items   : [
                                    {
                                        xtype          : 'combo',
                                        name           : 'la_country_id',
                                        store          : countries_store,
                                        fieldLabel     : 'Страна',
                                        forceSelection : true,
                                        editable       : false,
                                        queryMode      : 'local',
                                        triggerAction  : 'all'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_area',
                                        fieldLabel : 'Область'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_region',
                                        fieldLabel : 'Район'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_city',
                                        fieldLabel : 'Город, село'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_zipcode',
                                        fieldLabel : 'Индекс',
                                        maxLength  : 6,
                                        minLength  : 6
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_address',
                                        fieldLabel : 'Адрес'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_telcode',
                                        fieldLabel : 'Тел. код'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_address_phone',
                                        fieldLabel : 'Телефон'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_fax',
                                        fieldLabel : 'Факс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'la_address_comment',
                                        fieldLabel : 'Комментарий'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Добавить',
                action: 'add'
            },
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});