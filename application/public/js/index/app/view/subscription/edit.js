/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.edit', {
    extend    : 'Ext.window.Window',
    alias     : 'widget.nx_subscription_edit',
    init_title: 'Редактирование подписки',
    autoShow  : true,
    width     : 900,
    resizable : true,

    initComponent: function() {
        if(this.nxparams.extend) {
            this.title =  'Продление подписки';
        }

        this.items = [
            {
                xtype: 'form',
                trackResetOnLoad: true,
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title   : 'свойства',
                                layout  : 'hbox',
                                padding : 5,
                                items   : 
                                [
                                    {
                                        flex   : 10,
                                        border : 0,
                                        items  : [
                                            {
                                                layout : 'form',
                                                border : 0,
                                                //flex : 10,
                                                items  : [
                                                    {
                                                        xtype       : 'textfield',
                                                        name        : 'id',
                                                        fieldLabel  : 'ID',
                                                        fieldStyle  : 'border: 0; background: none;',
                                                        submitValue : false,
                                                        readOnly    : true
                                                        //disabled   : true
                                                    },
                                                    {
                                                        xtype            : 'combobox',
                                                        //displayField   : '',
                                                        name             : 'order_state_id',
                                                        store            : orderStatesStore,
                                                        fieldLabel       : 'Статус',
                                                        editable         : false,
                                                        queryMode        : 'local',
                                                        //forceSelection : true,
                                                        triggerAction    : 'all',
                                                        //selectOnFocus  : true
                                                        listeners     : {
                                                            'beforeselect' : function(combo, record, index) 
                                                            {
                                                                if(record.data.field1=='3')
                                                                {
                                                                    Ext.Msg.alert('Вы уверены?','Если подписка включена, имеет способ отправки pdf, то статус "оплачено" приводит к автоматической отправке pdf подписок.');
                                                                }
                                                            }
                                                        }
                                                    },
                                                    {
                                                        xtype            : 'combobox',
                                                        name             : 'payment_type_id',
                                                        store            : payment_types_store,
                                                        fieldLabel       : 'Способ оплаты',
                                                        editable         : false,
                                                        queryMode        : 'local',
                                                        triggerAction    : 'all'
                                                    },
                                                    {
                                                        xtype      : 'textarea',
                                                        name       : 'comment',
                                                        fieldLabel : 'Комментарий',
                                                        grow       : true,
                                                        growMin    : 0,
                                                        growMax    : 200
                                                    },
                                                    /*{
                                                        xtype          : 'checkbox',
                                                        name           : 'enabled',
                                                        fieldLabel     : 'Включена?',
                                                        inputValue     : '1',
                                                        uncheckedValue : '0'
                                                    },*/
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'email',
                                                        fieldLabel : 'Email',
                                                        vtype      : 'email'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'extra_emails',
                                                        fieldLabel : 'Дополнительные Email (через ;)',
                                                        vtype      : 'emails'
                                                    },
                                                    {
                                                        xtype      : 'datefield',
                                                        name       : 'payment_date',
                                                        fieldLabel : 'Дата оплаты',
                                                        format     : 'Y-m-d',
                                                        editable   : false
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'payment_docno',
                                                        fieldLabel : 'Номер док. оплаты'
                                                    },
                                                    /*{
                                                        xtype      : 'textfield',
                                                        name       : 'action_code',
                                                        fieldLabel : 'Промокод',
                                                        fieldStyle : 'border: 0; background: none;',
                                                        readOnly   : true
                                                    },*/
                                                    {
                                                        border: 0,
                                                        layout: 'hbox',
                                                        items: [
                                                            {
                                                                xtype  : 'textfield',
                                                                hidden : true,
                                                                name   : 'person_id',
                                                                itemId : 'nx_edit_person_id'
                                                            },
                                                            {
                                                                xtype      : 'textfield',
                                                                fieldLabel : 'Физ. лицо',
                                                                name       : 'person_id_name',
                                                                itemId     : 'nx_edit_person_name',
                                                                readOnly   : true,
                                                                flex       : 4
                                                            },
                                                            {
                                                                xtype           : 'nx.ux.JsonCombo',
                                                                flex            : 2,
                                                                emptyText       : 'выбрать физ. лицо',
                                                                url             : '/person/get-objects-for-combo',
                                                                matchFieldWidth : false,
                                                                listeners       : {
                                                                    scope: this,
                                                                    'select' : function(combo) {
                                                                        this.down('#nx_edit_person_id').setValue(combo.getValue());
                                                                        this.down('#nx_edit_person_name').setValue(combo.getRawValue());
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        border: 0,
                                                        layout: 'hbox',
                                                        items: [
                                                            {
                                                                xtype  : 'textfield',
                                                                name   : 'company_id',
                                                                itemId : 'nx_edit_company_id',
                                                                hidden : true,
                                                            },
                                                            {
                                                                xtype      : 'textfield',
                                                                fieldLabel : 'Организация',
                                                                name       : 'company_id_name',
                                                                itemId     : 'nx_edit_company_name',
                                                                readOnly   : true,
                                                                flex       : 4
                                                            },
                                                            {
                                                                xtype           : 'nx.ux.JsonCombo',
                                                                emptyText       : 'выбрать организацию',
                                                                url             : '/company/get-objects-for-combo',
                                                                matchFieldWidth : false,
                                                                flex            : 2,
                                                                listeners       : {
                                                                    scope: this,
                                                                    'select' : function(combo) {
                                                                        this.down('#nx_edit_company_id').setValue(combo.getValue());
                                                                        this.down('#nx_edit_company_name').setValue(combo.getRawValue());
                                                                        //if(combo.getValue()) {
                                                                            this.fireEvent('nx_order_form_refresh_ui', this, this.down('form').getForm().getValues());
                                                                        //}
                                                                    }
                                                                }
                                                            } 
                                                        ]
                                                    },
                                                    {
                                                        border: 0,
                                                        layout: 'hbox',
                                                        items: [
                                                            {
                                                                xtype  : 'textfield',
                                                                hidden : true,
                                                                name   : 'user_id',
                                                                itemId : 'nx_edit_user_id'
                                                            },
                                                            {
                                                                xtype      : 'textfield',
                                                                fieldLabel : 'OSP юзер',
                                                                name       : 'user_name',
                                                                itemId     : 'nx_edit_user_name',
                                                                readOnly   : true,
                                                                flex       : 4
                                                            },
                                                            {
                                                                xtype           : 'nx.ux.JsonCombo',
                                                                flex            : 2,
                                                                emptyText       : 'выбрать OSP юзера',
                                                                url             : '/user/get-objects-for-combo',
                                                                matchFieldWidth : false,
                                                                listeners       : {
                                                                    scope: this,
                                                                    'select' : function(combo) {
                                                                        this.down('#nx_edit_user_id').setValue(combo.getValue());
                                                                        this.down('#nx_edit_user_name').setValue(combo.getRawValue());
                                                                    }
                                                                }
                                                            },
                                                            {
                                                                xtype: 'button',
                                                                iconCls: 'search-ico',
                                                                tooltip: 'автопоиск по email',
                                                                handler: function() {
                                                                    var combo   = this.prev('combo');
                                                                    var keyword = this.up('form').getForm().getValues().email;
                                                                    //combo.getStore().getProxy().extraParams = {test: 1};
                                                                    combo.doQuery(keyword);
                                                                }
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        //hidden       : !nx.app.isperm('admin'),
                                                        xtype        : 'comboboxselect',
                                                        name         : 'tags[]',
                                                        fieldLabel   : 'Теги',
                                                        displayField : 'name',
                                                        valueField   : 'id',
                                                        itemId       : 'nx_edit_tags',
                                                        minChars     : 2,
                                                        //grow         : false,
                                                        //queryMode    : 'remote',
                                                        store        : new Ext.data.JsonStore({
                                                            proxy: {
                                                                type: 'ajax',
                                                                url : '/tag/get-objects-for-combo',
                                                                reader: {
                                                                    type            : 'json',
                                                                    root            : 'objects',
                                                                    successProperty : 'success',
                                                                    totalProperty   : 'total'
                                                                },
                                                            },
                                                            root       : 'objects',
                                                            fields     : [
                                                                {name : 'id',   type : 'int'},
                                                                {name : 'name', type : 'string'}
                                                            ],
                                                        })
                                                    },
                                                    {
                                                        xtype    : 'nx_comment_list',
                                                        entity_id: 1,
                                                        row_id   : this.nxparams.id,
                                                        hidden   : !nx.app.isperm('admin')
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        layout   : 'form',
                                        border   : 0,
                                        flex     : 6,
                                        margins  : '0 0 0 25',
                                        defaults : {
                                            labelAlign: 'right'
                                        },
                                        items: [
                                            {
                                                xtype         : 'combo',
                                                name          : 'new_state',
                                                store         : [['0', 'не указано'],['-1', 'не новый'],['1', 'новый']],
                                                fieldLabel    : 'новый?',
                                                editable      : false,
                                                queryMode     : 'local',
                                                triggerAction : 'all',
                                                hidden        : !nx.app.isperm('admin')
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'xpress_id',
                                                fieldLabel  : 'xpress ID',
                                                vtype       : 'Numeric'
                                                //fieldStyle  : 'border: 0; background: none;',
                                                //submitValue : false,
                                                //readOnly    : true
                                            },
                                            {
                                                xtype         : 'combo',
                                                name          : 'type',
                                                store         : subscription_type_store,
                                                fieldLabel    : 'тип',
                                                editable      : false,
                                                queryMode     : 'local',
                                                triggerAction : 'all',
                                                listeners     : {
                                                    'beforeselect' : function(combo, record, index) 
                                                    {
                                                        if(record.data.field1!='0')
                                                        {
                                                            Ext.Msg.alert('Вы уверены?','Если подписка включена, имеет способ отправки pdf, то тип "' + record.data.field2 + '" приводит к автоматической отправке pdf подписок.');
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'price',
                                                fieldLabel  : 'стоимость',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'payment_sum',
                                                fieldLabel  : 'оплачено',
                                                vtype       : 'Numeric'
                                            },
                                            {
                                                xtype         : 'combobox',
                                                name          : 'payment_agent_id',
                                                store         : payment_agents_store,
                                                fieldLabel    : 'оплата через',
                                                editable      : false,
                                                queryMode     : 'local',
                                                triggerAction : 'all',
                                                tpl           : new Ext.XTemplate(
                                                    '<tpl for="."><tpl if="this.isAllowedValue(field1)"><div class="x-boundlist-item">{field2}</div><tpl else><div class="x-boundlist-item x-hidden">{field2}</div></tpl></tpl>',
                                                    {
                                                        isAllowedValue: function(value) 
                                                        {
                                                            var allowed  = [0,1,2,47];
                                                            var is_allow = 0;
                                                            for (var i = allowed.length - 1; i >= 0; i--) {
                                                                if(value == allowed[i]) {
                                                                    is_allow = 1;
                                                                }
                                                            };
                                                            return is_allow;
                                                        }
                                                    }
                                                ),
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'action_code',
                                                fieldLabel  : 'промокод',
                                                fieldStyle  : 'border: 0; background: none;',
                                                //submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'action_id',
                                                //submitValue : false,
                                                hidden      : true
                                            },
                                            {
                                                xtype       : 'textarea',
                                                name        : 'action_id_name',
                                                fieldLabel  : 'акция',
                                                fieldStyle  : 'border: 0; background: none;',
                                                grow        : true,
                                                growMin     : 0,
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                border : 0,
                                                layout : 'hbox',
                                                items  : [
                                                    {
                                                        xtype         : 'combo',
                                                        name          : 'manager_id',
                                                        store         : Ext.create('Ext.data.Store', {fields : ['id', 'name', 'email'], data : managers_data}),
                                                        fieldLabel    : 'менеджер',
                                                        editable      : false,
                                                        queryMode     : 'local',
                                                        triggerAction : 'all',
                                                        labelAlign    : 'right',
                                                        flex          : 1,
                                                        displayField  : 'name',
                                                        valueField    : 'id'
                                                    },
                                                    {
                                                        xtype   : 'button',
                                                        icon    : '/images/extjs/icons/mail.gif',
                                                        //iconCls : 'search-ico',
                                                        tooltip : 'отправить письмо',
                                                        scope   : this,
                                                        handler : function(btn) {
                                                            var combo = btn.prev('combo');
                                                            var id    = combo.getValue();
                                                            var rec   = combo.getStore().getById(id);
                                                            if(rec && rec.get('email'))
                                                            {
                                                                Ext.widget('nx_ux_mailform', {
                                                                    subject     : 'NX - Подписка #' + this.nxparams.id,
                                                                    message     : 'Подписка #' + this.nxparams.id,
                                                                    title       : 'Отправка письма по подписке #' + this.nxparams.id + ' для менеджера ' + rec.get('name'),
                                                                    log_message : 'Отправка письма по подписке #' + this.nxparams.id + ' для менеджера ' + rec.get('name'),
                                                                    // order_id    : this.nxparams.id,
                                                                    entity_name : 'order',
                                                                    email       : rec.get('email'),
                                                                });
                                                            }
                                                            else
                                                            {
                                                                Ext.Msg.alert('Ошибка', 'Выберите менеджера, либо не заполнен email.');
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'create_user_name',
                                                fieldLabel  : 'завел',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'created',
                                                fieldLabel  : 'внесение',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_user_name',
                                                fieldLabel  : 'изменил',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_updated',
                                                fieldLabel  : 'правка',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title: 'Адрес доставки',
                                layout: 'hbox',
                                padding: 5,
                                items: [
                                    {
                                        layout  : 'form',
                                        itemId  : 'addressLeftForm',
                                        flex    : 1,
                                        border  : 0,
                                        padding : 0,
                                        tbar    : [
                                            '->',
                                            {
                                                xtype : 'button',
                                                text  : 'разобрать адрес >',
                                                //enableToggle : true,
                                                //border       : 1,
                                                scope: this,
                                                handler      : function() {
                                                    var fields = {};
                                                    Ext.each(this.down('#addressLeftForm').items.items, function(item){
                                                        fields[item.name] = item.value;
                                                    });
                                                    Ext.Ajax.request({
                                                        url     : '/address/parse',
                                                        method  : 'POST',
                                                        scope   : this,
                                                        params  : fields,
                                                        success : function(response) {
                                                            var response = Ext.JSON.decode(response.responseText);
                                                            //var addrform = this.down('#addressRightForm');
                                                            if(response.data.length == 1)
                                                            {
                                                                var addr = response.data[0];
                                                                this.mapAddrToFields(addr);
                                                                //if(this.down('textfield[name="map_house"]').getValue()=='') {
                                                                //    Ext.Msg.alert('Предупреждение', 'Адрес разобран не полностью!');
                                                                //}
                                                            }
                                                            else if(response.data.length > 1)
                                                            {
                                                                this.showAddressList(response.data);
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        ],
                                        items   : [
                                            {
                                                xtype         : 'combo',
                                                name          : 'country_id',
                                                store         : countries_store,
                                                fieldLabel    : 'Страна',
                                                editable      : false,
                                                queryMode     : 'local',
                                                triggerAction : 'all'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'area',
                                                fieldLabel : 'Область'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'region',
                                                fieldLabel : 'Район'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'city',
                                                fieldLabel : 'Город, село'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'city1',
                                                fieldLabel : 'Населенный пункт'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'zipcode',
                                                fieldLabel : 'Индекс'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'address',
                                                fieldLabel : 'Адрес'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'street',
                                                fieldLabel : 'Улица',
                                                readOnly   : true,
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'house',
                                                fieldLabel : 'Дом',
                                                readOnly   : true,
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'building',
                                                fieldLabel : 'Корпус',
                                                readOnly   : true,
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'structure',
                                                fieldLabel : 'Строение',
                                                readOnly   : true,
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'apartment',
                                                fieldLabel : 'Квартира',
                                                readOnly   : true,
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'telcode',
                                                fieldLabel : 'Тел. код'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'phone',
                                                fieldLabel : 'Телефон'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                name       : 'fax',
                                                fieldLabel : 'Факс'
                                            },
                                            {
                                                xtype      : 'textarea',
                                                name       : 'address_comment',
                                                fieldLabel : 'Комментарий',
                                                grow       : true,
                                                growMin    : 0
                                            }
                                        ]
                                    },
                                    {
                                        flex    : 1,            
                                        border  : 0,
                                        padding : 0,
                                        margin  : '0 0 0 10px',
                                        items   : [
                                            {
                                                title       : 'адрес по КЛАДР',
                                                itemId      : 'addressRightForm',
                                                bodyPadding : 5,
                                                border      : 1,
                                                layout      : 'form',
                                                defaults    : {
                                                    labelWidth: 130,
                                                    listeners: {
                                                        'dirtychange': function(el, isDirty) {
                                                            if(isDirty)
                                                            {
                                                                el.setFieldStyle({borderColor: '#eeee14'});
                                                            }
                                                            else
                                                            {
                                                                el.setFieldStyle({borderColor: ''});
                                                            }
                                                        },
                                                        'focus': function(el){
                                                            el.setFieldStyle({borderColor: ''});
                                                        }
                                                    }
                                                },
                                                items: [
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_zipcode',
                                                        fieldLabel : 'Индекс'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_area',
                                                        fieldLabel : 'Регион'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_region',
                                                        fieldLabel : 'Район'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_city',
                                                        fieldLabel : 'Населенный пункт'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_city1',
                                                        fieldLabel : 'Населенный пункт1'
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_street',
                                                        fieldLabel : 'Улица',
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_house',
                                                        fieldLabel : 'Дом',
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_building',
                                                        fieldLabel : 'Корпус',
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_structure',
                                                        fieldLabel : 'Строение',
                                                    },
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_apartment',
                                                        fieldLabel : 'Квартира',
                                                    }
                                                ]
                                            },
                                            {
                                                //title       : '',
                                                bodyPadding : 5,
                                                border      : 1,
                                                margin      : '10px 0 0 0',
                                                layout      : 'form',
                                                items       : [
                                                    {
                                                        xtype      : 'textfield',
                                                        name       : 'map_post_box',
                                                        vtype      : 'Numeric',
                                                        fieldLabel : 'а/я'
                                                    },
                                                    {
                                                        xtype          : 'checkbox',
                                                        name           : 'map_is_onclaim',
                                                        fieldLabel     : 'до востребования?',
                                                        labelWidth     : 130,
                                                        inputValue     : '1',
                                                        uncheckedValue : '0'
                                                    },
                                                    {
                                                        xtype          : 'checkbox',
                                                        name           : 'map_is_enveloped',
                                                        fieldLabel     : 'конвертом, не в МАП?',
                                                        inputValue     : '1',
                                                        uncheckedValue : '0'
                                                    },
                                                    {
                                                        xtype      : 'textarea',
                                                        name       : 'map_address_comment',
                                                        fieldLabel : 'Примечание',
                                                        grow       : true,
                                                        growMin    : 0
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title   : 'Издания',
                                padding : 5,
                                items   : [
                                    {
                                        xtype           : 'subscription_magazines_editor',
                                        itemId          : 'subscriptionMagazinesEditor',
                                        subscription_id : this.nxparams.id,
                                        border          : 0,
                                        view            : this
                                    }
                                ]
                            },
                            {
                                title   : 'Документы',
                                border  : 0,
                                padding : 5,
                                items   : [
                                    {
                                        xtype   : 'fieldset',
                                        padding : 5,
                                        title   : 'NX',
                                        items   : [
                                            {
                                                itemId: 'subscriptionPaydocLink',
                                                border: 0
                                            }
                                        ]
                                    },
                                    {
                                        xtype   : 'fieldset',
                                        itemId  : 'subscriptionDoc_1c',
                                        hidden  : !nx.app.isperm('ship_manager'),//nx.config.perm.is_admin ? 0 : 1,
                                        padding : 5,
                                        title   : '1C',
                                        layout  : {type:'hbox'},
                                        items   : [
                                            {
                                                border : 0,
                                                items  : [
                                                    {
                                                        xtype  : 'linkbutton',
                                                        text   : 'счет',
                                                        handler: function(){
                                                            this.setLoading(true);
                                                            Ext.Ajax.request({
                                                                url     : '/subscription/get-bill',
                                                                method  : 'POST',
                                                                scope   : this,
                                                                params  : {
                                                                    id : this.up('window').nxparams.id
                                                                },
                                                                success : function(response) {
                                                                    this.setLoading(false);
                                                                    var id = this.up('window').nxparams.id;
                                                                    var response = Ext.JSON.decode(response.responseText);
                                                                    var isok = response.result[id] != undefined && !response.result[id]['error'];
                                                                    if(isok) {
                                                                        window.location = '/file-storage/download/hash/' + response.result[id]['bill_file_hash'];
                                                                    }
                                                                    else {
                                                                        Ext.Msg.alert('Сообщение', 'Ошибка при получении счета из 1С. ' + response.result[id]['error']); 
                                                                    }
                                                                    //console.log(response);
                                                                }
                                                            }, this);
                                                        }
                                                    },
                                                    {html: '', border:0},
                                                    {
                                                        xtype  : 'linkbutton',
                                                        text   : 'счет с печатью',
                                                        handler: function(){
                                                            this.setLoading(true);
                                                            Ext.Ajax.request({
                                                                url     : '/subscription/get-bill',
                                                                method  : 'POST',
                                                                scope   : this,
                                                                params  : {
                                                                    id    : this.up('window').nxparams.id,
                                                                    signed: true
                                                                },
                                                                success : function(response) {
                                                                    this.setLoading(false);
                                                                    var id = this.up('window').nxparams.id;
                                                                    var response = Ext.JSON.decode(response.responseText);
                                                                    var isok = response.result[id] != undefined && !response.result[id]['error'];
                                                                    if(isok) {
                                                                        window.location = '/file-storage/download/hash/' + response.result[id]['bill_file_hash'];
                                                                    }
                                                                    else {
                                                                        Ext.Msg.alert('Сообщение', 'Ошибка при получении счета из 1С. ' + response.result[id]['error']); 
                                                                    }
                                                                    //console.log(response);
                                                                }
                                                            }, this);
                                                        }
                                                    },
                                                ]
                                            },
                                            {
                                                xtype:'tbspacer',
                                                flex:1
                                            },
                                            {
                                                xtype     : 'grid',
                                                itemId    : 'nx_items_docs_grid',
                                                width     : 300, 
                                                maxHeight : 300,
                                                store     : new Ext.data.JsonStore({
                                                    proxy: {
                                                        type: 'ajax',
                                                        api: {
                                                            read: '/subscription/items',
                                                        },
                                                        reader: {
                                                            type: 'json',
                                                            root: 'items',
                                                            successProperty: 'success',
                                                            totalProperty: 'total'
                                                        },
                                                        extraParams: {
                                                            filter: '[' + 
                                                                '{"property":"order_id","value":"' + this.nxparams.id + '", "xoperator":"eq"},' +
                                                                '{"property":"type","value":"pdf"}' +
                                                            ']'
                                                        }
                                                    },
                                                    remoteFilter: true,
                                                    fields        : [
                                                        {name: 'id'},
                                                        {name: 'periodical_name'},
                                                        {name: 'byear'},
                                                        {name: 'bnum'},
                                                        {name: 'rqty'},
                                                    ],
                                                    autoLoad: true,
                                                    listeners: {
                                                        scope: this,
                                                        'load' : function(store) {
                                                            if(!store.getCount()) {
                                                                this.down('#nx_items_docs_grid').setTitle('pdf услуги не найдены');
                                                                this.down('#nx_items_docs_grid').down('toolbar').hide();
                                                            }
                                                        }
                                                    }
                                                }),
                                                columns : [
                                                    {text: 'ID', dataIndex: 'id', width: 60},
                                                    {text: 'Издание', dataIndex: 'periodical_name', width: 70},
                                                    {text: 'Год', dataIndex: 'byear', width: 50},
                                                    {text: 'Нач.выпуск', dataIndex: 'bnum', width: 65},
                                                    {text: 'Номеров', dataIndex: 'rqty', width: 55},
                                                ],
                                                viewConfig : { 
                                                    enableTextSelection: true
                                                },
                                                allowDeselect: true,
                                                selModel: {
                                                    mode: 'MULTI'
                                                },
                                                tbar: [
                                                    {
                                                        text   : 'без печати',
                                                        hidden : !nx.app.isperm('ship_manager'),//nx.config.perm.is_admin ? false : true,
                                                        scope  : this,
                                                        handler: function(btn) {
                                                            var selection = btn.up('grid').getSelectionModel().getSelection();
                                                            if(selection.length)
                                                            {
                                                                var ids = [];
                                                                Ext.each(selection, function(row) {
                                                                    ids.push(row.get('id'));
                                                                });
                                                                this.fireEvent('nx_get_items_ship_doc', btn.up('grid'), ids, false);
                                                            }
                                                        }
                                                    },
                                                    '->',
                                                    {
                                                        text   : 'с печатью',
                                                        hidden : !nx.app.isperm('ship_manager'),//nx.config.perm.is_admin ? false : true,
                                                        scope  : this,
                                                        handler: function(btn) {
                                                            var selection = btn.up('grid').getSelectionModel().getSelection();
                                                            if(selection.length)
                                                            {
                                                                var ids = [];
                                                                Ext.each(selection, function(row) {
                                                                    ids.push(row.get('id'));
                                                                });
                                                                this.fireEvent('nx_get_items_ship_doc', btn.up('grid'), ids, true);
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                xtype:'tbspacer',
                                                flex:1
                                            },
                                            {
                                                xtype     : 'grid',
                                                itemId    : 'nx_docs_grid',
                                                width     : 200, 
                                                maxHeight : 200,
                                                store     : new Ext.data.JsonStore({
                                                    proxy: {
                                                        type: 'ajax',
                                                        api: {
                                                            read: '/ship/shippings',
                                                        },
                                                        reader: {
                                                            type: 'json',
                                                            root: 'items',
                                                            successProperty: 'success',
                                                            totalProperty: 'total'
                                                        },
                                                        extraParams: {
                                                            filter: '[' + 
                                                                '{"property":"order_id","value":"' + this.nxparams.id + '"},' +
                                                                '{"property":"is_ship","value":"1"}' +
                                                            ']'
                                                        }
                                                    },
                                                    remoteFilter: true,
                                                    fields        : [
                                                        {name: 'id'},
                                                        {name: 'periodical_name'},
                                                        {name: 'issue_title'},
                                                    ],
                                                    autoLoad: true,
                                                    listeners: {
                                                        scope: this,
                                                        'load' : function(store) {
                                                            if(!store.getCount()) {
                                                                this.down('#nx_docs_grid').setTitle('отгрузки не найдены');
                                                                this.down('#nx_docs_grid').down('toolbar').hide();
                                                            }
                                                        }
                                                    }
                                                }),
                                                columns : [
                                                    {text: 'ID', dataIndex: 'id', width: 60, filter: {xtype: 'textfield'}},
                                                    {text: 'Издание', dataIndex: 'periodical_name', width: 70, filter: {xtype: 'combo', filterName: 'periodical_id', store: subscribleMagazinesStore}},
                                                    {text: 'Выпуск', dataIndex: 'issue_title', width: 50, filter: {xtype: 'textfield'}},
                                                ],
                                                viewConfig : { 
                                                    enableTextSelection: true
                                                },
                                                allowDeselect: true,
                                                selModel: {
                                                    mode: 'MULTI'
                                                },
                                                tbar: [
                                                    {
                                                        text   : 'без печати',
                                                        hidden : !nx.app.isperm('ship_manager'),//nx.config.perm.is_admin ? false : true,
                                                        scope  : this,
                                                        handler: function(btn) {
                                                            var selection = btn.up('grid').getSelectionModel().getSelection();
                                                            if(selection.length)
                                                            {
                                                                var ids = [];
                                                                Ext.each(selection, function(row) {
                                                                    ids.push(row.get('id'));
                                                                });
                                                                this.fireEvent('nx_get_ship_doc', btn.up('grid'), ids);
                                                            }
                                                        }
                                                    },
                                                    '->',
                                                    {
                                                        text   : 'с печатью',
                                                        hidden : !nx.app.isperm('ship_manager'),//nx.config.perm.is_admin ? false : true,
                                                        scope  : this,
                                                        handler: function(btn) {
                                                            var selection = btn.up('grid').getSelectionModel().getSelection();
                                                            if(selection.length)
                                                            {
                                                                var ids = [];
                                                                Ext.each(selection, function(row) {
                                                                    ids.push(row.get('id'));
                                                                });
                                                                this.fireEvent('nx_get_ship_doc', btn.up('grid'), ids, true);
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                                
                                            /*{
                                                xtype    : 'grid',
                                                autoShow : true,
                                                width    : 120,
                                                store    : {
                                                    type: 'file_storage', 
                                                    autoLoad: true,
                                                    listeners: {
                                                        scope: this,
                                                        beforeload: function(store) {
                                                            store.getProxy().extraParams = {
                                                                filter: '[' + 
                                                                    '{"property":"entity_type","value":"order"},' +
                                                                    //'{"property":"type","value":"1"},' +
                                                                    '{"property":"entity_id","value":"' + this.nxparams.id + '"}' +
                                                                ']'
                                                            }
                                                        }
                                                    }
                                                },
                                                hideHeaders: true,
                                                columns : [
                                                    {
                                                        xtype : 'actioncolumn',
                                                        width : 30, 
                                                        items : [
                                                            {
                                                                icon    : '/images/extjs/icons/download.png',
                                                                tooltip : 'скачать',
                                                                scope   : this,
                                                                handler : function(view, rowindex, colindex, item, e, rec)
                                                                {
                                                                    window.open('/file-storage/download/id/' + rec.get('id'), '_blank');
                                                                }
                                                            }
                                                        ]
                                                    },
                                                    {text: 'Название', dataIndex: 'name'}
                                                ],
                                                viewConfig : { 
                                                    enableTextSelection: true
                                                },
                                                dockedItems: [
                                                {
                                                    xtype: 'toolbar',
                                                    items : [
                                                        {
                                                            xtype   : 'button',
                                                            text    : 'Обновить',
                                                            scope   : this,
                                                            handler : function(btn) {
                                                                btn.up('grid').setLoading('Загрузка...');
                                                                Ext.Ajax.request({
                                                                    url     : '/subscription/get-bill',
                                                                    method  : 'POST',
                                                                    scope   : this,
                                                                    params  : {
                                                                        id : this.nxparams.id
                                                                    },
                                                                    success : function(response) {
                                                                        var response = Ext.JSON.decode(response.responseText);
                                                                        btn.up('grid').setLoading(false);
                                                                        var isok = response.result[this.nxparams.id] != undefined && !response.result[this.nxparams.id]['error'];
                                                                        if(isok) {
                                                                            btn.up('grid').getStore().reload();
                                                                        }
                                                                        Ext.Msg.alert('Сообщение', isok ? 'Документы успешно обновлен' : 'Ошибка при обновлении. ' + response.result[this.nxparams.id]['error']); 
                                                                    }
                                                                }, this);
                                                            }
                                                        }
                                                    ]
                                                }]
                                            }*/
                                        ]
                                    }
                                ]
                            },
                            {
                                title   : 'Рекламации',
                                border  : 0,
                                padding : 5,
                                //hidden  : !nx.app.isperm('admin'),
                                items   : [
                                    {
                                        xtype: 'grid',
                                        store     : new Ext.data.JsonStore({
                                            proxy: {
                                                type: 'ajax',
                                                api: {
                                                    read: '/ship-item/reclamation',
                                                },
                                                reader: {
                                                    type: 'json',
                                                    root: 'items',
                                                    successProperty: 'success',
                                                    totalProperty: 'total'
                                                },
                                                extraParams: {
                                                    filter: '[' + 
                                                        '{"property":"order_id","value":"' + this.nxparams.id + '"}' +
                                                    ']'
                                                }
                                            },
                                            remoteFilter: true,
                                            fields        : [
                                                {name: 'id'},
                                                {name: 'shipment_created'},
                                                {name: 'channel_name'},
                                                {name: 'release_date'},
                                                {name: 'issue_name'},
                                                {name: 'cancel_date'},
                                                {name: 'cancel_user_name'},
                                                {name: 'create_user_name'},
                                                {name: 'item_release_id'}
                                            ],
                                            autoLoad: true,
                                        }),
                                        columns : [
                                            //{text: 'ID', dataIndex: 'id', width: 30},
                                            {text: 'ID отгр.', dataIndex: 'item_release_id', width: 50},
                                            {text: 'Журнал', dataIndex: 'issue_name', flex: 1},
                                            {text: 'Дата выхода', dataIndex: 'release_date', width: 120},
                                            {text: 'Создал', dataIndex: 'create_user_name', width: 100},
                                            {text: 'Дата создания', dataIndex: 'shipment_created', width: 120},
                                            {text: 'Отменил', dataIndex: 'cancel_user_name', width: 100},
                                            {text: 'Дата отмены', dataIndex: 'cancel_date', width: 120},
                                            {text: 'Канал', dataIndex: 'channel_name', width: 80},

                                        ],
                                        viewConfig : { 
                                            enableTextSelection: true
                                        },
                                    }
                                ]
                            }
                        ]
                    }
                ],
                listeners: {
                    scope: this,
                    'actioncomplete' : function(form, action) {
                        if(action.result.items)
                        {
                            this.down('#subscriptionMagazinesEditor').fill(action.result.items);
                            if(this.nxparams.extend) {
                                this.down('#subscriptionMagazinesEditor').extendAll();
                                this.down('combobox[name="order_state_id"]').setValue('2');
                                this.down('textfield[name="xpress_id"]').setValue('0');
                                this.down('textfield[name="payment_date"]').setValue('');
                                this.down('textfield[name="payment_docno"]').setValue('');
                                this.down('textfield[name="payment_sum"]').setValue('0');
                                this.down('checkbox[name="map_is_enveloped"]').setValue(false);
                                this.down('combo[name="new_state"]').setValue('0');
                                this.down('combo[name="payment_agent_id"]').setValue('47');
                            }
                        }
                        if(action.result.data.hash) {
                            this.down('#subscriptionPaydocLink').update('<a href="' + nx.config.services.paydoc.url + '?hash=' + action.result.data.hash + '" target="_blank">' + (action.result.data.subscriber_type_id==1 ? 'квитанция' : 'счет') + '</a>');
                        }
                        if(action.result.data.subscriber_type_id==1) {
                            this.down('#subscriptionDoc_1c').hide();
                        }
                        this.fireEvent('nx_order_form_refresh_ui', this, action.result.data);
                    }
                }
            }
        ];

        this.buttons = [
            {
                text   : this.nxparams.extend ? 'Продлить' : 'Сохранить',
                action : this.nxparams.extend ? 'extend' : 'save'
            },
            {
                text    : 'Закрыть',
                action  : 'close',
                scope   : this,
                handler : this.close
            }
        ];
        this.addEvents('nx_get_ship_doc');
        this.callParent(arguments);
    },
    mapAddrToFields: function(addr)
    {
        var map = {
            'area'      : 'map_area',
            'region'    : 'map_region',
            'city'      : 'map_city',
            'city1'     : 'map_city1',
            'street'    : 'map_street',
            'house'     : 'map_house',
            'building'  : 'map_building',
            'structure' : 'map_structure',
            'apartment' : 'map_apartment',
            'zipcode'   : 'map_zipcode'
        };
        var addrform = this.down('#addressRightForm');
        for(field in addr)
        {
            var value = addr[field];
            addrform.down('textfield[name="'+map[field]+'"]').setValue(value);
        }

    },
    showAddressList: function(data)
    {
        new Ext.window.Window({
            title     : 'Список подходящих адресов',
            itemId    : 'addressParseListWindow',
            iconCls   : 'watch-ico',
            layout    : 'fit',
            //width     : 700,
            //height    : 600,
            border    : false,
            resizable : false,
            //modal     : true,
            autoShow  : true, 
            items     : [
                {
                    xtype : 'grid',
                    width : 800,
                    //height: 500,
                    autoShow: true,
                    store: new Ext.data.JsonStore({
                        proxy:
                        {
                            type: 'memory',
                            reader: {
                                type: 'json',
                                root: 'items'
                            }
                        },
                        data: {'items' : data},
                        pageSize: 9999,
                        remoteFilter: true,
                        autoLoad : true,
                        fields        : [
                            {name: 'zipcode'},
                            {name: 'area'},
                            {name: 'region'},
                            {name: 'city'},
                            {name: 'city1'},
                            {name: 'street'},
                            {name: 'house'},
                            {name: 'building'},
                            {name: 'structure'},
                            {name: 'apartment'}
                        ]
                    }),
                    columns    : [
                        {xtype: 'rownumberer', width: 50},
                        {text: 'индекс', dataIndex: 'zipcode', width: 60},
                        {text: 'регион', dataIndex: 'area', flex:1},
                        {text: 'район', dataIndex: 'region', flex:1},
                        {text: 'нас. пункт1', dataIndex: 'city', flex:1},
                        {text: 'нас. пункт2', dataIndex: 'city1', flex:1},
                        {text: 'улица', dataIndex: 'street', flex:1},
                        {text: 'дом', dataIndex: 'house', width: 35},
                        {text: 'корп.', dataIndex: 'building', width: 35},
                        {text: 'стр.', dataIndex: 'structure', width: 35},
                        {text: 'кв.', dataIndex: 'apartment', width: 35}
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    },
                    listeners: {
                        scope: this,
                        'selectionchange': function(grid, selected) {
                            if(selected.length){
                                this.mapAddrToFields(selected[0].raw);
                            }
                        }
                    }
                }
            ],
            buttons : [
                {
                    text: 'Закрыть',
                    handler: function()
                    {
                        this.up('window').close();
                    }
                }
            ]
        });
    }
});