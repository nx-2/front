/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.items' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_subscription_items',
    itemId: 'nx_subscription_items',

    title: 'Подписки - позиции',

    store: 'subscription_item',

    initComponent: function() {

        this.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 2,
        });

        this.columns = [
            {text: 'ID позиции', dataIndex: 'id', width: 70, xfilter: true, hidden: true},
            {text: 'ID заказа', dataIndex: 'order_id', width: 65, xfilter: true, 
                renderer :function(value, metadata,record, rowIndex, colIndex, store, grid) { 
                    return parseInt(value) ? '<a href="#" onclick="Ext.getCmp(\'' + grid.id + '\').up(\'grid\').onOrderIdClick(\''+record.get('order_id')+'\'); return false;">' + value + '</a>' : value; 
                }
            },
            {text: 'Подписчик (возможен поиск по email)', dataIndex: 'name', flex: 1, tdCls: 'nx-multiline', xfilter: true},
            //{text: 'Email', dataIndex: 'email', width: 150, filter: {xtype: 'textfield'}},
            {text: 'Состояние', dataIndex: 'order_state_name', width: 70, xfilter: 'order_state_id'},
            //{text: 'Тип', dataIndex: 'order_type', width: 70, filter: {xtype: 'combo', store: subscription_type_store}},
            {text: 'Издание', dataIndex: 'periodical_name', width: 100, xfilter: 'periodical_id'},
            {text: 'Сум. заказа', width: 75, dataIndex: 'order_price', hidden: true},
            {text: 'Сумма', width: 80, 
                renderer: function(v, meta, row) { 
                    var total = parseFloat(row.get('price')) * parseInt(row.get('rqty')) * parseInt(row.get('quantity'));
                    total = total - total * row.get('discount') / 100.0;
                    return total.toFixed(2).toString().replace('.',','); 
                }, 
                summaryType: function(rows) {
                    var sum = 0;
                    Ext.each(rows, function(row){
                        sum+=parseFloat(row.get('price')) * parseInt(row.get('rqty')) * parseInt(row.get('quantity'));
                    });
                    return sum.toFixed(2);
                },
                summaryRenderer: function(value) {
                    return value;
                }
            },
            {text: 'Способ оплаты', dataIndex: 'payment_type_name', width: 90, xfilter: 'payment_type_id'},
            {text: 'Способ доставки', dataIndex: 'delivery_type_name', width: 100, xfilter: 'delivery_type_id'},
            //{text: 'Дата', dataIndex: 'order_date', width: 90, renderer: Ext.util.Format.dateRenderer('Y-m-d'), xfilter: true},
            {text: 'Дата оплаты', dataIndex: 'payment_date',  width: 90, renderer: Ext.util.Format.dateRenderer('Y-m-d'), xfilter: true},
            //{text: 'Дата создания', dataIndex: 'created', width: 120, xfilter: true},
            {text: 'Метка', dataIndex: 'order_label', width: 150, xfilter: true},
            {text: 'Email', dataIndex: 'email', width: 150, xfilter: true},
            {text: 'Актуальна?', dataIndex: 'is_present', width: 70, sortable: false, renderer: function(value) {if(value=='1') return 'да'; else return 'нет';}, xfilter: true},
            {text: 'Дата начала', dataIndex: 'date_start', width: 90, filter: {xtype: 'textfield'}, hidden: true},
            {text: 'Дата оконч.', dataIndex: 'date_end', width: 90, filter: {xtype: 'textfield'}, hidden: true},
            {text: 'Период', dataIndex: 'period', width: 60, filter: {xtype: 'textfield'}, hidden: true},
            {text: 'Год', dataIndex: 'byear', width: 50, filter: {xtype: 'textfield'}, hidden: true},
            {text: 'Нач. выпуск', dataIndex: 'bnum', width: 75, filter: {xtype: 'textfield'}, hidden: true},
            {text: 'Номеров', dataIndex: 'rqty', width: 70, filter: {xtype: 'textfield'}, hidden: true},
            {text: 'Кол-во', dataIndex: 'quantity', width: 50, filter: {xtype: 'textfield'}, hidden: true},
            {text: 'Цена', dataIndex: 'price', width: 70, filter: {xtype: 'textfield'}, hidden: true, 
                renderer: function(v, meta, row) {
                    return v.toString().replace('.',','); 
                }, 
                summaryType: function(rows) {
                    var sum = 0;
                    Ext.each(rows, function(row){
                        sum+=parseFloat(row.get('price'));
                    });
                    return sum.toFixed(2);
                },
                summaryRenderer: function(value) {
                    return value;
                }
            },
            {text: 'Скидка', dataIndex: 'discount', width: 50, hidden: true},
            {text: 'Продление', dataIndex: 'prolong_state', width: 90, hidden: false, editor: {xtype: 'combo', editable : false, store: prolong_state_store}, xfilter: true, renderer: function(val){
                for(var i=0; i<prolong_state_store.length; i++) { if(prolong_state_store[i][0] == val) return prolong_state_store[i][1]; } 
            }},
            {text: 'Дата создания', dataIndex: 'created',  width: 130, filter: {xtype: 'textfield'}, hidden: true},
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        var filters = {
            ftype: 'filters',
            menuFilterText: 'Фильтровать',
            encode: true,
            local: false
        };

        Ext.apply(this, {
            plugins: [gridheaderfilters, this.cellEditing],      
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    var css_class = '';
                    css_class += rec.get('enabled') == 0 ? ' subscription-row-disabled' : ''; 
                    return css_class;
                } 
            },
            features: [{
                ftype: 'summary'
            }],
            xfilters : [
                {fieldLabel: 'id позиции', name: 'id', xtype: 'textfield'},
                {fieldLabel: 'id заказа', name: 'order_id', xtype: 'textfield'},
                {fieldLabel: 'подписчик', name: 'name', xtype: 'textfield'},
                {fieldLabel: 'состояние', name: 'order_state_id', xtype: 'combo', store: orderStatesStore},
                {fieldLabel: 'издание', name: 'periodical_id', xtype: 'combo', store: subscribleMagazinesStore, editable : false, multiSelect: true, invalidCls: ''},
                {fieldLabel: 'сумма', name: 'price', xtype: 'textfield'},
                {fieldLabel: 'способ оплаты', name: 'payment_type_id', xtype: 'combo', store: payment_types_store},
                {fieldLabel: 'способ доставки', name: 'delivery_type_id', xtype: 'combo', store: delivery_types_store},
                //{fieldLabel: 'дата', name: 'order_date', xtype: 'textfield', onlyheader: false},
                    //{fieldLabel: 'дата', name: 'date_start', xtype: 'datefield', group: 'date', width: 230, format: 'Y-m-d'},
                    //{name: 'date_end', xtype: 'datefield', group: 'date', width: 120, format: 'Y-m-d'},
                {fieldLabel: 'дата оплаты', name: 'payment_date', xtype: 'textfield'},
                {fieldLabel: 'интервал опл.', name: 'payment_date_start', xtype: 'datefield', group: 'payment_date', width: 230, format: 'Y-m-d', allowSubmitBlank: false},
                    {name: 'payment_date_end', xtype: 'datefield', group: 'payment_date', width: 120, format: 'Y-m-d', allowSubmitBlank: false},
                {fieldLabel: 'метка', name: 'order_label', xtype: 'textfield'},
                {fieldLabel: 'email', name: 'email', xtype: 'textfield'},
                {fieldLabel: 'актуальна?', name: 'is_present', xtype: 'combo', editable : false, store: [['0','нет'],['1','да']]},
                {fieldLabel: 'включена?', name: 'order_enabled', xtype: 'combo', editable : false, store: [['0','нет'],['1','да']]},
                {fieldLabel: 'заголовок выпуска', name: 'issue_title', xtype: 'textfield'},
                {fieldLabel: 'статус продления', name: 'prolong_state', xtype: 'combo', editable : false, store: prolong_state_store},
            ],
            allowDeselect: true,
            selModel: {
                mode: 'MULTI'
            },
        });
    
        Ext.apply(this, {
            dockedItems: [
            {
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных',
                plugins     : [{ptype: 'pagesize'}]
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        xtype: 'view_state_switcher',
                    },
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters(false);
                            this.store.clearFilter();
                        }
                    },
                    '-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Подписки - позиции');
                                }
                            },this);   
                        }
                    },
                    '-',
                    {
                        text       : 'По заказам',
                        iconCls    : 'subscription-items-ico',
                        scope      : this,
                        href       : '/#subscription/index',
                        hrefTarget : '_self',
                        hidden     : this.nxparams.by ? true : false
                    },
                    '->',
                    {
                        text    : 'Обновить статус продления',
                        scope   : this,
                        hidden  : !nx.app.isperm('admin'),
                        handler : function(button) {
                            this.fireEvent('nx_subscription_re_prolong', button);
                        }
                    }
                ]
            }],
        });
        this.addEvents('nx_open_order_edit_form');
        this.callParent(arguments);
    },
    onOrderIdClick: function(order_id) {
        this.fireEvent('nx_open_order_edit_form', this, order_id);
    }
});