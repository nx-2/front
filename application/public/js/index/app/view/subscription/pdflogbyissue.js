/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.pdflogbyissue' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_pdflogbyissue_list',

    title: 'Лог отправки pdf на email по выпускам',

    //store: 'subscription',

    initComponent: function() {

        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/subscription/pdflogbyissue',
                        //update: 'js/index/data/updateUsers.json'
                    },
                    //url : '/subscription/pdflog',
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                //autoLoad : true,
                fields        : [
                    {name: 'id'},
                    {name: 'periodical_name'},
                    {name: 'issue_id'},
                    {name: 'issue_title'},
                    {name: 'sended'},
                    {name: 'queue'},
                    {name: 'created'}
                ]
            })
        });

        Ext.apply(this, {
            columns    : [
                {header: 'Издание', dataIndex: 'periodical_name', flex: 1, filter: {xtype: 'combo', filterName: 'periodical_id', store: subscribleMagazinesStore}},
                {header: 'Выпуск', dataIndex: 'issue_title', flex: 1, filter: {xtype: 'textfield'}},
                {
                    header: 'Отправлено', dataIndex: 'sended', flex: 1, filter: {xtype: 'textfield'},
                    renderer: function(value) { return '<a href="" onclick="return false;">' + value + '</a>'; },
                    listeners: {
                        scope: this,
                        click: function(grid, cell, rowIndex, colIndex, event) {
                            var rec = this.store.getAt(rowIndex);
                            this.showDetails(rec.get('issue_id'), 1);
                        }
                    }
                },
                {
                    header: 'Всего', dataIndex: 'queue', flex: 1, filter: {xtype: 'textfield'},
                    renderer: function(value) { return '<a href="" onclick="return false;">' + value + '</a>'; },
                    listeners: {
                        scope: this,
                        click: function(grid, cell, rowIndex, colIndex, event) {
                            var rec = this.store.getAt(rowIndex);
                            this.showDetails(rec.get('issue_id'), '');
                        }
                    }
                },
                {header: 'Дата', dataIndex: 'created', flex: 1, filter: {xtype: 'textfield'}}
            ]
        });

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            } 
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    }
                ]
            }],
        });
        this.addEvents('nx_pdf_log_details');
        this.callParent(arguments);
    },
    showDetails: function(issue_id, send) {
        this.fireEvent('nx_pdf_log_details', issue_id, send);
        /*ExtWindow = new Ext.window.Window({
            title: send ? 'Отправлено' : 'Всего',
            autoShow: true,
            width: 400,
            maxHeight:500,
            layout: 'fit',
            y:100,
            //modal: true,
            items: [
                {
                    xtype: 'grid',
                    store: new Ext.data.JsonStore({
                        proxy: {
                            type: 'ajax',
                            url : '/subscription/pdflog',
                            reader: {
                                type: 'json',
                                root: 'items',
                                successProperty: 'success',
                                totalProperty: 'total'
                            },
                            extraParams: {
                                filter: '[{"property":"issue_id","value":"'+issue_id+'"}' + (send ? ', {"property":"send","value":"'+send+'"}' : '') + ']'
                            }
                        },
                        pageSize: 999,
                        remoteFilter: true,
                        autoLoad : true,
                        fields        : [
                            {name: 'email'},
                            {name: 'send'},
                            {name: send ? 'sendtime' : 'created'},
                            {name: 'subscription_id'}
                        ]
                    }),
                    columns    : [
                        {xtype: 'rownumberer', width: 40},
                        {header: 'Email', dataIndex: 'email', flex: 1},
                        {header: 'Статус', dataIndex: 'send', flex: 1, renderer: function(value) { if(value=='1') { return 'отправлено'; } else { return value; } }},
                        {header: 'Дата', dataIndex: send ? 'sendtime' : 'created', flex: 1},
                        {header: 'id подписки', dataIndex: 'subscription_id', flex: 1}
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    } 
                }
            ]
        });*/
    }
});