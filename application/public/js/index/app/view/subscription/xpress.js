/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.xpress' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_subscription_xpress',

    title: 'Подписки из xpress',

    initComponent: function() {

        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/subscription/xpress'
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields        : [
                    {name: 'id'},
                    {name: 'name'},
                    {name: 'total'},
                    {name: 'payment_sum'},
                    {name: 'payment_date'},
                    {name: 'periodical_names'},
                    {name: 'delivery'},
                    {name: 'is_payed'},
                    {name: 'is_present'},
                    {name: 'manager'},
                    {name: 'last_release'}
                ]
            })
        });

        var delivery_store = [
            ['10', 'курьер'],
            ['50', 'курьер-Электрический'],
            ['12', 'машина'],
            ['14', 'офис'],
            ['20', 'почта гор'],
            ['6' , 'почта ДЗ'],
            ['43', 'почта ДЗ авиа'],
            ['5' , 'почта РФ'],
            ['18', 'почта СНГ'],
            ['52', 'Электронная доставка']
        ];

        this.columns = [
            {text: 'id',  dataIndex: 'id',  width: 50, filter: {xtype: 'textfield'}},
            {text: 'Получатель',  dataIndex: 'name',  flex: 1, filter: {xtype: 'textfield', minLength: 2}},
            {text: 'Издания',  dataIndex: 'periodical_names',  width: 140, filter: {xtype: 'combo', editable: false, filterName: 'periodical_id', store: [
                ['1', 'МПК+DVD'],
                ['2', 'CWR'],
                ['3', 'КЖ'],
                ['4', 'LAN'],
                ['5', 'Сети'],
                ['6', 'ОС'],
                ['7', 'PUB'],
                ['9', 'Win'],
                ['14', 'ЛВ'],
                ['20', 'ДИС'],
                ['112', 'Stuff'],
                ['127', 'ПМ'],
                ['130', 'HI-FI'] 
            ]}},
            {text: 'Доставка', dataIndex: 'delivery', width: 100, renderer: function(value) {
                    for(i in delivery_store)
                    {
                        if(delivery_store[i][0] == value) return delivery_store[i][1];
                    }
                }, filter: {xtype: 'combo', editable: false, store: delivery_store}
            },
            {text: 'Сумма',  dataIndex: 'total',  width: 80, filter: {xtype: 'textfield'}},
            {text: 'Оплачено',  dataIndex: 'payment_sum',  width: 80, filter: {xtype: 'textfield'}},
            {text: 'Дата оплаты',  dataIndex: 'payment_date',  width: 90, filter: {xtype: 'textfield'}},
            {text: 'Оплачена?', dataIndex: 'is_payed', width: 70, renderer: function(value) {if(value=='1') return 'да'; else return 'нет';}, filter: {xtype: 'combo', editable : false, store: [['0','нет'],['1','да']]}},
            {text: 'Актуальна?', dataIndex: 'is_present', width: 70, renderer: function(value) {if(value=='1') return 'да'; else return 'нет';}, filter: {xtype: 'combo', editable : false, store: [['0','нет'],['1','да']]}},
            {text: 'Менеджер', dataIndex: 'manager', width: 70, filter: {xtype: 'textfield'}},
            {text: 'Последний выпуск', dataIndex: 'last_release', width: 150}
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true 
            },
            allowDeselect: true,
            selModel: {
                mode: 'MULTI'
            }
        });

        Ext.apply(this, {
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных',
                plugins: [{ptype: 'pagesize'}]
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },
                    '->',
                    {
                        text    : 'Импортировать',
                        iconCls : 'refresh-ico',
                        scope   : this,
                        itemId  : 'importXpressButton',
                        hidden  : nx.config.perm.is_admin /*|| nx.config.perm.is_subscribe_manager*/ ? false : true,
                        disabled: true,
                        handler : function(button) {
                            Ext.Msg.confirm('Подтверждение','Импортировать из xpress в nx все подписки в статусе "связь" и "добавление"?', function(btn){
                                if(btn=='yes')
                                {
                                    button.disable();
                                    var panel = Ext.ComponentQuery.query('#importStatsPanel')[0];
                                    panel.setLoading({msg:'Загрузка...'});
                                    Ext.Ajax.request({
                                        url     : '/subscription/import-xpress-all',
                                        method  : 'GET',
                                        scope   : this,
                                        success : function(response) {
                                            panel.setLoading(false);
                                            Ext.Msg.alert('Внимание', 'Импорт запущен. Данные обновятся через 5-15 минут.');
                                            //this.refreshStatsPanel();
                                        }
                                    });
                                }
                            },this);  
                        }
                    },
                    '-',
                    {
                        xtype     : 'panel',
                        itemId    : 'importStatsPanel',
                        width     : 500,
                        border    : false,
                        bodyStyle : 'background:none; margin-left: 10px;',
                        defaults  : {
                            border: false,
                            bodyStyle : 'background:none',
                        },
                        listeners: {
                            scope: this,
                            'afterrender': function() {
                                this.refreshStatsPanel();
                            }
                        },
                        showCountsInfo: function(action)
                        {
                            if(this.data == undefined) {
                                return;
                            }
                            var data = [];
                            var action_names = {
                                'lock'   : 'xpress_id занят ',
                                'link'   : 'связь с подпиской ',
                                'add'    : 'добавление',
                                'manual' : 'вручную'
                            };
                            Ext.each(this.data.ids[action], function(subscription) {
                                data.push({
                                    'xpress_id'   : subscription.id, 
                                    'action'      : action, 
                                    'action_name' : action_names[action] + (action=='link' || action=='lock' ? subscription.link_id : ''), 
                                    'comment'     : subscription.comment, 
                                    'link_id'     : action=='link' || action=='lock' ? subscription.link_id : 0
                                });
                            });
                            this.up('grid').showXpressImportCheckList([], data);
                        }
                    },
                    {
                        text    : 'Проверить',
                        icon    : '/images/extjs/icons/xpress-check.png',
                        hidden  : nx.config.perm.is_admin || nx.config.perm.is_subscribe_manager ? false : true,
                        scope   : this,
                        handler : function() {
                            var selectionSubscription = this.getSelectionModel().getSelection();
                            if(selectionSubscription.length)
                            {
                                var ids = [];
                                Ext.each(selectionSubscription, function(row) {
                                    ids.push(row.get('id'));
                                });
                                this.showXpressImportCheckList(ids.join(','));
                            }
                            else
                            {
                                Ext.Msg.alert('Ошибка', 'Выберите подписку для импорта');
                            }
                        }
                    },
                    {
                        text    : 'Импорт из Xpress',
                        icon    : '/images/extjs/icons/xpress-import.png',
                        hidden  : nx.config.perm.is_admin || nx.config.perm.is_subscribe_manager ? false : true,
                        scope   : this,
                        handler : function() {
                            var selectionSubscription = this.getSelectionModel().getSelection();
                            if(selectionSubscription.length)
                            {
                                if(selectionSubscription.length==1)
                                {
                                    var xpress_id = selectionSubscription[0].get('id');
                                    //console.log(this,selectionSubscription);
                                    if(xpress_id)
                                    {
                                        Ext.Ajax.request({
                                            url     : '/subscription/import-xpress',
                                            method  : 'GET',
                                            params  : {
                                                xpress_id : xpress_id,
                                                step      : 1
                                            },
                                            scope   : this,
                                            success : function(response) {
                                                //this.down('grid').getStore().reload();
                                                //Ext.getCmp('addUserRoleWindow').close();
                                                //console.log(response);
                                                var response = Ext.JSON.decode(response.responseText);
                                                //Ext.getCmp('importXpressSubscriptionWindow').close();
                                                this.showXpressImportStep2Window(xpress_id, response.result);
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    var ids = [];
                                    Ext.each(selectionSubscription, function(row) {
                                        ids.push(row.get('id'));
                                    });
                                    this.showXpressImportCheckList(ids.join(','));
                                }
                            }
                            else
                            {
                                Ext.Msg.alert('Ошибка', 'Выберите подписку для импорта');
                                //this.showXpressImportCheckList();
                            }
                        }
                    }
                ]
            }]
        });

        this.callParent(arguments);
    },
    refreshStatsPanel: function()
    {
        var panel = Ext.ComponentQuery.query('#importStatsPanel')[0];
        panel.setLoading({msg:'Загрузка...'});
        Ext.Ajax.request({
            url     : '/subscription/get-xpress-import-stats',
            method  : 'GET',
            scope   : panel,
            success : function(response) {
                var response = Ext.JSON.decode(response.responseText);
                var counts   = response.data.counts;
                panel.data   = response.data;
                panel.removeAll();
                var manual_link = '<a href="" onclick="Ext.getCmp(\'' + panel.id  + '\').showCountsInfo(\'manual\'); return false;">' + counts.manual + '</a>';
                var lock_link   = '<a href="" onclick="Ext.getCmp(\'' + panel.id  + '\').showCountsInfo(\'lock\'); return false;">' + counts.lock + '</a>';
                var add_link    = '<a href="" onclick="Ext.getCmp(\'' + panel.id  + '\').showCountsInfo(\'add\'); return false;">' + counts.add + '</a>';
                var link_link   = '<a href="" onclick="Ext.getCmp(\'' + panel.id  + '\').showCountsInfo(\'link\'); return false;">' + counts.link + '</a>';
                panel.add({html: 'Активных (последнее обновление ' + panel.data.mtime + '): ' + counts.total + '<br/>Занято: ' + lock_link + ' - Связь: ' + link_link + ' - Добавление: ' + add_link + ' - Вручную: ' + manual_link});
                if(panel.data.running) {
                    Ext.ComponentQuery.query('#importXpressButton')[0].disable();
                }
                else {
                    Ext.ComponentQuery.query('#importXpressButton')[0].enable();
                }
                panel.setLoading(false);
            }
        });
    },
    showXpressImportCheckList: function(ids, data)
    {
        new Ext.window.Window({
            title     : 'Импорт подписок из xpress',
            itemId    : 'importXpressSubscriptionWindow',
            iconCls   : 'xpress-import-ico',
            layout    : 'fit',
            //width     : 700,
            //height    : 600,
            border    : false,
            resizable : true,
            //modal     : true,
            autoShow  : true, 
            items     : [
                {
                    xtype : 'grid',
                    width : 500,
                    height: 500,
                    autoShow: true,
                    store: new Ext.data.JsonStore({
                        proxy: data ? 
                        {
                            type: 'memory',
                            reader: {
                                type: 'json',
                                root: 'items'
                            }
                        }
                        : 
                        {
                            type: 'ajax',
                            url : '/subscription/import-xpress-list',
                            actionMethods : {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                            reader: {
                                type: 'json',
                                root: 'items',
                                successProperty: 'success',
                                totalProperty: 'total'
                            },
                            extraParams: {
                                ids : ids
                            },
                            timeout: 600000
                        },
                        data: data ? {'items' : data} : null,
                        pageSize: 9999,
                        remoteFilter: true,
                        autoLoad : true,
                        fields        : [
                            {name: 'xpress_id'},
                            {name: 'action'},
                            {name: 'action_name'},
                            {name: 'comment'},
                            {name: 'link_id'}
                        ]
                    }),
                    columns    : [
                        {xtype: 'rownumberer', width: 50},
                        {text: 'xpress_id', dataIndex: 'xpress_id', width: 70},
                        {text: 'действие', dataIndex: 'action_name', width: 150},
                        {text: 'комментарий', dataIndex: 'comment', flex: 1, tdCls: 'nx-multiline'},
                        {
                            xtype: 'actioncolumn',
                            width: 30, 
                            items: [
                                {
                                    scope: this,
                                    handler: function(view, rowindex, colindex, item, e, rec)
                                    {
                                        var xpress_id = rec.get('xpress_id');
                                        Ext.Ajax.request({
                                            url     : '/subscription/compare-xpress-nx',
                                            method  : 'GET',
                                            params  : {
                                                xpress_id : xpress_id,
                                                subscription_id : rec.get('link_id')
                                            },
                                            scope   : this,
                                            success : function(response) {
                                                var response = Ext.JSON.decode(response.responseText);
                                                this.showXpressIdDetails(xpress_id, response.result);
                                            }
                                        });
                                    },
                                    getClass: function(value, meta, rec, rowindex)
                                    {
                                        if(rec.get('action')=='lock' || rec.get('action')=='link')
                                        {
                                            return 'watch-ico';
                                        }
                                        return 'x-hide-display';
                                    }
                                }
                            ]
                        }
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    },
                    dockedItems: [
                    {
                        xtype: 'toolbar',
                        items : [
                            {
                                text   : 'Экспорт CSV',
                                iconCls: 'export-csv-ico',
                                //scope  : this, 
                                handler: function() 
                                {
                                    var grid = this.up('#importXpressSubscriptionWindow').down('grid');
                                    Ext.Msg.confirm('Подтверждение','Экспортировать ' + grid.store.getCount() + ' строк?', function(btn){
                                        if(btn=='yes')
                                        {
                                            var data = Ext.ux.exporter.Exporter.exportGrid(grid, 'csv');
                                            submitFakeForm('/index/export-data', data, 'Подписки');
                                        }
                                    },grid);  
                                }
                            }
                        ]
                    }]
                }
            ],
            //buttonAlign : 'left', 
            buttons : [
                {
                    text: 'Закрыть',
                    handler: function()
                    {
                        Ext.ComponentQuery.query('#importXpressSubscriptionWindow')[0].close();
                    }
                }
            ]
        });
    },
    showXpressIdDetails: function(xpress_id, data)
    {
        new Ext.window.Window({
            title     : 'Сравнение подписок из xpress и nx',
            itemId    : 'compareXpressSubscriptionWindowDetail',
            iconCls   : 'watch-ico',
            layout    : 'fit',
            //width     : 700,
            //height    : 600,
            border    : false,
            resizable : false,
            //modal     : true,
            autoShow  : true, 
            items     : [
                {
                    layout: 'hbox',
                    items: [
                        {
                            title : 'xpress подписка',
                            xtype : 'treepanel',
                            width : 400,
                            height: 500,
                            autoShow: true,
                            rootVisible: false,
                            rowLines: true,
                            bodyCls: 'x-tree-noicon',
                            store: new Ext.data.TreeStore({
                                autoLoad: true,
                                fields: [
                                    {name: 'text'},
                                    {name: 'value'}
                                ],
                                root: {
                                    text: 'Данные',
                                    expanded: true,
                                    children: convertDataToTree(data.xpress)
                                }
                            }),
                            columns: [
                                {xtype: 'treecolumn', dataIndex: 'text', text: 'свойство', width:190},
                                {dataIndex: 'value', text: 'значение', flex:1, tdCls: 'nx-multiline'}
                            ],
                            listeners: {
                                'render' : function() {
                                    //this.getRootNode().getChildAt(0).expandChildren(true);
                                    //this.getRootNode().getChildAt(0).expand();
                                    this.expandAll();
                                    this.getRootNode().getChildAt(2).collapse();
                                }
                            },
                            viewConfig : { 
                                enableTextSelection: true
                            }
                        },
                        {
                            title : 'nx подписка',
                            xtype : 'treepanel',
                            width : 400,
                            height: 500,
                            autoShow: true,
                            rootVisible: false,
                            rowLines: true,
                            bodyCls: 'x-tree-noicon',
                            store: new Ext.data.TreeStore({
                                autoLoad: true,
                                fields: [
                                    {name: 'text'},
                                    {name: 'value'}
                                ],
                                root: {
                                    text: 'Данные',
                                    expanded: true,
                                    children: convertDataToTree(data.nx)
                                }
                            }),
                            columns: [
                                {xtype: 'treecolumn', dataIndex: 'text', text: 'свойство', width:190},
                                {dataIndex: 'value', text: 'значение', flex:1, tdCls: 'nx-multiline'}
                            ],
                            listeners: {
                                'render' : function() {
                                    this.expandAll();
                                    this.getRootNode().getChildAt(2).collapse();
                                }
                            },
                            viewConfig : { 
                                enableTextSelection: true
                            }
                        }
                    ]
                }
            ],
            //buttonAlign : 'left', 
            buttons : [
                {
                    text: 'Закрыть',
                    handler: function()
                    {
                        Ext.ComponentQuery.query('#compareXpressSubscriptionWindowDetail')[0].close();
                    }
                }
            ]
        });
    },
    /*showXpressImportStep1Window: function()
    {
        var viewid = this.id;
        //console.log(viewid);
        ExtWindow = new Ext.window.Window({
            title     : 'Импорт подписки из xpress',
            id        : 'importXpressSubscriptionWindow',
            iconCls   : 'xpress-import-ico',
            layout    : 'fit',
            width     : 200,
            border    : false,
            resizable : false,
            //modal     : true,
            autoShow  : true, 
            items     : [
                {
                    xtype      : 'textfield',
                    name       : 'xpress_id',
                    allowBlank : false,
                    vtype      : 'Numeric',
                    fieldLabel : 'xpress ID',
                    id         : 'importXpressID',
                    //value: '137989'
                }
            ],
            buttons : [
                {
                    text    : 'Проверить',
                    scope   : this,
                    handler : function() {
                        var xpress_id = Ext.getCmp('importXpressID').getValue();
                        Ext.Ajax.request({
                            url     : '/subscription/import-xpress',
                            method  : 'GET',
                            params  : {
                                xpress_id : xpress_id,
                                step      : 1
                            },
                            scope   : this,
                            success : function(response) {
                                //this.down('grid').getStore().reload();
                                //Ext.getCmp('addUserRoleWindow').close();
                                console.log(response);
                                var response = Ext.JSON.decode(response.responseText);
                                Ext.getCmp('importXpressSubscriptionWindow').close();
                                this.showXpressImportStep2Window(xpress_id, response.result);
                            }
                        });
                    }
                }
            ]
        });
    },*/
    showXpressImportStep2Window: function(xpress_id, data)
    {
        new Ext.window.Window({
            title     : 'Импорт подписки из xpress: проверьте данные импортируемой подписки',
            id        : 'importXpressSubscriptionWindow',
            iconCls   : 'xpress-import-ico',
            layout    : 'fit',
            //width     : 700,
            //height    : 600,
            border    : false,
            resizable : false,
            //modal     : true,
            autoShow  : true, 
            items     : [
                {
                    xtype : 'treepanel',
                    width : 500,
                    height: 500,
                    autoShow: true,
                    //singleExpand: true,
                    rootVisible: false,
                    rowLines: true,
                    //lines: false,
                    bodyCls: 'x-tree-noicon',
                    /*root: {
                        text: 'Данные',
                        expanded: true,
                        children: convertDataToTree(data)
                    },*/
                    store: new Ext.data.TreeStore({
                        autoLoad: true,
                        fields: [
                            {name: 'text'},
                            {name: 'value'}
                        ],
                        root: {
                            text: 'Данные',
                            expanded: true,
                            children: convertDataToTree(data)
                        }
                    }),
                    columns: [
                        {xtype: 'treecolumn', dataIndex: 'text', text: 'свойство', width:190},
                        {dataIndex: 'value', text: 'значение', flex:1, tdCls: 'nx-multiline'}
                    ],
                    listeners: {
                        'render' : function() {
                            this.getRootNode().getChildAt(0).expandChildren(true);
                            this.getRootNode().getChildAt(0).expand();
                        }
                    }
                }
            ],
            buttonAlign : 'left', 
            buttons : [
                {
                    text: 'Отмена',
                    handler: function()
                    {
                        Ext.getCmp('importXpressSubscriptionWindow').close();
                    }
                },
                '->',
                {
                    text    : 'Подтверждаю',
                    scope   : this,
                    handler : function() {
                        Ext.Ajax.request({
                            url     : '/subscription/import-xpress',
                            method  : 'GET',
                            params  : {
                                xpress_id : xpress_id,
                                step      : 2
                            },
                            scope   : this,
                            success : function(response) {
                                //this.down('grid').getStore().reload();
                                //Ext.getCmp('addUserRoleWindow').close();
                                //console.log(response);
                                var response = Ext.JSON.decode(response.responseText);
                                Ext.getCmp('importXpressSubscriptionWindow').close();
                                this.showXpressImportStep3Window(xpress_id, response.result);
                            }
                        });
                    }
                }
            ]
        });
    },
    showXpressImportStep3Window: function(xpress_id, data)
    {//console.log(data);
        new Ext.window.Window({
            title     : 'Импорт подписки из xpress: найдены следующие совпадения в базе NX',
            id        : 'importXpressSubscriptionWindow',
            iconCls   : 'xpress-import-ico',
            //layout    : 'border',
            //width     : 700,
            maxHeight    : 500,
            border    : false,
            resizable : false,
            //modal     : true,
            autoShow  : true, 
            overflowY : 'auto',
            items     : [
                {
                    xtype    : 'checkbox',
                    name     : 'is_just_link_subscription',
                    itemId   : 'is_just_link_subscription',
                    boxLabel : 'связывать подписки без обновления данных',
                    checked  : data.is_lock_subscription ? false : true,
                    hidden   : !data.subscriptions || data.is_lock_subscription ? true : false,
                    margin   : 5,
                    disabled : true,
                    listeners: {
                        'change' : function() {
                            if(this.checked) {
                                this.next('#person_company_panel').mask();
                            }
                            else {
                                this.next('#person_company_panel').unmask();   
                            }
                        }
                    }
                },
                {
                    xtype         : 'grid',
                    id            : 'importXpressSubscriptionGridSubscription',
                    title         : data.subscriptions ? 'Выберите подписку для связи или обновления' : 'Совпадений подписок не найдено',
                    width         : 600,
                    autoShow      : true,
                    allowDeselect : data.is_lock_subscription ? false : true,
                    hideHeaders   : data.subscriptions ? false : true,
                    store         : {
                        type      : 'subscription',
                        autoLoad  : true,
                        listeners : {
                            'beforeload' : function() {
                                if(!data.subscriptions) {
                                    return false;
                                }
                                this.getProxy().extraParams = {
                                    json_ids: Ext.JSON.encode(data.subscriptions)
                                }
                            },
                            'load' : function() {
                                if(data.is_lock_subscription) {
                                    Ext.getCmp('importXpressSubscriptionGridSubscription').getSelectionModel().select(0);
                                }
                            }
                        }
                    },
                    columns: [
                        {text: 'id', dataIndex: 'id', width: 50},
                        {text: 'Подписчик', dataIndex: 'name', flex: 1, tdCls: 'nx-multiline'},
                        {text: 'Состояние', dataIndex: 'order_state_name', width: 70},
                        {text: 'Издание', dataIndex: 'periodical_names', width: 100},
                        {text: 'Сумма', dataIndex: 'price', width: 70},
                        {text: 'Дата', dataIndex: 'date',  width: 110, renderer: Ext.util.Format.dateRenderer('Y-m-d')},
                        {
                            xtype   : 'actioncolumn',
                            iconCls : 'watch-ico',
                            tooltip : 'сравнить',
                            width   : 30, 
                            items   : [
                                {
                                    scope: this,
                                    handler: function(view, rowindex, colindex, item, e, rec)
                                    {
                                        var subscription_id = rec.get('id');
                                        Ext.Ajax.request({
                                            url     : '/subscription/compare-xpress-nx',
                                            method  : 'GET',
                                            params  : {
                                                xpress_id : xpress_id,
                                                subscription_id: subscription_id
                                            },
                                            scope   : this,
                                            success : function(response) {
                                                var response = Ext.JSON.decode(response.responseText);
                                                this.showXpressIdDetails(xpress_id, response.result);
                                            }
                                        });
                                    }
                                }
                            ]
                        }
                    ],
                    listeners: {
                        'selectionchange': function() {
                            //if(!data.is_lock_subscription)
                            //{
                                var selection = Ext.getCmp('importXpressSubscriptionGridSubscription').getSelectionModel().getSelection();
                                if(selection.length){
                                    this.prev('#is_just_link_subscription').enable().fireEvent('change');
                                }
                                else {
                                    this.prev('#is_just_link_subscription').disable();
                                } 
                            //}
                        }
                    }
                },
                {
                    xtype   : 'panel',
                    baseCls : 'x-plain',
                    itemId  : 'person_company_panel',
                    items   : [
                        {
                            xtype    : 'checkbox',
                            name     : 'is_just_link_person',
                            itemId   : 'is_just_link_person',
                            boxLabel : 'связывать физ. лица без обновления данных',
                            checked  : data.is_lock_person ? true : false,
                            hidden   : !data.persons ? true : false,
                            margin   : 5,
                            disabled : true
                        },
                        {
                            xtype         : 'grid',
                            id            : 'importXpressSubscriptionGridPerson',
                            title         : data.persons ? 'Выберите физ. лицо для связи или обновления' : 'Совпадений физ. лиц не найдено',
                            width         : 600,
                            autoShow      : true,
                            allowDeselect : data.is_lock_person ? false : true,
                            hideHeaders   : data.persons ? false : true,
                            store         : {
                                type      : 'person',
                                autoLoad  : true,
                                listeners : {
                                    'beforeload' : function() {
                                        if(!data.persons) return false;
                                        this.getProxy().extraParams = {
                                            json_ids: Ext.JSON.encode(data.persons)
                                        }
                                    },
                                    'load' : function() {
                                        if(data.is_lock_person)
                                        {
                                            Ext.getCmp('importXpressSubscriptionGridPerson').getSelectionModel().select(0);
                                        }
                                    }
                                }
                            },
                            columns: [
                                {text: 'id', dataIndex: 'id', width: 50},
                                {text: 'Имя', dataIndex: 'name', flex: 1},
                                {text: 'email', dataIndex: 'email', width: 170},
                                {text: 'Дата', dataIndex: 'created',  width: 120}
                            ],
                            listeners: {
                                'selectionchange': function() {
                                    //if(!data.is_lock_person)
                                    //{
                                        var selection = Ext.getCmp('importXpressSubscriptionGridPerson').getSelectionModel().getSelection();
                                        if(selection.length){
                                            this.prev('#is_just_link_person').enable();
                                        }
                                        else {
                                            this.prev('#is_just_link_person').disable();
                                        } 
                                    //}
                                }
                            }
                        },
                        {
                            xtype    : 'checkbox',
                            name     : 'is_just_link_company',
                            itemId   : 'is_just_link_company',
                            boxLabel : 'связывать организации без обновления данных',
                            checked  : data.is_lock_company ? true : false,
                            hidden   : !data.companies ? true : false,
                            margin   : 5,
                            disabled : true
                        },
                        {
                            xtype         : 'grid',
                            id            : 'importXpressSubscriptionGridCompany',
                            title         : data.companies ? 'Выберите организацию для связи или обновления' : 'Совпадений организаций не найдено',
                            width         : 600,
                            autoShow      : true,
                            allowDeselect : data.is_lock_company ? false : true,
                            hideHeaders   : data.companies ? false : true,
                            store         : {
                                type      : 'company',
                                autoLoad  : true,
                                listeners : {
                                    'beforeload' : function() {
                                        if(!data.companies) return false;
                                        this.getProxy().extraParams = {
                                            json_ids: Ext.JSON.encode(data.companies)
                                        }
                                    },
                                    'load' : function() {
                                        if(data.is_lock_company)
                                        {
                                            Ext.getCmp('importXpressSubscriptionGridCompany').getSelectionModel().select(0)
                                        }
                                    }
                                }
                            },
                            columns: [
                                {text: 'id', dataIndex: 'id', width: 50},
                                {text: 'Название', dataIndex: 'name', flex: 1},
                                {text: 'Дата', dataIndex: 'created',  width: 120}
                            ],
                            listeners: {
                                'selectionchange': function() {
                                    //if(!data.is_lock_company)
                                    //{
                                        var selection = Ext.getCmp('importXpressSubscriptionGridCompany').getSelectionModel().getSelection();
                                        if(selection.length){
                                            this.prev('#is_just_link_company').enable();
                                        }
                                        else {
                                            this.prev('#is_just_link_company').disable();
                                        } 
                                    //}
                                }
                            }
                        }
                    ]
                }
            ],
            buttonAlign : 'left', 
            buttons     : [
                {
                    text    : 'Назад',
                    scope   : this,
                    handler : function() {
                        Ext.Ajax.request({
                            url     : '/subscription/import-xpress',
                            method  : 'GET',
                            params  : {
                                xpress_id : xpress_id,
                                step      : 1
                            },
                            scope   : this,
                            success : function(response) {
                                //console.log(response);
                                var response = Ext.JSON.decode(response.responseText);
                                Ext.getCmp('importXpressSubscriptionWindow').close();
                                this.showXpressImportStep2Window(xpress_id, response.result);
                            }
                        });
                    }
                },
                '->',
                {
                    text    : 'Импортировать',
                    //scope   : this,
                    handler : function() 
                    {
                        var selectionSubscription     = Ext.getCmp('importXpressSubscriptionGridSubscription').getSelectionModel().getSelection();
                        var selectionPerson           = Ext.getCmp('importXpressSubscriptionGridPerson').getSelectionModel().getSelection();
                        var selectionCompany          = Ext.getCmp('importXpressSubscriptionGridCompany').getSelectionModel().getSelection();
                        var subscription_link_id      = 0;
                        var person_link_id            = 0;     
                        var company_link_id           = 0; 
                        var is_just_link_subscription = this.up('window').down('#is_just_link_subscription').getValue();
                        var is_just_link_person       = this.up('window').down('#is_just_link_person').getValue();
                        var is_just_link_company      = this.up('window').down('#is_just_link_company').getValue();

                        if(selectionSubscription.length)
                        {
                            subscription_link_id = selectionSubscription[0].get('id');
                        }
                        if(selectionPerson.length)
                        {
                            person_link_id = selectionPerson[0].get('id');
                        }
                        if(selectionCompany.length)
                        {
                            company_link_id = selectionCompany[0].get('id');
                        }
                        var confirm = 'Вы действительно хотите' + '<br/>'
                            + (subscription_link_id ? ' ' + (is_just_link_subscription ? 'связать' : 'обновить') + ' подписку #' + subscription_link_id : ' создать новую подписку') + '<br/>'
                            + (!is_just_link_subscription || !subscription_link_id ? ''
                                + (person_link_id ? ' ' + (is_just_link_person ? 'связать' : 'обновить') + ' физ. лицо #' + person_link_id : '') + '<br/>'
                                + (company_link_id ? ' ' + (is_just_link_company ? 'связать' : 'обновить') + ' организацию #' + company_link_id : '') + '?'
                            : '');

                        Ext.Msg.confirm('Подтверждение', confirm, function(btn){
                            if (btn == 'yes')
                            {
                                Ext.Ajax.request({
                                    url     : '/subscription/import-xpress',
                                    method  : 'GET',
                                    params  : {
                                        xpress_id                 : xpress_id,
                                        step                      : 3,
                                        subscription_link_id      : subscription_link_id,
                                        person_link_id            : person_link_id,
                                        company_link_id           : company_link_id,
                                        is_just_link_subscription : is_just_link_subscription,
                                        is_just_link_person       : is_just_link_person,
                                        is_just_link_company      : is_just_link_company
                                    },
                                    //scope   : this,
                                    success : function(response) {
                                        //console.log(response);
                                        var response = Ext.JSON.decode(response.responseText);
                                        Ext.getCmp('importXpressSubscriptionWindow').close();
                                        Ext.Msg.alert('', 'Импорт завершен');
                                        //this.showXpressImportStep3Window(xpress_id, response.result);
                                    }
                                });
                            }
                        });
                    }
                }
            ]
        });
    },
    convertDataToTree : function (data)
    {
        var results = [];
        if(data instanceof Array || Object.prototype.toString.call(data) === '[object Array]')
        {
            for(var i = 0, len = data.length; i < len; i++)
            {
                var record   = data[i];
                var children = this.convertDataToTree(record);
                results.push({
                    'text'     : i,
                    'children' : children,
                    //'expanded' : true,
                    'value'    : '',
                    'leaf'     : 0
                });
            }
        }
        else if(Object.prototype.toString.call(data) === '[object Object]')
        {
            for(var prop in data)
            {
                var value = data[prop];
                if(Object.prototype.toString.call(value) === '[object Object]' || Object.prototype.toString.call(value) === '[object Array]')
                {
                    var children = this.convertDataToTree(value);
                    results.push({
                        'text'     : prop,
                        'children' : children,
                        //'expanded' : true,
                        'value'    : '',
                        'leaf'     : 0
                    });
                }
                else
                {
                    results.push({
                        'text'  : prop,// + ': ' + value,
                        'value' : value,
                        'leaf'  : 1
                    });
                }
            }
        }
        else
        {
            for(var prop in data)
            {
                var value = data[prop];
                results.push({
                    'text'  : prop,// + ': ' + value,
                    'value' : value,
                    'leaf'  : 1
                });
            }
        }
        return results;
    }
});