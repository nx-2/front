/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.action' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_subscription_action',
    itemId: 'nx_subscription_action',

    title: 'Акции',

    store: 'action',

    initComponent: function() {

        this.columns = [
            {text: 'id',  dataIndex: 'id',  width: 50, filter: {xtype: 'textfield'}},
            {text: 'Название',  dataIndex: 'name',  flex: 1, filter: {xtype: 'textfield'}},
            {text: 'Email', dataIndex: 'email', width: 100},
            {text: 'Скидка', dataIndex: 'discount', width: 50},
            {text: 'Дата создания', dataIndex: 'created', width: 140},
            {text: 'Дата начала', dataIndex: 'date_begin', width: 140},
            {text: 'Дата конца', dataIndex: 'date_finish', width: 140},
            {text: 'Подписок', dataIndex: 'count', width: 80, renderer: function(val,meta,rec,rowindex,colindex,store,view){
                    var res = '';
                    res += (rec.get('count_pay') ? rec.get('count_pay') : 0) + ' / ' + (rec.get('count') ? rec.get('count') : 0);
                    return res;
                }
            },
            {
                xtype        : 'actioncolumn',
                width        : 40,
                menuDisabled : true,
                items        : [
                    {
                        icon: '/images/extjs/icons/watch.png',
                        tooltip: 'Показать тех. данные',
                        scope: this,
                        handler: function(grid, rowIndex, colIndex, item, e, rec) 
                        {
                            var id = rec.get('id');
                            Ext.Ajax.request({
                                url     : '/action/get',
                                method  : 'POST',
                                params  : {
                                    id : id
                                },
                                scope   : this,
                                success : function(response) {
                                    var response = Ext.JSON.decode(response.responseText);
                                    showDataAsTree(response.data);
                                }
                            });
                        }
                    },
                    {
                        icon    : '/images/extjs/icons/external.png',
                        tooltip : 'Ссылка',
                        scope   : this,
                        handler : function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            window.open('http://www.osp.ru/subscribe/discount/' + rec.get('code').split('').reverse().join(''),'_blank');
                        },
                        getClass: function(value, data, record, rowIndex, colIndex, store) 
                        {
                            var rec = store.getAt(rowIndex);
                            if(!rec.get('code') || !rec.get('page_id')) {
                                return 'x-hide-display';
                            }
                        }
                    }
                ]
            }
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    var css_class = '';
                    var date = new Date(rec.get('date_finish')).getTime();
                    css_class += rec.get('checked') == 0 || (rec.get('date_finish') && date<Ext.Date.now()) || rec.get('date_finish')=='0000-00-00 00:00:00' ? ' subscription-row-disabled' : ''; 
                    return css_class;
                } 
            } ,
            xfilters : [//filters property already taken up by filters feature 
                {fieldLabel: 'id', name: 'id', xtype: 'textfield'},
                {fieldLabel: 'название', name: 'name', xtype: 'textfield'},
                {fieldLabel: 'активная?', name: 'is_active', xtype: 'combo', store: [['1','да'],['0','нет']], editable: false},
            ]
        });

        Ext.apply(this, {
            dockedItems: [{
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных',
                plugins     : [{ptype: 'pagesize'}]
            },
            {
                xtype : 'toolbar',
                items : [
                    {
                        xtype: 'view_state_switcher',
                    },
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters(false);
                            this.store.clearFilter();
                        }
                    }
                ]
            }]
        });

        this.callParent(arguments);
    }
});