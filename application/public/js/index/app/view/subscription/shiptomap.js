/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.shiptomap' ,{
    extend : 'Ext.grid.Panel',
    alias  : 'widget.nx_subscription_shiptomap',
    title  : 'Плановые отгрузки в Почту России',

    initComponent: function() {

        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                autoLoad: false,
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/ship/shiptomap',
                    },
                    reader: {
                        type            : 'json',
                        root            : 'items',
                        successProperty : 'success',
                        totalProperty   : 'total'
                    }
                },
                remoteFilter: true,
                fields        : [
                    {name: 'is_partial'},
                    {name: 'is_ship'},
                    //{name: 'periodical_name'},
                    //{name: 'year'},
                    {name: 'subscription_id'},
                    // {name: 'payment_date'},
                    {name: 'qty1'},
                    {name: 'qty2'},
                    {name: 'qty3'},
                    {name: 'qty4'},
                    {name: 'qty5'},
                    {name: 'qty6'},
                    {name: 'qty7'},
                    {name: 'qty8'},
                    {name: 'qty9'},
                    {name: 'qty10'},
                    {name: 'qty11'},
                    {name: 'qty12'},
                    {name: 'sumqty'},
                    {name: 'deltype'},
                    {name: 'postcode'},
                    {name: 'region'},
                    {name: 'area'},
                    {name: 'city'},
                    {name: 'city1'},
                    {name: 'street'},
                    {name: 'dom'},
                    {name: 'korp'},
                    {name: 'str'},
                    {name: 'kv'},
                    {name: 'surname'},
                    {name: 'name'},
                    {name: 'patronymic'},
                    // {name: 'fio'}
                ]
            })
        });

        Ext.apply(this, {
            columns    : [
                //{header: 'Издание', dataIndex: 'periodical_name', width: 100, filter: {xtype: 'textfield'}},
                //{header: 'Год', dataIndex: 'year', width: 50, filter: {xtype: 'textfield'}},
                {xtype: 'rownumberer', width: 40},
                {header: 'ID подписки', dataIndex: 'subscription_id', width: 70},
                // {header: 'Оплата', dataIndex: 'payment_date', width: 70},
                {header: 'QTY1', dataIndex: 'qty1', width: 40},
                {header: 'QTY2', dataIndex: 'qty2', width: 40},
                {header: 'QTY3', dataIndex: 'qty3', width: 40},
                {header: 'QTY4', dataIndex: 'qty4', width: 40},
                {header: 'QTY5', dataIndex: 'qty5', width: 40},
                {header: 'QTY6', dataIndex: 'qty6', width: 40},
                {header: 'QTY7', dataIndex: 'qty7', width: 40},
                {header: 'QTY8', dataIndex: 'qty8', width: 40},
                {header: 'QTY9', dataIndex: 'qty9', width: 40},
                {header: 'QTY10', dataIndex: 'qty10', width: 40},
                {header: 'QTY11', dataIndex: 'qty11', width: 40},
                {header: 'QTY12', dataIndex: 'qty12', width: 40},
                {header: 'SUMQTY', dataIndex: 'sumqty', width: 50},
                {header: 'DELTYPE', dataIndex: 'deltype', flex: 1},
                {header: 'POSTCODE', dataIndex: 'postcode', width: 70},
                {header: 'REGION', dataIndex: 'region', flex: 1},
                {header: 'AREA', dataIndex: 'area', flex: 1},
                {header: 'CITY', dataIndex: 'city', flex: 1},
                {header: 'CITY1', dataIndex: 'city1', flex: 1},
                {header: 'STREET', dataIndex: 'street', flex: 1},
                {header: 'DOM', dataIndex: 'dom', width: 50},
                {header: 'KORP', dataIndex: 'korp', width: 50},
                {header: 'STR', dataIndex: 'str', width: 50},
                {header: 'KV', dataIndex: 'kv', width: 50},
                // {header: 'FIO', dataIndex: 'fio', flex: 2},
                {header: 'SURNAME', dataIndex: 'surname', flex: 2},
                {header: 'NAME', dataIndex: 'name', flex: 2},
                {header: 'PATRONYMIC', dataIndex: 'patronymic', flex: 2},
                // {header: 'FULLNAME', dataIndex: 'fio', flex: 2}
            ]
        });

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    var css_class = '';
                    css_class += rec.get('is_partial') == 1 ? ' ship-row-partial' : '';
                    css_class += rec.get('is_ship') == 1 ? ' nx-transparent' : '';
                    if (rec.raw.has_duplicate == 1) {
                        css_class += rec.raw.has_duplicate == 1 ? ' ship-row-has-duplicate' : '';
                        store.hasDuplicates = true;
                    }
                    return css_class;
                } 
            }
        });
    
        Ext.apply(this, {     
            dockedItems: [/*{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },*/
            {
                xtype  : 'toolbar',
                layout : {
                    type  : 'vbox',
                    align :'stretch'
                },
                items : [
                    {
                        xtype     : 'panel', 
                        border    : false,
                        style     : {marginLeft: '5px'},
                        bodyStyle : 'background:none;',
                        html      : 'Учитываются включенные, почтовые, оплаченные или бесплатные подписки на физ. лица, не помеченные как конверт, не рассылки. Подсвечиваются заказы на неполный месячный комплект.'
                    },
                    {
                        xtype  : 'toolbar',
                        border : false,
                        flex   : 1,
                        items  : [
                            {
                                text    : 'Искать',
                                iconCls : 'search-ico',
                                scope   : this,
                                handler : function() {
                                    this.reloadStore();
                                }
                            },
                            /*{
                                text    : 'Сброс поиска',
                                iconCls : 'clear-ico',
                                scope   : this,
                                handler : function() {
                                    this.resetHeaderFilters();
                                }
                            },*/
                            '-',
                            {
                                text   : 'Экспорт CSV',
                                iconCls: 'export-csv-ico',
                                scope  : this, 
                                handler: function() 
                                {
                                    Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                        if(btn=='yes')
                                        {
                                            var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                            submitFakeForm('/index/export-data', data, 'Подписки в Почту России');
                                        }
                                    },this);   
                                }
                            },
                            '-',
                            {
                                xtype           : 'combo',
                                itemId          : 'periodical_id',
                                store           : subscribleMagazinesStore,
                                //value         : 1,
                                editable        : false,
                                queryMode       : 'local',
                                triggerAction   : 'all',
                                forceSelection  : true,
                                enableKeyEvents : true,
                                listeners       : 
                                {
                                    scope: this,
                                    'select': function(field, e) 
                                    {
                                        this.reloadStore();
                                    }
                                }
                            },
                            {
                                xtype           : 'textfield',
                                itemId          : 'year',
                                width           : 50,
                                value           : Ext.Date.format(new Date(), 'Y'),
                                regex           : /^\d{4}$/,
                                enableKeyEvents : true,
                                listeners       : 
                                {
                                    scope: this,
                                    'keydown': function(field, e) 
                                    {
                                        if(field.isValid() && e.getCharCode() == 13)
                                        {
                                            this.reloadStore();
                                        }
                                    }
                                }
                            },
                            {
                                xtype           : 'textfield',
                                itemId          : 'month_start',
                                width           : 50,
                                value           : Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, 1), 'm'),
                                regex           : /^\d{1,2}$/,
                                enableKeyEvents : true,
                                listeners       : 
                                {
                                    scope: this,
                                    'keydown': function(field, e) 
                                    {
                                        if(field.isValid() && e.getCharCode() == 13)
                                        {
                                            this.reloadStore();
                                        }
                                    }
                                }
                            },
                            {html:'-'},
                            {
                                xtype           : 'textfield',
                                itemId          : 'month_end',
                                width           : 50,
                                value           : Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, 1), 'm'),
                                regex           : /^\d{1,2}$/,
                                enableKeyEvents : true,
                                listeners       : 
                                {
                                    scope: this,
                                    'keydown': function(field, e) 
                                    {
                                        if(field.isValid() && e.getCharCode() == 13)
                                        {
                                            this.reloadStore();
                                        }
                                    }
                                }
                            },
                            /*{
                                xtype     : 'panel',
                                border    : false,
                                style     : {marginLeft: '10px'},
                                bodyStyle : 'background:none;',
                                flex      : 1,
                                html      : 'Учитываются включенные, почтовые, оплаченные или бесплатные подписки на физ. лица, не помеченные как конверт, не рассылки. Подсвечиваются заказы на неполный месячный комплект.'
                            },*/
                            '->',
                            {
                                text    : 'Закрыть отгрузку',
                                scope   : this,
                                nx_data : {state: 1},
                                handler : function(btn) {
                                    this.setLoading(true);
                                    Ext.Ajax.request({
                                        url     : '/subscription/toggle-map-close',
                                        method  : 'POST',
                                        params  : {
                                            state: btn.nx_data.state
                                        },
                                        scope   : this,
                                        success : function(response) {
                                            this.setLoading(false);
                                            var response = Ext.JSON.decode(response.responseText);
                                            btn.refreshState();
                                        }
                                    });
                                },
                                refreshState: function()
                                {
                                    this.setLoading(true);
                                    Ext.Ajax.request({
                                        url     : '/subscription/check-map-close',
                                        method  : 'POST',
                                        scope   : this,
                                        success : function(response) {
                                            this.setLoading(false);
                                            var response = Ext.JSON.decode(response.responseText);
                                            if(response.result) {
                                                this.nx_data.state = 0;
                                                this.setText('Открыть отгрузку');
                                            }
                                            else {
                                                this.nx_data.state = 1;
                                                this.setText('Закрыть отгрузку');
                                            }
                                        }
                                    });
                                },
                                listeners: {
                                    'afterrender': function() {
                                        this.refreshState(); 
                                    }
                                }
                            },
                            {
                                text     : 'Все',
                                scope    : this,
                                itemId   : 'nx_ship_state_btn',
                                scope    : this,
                                nx_state : 0,
                                handler  : function(btn) 
                                {
                                    btn.setState((btn.nx_state+1)%3);
                                    this.store.reload();
                                },
                                setState : function(state)
                                {
                                    var store = this.up('grid').getStore();
                                    this.nx_state = state;
                                    if(this.nx_state==0) {
                                        this.setText('Все');
                                        delete store.getProxy().extraParams.is_ship;
                                        //store.filters.removeAtKey('is_ship');
                                    }
                                    if(this.nx_state==1) {
                                        this.setText('Не отгруженные');
                                        store.getProxy().extraParams.is_ship = 0;
                                        /*store.filters.add('is_ship', new Ext.util.Filter({
                                            property: 'is_ship',
                                            value   : 0
                                        }));*/
                                    }
                                    if(this.nx_state==2) {
                                        this.setText('Отгруженные');
                                        store.getProxy().extraParams.is_ship = 1;
                                        /*store.filters.add('is_ship', new Ext.util.Filter({
                                            property: 'is_ship',
                                            value   : 1
                                        }));*/
                                    }
                                }
                            },
                            '-',
                            {
                                text    : 'Отправить',
                                scope   : this,
//                                hidden  : !nx.app.isperm('ship_manager'),
                                hidden  : false,
                                handler : function() {
                                    if(!this.store.getCount())
                                    {
                                        Ext.Msg.alert('Ошибка', 'Не выбраны отгрузки.');
                                    }
                                    else
                                    {
                                        Ext.Msg.confirm('Подтверждение','Неполные месячные комлпекты не будут отправлены. Вы уверены?', function(btn){
                                            if(btn=='yes')
                                            {
                                                Ext.Ajax.request({
                                                    url     : '/ship/ship-by-map',
                                                    method  : 'POST',
                                                    params  : {
                                                        periodical_id : this.down('#periodical_id').getValue(),
                                                        year          : this.down('#year').getRawValue(),
                                                        month_start   : this.down('#month_start').getRawValue(),
                                                        month_end     : this.down('#month_end').getRawValue(),
                                                        //data          : Ext.ux.exporter.Exporter.exportGrid(this, 'csv')
                                                        /*filter: '[' + 
                                                            '{"property":"periodical_id","value":"' + this.down('#periodical_id').getValue() + '"},' +
                                                            '{"property":"year","value":"' + this.down('#year').getRawValue() + '"},' +
                                                            '{"property":"month_start","value":"' + this.down('#month_start').getRawValue() + '"},' +
                                                            '{"property":"month_end","value":"' + this.down('#month_start').getRawValue() + '"}' + 
                                                        ']'*/
                                                    },
                                                    scope   : this,
                                                    success : function(response) {
                                                        var response = Ext.JSON.decode(response.responseText);
                                                        if(response.result==false)
                                                        {
                                                            Ext.Msg.alert('Ошибка', 'Не выбраны отгрузки, либо все уже отгружено.');
                                                        }
                                                    }
                                                });
                                            }
                                        },this);  
                                    }
                                }
                            }
                        ]
                    }
                ]
            }],
        });

        this.callParent(arguments);

        /*if(this.nxparams.filter)
        {
            var combo = this.down('#periodical_id');
            var filters = this.nxparams.filter.split('_');
            this.down('#year').setValue(filters[1]);
            this.down('#month_start').setValue(filters[2]);
            //this.down('#month_end').setValue(filters[2]);
            combo.select(combo.getStore().getAt(combo.getStore().find('field2',filters[0])));
            this.reloadStore();    
        }*/
        var filtered = 0;
        if(this.nxparams.is_ship)
        {
            filtered = 1;
            this.down('#nx_ship_state_btn').setState(parseInt(this.nxparams.is_ship)+1);
        }
        if(this.nxparams.periodical_id)
        {
            filtered = 1;
            var combo = this.down('#periodical_id');
            combo.select(combo.getStore().getAt(combo.getStore().find('field1',this.nxparams.periodical_id)));   
        }
        if(this.nxparams.year)
        {
            filtered = 1;
            this.down('#year').setValue(this.nxparams.year); 
        }
        if(this.nxparams.month)
        {
            filtered = 1;
            this.down('#month_start').setValue(this.nxparams.month);
            this.down('#month_end').setValue(this.nxparams.month);
        }
        if(filtered) {
            this.reloadStore();
        }
    },

    reloadStore: function()
    {
        this.store.hasDuplicates = false;
        this.store.load({
            params: {
                /*filter: '[' +
                    '{"property":"periodical_id","value":"' + this.down('#periodical_id').getValue() + '"},' +
                    '{"property":"year","value":"' + this.down('#year').getRawValue() + '"},' +
                    '{"property":"month_start","value":"' + this.down('#month_start').getRawValue() + '"},' +
                    '{"property":"month_end","value":"' + this.down('#month_start').getRawValue() + '"}' +
                ']'*/
                periodical_id : this.down('#periodical_id').getValue(),
                year          : this.down('#year').getRawValue(),
                month_start   : this.down('#month_start').getRawValue(),
                month_end     : this.down('#month_end').getRawValue()
                //'yearmonth_start': Ext.ComponentQuery.query('#yearmonth_start')[0].getRawValue(),
                //'yearmonth_end': Ext.ComponentQuery.query('#yearmonth_end')[0].getRawValue()
            },
            scope: this,
            callback: function(records, operation, success) {
                // the operation object
                // contains all of the details of the load operation
                // console.log(operation);
                if (this.store.hasDuplicates) {
                    Ext.Msg.alert('Внимание', 'В отображенном списке обнаружены предположительно дубликаты. Они помечены красным фоном.Возможно требуется корректировка этих заказов.');
                }
            }
        });
    }
});