/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.add_csv', {
    extend     : 'Ext.window.Window',
    alias      : 'widget.nx_subscription_add_csv',
    title      : 'Добавление подписки из csv файла',
    autoShow   : true,
    width      : 900,
    autoScroll : true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title   : 'свойства',
                                padding : 5,
                                layout  : 'form',
                                border  : 0,
                                items   : [
                                    {
                                        xtype         : 'combo',
                                        name          : 'type',
                                        store         : subscription_type_store,
                                        fieldLabel    : 'тип',
                                        editable      : false,
                                        queryMode     : 'local',
                                        triggerAction : 'all',
                                        value         : '0'
                                    },
                                    {
                                        xtype          : 'combobox',
                                        name           : 'order_state_id',
                                        store          : orderStatesStore,
                                        fieldLabel     : 'Статус',
                                        editable       : false,
                                        queryMode      : 'local',
                                        forceSelection : true,
                                        triggerAction  : 'all',
                                        value          : '2'
                                    },
                                    {
                                        xtype         : 'combobox',
                                        name          : 'payment_type_id',
                                        store         : payment_types_store,
                                        fieldLabel    : 'Способ оплаты',
                                        editable      : false,
                                        forceSelection : true,
                                        queryMode     : 'local',
                                        triggerAction : 'all',
                                        value         : '1'
                                    },
                                    {
                                        layout: 'hbox',
                                        border: 0,
                                        items : [
                                            {
                                                xtype      : 'textfield',
                                                name       : 'label',
                                                //value      : Ext.Date.format(new Date(), 'Ymd_His') + '_subscription' ,
                                                fieldLabel : 'Метка',
                                                allowBlank : false,
                                                width      : 400
                                            },
                                            {
                                                html  : '_nx_csv_subscription',
                                                border: 0
                                            }
                                        ]
                                    },
                                    {
                                        //hidden       : !nx.app.isperm('admin'),
                                        xtype        : 'comboboxselect',
                                        name         : 'tags[]',
                                        fieldLabel   : 'Теги',
                                        displayField : 'name',
                                        valueField   : 'id',
                                        //itemId       : 'nx_edit_tags',
                                        minChars     : 2,
                                        //grow         : false,
                                        //queryMode    : 'remote',
                                        store        : new Ext.data.JsonStore({
                                            proxy: {
                                                type: 'ajax',
                                                url : '/tag/get-objects-for-combo',
                                                reader: {
                                                    type            : 'json',
                                                    root            : 'objects',
                                                    successProperty : 'success',
                                                    totalProperty   : 'total'
                                                },
                                            },
                                            root       : 'objects',
                                            fields     : [
                                                {name : 'id',   type : 'int'},
                                                {name : 'name', type : 'string'}
                                            ],
                                        })
                                    },
                                ]
                            },
                            {
                                title   : 'Адрес доставки',
                                layout  : 'form',
                                padding : 5,
                                items   : [
                                    {
                                        xtype          : 'checkbox',
                                        name           : 'map_is_enveloped',
                                        fieldLabel     : 'конвертом, не в МАП?',
                                        inputValue     : '1',
                                        uncheckedValue : '0'
                                    }
                                ]
                            },
                            {
                                title   : 'Издания',
                                padding : 5,
                                items   : [
                                    {
                                        xtype : 'subscription_magazines_editor',
                                        itemId: 'subscriptionMagazinesEditor',
                                        border: 0
                                    }
                                ]
                            },
                            {
                                title   : 'csv файл',
                                layout  : 'form',
                                padding : 5,
                                items   : [
                                    {
                                        xtype      : 'filefield',
                                        name       : 'file',
                                        fieldLabel : 'Файл',
                                        labelWidth : 50,
                                        allowBlank : false,
                                        buttonText : 'Выбрать CSV файл'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        fieldLabel : 'кроме строк (п: 4,7)',
                                        name       : 'pass_rows'
                                    },
                                    {
                                        xtype : 'csv_to_db_editor',
                                        //itemId: 'subscriptionMagazinesEditor',
                                        border: 0,
                                        fields: [//post name, label
                                            ['order_email', 'email заказа'],
                                            ['person_f' , 'фамилия'],
                                            ['person_i' , 'имя'],
                                            ['person_o' , 'отчество'],
                                            ['person_name' , 'фио'],
                                            ['person_position', 'должность'],
                                            ['address_address' , 'адрес'],
                                            ['address_phone' , 'телефон'],
                                            ['address_zipcode' , 'индекс'],
                                            ['address_area' , 'область'],
                                            ['address_city' , 'город'],
                                            ['address_street' , 'улица'],
                                            ['address_house' , 'дом'],
                                            ['address_building' , 'корпус'],
                                            ['address_apartment' , 'квартира'],
                                            ['address_comment' , 'населенный пункт'],
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text   : 'Проверить',
                action : 'add'
            },
            {
                text    : 'Закрыть',
                action  : 'close',
                scope   : this,
                handler : this.close
            }
        ];

        this.callParent(arguments);
    }
});