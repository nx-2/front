/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.shiptable' ,{
    extend: 'Ext.grid.Panel',
    itemId: 'nx_subscription_shiptable',

    title: 'Плановые отгрузки - статистика таблицей',
    nx_data: {
        show_num   : true,
        show_count : true,
        show_sum   : true
    },

    initComponent: function() 
    {
        var fields = [
            {name: 'yearmonth'}
        ];
        Ext.each(subscribleMagazinesStore, function(magazine){
            if(magazine[0]) {
                fields[fields.length] = {name: magazine[1]};
            }
        });

        var columns = [
            {text: 'год-месяц', dataIndex: 'yearmonth', width: 100}
        ];
        Ext.each(subscribleMagazinesStore, function(magazine){
            if(magazine[0]) {
                columns[columns.length] = {text:  magazine[1], dataIndex: magazine[1], flex: 1, renderer: this.cellRenderer};
            }
        }, this);

        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/ship/shiptable',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter : true,
                fields       : fields,
                autoLoad     : false
            })
        });

        Ext.apply(this, {
            columns : columns
        });

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            },
            xfilters : [
                //{fieldLabel: 'метка', name: 'label', xtype: 'textfield', ops: ['=','!=']},
                {fieldLabel: 'тип подписчика', name: 'subscriber_type_id', xtype: 'combo', store: subscriber_type_store},
                //{fieldLabel: 'тип', name: 'order_type', xtype: 'combo', store: subscription_type_store},
                {fieldLabel: 'id акции', name: 'action_id', xtype: 'textfield'},
                {fieldLabel: 'год-месяц начало', name: 'yearmonth_start', xtype: 'textfield'},
                {fieldLabel: 'год-месяц конец', name: 'yearmonth_end', xtype: 'textfield'},
            ]
        });
    
        Ext.apply(this, {     
            dockedItems: [/*{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },*/
            {
                xtype: 'toolbar',
                items : [
                    {
                        xtype: 'view_state_switcher',
                    },
                    /*{
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.reloadStore();
                        }
                    },*/
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            //this.resetHeaderFilters(false);
                            this.store.clearFilter(true);
                            this.down('#period-switcher-quarter').handler.call(this.down('#period-switcher-quarter'));
                        }
                    },
                    '-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Статистика');
                                }
                            },this);   
                        }
                    },
                    /*{
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            Ext.ComponentQuery.query('#yearmonth_start')[0].setRawValue(Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), 'Ym'));
                            Ext.ComponentQuery.query('#yearmonth_end')[0].setRawValue(Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, 1), 'Ym'));
                            this.reloadStore();
                        }
                    },*/
                    {
                        icon: '/images/extjs/icons/list-prev.png',
                        handler: function() {
                            var step      = Ext.ComponentQuery.query('button[cls=period-switcher][pressed=true]')[0].step;
                            var grid      = this.up('grid');
                            var filters   = grid.store.filters;
                            var cur_start = filters.getAt(filters.findIndex('property', new RegExp('^yearmonth_start$'))).value;
                            var cur_end   = filters.getAt(filters.findIndex('property', new RegExp('^yearmonth_end$'))).value;
                            var cur_start_month = cur_start.slice(4,6);
                            grid.applyYearMonthFilters(
                                Ext.Date.format(Ext.Date.add(new Date(cur_start.slice(0,4) + '-' + cur_start.slice(4,6) + '-01'), Ext.Date.MONTH, -step+(cur_start_month - step == 0 ? 1 : 0 ) ), 'Ym'),
                                Ext.Date.format(Ext.Date.add(new Date(cur_end.slice(0,4) + '-' + cur_end.slice(4,6) + '-01'), Ext.Date.MONTH, -step), 'Ym')
                            );
                            /*var cur_start = Ext.ComponentQuery.query('#yearmonth_start')[0].getRawValue();
                            var cur_end   = Ext.ComponentQuery.query('#yearmonth_end')[0].getRawValue();
                            var cur_start_month = cur_start.slice(4,6);
                            Ext.ComponentQuery.query('#yearmonth_start')[0].setRawValue(Ext.Date.format(Ext.Date.add(new Date(cur_start.slice(0,4) + '-' + cur_start.slice(4,6) + '-01'), Ext.Date.MONTH, -step+(cur_start_month - step == 0 ? 1 : 0 ) ), 'Ym'));
                            Ext.ComponentQuery.query('#yearmonth_end')[0].setRawValue(Ext.Date.format(Ext.Date.add(new Date(cur_end.slice(0,4) + '-' + cur_end.slice(4,6) + '-01'), Ext.Date.MONTH, -step), 'Ym'));
                            this.up('grid').reloadStore();*/
                        }
                    },
                    {
                        text: 'Квартал',
                        enableToggle : true,
                        cls: 'period-switcher',
                        step: 3,
                        itemId: 'period-switcher-quarter',
                        handler: function() {
                            Ext.each(Ext.ComponentQuery.query('button[cls=period-switcher]'), function(btn) {
                                btn.toggle(0);
                                this.toggle(1);
                            }, this);
                            var cur_date    = new Date();
                            var cur_month   = cur_date.getMonth();
                            var start_month = '01';
                            var end_month   = '03'; 
                            if(cur_month>3 && cur_month<=6) {
                                start_month = '04';
                                end_month   = '06'; 
                            }
                            if(cur_month>6 && cur_month<=9) {
                                start_month = '07';
                                end_month   = '09'; 
                            }
                            if(cur_month>9) {
                                start_month = '10';
                                end_month   = '12'; 
                            }
                            //Ext.ComponentQuery.query('#yearmonth_start')[0].setRawValue(Ext.Date.format(new Date(), 'Y' + start_month));
                            //Ext.ComponentQuery.query('#yearmonth_end')[0].setRawValue(Ext.Date.format(new Date(), 'Y' + end_month));
                            //this.up('grid').reloadStore();
                            this.up('grid').applyYearMonthFilters(
                                Ext.Date.format(new Date(), 'Y' + start_month),
                                Ext.Date.format(new Date(), 'Y' + end_month)
                            );
                        }
                    },
                    {
                        text: 'Полгода',
                        enableToggle : true,
                        cls: 'period-switcher',
                        step: 6,
                        itemId: 'period-switcher-halfyear',
                        handler: function() {
                            Ext.each(Ext.ComponentQuery.query('button[cls=period-switcher]'), function(btn) {
                                btn.toggle(0);
                                this.toggle(1);
                            }, this);
                            var cur_date    = new Date();
                            var start_month = '01';
                            var end_month   = '06'; 
                            if(cur_date.getMonth()>5) {
                                start_month = '07';
                                end_month   = '12'; 
                            }
                            /*Ext.ComponentQuery.query('#yearmonth_start')[0].setRawValue(Ext.Date.format(new Date(), 'Y' + start_month));
                            Ext.ComponentQuery.query('#yearmonth_end')[0].setRawValue(Ext.Date.format(new Date(), 'Y' + end_month));
                            this.up('grid').reloadStore();*/
                            this.up('grid').applyYearMonthFilters(
                                Ext.Date.format(new Date(), 'Y' + start_month),
                                Ext.Date.format(new Date(), 'Y' + end_month)
                            );
                        }
                    },
                    /*{
                        text: 'Год',
                        enableToggle : true,
                        cls: 'period-switcher',
                        step: 12,
                        itemId: 'period-switcher-year',
                        pressed: true,
                        handler: function() {
                            Ext.each(Ext.ComponentQuery.query('button[cls=period-switcher]'), function(btn) {
                                btn.toggle(0);
                                this.toggle(1);
                            }, this);
                            Ext.ComponentQuery.query('#yearmonth_start')[0].setRawValue(Ext.Date.format(new Date(), 'Y01'));
                            Ext.ComponentQuery.query('#yearmonth_end')[0].setRawValue(Ext.Date.format(new Date(), 'Y12'));
                            this.up('grid').reloadStore();
                        }
                    },*/
                    {
                        icon: '/images/extjs/icons/list-next.png',
                        handler: function() {
                            var step      = Ext.ComponentQuery.query('button[cls=period-switcher][pressed=true]')[0].step;
                            var grid      = this.up('grid');
                            var filters   = grid.store.filters;
                            var cur_start = filters.getAt(filters.findIndex('property', new RegExp('^yearmonth_start$'))).value;
                            var cur_end   = filters.getAt(filters.findIndex('property', new RegExp('^yearmonth_end$'))).value;
                            grid.applyYearMonthFilters(
                                Ext.Date.format(Ext.Date.add(new Date(cur_start.slice(0,4) + '-' + cur_start.slice(4,6) + '-01'), Ext.Date.MONTH, step), 'Ym'),
                                Ext.Date.format(Ext.Date.add(new Date(cur_end.slice(0,4) + '-' + cur_end.slice(4,6) + '-01'), Ext.Date.MONTH, step), 'Ym')
                            );
                            //var cur_start = Ext.ComponentQuery.query('#yearmonth_start')[0].getRawValue();
                            //var cur_end   = Ext.ComponentQuery.query('#yearmonth_end')[0].getRawValue();
                            //Ext.ComponentQuery.query('#yearmonth_start')[0].setRawValue(Ext.Date.format(Ext.Date.add(new Date(cur_start.slice(0,4) + '-' + cur_start.slice(4,6) + '-01'), Ext.Date.MONTH, step), 'Ym'));
                            //Ext.ComponentQuery.query('#yearmonth_end')[0].setRawValue(Ext.Date.format(Ext.Date.add(new Date(cur_end.slice(0,4) + '-' + cur_end.slice(4,6) + '-01'), Ext.Date.MONTH, step), 'Ym'));
                            //this.up('grid').reloadStore();
                        }
                    },
                    /*{
                        xtype: 'textfield',
                        //fieldLabel: 'Даты',
                        itemId: 'yearmonth_start',
                        //format: 'Ym',
                        value: Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), 'Ym'),
                        regex: /^\d{4}\d{2}$/,
                        enableKeyEvents: true,
                        listeners: 
                        {
                            scope: this,
                            'keydown': function(field, e) 
                            {
                                if(field.isValid() && e.getCharCode() == 13)
                                {
                                    this.reloadStore();
                                }
                            }
                        }
                    },
                    {html:'-'},
                    {
                        xtype: 'textfield',
                        itemId: 'yearmonth_end',
                        //format: 'Ym',
                        value: Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, 1), 'Ym'),
                        regex: /^\d{4}\d{2}$/,
                        enableKeyEvents: true,
                        listeners: 
                        {
                            scope: this,
                            'keydown': function(field, e) 
                            {
                                if(field.isValid() && e.getCharCode() == 13)
                                {
                                    this.reloadStore();
                                }
                            }
                        }
                    },*/
                    {
                        text         : '#',
                        scope        : this,
                        enableToggle : true,
                        pressed      : this.nx_data.show_num,
                        handler      : function(btn) {
                            this.nx_data.show_num = btn.pressed;
                            this.reconfigure();
                        }
                    },
                    {
                        text         : 'pdf / бум.',
                        scope        : this,
                        enableToggle : true,
                        pressed      : this.nx_data.show_count,
                        handler      : function(btn) {
                            this.nx_data.show_count = btn.pressed;
                            this.reconfigure();
                        }
                    },
                    {
                        text         : 'сумма',
                        scope        : this,
                        enableToggle : true,
                        pressed      : this.nx_data.show_sum,
                        handler      : function(btn) {
                            this.nx_data.show_sum = btn.pressed;
                            this.reconfigure();
                        }
                    },
                    '->',
                    {
                        text    : 'Почта России',
                        iconCls : 'ship-ico',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'subscription/shiptomap';
                        }
                    },
                    /*'-',
                    {
                        text    : 'Отгрузки',
                        iconCls : 'ship-ico',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'subscription/ship';
                        }
                    },*/
                    '-',
                    {
                        text    : 'Списком',
                        iconCls : 'ship-ico',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'subscription/shipcounts';
                        }
                    }
                ]
            }]
        });

        this.callParent(arguments);
        this.down('#period-switcher-quarter').handler.call(this.down('#period-switcher-quarter'));
        //this.reloadStore();
    },
    applyYearMonthFilters:function(start, end) {
        var filters = this.store.filters;
        var index   = filters.findIndex('property', 'yearmonth_start');
        if(index!=-1) {
            filters.removeAt(index);
        }
        var index = filters.findIndex('property', 'yearmonth_end');
        if(index!=-1) {
            filters.removeAt(index);
        }
        filters.add(new Ext.util.Filter({
            property: 'yearmonth_start',
            value   : start
        }));
        filters.add(new Ext.util.Filter({
            property: 'yearmonth_end',
            value   : end
        }));
        this.store.load();
    },
    /*reloadStore: function()
    {
        this.store.load({
            params: {
                filter: '[' + 
                    '{"property":"yearmonth_start","value":"' + Ext.ComponentQuery.query('#yearmonth_start')[0].getRawValue() + '"},' +
                    '{"property":"yearmonth_end","value":"' + Ext.ComponentQuery.query('#yearmonth_end')[0].getRawValue() + '"}' + 
                ']'
                //'yearmonth_start': Ext.ComponentQuery.query('#yearmonth_start')[0].getRawValue(),
                //'yearmonth_end': Ext.ComponentQuery.query('#yearmonth_end')[0].getRawValue()
            }
        });
    },*/
    /*getColumns: function(renderer) {
        var columns = [
            {text: 'год-месяц', dataIndex: 'yearmonth', width: 100}
        ];
        Ext.each(subscribleMagazinesStore, function(magazine){
            if(magazine[0]) {
                columns[columns.length] = {text:  magazine[1], dataIndex: magazine[1], flex: 1, renderer: renderer};
            }
        }, this);
        return columns;
    },*/
    cellRenderer: function(value, meta, rec, rowIndex, colIndex, store, view) 
    {
        var is_export       = meta['exporter'] ? true : false;
        var grid            = view.up('grid');
        var cfg             = grid.nx_data;
        var periodical_name = grid.columns[colIndex].dataIndex;
        var yearmonth       = rec.get('yearmonth') + '';
        var year            = yearmonth.slice(0,4);
        var month           = yearmonth.slice(4,6);
        var content         = '';
        if(cfg.show_num) {
            for(issue in value) {
                //content += '<div class="nx-subscription-ship' + (value[issue]['state'] ? '-sended' : '') + '"><a title="подробнее" href="" onclick="Ext.getCmp(\'' + this.id  + '\').showIssueCounts(\'' + this.id + '\', \'' + periodical_name + '\', \'' + yearmonth + '\', \'' + issue + '\'); return false;">#' + issue + '</a> ';
                content += !is_export ? '<div class="nx-subscription-ship' + (value[issue]['state'] ? '-sended' : '') + '">' : '';
                content += cfg.show_num && !is_export ? '<a title="подробнее" href="" onclick="window.open(\'/#subscription/shipcounts/periodical_name/' + periodical_name + '/issue_num/' + issue + '/year/' + year + '\', \'_blank\'); return false;">' : '';
                content += cfg.show_num ? '#' + issue : '';
                content += cfg.show_num && !is_export ? '</a>' : '';
                content += cfg.show_num && cfg.show_count ? ' ' : '';
                content += cfg.show_count ? value[issue]['types']['pdf'] + '/' + value[issue]['types']['paper'] : '';
                content += cfg.show_sum && (cfg.show_num || cfg.show_count) ? (is_export ? " " : '<br/>') : '';
                content += cfg.show_sum ? value[issue]['total_price'].toString().replace('.',',') : '';     
                content += cfg.show_num && !is_export ? "</div>" : '';
                //content += (count ? '<a title="' + type + '" href="" onclick="Ext.getCmp(\'' + this.id  + '\').cellCountCallback(\'' + periodical_name + '\', \'' + yearmonth + '\', \'' + issue + '\', \'' + type + '\'); return false;">' : '') + count + (count ? '</a>' : '') + '/';        
            };
        }
        else {
            var count_pdf       = 0;
            var count_paper     = 0;
            var total_price     = 0;
            for(issue in value) {
                count_pdf   += value[issue]['types']['pdf'];
                count_paper += value[issue]['types']['paper'];
                total_price += value[issue]['total_price'];     
            };
            total_price = total_price.toFixed(2);
            content += cfg.show_count ? count_pdf + '/' + count_paper : '';
            content += cfg.show_sum && cfg.show_count ? (is_export ? " " : '<br/>') : '';
            content += cfg.show_sum ? total_price.toString().replace('.',',') : '';
        }
        return content;
    },
    /*cellCountCallback: function(periodical_name, yearmonth, issue, type)
    {
        ExtWindow = new Ext.window.Window({
            title     : 'Плановые отгрузки ' + periodical_name + ' ' + yearmonth + ' ' + issue + ' ' + type,
            autoShow  : true,
            width     : 800,
            maxHeight : 500,
            layout    : 'fit',
            y         : 100,
            //modal: true,
            items: [
                {
                    xtype: 'grid',
                    store: new Ext.data.JsonStore({
                        proxy: {
                            type: 'ajax',
                            url : '/subscription/shippings',
                            reader: {
                                type: 'json',
                                root: 'items',
                                successProperty: 'success',
                                totalProperty: 'total'
                            },
                            extraParams: {
                                filter: '[' + 
                                    '{"property":"periodical_name","value":"' + periodical_name + '"},' +
                                    '{"property":"yearmonth","value":"' + yearmonth + '"},' + 
                                    '{"property":"issue_num","value":"' + issue + '"},' + 
                                    '{"property":"type","value":"' + type + '"}' + 
                                ']'
                            }
                        },
                        pageSize: 9999,
                        remoteFilter: true,
                        autoLoad : true,
                        fields        : [
                            {name: 'name'},
                            {name: 'email'},
                            {name: 'periodical_name'},
                            {name: 'issue_title'},
                            {name: 'type'},
                            {name: 'state'},
                            {name: 'sendtime'},
                            {name: 'subscription_id'}
                        ]
                    }),
                    columns : [
                        {xtype: 'rownumberer', width: 40},
                        {text: 'Подписчик', dataIndex: 'name', flex: 1},
                        {text: 'Email', dataIndex: 'email', width: 170},
                        //{header: 'Издание', dataIndex: 'periodical_name', width: 100},
                        //{header: 'Выпуск', dataIndex: 'issue_title', width: 100},
                        //{header: 'Тип', dataIndex: 'type', width: 100},
                        {
                            text: 'Статус', dataIndex: 'state', width: 100, 
                            renderer: function(value) { if(value=='1') { return 'отправлено'; } else { return value; }}
                        },
                        {text: 'Дата отправки', dataIndex: 'sendtime', width: 130},
                        {text: 'id подписки', dataIndex: 'subscription_id', width: 70}
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    } 
                }
            ],
            dockedItems: [
            {
                xtype: 'toolbar',
                items : [
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        //scope  : this, 
                        handler: function() 
                        {
                            var grid = this.up('window').down('grid');
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + grid.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(grid, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Отгрузки');
                                }
                            },grid);  
                        }
                    }
                ]
            }]
        });
    },*/

    /*showIssueCounts: function(viewid, periodical_name, yearmonth, issue) {
        var year  = yearmonth.slice(0,4);
        var month = yearmonth.slice(4,6);
        new Ext.window.Window({
            title     : 'Плановые отгрузки ' + periodical_name + ' ' + yearmonth + ' ' + issue + ' - статистика',
            autoShow  : true,
            width     : 800,
            maxHeight : 500,
            layout    : 'fit',
            y         : 100,
            //modal   : true,
            items     : [
                {
                    xtype: 'grid',
                    store: new Ext.data.JsonStore({
                        proxy: {
                            type: 'ajax',
                            api: {
                                read: '/subscription/shipcounts',
                            },
                            reader: {
                                type: 'json',
                                root: 'items',
                                successProperty: 'success',
                                totalProperty: 'total'
                            },
                            extraParams: {
                                periodical_name : periodical_name,
                                yearmonth       : yearmonth,
                                issue_num       : issue
                            }
                        },
                        autoLoad     : true,
                        remoteFilter : true,
                        fields       : [
                            {name: 'id'},
                            {name: 'count'},
                            {name: 'count_paper'},
                            {name: 'count_pdf'},
                            {name: 'count_post'},
                            {name: 'count_map'},
                            {name: 'count_map_sended'},
                            {name: 'count_courier'},
                            {name: 'count_envelope'},
                            {name: 'count_envelope_dz'},
                            {name: 'count_envelope_org'}
                        ]
                    }),
                    columns    : [
                        //{text: 'ID', dataIndex: 'id', width: 60},
                        {text: 'Всего', dataIndex: 'count', width: 60},
                        {text: 'Бумага', dataIndex: 'count_paper', width: 60},
                        {text: 'PDF', dataIndex: 'count_pdf', width: 60},
                        {text: 'Почта', dataIndex: 'count_post', width: 60},
                        {text: 'МАП', dataIndex: 'count_map', width: 60, renderer: function(val, meta, rec){return val;
                            //var not_sended_count = val - rec.get('count_map_sended');
                            //return '<a href="" onclick="window.open(\'/#subscription/shiptomap/filter/' + periodical_name + '_' + year + '_' + month + '\', \'_blank\'); return false;">' + val + '</a>';
                            //return '<a href="" onclick="this.fireEvent(\'nx_shiptable_show\', this, rec);">' + val + '</a>';
                            //return '<a href="" onclick="Ext.getCmp(\'' + viewid + '\').showByXtypeInWindow(\'nx_subscription_shiptomap\', {periodical_name: \'' + periodical_name +'\', year: \'' + yearmonth.slice(0,4) + '\', month: \'' + yearmonth.slice(4,6) + '\'}); return false;">' + val + '</a>';
                        }},
                        {text: 'Курьер', dataIndex: 'count_courier', width: 60},
                        {text: 'Конверт', dataIndex: 'count_envelope', width: 60, renderer: function(val){return val;
                            //return '<a href="" onclick="window.open(\'/#subscription/ship\', \'_blank\'); return false;">' + val + '</a>';
                        }},
                        {text: 'Конверт ДЗ', dataIndex: 'count_envelope_dz', width: 80},
                        {text: 'Конверт юр.л.', dataIndex: 'count_envelope_org', width: 80}
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    } 
                }
            ]
        });
    }*/
});