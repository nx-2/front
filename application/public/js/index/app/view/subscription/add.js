/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.add', {
    extend     : 'Ext.window.Window',
    alias      : 'widget.nx_subscription_add',
    title      : 'Добавление подписки',
    autoShow   : true,
    width      : 900,
    autoScroll : true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title: 'свойства',
                                padding: 5,
                                layout: 'form',
                                border: 0,
                                items: [
                                    {
                                        xtype         : 'combo',
                                        name          : 'type',
                                        store         : subscription_type_store,
                                        fieldLabel    : 'тип',
                                        editable      : false,
                                        queryMode     : 'local',
                                        triggerAction : 'all',
                                        value         : '0'
                                    },
                                    {
                                        xtype          : 'combobox',
                                        name           : 'order_state_id',
                                        store          : orderStatesStore,
                                        fieldLabel     : 'Статус',
                                        editable       : false,
                                        queryMode      : 'local',
                                        forceSelection : true,
                                        triggerAction  : 'all',
                                        value          : '2'
                                    },
                                    {
                                        xtype         : 'combobox',
                                        name          : 'payment_type_id',
                                        store         : payment_types_store,
                                        fieldLabel    : 'Способ оплаты',
                                        editable      : false,
                                        forceSelection : true,
                                        queryMode     : 'local',
                                        triggerAction : 'all',
                                        value         : '1'
                                    },
                                    {
                                        xtype: 'textfield',
                                        name : 'comment',
                                        fieldLabel: 'Комментарий'
                                    },
                                    {
                                        xtype          : 'checkbox',
                                        name           : 'enabled',
                                        fieldLabel     : 'Включена?',
                                        inputValue     : '1',
                                        uncheckedValue : '0',
                                        checked        : true
                                    },
                                    {
                                        xtype: 'textfield',
                                        name : 'email',
                                        fieldLabel: 'Email',
                                        vtype: 'email'
                                    },
                                    {
                                        xtype: 'textfield',
                                        name : 'extra_emails',
                                        fieldLabel: 'Дополнительные Email (через ;)'
                                    },
                                    {
                                        xtype      : 'datefield',
                                        name       : 'payment_date',
                                        fieldLabel : 'Дата оплаты',
                                        format     : 'Y-m-d',
                                        editable   : false
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'payment_docno',
                                        fieldLabel : 'Номер док. оплаты'
                                    },
                                    {
                                        border: 0,
                                        layout: 'hbox',
                                        items: [
                                            {
                                                xtype  : 'textfield',
                                                hidden : true,
                                                name   : 'person_id',
                                                itemId : 'nx_add_person_id'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                fieldLabel : 'Физ. лицо',
                                                name       : 'person_name',
                                                itemId     : 'nx_add_person_name',
                                                readOnly   : true,
                                                flex       : 4
                                            },
                                            {
                                                xtype           : 'nx.ux.JsonCombo',
                                                flex            : 2,
                                                emptyText       : 'выбрать физ. лицо',
                                                url             : '/person/get-objects-for-combo',
                                                matchFieldWidth : false,
                                                listeners       : {
                                                    scope: this,
                                                    'select' : function(combo) {
                                                        this.down('#nx_add_person_id').setValue(combo.getValue());
                                                        this.down('#nx_add_person_name').setValue(combo.getRawValue());
                                                        if(combo.getValue()) 
                                                        {
                                                            Ext.Ajax.request({
                                                                url     : '/person/get',
                                                                method  : 'POST',
                                                                params  : {'id' : combo.getValue()},
                                                                scope   : this,
                                                                success : function(response) {
                                                                    var response = Ext.JSON.decode(response.responseText);
                                                                    this.setValuesIf({
                                                                        'email'           : response.data.email,
                                                                        'address'         : response.data.address,
                                                                        'country_id'      : response.data.country_id,
                                                                        'area'            : response.data.area,
                                                                        'region'          : response.data.region,
                                                                        'city'            : response.data.city,
                                                                        'zipcode'         : response.data.zipcode,
                                                                        'telcode'         : response.data.telcode,
                                                                        'fax'             : response.data.fax,
                                                                        'address_comment' : response.data.address_comment,
                                                                        'phone'           : response.data.address_phone ? response.data.address_phone : response.data.phone
                                                                    }, false);
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        border: 0,
                                        layout: 'hbox',
                                        items: [
                                            {
                                                xtype  : 'textfield',
                                                name   : 'company_id',
                                                itemId : 'nx_add_company_id',
                                                hidden : true,
                                            },
                                            {
                                                xtype      : 'textfield',
                                                fieldLabel : 'Организация',
                                                name       : 'company_name',
                                                itemId     : 'nx_add_company_name',
                                                readOnly   : true,
                                                flex       : 4
                                            },
                                            {
                                                xtype           : 'nx.ux.JsonCombo',
                                                emptyText       : 'выбрать организацию',
                                                url             : '/company/get-objects-for-combo',
                                                matchFieldWidth : false,
                                                flex            : 2,
                                                listeners       : {
                                                    scope: this,
                                                    'select' : function(combo) {
                                                        this.down('#nx_add_company_id').setValue(combo.getValue());
                                                        this.down('#nx_add_company_name').setValue(combo.getRawValue());
                                                        if(combo.getValue()) 
                                                        {
                                                            Ext.Ajax.request({
                                                                url     : '/company/get',
                                                                method  : 'POST',
                                                                params  : {'id' : combo.getValue()},
                                                                scope   : this,
                                                                success : function(response) {
                                                                    var response = Ext.JSON.decode(response.responseText);
                                                                    this.setValuesIf({
                                                                        'email'           : response.data.email,
                                                                        'phone'           : response.data.phone,
                                                                        'address'         : response.data.address,
                                                                        'country_id'      : response.data.country_id,
                                                                        'area'            : response.data.area,
                                                                        'region'          : response.data.region,
                                                                        'city'            : response.data.city,
                                                                        'zipcode'         : response.data.zipcode,
                                                                        'telcode'         : response.data.telcode,
                                                                        'fax'             : response.data.fax,
                                                                        'address_comment' : response.data.comment,
                                                                    }, true);
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            } 
                                        ]
                                    },
                                    {
                                        border: 0,
                                        layout: 'hbox',
                                        items: [
                                            {
                                                xtype  : 'textfield',
                                                hidden : true,
                                                name   : 'user_id',
                                                itemId : 'nx_add_user_id'
                                            },
                                            {
                                                xtype      : 'textfield',
                                                fieldLabel : 'OSP юзер',
                                                name       : 'user_name',
                                                itemId     : 'nx_add_user_name',
                                                readOnly   : true,
                                                flex       : 4
                                            },
                                            {
                                                xtype           : 'nx.ux.JsonCombo',
                                                flex            : 2,
                                                emptyText       : 'выбрать OSP юзера',
                                                url             : '/user/get-objects-for-combo',
                                                matchFieldWidth : false,
                                                listeners       : {
                                                    scope: this,
                                                    'select' : function(combo) {
                                                        this.down('#nx_add_user_id').setValue(combo.getValue());
                                                        this.down('#nx_add_user_name').setValue(combo.getRawValue());
                                                    }
                                                }
                                            }
                                        ]
                                    }    
                                ]
                            },
                            {
                                title: 'Адрес доставки',
                                layout: 'form',
                                padding: 5,
                                items: [
                                    {
                                        xtype         : 'combo',
                                        name          : 'country_id',
                                        store         : countries_store,
                                        fieldLabel    : 'Страна',
                                        editable      : false,
                                        queryMode     : 'local',
                                        triggerAction : 'all',
                                        value         : '165'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'area',
                                        fieldLabel : 'Область'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'region',
                                        fieldLabel : 'Район'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'city',
                                        fieldLabel : 'Город, село'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'zipcode',
                                        fieldLabel : 'Индекс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'address',
                                        fieldLabel : 'Адрес'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'telcode',
                                        fieldLabel : 'Тел. код'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'phone',
                                        fieldLabel : 'Телефон'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'fax',
                                        fieldLabel : 'Факс'
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'address_comment',
                                        fieldLabel : 'Комментарий'
                                    }
                                ]
                            },
                            {
                                title: 'Издания',
                                padding: 5,
                                items: [
                                    {
                                        xtype  : 'subscription_magazines_editor',
                                        itemId : 'subscriptionMagazinesEditor',
                                        border : 0,
                                        view   : this
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Добавить',
                action: 'add'
            },
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    },
    setValuesIf: function(data, replace) 
    {
        var form = this.down('form').getForm();
        var changed = [];
        for(i in data) 
        {
            var field = form.findField(i);//console.log(field);
            if(field && ((!replace && !field.getValue()) || replace) && data[i])
            {
                field.setValue(data[i]);
                changed[changed.length] = field.fieldLabel;
            }
        };
        if(changed.length)
        {
            Ext.Msg.show({title: 'Внимание', msg: 'Были обновлены поля: ' + changed.join(', '), modal: false});
            Ext.defer(function() {
                Ext.MessageBox.hide();
            }, 5000);
        }
    }/*,
    create: function() {
        var items = [];
        Ext.each(function(){

        });
        Ext.getCmp('magazinesCheckboxSelection').add(
            {
                xtype: 'checkboxgroup',
            }
        );
    }*/
});