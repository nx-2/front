/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.pdflog' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_pdflog_list',

    title: 'Лог отправки pdf на email',

    //store: 'subscription',

    initComponent: function() {

        Ext.apply(this, {
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/subscription/pdflog',
                        //update: 'js/index/data/updateUsers.json'
                    },
                    //url : '/subscription/pdflog',
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                //autoLoad : true,
                fields        : [
                    {name: 'periodical_name'},
                    {name: 'issue_title'},
                    {name: 'send'},
                    {name: 'email'},
                    {name: 'created'},
                    {name: 'sendtime'},
                    {name: 'subscription_id'}
                ]
            })
        });

        Ext.apply(this, {
            columns    : [
                {header: 'Издание', dataIndex: 'periodical_name', flex: 1, filter: {xtype: 'combo', filterName: 'periodical_id', store: subscribleMagazinesStore}},
                {header: 'Выпуск', dataIndex: 'issue_title', flex: 1, filter: {xtype: 'textfield'}},
                {
                    header: 'Статус', 
                    dataIndex: 'send', 
                    flex: 1, 
                    renderer: function(value) {
                        if(value=='1') 
                        {
                            return 'отправлено';
                        }
                        else 
                        {
                            return value;
                        }
                    }, 
                    filter: {xtype: 'combo', editable : false, store: [['','-'],['0','0'],['1','отправлено']]}
                },
                {header: 'Email', dataIndex: 'email', flex: 1, filter: {xtype: 'textfield'}},
                {header: 'Дата очереди', dataIndex: 'created', flex: 1, filter: {xtype: 'textfield'}},
                {header: 'Дата отправки', dataIndex: 'sendtime', flex: 1, filter: {xtype: 'textfield'}},
                {header: 'id подписки', dataIndex: 'subscription_id', flex: 1, filter: {xtype: 'textfield'}}
            ]
        });

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            } 
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },'->',
                    {
                        text    : 'Лог отправки по выпускам',
                        iconCls : 'subscription-pdflogbyissue-ico',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'subscription/pdflogbyissue';
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);
    }
});