/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.ship' ,{
    extend : 'Ext.grid.Panel',
    alias  : 'widget.nx_subscription_ship_list',
    title  : 'Плановые отгрузки по подпискам',
    itemId : 'nx_subscription_ship_list',

    initComponent: function() {

        var locked = 0;
        if(this.nxparams.shipment_id || this.nxparams.issue_id || ((this.nxparams.by_channel_id || this.nxparams.channel_id || this.nxparams.type) && this.nxparams.year && this.nxparams.month ) ) {
            locked = 1;
        }

        var stickers = 0;
        if(this.nxparams.by_channel_id==2 || this.nxparams.channel_id==2) {
            stickers = 2;
        }
        else if(this.nxparams.by_channel_id==3 || this.nxparams.channel_id==3 || this.nxparams.by_channel_id==4 || this.nxparams.channel_id==4) {
            stickers = 1;
        }
        else if(this.nxparams.type=='pdf') {
            stickers = 1;
        }

        Ext.apply(this, {
            store: new nx.store.base({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/ship/shippings',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                remoteSort  : !locked,
                fields        : [
                    {name: 'id'},
                    {name: 'name'},
                    {name: 'email'},
                    {name: 'periodical_name'},
                    {name: 'issue_title'},
                    {name: 'year'},
                    {name: 'month'},
                    {name: 'type'},
                    {name: 'state'},
                    {name: 'sendtime'},
                    {name: 'subscription_id'},
                    {name: 'subscriber_type_id'},
                    {name: 'shipment_id'},
                    {name: 'zipcode'},
                    {name: 'region'},
                    {name: 'area'},
                    {name: 'city'},
                    {name: 'city1'},
                    {name: 'street'},
                    {name: 'house'},
                    {name: 'building'},
                    {name: 'apartment'},
                    {name: 'full_map_address'},
                    {name: 'is_enveloped'},
                    {name: 'shipdate'},
                    //{name: 'channel_name'},
                    {name: 'payment_type_id'},
                    {name: 'payment_agent_name'},
                    {name: 'payment_date'},
                    {name: 'price'},
                    {name: 'discount'},
                    {name: 'doc_result'},
                    {name: 'last_request_date'},
                    {name: 'xpress_id'},
                    {name: 'quantity'},
                    {name: 'ship_count'},
                ],
                autoLoad: false,
                pageSize: locked ? 99999 : page_size,
                listeners : {
                    scope: this,
                    'beforeload': function(store)
                    {
                        var filters = this.store.filters;
                        var is_ship = filters.findIndex('property', 'is_ship');
                        /*var fnames  = ['id','periodical_id','name','email','subscription_id','doc_result'];
                        var allow   = false;
                        for(var i=0;i<fnames.length;i++) {
                            var f = filters.findIndex('property', fnames[i]);
                            if(f!=-1) {
                                allow = true;
                            }
                        }*/
                        if(!locked) {
                            //if(!allow) {
                            if((is_ship==-1 && !filters.length) || (is_ship!=-1 && filters.length==1)) {
                                return false;
                            }
                        }
                    }
                }
            })
        });

        Ext.apply(this, {
            columns    : [
                {xtype: 'rownumberer', width: 40},
                {text: 'ID', dataIndex: 'id', width: 60, xfilter: true},
                {text: 'Издание', dataIndex: 'periodical_name', width: 70, xfilter: 'periodical_id'},
                {text: 'Выпуск', dataIndex: 'issue_title', width: 60, xfilter: true},
                {text: 'Год', dataIndex: 'year', width:  60, hidden: true, xfilter: true},
                {text: 'Месяц', dataIndex: 'month', width:  50, hidden: true, xfilter: true},
                {text: 'Подписчик', dataIndex: 'name', flex: 1, tdCls: 'nx-multiline', xfilter: true},
                {text: 'Индекс', dataIndex: 'zipcode', width: 50},//, filter: {xtype: 'textfield'}},  
                {text: 'Адрес', dataIndex: 'full_map_address', flex: 1, tdCls: 'nx-multiline'},//, filter: {xtype: 'textfield'}},  
                {text: 'Email', dataIndex: 'email', width: 150, xfilter: true},  
                {text: 'Тип', dataIndex: 'type', width: 70, xfilter: true, hidden: true},
                /*{
                    text: 'Статус', dataIndex: 'state', width: 80, 
                    renderer: function(value) { if(value=='1') { return 'отправлено'; } else { return value; }}, 
                    filter: {xtype: 'combo', editable : false, store: [['0','0'],['1','отправлено']]}
                },*/
                /*{
                    text: 'Дата отправки', dataIndex: 'sendtime', width: 120, 
                    //filter: [{xtype: 'textfield', filterName: 'sendtime_start', fieldLabel: '', width:95, regex: /^\d{4}-\d{2}-\d{2}/},{xtype: 'textfield', filterName: 'sendtime_end', fieldLabel: '', width:95, regex: /^\d{4}-\d{2}-\d{2}/}]
                    //filter: {xtype: 'textfield', type: 'date', dateFormat: 'Y-m-d', beforeText: 'Перед', afterText: 'До', onText: 'На'}
                },*/
                {text: 'Отгруж.', width: 50, hidden: true, renderer: function(val, meta, rec) { 
                    return parseInt(rec.get('shipment_id')) ? 1 : 0;
                }},
                {text: 'Дата отгрузки', dataIndex: 'shipdate', width: 120, hidden: !!locked, xfilter: true},
                {text: 'Отгрузок', dataIndex: 'ship_count', width: 50, hidden: true, xfilter: true},
                {text: 'id подписки', dataIndex: 'subscription_id', width: 60, xfilter: true},
                {text: 'xpress id', dataIndex: 'xpress_id', width: 60, hidden: true, xfilter: true},
                {text: 'тип подписчика', dataIndex: 'subscriber_type_id', width: 80, xfilter: true, renderer: function(val) { 
                    for(var i=0; i<subscriber_type_store.length; i++) { if(subscriber_type_store[i][0] == val) return subscriber_type_store[i][1]; } 
                }},
                {text: 'Кол-во', dataIndex: 'quantity', width: 50, hidden: true},
                {text: 'Цена', dataIndex: 'price', width: 50, hidden: !!locked},
                {text: 'Скидка', dataIndex: 'discount', width: 50, hidden: !!locked},
                {text: 'Стоимость', width: 50, hidden: true, 
                    summaryType: function(rows) {
                        var sum = 0;
                        Ext.each(rows, function(rec){
                            var cost = parseInt(rec.get('quantity')) * (parseFloat(rec.get('price')) - parseFloat(rec.get('price')) * parseFloat(rec.get('discount')) / 100.0);
                            sum+=cost;
                        });
                        return sum.toFixed(2).toString().replace('.',',');
                    },
                    summaryRenderer: function(value) {
                        return value;
                    }, renderer: function(val, meta, rec) {
                        var cost = parseInt(rec.get('quantity')) * (parseFloat(rec.get('price')) - parseFloat(rec.get('price')) * parseFloat(rec.get('discount')) / 100.0);
                        return cost.toString().replace('.',',');
                    }
                },
                {text: 'Дата оплаты', dataIndex: 'payment_date', width: 80, hidden: !locked},
                {text: 'Опл. через', dataIndex: 'payment_agent_name', width: 70, xfilter: 'payment_agent_id', renderer: function(val, meta, rec) {
                    return rec.get('payment_type_id')=='0' ? 'без оплаты' : (rec.get('payment_type_id')=='3' ? 'наличные' : val);
                }},
                {text: 'Ответ 1с', dataIndex: 'doc_result', width: 150, hidden: true},
                {text: 'Дата запроса 1c', dataIndex: 'last_request_date', width: 100, hidden: true, xfilter: true},
                //{text: 'отпр-но', dataIndex: 'channel_name', width: 60, hidden: true}
                /*{text: 'конверт?', dataIndex: 'is_enveloped', width: 60, filter: {xtype: 'combo', editable : false, store: [['0','нет'],['1','да']]}, renderer: function(val) {
                    return val && parseInt(val) ? 'да' : 'нет';
                }},*/
                {
                    xtype        : 'actioncolumn',
                    width        : 30,
                    menuDisabled : true,
                    items        : [
                        {
                            icon    : '/images/extjs/icons/watch.png',
                            tooltip : 'Показать тех. данные',
                            scope   : this,
                            handler : function(grid, rowIndex, colIndex, item, e, rec) 
                            {
                                var id = rec.get('id');
                                Ext.Ajax.request({
                                    url     : '/ship/get',
                                    method  : 'POST',
                                    params  : {
                                        id : id
                                    },
                                    scope   : this,
                                    success : function(response) {
                                        var response = Ext.JSON.decode(response.responseText);
                                        showDataAsTree(response.data);
                                    }
                                });
                            }
                        },
                    ]
                }
            ]
        });

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false,
            //layout                : 'hbox'
        });

        /*var filters = {
            ftype: 'filters',
            menuFilterText: 'Фильтровать',
            encode: true,
            local: false,
            /*buildQuery: function(filters) {
                //getHeaderFilters();
                console.log(filters);
                var p = {}, i, f, root, dataPrefix, key, tmp,
                len = filters.length;

                if (!this.encode){
                    for (i = 0; i < len; i++) {
                        f = filters[i];
                        root = [this.paramPrefix, '[', i, ']'].join('');
                        p[root + '[field]'] = f.field;

                        dataPrefix = root + '[data]';
                        for (key in f.data) {
                            p[[dataPrefix, '[', key, ']'].join('')] = f.data[key];
                        }
                    }
                } else {
                    tmp = [];
                    for (i = 0; i < len; i++) {
                        f = filters[i];
                        tmp.push(Ext.apply(
                            {},
                            {field: f.field},
                            f.data
                        ));
                    }
                    // only build if there is active filter
                    if (tmp.length > 0){
                        p[this.paramPrefix] = Ext.JSON.encode(tmp);
                    }
                }console.log(p,this,this.view.up('grid').getHeaderFilters());
                return p;
            }*/
        //};

        Ext.apply(this, {
            plugins: locked ? [] : [gridheaderfilters],
            //features: [filters],
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    var css_class = '';
                    css_class += rec.get('shipment_id')!='0' ? ' nx-transparent' : '';
                    return css_class;
                } 
            },
            features: [{
                ftype: 'summary'
            }],
            allowDeselect: true,
            selModel: {
                mode: 'MULTI'
            },
            xfilters : [//filters property already taken up by filters feature 
                {fieldLabel: 'id', name: 'id', xtype: 'textfield'},
                {fieldLabel: 'издание', name: 'periodical_id', xtype: 'combo', store: subscribleMagazinesStore},
                {fieldLabel: 'выпуск', name: 'issue_title', xtype: 'textfield'},
                {fieldLabel: 'год', name: 'year', xtype: 'textfield'},
                {fieldLabel: 'месяц', name: 'month', xtype: 'combo', store: [1,2,3,4,5,6,7,8,9,10,11,12], editable : false, multiSelect: true, invalidCls: ''},
                {fieldLabel: 'подписчик', name: 'name', xtype: 'textfield', minLength: 3},
                {fieldLabel: 'email', name: 'email', xtype: 'textfield', minLength: 3},
                {fieldLabel: 'отгружено', name: 'is_ship', xtype: 'combo', editable: false, store: [[0,'нет'],[1,'да']]},
                {fieldLabel: 'дата отгрузки', name: 'shipdate', xtype: 'textfield'},
                {fieldLabel: 'отгрузок', name: 'ship_count', xtype: 'numberfield', allowDecimals: false, minValue:0},
                {fieldLabel: 'id заказа', name: 'subscription_id', xtype: 'textfield'},
                {fieldLabel: 'xpress id', name: 'xpress_id', xtype: 'textfield'},
                {fieldLabel: 'тип заказа', name: 'order_type', xtype: 'combo', store: order_type_store},
                {fieldLabel: 'тип подписчика', name: 'subscriber_type_id', xtype: 'combo', editable : false, store: subscriber_type_store},
                {fieldLabel: 'способ оплаты', name: 'payment_type_id', xtype: 'combo', store: payment_types_store, editable : false, multiSelect: true},
                {fieldLabel: 'опл. через', name: 'payment_agent_id', xtype: 'combo', store: payment_agents_store, editable : false, multiSelect: true},
                {fieldLabel: 'ответ 1с', name: 'doc_result', xtype: 'textfield', ops: ['=','!=']},
                {fieldLabel: 'дата запроса 1c', name: 'last_request_date', xtype: 'textfield'},
                {fieldLabel: 'в очереди в 1с', name: 'is_doc_wait', xtype: 'combo', editable: false, store: [[0,'нет'],[1,'да']]},
                {fieldLabel: 'теги заказа', name: 'order_tags', xtype: 'nx.ux.JsonCombo', url: '/tag/get-objects-for-combo', baseParams: {entity_id: 1}, multiSelect: true, ops: ['AND','OR']},
                {fieldLabel: 'тип подписки', name: 'type', xtype: 'combo', store: [['pdf','PDF'],['paper','Бумага']], editable: false}
            ]
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных',
                plugins     : [{ptype: 'pagesize'}],
                hidden      : locked ? true : false 
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        xtype      : 'view_state_switcher',
                        filter_menu: !locked
                        //hidden: !!locked
                    },
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        hidden  : locked ? true : false,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        hidden  : locked ? true : false,
                        handler : function() {
                            this.resetHeaderFilters(false);
                            this.store.clearFilter(true);
                            this.store.removeAll();
                        }
                    },
                    '-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Отгрузки');
                                }
                            },this);   
                        }
                    },
                    '-',
                    /*{
                        text         : 'Конвертом',
                        enableToggle : true,
                        handler      : function() 
                        {
                            var store = this.up('nx_subscription_ship_list').getStore();
                            if(this.pressed)
                            {
                                store.filters.add('is_enveloped', new Ext.util.Filter({
                                    property: 'is_enveloped',
                                    value   : 1
                                }));
                            }
                            else
                            {
                                store.filters.removeAtKey('is_enveloped');
                            }
                            store.load();
                        }
                    },*/
                    {
                        text     : 'Все',
                        itemId   : 'nx_ship_state_btn',
                        width    : 100,
                        scope    : this,
                        nx_state : 0,
                        handler  : function(btn) 
                        {
                            btn.setState((btn.nx_state+1)%3);
                            this.store.reload();
                        },
                        setState : function(state)
                        {
                            var store     = this.up('grid').getStore();
                            this.nx_state = state;
                            var index     = store.filters.findIndex('property', new RegExp('^is_ship$'));
                            if(index!=-1) {
                                store.filters.removeAt(index);
                            }
                            if(this.nx_state==0) {
                                this.setText('Все');
                                store.filters.removeAtKey('is_ship');
                            }
                            if(this.nx_state==1) {
                                this.setText('Не отгруженные');
                                store.filters.add('is_ship', new Ext.util.Filter({
                                    property: 'is_ship',
                                    value   : 0
                                }));
                            }
                            if(this.nx_state==2) {
                                this.setText('Отгруженные');
                                store.filters.add('is_ship', new Ext.util.Filter({
                                    property: 'is_ship',
                                    value   : 1
                                }));
                            }
                        }
                    },
                    '-',
                    {
                        text    : 'Отменить отправку',
//                        hidden  : !nx.app.isperm('ship_manager') || !locked,
                        hidden  : false,
                        scope   : this,
                        handler : function() {
                            var selection = this.getSelectionModel().getSelection();  
                            if(selection.length)
                            {
                                var ids       = [];
                                Ext.each(selection, function(row) {
                                    ids.push(row.get('id'));
                                });
                                Ext.Msg.confirm('Подтверждение','Отменить отправку ' + selection.length + ' отгрузок?', function(btn){
                                    if(btn=='yes')
                                    {
                                        Ext.Ajax.request({
                                            url     : '/ship/cancel-ship',
                                            method  : 'POST',
                                            params  : { ids: ids.join(',') },
                                            scope   : this,
                                            success : function(response) {
                                                var response = Ext.JSON.decode(response.responseText);
                                                if(response.result==false)
                                                {
                                                    Ext.Msg.alert('Ошибка', '');
                                                }
                                                else {
                                                    Ext.Msg.alert('Сообщение', 'Отмена завершена.');
                                                    this.store.reload();
                                                }
                                            }
                                        });
                                    }
                                },this); 
                            }
                            else {
                                Ext.Msg.alert('Сообщение', 'Выберите отгрузки для отмены!');
                            }
                            
                        }
                    },
                    {
                        text    : 'Отправить как',
//                        hidden  : !nx.app.isperm('ship_manager') || !locked,
                        hidden  : false,
                        scope   : this,
                        itemId  : 'nx_send_btn',
                        handler : function() {
                            var channel    = this.down('#nx_combo_channel').getRawValue();
                            var channel_id = this.down('#nx_combo_channel').getValue();
                            if(channel_id) 
                            {
                                var selection = this.getSelectionModel().getSelection();  
                                var count     = 0;
                                var ids       = [];
                                if(selection.length)
                                {
                                    count = selection.length;
                                    Ext.each(selection, function(row) {
                                        ids.push(row.get('id'));
                                    });
                                }
                                else {
                                    count = this.store.getTotalCount();
                                    this.store.data.each(function(row){
                                        ids.push(row.get('id'));
                                    });
                                }
                                if(!count) {
                                    Ext.Msg.alert('Сообщение', 'Выберите отгрузки для отправки!');
                                    return false;
                                }
                                //Ext.Msg.confirm('Подтверждение','Отправить ' + count + ' отгрузок как ' + channel + '?', function(btn){
                                //    if(btn=='yes')
                                //    {
                                var curDate = new Date();
                                new Ext.window.Window({
                                    title     : 'Отправить ' + count + ' отгрузок как ' + channel + '?',
                                    //layout    : 'fit',
                                    border    : false,
                                    //resizable : true,
                                    autoShow  : true, 
                                    items     : [
                                        {
                                            xtype      : 'datefield',
                                            fieldLabel : 'Дата',
                                            value      : curDate,
                                            format     : 'Y-m-d',
                                            allowBlank : false
                                        },
                                        {
                                            xtype      : 'timefield',
                                            fieldLabel : 'Время',
                                            format     : 'H:i:s',
                                            value      : curDate,
                                            allowBlank : false
                                        }
                                    ],
                                    buttons: [
                                        {
                                            text: 'Отправить',
                                            scope: this,
                                            handler: function(btn) {
                                                var win = btn.up('window');
                                                var date_field = win.down('datefield');
                                                var time_field = win.down('timefield');
                                                if(date_field.isValid() && time_field.isValid())
                                                {
                                                    var date = date_field.getRawValue() + ' ' + time_field.getRawValue();
                                                    var store   = this.getStore();
                                                    var options = {
                                                        action  : 'read',
                                                        //filters : store.filters.items,
                                                        params  : { 
                                                            channel_id : channel_id,
                                                            ids        : ids.join(','),
                                                            date       : date
                                                        }
                                                    };
                                                    var operation   = new Ext.data.Operation(options);
                                                    var fakeRequest = store.getProxy().buildRequest(operation);
                                                    var params      = fakeRequest.params;
                                                    Ext.Ajax.request({
                                                        url     : '/ship/ship',
                                                        method  : 'POST',
                                                        params  : params,
                                                        scope   : this,
                                                        success : function(response) {
                                                            var response = Ext.JSON.decode(response.responseText);
                                                            if(response.result==false)
                                                            {
                                                                Ext.Msg.alert('Ошибка', 'Не выбраны отгрузки, либо все уже отгружено.');
                                                            }
                                                            else {
                                                                win.close();
                                                                Ext.Msg.alert('Сообщение', 'Отправка завершена.');
                                                                this.store.reload();
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    ]
                                });
                                //    }
                                //},this); 
                            } 
                        }
                    },
                    {
                        xtype    : 'combo',
//                        hidden   : !nx.app.isperm('ship_manager') || !locked,
                        hidden   : false,
                        itemId   : 'nx_combo_channel',
                        store    : ship_channels_store,
                        editable : false,
                        readOnly : locked ? true : false,
                    },
                    //'-',
//                    {
//                        text   : 'Документы из 1С',
//                        hidden : !nx.app.isperm('ship_manager'),//nx.config.perm.is_admin ? false : true,
//                        scope  : this,
//                        handler: function() {
//                            var selection = this.getSelectionModel().getSelection();
//                            if(selection.length)
//                            {
//                                var ids = [];
//                                Ext.each(selection, function(row) {
//                                    ids.push(row.get('id'));
//                                });
//                                this.getDocFile(ids);//, 'invoice_torg12');
//                            }
//                            else
//                            {
//                                //this.getDocFile([0], 'invoice_torg12');
//                            }
//                        }
//                    },
                    {
                        text    : 'Наклейки',
                        scope   : this,
                        hidden  : stickers ? false : true,
                        nx_mode : stickers,
                        handler : function(btn) {
                            var ids = [];
                            var selection = this.getSelectionModel().getSelection();
                            if(selection.length)
                            {
                                Ext.each(selection, function(row) {
                                    ids.push(row.get('id'));
                                });
                            }
                            else
                            {
                                this.store.data.each(function(row) {
                                    ids.push(row.get('id'));
                                });
                            }
                            Ext.Ajax.request({
                                url     : '/ship/get-stickers-data',
                                method  : 'POST',
                                scope   : this,
                                params  : {ids:ids.join(',')},
                                success : function(response) {
                                    var response = Ext.JSON.decode(response.responseText);
                                    if(response.result)
                                    {
                                        this.showStickersData(response.result, btn.nx_mode);
                                    }
                                }
                            }, this);
                        }
                    },
                    '->',
                    {
                        text    : 'Почта России',
                        iconCls : 'ship-ico',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'subscription/shiptomap';
                        }
                    },
                    '-',
                    {
                        text    : 'Статистика',
                        iconCls : 'ship-ico',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'subscription/shiptable';
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);

        if(this.nxparams.is_ship)
        {
            this.down('#nx_ship_state_btn').setState(parseInt(this.nxparams.is_ship)+1);
        }
        else if(!locked)
        {
            this.down('#nx_ship_state_btn').setState(2);
        }
        if(this.nxparams.channel_id)
        {
            this.store.filters.add('channel_id', new Ext.util.Filter({
                property: 'channel_id',
                value   : this.nxparams.channel_id
            }));
            var combo = this.down('#nx_combo_channel');
            combo.select(combo.getStore().getAt(combo.getStore().find('field1',this.nxparams.by_channel_id)));
            this.down('#nx_send_btn').disable();
        }
        if(this.nxparams.by_channel_id)
        {
            this.store.filters.add('by_channel_id', new Ext.util.Filter({
                property: 'by_channel_id',
                value   : this.nxparams.by_channel_id
            }));
            var combo = this.down('#nx_combo_channel');
            combo.select(combo.getStore().getAt(combo.getStore().find('field1',this.nxparams.by_channel_id)));
        }
        if(this.nxparams.issue_id)
        {
            this.store.filters.add('issue_id', new Ext.util.Filter({
                property: 'issue_id',
                value   : this.nxparams.issue_id
            }));
        }
        if(this.nxparams.type)
        {
            this.store.filters.add('type', new Ext.util.Filter({
                property: 'type',
                value   : this.nxparams.type
            }));
        }
        if(this.nxparams.year)
        {
            this.store.filters.add('year', new Ext.util.Filter({
                property: 'year',
                value   : this.nxparams.year
            }));
        }
        if(this.nxparams.month)
        {
            this.store.filters.add('month', new Ext.util.Filter({
                property: 'month',
                value   : this.nxparams.month
            }));
        }
        if(this.nxparams.shipment_id)
        {
            this.store.filters.add('shipment_id', new Ext.util.Filter({
                property: 'shipment_id',
                value   : this.nxparams.shipment_id
            }));
        }
        if(locked) {
            this.store.load();
        }

        /*this.headerCt.on('menucreate', function (cmp, menu, eOpts) {
            menu.on('beforeshow', this.showHeaderMenu);
        }, this);*/
        this.addEvents('nx_get_ship_doc');
    },
    getDocFile: function(ids)
    {
        this.fireEvent('nx_get_ship_doc', this, ids);
    },
    showStickersData: function(data, mode) {
        new Ext.window.Window({
            title     : 'Наклейки',
            //iconCls   : 'stickers-ico',
            layout    : 'fit',
            border    : false,
            resizable : true,
            autoShow  : true, 
            items     : [
                {
                    xtype : 'grid',
                    width : 700,
                    height: 500,
                    autoShow: true,
                    store: new Ext.data.JsonStore({
                        proxy: {
                            type: 'memory',
                            reader: {
                                type: 'json',
                                root: 'items'
                            }
                        },
                        data         : {'items' : data},
                        pageSize     : 9999,
                        autoLoad     : true,
                        fields       : [
                            {name: 'periodical_name'},
                            {name: 'issue_title'},
                            {name: 'zipcode'},
                            {name: 'address'},
                            {name: 'name'},
                            {name: 'phone'},
                            {name: 'quantity'},
                            {name: 'delivery_type_name'},
                            {name: 'order_id'},
                            {name: 'subscriber_type_mark'},
                            {name: 'payment_agent_name'},
                            {name: 'payment_type_id'}
                        ]
                    }),
                    columns    : [
                        {xtype: 'rownumberer', width: 50},
                        {text: 'Издание', dataIndex: 'periodical_name', width: 70},
                        {text: 'Выпуск', dataIndex: 'issue_title', width: 50},
                        {text: 'Индекс', dataIndex: 'zipcode', width: 50, hidden: (mode==2 ? true : false)},
                        {text: 'Адрес', dataIndex: 'address', flex: 1, tdCls: 'nx-multiline'},
                        {text: 'Подписчик', dataIndex: 'name', flex: 1, tdCls: 'nx-multiline'},
                        {text: 'Телефон', dataIndex: 'phone', width: 100, hidden: (mode==1 ? true : false)},
                        {text: 'Штук', dataIndex: 'quantity', width: 40},
                        {text: 'Доставка', dataIndex: 'delivery_type_name', width: 50, hidden: (mode==2 ? true : false)},
                        {text: 'Опл. через', dataIndex: 'payment_agent_name', width: 70, renderer: function(val, meta, rec) {
                            return rec.get('payment_type_id')=='0' ? 'без оплаты' : (rec.get('payment_type_id')=='3' ? 'наличные' : val);
                        }},
                        {text: 'UIN', dataIndex: 'order_id', width: 50},
                        {text: 'Звезда', dataIndex: 'subscriber_type_mark', width: 40},
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    }
                }
            ], 
            buttons : [
                {
                    text: 'Закрыть',
                    handler: function() {
                        this.up('window').close();
                    }
                }
            ],
            dockedItems: [
            {
                xtype: 'toolbar',
                items : [
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        handler: function() 
                        {
                            var grid = this.up('window').down('grid');
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + grid.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(grid, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Наклейки');
                                }
                            },grid);  
                        }
                    }
                ]
            }]
        });
    }
    /*showHeaderMenu: function (menu, eOpts)
    {
        //define array to store added compoents in
        if(this.myAddedComponents === undefined)
        {
            this.myAddedComponents = new Array();
        }

        var columnDataIndex = menu.activeHeader.dataIndex,
            customMenuComponents = this.myAddedComponents.length;

        //remove components if any added
        if(customMenuComponents > 0)
        {
            for(var i = 0; i < customMenuComponents; i++)
            {
                menu.remove(this.myAddedComponents[i][0].getItemId());
            }

            this.myAddedComponents.splice(0, customMenuComponents);
        }

        //add components by column index
        switch(columnDataIndex)
        {
            case 'sendtime': this.myAddedComponents.push(menu.add([{
                                text: 'Custom Item'}]));

                    break;
        }
    },*/
    /*listeners: {
        'headerfilterchange': function(grid, filters)
        {
            var filters            = grid.getStore().filters;console.log(filters);
            var f_type_idx         = filters.findIndex('property', 'type');
            var f_periodical_idx   = filters.findIndex('property', 'periodical_id');
            var f_issue_idx        = filters.findIndex('property', 'issue_title');
            var f_is_enveloped_idx = filters.findIndex('property', 'is_enveloped');
            if(f_is_enveloped_idx!=-1 && f_periodical_idx!=-1 && f_issue_idx!=-1 && f_type_idx!=-1 && filters.getAt(f_type_idx).value == 'paper')
            {console.log(this.down('#nx_btn_send_envelop'));
                this.down('#nx_btn_send_envelop').enable();
            }
            else
            {
                this.down('#nx_btn_send_envelop').disable();
            }
        }
        afterrender: function() {
            var menu = this.headerCt.getMenu();
            menu.add([{
                text: 'Custom Item',
                handler: function() {
                    var columnDataIndex = menu.activeHeader.dataIndex;
                    alert('custom item for column "'+columnDataIndex+'" was pressed');
                }
            }]);           
        }
    }*/
});