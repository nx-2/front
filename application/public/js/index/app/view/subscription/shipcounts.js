/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.shipcounts' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_subscription_shipcounts',
    itemId: 'nx_subscription_shipcounts',

    title: 'Плановые отгрузки - статистика списком',

    initComponent: function() {
        var view = this;
        var channels = {
            1: 'map',
            2: 'courier',
            3: 'envelope_post',
            4: 'envelope_dz',
            7: 'office',
            //5: 'envelope_org'
        };
        var channel_fields = [];
        for(var channel_id in channels)
        {  
            var channel_name = channels[channel_id];
            channel_fields[channel_fields.length] = {name: 'count_' + channel_name};
            channel_fields[channel_fields.length] = {name: 'count_' + channel_name + '_sended'};
            channel_fields[channel_fields.length] = {name: 'count_' + channel_name + '_not_sended'};
        }

        Ext.apply(this, {
            as_links: true,
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/ship/shipcounts',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields        : Ext.Array.merge([
                    {name: 'id'},
                    //{name: 'ids'},
                    {name: 'periodical_id'},
                    {name: 'periodical_name'},
                    {name: 'issue_id'},
                    {name: 'issue_num', type: 'int'},
                    {name: 'issue_title'},
                    //{name: 'yearmonth'},
                    {name: 'year'},
                    {name: 'month', type: 'int'},
                    {name: 'state'},
                    {name: 'sendtime'},
                    {name: 'count'},
                    {name: 'count_price'},
                    {name: 'count_paper'},
                    {name: 'count_paper_full'},
                    {name: 'count_paper_price'},
                    {name: 'count_pdf'},
                    {name: 'count_pdf_price'},
                    {name: 'count_pdf_log'},
                    {name: 'count_post'},
                    //{name: 'count_office'}
                ], channel_fields),
                listeners : {
                    scope: this,
                    'beforeload': function(store)
                    {
                        var filters   = this.store.filters;
                        var f         = [];
                        var allowed   = ['month', 'year', 'issue_num', 'issue_title', 'periodical_id'];
                        this.as_links = true;
                        filters.each(function(filter){
                            if(allowed.indexOf(filter.property)==-1) {
                                this.as_links = false;
                            }
                            f[filter.property] = 1;
                        }, this);
                        if(!(
                            (f['year'] && f['month']) || 
                            (f['periodical_id'] && f['month']) || 
                            (f['periodical_id'] && f['year']) || 
                            f['issue_num'] || 
                            f['shipment_date_start'] ||
                            f['payment_date_start'] 
                        )) {
                            return false;
                        }
                    }
                }
            })
        });

        Ext.apply(this, {
            countsRenderer : function(val, rec, channel_id) {
                var res = '';
                var channel_name = channels[channel_id];
                val = parseInt(val);
                res += ( val && this.as_links ? '<a title="всего" href="#" onclick="window.open(\'/#subscription/ship/issue_id/' + rec.get('issue_id') + '/by_channel_id/' + channel_id + '\', \'_blank\'); return false;">' + val + '</a>' : val);
                val = parseInt(rec.get('count_' + channel_name + '_sended'));
                res += ' / ' + ( val && this.as_links ? '<a title="отгружено" href="#" onclick="window.open(\'/#subscription/ship/issue_id/' + rec.get('issue_id') + '/channel_id/' + channel_id + '\', \'_blank\'); return false;">' + val + '</a>' : val);
                var curDate  = new Date();
                var curYear  = curDate.getFullYear();
                var curMonth = curDate.getMonth()+1;
                var year     = rec.get('year');
                var month    = rec.get('month');
                var ispast   = curYear == year && month<curMonth || year < curYear;  
                val = parseInt(rec.get('count_' + channel_name + '_not_sended'));
                res += ' / ' + (val && this.as_links ? '<a title="не отгружено" href="#" onclick="window.open(\'/#subscription/ship/issue_id/' + rec.get('issue_id') + '/by_channel_id/' + channel_id + '/is_ship/0\', \'_blank\'); return false;">' + (ispast ? '<span style="font-weight: bold; color: red;">' : '') + val + (ispast ? '</span>' : '') + '</a>' : val);
                return res;
            },
            summaryCalc : function(rows, channel_id) {
                var channel_name = channels[channel_id];
                var sum = []; 
                sum['total'] = sum['sended'] = sum['not_sended'] = 0;
                Ext.each(rows, function(row){
                    sum['total']+=parseInt(row.get('count_' + channel_name));
                    sum['sended']+=parseInt(row.get('count_' + channel_name + '_sended'));
                    sum['not_sended']+=parseInt(row.get('count_' + channel_name + '_not_sended'));
                });
                return sum;
            },
            summaryRender : function(value, grid, channel_id) {
                var res   = '';
                var year  = grid.getFilterValueBy('year');
                var month = grid.getFilterValueBy('month');
                if(month && year) {
                    res += (value['total'] > 1500 || !this.as_links ? value['total'] : '<a title="всего" href="#" onclick="window.open(\'/#subscription/ship/year/' + year + '/month/' + month + '/by_channel_id/' + channel_id + '\', \'_blank\'); return false;">' + value['total'] + '</a>');
                    res += ' / ' + (value['sended'] > 1500 || !this.as_links ? value['sended'] : '<a title="отгружено" href="#" onclick="window.open(\'/#subscription/ship/year/' + year + '/month/' + month + '/channel_id/' + channel_id + '\', \'_blank\'); return false;">' + value['sended'] + '</a>');
                    res += ' / ' + (value['not_sended'] > 1500 || !this.as_links ? value['not_sended'] : '<a title="не отгружено" href="#" onclick="window.open(\'/#subscription/ship/year/' + year + '/month/' + month + '/by_channel_id/' + channel_id + '/is_ship/0\', \'_blank\'); return false;">' + value['not_sended'] + '</a>');
                }
                return res;
            }
        });
        Ext.apply(this, {
            columns    : [
                {text: 'Издание', dataIndex: 'periodical_name', width: 100, xfilter: 'periodical_id'},
                {text: '#', dataIndex: 'issue_num', width: 60, hidden: true, xfilter: true},
                {text: '#', dataIndex: 'issue_title', width: 60, xfilter: true},
                {text: 'Год', dataIndex: 'year', width:  60, xfilter: true},
                {text: 'Месяц', dataIndex: 'month', width:  50, xfilter: true},
                {text: 'Всего', dataIndex: 'count', width: 60, renderer: function(val, meta, rec) {
                        return  this.as_links ? '<a href="" onclick="window.open(\'/#subscription/ship/issue_id/' + rec.get('issue_id') + '\', \'_blank\'); return false;">' + val + '</a>' : val;
                    },
                },
                {text: 'Сумма всего', dataIndex: 'count_price', width: 70, hidden: true, renderer: function(v) { return parseFloat(v).toFixed(2).toString().replace('.',','); }, 
                    summaryType: function(rows) {
                        var sum = 0;
                        Ext.each(rows, function(row){
                            sum+=parseFloat(row.get('count_price'));
                        });
                        return sum.toFixed(2);
                    }
                },
                {text: 'Бумага', dataIndex: 'count_paper', width: 60, renderer: function(val,meta,rec,rowindex,colindex,store,view) {
                        var res = '';
                        val = parseInt(rec.get('count_paper_full'));
                        res +=  val;
                        val = parseInt(rec.get('count_paper'));
                        res += ' (' + val + ')';
                        return res;
                    }
                },
                {text: 'Сумма бум.', dataIndex: 'count_paper_price', width: 70, hidden: true, renderer: function(v) { return parseFloat(v).toFixed(2).toString().replace('.',','); }, 
                    summaryType: function(rows) {
                        var sum = 0;
                        Ext.each(rows, function(row){
                            sum+=parseFloat(row.get('count_paper_price'));
                        });
                        return sum.toFixed(2);
                    }
                },
                {text: 'PDF', dataIndex: 'count_pdf', width: 60, renderer: function(val,meta,rec,rowindex,colindex,store,view){
                        var res = '';
                        val = parseInt(rec.get('count_pdf'));
                        res += ( val && this.as_links ? '<a title="всего" href="" onclick="window.open(\'/#subscription/ship/issue_id/' + rec.get('issue_id') + '/type/pdf\', \'_blank\'); return false;">' + val + '</a>' : val);
                        val = parseInt(rec.get('count_pdf_log'));
                        res += ' / ' + ( val && this.as_links ? '<a title="в логе" href="" onclick="Ext.getCmp(\'' + view.id + '\').up(\'grid\').fireEvent(\'nx_pdf_log_details\', \'' + rec.get('issue_id') + '\'); return false;">' + val + '</a>' : val);
                        return res;
                    },
                    summaryType: function(rows) { 
                        var sum = 0;
                        Ext.each(rows, function(row){
                            sum+=parseInt(row.get('count_pdf'));
                        });
                        return sum;
                    },
                    summaryRenderer: function(value, summaryData, dataIndex) { 
                        var res   = '';
                        var year  = view.getFilterValueBy('year');
                        var month = view.getFilterValueBy('month');
                        if(month && year) {
                            res += (this.as_links ? '<a title="всего" href="#" onclick="window.open(\'/#subscription/ship/year/' + year + '/month/' + month + '/type/pdf\', \'_blank\'); return false;">' + value + '</a>' : value);
                        }
                        return res;
                    }
                },
                {text: 'Сумма pdf', dataIndex: 'count_pdf_price', width: 70, hidden: true, renderer: function(v) { return parseFloat(v).toFixed(2).toString().replace('.',','); }, 
                    summaryType: function(rows) {
                        var sum = 0;
                        Ext.each(rows, function(row){
                            sum+=parseFloat(row.get('count_pdf_price'));
                        });
                        return sum.toFixed(2);
                    }
                },
                {text: 'Почта', dataIndex: 'count_post', width: 60},
                {text: 'Офис', dataIndex: 'count_office', renderer: function(val,meta,rec){return view.countsRenderer(val, rec, 7);},
                    summaryType: function(rows) { return view.summaryCalc(rows, 7); },
                    summaryRenderer: function(value, summaryData, dataIndex) { return view.summaryRender(value, view, 7); }
                },
                {text: 'Почта России', dataIndex: 'count_map', width: 90, renderer: function(val,meta,rec){
                    var res = '';
                    var channel_name = 'map';
                    var channel_id = 1;
                    val = parseInt(val);
                    res += ( val && this.as_links ? '<a title="всего" href="" onclick="window.open(\'/#subscription/shiptomap/periodical_id/' + rec.get('periodical_id') + '/year/' + rec.get('year') + '/month/' + rec.get('month') + '\', \'_blank\'); return false;">' + val + '</a>' : val);
                    val = parseInt(rec.get('count_' + channel_name + '_sended'));
                    res += ' / ' + ( val && this.as_links ? '<a title="отгружено" href="" onclick="window.open(\'/#subscription/ship/issue_id/' + rec.get('issue_id') + '/channel_id/' + channel_id + '\', \'_blank\'); return false;">' + val + '</a>' : val);
                    var curDate  = new Date();
                    var curYear  = curDate.getFullYear();
                    var curMonth = curDate.getMonth()+1;
                    var year     = rec.get('year');
                    var month    = rec.get('month');
                    var ispast   = curYear == year && month<curMonth || year < curYear;  
                    val = parseInt(rec.get('count_' + channel_name + '_not_sended'));
                    res += ' / ' + ( val && this.as_links ? '<a title="не отгружено" href="" onclick="window.open(\'/#subscription/shiptomap/periodical_id/' + rec.get('periodical_id') + '/year/' + rec.get('year') + '/month/' + rec.get('month') + '/is_ship/0\', \'_blank\'); return false;">' + (ispast ? '<span style="font-weight: bold; color: red;">' : '') + val + (ispast ? '</span>' : '') + '</a>' : val);
                    return res;
                }},
                {text: 'Курьер', dataIndex: 'count_courier', width: 100, renderer: function(val,meta,rec){return view.countsRenderer(val, rec, 2);},
                    summaryType: function(rows) { return view.summaryCalc(rows, 2); },
                    summaryRenderer: function(value, summaryData, dataIndex) { return view.summaryRender(value, view, 2); }
                },
                {text: 'Конверт почта', dataIndex: 'count_envelope_post', width: 100, renderer: function(val,meta,rec){return view.countsRenderer(val, rec, 3);},
                    summaryType: function(rows) { return view.summaryCalc(rows, 3); },
                    summaryRenderer: function(value, summaryData, dataIndex) { return view.summaryRender(value, view, 3); }
                },
                {text: 'Конверт почта ДЗ', dataIndex: 'count_envelope_dz', width: 100, renderer: function(val,meta,rec){return view.countsRenderer(val, rec, 4);},
                    summaryType: function(rows) { return view.summaryCalc(rows, 4); },
                    summaryRenderer: function(value, summaryData, dataIndex) { return view.summaryRender(value, view, 4); }
                },
                //{text: 'Конверт юр.л.', dataIndex: 'count_envelope_org', width: 80, renderer: function(val,meta,rec,row,col,store,grid){return countsRenderer(val, rec, 5);}},
            ]
        });

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false,
            layout                : 'hbox'
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            },
            features: [{
                ftype: 'summary'
            }],
            xfilters : [
                {fieldLabel: 'издание', xtype: 'combo', name: 'periodical_id', store: subscribleMagazinesStore, editable : false, multiSelect: true, invalidCls: ''},
                {fieldLabel: 'поряд.№ выпуска', name: 'issue_num', xtype: 'textfield'},
                {fieldLabel: 'заголовок выпуска', name: 'issue_title', xtype: 'textfield'},
                {fieldLabel: 'год', name: 'year', xtype: 'textfield'},
                {fieldLabel: 'месяц', xtype: 'combo', name: 'month', store: [1,2,3,4,5,6,7,8,9,10,11,12], editable : false, multiSelect: true, invalidCls: ''},
                {fieldLabel: 'дата отгрузки', name: 'shipment_date_start', xtype: 'datefield', group: 'shipment_date', width: 230, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                    {name: 'shipment_date_end', xtype: 'datefield', group: 'shipment_date', width: 120, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                {fieldLabel: 'дата оплаты', name: 'payment_date_start', xtype: 'datefield', group: 'payment_date', width: 230, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                    {name: 'payment_date_end', xtype: 'datefield', group: 'payment_date', width: 120, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                {fieldLabel: 'тип подписчика', name: 'subscriber_type_id', xtype: 'combo', store: subscriber_type_store},
                {fieldLabel: 'оплата через', name: 'payment_agent_id', xtype: 'combo', store: payment_agents_store, editable : false, multiSelect: true, invalidCls: ''},
                //{fieldLabel: 'тип подписки', name: 'subscription_type', xtype: 'combo', store: subscription_type_store2},
                {fieldLabel: 'отгружено?', name: 'is_shipped', xtype: 'combo', editable : false, store: [['0','нет'],['1','да']]},
                {fieldLabel: 'способ оплаты', name: 'payment_type_id', xtype: 'combo', store: payment_types_store, editable : false, multiSelect: true, invalidCls: ''},
            ]
        });
    
        Ext.apply(this, {     
            dockedItems: [
            {
                xtype : 'toolbar',
                items : [
                    // {
                    //     xtype: 'view_state_switcher',
                    // },
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetFilters();
                        }
                    },
                    {
                        icon    : '/images/extjs/icons/list-prev.png',
                        tooltip : 'предыдущий месяц',
                        scope   : this,
                        handler : function() {
                            var cur_month  = this.getHeaderFilterField('month').getRawValue();
                            var cur_year   = this.getHeaderFilterField('year').getRawValue();
                            var prev_month = (parseInt(cur_month)-1) == 0 ? 12 : (parseInt(cur_month)-1);
                            var prev_year  = (parseInt(cur_month)-1) == 0 || !cur_month ? parseInt(cur_year)-1 : cur_year;
                            if(cur_month) {
                                this.setHeaderFilter('month', prev_month, prev_year ? false : true);
                            }
                            if(prev_year) {
                                this.setHeaderFilter('year', prev_year);
                            }
                        }
                    },
                    {
                        icon    : '/images/extjs/icons/list-next.png',
                        tooltip : 'следующий месяц',
                        scope   : this,
                        handler : function() {
                            var cur_month  = this.getHeaderFilterField('month').getRawValue();
                            var cur_year   = this.getHeaderFilterField('year').getRawValue();
                            var next_month = (parseInt(cur_month)+1)>12 ? 1 : (parseInt(cur_month)+1);
                            var next_year  = (parseInt(cur_month)+1)>12 || !cur_month ? parseInt(cur_year)+1 : cur_year;
                            if(cur_month) {
                                this.setHeaderFilter('month', next_month, next_year ? false : true);
                            }
                            if(next_year) {
                                this.setHeaderFilter('year', next_year);
                            }
                        }
                    },
                    '-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Отгрузки - статистика');
                                }
                            },this);   
                        }
                    },
                    '-',
                    {
                        text       : 'Лог',
                        iconCls    : 'ship-ico',
                        scope      : this,
                        href       : '/#shipment/index',
                        hrefTarget : '_self'
                    },
                    '->',
                    {
                        text       : 'Списком',
                        iconCls    : 'ship-ico',
                        scope      : this,
                        href       : '/#subscription/ship',
                        hrefTarget : '_self'
                    },
                    '-',
                    {
                        text       : 'Почта России',
                        iconCls    : 'ship-ico',
                        scope      : this,
                        href       : '/#subscription/shiptomap',
                        hrefTarget : '_self'
                    },
                    '-',
                    {
                        text       : 'Статистика',
                        iconCls    : 'ship-ico',
                        scope      : this,
                        href       : '/#subscription/shiptable',
                        hrefTarget : '_self'
                    }
                ]
            }],
        });

        this.callParent(arguments);

        var filtered = 0;
        if(this.nxparams.periodical_name)
        {
            filtered = 1;
            var periodical_name = this.nxparams.periodical_name;
            var periodical_id   = Ext.Array.findBy(subscribleMagazinesStore, function(item){ if(item[1]==periodical_name) return true; else return false; })[0];
            this.store.filters.add('periodical_id', new Ext.util.Filter({
                property: 'periodical_id',
                value   : periodical_id
            }));
        }
        if(this.nxparams.issue_num)
        {
            filtered = 1;
            this.store.filters.add('issue_num', new Ext.util.Filter({
                property: 'issue_num',
                value   : this.nxparams.issue_num
            }));
        }
        if(this.nxparams.issue_title)
        {
            filtered = 1;
            this.store.filters.add('issue_title', new Ext.util.Filter({
                property: 'issue_title',
                value   : this.nxparams.issue_title
            }));
        }
        if(this.nxparams.year)
        {
            filtered = 1;
            this.store.filters.add('year', new Ext.util.Filter({
                property: 'year',
                value   : this.nxparams.year
            }));
        }

        if(!filtered)
        {
            var cur_date = new Date();
            this.store.filters.add('year', new Ext.util.Filter({
                property: 'year',
                value   : cur_date.getFullYear()
            }));
            this.store.filters.add('month', new Ext.util.Filter({
                property: 'month',
                value   : cur_date.getMonth()+1
            }));
        }
        this.addEvents('nx_pdf_log_details');
    },
    resetFilters: function() {
        this.resetHeaderFilters(false);
        this.store.clearFilter(true);
        var cur_date = new Date();
        this.setHeaderFilter('month', cur_date.getMonth()+1, false);
        this.setHeaderFilter('year', cur_date.getFullYear()); 
        //this.store.load();
    },
    getFilterValueBy:function(property) {
        var filters = this.store.filters;
        var index   = filters.findIndex('property', new RegExp('^' + property + '$'));
        return index==-1 ? null : filters.getAt(index).value;
    },
});