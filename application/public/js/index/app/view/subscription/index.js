/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.subscription.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_subscription_list',
    itemId: 'nx_subscription_list',

    title: 'Подписки',

    store: 'subscription',

    initComponent: function() {

        this.actionsmenu = Ext.create('Ext.menu.Menu', {
            showSeparator : false,
            cls           : 'no-icon-menu',
            items         : [
                {
                    //iconCls : 'x-hide-display',
                    text    : 'Продлить',
                    tooltip : 'клонировать заказ, сбросить данные об оплате и пометку о конверте, а дату начала увеличить на период',
                    scope   : this,
                    handler : function(btn) {
                        this.fireEvent('nx_subscription_extend', this, this.actionsmenu.rec);
                    }
                },
                {
                    //iconCls : 'x-hide-display',
                    text: 'Выбрать с таким же email',
                    scope: this,
                    handler: function(btn) {
                        if(this.actionsmenu.rec.get('email')) {
                            this.resetHeaderFilters(false);
                            this.store.clearFilter(true);
                            this.setHeaderFilter('name', this.actionsmenu.rec.get('email'));
                            this.store.filter();
                        }
                        else {
                            Ext.Msg.alert('Ошибка', 'У заказа отсутствует email!');
                        }
                    }
                },
                {
                    //cls  : this.actionsmenu.rec.get('subscriber_type_id')==2 ? 'x-hide-display' : '',
                    text    : 'Отправить юр. лицо в 1С',
                    scope   : this,
                    handler : function(btn) {
                        this.fireEvent('nx_subscription_company_send_ws1c', btn, this.actionsmenu.rec);
                    }
                },
                {
                    text    : 'Зарегистрировать юзера',
                    scope   : this,
                    hidden  : !nx.app.isperm('developer'),
                    handler : function(btn) {
                        this.fireEvent('nx_subscription_register', this.actionsmenu.rec);
                    }
                }
            ],
            listeners: {
                show: function() {
                    if(this.rec.get('subscriber_type_id')!=2) {
                        this.items.getAt(2).hide();
                    }
                    else {
                        this.items.getAt(2).show();
                    }
                }
            }
        });

        this.columns = [
            {
                xtype        : 'actioncolumn',
                width        : 60,
                menuDisabled : true,
                items        : [
                    {
                        icon    : '/images/extjs/icons/edit.png',
                        tooltip : 'Редактировать',
                        scope   : this,
                        handler : function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            //alert("Edit " + rec.get('firstname'));
                            //window.location.hash = '#subscription/edit/id/' + rec.get('id');
                            this.fireEvent('nx_subscription_edit', this, rec);
                        }
                    },
                    /*{
                        icon: '/images/extjs/icons/enabled.png',
                        //tooltip: 'Выключить',
                        scope: this,
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Подтверждение', (rec.get('enabled')=='0' ? 'Включить' : 'Выключить') + ' подписку?', function(btn){
                                if (btn == 'yes')
                                {
                                   this.fireEvent('nx_subscription_switch_field', this, rec, 'enabled');
                                }
                            },this); 
                        },
                        getClass: function(value, data, record, rowIndex, colIndex, store) {
                            var rec = store.getAt(rowIndex);
                            if(rec.get('enabled')=='0')
                            {
                                this.columns[0].items[1].tooltip = 'Включить';
                                return 'subscription-disabled';
                            }
                            this.columns[0].items[1].tooltip = 'Выключить';
                            return 'subscription-enabled';
                        }
                    },*/
                    {
                        icon    : '/images/extjs/icons/subscription-remind.gif',
                        tooltip : 'Напоминание',
                        scope   : this,
                        handler : function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            //if(rec.get('order_state_id')!='3' && rec.get('type')=='0')
                            //{ 
                                //view = Ext.widget('nx_subscription_remind', {subscription: rec});
                                Ext.widget('nx_ux_mailform', {
                                    entity_id       : rec.get('id'), 
                                    entity_name     : 'order',
                                    log_message     : 'Отправка письма для подписки #' + rec.get('id'),
                                    order_id        : rec.get('id'),
                                    get_message_url : '/subscription/get-mail-message',
                                    email           : rec.get('email'),
                                    templates       : [
                                        /*                                  {id: 9, name: 'Шаблон продления из личного кабинета'},*/
                                        /*                          {id: 161, name: 'Шаблон продления оператором'}*/

                                        // {id: 1, name: 'Шаблон напоминания'},
                                        // {id: 14, name: 'Шаблон продления'},
                                        // {name: 'Квитанция/Счет', url: nx.config.services.paydoc.url + '?hash=' + rec.get('hash'), subject: 'Оформление подписки на сайте Издательства Открытые системы'},
                                        // {id: 21, name: 'Шаблон из асурц №21'},
                                    ]
                                });
                            //}
                        }/*,
                        getClass: function(value, data, record, rowIndex, colIndex, store) {
                            var rec = store.getAt(rowIndex);
                            //if(rec.get('order_state_id')=='3' || rec.get('type')!='0')
                            //{
                            //    return 'subscription-disabled';
                            //}
                            return 'subscription-enabled';
                        }*/
                    },
                    {
                        icon    : '/images/extjs/icons/expand.png',
                        tooltip : 'действия',
                        scope   : this,
                        handler : function(grid, rowIndex, colIndex, item, e) {
                            var rec = grid.getStore().getAt(rowIndex);
                            this.actionsmenu.rec = rec;
                            this.actionsmenu.showAt(e.getXY());
                        }
                    }
                ]
            },
            {text: 'ID',  dataIndex: 'id', width: 50, filter: {xtype: 'textfield'}},
            {
                text: 'Подписчик (возможен поиск по email)', 
                //xtype: 'templatecolumn', 
                dataIndex: 'name', 
                flex: 1, 
                tdCls: 'nx-multiline',
                //tpl: '{name}', 
                //, renderer: function(value) {return Ext.String.format('<div class="topic">{0}</div>', value);}
                xfilter: true
            },
            {text: 'Email', dataIndex: 'email', width: 120, xfilter: true, hidden: true},
            {text: 'Телефон', dataIndex: 'phone', width: 120, xfilter: true, hidden: true},
            {text: 'Индекс', dataIndex: 'map_zipcode', width: 60, xfilter: true, hidden: true},
            {text: 'Адрес', dataIndex: 'full_map_address', flex: 1, hidden: true},
            {text: 'Комментарий', dataIndex: 'comment', flex: 1, hidden: true, tdCls: 'nx-multiline'},
            {text: 'Состояние', dataIndex: 'order_state_name', width: 70, xfilter: 'order_state_id'},
            {text: 'Издание', dataIndex: 'periodical_names', width: 100, xfilter: 'periodical_id'},
            {text: 'Начало подписки', dataIndex: 'date_start_sub',hidden: true, width: 100, xfilter: 'date_start_sub_id', renderer: Ext.util.Format.dateRenderer('Y-m-d')},
            {text: 'Сумма', dataIndex: 'price', width: 80, xfilter: true, renderer: function(v) { return v.toString().replace('.',','); }, 
                summaryType: function(rows) {
                    var sum = 0;
                    Ext.each(rows, function(row){
                        sum+=parseFloat(row.get('price'));
                    });
                    return sum.toFixed(2);
                }/*,
                summaryRenderer: function(value, summaryData, dataIndex) {
                    return Ext.String.format('Итого: {0}', value); 
                }*/
            },
            {text: 'Способ оплаты', dataIndex: 'payment_type_name', width: 90, xfilter: 'payment_type_id'},
            {text: 'Способ доставки', dataIndex: 'delivery_type_names', width: 100, xfilter: 'delivery_type_id'},
            {text: 'Дата оплаты', dataIndex: 'payment_date',  width: 90, renderer: Ext.util.Format.dateRenderer('Y-m-d'), xfilter: true},
            {text: 'Дата создания', dataIndex: 'created', width: 120, xfilter: true},
            {text: 'Дата обновления', dataIndex: 'last_updated', width: 120, xfilter: true, hidden: true},
            {text: 'Метка', dataIndex: 'label', width: 150, xfilter: true},
            //{text: 'Актуальна?', dataIndex: 'is_present', width: 70, sortable: false, renderer: function(value) {if(value=='1') return 'да'; else return 'нет';}, xfilter: true},
            {text: 'Xpress ID',  dataIndex: 'xpress_id', width: 60, hidden: true, xfilter: true},
            {text: 'Тип подписчика', dataIndex: 'subscriber_type_id', width: 80, hidden: true, xfilter: true, renderer: function(val) { 
                    for(var i=0; i<subscriber_type_store.length; i++) { if(subscriber_type_store[i][0] == val) return subscriber_type_store[i][1]; } 
            }},
            {text: 'ИНН', dataIndex: 'company_inn', width: 90, hidden: true},
            {text: 'КПП', dataIndex: 'company_kpp', width: 90, hidden: true},
            {text: 'Оплата через', dataIndex: 'payment_agent_id', width: 90, hidden: true, xfilter: true, renderer: function(val){
                for(var i=0; i<payment_agents_store.length; i++) { if(payment_agents_store[i][0] == val) return payment_agents_store[i][1]; } 
            }},
            {
                xtype        :'actioncolumn',
                width        :30,
                menuDisabled : true,
                items        : [{
                    icon: '/images/extjs/icons/disabled.png',
                    tooltip: 'Вкл/Выкл',
                    handler: function(grid, rowIndex, colIndex) 
                    {
                        var rec = grid.getStore().getAt(rowIndex);
                        Ext.Msg.confirm('Подтверждение', (rec.get('enabled')=='0' ? 'Включить' : 'Выключить') + ' подписку?', function(btn){
                            if (btn == 'yes')
                            {
                                this.up('nx_subscription_list').fireEvent('nx_subscription_switch_field', grid, rec, 'enabled');
                            }
                        }, this); 
                    },
                    getClass: function(value, data, record, rowIndex, colIndex, store) 
                    {
                        var rec = store.getAt(rowIndex);
                        if(rec.get('order_state_id')=='3' && rec.get('enabled')=='1') {
                            return 'x-hide-display';
                        }
                        if(rec.get('enabled')=='0')
                        {
                            return 'nx-disabled';
                        }
                        return 'nx-enabled';
                    }
                }]
            }
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        /*var filters = {
            ftype: 'filters',
            showMenu: false,
            menuFilterText: 'Фильтровать',
            encode: true,
            local: false,
            filters: [{
                type: 'textfield',
                dataIndex: 'manager_id'
            }]
        };*/
        Ext.apply(this, {
            plugins: [gridheaderfilters],
            //features: [filters],       
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    var css_class = '';
                    if(rec.get('order_state_id') == 3 && rec.get('subscriber_type_id') == '2' && rec.get('delivery_type_names').indexOf('pdf') != -1) {
                        return 'subscription-row-orgpdf';
                    }
                    css_class += rec.get('order_state_id') == 3 ? 'subscription-row-payed' : '';
                    css_class += rec.get('enabled') == 0 ? ' subscription-row-disabled' : ''; 
                    return css_class;
                } 
            },
            features: [{
                ftype: 'summary'
            }],
            xfilters : [//filters property already taken up by filters feature 
                {fieldLabel: 'id', name: 'id', xtype: 'textfield'},
                {fieldLabel: 'подписчик', name: 'name', xtype: 'textfield'},
                {fieldLabel: 'email', name: 'email', xtype: 'textfield'},
                {fieldLabel: 'телефон', name: 'phone', xtype: 'textfield'},
                {fieldLabel: 'состояние', name: 'order_state_id', xtype: 'combo', store: orderStatesStore},
                {fieldLabel: 'издание', name: 'periodical_id', xtype: 'combo', store: magazines_store, editable : false, multiSelect: true, invalidCls: ''},
                {fieldLabel: 'дата начала', name: 'date_start_sub_id', xtype: 'datefield',  format: 'Y-m-d'},
                {fieldLabel: 'сумма', name: 'price', xtype: 'textfield'},
                {fieldLabel: 'способ оплаты', name: 'payment_type_id', xtype: 'combo', store: payment_types_store},
                {fieldLabel: 'способ доставки', name: 'delivery_type_id', xtype: 'combo', store: delivery_types_store},
                //{fieldLabel: 'дата', name: 'date', xtype: 'textfield'},
                //{fieldLabel: 'интервал дат', name: 'date_start', xtype: 'datefield', group: 'date', width: 230, format: 'Y-m-d', allowSubmitBlank: false},
                //    {name: 'date_end', xtype: 'datefield', group: 'date', width: 120, format: 'Y-m-d', allowSubmitBlank: false},
                {fieldLabel: 'дата создания', name: 'created', xtype: 'textfield'},
                {fieldLabel: 'интервал созд.', name: 'created_start', xtype: 'datefield', group: 'created', width: 230, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                    {name: 'created_end', xtype: 'datefield', group: 'created', width: 120, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                {fieldLabel: 'дата оплаты', name: 'payment_date', xtype: 'textfield'},
                {fieldLabel: 'интервал опл.', name: 'payment_date_start', xtype: 'datefield', group: 'payment_date', width: 230, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                    {name: 'payment_date_end', xtype: 'datefield', group: 'payment_date', width: 120, format: 'Y-m-d', allowSubmitBlank: false, editable: false},
                {fieldLabel: 'дата обновления', name: 'last_updated', xtype: 'textfield'},
                {fieldLabel: 'метка', name: 'label', xtype: 'textfield', ops: ['=','!=']},
                {fieldLabel: 'актуальна?', name: 'is_present', xtype: 'combo', editable : false, store: [['0','нет'],['1','да']]},
                //{fieldLabel: 'включена?', name: 'enabled', xtype: 'combo', editable : false, store: [['0','нет'],['1','да']]},
                {fieldLabel: 'xpress ID', name: 'xpress_id', xtype: 'textfield'},
                {fieldLabel: 'тип подписчика', name: 'subscriber_type_id', xtype: 'combo', store: subscriber_type_store},
                //{fieldLabel: 'ИНН', name: 'company_inn', xtype: 'textfield'},
                //{fieldLabel: 'КПП', name: 'company_kpp', xtype: 'textfield'},
                {fieldLabel: 'оплата через', name: 'payment_agent_id', xtype: 'combo', store: payment_agents_store},
                {fieldLabel: 'менеджер', name: 'manager_id', xtype: 'combo', displayField: 'name', valueField: 'id', store: Ext.create('Ext.data.Store', {fields : ['id', 'name'], data : managers_data})},
                {fieldLabel: 'новый заказ?', name: 'new_state', xtype: 'combo', store: [['1','да'],['-1','нет']], editable: false},
                {fieldLabel: 'тип', name: 'type', xtype: 'combo', store: subscription_type_store},
                {fieldLabel: 'промокод', name: 'action_code', xtype: 'textfield'},
                {fieldLabel: 'id акции', name: 'action_id', xtype: 'textfield'},
                {fieldLabel: 'заголовок выпуска', name: 'issue_title', xtype: 'textfield'},
                {fieldLabel: 'теги', name: 'tags', xtype: 'nx.ux.JsonCombo', url: '/tag/get-objects-for-combo', baseParams: {entity_id: 1}, editable : false, multiSelect: true, ops: ['AND','OR']}
            ]
        });
    
        Ext.apply(this, {
                   
            dockedItems: [
            {
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных',
                plugins     : [{ptype: 'pagesize'}]
            },
            {
                xtype: 'toolbar',
                items : [
                    // {
                    //     xtype: 'view_state_switcher',
                    // },
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters(false);
                            this.store.clearFilter();
                        }
                    },
                    '-',
                    {
                        text         : 'Показать все',
                        enableToggle : true,
                        is_enabled   : 0,
                        handler      : function() 
                        {
                            this.is_enabled = !this.is_enabled;
                            var store = this.up('nx_subscription_list').getStore();
                            if(this.is_enabled)
                            {
                               store.getProxy().extraParams['show_all'] = 1;
                            }
                            else
                            {
                                delete store.getProxy().extraParams.show_all;
                            }
                            store.load();
                        }
                    },
                    {
                        text         : 'Выключенные',
                        enableToggle : true,
                        handler      : function() 
                        {
                            var store = this.up('nx_subscription_list').getStore();
                            if(this.pressed)
                            {
                                store.filters.add('enabled', new Ext.util.Filter({
                                    property: 'enabled',
                                    value   : 0
                                }));
                            }
                            else
                            {
                                store.filters.removeAtKey('enabled');
                            }
                            store.load();
                        }
                    },
                    '-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Подписки');
                                }
                            },this);   
                        }
                    },
                    // '-',
                    // {
                    //     text       : 'По содержанию',
                    //     iconCls    : 'subscription-items-ico',
                    //     scope      : this,
                    //     href       : '/#subscription/items',
                    //     hrefTarget : '_self',
                    //     hidden     : this.nxparams.by ? true : false
                    // },
                    '->',
                    {
                        xtype   : 'splitbutton',
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        handler : function() {
                            this.fireEvent('nx_subscription_add', this);
                        },
                        menu: {
                            showSeparator : false,
                            cls           : 'no-icon-menu',
                            items         : [
                                {
                                    text    : 'загрузить CSV',
                                    hidden  : !nx.app.isperm('admin') && nx.config.user.email!='sobol@osp.ru',
                                    scope   : this,
                                    handler : function() {
                                        Ext.widget('nx_subscription_add_csv', {grid: this});
                                    }
                                }
                            ]
                        }
                    }
                ]
            }],
        });

        this.addEvents('nx_subscription_edit');
        this.addEvents('nx_subscription_add','nx_subscription_switch_field');
        //this.addEvents('nx_subscription_clone');
        this.addEvents('nx_subscription_extend');
        this.callParent(arguments);
    },
    //listeners: {
    //    scope: this,
        /*'headerfiltersrender': function() {
            this.getHeaderFilterField('checked').setValue('1');
            this.applyHeaderFilters();
        }*/
        /*beforeitemcontextmenu: function(view, record, item, index, e)
        {console.log(this,view);
            e.stopEvent();
            view.up('grid').contextMenu.showAt(e.getXY());
            return false;
        }*/
    //}
});