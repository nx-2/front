/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

/*Ext.define('nx.view.subscription.remind', {
    extend: 'Ext.window.Window',
    alias: 'widget.nx_subscription_remind',

    title: 'Отправить напоминание',
    autoShow: true,
    width: 800,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        border  : 0,
                        padding : 5,
                        items   : [
                            {
                                xtype      : 'textfield',
                                name       : 'subject',
                                fieldLabel : 'Тема',
                                labelWidth : 70,
                                inputWidth : 700
                            }
                        ]
                    },
                    {
                        xtype : 'htmleditor',
                        width : '100%',
                        height: 500,
                        //grow  : true,
                        //growMax : 400,
                        name  : 'message'
                    }
                ]
            }
        ];

        Ext.apply(this, { 
            dockedItems: [
                {
                    xtype: 'toolbar',
                    items : [
                        {
                            text    : 'Шаблон напоминания',
                            scope   : this,
                            handler : function() {
                                this.loadMailFormByTemplateID(this.subscription.get('label') == 'lvrach_subscription' ? 2 : 1);
                            }
                        },
                        {
                            text    : 'Шаблон продления из личного кабинета',
                            scope   : this,
                            handler : function() {
                                this.loadMailFormByTemplateID(9);
                            }
                        },
                        {
                            text    : 'Шаблон продления',
                            scope   : this,
                            handler : function() {
                                this.loadMailFormByTemplateID(14);
                            }
                        },
                        {
                            text    : 'Квитанция/Счет',
                            scope   : this,
                            handler : function() {
                                this.loadMailFormByUrl(nx.config.services.paydoc.url + '?hash=' + this.subscription.get('hash'), 'Оформление подписки на сайте Издательства Открытые системы');
                            }
                        }
                    ]
                }
            ]
        });


        this.buttons = [
            {
                text: 'Отправить',
                scope: this,
                handler: function(){
                    this.down('form').getForm().submit({
                        url     : '/subscription/send-mail-message',
                        waitMsg : 'Отправка данных',
                        scope   : this,
                        params  : {
                            subscription_id : this.subscription.get('id')
                        },
                        success : function(form, response) {
                            this.close();
                        }
                    });
                }
            },
            {
                text: 'Отправить себе',
                scope: this,
                handler: function(){
                    this.down('form').getForm().submit({
                        url     : '/subscription/send-mail-message',
                        waitMsg : 'Отправка данных',
                        scope   : this,
                        params  : {
                            debug : true,
                            subscription_id : this.subscription.get('id')
                        },
                        success : function(form, response) {
                            this.close();
                        }
                    });
                }
            },
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    },
    onRender: function() 
    {
        this.callParent(arguments);
        //console.log(this.down('toolbar').items.getAt(0));
        //this.down('toolbar').items.getAt(0).fireEvent('click', this.down('toolbar').items.getAt(0));
        var btn = this.down('toolbar').items.getAt(0);
        btn.handler.call(this);
    },
    loadMailFormByTemplateID: function(template_id)
    {
        this.down('form').getForm().load(
        {
            url     : '/subscription/get-mail-message',
            waitMsg : 'Загрузка',
            scope   : this,
            params  : 
            {
                subscription_id : this.subscription.get('id'),
                template_id     : template_id
            },
            success : function() 
            {
            }
        });
    },
    loadMailFormByUrl: function(url, subject)
    {
        Ext.Ajax.request({
            url     : '/index/get-url-content',
            waitMsg : 'Загрузка',
            method  : 'POST',
            params  : {
                subscription_id : this.subscription.get('id'),
                url             : url
            },
            scope   : this,
            success : function(response) {
                var response = Ext.JSON.decode(response.responseText);
                this.down('htmleditor[name="message"]').setValue(response.data);
                if(subject) {
                    this.down('textfield[name="subject"]').setValue(subject);
                }
            }
        });
    }
});*/