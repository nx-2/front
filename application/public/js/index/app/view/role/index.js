/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.role.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_role_list',

    title: 'Роли',

    store: 'role',

    initComponent: function() {

        /*Ext.apply(this, {
            store: new Ext.data.JsonStore({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/role/index'
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields        : [
                    {name: 'id'},
                    {name: 'name'}
                ]
            })
        });*/

        this.columns = [
            {
                xtype:'actioncolumn',
                width:50,
                items: [
                {
                    icon: '/images/extjs/icons/edit.png',
                    tooltip: 'Редактировать',
                    scope: this,
                    handler: function(grid, rowIndex, colIndex) 
                    {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_role_edit', this, rec);
                    }
                }]
            },
            {header: 'id',  dataIndex: 'id',  flex: 1},
            {header: 'Название',  dataIndex: 'name',  flex: 1}
        ];

        Ext.apply(this, {
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    '->',
                    {
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        handler : function() {
                            this.fireEvent('nx_role_add', this);
                        }
                    }
                ]
            }]
        });

        this.addEvents('nx_role_edit');
        this.addEvents('nx_role_add');
        this.callParent(arguments);
    }
});