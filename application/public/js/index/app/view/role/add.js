/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.role.add', {
    extend: 'Ext.window.Window',
    alias: 'widget.nx_role_add',
    title: 'Добавление роли',
    autoShow: true,
    width: 700,

    initComponent: function() {
        var viewid = this.id;
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title: 'свойства',
                                padding: 5,
                                layout: 'form',
                                border: 0,
                                items: [
                                    {
                                        xtype      : 'textfield',
                                        name       : 'name',
                                        fieldLabel : 'Название',
                                        allowBlank : false
                                    },
                                    {
                                        xtype      : 'textfield',
                                        name       : 'code',
                                        fieldLabel : 'Техническое название',
                                        vtype      : 'alpha',
                                        allowBlank : false
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Добавить',
                action: 'add'
            },
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});