/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.user.edit', {
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_user_edit',
    title    : 'Редактирование пользователя',
    autoShow : true,
    width    : 900,

    initComponent: function() {
        var viewid = this.id;
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title: 'роли',
                                layout: 'fit',
                                padding: 5,
                                items: [
                                    {
                                        xtype: 'grid',
                                        store: new Ext.data.JsonStore({
                                            proxy: {
                                                type: 'ajax',
                                                url : '/user/get-roles',
                                                reader: {
                                                    type: 'json',
                                                    root: 'items',
                                                    successProperty: 'success',
                                                    totalProperty: 'total'
                                                },
                                                extraParams: {
                                                    user_id: this.nxparams.id
                                                }
                                            },
                                            pageSize: 999,
                                            remoteFilter: true,
                                            autoLoad : true,
                                            fields        : [
                                                {name: 'id', mapping: 'id'},
                                                {name: 'name', mapping: 'name'}
                                            ]
                                        }),
                                        columns    : [
                                            {dataIndex: 'name', flex: 1}
                                        ],
                                        viewConfig : { 
                                            enableTextSelection: true
                                        },
                                        header       : false,
                                        allowDeselect: true,
                                        bbar: [
                                            { 
                                                xtype   : 'button', 
                                                text    : 'Добавить', 
                                                iconCls : 'add-ico',
                                                scope   : this,
                                                handler : function() {
                                                    this.openAddUserRoleWindow(this.nxparams.id);
                                                }
                                            },
                                            { 
                                                xtype   : 'button', 
                                                text    : 'Удалить', 
                                                iconCls : 'delete-ico',
                                                scope   : this,
                                                handler : function() {
                                                    var selection = this.down('grid').getSelectionModel().getSelection();
                                                    if(selection.length)
                                                    {
                                                        Ext.Msg.confirm('Подтверждение', 'Удалить выделенную роль?', function(btn){
                                                            if (btn == 'yes')
                                                            {   
                                                                Ext.Ajax.request({
                                                                    url: '/user/delete-role',
                                                                    method  : 'POST',
                                                                    waitMsg : 'Отправка данных',
                                                                    scope   : this,
                                                                    params  : {
                                                                        user_id: this.nxparams.id,
                                                                        role_id: selection[0].get('id')
                                                                    },
                                                                    success: function(form, action) 
                                                                    {
                                                                        this.down('grid').getStore().reload();
                                                                    }
                                                                });
                                                            }
                                                        }, this);
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            /*{
                text: 'Сохранить',
                action: 'save'
            },*/
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    },
    openAddUserRoleWindow : function(user_id) {
        ExtWindow = new Ext.window.Window({
            title     : 'Добавление роли',
            id        : 'addUserRoleWindow',
            iconCls   : 'role-ico',
            layout    : 'fit',
            width     : 280,
            border    : false,
            resizable : false,
            modal     : true,
            autoShow  : true, 
            items     : [
                {
                    xtype          : 'combo',
                    name           : 'role_id',
                    store          : roles_store,
                    forceSelection : true,
                    editable       : false,
                    queryMode      : 'local',
                    triggerAction  : 'all',
                    value          : '1',
                    id             : 'newRole'
                }
            ],
            buttons : [
                {
                    text    : 'Добавить',
                    scope   : this,
                    handler : function() {
                        Ext.Ajax.request({
                            url     : '/user/add-role',
                            method  : 'POST',
                            params  : {
                                user_id : user_id,
                                role_id : Ext.getCmp('newRole').getValue()
                            },
                            scope   : this,
                            success : function(response) {
                                this.down('grid').getStore().reload();
                                Ext.getCmp('addUserRoleWindow').close();
                            }
                        });
                    }
                }
            ]
        });
    }
});