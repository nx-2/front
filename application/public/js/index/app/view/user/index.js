/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.user.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_user_list',

    title: 'Пользователи',

    store: 'user',

    initComponent: function() {

        this.columns = [
            {
                xtype:'actioncolumn',
                width:50,
                items: [
                {
                    //icon: '/images/extjs/icons/edit.png',
                    iconCls  : 'edit-ico',
                    tooltip: 'Редактировать',
                    scope: this,
                    handler: function(grid, rowIndex, colIndex) 
                    {
                        var rec = grid.getStore().getAt(rowIndex);
                        //alert("Edit " + rec.get('firstname'));
                        //window.location.hash = '#user/edit/id/' + rec.get('id');
                        this.fireEvent('nx_user_edit', this, rec);
                        //Ext.History.fireEvent('change', 'users/edit/id/' + rec.get('id'), {rec: rec});
                        //Ext.History.add('users/edit/id/' + rec.get('id'));
                    }
                },
                {
                    iconCls  : 'user-go-ico',
                    tooltip  : 'Превратиться в пользователя',
                    scope    : this,
                    handler  : function(grid, rowIndex, colIndex) 
                    {
                        var rec = this.store.getAt(rowIndex);
                        window.location = '/user/auth-fake-user/user_id/' + rec.get('id')
                    }
                }]
            },
            {header: 'id',  dataIndex: 'id',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Логин',  dataIndex: 'login',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'ФИО',  dataIndex: 'name',  flex: 1, filter: {xtype: 'textfield'}},
            {header: 'Email', dataIndex: 'email', flex: 1, filter: {xtype: 'textfield'}}//,
            //{header: 'Включен', dataIndex: 'enabled', flex: 1}
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true
            },
            features: [{ftype:'grouping'}]
        });

        Ext.apply(this, {
            
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: this.store,
                dock: 'bottom',
                displayInfo: true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    }
                ]
            }],
        });

        this.addEvents('nx_user_edit');
        this.callParent(arguments);
    }
});