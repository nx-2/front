/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

/*Ext.define('nx.view.payment.index', {
    extend : 'Ext.grid.Panel',
    alias  : 'widget.nx_payment_list',
    title  : 'Платежи 1C',
    store  : 'payment',

    initComponent: function() {

        this.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 2,
            listeners: {
                beforeedit: function(editor, e) {
                    if(e.record.get('status')==1) {
                        return false;
                    }
                }
            }
        });

        this.columns = [
            {header: 'ID',  dataIndex: 'id', width: 50, filter: {xtype: 'textfield'}},
            {header: 'Год', dataIndex: 'year', width: 50, filter: {xtype: 'textfield'}},
            {header: 'ID 1C', dataIndex: 'ws1c_id', width: 100, hidden: true, filter: {xtype: 'textfield'}},
            {
                header: 'NX ID заказа',  dataIndex: 'order_id', width: 80, filter: {xtype: 'textfield'}, editor: {}, 
                renderer :function(value, metadata,record) 
                { 
                    if(record.get('status')!=1)
                    {
                        metadata.tdAttr = 'data-qtip="нажмите дважды для редактирования"'; 
                    }
                    return value; 
                }
            },
            {header: 'ID транз.',  dataIndex: 'transaction_id', width: 60, filter: {xtype: 'textfield'}},
            {header: 'Орг.',  dataIndex: 'payment_agent_name', width: 70, filter: {xtype: 'textfield'}},
            {header: 'Плательщик',  dataIndex: 'contractor_name', width: 150, tdCls: 'nx-multiline', filter: {xtype: 'textfield'}},
            {header: '№ поступл.', dataIndex: 'docno', width: 70, filter: {xtype: 'textfield'}},
            {header: 'Дата поступления', dataIndex: 'date', width: 130, filter: {xtype: 'textfield'}},
            {header: 'Сумма', dataIndex: 'sum', width: 80, filter: {xtype: 'textfield'}},
            {header: 'Назначение', dataIndex: 'purpose', flex: 1, tdCls: 'nx-multiline', filter: {xtype: 'textfield'}},
            {header: 'Комментарий', dataIndex: 'comment', flex: 1, tdCls: 'nx-multiline', filter: {xtype: 'textfield'}},
            {
                header: 'Статус', dataIndex: 'status', maxWidth: 150, tdCls: 'nx-multiline', filter: {xtype: 'textfield'}, 
                renderer :function(value, metadata,record) {
                    return value + (record.get('status_comment') ? ' - ' + record.get('status_comment') : '');
                }
            },
            {
                xtype        : 'actioncolumn',
                width        : 30,
                menuDisabled : true,
                items        : [{
                    icon    : '/images/extjs/icons/refresh.png',
                    tooltip : 'Сбросить статус',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex, item, e, rec) 
                    {
                        Ext.Msg.confirm('Подтверждение',(rec.get('status')==1 ? 'Платеж был успешно сохранен. Все равно с' : 'С') + 'бросить статус?', function(btn){
                            if(btn=='yes')
                            {
                                var id = rec.get('id');
                                Ext.Ajax.request({
                                    url     : '/payment/re',
                                    method  : 'POST',
                                    params  : {
                                        id : id
                                    },
                                    scope   : this,
                                    success : function(response) {
                                        var response = Ext.JSON.decode(response.responseText);
                                        this.getStore().reload();
                                    }
                                });
                            }
                        },this);
                    },
                    getClass: function(value, meta, rec, rowindex)
                    {
                        if( rec.get('status')!=0 && rec.get('status')!=1)
                        {
                            return '';
                        }
                        if( rec.get('status')==1 && rec.get('label')=='ws1c_payment')
                        {
                            return 'nx-transparent';
                        }
                        return 'x-hide-display';
                    }
                }]
            },
            {header: 'Метка', dataIndex: 'label', width: 130, hidden: this.nxparams.filter_label ? true : false, filter: {xtype: 'textfield'}},
            {header: 'Дата создания', dataIndex: 'created',  width: 115, filter: {xtype: 'textfield'}},
            {header: 'Дата обновления', dataIndex: 'last_updated',  width: 115, hidden: true, filter: {xtype: 'textfield'}},
            {
                xtype        : 'actioncolumn',
                width        : 30,
                menuDisabled : true,
                items        : [{
                    icon: '/images/extjs/icons/watch.png',
                    tooltip: 'Показать тех. данные',
                    scope: this,
                    handler: function(grid, rowIndex, colIndex, item, e, rec) 
                    {
                        var id = rec.get('id');
                        Ext.Ajax.request({
                            url     : '/payment/get',
                            method  : 'POST',
                            params  : {
                                id : id
                            },
                            scope   : this,
                            success : function(response) {
                                var response = Ext.JSON.decode(response.responseText);
                                showDataAsTree(response.data);
                            }
                        });
                    }
                }]
            }
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters, this.cellEditing],      
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    return rec.get('status') == 1 ? 'nx-success-row' : (rec.get('status') != 0 ? 'nx-fail-row' : (rec.get('result') ? 'nx-row-warn' : ''));
                } 
            } 
        });
    
        Ext.apply(this, {
            dockedItems: [
            {
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },
                    '->',
                    {
                        text    : 'Отправить',
                        iconCls : 'payment-ico',
                        scope   : this,
                        hidden  : this.nxparams.filter_label == 'ws1c_payment' ? false : true,
                        handler : function(button) {
                            Ext.Msg.confirm('Подтверждение','Отправить платежи в 1С?', function(btn){
                                if(btn=='yes')
                                {
                                    Ext.Ajax.request({
                                        url     : '/payment/send-nds',
                                        method  : 'GET',
                                        scope   : this,
                                        success : function(response) {
                                            this.getStore().reload();
                                            Ext.Msg.alert('Внимание', 'Отправка завершена.');
                                        }
                                    });
                                }
                            },this);  
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);

        if(this.nxparams.filter_label) {
            this.store.filters.add('label', new Ext.util.Filter({
                property: 'label',
                value   : this.nxparams.filter_label
            }));
        }
        else {
            this.store.filters.removeAtKey('label');
        }
    }
});*/