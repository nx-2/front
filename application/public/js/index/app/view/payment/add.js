/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.payment.add', {
    extend: 'Ext.window.Window',
    alias: 'widget.nx_payment_add',

    title: 'Добавление платежа',
    autoShow: true,
    width: 700,
    autoScroll: true,

    initComponent: function() {
        var viewid = this.id;
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title: 'свойства',
                                padding: 5,
                                layout: 'form',
                                border: 0,
                                items: [
                                    {
                                        xtype          : 'combobox',
                                        name           : 'order_state_id',
                                        store          : orderStatesStore,
                                        fieldLabel     : 'Статус',
                                        editable       : false,
                                        queryMode      : 'local',
                                        forceSelection : true,
                                        triggerAction  : 'all',
                                        value          : '2'
                                    },
                                    {
                                        xtype         : 'combobox',
                                        name          : 'payment_type_id',
                                        store         : payment_types_store,
                                        fieldLabel    : 'Способ оплаты',
                                        editable      : false,
                                        forceSelection : true,
                                        queryMode     : 'local',
                                        triggerAction : 'all',
                                        value         : '1'
                                    },
                                    {
                                        xtype: 'textfield',
                                        name : 'comment',
                                        fieldLabel: 'Комментарий'
                                    },
                                    {
                                        xtype          : 'checkbox',
                                        name           : 'enabled',
                                        fieldLabel     : 'Включен?',
                                        inputValue     : '1',
                                        uncheckedValue : '0',
                                        checked        : true
                                    },
                                    {
                                        xtype: 'textfield',
                                        name : 'email',
                                        fieldLabel: 'Email',
                                        vtype: 'email'
                                    },
                                    {
                                        border: 0,
                                        layout: 'hbox',
                                        items: [
                                            {
                                                xtype  : 'textfield',
                                                hidden : true,
                                                name   : 'person_id',
                                                id     : 'nx_add_person_id' + viewid
                                            },
                                            {
                                                xtype      : 'textfield',
                                                fieldLabel : 'Физ. лицо',
                                                name       : 'person_name',
                                                id         : 'nx_add_person_name' + viewid,
                                                readOnly   : true,
                                                flex       : 4
                                            },
                                            {
                                                xtype           : 'nx.ux.JsonCombo',
                                                flex            : 2,
                                                emptyText       : 'выбрать физ. лицо',
                                                url             : '/person/get-objects-for-combo',
                                                matchFieldWidth : false,
                                                listeners       : {
                                                    scope: this,
                                                    'select' : function(combo) {
                                                        Ext.getCmp('nx_add_person_id' + viewid).setValue(combo.getValue());
                                                        Ext.getCmp('nx_add_person_name' + viewid).setValue(combo.getRawValue());
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        border: 0,
                                        layout: 'hbox',
                                        items: [
                                            {
                                                xtype  : 'textfield',
                                                hidden : true,
                                                name   : 'user_id',
                                                id     : 'nx_add_user_id' + viewid
                                            },
                                            {
                                                xtype      : 'textfield',
                                                fieldLabel : 'OSP юзер',
                                                name       : 'user_name',
                                                id         : 'nx_add_user_name' + viewid,
                                                readOnly   : true,
                                                flex       : 4
                                            },
                                            {
                                                xtype           : 'nx.ux.JsonCombo',
                                                flex            : 2,
                                                emptyText       : 'выбрать OSP юзера',
                                                url             : '/user/get-objects-for-combo',
                                                matchFieldWidth : false,
                                                listeners       : {
                                                    scope: this,
                                                    'select' : function(combo) {
                                                        Ext.getCmp('nx_add_user_id' + viewid).setValue(combo.getValue());
                                                        Ext.getCmp('nx_add_user_name' + viewid).setValue(combo.getRawValue());
                                                    }
                                                }
                                            }
                                        ]
                                    }    
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Добавить',
                action: 'add'
            },
            {
                text: 'Закрыть',
                action: 'close',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});