/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.payment.order', {
    extend : 'Ext.grid.Panel',
    alias  : 'widget.nx_payment_order',
    title  : 'Платежи 1C',
    store  : 'payment',

    initComponent: function() {

        this.is_success = this.nxparams.by == 'success';

        this.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 2,
            listeners: {
                beforeedit: function(editor, e) {
                    if(e.record.get('status')>0) {
                        return false;
                    }
                }
            }
        });

        this.columns = [
            {text: 'ID',  dataIndex: 'id', width: 50, filter: {xtype: 'textfield'}},
            {text: 'Год', dataIndex: 'year', width: 50, hidden: true, filter: {xtype: 'textfield'}},
            {text: 'ID 1C', dataIndex: 'ws1c_id', width: 100, hidden: true, filter: {xtype: 'textfield'}},
            {
                text: 'ID заказа',  dataIndex: 'order_id', width: 55, filter: {xtype: 'textfield'}, editor: {}, 
                renderer :function(value, metadata,record, rowIndex, colIndex, store, grid) 
                { 
                    var icon = '';
                    if(record.get('status')<1)
                    {
                        metadata.tdAttr = 'data-qtip="нажмите дважды для редактирования"'; 
                    }
                    if(record.get('status')<=0){
                        icon = '<a href="#" onclick="return false;"><img src="/images/extjs/icons/edit_10.png"/></a>';
                    }
                    return icon + (parseInt(value) ? '<a href="#" onclick="Ext.getCmp(\'' + grid.id + '\').up(\'grid\').onOrderIdClick(\''+record.get('order_id')+'\'); return false;">' + value + '</a>' : value); 
                }
            },
            {text: 'Орг.', dataIndex: 'payment_agent_name', width: 70, filter: {xtype: 'textfield'}},
            {text: 'Плательщик', dataIndex: 'contractor_name_inn', width: 150, tdCls: 'nx-multiline', filter: {xtype: 'textfield'}},
            {text: '№ поступл.', dataIndex: 'docno', width: 70, filter: {xtype: 'textfield'}},
            {text: 'Дата поступления',  dataIndex: 'date', width: 70, filter: {xtype: 'textfield'}, renderer: Ext.util.Format.dateRenderer('Y-m-d')},
            {text: 'Сумма', dataIndex: 'sum', width: 70, filter: {xtype: 'textfield'}, renderer: function(v) { return v.toString().replace('.',','); }},
            {text: 'Назначение', dataIndex: 'purpose', flex: 1, tdCls: 'nx-multiline', filter: {xtype: 'textfield'}},
            {text: 'Комментарий', dataIndex: 'comment', flex: 1, tdCls: 'nx-multiline', filter: {xtype: 'textfield'}},
            {
                text: 'Статус', dataIndex: 'status', width: 140, tdCls: 'nx-multiline', filter: {xtype: 'textfield', hidden: this.is_success ? true : false}, 
                renderer :function(value, metadata,record) { 
                    return value + (record.get('status_comment') ? ' - ' + record.get('status_comment') : '');
                }
            },
            {
                xtype        : 'actioncolumn',
                width        : 50,
                menuDisabled : true,
                items        : [{
                    icon    : '/images/extjs/icons/refresh.png',
                    tooltip : 'Сбросить статус',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex, item, e, rec) 
                    {
                        Ext.Msg.confirm('Подтверждение',(rec.get('status')>0 ? 'Платеж был успешно сохранен. Все равно с' : 'С') + 'бросить статус?', function(btn){
                            if(btn=='yes')
                            {
                                var id = rec.get('id');
                                Ext.Ajax.request({
                                    url     : '/payment/re',
                                    method  : 'POST',
                                    params  : {
                                        id : id
                                    },
                                    scope   : this,
                                    success : function(response) {
                                        var response = Ext.JSON.decode(response.responseText);
                                        //showDataAsTree(response.data);
                                        this.getStore().reload();
                                    }
                                });
                            }
                        },this);
                    },
                    getClass: function(value, meta, rec, rowindex)
                    {
                        if( rec.get('status')<0 )// && rec.get('status')!=1 )
                        {
                            return '';
                        }
                        if( rec.get('status')>0 )
                        {
                            return 'nx-transparent';
                        }
                        return 'x-hide-display';
                    }
                },{
                    icon   : '/images/extjs/icons/warn.png',
                    scope  : this,
                    handler: function(grid, rowIndex, colIndex, item, e, rec) {
                        Ext.Msg.confirm('Подтверждение', 'Скопировать инн, кпп из платежа в юр.лицо заказа?', function(btn){
                            if(btn=='yes')
                            {
                                var id = rec.get('id');
                                Ext.Ajax.request({
                                    url     : '/payment/sync-company',
                                    method  : 'POST',
                                    params  : {
                                        id : id
                                    },
                                    scope   : this,
                                    success : function(response) {
                                        var response = Ext.JSON.decode(response.responseText);
                                        this.getStore().reload();
                                    }
                                });
                            }
                        },this);
                    },
                    getTip: function(value, meta, rec) {
                        return rec.get('contractor_match_comment');
                    },
                    getClass: function(value, meta, rec, rowindex)
                    {
                        if( rec.get('contractor_match_comment')!='' ) {
                            return '';
                        }
                        return 'x-hide-display';
                    }
                }]
            },
            {text: 'Дата создания', dataIndex: 'created',  width: 115, hidden: true, filter: {xtype: 'textfield'}},
            {text: 'Дата обновления', dataIndex: 'last_updated',  width: 115, hidden: true, filter: {xtype: 'textfield'}},
            //{text: 'Включен', dataIndex: 'enabled', width: 70, hidden: true, filter: {xtype: 'textfield'}},
            {
                xtype        : 'actioncolumn',
                width        : 50,
                menuDisabled : true,
                items        : [
                    {
                        icon: '/images/extjs/icons/watch.png',
                        tooltip: 'Показать тех. данные',
                        scope: this,
                        handler: function(grid, rowIndex, colIndex, item, e, rec) 
                        {
                            var id = rec.get('id');
                            Ext.Ajax.request({
                                url     : '/payment/get',
                                method  : 'POST',
                                params  : {
                                    id : id
                                },
                                scope   : this,
                                success : function(response) {
                                    var response = Ext.JSON.decode(response.responseText);
                                    showDataAsTree(response.data);
                                }
                            });
                        }
                    },
                    {
                        icon    : '/images/extjs/icons/disabled.png',
                        tooltip : 'Показать/спрятать',
                        scope   : this,
                        handler : function(grid, rowIndex, colIndex) 
                        {
                            var rec = grid.getStore().getAt(rowIndex);
                            Ext.Msg.confirm('Подтверждение', (rec.get('enabled')=='0' ? 'Показать' : 'Спрятать') + '?', function(btn)
                            {
                                if (btn == 'yes')
                                {
                                    this.fireEvent('nx_switch_field', grid, rec, 'enabled');
                                }
                            }, this); 
                        },
                        getClass: function(value, data, record, rowIndex, colIndex, store) 
                        {
                            var rec = store.getAt(rowIndex);
                            if(this.is_success && rec.get('enabled')=='1')
                            {
                                return 'x-hide-display';
                            }
                            if(rec.get('enabled')=='0')
                            {
                                return 'nx-disabled';
                            }
                            return 'nx-enabled';
                        }
                    }
                ]
            }
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters, this.cellEditing],      
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    return rec.get('status') == 1 ? 'nx-success-row' : (rec.get('status') < 0 ? 'nx-fail-row' : (rec.get('result') ? 'nx-row-warn' : ''));
                } 
            } 
        });
    
        Ext.apply(this, {
            dockedItems: [
            {
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных',
                plugins     : [{ptype: 'pagesize'}]
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },
                    /*'-',
                    {
                        text   : 'Экспорт CSV',
                        iconCls: 'export-csv-ico',
                        scope  : this, 
                        handler: function() 
                        {
                            Ext.Msg.confirm('Подтверждение','Экспортировать ' + this.store.getCount() + ' строк?', function(btn){
                                if(btn=='yes')
                                {
                                    var data = Ext.ux.exporter.Exporter.exportGrid(this, 'csv');
                                    submitFakeForm('/index/export-data', data, 'Платежи 1С');
                                }
                            },this);   
                        }
                    },*/
                    '-',
                    {
                        text    : this. is_success ? 'Не обработанные' : 'Обработанные',
                        //iconCls : 'payment-ico',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'payment/order' + (this.is_success ? '' : '/by/success');
                        }
                    },
                    {
                        text     : 'Все',
                        itemId   : 'nx_isenable_state_btn',
                        width    : 100,
                        scope    : this,
                        nx_state : 0,
                        handler  : function(btn) 
                        {
                            btn.setState((btn.nx_state+1)%3);
                            this.store.reload();
                        },
                        setState : function(state)
                        {
                            var store = this.up('grid').getStore();
                            this.nx_state = state;
                            if(this.nx_state==0) {
                                this.setText('Все');
                                store.filters.removeAtKey('enabled');
                            }
                            if(this.nx_state==1) {
                                this.setText('Включенные');
                                store.filters.add('enabled', new Ext.util.Filter({
                                    property: 'enabled',
                                    value   : 1
                                }));
                            }
                            if(this.nx_state==2) {
                                this.setText('Выключенные');
                                store.filters.add('enabled', new Ext.util.Filter({
                                    property: 'enabled',
                                    value   : 0
                                }));
                            }
                        }
                    },
                    '->',
                    {
                        text    : 'Отправить',
                        iconCls : 'payment-ico',
                        scope   : this,
                        handler : function(button) {
                            Ext.Msg.confirm('Подтверждение','Отправить платежи в 1С (для успешных заказов проставить статус оплачено, дату, сумму оплаты и платеж. агента)?', function(btn){
                                if(btn=='yes')
                                {
                                    button.disable();
                                    Ext.Ajax.request({
                                        url     : '/payment/send-nds',
                                        method  : 'GET',
                                        scope   : this,
                                        success : function(response) {
                                            button.enable();
                                            this.getStore().reload();
                                            //Ext.Msg.alert('Внимание', 'Отправка завершена.');
                                            var response = Ext.JSON.decode(response.responseText);
                                            var data = [];
                                            for(var i in response.result)
                                            {
                                                data.push({
                                                    'id'     : i, 
                                                    'result' : response.result[i]
                                                });
                                            }
                                            if(data.length) {
                                                this.showSendDetails(data);
                                            }
                                            else {
                                                Ext.Msg.alert('Внимание', 'Нет платежей на отправку.');
                                            }
                                        }
                                    });
                                }
                            },this);  
                        }
                    }
                ]
            }],
        });

        this.addEvents('nx_switch_field','nx_open_order_edit_form');

        this.callParent(arguments);

        this.down('#nx_isenable_state_btn').setState(1);

        this.store.filters.add('label', new Ext.util.Filter({
            property: 'label',
            value   : 'ws1c_payment'
        }));

        if(this.is_success)
        {
            this.store.filters.add('status', new Ext.util.Filter({
                property: 'status_greater',
                value   : '0'
            }));
        }
        else
        {
            this.store.filters.removeAtKey('status');
        }
    },
    showSendDetails: function(data)
    {
        new Ext.window.Window({
            title     : 'Отправка платежей в 1С',
            itemId    : 'sendPaymentTo1CWindow',
            iconCls   : 'payment-ico',
            layout    : 'fit',
            border    : false,
            resizable : true,
            autoShow  : true, 
            items     : [
                {
                    xtype : 'grid',
                    width : 500,
                    height: 500,
                    autoShow: true,
                    store: new Ext.data.JsonStore({
                        proxy: 
                        {
                            type: 'memory',
                            reader: {
                                type: 'json',
                                root: 'items'
                            }
                        },
                        data         : {'items' : data},
                        pageSize     : 9999,
                        //remoteFilter : true,
                        autoLoad     : true,
                        fields       : [
                            {name: 'id'},
                            {name: 'result'}
                        ]
                    }),
                    columns    : [
                        //{xtype: 'rownumberer', width: 50},
                        {text: 'id платежа', dataIndex: 'id', width: 70},
                        {text: 'комментарий', dataIndex: 'result', flex: 1, tdCls: 'nx-multiline'},
                    ],
                    viewConfig : { 
                        enableTextSelection: true
                    }
                }
            ], 
            buttons : [
                {
                    text: 'Закрыть',
                    handler: function()
                    {
                        //Ext.ComponentQuery.query('#importXpressSubscriptionWindow')[0].close();
                        this.up('window').close();
                    }
                }
            ]
        });
    },
    onOrderIdClick: function(order_id) {
        this.fireEvent('nx_open_order_edit_form', this, order_id);
    }
});