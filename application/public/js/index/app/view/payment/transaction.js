/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.payment.transaction', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_payment_transaction',

    title: 'Платежи 1C - лог отправки онлайн платежей в 1С',

    store: 'payment',

    initComponent: function() {

        this.columns = [
            {text: 'ID',  dataIndex: 'id', width: 50, filter: {xtype: 'textfield'}},
            {text: 'Год', dataIndex: 'year', width: 50, filter: {xtype: 'textfield'}},
            {text: 'NX ID заказа',  dataIndex: 'order_id', width: 80, filter: {xtype: 'textfield'}},
            {text: 'ID транз.',  dataIndex: 'transaction_id', width: 60, filter: {xtype: 'textfield'}},
            {text: 'Орг.', dataIndex: 'payment_agent_name', width: 70, filter: {xtype: 'textfield'}},
            {text: 'Плательщик', dataIndex: 'contractor_name', width: 150, tdCls: 'nx-multiline', filter: {xtype: 'textfield'}},
            {text: '№ поступл.', dataIndex: 'docno', width: 70, filter: {xtype: 'textfield'}},
            {text: 'Дата поступления',  dataIndex: 'date', width: 130, filter: {xtype: 'textfield'}},
            {text: 'Сумма', dataIndex: 'sum', width: 80, filter: {xtype: 'textfield'}, renderer: function(v) { return v.toString().replace('.',','); }, 
                summaryType: function(rows) {
                    var sum = 0;
                    Ext.each(rows, function(row){
                        sum+=parseFloat(row.get('sum'));
                    });
                    return sum.toFixed(2);
                }
            },
            {
                text: 'Статус', dataIndex: 'status', maxWidth: 150, tdCls: 'nx-multiline', filter: {xtype: 'textfield'}, 
                renderer :function(value, metadata,record) { 
                    return value + (record.get('status_comment') ? ' - ' + record.get('status_comment') : '');
                }
            },
            /*{
                xtype        : 'actioncolumn',
                width        : 30,
                menuDisabled : true,
                items        : [{
                    icon    : '/images/extjs/icons/refresh.png',
                    tooltip : 'Сбросить статус',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex, item, e, rec) 
                    {
                        Ext.Msg.confirm('Подтверждение',(rec.get('status')==1 ? 'Платеж был успешно сохранен. Все равно с' : 'С') + 'бросить статус?', function(btn){
                            if(btn=='yes')
                            {
                                var id = rec.get('id');
                                Ext.Ajax.request({
                                    url     : '/payment/re',
                                    method  : 'POST',
                                    params  : {
                                        id : id
                                    },
                                    scope   : this,
                                    success : function(response) {
                                        var response = Ext.JSON.decode(response.responseText);
                                        //showDataAsTree(response.data);
                                        this.getStore().reload();
                                    }
                                });
                            }
                        },this);
                    },
                    getClass: function(value, meta, rec, rowindex)
                    {
                        if( rec.get('status') > 0 ) {
                            return 'nx-transparent';
                        }
                        if( rec.get('status') < 0 ) {
                            return '';
                        }
                        return 'x-hide-display';
                    }
                }]
            },*/
            {text: 'Дата создания', dataIndex: 'created',  width: 130, filter: {xtype: 'textfield'}},
            {text: 'Дата обновления', dataIndex: 'last_updated',  width: 130, filter: {xtype: 'textfield'}},
            {
                xtype        : 'actioncolumn',
                width        : 30,
                menuDisabled : true,
                items        : [{
                    icon: '/images/extjs/icons/watch.png',
                    tooltip: 'Показать тех. данные',
                    scope: this,
                    handler: function(grid, rowIndex, colIndex, item, e, rec) 
                    {
                        var id = rec.get('id');
                        Ext.Ajax.request({
                            url     : '/payment/get',
                            method  : 'POST',
                            params  : {
                                id : id
                            },
                            scope   : this,
                            success : function(response) {
                                var response = Ext.JSON.decode(response.responseText);
                                showDataAsTree(response.data);
                            }
                        });
                    }
                }]
            }
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
            stateful              : false, 
            ensureFilteredVisible : false,
            reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],      
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    return rec.get('status') == 1 ? 'nx-success-row' : (rec.get('status') != 0 ? 'nx-fail-row' : (rec.get('result') ? 'nx-row-warn' : ''));
                } 
            },
            allowDeselect: true,
            selModel: {
                mode: 'MULTI'
            },
            features: [{
                ftype: 'summary'
            }],
        });
    
        Ext.apply(this, {
            dockedItems: [
            {
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных',
                plugins     : [{ptype: 'pagesize'}]
            },
            {
                xtype: 'toolbar',
                items : [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },
                    /*'-',
                    {
                        text    : 'платежи',
                        scope   : this,
                        handler : function() {
                            window.location.hash = 'payment/order';
                        }
                    },*/
                    '->',
                    {
                        text    : 'Сбросить',
                        icon    : '/images/extjs/icons/refresh.png',
                        scope   : this,
                        handler : function(button) {
                            var selection = this.getSelectionModel().getSelection();  
                            if(selection.length)
                            {
                                var ids = [];
                                Ext.each(selection, function(row) {
                                    ids.push(row.get('id'));
                                });
                                Ext.Msg.confirm('Подтверждение','Сбросить статус отправки в 1С?', function(btn){
                                    if(btn=='yes')
                                    {
                                        button.setLoading(true);
                                        Ext.Ajax.request({
                                            url     : '/payment/re',
                                            method  : 'POST',
                                            scope   : this,
                                            params  : {
                                                id : ids.join(',')
                                            },
                                            success : function(response) {
                                                button.setLoading(false);
                                                this.getStore().reload();
                                            }
                                        });
                                    }
                                },this);  
                            }
                        }
                    },
                    {
                        text    : 'Отправить',
                        iconCls : 'payment-ico',
                        scope   : this,
                        handler : function(button) {
                            var selection = this.getSelectionModel().getSelection();  
                            if(selection.length)
                            {
                                var ids = [];
                                Ext.each(selection, function(row) {
                                    ids.push(row.get('id'));
                                });
                                Ext.Msg.confirm('Подтверждение','Отправить платежи в 1С?', function(btn){
                                    if(btn=='yes')
                                    {
                                        button.setLoading(true);
                                        Ext.Ajax.request({
                                            url     : '/payment/send-transaction',
                                            method  : 'POST',
                                            scope   : this,
                                            params  : {
                                                ids : ids.join(',')
                                            },
                                            success : function(response) {
                                                button.setLoading(false);
                                                this.getStore().reload();
                                            }
                                        });
                                    }
                                },this);  
                            }
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);

        this.store.filters.add('label', new Ext.util.Filter({
            property: 'label',
            value   : 'transaction_payment'
        }));
    }
});