/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.price.zone_country' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_price_zone_country',

    title: 'Зональная разбивка',

    initComponent: function() {
        
        Ext.apply(this, {     
            store: new Ext.data.Store({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/zone/zone-country',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields : [
                    {name: 'id'},
                    {name: 'zone_name'},
                    {name: 'country_name'},
                    //{name: 'item_type'},
                    //{name: 'customer_type_id'}
                ]
            })
        });

        this.columns = [
            {
                xtype:'actioncolumn',
                width:40,
                items: [{
                    icon    : '/images/extjs/icons/edit.png',
                    tooltip : 'Редактировать',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_zone_country_edit', this, rec);
                    }
                },{
                    icon    : '/images/extjs/icons/delete.png',
                    tooltip : 'Удалить',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_zone_country_delete', this, rec);
                    }
                }]
            },
            {text: 'id', dataIndex: 'id', width: 50},
            {text: 'Зона', dataIndex: 'zone_name', width: 100},
            {text: 'Страна', dataIndex: 'country_name', width: 150},
        ];

        Ext.apply(this, {
            viewConfig : { 
                enableTextSelection: true,
            } 
        });
    
        Ext.apply(this, { 
            dockedItems: [    
            {
                xtype: 'toolbar',
                items: [
                    {
                        text       : 'Цены',
                        href       : '/#price/index',
                        hrefTarget : '_self',
                    },
                    '-',
                    {
                        text       : 'Зональная разбивка',
                        href       : '/#price/zonecountry',
                        hrefTarget : '_self',
                    },
                    '-',
                    {
                        text       : 'Способы доставки',
                        href       : '/#price/zonedelivery',
                        hrefTarget : '_self',
                    },
                    '->',
                    {
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        handler : function() {
                            this.fireEvent('nx_zone_country_add', this);
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);
    }
});