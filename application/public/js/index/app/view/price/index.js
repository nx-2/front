/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.price.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_price_list',

    title: 'Таблицы цен',

    initComponent: function() {
        
        Ext.apply(this, {     
            store: new Ext.data.Store({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/price/index',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields : [
                    {name: 'id'},
                    {name: 'name'},
                    {name: 'date'},
                    {name: 'item_type'},
                    {name: 'customer_type_id'},
                    {name: 'checked'},
                    {name: 'active'}
                ]
            })
        });

        this.columns = [
            {
                xtype:'actioncolumn',
                width:20,
                items: [{
                    icon    : '/images/extjs/icons/edit.png',
                    tooltip : 'Редактировать',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_price_edit', this, rec);
                    }
                }]
            },
            {text: 'id', dataIndex: 'id', width: 50},
            {text: 'Дата', dataIndex: 'date', width: 100},
            {text: 'Тип клиента', dataIndex: 'customer_type_id', width: 100, renderer: function(val){
                if(val=='1') return 'физ. лицо'; 
                else if(val=='2') return 'юр. лицо'; 
                else return 'для всех'; 
            }},
            {text: 'Название', dataIndex: 'name', flex: 1},
        ];

        Ext.apply(this, {
            viewConfig : { 
                enableTextSelection: true,
                getRowClass: function(rec, rowIdx, params, store) {
                    var css_class = '';
                    css_class += rec.get('checked')=='0' ? ' nx-transparent' : '';
                    css_class += rec.get('active')=='1' ? ' nx-row-active' : '';
                    return css_class;
                }
            } 
        });
    
        Ext.apply(this, { 
            dockedItems: [    
            {
                xtype: 'toolbar',
                items: [
                    {
                        text       : 'Цены',
                        href       : '/#price/index',
                        hrefTarget : '_self',
                    },
                    '-',
                    {
                        text       : 'Зоны',
                        href       : '/#price/zone',
                        hrefTarget : '_self',
                        hidden     : !nx.app.isperm('admin'),
                    },
                    /*'-',
                    {
                        text       : 'Зональная разбивка',
                        href       : '/#price/zonecountry',
                        hrefTarget : '_self',
                        hidden     : !nx.app.isperm('admin'),
                    },
                    '-',
                    {
                        text       : 'Способы доставки',
                        href       : '/#price/zonedelivery',
                        hrefTarget : '_self',
                        hidden     : !nx.app.isperm('admin'),
                    },*/
                    '->',
                    {
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        handler : function() {
                            this.fireEvent('nx_price_add', this);
                        }
                    },
                ]
            }],
        });

        this.callParent(arguments);
    }
});