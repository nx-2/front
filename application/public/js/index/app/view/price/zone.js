/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.price.zone' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_price_zone',

    title: 'Зоны',

    initComponent: function() {
        
        Ext.apply(this, {     
            store: new Ext.data.Store({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/zone/index',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields : [
                    {name: 'id'},
                    {name: 'name'}
                ]
            })
        });

        this.columns = [
            {
                xtype:'actioncolumn',
                width:40,
                items: [{
                    icon    : '/images/extjs/icons/edit.png',
                    tooltip : 'Редактировать',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_zone_edit', this, rec);
                    }
                },{
                    icon    : '/images/extjs/icons/delete.png',
                    tooltip : 'Удалить',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_zone_delete', this, rec);
                    },
                    getClass: function(value, meta, rec, rowindex)
                    {
                        return nx.app.isperm('admin') ? '' : 'x-hide-display';
                    }
                }]
            },
            {text: 'id', dataIndex: 'id', width: 50},
            {text: 'Название', dataIndex: 'name', flex: 1},
        ];

        Ext.apply(this, {
            viewConfig : { 
                enableTextSelection: true,
            } 
        });
    
        Ext.apply(this, { 
            dockedItems: [    
            {
                xtype: 'toolbar',
                items: [
                    {
                        text       : 'Цены',
                        href       : '/#price/index',
                        hrefTarget : '_self',
                    },
                    '-',
                    {
                        text       : 'Зоны',
                        href       : '/#price/zone',
                        hrefTarget : '_self',
                        hidden     : !nx.app.isperm('admin'),
                    },
                    /*'-',
                    {
                        text       : 'Зональная разбивка',
                        href       : '/#price/zonecountry',
                        hrefTarget : '_self',
                        hidden     : !nx.app.isperm('admin'),
                    },
                    '-',
                    {
                        text       : 'Способы доставки',
                        href       : '/#price/zonedelivery',
                        hrefTarget : '_self',
                        hidden     : !nx.app.isperm('admin'),
                    },*/
                    '->',
                    {
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        hidden  : !nx.app.isperm('admin'),
                        handler : function() {
                            this.fireEvent('nx_zone_add', this);
                        }
                    },
                ]
            }],
        });

        this.callParent(arguments);
    }
});