/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.price.magazine' ,{
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_price_magazine',
    autoShow : true,
    title    : 'Таблица цен',
    width    : 900,
    //width       : Ext.dom.Element.getViewportWidth()  - 60,
    //height      : Ext.dom.Element.getViewportHeight() - 60,
    initComponent: function() 
    {
        if(this.mode=='edit') {
            this.title += ' #' + this.price_id;
        }
        var zones = this.nxdata.data.zones;

        var columns = [
            {text: 'Журнал', dataIndex: 'periodical_name', width: 100}
        ];

        var fields = [{name: 'periodical_id'}, {name: 'periodical_name'}];

        Ext.each(zones, function(zone){
            var incolumns = [];
            Ext.each(zone['delivery_type'], function(delivery_type){
                incolumns[incolumns.length] = { text: delivery_type.name, dataIndex: 'price_' + zone.zone_id + '_' + delivery_type.id, flex: 1, editor: {xtype: 'textfield'} };
                fields[fields.length] = {name: 'price_' + zone.zone_id + '_' + delivery_type.id};
            });
            columns[columns.length] = {text: zone.zone_name, columns: incolumns, flex: 1};
        });

        this.cellEditing = new Ext.grid.plugin.CellEditing({
            clicksToEdit: 1,
        });

        this.items = [
            {
                xtype     : 'form',
                bodyStyle : 'padding: 5px;',
                items     : [
                    {
                        xtype      : 'textfield',
                        name       : 'name',
                        fieldLabel : 'Название',
                        allowBlank : false,
                        value      : new Date().getFullYear() + ' текущий'
                    },
                    {
                        xtype      : 'datefield',
                        name       : 'date',
                        fieldLabel : 'Дата начала действия',
                        format     : 'Y-m-d',
                        allowBlank : false
                    },
                    {
                        xtype      : 'combobox',
                        store      : [['0', 'для всех'],['1', 'физ. лицо'],['2', 'юр. лицо']],
                        fieldLabel : 'Тип клиента',
                        name       : 'customer_type_id',
                        value      : '0',
                        editable   : false,
                    },
                    {
                        xtype          : 'checkbox',
                        name           : 'checked',
                        fieldLabel     : 'Включено?',
                        inputValue     : '1',
                        uncheckedValue : '0',
                        checked        : true 
                    }
                ],
                listeners: {
                    scope: this,
                    'beforerender': function(form)
                    {
                        form.getForm().setValues(this.nxdata.data.price);
                    }
                }
            },
            {
                xtype   : 'grid',
                columns : columns,
                store   : new Ext.data.JsonStore({
                    proxy: {
                        type: 'memory',
                        reader: {
                            type: 'json',
                            root: 'items'
                        }
                    },
                    data         : {'items' : this.nxdata.data.data},
                    remoteFilter : true,
                    fields       : fields,
                    autoLoad     : false
                }),
                plugins: [this.cellEditing],
            }
        ];


        this.buttons = [
            {
                text   : this.mode=='edit' ? 'Сохранить' : 'Добавить',
                action : this.mode=='edit' ? 'save' : 'add'
            },
            {
                text    : 'Закрыть',
                action  : 'close',
                scope   : this,
                handler : this.close
            }
        ];


        this.callParent(arguments);
    },
});