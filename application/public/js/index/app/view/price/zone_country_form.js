/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.price.zone_country_form', {
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_price_zone_country_form',
    title    : 'Привязка зона-страна',
    autoShow : true,
    width    : 700,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                bodyStyle: 'padding:5px;',
                items: 
                [
                    {
                        //xtype      : 'combo',
                        name       : 'zone_id',
                        fieldLabel : 'Зона',
                        editable   : false,
                        xtype      : 'nx.ux.JsonCombo', 
                        url        : '/zone/get-zones-for-combo',
                        allowBlank : false
                    },
                    {
                        xtype         : 'combo',
                        name          : 'country_id',
                        fieldLabel    : 'Страна',
                        store         : countries_store,
                        editable      : false,
                        queryMode     : 'local',
                        triggerAction : 'all',
                        value         : '165'
                    }          
                ]
            }
        ];

        this.buttons = [
            {
                text   : this.nxparams.id ? 'Сохранить' : 'Добавить',
                action : this.nxparams.id ? 'save' : 'add'
            },
            {
                text    : 'Закрыть',
                action  : 'close',
                scope   : this,
                handler : this.close
            }
        ];

        this.callParent(arguments);
    }
});