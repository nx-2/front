/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.tag.index' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.nx_tag_list',

    title: 'Теги',

    initComponent: function() {

        Ext.apply(this, {     
            store: new Ext.data.Store({
                proxy: {
                    type: 'ajax',
                    api: {
                        read: '/tag/index',
                    },
                    reader: {
                        type: 'json',
                        root: 'items',
                        successProperty: 'success',
                        totalProperty: 'total'
                    }
                },
                remoteFilter: true,
                fields : [
                    {name: 'id'},
                    {name: 'name'}
                ]
            })
        });

        this.columns = [
            {
                xtype:'actioncolumn',
                width:20,
                items: [{
                    icon    : '/images/extjs/icons/edit.png',
                    tooltip : 'Редактировать',
                    scope   : this,
                    handler : function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        this.fireEvent('nx_tag_edit', this, rec);
                    }
                }]
            },
            {text: 'id', dataIndex: 'id', width: 50, filter: {xtype: 'textfield'}},
            {text: 'Тег', dataIndex: 'name', flex: 1, filter: {xtype: 'textfield'}},
        ];

        var gridheaderfilters = new Ext.ux.grid.plugin.HeaderFilters({ 
                stateful              : false, 
                ensureFilteredVisible : false,
                reloadOnChange        : false
        });

        Ext.apply(this, {
            plugins: [gridheaderfilters],
            viewConfig : { 
                enableTextSelection: true,
            } 
        });
    
        Ext.apply(this, {     
            dockedItems: [{
                xtype       : 'pagingtoolbar',
                store       : this.store,
                dock        : 'bottom',
                displayInfo : true,
                displayMsg  : 'Показано {0} - {1} из {2}',
                emptyMsg    : 'Нет данных'
            },
            {
                xtype: 'toolbar',
                items: [
                    {
                        text    : 'Искать',
                        iconCls : 'search-ico',
                        scope   : this,
                        handler : function() {
                            this.applyHeaderFilters();
                        }
                    },
                    {
                        text    : 'Сброс поиска',
                        iconCls : 'clear-ico',
                        scope   : this,
                        handler : function() {
                            this.resetHeaderFilters();
                        }
                    },
                    '->',
                    {
                        text    : 'Добавить',
                        iconCls : 'add-ico',
                        scope   : this,
                        handler : function() {
                            this.fireEvent('nx_tag_add', this);
                        }
                    }
                ]
            }],
        });

        this.callParent(arguments);
    }
});