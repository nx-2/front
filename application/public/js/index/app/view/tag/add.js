/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.tag.add', {
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_tag_add',
    title    : 'Добавление тега',
    autoShow : true,
    width    : 700,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title   : 'свойства',
                                layout  : 'hbox',
                                padding : 5,
                                items   : 
                                [
                                    {
                                        layout : 'form',
                                        border : 0,
                                        flex   : 10,
                                        items  : [
                                            {
                                                xtype      : 'textfield',
                                                name       : 'name',
                                                fieldLabel : 'Название',
                                                allowBlank : false
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text   : 'Добавить',
                action : 'add'
            },
            {
                text    : 'Закрыть',
                action  : 'close',
                scope   : this,
                handler : this.close
            }
        ];

        this.callParent(arguments);
    }
});