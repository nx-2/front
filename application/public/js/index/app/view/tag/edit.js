/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * 
 */

Ext.define('nx.view.tag.edit', {
    extend   : 'Ext.window.Window',
    alias    : 'widget.nx_tag_edit',
    title    : 'Редактирование тега',
    autoShow : true,
    width    : 900,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                items: 
                [
                    {
                        xtype: 'tabpanel',
                        items: 
                        [
                            {
                                title   : 'свойства',
                                layout  : 'hbox',
                                padding : 5,
                                items   : 
                                [
                                    {
                                        layout : 'form',
                                        border : 0,
                                        flex   : 10,
                                        items  : [
                                            {
                                                xtype       : 'textfield',
                                                name        : 'id',
                                                fieldLabel  : 'ID',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'name',
                                                fieldLabel  : 'Тег',
                                            },
                                        ]
                                    },
                                    {
                                        layout   : 'form',
                                        border   : 0,
                                        flex     : 6,
                                        margins  : '0 0 0 25',
                                        defaults : {
                                            labelAlign: 'right'
                                        },
                                        items: [
                                            {
                                                xtype       : 'textfield',
                                                name        : 'create_user_name',
                                                fieldLabel  : 'завел',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'created',
                                                fieldLabel  : 'внесение',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_user_name',
                                                fieldLabel  : 'изменил',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            },
                                            {
                                                xtype       : 'textfield',
                                                name        : 'last_updated',
                                                fieldLabel  : 'правка',
                                                fieldStyle  : 'border: 0; background: none;',
                                                submitValue : false,
                                                readOnly    : true
                                            }
                                        ]
                                    }
                                ]
                            },
                        ]
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text   : 'Сохранить',
                action : 'save'
            },
            {
                text    : 'Закрыть',
                action  : 'close',
                scope   : this,
                handler : this.close
            }
        ];

        this.callParent(arguments);
    }
});