/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 *
 */

Ext.Loader.setConfig({ 
    enabled: true,
    paths: {
        //'nx'                        : '../index',
        //'Ext'                       : '../js/extjs/src',
        'Ext.ux'                      : '../js/extjs/examples/ux',
        'nx.ux'                       : '../js/index/app/ux',
        'Ext.ux.exporter'             : '../js/index/plugins/exporter',
        'Ext.ux.form.field.BoxSelect' : '../js/index/plugins/boxselect/BoxSelect.js',
        'Ext.rtl.EventObjectImpl'     : '../js/extjs/src/rtl/EventObject.js'//extjs4.2.1 bug no EventObjectImpl.js file
    }
});

/*Ext.Loader.require([
    'Ext.form.*',
    'Ext.History',
    'Ext.container.*',
    'Ext.layout.*',
    'Ext.grid.*',
    'Ext.tab.*',
    'Ext.data.*',
    'Ext.tree.*',
    'Ext.tip.*'
]);*/
/*Ext.Loader.require([
    //'Ext.*',
    'Ext.form.*',
    'Ext.Ajax.*'
]);*/

var nxApp;
//window.onload = function() {
Ext.onReady(function()
{

Ext.Ajax.timeout = 300000;
//console.log(nx.getViewportHeight());

Ext.apply(Ext.form.field.VTypes, {
    Numeric     :  function(v) {
        return /^[0-9\.]+$/.test(v);
    },
    NumericText : 'Должно быть числом',
    NumericMask : /[\d\.]/i,
    alpharu     :  function(v) {
        return /^[a-zа-яё]+$/i.test(v);
    },
    alpharuText : 'Только буквы',
    alpharuMask : /[a-zа-яё]/i,
    emails: function(v) {
        return /^[\w.\-@'"!#$%&'*+/=?^_`{|}~,; ]+$/i.test(v);
    },
    emailsText: 'Должно быть перечислением адресов электронной почты через ; или ,',
    emailsMask: /[\w.\-@'"!#$%&'*+/=?^_`{|}~,; ]/i
});


//nxApp = Ext.create('Ext.app.Application', {
Ext.application({
    requires: [
        //'Ext.*',
        /*'Ext.container.*',
        'Ext.layout.*',
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.tab.*',
        'Ext.toolbar.*',
        'Ext.tree.*',
        'Ext.util.*',*/
        //'Ext.ux.grid.FiltersFeature',
        'Ext.ux.exporter.Exporter',
        'Ext.ux.form.field.BoxSelect',
        //'Ext.ux.form.MultiSelect',
        'nx.ux.JsonCombo',
        'nx.ux.PageSize',
        'nx.ux.subscription_magazines_editor',
        'nx.ux.YearCombo',
        'nx.ux.view_state_switcher',
        'nx.ux.LinkButton',
        'nx.ux.grid_filters',
        'nx.ux.mailform',
        'nx.ux.notification_handler',
        'nx.ux.csv_to_db_editor'
    ],// 'Ext.tree.Panel'],
    name: 'nx',
    //appProperty: 'app',

    appFolder: '../js/index/app',

    controllers: [
        'user',
        'subscription',
        'periodical',
        'address',
        'company',
        'person',
        'log',
        'role',
        'transaction',
        'order',
        'maillog',
        'payment',
        'chart',
        'notification',
        'tag',
        'price'
    ],

    launch: function() {
        //Ext.QuickTips.init();
        nx.config = nx_config || {};
        nxApp = nx.app;
        Ext.History.init();
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [
                {
                    region  : 'north',
                    xtype   : 'panel',
                    //baseCls : 'x-plain',
                    tbar    : topMenu
                },
                {
                    id     : 'NXContentPanel', 
                    region : 'center',
                    layout : 'fit',
                    listeners: {
                        boxready:function()
                        {
                            page_size = Math.floor(this.getHeight()/23);
                        }
                    },
                    lbar: leftMenu
                }
            ]
        });

        var componentToken = Ext.History.getToken();

        ProcessLocation(this, componentToken);

        Ext.History.on('change', function(componentToken, opts) 
        {
            ProcessLocation(this, componentToken, opts);
        }, this);

        Ext.Ajax.on('requestcomplete', function(conn, response, options) 
        {
            var data = Ext.decode(response.responseText);
            if (data.redirect)
            {
                window.location = data.redirect;
            }
            else if (data.systemMessage) 
            {
                var data = data.systemMessage;
                infoReport(data.title, data.message, data.icon);
            }
        });

        //ROUTE TO CONTROLLER ACTION
        //this.initRoutes();
        //this.callParent(arguments);
    },
    initComponent: function(componentToken) 
    {
        var currentToken = Ext.History.getToken();
        if(currentToken == componentToken) 
        {
            ProcessLocation(this, componentToken);
        } else 
        {
            Ext.History.add(componentToken);
        }
    },
    isperm: function(role) {
        return (nx.config.perm['is_' + role] || nx.config.perm['is_admin'] || nx.config.perm['is_developer']) ? true : false;
    }
    /*,
    onLaunch: function(application)
    {
        console.log('onLaunch');
        Ext.History.on('change', function(componentToken) 
        {
            console.log(application, componentToken);
            ProcessLocation(application, componentToken);
        });
    }*/
});

});

