<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


chdir(dirname(__DIR__));
$appPath = getcwd();
$autoloaderFile = $appPath . '/init_autoloader.php';
$configsFile = $appPath . '/config/application.config.php';

require $autoloaderFile;

use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

$configuration = include $configsFile;

$serviceManager = new ServiceManager(new ServiceManagerConfig());
$serviceManager->setService('ApplicationConfig', $configuration);


// load modules -- which will provide services, configuration, and more
$serviceManager->get('ModuleManager')->loadModules();

$application = $serviceManager->get('Application');
$application->bootstrap();

$subscriptionService = $serviceManager->get('nx\Service\Subscription');

//reset new_state
//$subscriptionService->mapper->update(array('new_state' => 0), array('order_state_id <> ?' => 3, 'new_state' => 1));

$subscriptions = $subscriptionService->mapper->getSubscriptions(['order_state_id' => 3, 'new_state' => 0, 'limit' => 100, 'isset_email' => 1]);

$ids_new   = [];
$ids_nonew = [];//var_dump(array_column($subscriptions->toArray(),'id'));
foreach($subscriptions as $subscription) {
    $check = $subscriptionService->isNew($subscription->id);
	if($check == 1) {
		$ids_new[] = $subscription->id;
	}
	if($check == -1) {
		$ids_nonew[] = $subscription->id;
	}
}
if(!empty($ids_new)) {
	$subscriptionService->mapper->update(['new_state' => 1], ['id' => $ids_new]);
}
if(!empty($ids_nonew)) {
	$subscriptionService->mapper->update(['new_state' => -1], ['id' => $ids_nonew]);
}

