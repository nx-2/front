<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


chdir(dirname(__DIR__));
$appPath = getcwd();
$autoloaderFile = $appPath . '/init_autoloader.php';
$configsFile = $appPath . '/config/application.config.php';

require $autoloaderFile;

use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

$configuration = include $configsFile;

$serviceManager = new ServiceManager(new ServiceManagerConfig());
$serviceManager->setService('ApplicationConfig', $configuration);


// load modules -- which will provide services, configuration, and more
$serviceManager->get('ModuleManager')->loadModules();

$application = $serviceManager->get('Application');
$application->bootstrap();

//$transactionModel  = $serviceManager->get('nx\Model\Transaction');
$paymentModel      = $serviceManager->get('nx\Model\Payment');
//$subscriptionModel = $serviceManager->get('nx\Model\Subscription');

$paymentModel->sendTransactionTo1c();
