<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


chdir(dirname(__DIR__));
$appPath = getcwd();
$autoloaderFile = $appPath . '/init_autoloader.php';
$configsFile = $appPath . '/config/application.config.php';

require $autoloaderFile;

use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

$configuration = include $configsFile;

$serviceManager = new ServiceManager(new ServiceManagerConfig());
$serviceManager->setService('ApplicationConfig', $configuration);

$serviceManager->get('ModuleManager')->loadModules();

$application = $serviceManager->get('Application');
$application->bootstrap();

$subscriptionModel      = $serviceManager->get('nx\Model\Subscription');

$cur_time = time();
$item_updated_end = date('Y-m-d', mktime(0, 0, 0, date('n', $cur_time), date('j', $cur_time)-14, date('Y', $cur_time)));

$subscriptions = $subscriptionModel->getSubscriptions(['is_present' => 1, 'item_updated_end' => $item_updated_end, 'limit' => 10000]);
$k = 0;
if($subscriptions->count()) {
	foreach($subscriptions as $subscription) {
		$subscriptionModel->itemReleaseModel->updateItemsIssues($subscription->id);
		$k++;
	}
}
die('subscription_fill_dates END. ' . $k . 'subscriptions proccesed.');

