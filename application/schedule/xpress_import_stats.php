<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


chdir(dirname(__DIR__));
require '/var/www/nx.osp.ru/htdocs/application/init_autoloader.php';

use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

$configuration = include '/var/www/nx.osp.ru/htdocs/application/config/application.config.php';

$serviceManager = new ServiceManager(new ServiceManagerConfig());
$serviceManager->setService('ApplicationConfig', $configuration);

$serviceManager->get('ModuleManager')->loadModules();
$application = $serviceManager->get('Application');
$application->bootstrap();
$subscriptionModel = $serviceManager->get('nx\Model\Subscription');

$subscriptionModel->updateXpressImportStats();
