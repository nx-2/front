<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


chdir(dirname(__DIR__));
$appPath = getcwd();
$autoloaderFile = $appPath . '/init_autoloader.php';
$configsFile = $appPath . '/config/application.config.php';

require $autoloaderFile;

use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

$configuration = include $configsFile;

$serviceManager = new ServiceManager(new ServiceManagerConfig());
$serviceManager->setService('ApplicationConfig', $configuration);

$serviceManager->get('ModuleManager')->loadModules();

$application = $serviceManager->get('Application');
$application->bootstrap();

$transactionModel = $serviceManager->get('nx\Model\Transaction');
$paymentModel = $serviceManager->get('nx\Model\Payment');
$orderModel = $serviceManager->get('nx\Model\Order');

$tr_id = !empty($argv[1]) ? (int)$argv[1] : 0;
$by = ['sys_status' => 2, 'date_start' => '2018-01-01', 'isset_sum' => 1];
if (!empty($tr_id)) {
    $by['id'] = $tr_id;
} else {
    $by['period_days'] = 7;
}

$transactions = $transactionModel->getTransactions($by);//,'shopid' => 4, 'system' => 2));//yandex
$transactions = $transactions->toArray();

foreach ($transactions as $transaction) {
    $check = $paymentModel->getBy(['transaction_id' => $transaction['id']]);
    if (!empty($check)) {
        continue;
    }
    if (empty($transaction['order_id'])) {
        continue;
    }
    $order = $orderModel->getOrderBy([
        'columns' => ['id', 'company_id', 'payment_docno'],
        'id' => $transaction['order_id']
    ]);
    if (empty($order)) {
        continue;
    }
    $publisher = $paymentModel->orderModel->getOwner($order->id);
    $publisherId = ($publisher && !empty($publisher) && isset($publisher['id'])) ? $publisher['id'] : false;

    $data['created'] = date('Y-m-d H:i:s');
    $data['date'] = $transaction['date'];
    $data['year'] = date('Y', strtotime($transaction['date']));
    $data['sum'] = $transaction['sum'];
    $data['transaction_id'] = $transaction['id'];
    $data['payment_agent_id'] = 47;//ООО ОС
    $data['label'] = 'transaction_payment';
    $data['order_id'] = $order->id;
    //$data['contractor_id']    = $order->company_id;//0 если физ. лицо
    $data['docno'] = $order->payment_docno;

    if ($publisherId)
        $data['publisher_id'] = $publisherId;

    $paymentModel->insert($data);
}
