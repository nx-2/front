<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


// очистка таблицы nx_log от записей старше чем N месяцев
chdir(dirname(__DIR__));
$appPath = getcwd();
$autoloaderFile = $appPath . '/init_autoloader.php';
$configsFile = $appPath . '/config/application.config.php';

require $autoloaderFile;

use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

$configuration = include $configsFile;

$serviceManager = new ServiceManager(new ServiceManagerConfig());
$serviceManager->setService('ApplicationConfig', $configuration);


// load modules -- which will provide services, configuration, and more
$serviceManager->get('ModuleManager')->loadModules();

$application = $serviceManager->get('Application');
$application->bootstrap();

$logModel = $serviceManager->get('nx\Model\Log');
// оставить только последние 6 месяцев
$date = date("Y-m-d", mktime(0, 0, 0, date('n')-8, date('j'), date('Y')));

$logModel->delete([new Expression('created < "' . $date . '"')]);
