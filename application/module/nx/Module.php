<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx;

use nx\Model\Address;
use nx\Model\Comment;
use nx\Model\FileStorage;
use nx\Model\Issue;
use nx\Model\IssueOrder;
use nx\Model\Log;
use nx\Model\MailLog;
use nx\Model\MailTemplates;
use nx\Model\Notification;
use nx\Model\OrderItem;
use nx\Model\Payment;
use nx\Model\Person;
use nx\Model\Price;
use nx\Model\Publisher;
use nx\Model\Role;
use nx\Model\Session;
use nx\Model\Shipment;
use nx\Model\SubscriptionAction;
use nx\Model\SubscriptionPdfLog;
use nx\Model\Tag;
use nx\Model\TagEntity;
use nx\Model\Transaction;
use nx\Model\User;
use nx\Model\UserRole;
use nx\Model\ViewState;
use nx\Model\XpressSubscription;
use nx\Model\Zone;
use nx\Service\Action;
use nx\Service\Chart;
use nx\Service\Company;
use nx\Service\Mailing;
use nx\Service\Order;
use nx\Service\Passport;
use nx\Service\PublishAdmin;
use nx\Service\Subscription;
use nx\Service\SubscriptionItem;
use nx\Service\SubscriptionItemRelease;
use nx\Service\Ws1c;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\Adapter\MemcachedOptions;
use Zend\EventManager\Event;
use Zend\EventManager\EventManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

//use Zend\ModuleManager\ModuleManager;
use Zend\Permissions\Rbac\Rbac;
use Zend\Session\SessionManager;
use Zend\Session\Config\SessionConfig;
use Zend\Session\Container;
use Zend\Session\Storage\ArrayStorage;

const NDS_SERVICE = 18;
const NDS_PRODUCT = 10;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        require_once(__DIR__ . '/src/nx/functions.php');
        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $eventManager = $application->getEventManager();


        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);


        $session = $sm->get('Zend\Session\SessionManager');
        $session->start();

        $sharedManager = $application->getEventManager()->getSharedManager();
        $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function ($e) use ($sm) {
            $sm->get('ControllerPluginManager')->get('AuthPlugin')->doAuthorization($e);
        }, 2 //priority
        );

        $eventManager->attach('render', function ($e) {
            $e->getViewModel()->setVariable('e', $e);
        });

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig(): array
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getServiceConfig(): array
    {
        return [
            'factories' => [
                'Memcached' => function ($serviceManager) {
                    $memcached = new Memcached(new MemcachedOptions([
                        'ttl' => 60,
                        'namespace' => 'nx_memcache_listener',
                        'key_pattern' => null,
                        'readable' => true,
                        'writable' => true,
                        'servers' => 'memcached',
                    ]));
                    return $memcached;
                },
                'Zend\Session\SessionManager' => function ($sm) {
                    $config = $sm->get('config');
                    $sessionStorage = new ArrayStorage();//default SessionArrayStorage has conflicted with Netcat sessions
                    $sessionConfig = new SessionConfig();
                    $sessionConfig->setOptions($config['session']);
                    $sessionManager = new SessionManager($sessionConfig, $sessionStorage);
                    Container::setDefaultManager($sessionManager);
                    return $sessionManager;
                },
                'nx\Model\SubscriptionItem' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Model\SubscriptionItem($dbAdapter);
                    $table->personModel = new Person($dbAdapter);
                    $table->companyModel = new Model\Company($dbAdapter);
                    $table->subscriptionModel = new Model\Subscription($dbAdapter);
                    return $table;
                },
                'nx\Model\SubscriptionItemRelease' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Model\SubscriptionItemRelease($dbAdapter);
                    //$table->subscriptionModel       = new \nx\Model\Subscription($dbAdapter);
                    $table->subscriptionService = $sm->get('nx\Service\Subscription');
                    $table->fileStorageModel = $sm->get('nx\Model\FileStorage');
                    $table->subscriptionPdfLogModel = new SubscriptionPdfLog($dbAdapter);
                    return $table;
                },
                'nx\Model\Subscription' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Model\Subscription($dbAdapter);
                    $table->addressModel = $sm->get('nx\Model\Address');
                    $table->personModel = $sm->get('nx\Model\Person');
                    $table->companyModel = $sm->get('nx\Model\Company');
                    $table->itemModel = $sm->get('nx\Model\SubscriptionItem');
                    $table->actionService = $sm->get('nx\Service\Action');
                    $xdb = $sm->get('nx\xpress\Adapter');
                    $table->xpressModel = new XpressSubscription($xdb);
                    $table->fileStorageModel = $sm->get('nx\Model\FileStorage');
                    $table->itemReleaseModel = new Model\SubscriptionItemRelease($dbAdapter);
                    $table->ws1c_client = $sm->get('nx\ws1c_client');
                    $table->mailTemplatesModel = $sm->get('nx\Model\MailTemplates');
                    $table->aecms_adapter = $sm->get('nx\aecms\Adapter');
                    $table->publishAdminService = $sm->get('nx\Service\PublishAdmin');

                    //$table->passportService    = $sm->get('nx\Service\Passport');
                    $table->tagEntityModel = $sm->get('nx\Model\TagEntity');
                    $table->priceModel = $sm->get('nx\Model\Price');
                    $table->getEvents()->attach(\nx\Model\Subscription::EVENT_HAS_BECOME_ACTIVE, function (Event $event) {
                        $orderId = $event->getParam('orderId', NULL);
                        if (empty($orderId)) {
                            return;
                        }
                        /** @var  \nx\Model\Subscription $ownerModel */
                        $ownerModel = $event->getTarget();
                        try {
                            $url = '/guarded/order/become-active/' . $orderId ;
                            $response = $ownerModel->publishAdminService->sendRequest($url, 'GET');
                        } catch (\Exception $e) {
                            return;
                        }
                    });
                    return $table;
                },
                'nx\Model\User' => function ($sm) {
                    $dbAdapter = $sm->get('nx\osp\Adapter');
                    $table = new User($dbAdapter);
                    $table->roleModel = new Role($dbAdapter);
                    $table->userRoleModel = new UserRole($dbAdapter);
                    return $table;
                },
                'nx\Model\Session' => function ($sm) {
                    $dbAdapter = $sm->get('nx\osp\Adapter');
                    $table = new Session($dbAdapter);
                    return $table;
                },
                /*'nx\Model\Periodical' =>  function($sm)
                {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table     = new \nx\Model\Periodical($dbAdapter);
                    return $table;
                },*/
                'nx\Model\Address' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Address($dbAdapter);
                    return $table;
                },
                'nx\Model\Company' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Model\Company($dbAdapter);
                    $table->addressModel = $sm->get('nx\Model\Address');
                    $table->personModel = $sm->get('nx\Model\Person');
                    $table->ws1c_client = $sm->get('nx\ws1c_client');
                    return $table;
                },
                'nx\Model\Person' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Person($dbAdapter);
                    $table->addressModel = $sm->get('nx\Model\Address');
                    return $table;
                },
                'nx\Model\Log' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Log($dbAdapter);
                    $table->userModel = new User($dbAdapter);
                    return $table;
                },
                'nx\Model\MailTemplates' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $config = $sm->get('config');
                    $table = new MailTemplates($dbAdapter, ['mail_service' => $config['nx']['mail_service']]);
                    return $table;
                },
                'nx\Model\Role' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Role($dbAdapter);
                    return $table;
                },
                'nx\Perm' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $user_session = new Container('user');
                    $rbac = new Rbac();
                    $user_model = new User($dbAdapter);
                    $user_roles = $user_model->getRoles(['user_id' => $user_session->user_id]);
                    if (!empty($user_roles)) {
                        foreach ($user_roles as $role) {
                            $rbac->addRole($role['code']);
                        }
                    }
                    return $rbac;
                },
                'nx\Model\Transaction' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Transaction($dbAdapter);
                    return $table;
                },
                'nx\Model\IssueOrder' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new IssueOrder($dbAdapter);
                    $table->itemModel = new OrderItem($dbAdapter);
                    $table->personModel = $sm->get('nx\Model\Person');
                    $table->subscriptionModel = new Model\Subscription($dbAdapter);
                    $table->priceModel = $sm->get('nx\Model\Price');
                    return $table;
                },
                'nx\Model\MailLog' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    //$isArchive = $sm->get('Request')->getQuery('from', '');
                    $table = new MailLog($dbAdapter);
                    return $table;
                },
                'nx\Model\Payment' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Payment($dbAdapter);
                    $table->companyModel = $sm->get('nx\Model\Company');
                    $table->publisherModel = $sm->get('nx\Model\Publisher');
                    $table->orderModel = $sm->get('nx\Model\Order');
                    $table->subscriptionService = $sm->get('nx\Service\Subscription');
                    $table->transactionModel = $sm->get('nx\Model\Transaction');
                    $table->ws1c_client = $sm->get('nx\ws1c_client');
                    $table->orderService = $sm->get('nx\Service\Order');
                    $table->companyService = $sm->get('nx\Service\Company');
                    return $table;
                },
                'nx\Model\FileStorage' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new FileStorage($dbAdapter);
                    return $table;
                },
                'nx\Model\ViewState' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new ViewState($dbAdapter);
                    return $table;
                },
                'nx\Model\SubscriptionAction' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new SubscriptionAction($dbAdapter);
                    return $table;
                },
                'nx\Model\SubscriptionPdfLog' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new SubscriptionPdfLog($dbAdapter);
                    return $table;
                },
                'nx\Model\Order' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Model\Order($dbAdapter);
                    $table->itemModel = $sm->get('nx\Model\OrderItem');
                    $table->fileStorageModel = new FileStorage($dbAdapter);
                    $table->publisherModel = $sm->get('nx\Model\Publisher');
                    return $table;
                },
                'nx\Model\Issue' => function ($sm) {
                    $config = $sm->get('config');
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Issue($dbAdapter);
                    //$table->nx_api_url = 'http://' . $config['nx']['http_host'] . '/index/api';
                    return $table;
                },
                'nx\Model\OrderItem' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new OrderItem($dbAdapter);
                    $table->issueModel = $sm->get('nx\Model\Issue');
                    $table->priceModel = $sm->get('nx\Model\Price');
                    return $table;
                },
                'nx\Model\Notification' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Notification($dbAdapter);
                    return $table;
                },
                'nx\Model\Tag' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Tag($dbAdapter);
                    return $table;
                },
                'nx\Model\Mailing' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Model\Mailing($dbAdapter);
                    return $table;
                },
                'nx\Model\TagEntity' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new TagEntity($dbAdapter);
                    return $table;
                },
                'nx\Model\Shipment' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Shipment($dbAdapter);
                    return $table;
                },
                'nx\Model\Comment' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Comment($dbAdapter);
                    return $table;
                },
                'nx\Model\Price' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Price($dbAdapter);
                    $table->zoneModel = new Zone($dbAdapter);
                    return $table;
                },
                'nx\Model\Publisher' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Publisher($dbAdapter);
                    return $table;
                },
                'nx\Model\Zone' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $table = new Zone($dbAdapter);
                    return $table;
                },
                'nx\Service\Subscription' => function ($sm) {
                    $mapper = $sm->get('nx\Model\Subscription');
                    $service = new Subscription($mapper);
                    $service->renderer = $sm->get('Zend\View\Renderer\PhpRenderer');
                    $service->passportService = $sm->get('nx\Service\Passport');
                    return $service;
                },
                'nx\Service\SubscriptionItemRelease' => function ($sm) {
                    $mapper = $sm->get('nx\Model\SubscriptionItemRelease');
                    $service = new SubscriptionItemRelease($mapper);
                    $service->subscriptionMapper = $sm->get('nx\Model\Subscription');
                    $service->orderService = $sm->get('nx\Service\Order');
                    return $service;
                },
                'nx\Service\Action' => function ($sm) {
                    $mapper = $sm->get('nx\Model\SubscriptionAction');
                    $service = new Action($mapper);
                    return $service;
                },
                'nx\Service\Order' => function ($sm) {
                    $mapper = $sm->get('nx\Model\Order');
                    $service = new Order($mapper);
                    $service->ws1c_client = $sm->get('nx\ws1c_client');
                    $service->subscriptionMapper = $sm->get('nx\Model\Subscription');
                    $service->mailTemplatesModel = $sm->get('nx\Model\MailTemplates');
                    $service->issueOrderMapper = $sm->get('nx\Model\IssueOrder');
                    $service->publisherModel = $sm->get('nx\Model\Publisher');
                    return $service;
                },
                'nx\Service\SubscriptionItem' => function ($sm) {
                    $mapper = $sm->get('nx\Model\SubscriptionItem');
                    $service = new SubscriptionItem($mapper);
                    $service->subscriptionMapper = $sm->get('nx\Model\Subscription');
                    $service->orderService = $sm->get('nx\Service\Order');
                    return $service;
                },
                'nx\Service\Chart' => function ($sm) {
                    $dbAdapter = $sm->get('nx\Db\Adapter');
                    $mapper = new Model\Chart($dbAdapter);
                    return new Chart($mapper);
                },
                'nx\Service\Passport' => function ($sm) {
                    $config = $sm->get('config');
                    return new Passport($config['nx']['passport_service']);
                },
                'nx\Service\Company' => function ($sm) {
                    $mapper = $sm->get('nx\Model\Company');
                    $service = new Company($mapper);
                    $service->ws1c_client = $sm->get('nx\ws1c_client');
                    return $service;
                },
                'nx\Service\Mailing' => function ($sm) {
                    $mapper = $sm->get('nx\Model\Mailing');
                    return new Mailing($mapper);
                },
                'nx\Service\Ws1c' => function ($sm) {
                    return new Ws1c();
                },
                'nx\Service\PublishAdmin' => function ($sm) {
                    $config = $sm->get('config');
                    $session = new \Zend\Session\Container('user');
                    if ($session->offsetExists('user')) {
                        $userData = $session->offsetGet('user');
                    } else {
                        $userData = [];
                    }
                    /** @var \Zend\Db\Adapter\Adapter $dbAdapter */
                    $dbAdapter = $sm->get('nx\Db\Adapter');

                    return new PublishAdmin($config['nx'], $userData, $dbAdapter);
                },
            ],
        ];
    }
}
