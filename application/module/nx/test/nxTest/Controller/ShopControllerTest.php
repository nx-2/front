<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nxTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class ShopControllerTest extends AbstractHttpControllerTestCase
{
	protected $traceError = true;
	public function setUp()
    {
        $this->setApplicationConfig(
            include '/var/www/nx.osp.ru/htdocs/application/config/application.config.php'
        );
        parent::setUp();
    }

    public function testIndexActionCanBeAccessed()
	{
		$orderTableMock = $this->getMockBuilder('nx\Model\IssueOrder')->disableOriginalConstructor()->getMock();

	    $orderTableMock->expects($this->once())->method('getList')->will($this->returnValue(['items' => [], 'total' => 0]));

	    $authMock = $this->getMock('nx\Controller\Plugin\AuthPlugin');
	    $authMock->expects($this->any())->method('doAuthorization')->will($this->returnValue(true));

	    $sm = $this->getApplicationServiceLocator();
	    $sm->setAllowOverride(true);
	    $sm->setService('nx\Model\IssueOrder', $orderTableMock);
		$sm->get('ControllerPluginManager')->setService('AuthPlugin', $authMock);

	    $this->dispatch('/order');
	    $this->assertResponseStatusCode(200);

	    $this->assertModuleName('nx');
	    $this->assertControllerName('nx\Controller\Order');
	    $this->assertControllerClass('OrderController');
	    $this->assertMatchedRouteName('*');
	}
}
