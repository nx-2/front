<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


use function nx\functions\applyFunctionToData;

class api
{
    private $sm;

    function __construct($sm)
    {
        $this->sm = $sm;
    }

    public function company_add(array $params)
    {
        $params = applyFunctionToData($params, 'strip_tags');
        $params = applyFunctionToData($params, 'trim');

        $companyData = $params['companyData'];
        $addressData = !empty($params['addressData']) ? $params['addressData'] : [];
        $legalAddressData = !empty($params['legalAddressData']) ? $params['legalAddressData'] : [];
        $personData = !empty($params['personData']) ? $params['personData'] : [];
        $sm = $this->sm;
        $dbModel = $sm->get('nx\Model\Company');
        $dbModel->adapter->isTransactional = 0;
        $dbModel->adapter->getDriver()->getConnection()->beginTransaction();
        try {
            if (!empty($personData)) {
                $check = [];
                if (!empty($personData['email']) && !empty($personData['name'])) {
                    $check = $dbModel->personModel->getBy(['name' => $personData['name'], 'email' => $personData['email']]);
                    if (!empty($check)) {//person already exists
                        $companyData['contact_person_id'] = $check->id;
                        $dbModel->personModel->updatePerson($companyData['contact_person_id'], ['personData' => $personData]);
                    }
                }
                if (empty($check)) {
                    $personData['label'] = 'company';
                    $companyData['contact_person_id'] = $dbModel->personModel->addPerson(['personData' => $personData]);
                }
            }
            if (empty($companyData['label'])) {
                $companyData['label'] = 'api_company';
            }
            $result = $dbModel->addCompany(['companyData' => $companyData, 'addressData' => $addressData, 'legalAddressData' => $legalAddressData]);
            $dbModel->adapter->getDriver()->getConnection()->commit();
            return $result;
        } catch (Exception $e) {
            $dbModel->adapter->getDriver()->getConnection()->rollback();
            var_dump($e->getMessage());
            return false;
        }
    }

    public function company_update(array $params)
    {
        $params = applyFunctionToData($params, 'strip_tags');
        $params = applyFunctionToData($params, 'trim');

        $companyID = (int)$params['id'];
        $companyData = !empty($params['companyData']) ? $params['companyData'] : [];
        $addressData = !empty($params['addressData']) ? $params['addressData'] : [];
        $legalAddressData = !empty($params['legalAddressData']) ? $params['legalAddressData'] : [];
        $personData = !empty($params['personData']) ? $params['personData'] : [];
        $sm = $this->sm;
        $dbModel = $sm->get('nx\Model\Company');
        $company = $dbModel->getByID($companyID);
        if (!empty($company)) {
            $dbModel->adapter->isTransactional = 0;
            $dbModel->adapter->getDriver()->getConnection()->beginTransaction();
            try {
                if (!empty($personData)) {
                    if (!empty($company->contact_person_id)) {
                        $dbModel->personModel->updatePerson($company->contact_person_id, ['personData' => $personData, 'addressData' => $addressData]);
                    } else {
                        $check = [];
                        if (!empty($personData['email']) && !empty($personData['name'])) {
                            $check = $dbModel->personModel->getBy(['name' => $personData['name'], 'email' => $personData['email']]);
                            if (!empty($check)) {//person already exists
                                $companyData['contact_person_id'] = $check->id;
                                $dbModel->personModel->updatePerson($companyData['contact_person_id'], ['personData' => $personData]);
                            }
                        }
                        if (empty($check)) {
                            $personData['label'] = 'company';
                            $companyData['contact_person_id'] = $dbModel->personModel->addPerson(['personData' => $personData, 'addressData' => $addressData]);
                        }
                    }
                }
                $result = $dbModel->updateCompany($companyID, ['companyData' => $companyData, 'addressData' => $addressData, 'legalAddressData' => $legalAddressData]);
                $dbModel->adapter->getDriver()->getConnection()->commit();
                return $result;
            } catch (Exception $e) {
                $dbModel->adapter->getDriver()->getConnection()->rollback();
                var_dump($e->getMessage());
                return false;
            }
        }
        return false;
    }

    public function company_get_by_id(array $params)
    {
        $id = (int)$params['id'];
        $sm = $this->sm;
        $dbModel = $sm->get('nx\Model\Company');
        $result = $dbModel->getByID($id);
        return $result;
    }

    public function company_list(array $params)
    {
        $sm = $this->sm;
        $dbModel = $sm->get('nx\Model\Company');
        $start = !empty($params['start']) ? (int)$params['start'] : 0;
        //$filters = !empty($params['filters']) ? $params['filters'] : array();
        $result = $dbModel->getList($start, (int)$params['limit'], $params);
        $result['items'] = $result['items']->toArray();
        return $result;
    }

    public function company_list_by(array $params)
    {
        $sm = $this->sm;
        $dbModel = $sm->get('nx\Model\Company');
        $params['as_array'] = true;
        $result = $dbModel->getCompaniesBy($params);
        return $result;
    }

    public function order_add(array $params)
    {
        $orderData = $params['orderData'];
        $itemsData = $params['itemsData'];
        $personData = $params['personData'];

        $sm = $this->sm;
        $dbModel = $sm->get('nx\Model\IssueOrder');

        if (empty($personData) || empty($orderData) || empty($itemsData)) {
            return false;
        }

        $dbModel->adapter->isTransactional = 0;
        $dbModel->adapter->getDriver()->getConnection()->beginTransaction();
        try {
            $check = [];
            $person_id = 0;
            if (!empty($personData['name'])) {
                $personData['name'] = trim(preg_replace('#\b(null)\b#isu', '', $personData['name']));
            }
            if (!empty($personData['email']) && !empty($personData['name'])) {
                $check = $dbModel->personModel->getBy(['name' => $personData['name'], 'email' => $personData['email']]);
                if (!empty($check)) {//person already exists
                    $person_id = $check->id;
                    $dbModel->personModel->updatePerson($person_id, ['personData' => $personData]);
                }
            }
            if (empty($check)) {
                $personData['label'] = 'order';
                $person_id = $dbModel->personModel->addPerson(['personData' => $personData]);
            }
            if (!$person_id) {
                return false;
            }
            $orderData['person_id'] = $person_id;
            if (empty($orderData['label'])) {
                $orderData['label'] = 'api_order';
            }

            $result = $dbModel->addOrder(['orderData' => $orderData, 'itemsData' => $itemsData]);
            $dbModel->adapter->getDriver()->getConnection()->commit();
            return $result;
        } catch (Exception $e) {
            $dbModel->adapter->getDriver()->getConnection()->rollback();
            var_dump($e->getMessage());
            return false;
        }
    }

    public function order_update(array $params)
    {
        $orderID = (int)$params['id'];
        $orderData = !empty($params['orderData']) ? $params['orderData'] : [];
        $itemsData = !empty($params['itemsData']) ? $params['itemsData'] : [];
        $personData = !empty($params['personData']) ? $params['personData'] : [];
        $sm = $this->sm;
        $dbModel = $sm->get('nx\Model\IssueOrder');
        $order = $dbModel->getByID($orderID);
        if (!empty($order)) {
            $dbModel->adapter->isTransactional = 0;
            $dbModel->adapter->getDriver()->getConnection()->beginTransaction();
            try {
                if (!empty($personData)) {
                    if (!empty($personData['name'])) {
                        $personData['name'] = trim(preg_replace('#\b(null)\b#isu', '', $personData['name']));
                    }
                    if (!empty($order['person_id'])) {
                        $dbModel->personModel->updatePerson($order['person_id'], ['personData' => $personData]);
                    } else {
                        $check = [];
                        $person_id = 0;
                        if (!empty($personData['email']) && !empty($personData['name'])) {
                            $check = $dbModel->personModel->getBy(['name' => $personData['name'], 'email' => $personData['email']]);
                            if (!empty($check)) {//person already exists
                                $person_id = $check->id;
                                $dbModel->personModel->updatePerson($person_id, ['personData' => $personData]);
                            }
                        }
                        if (empty($check)) {
                            $personData['label'] = 'order';
                            $person_id = $dbModel->personModel->addPerson(['personData' => $personData]);
                        }
                        if (!$person_id) {
                            return false;
                        }
                        $orderData['person_id'] = $person_id;
                    }
                }
                $result = $dbModel->updateOrder($orderID, ['orderData' => $orderData, 'itemsData' => $itemsData]);
                $dbModel->adapter->getDriver()->getConnection()->commit();
                return $result;
            } catch (Exception $e) {
                $dbModel->adapter->getDriver()->getConnection()->rollback();
                var_dump($e->getMessage());
                return false;
            }
        }
        return false;
    }

    public function subscription_add(array $params)
    {
        $subscriptionData = $params['subscriptionData']['subscriptionData'];
        $itemsData = $params['subscriptionData']['itemsData'];
        $addressData = !empty($params['subscriptionData']['addressData']) ? $params['subscriptionData']['addressData'] : [];
        $mapAddressData = !empty($params['subscriptionData']['mapAddressData']) ? $params['subscriptionData']['mapAddressData'] : [];
        $personData = !empty($params['personData']['personData']) ? $params['personData']['personData'] : [];
        $personAddressData = !empty($params['personData']['addressData']) ? $params['personData']['addressData'] : [];
        $companyData = !empty($params['companyData']['companyData']) ? $params['companyData']['companyData'] : [];
        $companyAddressData = !empty($params['companyData']['addressData']) ? $params['companyData']['addressData'] : [];
        $companyLegalAddressData = !empty($params['companyData']['legalAddressData']) ? $params['companyData']['legalAddressData'] : [];
        $orderData = !empty($params['orderData']) ? $params['orderData'] : [];
        $tagsData = !empty($params['subscriptionData']['tagsData']) ? $params['subscriptionData']['tagsData'] : [];

        $sm = $this->sm;
        $dbModel = $sm->get('nx\Model\Subscription');

        if ((empty($personData) && empty($companyData)) || empty($subscriptionData) || empty($itemsData)) {
            return false;
        }

        $dbModel->adapter->isTransactional = 0;
        $dbModel->adapter->getDriver()->getConnection()->beginTransaction();
        try {
            $check = [];
            $person_id = null;
            if (!empty($personData['name'])) {
                $personData['name'] = trim(preg_replace('#\b(null)\b#isu', '', $personData['name']));
            }
            if (!empty($personData['email']) && !empty($personData['name'])) {
                $check = $dbModel->personModel->getBy(['name' => $personData['name'], 'email' => $personData['email']]);
                if (!empty($check)) {
                    $person_id = $check->id;
                    if (isset($personData['create_user_id'])) {
                        $personData['last_user_id'] = $personData['create_user_id'];
                        unset($personData['create_user_id']);
                    }
                    $dbModel->personModel->updatePerson($person_id, ['personData' => $personData, 'addressData' => $personAddressData]);
                }
            }
            if (empty($check)) {
                $personData['label'] = !empty($personData['label']) ? $personData['label'] : 'subscription';
                $person_id = $dbModel->personModel->addPerson(['personData' => $personData, 'addressData' => $personAddressData]);
            }
            $check = 0;
            $company_id = null;
            if (!empty($companyData['name'])) {
                $check = $dbModel->companyModel->issetCompany($companyData['name']);
                if (!empty($check)) {
                    $company_id = $check;
                    if (isset($companyData['create_user_id'])) {
                        $companyData['last_user_id'] = $companyData['create_user_id'];
                        unset($companyData['create_user_id']);
                    }
                    $dbModel->companyModel->updateCompany($company_id, ['companyData' => $companyData, 'addressData' => $companyAddressData, 'legalAddressData' => $companyLegalAddressData]);
                } else {
                    $companyData['label'] = !empty($companyData['label']) ? $companyData['label'] : 'subscription';
                    $company_id = $dbModel->companyModel->addCompany(['companyData' => $companyData, 'addressData' => $companyAddressData, 'legalAddressData' => $companyLegalAddressData]);
                }
            }

            if (!$person_id && !$company_id) {
                throw new Exception('Не указан подписчик при создании подписки.');
            }
            $subscriptionData['person_id'] = $person_id;
            $subscriptionData['company_id'] = empty($company_id) ? null : $company_id;
            if (empty($subscriptionData['label'])) {
                $subscriptionData['label'] = 'api_subscription';
            }

            $subscription_id = $dbModel->addSubscription([
                'orderData' => $orderData,
                'subscriptionData' => $subscriptionData,
                'magazinesData' => ['magazines' => $itemsData],
                'addressData' => $addressData,
                'mapAddressData' => $mapAddressData,
                'tagsData' => $tagsData,
                'nx_api_debug' => $params
            ]);
            $dbModel->adapter->getDriver()->getConnection()->commit();
            return $subscription_id;
        } catch (Exception $e) {
            $dbModel->adapter->getDriver()->getConnection()->rollback();
            var_dump($e->getMessage());
            return false;
        }
    }

    public function subscription_update(array $params)
    {
        $subscriptionID = (int)$params['id'];
        $subscriptionData = $params['subscriptionData']['subscriptionData'];
        $itemsData = !empty($params['subscriptionData']['itemsData']) ? $params['subscriptionData']['itemsData'] : [];
        $addressData = !empty($params['subscriptionData']['addressData']) ? $params['subscriptionData']['addressData'] : [];
        $personData = !empty($params['personData']['personData']) ? $params['personData']['personData'] : [];
        $personAddressData = !empty($params['personData']['addressData']) ? $params['personData']['addressData'] : [];

        if (empty($subscriptionID) || (empty($personData) && empty($subscriptionData) && empty($itemsData))) {
            return false;
        }

        $sm = $this->sm;
        $dbModel = $sm->get('nx\Model\Subscription');
        $subscription = $dbModel->getByID($subscriptionID);

        if (empty($subscription)) {
            return false;
        }

        $dbModel->adapter->isTransactional = 0;
        $dbModel->adapter->getDriver()->getConnection()->beginTransaction();
        try {
            if (!empty($personData)) {
                if (!empty($personData['name'])) {
                    $personData['name'] = trim(preg_replace('#\b(null)\b#isu', '', $personData['name']));
                }
                if (!empty($subscription->person_id)) {
                    $dbModel->personModel->updatePerson($subscription->person_id, ['personData' => $personData, 'addressData' => $personAddressData]);
                } else {
                    if (!empty($personData['name'])) {
                        $check = [];
                        $person_id = 0;
                        if (!empty($personData['email'])) {
                            $check = $dbModel->personModel->getBy(['name' => $personData['name'], 'email' => $personData['email']]);
                            if (!empty($check)) {
                                $person_id = $check->id;
                                $dbModel->personModel->updatePerson($person_id, ['personData' => $personData, 'addressData' => $personAddressData]);
                            }
                        }
                        if (empty($check)) {
                            if (isset($personData['last_user_id'])) {
                                $personData['create_user_id'] = $personData['last_user_id'];
                                unset($personData['last_user_id']);
                            }
                            $personData['label'] = !empty($personData['label']) ? $personData['label'] : 'subscription';
                            $person_id = $dbModel->personModel->addPerson(['personData' => $personData, 'addressData' => $personAddressData]);
                        }
                        if ($person_id) {
                            $subscriptionData['person_id'] = $person_id;
                        }
                    }
                }
            }
            $subscription_id = $dbModel->updateSubscription($subscriptionID, ['subscriptionData' => $subscriptionData, 'magazinesData' => ['magazines' => $itemsData], 'addressData' => $addressData]);
            if (empty($subscription_id)) {
                throw new Exception('Ошибка при обновлении подписки.');
            }
            $dbModel->adapter->getDriver()->getConnection()->commit();
            return $subscription_id;
        } catch (Exception $e) {
            $dbModel->adapter->getDriver()->getConnection()->rollback();
            var_dump($e->getMessage());
            return false;
        }
    }

    public function subscription_list(array $params)
    {
        $sm = $this->sm;
        $orderModel = $sm->get('nx\Model\Order');

        $params['shop_id'] = 1;//подписки
        //$params['debug'] = true;
        $result = $orderModel->getOrdersBy($params);

        return $result->toArray();
    }

    public function price_get_current()
    {
        $sm = $this->sm;
        $priceModel = $sm->get('nx\Model\Price');
        $result = $priceModel->getMagazinePriceList();
        return $result;
    }
}
