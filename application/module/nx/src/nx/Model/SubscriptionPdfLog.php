<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class SubscriptionPdfLog extends AbstractTable
{
    public function __construct(Adapter $adapter)
    {
        parent::__construct('MagazineSubscribes', $adapter);
    }

    public function getPdfLog($offset, $limit, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['S'=>'MagazineSubscribes'])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS Q.id AS id'),
            ])
            ->join(['Q'=>'MagazineSubscribesQueue'],'Q.subscribeID = S.id', ['email', 'send', 'created', 'sendtime' => 'sendTime', 'subscription_id' => 'subscriptionID'])
            ->join(['I'=>'Issue'], 'I.Message_ID = S.issueID', ['issue_title'])
            ->join(['M'=>'Edition'], 'M.Message_ID = I.MagazineID', ['periodical_name' => 'HumanizedName'])
            ->join(['PE'=>'publisher_editions'], 'PE.magazine_id = M.Message_ID', ['publisher_id' => 'publisher_id'])
            ->group('Q.id')
            ->order('Q.created DESC')
            ->limit($limit)
            ->offset($offset);

            if ($this->publisher_id) {
                $select
//                ->join(array('PR'=>'nx_price'), 'PR.id = O.price_id', array())
                ->where(['PE.publisher_id' => $this->publisher_id]);
            } else {
                $select->where('0'); //не выводить ничего если не знаем какой издатель
            }

        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(empty($filter['value']) && $filter['property']!='send')
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'created':
                    case 'email':
                        $select->where->like('Q.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'send':
                        $select->where->equalTo('Q.'.$filter['property'], '' . $filter['value'] . '');
                        break;
                    case 'issue_id':
                        $select->where->equalTo('S.issueID', '' . $filter['value'] . '');
                        break;
                    case 'subscription_id':
                        $select->where->like('Q.subscriptionID', '%' . $filter['value'] . '%');
                        break;
                    case 'sendtime':
                        $select->where->like('Q.sendTime', '%' . $filter['value'] . '%');
                        break;
                    case 'periodical_name':
                        $select->where->like('M.HumanizedName', '%' . $filter['value'] . '%');
                        break;
                    case 'periodical_id':
                        $select->where->equalTo('M.Message_ID', $filter['value']);
                        break;
                    case 'issue_title':
                        $select->where->like('I.issue_title', '%' . $filter['value'] . '%');
                        break;
                }
            }
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $items = $result->toArray();

        return [
            'items' => $items,
            'total' => $count['count']
        ];
    }

    public function getPdfLogByIssue($params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $columns = [
            'created',
            'sended'   => new Expression('SUM(S.sended)'),
            'queue'    => new Expression('SUM(S.queue)'),
            'issue_id' => 'issueID'
        ];
        if(!empty($params['calc_found_rows'])) {
            array_unshift($columns, new Expression('SQL_CALC_FOUND_ROWS S.id AS id'));
        }
        else {
            array_unshift($columns, 'id');
        }
        $select = $sql->select()
            ->from(['S'=>'MagazineSubscribes'])
            ->columns($columns)
            ->join(['I'=>'Issue'], 'I.Message_ID = S.issueID', ['issue_title'])
            ->join(['M'=>'Edition'], 'M.Message_ID = I.MagazineID', ['periodical_name' => 'HumanizedName'])
            ->order('S.created DESC')
            ->group(['S.issueID']);
        if(!empty($params['limit'])) {
            $select->limit($params['limit']);
        }
        if(!empty($params['offset'])) {
            $select->offset($params['offset']);
        }
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(empty($filter['value']) && $filter['property']!='send')
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'created':
                    case 'sended':
                    case 'queue':
                        $select->where->like('S.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'issue_title':
                        $select->where->like('I.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'periodical_name':
                        $select->where->like('M.HumanizedName', '%' . $filter['value'] . '%');
                        break;
                    case 'periodical_id':
                        $select->where->equalTo('M.Message_ID', $filter['value']);
                        break;
                    case 'issue_id':
                        $select->where(['S.issueID' => $filter['value']]);
                        break;
                }
            }
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        if(!empty($params['calc_found_rows'])) {
            $count = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();
        }

        $items = $result->toArray();

        $result = !empty($params['calc_found_rows']) ? ['items' => $items, 'total' => $count['count']] : $items;
        return $result;
    }
}
