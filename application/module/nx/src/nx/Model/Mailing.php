<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Mailing extends AbstractTable
{
    public function __construct(Adapter $adapter)
    {
        parent::__construct('Subs_Subscription', $adapter);
    }

    public function subscribeForAnons($params)
    {
        if(empty($params['periodical_id']) || empty($params['user_id']))
        {
            return false;
        }
        $data = [
            'User_ID'     => (int)$params['user_id'],
            'Magazine_ID' => (int)$params['periodical_id']
        ];
        //$this->insert($data);
        //$mailing_id = $this->lastInsertValue;
        //return $mailing_id;
    }
}
