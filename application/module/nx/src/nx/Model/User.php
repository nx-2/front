<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\TableIdentifier;
//use Zend\Db\RowGateway\RowGateway;

class User extends AbstractTable
{
    const Netcat_Permission_Group_ID = 20;
    const ROLE_ADMIN             = 'admin';
    const ROLE_ACCOUNTANT        = 'accountant';
    const ROLE_SUBSCRIBE_MANAGER = 'subscribe_manager';
    const ROLE_SHIP_MANAGER      = 'ship_manager';
    const ROLE_DEVELOPER         = 'developer';
    const ROLE_PUBLISHER         = 'publisher';

    public $logger, $user_session, $roleModel, $userRoleModel;

    public function __construct(Adapter $adapter)
    {
        //$this->table   = 'User';
        //$this->adapter = $adapter;
        //$this->featureSet = new \Zend\Db\TableGateway\Feature\FeatureSet();
        //$this->featureSet->addFeature(new \Zend\Db\TableGateway\Feature\RowGatewayFeature('id'));
        $this->user_session = new \Zend\Session\Container('user');
        //$this->resultSetPrototype = new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\User);
        parent::__construct('User', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\User));
    }

    public function getList($offset, $limit, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['U'=>'User'])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS U.User_ID AS id'),
                new Expression('CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName) AS full_name'),
                'checked' => 'Checked',
                'email'   => 'Email',
                'login'   => 'Login'
            ])
            // ->join(array('G' => 'User_Group'), 'G.User_ID = U.User_ID')
            ->join(['UR' => 'nx_user_role'], 'UR.user_id = U.User_ID', [], 'left')
            ->join(['R' => 'nx_role'], 'R.id = UR.role_id', ['roles' => new Expression('GROUP_CONCAT(DISTINCT IFNULL(R.name,"") SEPARATOR ", ")')], 'left')
            // ->where('G.PermissionGroup_ID = ' . self :: Netcat_Permission_Group_ID)
            ->where(['U.Checked' => 1])
            ->order('User_ID ASC')
            ->limit($limit)
            ->offset($offset)
            ->group('U.User_ID');
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(empty($filter['value']))
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'id':
                        $select->where->like('U.User_ID', '%' . $filter['value'] . '%');
                        break;
                    case 'email':
                        $select->where->like('U.Email', '%' . $filter['value'] . '%');
                        break;
                    case 'login':
                        $select->where->like('U.Login', '%' . $filter['value'] . '%');
                        break;
                    case 'name':
                        $select->where->NEST
                            ->like('U.LastName', '%' . $filter['value'] . '%')->OR
                            ->like('U.FirstName', '%' . $filter['value'] . '%')->OR
                            ->like('U.MidName', '%' . $filter['value'] . '%')->UNNEST;
                        break;
                }
            }
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        //$result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function getUsers($params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['U'=>'User'])
            ->columns([
                'id'        => 'User_ID',
                'full_name' => new Expression('CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName)'),
                'checked'   => 'Checked',
                'email'     => 'Email',
                'login'     => 'Login'
            ])
            // ->join(array('G' => 'User_Group'), 'G.User_ID = U.User_ID', array())
            ->join(['UR' => 'nx_user_role'], 'UR.user_id = U.User_ID', [], 'left')
            ->join(['R' => 'nx_role'], 'R.id = UR.role_id', ['roles' => new Expression('GROUP_CONCAT(DISTINCT IFNULL(R.name,"") SEPARATOR ", ")')], 'left')
            // ->where('G.PermissionGroup_ID = ' . self :: Netcat_Permission_Group_ID)
            ->where(['U.Checked' => 1])
            ->order('U.User_ID ASC')
            ->group('U.User_ID');

        if(!empty($params['role_id']))
        {
            $select->where(['UR.role_id' => (int)$params['role_id']]);
        }

        if(!empty($params['role_code']))
        {
            $select->where(['R.code' => $params['role_code']]);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getByID($id)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['U'=>'User'])
            ->columns([
                new Expression('CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName) AS full_name'),
                'id'         => 'User_ID',
                'login'      => 'Login',
                'email'      => 'Email',
                'checked'    => 'Checked',
                'last_name'  => 'LastName',
                'first_name' => 'FirstName',
                'mid_name'   => 'MidName',
                'publisher_id' => 'publisher_id',
            ])
            // ->join(array('G' => 'User_Group'), 'G.User_ID = U.User_ID', array())
            // ->where('G.PermissionGroup_ID = ' . self :: Netcat_Permission_Group_ID)
            ->where(['U.User_ID' => $id])
            ->where(['U.Checked' => 1])
            ->group('U.User_ID')
            ->limit(1);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function findByCredentials($login, $password)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $password = self::mysqlPassword($password);
        $select = $sql->select()
            ->from(['U'=>'User'])
            ->columns(['id' => 'User_ID'])
            // ->join(array('G' => 'User_Group'), 'G.User_ID = U.User_ID', array())
            // ->where('G.PermissionGroup_ID = ' . self :: Netcat_Permission_Group_ID)
            ->where(['U.Login' => $login])
            ->where(['U.Password = ?' => $password])
            ->where(['U.Checked' => 1])
            ->where('U.publisher_id IS NOT NULL')
            ->group('U.User_ID')
            ->limit(1);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    /**
     * Отдает список объектов для комбокса
     * @param string $keywords
     * @return array
     */
    public function getObjectsForCombo($keywords = '')
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['U'=>$this->table])
            ->columns([
                'id' => 'User_ID',
                'name' => new Expression('CONCAT(U.User_ID,". ", IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login))')
            ]);
        if(!empty($keywords))
        {
            $select->where->NEST
                ->like('U.User_ID', '%' . $keywords . '%')->OR
                ->like('U.Login', '%' . $keywords . '%')->OR
                ->like('U.Email', '%' . $keywords . '%')->OR
                ->like('U.LastName', '%' . $keywords . '%')->OR
                ->like('U.FirstName', '%' . $keywords . '%')->OR
                ->like('U.MidName', '%' . $keywords . '%')->UNNEST;
        }
        $select->order('U.User_ID ASC')->limit('150');
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $result       = array_merge([['id' => '0', 'name' => '0']], $result->toArray());
        return $result;
    }

    public static function generate_password($number)
    {
        $arr = ['a','b','c','d','e','f',

                     'g','h','i','j','k','l',

                     'm','n','o','p','r','s',

                     't','u','v','x','y','z',

                     'A','B','C','D','E','F',

                     'G','H','I','J','K','L',

                     'M','N','O','P','R','S',

                     'T','U','V','X','Y','Z',

                     '1','2','3','4','5','6',

                     '7','8','9','0'];
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }

        return $pass;
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function registerBySubscription($subscription)
    {
        if(empty($subscription['email']))
        {
            return false;
        }

        $userName = $subscription['person_name'];
        $userName = explode(' ', trim($userName).'   ');

        $password = self::generate_password(8);
        $password = self::mysqlPassword($password);
        $userData = [
            'Lastname'  =>  $userName[0],
            'Firstname' =>  $userName[1],
            'Midname'   =>  $userName[2],
            'Password'  =>  $password,
            'login'     =>  strtolower($subscription['email']),
            'email'     =>  strtolower($subscription['email']),
            'phone'     =>  $subscription['phone'],
            'country'   =>  $subscription['country_id'],
            'index_num' =>  $subscription['zipcode'],
            'address'   =>  $subscription['address'],
            'company'   =>  $subscription['company_name'] ? $order['company_name'] : '',
            'captcha'   =>  md5(uniqid(rand(), true)),
            'come_from' =>  !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''
        ];

        //insert
        //send mail message with credentials
        /*
        // изменить userID в таблице подписки
            $nc_core->db->query('
                UPDATE `nx_subscription` SET
                    `user_id` = '.intval($args['userID']).'
                WHERE `id` = '.intval($order['id']).'
            ');

            // подтвердить за пользователя его регистрацию
            $nc_core->db->query('
                UPDATE `User` SET
                    `Confirmed` = 1,
                    `RegistrationCode` = "",
                    `Checked` = 1
                WHERE `User_ID` = '.intval($args['userID']).'
            ');

            if($sendMail)
            {
                require_once $_SERVER['DOCUMENT_ROOT'] . '/osp_moduls/classes/MailTemplates.class.php';
                $mailTemplateModel = new MailTemplates();

                $magazinesNames = '';
                if(!empty($order_items))
                {
                    $magazinesNames = array();
                    foreach($order_items as $item)
                    {
                        $magazinesNames[$item['HumanizedName']] = 1;
                    }
                    $magazinesNames = implode(', ', array_keys($magazinesNames));
                }

                $mailTemplateModel->getParseAndSendTemplate(5,
                    array(
                        '%%USER_NAME%%'       => $order['Name'],
                        '%%MAGAZINES_NAMES%%' => $magazinesNames,
                        '%%USER_LOGIN%%'      => $userInfo['login'],
                        '%%USER_PASSWORD%%'   => $userInfo['password']
                    ),
                    array(
                        'priority' => 5,
                        'from'     => 'xpress@osp.ru',
                        'to_email' => $order['Email'],
                        'to_name'  => $order['Name']
                    )
                );
            }
            */
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function getRoles($data)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['R'=>'nx_role'])
            ->columns([
                'id',
                'name',
                'code'
            ])
            ->join(['UR' => 'nx_user_role'], 'UR.role_id = R.id', [])
            ->order('R.name DESC');
        if(!empty($data['user_id']))
        {
            $select->where('UR.user_id = ' . (int)$data['user_id']);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        return $result->toArray();
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function addUserRole($user_id, $role_id)
    {
        $data = [
            'user_id' => $user_id,
            'role_id' => $role_id
        ];

        $user  = $this->getByID($user_id);
        $role  = $this->roleModel->getByID($role_id);
        $check = $this->userRoleModel->select($data)->toArray();

        if(!empty($check) || empty($role->id) || empty($user->id))
        {
            return false;
        }

        $this->adapter->getDriver()->getConnection()->beginTransaction();
        try
        {
            $db  = $this->adapter;
            $sql = new Sql($db);
            $insert = $sql->insert('nx_user_role')->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

            $user_role_id = $db->getDriver()->getLastGeneratedValue();
            $this->logger->log('Добавление роли ' . $role->name . ' пользователю ' . $user->full_name . '#' . $user->id, 'add_role', $data, $this->user_session->user_id);
            $this->adapter->getDriver()->getConnection()->commit();
            return $user_role_id;
        }
        catch(\Exception $E)
        {
            $this->adapter->getDriver()->getConnection()->rollback();
            throw $E;
            return false;
        }
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function deleteUserRole($user_id, $role_id)
    {
        $data = [
            'user_id' => $user_id,
            'role_id' => $role_id
        ];

        $user = $this->getByID($user_id);
        $role = $this->roleModel->getByID($role_id);

        if(empty($role->id) || empty($user->id))
        {
            return false;
        }

        $this->adapter->getDriver()->getConnection()->beginTransaction();
        try
        {
            $db           = $this->adapter;
            $sql          = new Sql($db);
            $delete       = $sql->delete('nx_user_role')->where($data);
            $selectString = $sql->getSqlStringForSqlObject($delete);
            $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

            $this->logger->log('Удаление роли ' . $role->name . ' у пользователя ' . $user->full_name . '#' . $user->id, 'delete_role', $data, $this->user_session->user_id);
            $this->adapter->getDriver()->getConnection()->commit();
            return true;
        }
        catch(\Exception $E)
        {
            $this->adapter->getDriver()->getConnection()->rollback();
            throw $E;
            return false;
        }
    }
    public static function mysqlPassword($string)
    {
        $stage1 = sha1($string,true);
        return '*' . strtoupper(sha1($stage1));
    }
}
