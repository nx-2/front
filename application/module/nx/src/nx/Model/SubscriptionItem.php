<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class SubscriptionItem extends AbstractTable
{
    public $personModel, $companyModel, $subscriptionModel;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_order_item', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\SubscriptionItem));
    }

    public function loadPerson($items)
    {
        $person_ids = [];
        foreach($items as $i => $item) {
            $person_ids[] = $item->person_id;
        }
        if(!empty($person_ids)) {
            $persons = $this->personModel->select(['id' => $person_ids]);
            $persons_by_ids = [];
            foreach($persons as $person) {
                $persons_by_ids[$person->id] = $person;
            }
            foreach($items as $item) {
                if($item->person_id && isset($persons_by_ids[$item->person_id])) {
                    $item->person_name = $persons_by_ids[$item->person_id]->name;
                }
            }
        }
    }

    public function loadCompany($items)
    {
        $company_ids = [];
        foreach($items as $item) {
            $company_ids[] = $item->company_id;
        }
        if(!empty($company_ids)) {
            $companys = $this->companyModel->select(['id' => $company_ids]);
            $companys_by_ids = [];
            foreach($companys as $company) {
                $companys_by_ids[$company->id] = $company;
            }
            foreach($items as $item) {
                if($item->company_id && isset($companys_by_ids[$item->company_id])) {
                    $item->company_name = $companys_by_ids[$item->company_id]->name;
                    $item->company_inn = $companys_by_ids[$item->company_id]->inn;
                    $item->company_kpp = $companys_by_ids[$item->company_id]->kpp;
                }
            }
        }
    }

    public function loadPaymentType($items)
    {
        $types = $this->subscriptionModel->getPaymentTypes();
        foreach($items as $item) {
            if(isset($item->payment_type_id) && isset($types[$item->payment_type_id])) {
                $item->payment_type_name = $types[$item->payment_type_id]['name'];
            }
        }
    }

    public function getItemByParams($params = [])
    {
        $res = $this->getList(0,1,$params);
        $res = !empty($res['total']) ? $res['items']->toArray() : [];
        return !empty($res) ? $res[0] : [];

    }

    public function getList($offset, $limit, $params = [])
    {
        $db      = $this->adapter;
        $sql     = new Sql($db);
        $columns = [
            new Expression('SQL_CALC_FOUND_ROWS I.id AS id'),
            'order_id',
            'price',
            'delivery_type_id',
            'enabled' => new Expression('O.enabled'),
            'quantity',
            'created',
            'prolong_state',
            'discount'
        ];
        $select = $sql->select()
            ->from(['I'=>$this->table])
            ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', [
                'date_start' => new Expression('DATE_FORMAT(S.date_start,"%Y-%m-%d")'),
                'date_end'   => new Expression('DATE_FORMAT(S.date_end,"%Y-%m-%d")'),
                'period',
                'byear',
                'bnum',
                'rqty',
                'type',
                'is_present' => new Expression('IF(S.date_end>NOW(),1,0)'),
            ])
            ->join(['O' => 'nx_order'], 'O.id = I.order_id', [
                'order_state_id',
                'order_type'  => 'type',
                'email',
                'person_id',
                'company_id',
                'subscriber_type_id',
                'order_price' => 'price',
                'payment_type_id',
                'order_label' => 'label',
                'payment_date'
                //'order_date'  => new Expression('IF(O.order_state_id=3,DATE_FORMAT(O.payment_date,"%Y-%m-%d"),DATE_FORMAT(O.created,"%Y-%m-%d"))'),
            ])
            ->join(['OS'=>'nx_order_state'], 'OS.id = O.order_state_id', ['order_state_name'=>'name'])
            ->join(['M' => 'Edition'], 'M.Message_ID = S.periodical_id', ['periodical_name' => 'EnglishName', 'periodical_title' => 'HumanizedName'])
            ->join(['DT'=> 'nx_delivery_type'], 'DT.id = I.delivery_type_id', ['delivery_type_name'=> 'name'])
            ->order('I.id DESC')
            ->limit($limit)
            ->offset($offset);
        $filtered = [];
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
            	if(empty($filter['value']) && in_array($filter['property'], ['deliery_type_id', 'periodical_type_id', 'date_start', 'date_end', 'created_start', 'created_end', 'payment_date_start', 'payment_date_end']))
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'price':
                    case 'created':
                    case 'id':
                    case 'order_id':
                    case 'last_updated':
                        if (!empty($filter['xoperator']) && $filter['xoperator']=='eq') {
                            $select->where->equalTo('I.'.$filter['property'], $filter['value']);
                        }
                        else {
                            $select->where->like('I.'.$filter['property'], '%' . $filter['value'] . '%');
                        }
                        break;
                    case 'delivery_type_id':
                    case 'enabled':
                    case 'quantity':
                    case 'prolong_state':
                        $select->where->equalTo('I.'.$filter['property'], $filter['value']);
                        break;
                    case 'date_start':
                    case 'date_end':
                    case 'byear':
                    case 'rqty':
                        $select->where->like('S.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'period':
                    case 'bnum':
                    case 'type':
                        $select->where->equalTo('S.'.$filter['property'], $filter['value']);
                        break;
                    case 'periodical_id':
                        $select->where(['S.'.$filter['property'] => $filter['value']]);
                        break;
                    case 'email':
                        $select->where->like('O.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'order_type':
                        $select->where->equalTo('O.type', $filter['value']);
                        break;
                    case 'payment_type_id':
                    case 'order_state_id':
                        $select->where->equalTo('O.'.$filter['property'], $filter['value']);
                        break;
                    case 'name':
                        $filtered['name'] = true;
                        $columns['name'] = new Expression('IF(O.subscriber_type_id=2, CONCAT(IFNULL(C.name,""), " (", IFNULL(P.name,"") , ")"), P.name)');
                        $select
                            ->join(['P'=>'nx_person'],'P.id = O.person_id', ['person_name' => 'name'], 'left')
                            ->join(['C'=>'nx_company'],'C.id = O.company_id', ['company_name' => 'name', 'company_inn' => 'inn', 'company_kpp' => 'kpp'], 'left');
                        $select->where->NEST
                            ->like('P.'.$filter['property'], '%' . $filter['value'] . '%')->OR
                            ->like('C.'.$filter['property'], '%' . $filter['value'] . '%')->OR
                            ->like('O.email', '%' . $filter['value'] . '%')->OR
                            ->like('P.email', '%' . $filter['value'] . '%')->OR
                            ->like('C.email', '%' . $filter['value'] . '%')->UNNEST;
                        break;
                    case 'is_present':
                        $select->where->literal('S.date_end ' . ( $filter['value'] ? '>' : '<=') . ' NOW()');
                        break;
                    case 'order_label':
                        $select->where->like('O.label', '%' .  $filter['value'] . '%');
                        break;
                    case 'order_price':
                        $select->where->like('O.price', '%' .  $filter['value'] . '%');
                        break;
                    //case 'order_date':
                    //    $select->where->NEST
                    //        ->NEST->like('O.created', '%' . $filter['value'] . '%')->AND->notEqualTo('O.order_state_id', '3')->UNNEST->OR
                    //        ->NEST->like('O.payment_date', '%' . $filter['value'] . '%')->AND->equalTo('O.order_state_id', '3')->UNNEST
                    //        ->UNNEST;
                    //    break;
                    case 'order_enabled':
                        $select->where->equalTo('O.enabled', $filter['value']);
                        break;
                    case 'issue_title':
                        //if(array_search('periodical_id', array_map(function($el){return $el['property'];}, $params['filters'])) === false) {
                        //    continue;
                        //}
                        $select
                            ->join(['II'=>'nx_order_item_release'],'II.item_id = I.id', [])
                            ->join(['R'=>'Issue'],'R.Message_ID = II.release_id', []);
                        $select->where->like('R.issue_title', '%' .  $filter['value'] . '%');
                        break;
                    case 'payment_date':
                        $select->where->like('O.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'payment_date_start':
                        $select->where->greaterThanOrEqualTo('O.payment_date', $filter['value']);
                        break;
                    case 'payment_date_end':
                        $select->where->lessThanOrEqualTo('O.payment_date', $filter['value']);
                        break;
                }
            }
        }
        $select->columns($columns);
        //if(empty($params['show_all']) && empty($filtered['enabled']))
        //{
        //    $select->where->equalTo('O.enabled', 1);
        //}

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        $this->loadPaymentType($resultSet);
        if(empty($filtered['name'])) {
            $this->loadPerson($resultSet);
            $this->loadCompany($resultSet);
            foreach($resultSet as $item) {
                $item->fillName();
            }
        }

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function addItem($data)
    {
        $db    = $this->adapter;
        $check = [];
        if(!empty($data['periodical_id']) && !empty($data['type']) && !empty($data['byear']) && !empty($data['bnum']) && !empty($data['rqty']))
        {
            $sql    = new Sql($db);
            $select = $sql->select()->from(['S'=>'nx_subscription'])
                ->columns(['id'])
                ->where(['S.periodical_id' => $data['periodical_id']])
                ->where(['S.type' => $data['type']])
                ->where(['S.byear' => $data['byear']])
                ->where(['S.bnum' => $data['bnum']])
                ->where(['S.rqty' => $data['rqty']]);
            $selectString = $sql->getSqlStringForSqlObject($select);
            $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
            $check        = $result->current();
        }
        if(!empty($check['id']))
        {
            $subscription_id = $check['id'];
        }
        else
        {
            $subscription_data = [
                'periodical_id'  => $data['periodical_id'],
                'date_start'     => !empty($data['date_start']) ? $data['date_start'] : '0000-00-00 00:00:00',
                'date_end'       => !empty($data['date_end']) ? $data['date_start'] : '0000-00-00 00:00:00',
                'type'           => $data['type'],
                'enabled'        => 1,
                'period'         => !empty($data['period']) ? $data['period'] : 0,
                'byear'          => $data['byear'],
                'bnum'           => $data['bnum'],
                'rqty'           => $data['rqty'],
                'created'        => date('Y-m-d H:i:s'),
                'create_user_id' => $data['create_user_id']
            ];
            $insert            = $sql->insert('nx_subscription')->values($subscription_data);
            $selectString      = $sql->getSqlStringForSqlObject($insert);
            $result            = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
            $subscription_id   = $db->getDriver()->getLastGeneratedValue();
        }

        $item_data = [
            'order_id'         => $data['order_id'],
            'item_id'          => $subscription_id,
            'item_type'        => 'nx_subscription',
            'price'            => $data['price'],
            'discount'         => $data['discount'],
            'quantity'         => $data['quantity'],
            'delivery_type_id' => $data['delivery_type_id'],
            'created'          => date('Y-m-d H:i:s'),
            'create_user_id'   => $data['create_user_id']
        ];
        if(!empty($data['id'])) {
            $item_data['id'] = $data['id'];
        }
        if(!empty($data['last_user_id'])) {
            $item_data['last_user_id'] = $data['last_user_id'];
        }
        $insert       = $sql->insert('nx_order_item')->values($item_data);
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
    }
}
