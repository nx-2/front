<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
//use Zend\Db\RowGateway\RowGateway;

class Role extends AbstractTable
{
    public $logger;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_role', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Role));
    }

    public function getList($offset = 0, $limit = 0, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['R'=>$this->table])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS R.id AS id'),
                'name',
                'code'
            ])
            ->order('R.name DESC');
        if(!empty($limit))
        {
            $select->limit($limit);
        }
        if(!empty($offset))
        {
            $select->offset($offset);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function getByID($id)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['R'=>$this->table])
            ->columns([
                'id',
                'name',
                'code',
                'created',
                'last_updated',
                'create_user_id',
                'last_user_id'
            ])
            ->join(['U'=>'User'], 'U.User_ID = R.create_user_id', ['create_user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')], 'left')
            ->join(['U2'=>'User'], 'U2.User_ID = R.last_user_id', ['last_user_name' => new Expression('IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')], 'left')
            ->where(['R.id' => $id]);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function addRole($data)
    {
        if(empty($data['roleData']))
        {
            return false;
        }

        $this->adapter->getDriver()->getConnection()->beginTransaction();
        try
        {
            $data['roleData']['created'] = date('Y-m-d H:i:s');
            $this->insert($data['roleData']);
            $role_id = $this->lastInsertValue;
            $this->logger->log('Добавление роли ' . $data['roleData']['name'] . ' #' . $role_id, 'add_role', $data, $data['roleData']['create_user_id']);
            $this->adapter->getDriver()->getConnection()->commit();
            return $role_id;
        }
        catch(\Exception $E)
        {
            $this->adapter->getDriver()->getConnection()->rollback();
            throw $E;
            return false;
        }
    }

    public function updateRole($id, $data)
    {
        if(empty($id) || empty($data['roleData']))
        {
            return false;
        }

        $role = $this->getByID($id);

        if(empty($role->id))
        {
            return false;
        }

        $this->adapter->getDriver()->getConnection()->beginTransaction();
        try
        {
            $this->update($data['roleData'], ['id' => $role->id]);

            $this->logger->log('Обновление роли #' . $role->id, 'update_role', $data, $data['roleData']['last_user_id']);
            $this->adapter->getDriver()->getConnection()->commit();
            return $role->id;
        }
        catch(\Exception $e)
        {
            $this->adapter->getDriver()->getConnection()->rollback();
            throw $e;
            return false;
        }
    }

}
