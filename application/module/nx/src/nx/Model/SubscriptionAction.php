<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class SubscriptionAction extends AbstractTable
{
    public function __construct(Adapter $adapter)
    {
        //$this->table   = 'Message414';
        //$this->adapter = $adapter;
        parent::__construct('Message414', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\SubscriptionAction));
    }

    public function loadCounts($actions)
    {
        $ids = [];
        foreach($actions as $action) {
            $ids[] = $action->id;
        }
        if(!empty($ids)) {
            $db     = $this->adapter;
            $sql    = new Sql($db);
            $select = $sql->select()
                ->columns([
                    'id',
                    'order_state_id',
                    'action_id'
                ])
                ->from(['O'=>'nx_order'])
                ->where(['O.action_id' => $ids]);
            $selectString  = $sql->getSqlStringForSqlObject($select);
            $result        = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result_by_ids = [];
            foreach($result as $cur) {
                if(!isset($result_by_ids[$cur->action_id])) {
                    $result_by_ids[$cur->action_id] = ['count' => 0, 'count_pay' => 0];
                }
                $result_by_ids[$cur->action_id]['count']++;
                if($cur->order_state_id == 3) {
                    $result_by_ids[$cur->action_id]['count_pay']++;
                }
            }
            foreach($actions as $action) {
                if(isset($result_by_ids[$action->id])) {
                    $action->count     = $result_by_ids[$action->id]['count'];
                    $action->count_pay = $result_by_ids[$action->id]['count_pay'];
                }
            }
        }
    }

    public function loadActionPages($actions)
    {
        $ids = [];
        foreach($actions as $action) {
            $ids[] = $action->id;
        }
        if(!empty($ids)) {
            $db     = $this->adapter;
            $sql    = new Sql($db);
            $select = $sql->select()
                ->columns([
                    'id'         => 'Message_ID',
                    'is_limited' => 'isLimited',
                    'action_id'  => 'actionID'
                ])
                ->from(['P'=>'Message683'])
                ->where(['P.actionID' => $ids]);
            $selectString  = $sql->getSqlStringForSqlObject($select);
            $result        = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result_by_ids = [];
            foreach($result as $cur) {
                $result_by_ids[$cur->action_id] = $cur;
            }
            foreach($actions as $action) {
                if(isset($result_by_ids[$action->id])) {
                    $action->page_id = $result_by_ids[$action->id]->id;
                }
            }
        }
    }

    public function getActionBy(array $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['A'=>$this->table])
            ->columns([
                'id'             => 'Message_ID',
                'name'           => 'Name',
                'discount'       => 'Discount',
                'discount_table' => 'DiscountTable',
                'code'           => 'Code',
                'codes'          => 'Codes',
                'is_grouped'     => 'isGrouped',
                'email'          => 'MailFrom',
                'checked'        => 'Checked',
                'date_begin'     => 'DateBegin',
                'date_finish'    => 'DateFinish',
                'created'        => 'Created',
                'last_updated'   => 'LastUpdated',
                'create_user_id' => 'User_ID',
                'text'           => 'Text'
            ])
            ->limit(1);
        if(!empty($params['id'])) {
            $select->where(['A.Message_ID' => (int)$params['id']]);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();//, $db::QUERY_MODE_EXECUTE);

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $result = $resultSet->current();
        if($result->id)
        {
            $result->prepare_from_db();
        }
        return $result;
    }

    public function getActions(array $params = [])
    {
        if(empty($params['cc']))
        {
            $params['cc'] = 1094;
        }
        $columns = [
            'name'           => 'Name',
            'discount'       => 'Discount',
            'discount_table' => 'DiscountTable',
            'code'           => 'Code',
            'codes'          => 'Codes',
            'is_grouped'     => 'isGrouped',
            'email'          => 'MailFrom',
            'checked'        => 'Checked',
            'date_begin'     => 'DateBegin',
            'date_finish'    => 'DateFinish',
            'created'        => 'Created'
        ];
        if(!empty($params['calc_found_rows']))
        {
            array_unshift($columns, new Expression('SQL_CALC_FOUND_ROWS A.Message_ID AS id'));
        }
        else
        {
            $columns['id'] = 'Message_ID';
        }
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['A' => 'Message414'])
            ->columns($columns)
            ->order('Message_ID DESC');

        if(!empty($params['is_current']))
        {
            $select->where('(DateBegin IS NULL OR DateBegin < NOW()) AND (DateFinish IS NULL OR DateFinish > NOW())');
        }
        if(isset($params['checked']))
        {
            $select->where(['Checked' => (int)$params['checked']]);
        }
        if(!empty($params['id']))
        {
            $select->where(['Message_ID' => (int) $params['id']]);
        }
        if(!empty($params['cc']))
        {
            $select->where(['Sub_Class_ID' => (int) $params['cc']]);
        }
        if(!empty($params['code']))
        {
            $select->where->NEST
                ->equalTo('Code', $params['code'])->OR
                ->like('Codes', "%\r\n" . $params['code'] . "\r\n%")->OR
                ->like('Codes', $params['code'] . "\r\n%")->OR
                ->like('Codes', "%\r\n" . $params['code'])->UNNEST;
        }
        if(!empty($params['limit']))
        {
            $select->limit($params['limit']);
        }
        if(!empty($params['offset']))
        {
            $select->offset($params['offset']);
        }
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                switch($filter['property'])
                {
                    case 'id':
                        $select->where->like('A.Message_ID', '%' . $filter['value'] . '%');
                        break;
                    case 'name':
                        $select->where->like('A.Name', '%' . $filter['value'] . '%');
                        break;
                    case 'is_active':
                        if(empty($filter['value'])) {
                            $select->where('(A.DateBegin IS NOT NULL AND A.DateBegin >= NOW()) OR (A.DateFinish IS NOT NULL AND A.DateFinish <= NOW()) OR A.Checked = 0');
                        }
                        else {
                            $select->where('(A.DateBegin IS NULL OR A.DateBegin < NOW()) AND (A.DateFinish IS NULL OR A.DateFinish > NOW()) AND A.Checked = 1');
                        }
                        break;
                }
            }
        }
        //$select->order('Priority');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();//, $db::QUERY_MODE_EXECUTE);
        //$result       = $result->toArray();
        if(!empty($params['calc_found_rows'])) {
            $count = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();
        }

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        if($resultSet->count())
        {
            foreach($resultSet as $key=>$cur)
            {
                $cur->discount_table = unserialize($cur->discount_table);
                if($cur->code || $cur->codes)
                {
                    $codes = [];
                    if($cur->code)
                    {
                        $codes[] = md5($cur->code);
                    }
                    if($cur->codes)
                    {
                        $cur->codes = explode("\r\n",trim($cur->codes));
                        foreach($cur->codes as $codekey=>$code)
                        {
                            $codes[] = md5($code);
                        }
                    }
                    $cur->promocodes = $codes;
                }
                /*if(!empty($cur['discount']))
                {
                    $is_discount_table = 0;
                    if(!empty($result[$key]['discount_table']))
                    {
                        foreach($result[$key]['discount_table'] as $row)
                        {
                            if($row[0]!=-1)
                            {
                                $is_discount_table = 1;
                            }
                        }
                    }
                    if(!$is_discount_table && empty($cur['code']) && empty($cur['codes']))
                    {
                        unset($result[$key]);
                    }
                }*/
            }
        }

        if(!empty($params['load'])) {
            if(in_array('order', $params['load'])) {
                $this->loadCounts($resultSet);
            }
            if(in_array('page', $params['load'])) {
                $this->loadActionPages($resultSet);
            }
        }

        $result = !empty($params['calc_found_rows']) ? ['items' => $resultSet, 'total' => $count['count']] : $resultSet;
        return $result;
    }

}
