<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Publisher extends AbstractTable
{
//    public $companyModel, $transactionModel, $ws1c_client;

    const INTEGRATION_1C_URL = 1;
    const INTEGRATION_1C_LOGIN = 2;
    const INTEGRATION_1C_PASSWORD = 3;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('publisher', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Publisher));
    }
    public function getOne($id = false)
    {
        if (!$id)
            $id = $this->publisher_id;

        if (empty($id))
            return [];

        $id = intval($id);

        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['P'=>$this->table])
            ->columns([
                'id',
                'company_id',
                'contact_person_id',
                'logo',
                'description',
                'name',
                'distribution',
                'audience',
                'official_email',
                'created',
                'last_updated',
                'enabled',
                'create_user_id',
                'last_user_id',
            ])
            ->where(['P.id' => $id]);

        $select->join(['IV' => 'integration1c_verify'], 'P.id = IV.publisher_id', ['1c_integration_verified' => 'is_verified', 'last_verification_date'], 'left');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        if (!$resultSet->current())
            return [];

        $publisher = $resultSet->current()->toArray();

        $select = $sql->select()
            ->from(['PP' => 'publisher_property'])
            ->columns(['value_name' => 'property_type_id', 'value' => 'value'])
            ->where(['publisher_id' => $id])
            ->where(['property_type_id' => [self::INTEGRATION_1C_URL, self::INTEGRATION_1C_LOGIN, self::INTEGRATION_1C_PASSWORD]]);
        $queryString = $sql->getSqlStringForSqlObject($select);

        $props = $db->query($queryString)->execute();
        $accountingData = [];
        while($current = $props->current()) {
            switch ($current['value_name']) {
                case (self::INTEGRATION_1C_URL):
                    $accountingData['url'] =  $current['value'];
                    break;
                case (self::INTEGRATION_1C_PASSWORD):
                    $accountingData['password'] =  $current['value'];
                    break;
                case (self::INTEGRATION_1C_LOGIN):
                    $accountingData['login'] =  $current['value'];
                    break;
            }
            $props->next();
        }
        $publisher['integration_1c'] = $accountingData;
        return $publisher;
    }
}
