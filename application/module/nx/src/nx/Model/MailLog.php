<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class MailLog extends AbstractTable
{
    public function __construct(Adapter $adapter)
    {
        parent::__construct('aecms_mail_send_queue', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\MailLog));
    }

    public function getList($offset, $limit, $params = [])
    {
        $is_archive = 0;
        $table      = $this->table;
        if(!empty($params['from']) && $params['from'] == 'archive')
        {
            $is_archive = 1;
            $table      = 'aecms_mail_send_queue_history';
        }
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['L'=>$table])
            ->columns([
                $is_archive ? 'id' : new Expression('SQL_CALC_FOUND_ROWS L.id AS id'),
                'email'   => 'toEmail',
                'created',
                'from'    => 'fromEmail',
                'mail_id' => 'mailID'
            ])
            //->join(array('M'=>'aecms_mail_send'),'M.id = L.mailID', array('subject'))
            ->order('L.id DESC')
            ->limit($limit)
            ->offset($offset);

        $is_filter_subject = 0;
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(empty($filter['value']) || ($is_archive && in_array($filter['property'], ['subject', 'from'])))
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'id':
                    case 'created':
                        if($is_archive)
                        {
                            $select->where->like('L.'.$filter['property'], $filter['value'] . '%');
                        }
                        else
                        {
                            $select->where->like('L.'.$filter['property'], '%' . $filter['value'] . '%');
                        }
                        break;
                    case 'email':
                        if($is_archive)
                        {
                            //$select->where->like('L.toEmail', $filter['value'] . '%');
                            $select->where->equalTo('L.toEmail', $filter['value']);
                        }
                        else
                        {
                            $select->where->like('L.toEmail', '%' . $filter['value'] . '%');
                        }
                        break;
                    case 'subject':
                        $is_filter_subject = 1;
                        $select->join(['M'=>'aecms_mail_send'],'M.id = L.mailID', ['subject']);
                        $select->where->like('M.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'from':
                        $select->where->like('L.fromEmail', '%' . $filter['value'] . '%');
                        break;
                }
            }
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        if(!$is_archive)
        {
            $count = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();
        }

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result->getDataSource());
        $resultSet->buffer();

        if($resultSet->count() && !$is_filter_subject)
        {
            $mail_ids = [];
            foreach($resultSet as $row)
            {
                $mail_ids[] = $row->mail_id;
            }
            if(!empty($mail_ids))
            {
                $sql    = new Sql($db);
                $select = $sql->select()
                    ->from(['M'=>'aecms_mail_send'])
                    ->columns([
                        'id',
                        'subject'
                    ])
                    ->where(['id' => $mail_ids]);
                $selectString = $sql->getSqlStringForSqlObject($select);
                $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                $mails        = $result->toArray();
                $mails_by_id  = [];
                foreach($mails as $mail)
                {
                    $mails_by_id[$mail['id']] = $mail;
                }
                foreach($resultSet as $row)
                {
                    if(!empty($mails_by_id[$row->mail_id]['subject']))
                    {
                        $row->subject = $mails_by_id[$row->mail_id]['subject'];
                    }
                }
            }
        }

        return [
            'items' => $resultSet,
            'total' => !$is_archive ? $count['count'] : 1000000
        ];
    }

    public function getBy(array $params)
    {
        $table = $this->table;
        if(!empty($params['from']) && $params['from'] == 'archive')
        {
            $table = 'aecms_mail_send_queue_history';
        }
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['L'=>$table])
            ->columns([
                'id',
                'email' => 'toEmail',
                'created',
                'from' => 'fromEmail'
            ])
            ->join(['M'=>'aecms_mail_send'],'M.id = L.mailID', ['subject', 'message' => new Expression('IF(M.html<>"", M.html, M.plain)')])
            ->limit(1);
        if(!empty($params['id']))
        {
            $select->where(['L.id' => $params['id']]);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }
}
