<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use DateTime;
use Exception;
use nx\Entity\SubscriptionOrder;
use nx\Service\PublishAdmin;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\TableIdentifier;
use Zend\EventManager\EventManager;
use ZendDeveloperTools\Collector\EventCollectorInterface;
use function nx\functions\applyFunctionToData;

/**
 * @property EventManager $events
 * @property PublishAdmin $publishAdminService
 *
 */
class Subscription extends AbstractTable
{
    const DELIVERY_POST = 1;
    const DELIVERY_OFFICE = 2;
    const DELIVERY_COURIER = 3;
    const DELIVERY_PDF = 5;
    const DELIVERY_AVIA = 9;

    const EVENT_HAS_BECOME_ACTIVE = 'hasBecomeActive';

    public $addressModel, $personModel, $companyModel, $itemModel, $xpressdb, $xpressModel, $fileStorageModel, $itemReleaseModel, $mailTemplatesModel, $tagEntityModel;
    public $priceModel;
    public $publishAdminService;
    public $actionService, $ws1c_client;//, $passportService;
    public $defaultCountryID = 165, $defaultZoneID = 4;
    public $currentPrice;
    public $aecms_adapter;
    private $data = [];
    private $map_close_day = 0;

    protected $events;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_order', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\SubscriptionOrder));

    }

    public function setEventManager(EventManager $events)
    {
        $this->events = $events;
    }

    public function getEvents(): EventManager
    {
        if (empty($this->events)) {
            $this->setEventManager(new EventManager([__CLASS__, get_called_class()]));
        }
        return $this->events;
    }

    public function getSubscriptionList($offset, $limit, $params = [])
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        $columns = [
            new Expression('SQL_CALC_FOUND_ROWS O.id AS id'),
            'xpress_id',
            'price',
            'label',
            'payment_type_id',
            'payment_agent_id',
            'order_state_id',
            'subscriber_type_id',
            'enabled',
            //'checked',
            'created',
            'payment_date',
            'last_updated',
            'date' => new Expression('IF(O.order_state_id=3,DATE_FORMAT(O.payment_date,"%Y-%m-%d"),DATE_FORMAT(O.created,"%Y-%m-%d"))'),
            //'name'       => new Expression('IF(O.subscriber_type_id=2, CONCAT(IFNULL(C.name,""), " (", IFNULL(P.name,"") , ")"), P.name)'),
            //'is_present' => new Expression('IF(MAX(S.date_end)>NOW(),1,0)'),
            'email',
            'type',
            'hash',
            'person_id',
            'company_id',
            'address_id',
            'map_address_id',
            'comment'

        ];
        $select = $sql->select()
            ->from(['O' => $this->table])
            //->join(array('P'=>'nx_person'),'P.id = O.person_id', array('person_name' => 'name'), 'left')
            //->join(array('C'=>'nx_company'),'C.id = O.company_id', array('company_name' => 'name', 'company_inn' => 'inn', 'company_kpp' => 'kpp'), 'left')
            ->join(['I' => 'nx_order_item'], 'I.order_id = O.id', [])
            ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', ['date_start_sub' => 'date_start'])
            //->join(array('M'=>'Edition'), 'M.Message_ID = S.periodical_id', array('periodical_names' => new Expression('GROUP_CONCAT(DISTINCT M.EnglishName)')))
            //->join(array('OS'=>'nx_order_state'), 'OS.id = O.order_state_id', array('order_state_name'=>'name'))
            //->join(array('PT'=>'nx_payment_type'), 'PT.id = O.payment_type_id', array('payment_type_name'=>'name'))
            //->join(array('DT'=>'nx_delivery_type'), 'DT.id = I.delivery_type_id', array('delivery_type_names'=> new Expression('GROUP_CONCAT(DISTINCT DT.name)')))
            ->where(['I.item_type' => 'nx_subscription'])
            ->group('O.id')
            //->order('O.id DESC')
            ->limit($limit)
            ->offset($offset);

        if ($this->publisher_id) {
            $select
                ->join(['PR' => 'nx_price'], 'PR.id = O.price_id', [])
                ->where(['PR.publisher_id' => $this->publisher_id]);
        } else {
            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }

        $filtered = [];
        $joins = [];
        if (!empty($params['filters'])) {
            foreach ($params['filters'] as $filter) {
                /*if(!empty($filter['field']))
                {
                    $filter['property'] = $filter['field'];
                }
                $filter['comparison'] = !empty($filter['comparison']) ? $filter['comparison'] : "";*/

                if (empty($filter['value']) && in_array($filter['property'], ['date_start', 'date_end', 'created_start', 'created_end', 'payment_date_start', 'payment_date_end'])) {
                    continue;
                }
                $filtered[$filter['property']] = true;
                switch ($filter['property']) {
                    //case 'last_updated':
                    //case 'payment_date':
                    case 'id':
                    case 'label':
                    case 'action_code':
                    case 'email':
                        if (!empty($filter['xoperator']) && $filter['xoperator'] == '!=') {
                            if (empty($filter['value'])) {
                                $select->where->notEqualTo('O.' . $filter['property'], $filter['value']);
                            } else {
                                $select->where->addPredicate(new \Zend\Db\Sql\Predicate\NotLike('O.' . $filter['property'], '%' . $filter['value'] . '%'));
                            }
                        } else {
                            if (empty($filter['value'])) {
                                $select->where->equalTo('O.' . $filter['property'], $filter['value']);
                            } else {
                                $select->where->like('O.' . $filter['property'], '%' . $filter['value'] . '%');
                            }
                        }
                        break;
                    case 'manager_id':
                    case 'payment_agent_id':
                    case 'subscriber_type_id':
                    case 'payment_type_id':
                    case 'order_state_id':
                    case 'enabled':
                    case 'type':
                    case 'action_id':
                    case 'price':
                    case 'new_state':
                        $select->where->equalTo('O.' . $filter['property'], '' . $filter['value'] . '');
                        break;
                    case 'xpress_id':
                        if ($filter['value']) {
                            $select->where->like('O.' . $filter['property'], '%' . $filter['value'] . '%');
                        } else {
                            $select->where->equalTo('O.' . $filter['property'], '' . $filter['value'] . '');
                        }
                        break;
                    case 'delivery_type_id':
                        $select->where->equalTo('I.' . $filter['property'], '' . $filter['value'] . '');
                        break;
                    case 'periodical_id':
                        if (empty($joins['nx_subscription'])) {
                            // $select->join(array('S'=>'nx_subscription'), 'S.id = I.item_id', array());
                            $joins['nx_subscription'] = true;
                        }
                        $select->where(['S.' . $filter['property'] => $filter['value']]);
                        break;
                    case 'name':
                        //$filtered['name'] = true;
                        $columns['name'] = new Expression('IF(O.subscriber_type_id=2, CONCAT(IFNULL(C.name,""), " (", IFNULL(P.name,"") , ")"), P.name)');
                        if (empty($joins['nx_person'])) {
                            $select->join(['P' => 'nx_person'], 'P.id = O.person_id', ['person_name' => 'name'], 'left');
                            $joins['nx_person'] = true;
                        }
                        if (empty($joins['nx_company'])) {
                            $select->join(['C' => 'nx_company'], 'C.id = O.company_id', ['company_name' => 'name', 'company_inn' => 'inn', 'company_kpp' => 'kpp'], 'left');
                            $joins['nx_company'] = true;
                        }
                        $select->where->NEST
                            ->like('P.' . $filter['property'], '%' . $filter['value'] . '%')->OR
                            ->like('C.' . $filter['property'], '%' . $filter['value'] . '%')->OR
                            ->like('O.email', '%' . $filter['value'] . '%')->OR
                            ->like('O.extra_emails', '%' . $filter['value'] . '%')->OR
                            ->like('P.email', '%' . $filter['value'] . '%')->OR
                            ->like('C.email', '%' . $filter['value'] . '%')->UNNEST;
                        break;
                    /*case 'date':
                        $select->where->NEST
                        ->NEST->like('O.created', '%' . $filter['value'] . '%')->AND->notEqualTo('O.order_state_id', '3')->UNNEST->OR
                        ->NEST->like('O.payment_date', '%' . $filter['value'] . '%')->AND->equalTo('O.order_state_id', '3')->UNNEST
                        ->UNNEST;
                        break;
                    case 'date_start':
                        $select->where->NEST
                            ->NEST->greaterThanOrEqualTo('O.created', $filter['value'])->AND->notEqualTo('O.order_state_id', '3')->UNNEST->OR
                            ->NEST->greaterThanOrEqualTo('O.payment_date', $filter['value'])->AND->equalTo('O.order_state_id', '3')->UNNEST
                            ->UNNEST;
                        break;
                    case 'date_end':
                        $select->where->NEST
                            ->NEST->lessThanOrEqualTo('O.created', $filter['value'])->AND->notEqualTo('O.order_state_id', '3')->UNNEST->OR
                            ->NEST->lessThanOrEqualTo('O.payment_date', $filter['value'])->AND->equalTo('O.order_state_id', '3')->UNNEST
                            ->UNNEST;
                        break;*/
                    case 'created':
                    case 'payment_date':
                    case 'last_updated':
                        $select->where->like('O.' . $filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'created_start':
                        $select->where->greaterThanOrEqualTo('O.created', $filter['value']);
                        break;
                    case 'created_end':
                        $select->where->lessThanOrEqualTo('O.created', $filter['value']);
                        break;
                    case 'payment_date_start':
                        $select->where->greaterThanOrEqualTo('O.payment_date', $filter['value']);
                        break;
                    case 'payment_date_end':
                        $select->where->lessThanOrEqualTo('O.payment_date', $filter['value']);
                        break;
                    case 'date_start_sub_id':
                        $select->where->like('S.date_start', '%' . $filter['value'] . '%');
                        break;
                    case 'is_present':
                        if (empty($joins['nx_subscription'])) {
                            $select->join(['S' => 'nx_subscription'], 'S.id = I.item_id', []);
                            $joins['nx_subscription'] = true;
                        }
                        $select->having->literal('MAX(S.date_end) ' . ($filter['value'] ? '>' : '<=') . ' NOW()');
                        break;
                    case 'issue_title':
                        $select
                            ->join(['II' => 'nx_order_item_release'], 'II.item_id = I.id', [])
                            ->join(['R' => 'Issue'], 'R.Message_ID = II.release_id', []);
                        $select->where->like('R.issue_title', '%' . $filter['value'] . '%');
                        break;
                    case 'tags':
                        if (empty($filter['value'])) {
                            $select->join(['TE' => 'nx_tag_entity'], 'TE.row_id = O.id', [], 'left');
                            $select->where->NEST->notEqualTo('TE.entity_id', TagEntity::ENTITY_ORDER)->OR->isNull('TE.id')->UNNEST;
                        } else {
                            if (!empty($filter['xoperator']) && $filter['xoperator'] == 'OR') {
                                $select->join(['TE' => 'nx_tag_entity'], 'TE.row_id = O.id', []);
                                $select->where(['TE.entity_id' => TagEntity::ENTITY_ORDER])->where(['TE.tag_id' => $filter['value']]);
                            } else {
                                foreach ($filter['value'] as $i => $tag_id) {
                                    $select->join(['TE' . $i => 'nx_tag_entity'], 'TE' . $i . '.row_id = O.id', []);
                                    $select->where(['TE' . $i . '.entity_id' => TagEntity::ENTITY_ORDER])->where(['TE' . $i . '.tag_id' => $tag_id]);
                                }
                            }
                        }
                        break;
                    case 'phone':
                        if (empty($filter['value'])) {
                            $select->join(['A' => 'nx_address'], 'A.id = O.address_id', [], 'left');
                            $select->where('A.id is NULL OR A.phone = ""');
                        } else {
                            $select->join(['A' => 'nx_address'], 'A.id = O.address_id', []);
                            $select->where->like('A.phone', '%' . $filter['value'] . '%');
                        }
                        break;
                }
            }
        }
        if (!empty($params['sorters'])) {
            foreach ($params['sorters'] as $sorter) {
                if (empty($sorter['direction'])) {
                    continue;
                }
                switch ($sorter['property']) {
                    case 'price':
                    case 'label':
                    case 'id':
                    case 'payment_date':
                    case 'created':
                        $select->order('O.' . $sorter['property'] . ' ' . $sorter['direction']);
                        break;
                    case 'order_state_name':
                        $select->order('O.order_state_id' . ' ' . $sorter['direction']);
                        break;
                    case 'payment_type_name':
                        $select->order('O.payment_type_id' . ' ' . $sorter['direction']);
                        break;
                    case 'delivery_type_names':
                        $select->order('I.delivery_type_id' . ' ' . $sorter['direction']);
                        break;
                    /*case 'periodical_names':
                        $select->order('S.periodical_id' . ' ' . $sorter['direction']);
                        break;*/
                    case 'date':
                        $select->order($sorter['property'] . ' ' . $sorter['direction']);
                        break;
                    case 'name':
                        $select->order($sorter['property'] . ' ' . $sorter['direction']);
                        $columns['name'] = new Expression('IF(O.subscriber_type_id=2, CONCAT(IFNULL(C.name,""), " (", IFNULL(P.name,"") , ")"), P.name)');
                        if (empty($joins['nx_person'])) {
                            $select->join(['P' => 'nx_person'], 'P.id = O.person_id', ['person_name' => 'name'], 'left');
                            $joins['nx_person'] = true;
                        }
                        if (empty($joins['nx_company'])) {
                            $select->join(['C' => 'nx_company'], 'C.id = O.company_id', ['company_name' => 'name', 'company_inn' => 'inn', 'company_kpp' => 'kpp'], 'left');
                            $joins['nx_company'] = true;
                        }
                        break;
                }
            }
        } else {
            $select->order('O.id DESC');
        }

        $select->columns($columns);
        if (!empty($params['by'])) {
            switch ($params['by']) {
                case 'site':
                    $select->where('O.label <> "netcat_subscription"');
                    $select->where('O.label <> "xpress_subscription"');
                    $select->where('O.label <> "nx_subscription"');
                    break;
                case 'pdf':
                    if (empty($joins['nx_subscription'])) {
                        // $select->join(array('S'=>'nx_subscription'), 'S.id = I.item_id', array());
                        $joins['nx_subscription'] = true;
                    }
                    $select->where('S.type = "pdf"');
                    $select->where('O.order_state_id = 3');
                    break;
            }
        }
        if (!empty($params['ids'])) {
            $select->where->In('O.id', $params['ids']);
            $params['show_all'] = 1;
        }
        if (empty($params['show_all']) && empty($filtered['enabled'])) {
            $select->where->equalTo('O.enabled', 1);
        }
        $selectString = $sql->buildSqlString($select); //var_dump($selectString);
        $result = $db->query($selectString)->execute();
        $count = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        $this->loadItems($resultSet);
        $this->loadState($resultSet);
        $this->loadPaymentType($resultSet);
        $this->loadAddress($resultSet);
        if (empty($columns['name'])) {
            $this->loadPerson($resultSet);
            $this->loadCompany($resultSet);
            foreach ($resultSet as $subscription) {
                $subscription->fillName();
            }
        }

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function loadItems($subscriptions)
    {
        $ids = [];
        foreach ($subscriptions as $subscription) {
            $ids[] = $subscription->id;
        }
        if (!empty($ids)) {
            $db = $this->adapter;
            $sql = new Sql($db);
            $select = $sql->select()
                ->columns([
                    'id',
                    'is_present' => new Expression('IF(MAX(S.date_end)>NOW(),1,0)'),
                ])
                ->from(['O' => $this->table])
                ->join(['I' => 'nx_order_item'], 'I.order_id = O.id', ['delivery_types' => new Expression('GROUP_CONCAT(I.delivery_type_id)')])
                ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', [])
                ->join(['M' => new TableIdentifier('Edition', 'nxnew')], 'M.Message_ID = S.periodical_id', ['periodical_names' => new Expression('GROUP_CONCAT(DISTINCT M.EnglishName)')])
                ->join(['DT' => 'nx_delivery_type'], 'DT.id = I.delivery_type_id', ['delivery_type_names' => new Expression('GROUP_CONCAT(DISTINCT DT.name)')])
                ->where(['O.id' => $ids])
                ->where(['I.item_type' => 'nx_subscription'])
                ->group('O.id');

            if ($this->publisher_id) {
                $select
                    ->join(['PR' => 'nx_price'], 'PR.id = O.price_id', [])
                    ->where(['PR.publisher_id' => $this->publisher_id]);
            } else {
                $select->where('0');
            }

            $selectString = $sql->buildSqlString($select);//echo($selectString);die();
            $result = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result_by_ids = [];
            foreach ($result as $cur) {
                $result_by_ids[$cur->id] = $cur;
            }
            foreach ($subscriptions as $subscription) {
                if (isset($result_by_ids[$subscription->id])) {
                    $subscription->periodical_names = $result_by_ids[$subscription->id]['periodical_names'];
                    $subscription->delivery_type_names = $result_by_ids[$subscription->id]['delivery_type_names'];
                    $subscription->is_present = $result_by_ids[$subscription->id]['is_present'];
                    $subscription->delivery_types = $result_by_ids[$subscription->id]['delivery_types'];
                }
            }
        }
    }

    public function loadState($subscriptions)
    {
        $states = $this->getOrderStates();
        foreach ($subscriptions as $subscription) {
            if (isset($subscription->order_state_id) && isset($states[$subscription->order_state_id])) {
                $subscription->order_state_name = $states[$subscription->order_state_id]['name'];
            }
        }
    }

    public function getOrderStates()
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        $select = $sql->select()
            ->from(['C' => 'nx_order_state']);
        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $result = $result->toArray();

        if (!empty($result)) {
            $states_by_id = [];
            foreach ($result as $state) {
                $states_by_id[$state['id']] = $state;
            }
            $result = $states_by_id;
        }

        return $result;
    }

    public function loadPaymentType($subscriptions)
    {
        $types = $this->getPaymentTypes();
        foreach ($subscriptions as $subscription) {
            if (isset($subscription->payment_type_id) && isset($types[$subscription->payment_type_id])) {
                $subscription->payment_type_name = $types[$subscription->payment_type_id]['name'];
            }
        }
    }

    public function getPaymentTypes()
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        $select = $sql->select()->from(['C' => 'nx_payment_type']);
        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $result = $result->toArray();

        if (!empty($result)) {
            $payments_by_id = [];
            foreach ($result as $payment) {
                $payments_by_id[$payment['id']] = $payment;
            }
            $result = $payments_by_id;
        }

        return $result;
    }

    public function loadAddress($items)
    {
        $this->loadExtra(['phone', 'country_id'], 'nx_address', 'id', $items, 'address_id');

        //$this->loadExtra(array('country_name' => 'Country_Name'), 'Classificator_Country', 'Country_ID', $items, 'country_id');

        $this->loadExtra(['area', 'region', 'city', 'city1', 'street', 'house', 'building', 'structure', 'apartment', 'comment', 'zipcode', 'post_box', 'is_onclaim'], 'nx_address', 'id', $items, 'map_address_id', 'map_address');

        foreach ($items as $i => $item) {
            if ($item->map_address) {
                $address_fields = ['area', 'region', 'city', 'city1', 'street', 'house', 'building', 'structure', 'apartment', 'comment'];
                $fields_prefix = ['house' => 'д.', 'building' => 'корп.', 'structure' => 'стр.', 'apartment' => 'кв.'];
                $item->map_address->comment = preg_replace('#[\t\v\r\n\f;]#ius', ' ', $item->map_address->comment);
                //if(!empty($item->country_id) && $item->country_id != \nx\Model\Address::COUNTRY_ID_RUSSIA) {
                //    array_unshift($address_fields, 'country_name');
                //}
                if (!empty($item->map_address->post_box) || !empty($item->map_address->is_onclaim)) {
                    $item->map_address->street = ($item->map_address->post_box ? 'а/я ' . $item->map_address->post_box : ($item->map_address->is_onclaim ? 'ДО ВОСТРЕБОВАНИЯ' : ''));
                    $item->map_address->house = $item->map_address->building = $item->map_address->structure = $item->map_address->apartment = '';
                }
                $item->full_map_address = '';
                foreach ($address_fields as $field) {
                    $item->full_map_address .= (!empty($item->map_address->$field) ? (!empty($fields_prefix[$field]) ? $fields_prefix[$field] . ' ' : '') . $item->map_address->$field . ', ' : '');
                }
                $item->full_map_address = mb_substr($item->full_map_address, 0, -2);
                $item->map_zipcode = $item->map_address->zipcode;
            }
        }
    }

    public function loadPerson($subscriptions)
    {
        $person_ids = [];
        foreach ($subscriptions as $i => $subscription) {
            $person_ids[] = $subscription->person_id;
        }
        if (!empty($person_ids)) {
            $persons = $this->personModel->select(['id' => $person_ids]);
            $persons_by_ids = [];
            foreach ($persons as $person) {
                $persons_by_ids[$person->id] = $person;
            }
            foreach ($subscriptions as $subscription) {
                if ($subscription->person_id && isset($persons_by_ids[$subscription->person_id])) {
                    $subscription->person_name = $persons_by_ids[$subscription->person_id]->name;
                }
            }
        }
    }

    public function loadCompany($subscriptions)
    {
        $company_ids = [];
        foreach ($subscriptions as $subscription) {
            $company_ids[] = $subscription->company_id;
        }
        if (!empty($company_ids)) {
            $companys = $this->companyModel->select(['id' => $company_ids]);
            $companys_by_ids = [];
            foreach ($companys as $company) {
                $companys_by_ids[$company->id] = $company;
            }
            foreach ($subscriptions as $subscription) {
                if ($subscription->company_id && isset($companys_by_ids[$subscription->company_id])) {
                    $subscription->company_name = $companys_by_ids[$subscription->company_id]->name;
                    $subscription->company_inn = $companys_by_ids[$subscription->company_id]->inn;
                    $subscription->company_kpp = $companys_by_ids[$subscription->company_id]->kpp;
                }
            }
        }
    }

    public function getPaymentAgents()
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        $select = $sql->select()->from('nx_payment_agent')->order('id');
        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $result = $result->toArray();
        return $result;
    }

    public function getDeliveryTypes($id_as_key = false)
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        $select = $sql->select()
            ->from(['C' => 'nx_delivery_type'])
            ->where(['enabled' => 1]);
        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $result = $result->toArray();

        if (!empty($result) && $id_as_key) {
            $delivery_by_id = [];
            foreach ($result as $delivery) {
                $delivery_by_id[$delivery['id']] = $delivery;
            }
            $result = $delivery_by_id;
        }

        return $result;
    }

    public function getCountries()
    {
        $db = $this->aecms_adapter;
        $sql = new Sql($db);
        $select = $sql->select()
            ->from(['C' => 'Classificator_Country'])
            ->columns([
                'id' => 'Country_ID',
                'name' => 'Country_Name'
            ]);
        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);

        return $result->toArray();
    }

    public function updateXpressImportStats()
    {
        $data = [
            'counts' => [
                'total' => 0,
                'lock' => 0,
                'link' => 0,
                'add' => 0,
                'manual' => 0
            ],
            'ids' => [
                'lock' => [],
                'link' => [],
                'add' => [],
                'manual' => []
            ]
        ];

        $subscriptions = $this->xpressModel->getSubscriptions(['is_present' => 1, 'is_payed' => 1, 'not_source' => 18]);
        if ($subscriptions->count()) {
            ini_set("max_execution_time", "90");
            $data['counts']['total'] = $subscriptions->count();
            foreach ($subscriptions as $subscription) {
                $result = $this->importFromXpress([
                    'xdb' => $this->xpressModel->getAdapter(),
                    'xpress_id' => $subscription->id,
                    'mode' => 2
                ]);
                $data['counts'][$result['action']]++;
                $subscription_data = ['id' => $subscription->id, 'comment' => $result['comment']];
                if ($result['subscription_link_id']) {
                    $subscription_data['link_id'] = $result['subscription_link_id'];
                }
                $data['ids'][$result['action']][] = $subscription_data;
            }
        }

        if (!empty($data['counts']['total'])) {
            file_put_contents('/var/www/nx.osp.ru/htdocs/application/data/cache/xpressImportStats.txt', serialize($data));
        }

        return $data;
    }

    public function getSubscriptions($params = [])
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        $select = $sql->select()
            ->from(['O' => $this->table])
            ->columns([
                'id',
                'price',
                'label',
                'payment_type_id',
                'order_state_id',
                'subscriber_type_id',
                'enabled',
                //'checked',
                'created',
                'payment_date',
                'company_id',
                'is_present' => new Expression('IF(MAX(S.date_end)>NOW(),1,0)')
            ])
            ->join(['I' => 'nx_order_item'], 'I.order_id = O.id', ['delivery_types' => new Expression('GROUP_CONCAT(I.delivery_type_id)')])
            ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', [])
            ->join(['C' => 'nx_company'], 'C.id = O.company_id', ['company_name' => 'name', 'company_inn' => 'inn', 'company_kpp' => 'kpp'], 'left')
            ->where(['I.item_type' => 'nx_subscription'])
            ->group('O.id')
            ->order('O.id DESC');

        if ($this->publisher_id) {
            $select
                ->join(['PR' => 'nx_price'], 'PR.id = O.price_id', [])
                ->where(['PR.publisher_id' => $this->publisher_id]);
        } else {
            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }


        if (!empty($params['is_present'])) {
            $select->having->literal('MAX(S.date_end) > NOW()');
        }
        if (!empty($params['not_magazine_ids'])) {
            $select->where->addPredicate(new \Zend\Db\Sql\Predicate\NotIn('S.periodical_id', $params['not_magazine_ids']));
        }
        if (!empty($params['ids'])) {
            $select->where->In('O.id', $params['ids']);
        }
        if (!empty($params['item_updated_end'])) {
            $select->where->lessThanOrEqualTo('I.last_updated', $params['item_updated_end']);
        }
        if (!empty($params['limit'])) {
            $select->limit($params['limit']);
        }
        if (!empty($params['order_state_id'])) {
            $select->where(['O.order_state_id' => (int)$params['order_state_id']]);
        }
        if (isset($params['new_state'])) {
            $select->where(['O.new_state' => (int)$params['new_state']]);
        }
        if (!empty($params['isset_email'])) {
            $select->where('O.email <> ""');
        }

        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        return $resultSet;
    }

    public function importFromXpress($data)
    {
        //mode:
        //0 - no db changes return true/false
        //1 - no db changes return insert data
        //2 - no db changes return founded similar data
        //3 - proceed db changes
        //4 - proceed db changes in auto mode
        $xdb = $data['xdb'];
        $db = $this->adapter;
        $mode = !empty($data['mode']) ? (int)$data['mode'] : 0;
        $check_mode = $mode == 3 || $mode == 4 ? 0 : 1;
        $xpress_id = (int)$data['xpress_id'];
        $nx_user_id = !empty($data['nx_user_id']) ? (int)$data['nx_user_id'] : 0;
        $subscription_link_id = !empty($data['subscription_link_id']) ? (int)$data['subscription_link_id'] : 0;
        $person_link_id = !empty($data['person_link_id']) ? (int)$data['person_link_id'] : 0;
        $company_link_id = !empty($data['company_link_id']) ? (int)$data['company_link_id'] : 0;

        $is_enable = 1;
        $is_auto_link_user = 0;

        $is_just_link_subscription = !empty($data['is_just_link_subscription']) ? (int)$data['is_just_link_subscription'] : 0;//is just save xpress_id to nx?
        $is_just_link_person = !empty($data['is_just_link_person']) ? (int)$data['is_just_link_person'] : 0;
        $is_just_link_company = !empty($data['is_just_link_company']) ? (int)$data['is_just_link_company'] : 0;
        $is_auto_mode = $mode == 4 ? 1 : 0;

        $warnings = [];

        if (empty($xdb) || empty($xpress_id)) {
            throw new Exception('Отсутствуют входные данные.');
        }

        $sql = new Sql($xdb);
        $select = $sql->select()->from(['S' => 'subscription'])->where(['S.id' => $xpress_id]);
        $selectString = $sql->buildSqlString($select);
        $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
        $xsubscription = $result->current();

        if (empty($xsubscription)) {
            throw new Exception('В xpress отсутствует подписка с id = ' . $xpress_id);
        }

        $subscriptionData = [];
        $magazinesData = [];
        $personData = [];
        $companyData = [];
        $check_persons = [];
        $check_companies = [];
        $is_lock_person = 0;
        $is_lock_company = 0;
        $person_id = 0;
        $company_id = 0;
        $resultData = [
            'personData' => [],
            'companyData' => [],
            'subscriptionData' => [],
        ];

        $mag_ids = [
            '1' => 7, //мир пк + dvd
            '2' => 1, //computeworld
            '3' => 16, //классный журнал
            '4' => 4, //lan
            '5' => 2, //nets
            '6' => 6, //os
            '7' => 13, //publish
            '9' => 5, //win
            '14' => 15, //lv
            //'19'  => 0, //искусство управления
            '20' => 3, //cio
            //'22'  => 17, //stuff + cd
            //'23'  => 0, //what car
            //'24'  => 0, //загородная жизнь
            //'104' => 0, //книги
            //'106' => 0, //CD сиди и ситай
            //'107' => 0, //CD в компл.
            //'108' => 0, //всемирный следопыт
            //'110' => 0, //комп мир от а до я
            '112' => 17, //stuff
            //'113' => 0, //каталог первые лица
            //'116' => 0, //фсп
            //'117' => 0, //homes & gardens
            //'118' => 7, //pcworld
            //'119' => 0, //champions
            //'122' => 0, //linux format
            //'123' => 0, //cd коференций
            //'124' => 0, //ogj
            //'125' => 0, //от рулона к полотну
            '127' => 18, //понимашка
            '130' => 14 //whf
            //'190' => 16, //Классный журнал + уникальные серии подарков (9 комплектов)
            //'191' => 18, //ПониМашка + уникальные серии развивающих игрушек (9 комплектов)
            //'208' => 16, //Классный журнал + РАЗВИВАЮЩАЯ ИГРА в каждом 4-м номере месяца
            //'209' => 18 //ПониМашка + РАЗВИВАЮЩАЯ ИГРА в каждом 4-м номере месяца
        ];

        $payment_ids = [
            '0' => 0, //не оплачивается
            '1' => 1, //банк -> банк
            '2' => 3 //нал -> нал
        ];

        $payment_agent_ids = [
            '0' => 0,
            '1' => 1,  //осп-курьер
            '2' => 2,  //зао ос,
            '9' => 9,  //мир пк
            '15' => 15, //интуит
            '47' => 47  //ооо ос
        ];

        $country_ids = [
            '0' => 165,
            '1' => 165,//РФ
            '9' => 2,//Австрия
            '5' => 3,//Азербайджан
            '4' => 12,//Армения
            '3' => 19,//Беларусь
            '20' => 21,//Бельгия
            '21' => 36,//Великобритания
            '25' => 37,//Венгри
            '24' => 53,//Германия
            '22' => 139,//Голландия -> Нидерланды
            '10' => 60,//Грузия
            '6' => 83,//Казахстан
            '19' => 92,//Киргизия
            '7' => 92,//Кыргызстан
            '11' => 105,//Латвия
            '17' => 110,//Литва
            '8' => 129,//Молдова
            '39' => 144,//Норвегия
            '29' => 91,//Республика Кипр
            '38' => 165,//Российская Федерация
            '35' => 165,//Россия
            '15' => 195,//Таджикистан
            '23' => 205,//Туркменистан
            '12' => 208,//Узбекистан
            '2' => 209,//Украина
            '18' => 233,//Эстония
            '31' => 27,//BOSNIA AND HERZEGOWINA
            '33' => 87,//CANADA
            '30' => 217,//FINLAND
            '27' => 220,//FRANCE
            '32' => 72,//ISRAEL
            '26' => 159,//POLAND
            '28' => 175,//SAUDI ARABIA
            '40' => 227,//SWEDEN
            '16' => 189//USA
        ];

        $delivery_ids = [
            '10' => 3,//курьер
            '50' => 3,//4,//курьер-Электрический
            //'12' => 6,//машина
            '14' => 2,//офис
            '20' => 1,//7,//почта гор
            '6' => 1,//8,//почта ДЗ
            '43' => 9,//почта ДЗ авиа
            '5' => 1,//10,//почта РФ
            '18' => 1,//11,//почта СНГ
            '52' => 5 //Электронная доставка
        ];

        if (!isset($payment_ids[$xsubscription['payment']])) {
            throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' отсутствует способ оплаты в системе nx.');
        }

        if (!isset($payment_agent_ids[$xsubscription['comagent']])) {
            throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' отсутствует платежный агент в системе nx.');
        }

        if (empty($delivery_ids[$xsubscription['delivery']])) {
            throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' отсутствует способ доставки в системе nx.');
        }

        $sql = new Sql($xdb);
        $select = $sql->select()->from(['I' => 'subscription_item'])->where(['I.subscription' => $xsubscription['id']]);
        $selectString = $sql->buildSqlString($select);
        $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
        $xitems = $result->toArray();

        if (empty($xitems)) {
            throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' отсутствуют позиции.');
        }

        //check for magazine exist in nx system
        $check = 1;
        foreach ($xitems as $xitem) {
            if (empty($mag_ids[$xitem['issue']])) {
                $check = 0;
                break;
            }
        }
        if (empty($check)) {
            throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' отсутствует такое издание в системе nx.');
        }

        if ($xsubscription['customer_class'] == 1 && !empty($xsubscription['customer_id'])) {//only person
            $sql = new Sql($xdb);
            $select = $sql->select()->from(['P' => 'person'])->where(['P.id' => $xsubscription['customer_id']]);
            $selectString = $sql->buildSqlString($select);
            $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
            $xperson = $result->current();

            if (!empty($xperson)) {
                //!!!check for person existence!!!
                if (!empty($xperson['email'])) {
                    $emails = explode(';', $xperson['email']);
                    if (count($emails) == 1) {
                        $emails = explode(',', $xperson['email']);
                    }
                    if (count($emails) > 1) {
                        $emails[] = $xperson['email'];
                    }
                    $emails = array_map('trim', $emails);
                    if (!empty($xperson['name'])) {
                        $check_person = $this->personModel->getPersons(['name' => $xperson['name'], 'email' => $emails])->toArray();
                        if (!empty($check_person)) {
                            $is_lock_person = 1;
                            $check_persons[] = $check_person;
                        }
                    }
                    if (!$is_lock_person) {
                        $check_persons[] = $this->personModel->getPersons(['email' => $emails])->toArray();
                    }
                }
                if (!$is_lock_person) {
                    $check_persons[] = $this->personModel->getPersons(['name' => $xperson['name']])->toArray();
                    $check_persons[] = $this->personModel->getPersons(['xpress_id' => $xperson['id'], 'xpress_type' => 'person'])->toArray();
                }

                $addressData = [
                    'country_id' => 0,
                    'area' => '',
                    'region' => '',
                    'city' => '',
                    'zipcode' => '',
                    'address' => '',
                    'phone' => '',
                    'fax' => '',
                    'comment' => '',
                    'telcode' => '',
                    'label' => '',
                    'xpress_id' => 0
                ];
                if (!empty($xperson['address'])) {
                    $sql = new Sql($xdb);
                    $select = $sql->select()->from(['A' => 'address'])->where(['A.id' => $xperson['address']]);
                    $selectString = $sql->buildSqlString($select);
                    $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
                    $xaddress = $result->current();
                    if (!empty($xaddress)) {
                        if (!empty($xaddress['country']) && empty($country_ids[$xaddress['country']])) {
                            throw new Exception('Для адреса физ.лица подписки в xpress с id = ' . $xpress_id . ' отсутствует страна в системе nx.');
                        }
                        $addressData = [
                            'country_id' => $country_ids[$xaddress['country']],
                            'area' => $xaddress['area'],
                            'region' => $xaddress['region'],
                            'city' => $xaddress['city'],
                            'zipcode' => $xaddress['zipcode'],
                            'address' => $xaddress['address'],
                            'phone' => $xaddress['phone'],
                            'fax' => $xaddress['fax'],
                            'comment' => $xaddress['comment'],
                            'telcode' => $xaddress['telcode'],
                            'label' => 'xpress_person',
                            'xpress_id' => $xaddress['id']
                        ];
                    }
                }

                $gender = ($xperson['gender'] == 'm' ? 1 : ($xperson['gender'] == 'f' ? 0 : ''));
                $personData = [
                    'company_id' => 0,
                    'position' => '',
                    'position_type_id' => 0,
                    'speciality' => '',
                    'phone' => '',
                    'telcode' => '',
                    //'address_id'     => $address_id,
                    'email' => trim($xperson['email']),
                    'channel_id' => $xperson['channel'],//!!!move channel list from xpress to nx!!!
                    'source_id' => $xperson['source'],//!!!move source list from xpress to nx!!!
                    'name' => $xperson['name'],
                    'f' => $xperson['f'],
                    'i' => $xperson['i'],
                    'o' => $xperson['o'],
                    'gender' => $gender,
                    'checked' => 1,
                    'comment' => !empty($xperson['comment']) ? $xperson['comment'] : '',
                    'created' => date('Y-m-d H:i:s'),//$xperson['ctime'],
                    'create_user_id' => $nx_user_id,
                    'label' => 'xpress_subscription',
                    'xpress_id' => $xperson['id'],
                    'xpress_type' => 'person'
                ];

                $resultData['personData'] = ['personData' => $personData, 'addressData' => $addressData];
            }
        }

        if (($xsubscription['customer_class'] == 2 || $xsubscription['customer_class'] == 3) && !empty($xsubscription['company'])) {//company
            $sql = new Sql($xdb);
            $select = $sql->select()->from(['C' => 'company'])->where(['C.id' => $xsubscription['company']]);
            $selectString = $sql->buildSqlString($select);
            $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
            $xcompany = $result->current();

            if (!empty($xcompany)) {
                $requisition = [];
                if (!empty($xcompany['pay_property'])) {
                    $sql = new Sql($xdb);
                    $select = $sql->select()->from(['P' => 'pay_property'])->where(['P.id' => $xcompany['pay_property']]);
                    $selectString = $sql->buildSqlString($select);
                    $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
                    $requisition = $result->current();
                }

                //!!!check for company existence!!!
                if (!empty($xcompany['name'])) {
                    if (!empty($requisition['inn'])) {
                        $check_company = $this->companyModel->getCompanies(['name_to_sorthash' => $xcompany['name'], 'inn' => $requisition['inn']]);
                        if ($check_company->count()) {
                            $is_lock_company = 1;
                            $check_companies[] = $check_company->toArray();
                        }
                    }
                    if (!$is_lock_company) {
                        $check_company = $this->companyModel->getCompanies(['name_to_sorthash' => $xcompany['name']]);
                        if ($check_company->count()) {
                            //$is_lock_company   = 1;
                            $check_companies[] = $check_company->toArray();
                        }
                        $check_companies[] = $this->companyModel->getCompanies(['xpress_id' => $xcompany['id']])->toArray();
                    }
                }

                $addressData = [
                    'country_id' => 0,
                    'area' => '',
                    'region' => '',
                    'city' => '',
                    'zipcode' => '',
                    'address' => '',
                    'phone' => '',
                    'fax' => '',
                    'comment' => '',
                    'telcode' => '',
                    'label' => '',
                    'xpress_id' => 0
                ];
                if (!empty($xcompany['address'])) {
                    $sql = new Sql($xdb);
                    $select = $sql->select()->from(['A' => 'address'])->where(['A.id' => $xcompany['address']]);
                    $selectString = $sql->buildSqlString($select);
                    $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
                    $xaddress = $result->current();
                    if (!empty($xaddress)) {
                        if (!empty($xaddress['country']) && empty($country_ids[$xaddress['country']])) {
                            throw new Exception('Для адреса организации подписки в xpress с id = ' . $xpress_id . ' отсутствует страна в системе nx.');
                        }
                        $addressData = [
                            'country_id' => $country_ids[$xaddress['country']],
                            'area' => $xaddress['area'],
                            'region' => $xaddress['region'],
                            'city' => $xaddress['city'],
                            'zipcode' => $xaddress['zipcode'],
                            'address' => $xaddress['address'],
                            'phone' => $xaddress['phone'],
                            'fax' => $xaddress['fax'],
                            'comment' => $xaddress['comment'],
                            'telcode' => $xaddress['telcode'],
                            'label' => 'xpress_company',
                            'xpress_id' => $xaddress['id']
                        ];
                    }
                }

                $legalAddressData = [
                    'country_id' => 0,
                    'area' => '',
                    'region' => '',
                    'city' => '',
                    'zipcode' => '',
                    'address' => '',
                    'phone' => '',
                    'fax' => '',
                    'comment' => '',
                    'telcode' => '',
                    'label' => '',
                    'xpress_id' => 0
                ];
                if (!empty($xcompany['legal_address'])) {
                    $sql = new Sql($xdb);
                    $select = $sql->select()->from(['A' => 'address'])->where(['A.id' => $xcompany['legal_address']]);
                    $selectString = $sql->buildSqlString($select);
                    $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
                    $xaddress = $result->current();
                    if (!empty($xaddress)) {
                        if (!empty($xaddress['country']) && empty($country_ids[$xaddress['country']])) {
                            throw new Exception('Для юр. адреса организации подписки в xpress с id = ' . $xpress_id . ' отсутствует страна в системе nx.');
                        }
                        $legalAddressData = [
                            'country_id' => $country_ids[$xaddress['country']],
                            'area' => $xaddress['area'],
                            'region' => $xaddress['region'],
                            'city' => $xaddress['city'],
                            'zipcode' => $xaddress['zipcode'],
                            'address' => $xaddress['address'],
                            'phone' => $xaddress['phone'],
                            'fax' => $xaddress['fax'],
                            'comment' => $xaddress['comment'],
                            'telcode' => $xaddress['telcode'],
                            'label' => 'xpress_company',
                            'xpress_id' => $xaddress['id']
                        ];
                    }
                }

                $sorthash = \nx\Model\Company::createSortHash($xcompany['name']);
                $companyData = [
                    'name' => strtr($xcompany['name'], ['{' => '"', '}' => '"']),
                    'source_id' => $xcompany['source'],//!!!move source list from xpress to nx!!!
                    'channel_id' => $xcompany['channel'],//!!!move channel list from xpress to nx!!!
                    'org_form' => $xcompany['orgform'],
                    //'address_id'        => $address_id,
                    //'legal_address_id'  => $legal_address_id,
                    'contact_person_id' => 0,
                    'email' => trim($xcompany['email']),
                    'url' => $xcompany['www'],
                    'description' => '',
                    'shortname' => '',
                    'othernames' => '',//!!!get from xpress!!!???
                    'wrongnames' => '',
                    //'sorthash'          => $sorthash,
                    'inn' => !empty($requisition['inn']) ? $requisition['inn'] : '',
                    'okonh' => !empty($requisition['okonh']) ? $requisition['okonh'] : '',
                    'okpo' => !empty($requisition['okpo']) ? $requisition['okpo'] : '',
                    'pay_account' => !empty($requisition['sAccount']) ? $requisition['sAccount'] : '',
                    'corr_account' => !empty($requisition['cAccount']) ? $requisition['cAccount'] : '',
                    'kpp' => !empty($requisition['kpp']) ? $requisition['kpp'] : '',
                    'bik' => !empty($requisition['BIK']) ? $requisition['BIK'] : '',
                    'bank' => !empty($requisition['bank']) ? $requisition['bank'] : '',
                    'label' => 'xpress_subscription',
                    'xpressid' => $xcompany['id'],
                    'checked' => $xcompany['active'],
                    'created' => date('Y-m-d H:i:s'),//$xcompany['ctime'],
                    'create_user_id' => $nx_user_id
                ];

                $resultData['companyData'] = ['companyData' => $companyData, 'addressData' => $addressData, 'legalAddressData' => $legalAddressData];
            }
        }

        if ($xsubscription['customer_class'] == 3 && (!empty($xsubscription['customer_id']) || !empty($xsubscription['company']))) {//person and company
            $sql = new Sql($xdb);
            $select = $sql->select()->from(['P' => 'employee'])->where(['P.id' => $xsubscription['customer_id']]);
            $selectString = $sql->buildSqlString($select);
            $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
            $xperson = $result->current();

            if (!empty($xperson)) {
                //!!!check for person existence!!!
                if (!empty($xperson['email'])) {
                    $emails = explode(';', $xperson['email']);
                    if (count($emails) == 1) {
                        $emails = explode(',', $xperson['email']);
                    }
                    if (count($emails) > 1) {
                        $emails[] = $xperson['email'];
                    }
                    $emails = array_map('trim', $emails);
                    if (!empty($xperson['name'])) {
                        $check_person = $this->personModel->getPersons(['name' => $xperson['name'], 'email' => $emails])->toArray();
                        if (!empty($check_person)) {
                            $is_lock_person = 1;
                            $check_persons[] = $check_person;
                        }
                    }
                    if (!$is_lock_person) {
                        $check_persons[] = $this->personModel->getPersons(['email' => $emails])->toArray();
                    }
                }
                if (!$is_lock_person) {
                    $check_persons[] = $this->personModel->getPersons(['name' => $xperson['name']])->toArray();
                    $check_persons[] = $this->personModel->getPersons(['xpress_id' => $xperson['id'], 'xpress_type' => 'employee'])->toArray();
                }
                $addressData = [
                    'country_id' => 0,
                    'area' => '',
                    'region' => '',
                    'city' => '',
                    'zipcode' => '',
                    'address' => '',
                    'phone' => '',
                    'fax' => '',
                    'comment' => '',
                    'telcode' => '',
                    'label' => '',
                    'xpress_id' => 0
                ];
                $gender = ($xperson['gender'] == 'm' ? 1 : ($xperson['gender'] == 'f' ? 0 : ''));
                $personData = [
                    'company_id' => $company_id,
                    'position' => $xperson['position'],
                    'position_type_id' => $xperson['ptype'],//!!!move position_type list from xpress to nx???
                    'speciality' => $xperson['speciality'],
                    'phone' => $xperson['phone'],
                    'telcode' => $xperson['telcode'],
                    'address_id' => 0,
                    'email' => trim($xperson['email']),
                    'channel_id' => $xperson['channel'],//!!!move channel list from xpress to nx!!!
                    'source_id' => $xperson['source'],//!!!move source list from xpress to nx!!!
                    'name' => $xperson['name'],
                    'f' => $xperson['f'],
                    'i' => $xperson['i'],
                    'o' => $xperson['o'],
                    'gender' => $gender,
                    'checked' => $xperson['active'],
                    'comment' => !empty($xperson['note']) ? $xperson['note'] : '',
                    'created' => date('Y-m-d H:i:s'),//$xperson['ctime'],
                    'create_user_id' => $nx_user_id,
                    'label' => 'xpress_subscription',
                    'xpress_id' => $xperson['id'],
                    'xpress_type' => 'employee'
                ];

                $resultData['personData'] = ['personData' => $personData, 'addressData' => $addressData];
            }
        }

        $check_person_ids = [];
        $check_company_ids = [];
        foreach ($check_persons as $persons) {
            if (!empty($persons)) {
                foreach ($persons as $person) {
                    if (!in_array($person['id'], $check_person_ids)) {
                        $check_person_ids[] = $person['id'];
                    }
                }
            }
        }
        foreach ($check_companies as $companies) {
            if (!empty($companies)) {
                foreach ($companies as $company) {
                    if (!in_array($company['id'], $check_company_ids)) {
                        $check_company_ids[] = $company['id'];
                    }
                }
            }
        }

        $subscriber_type_id = ($xsubscription['customer_class'] == 3 || $xsubscription['customer_class'] == 2) ? 2 : 1;
        $payment_type_id = $payment_ids[$xsubscription['payment']];
        $order_state_id = ((float)$xsubscription['payment_sum'] >= (float)$xsubscription['total']) ? 3 : 2;// || in_array($xsubscription['type'], array('-1','1')) ? 3 : 2;
        $user_id = 0;
        //$email              = '';
        $extra_emails = '';
        $email = !empty($personData['email']) ? $personData['email'] : (!empty($companyData['email']) ? $companyData['email'] : '');
        if ($email) {
            $emails = explode(';', $email);
            if (count($emails) == 1) {
                $emails = explode(',', $email);
            }
            $emails = array_map('trim', $emails);
            foreach ($emails as $key => $email) {
                $emails[$key] = filter_var($email, FILTER_SANITIZE_EMAIL);
            };
            $email = $emails[0];
            $extra_emails = implode(';', array_slice($emails, 1));
        }

        if ($email) {
            $sql = new Sql($db);
            $select = $sql->select()->from(['U' => 'User'], ['User_ID AS id'])->where(['U.Email' => $email]);
            $selectString = $sql->buildSqlString($select);
            $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
            $check_user = $result->current();
            if (!empty($check_user['id']) && $is_auto_link_user) {
                $user_id = $check_user['id'];
            }
        }

        $addressData = [
            'country_id' => 0,
            'area' => '',
            'region' => '',
            'city' => '',
            'zipcode' => '',
            'address' => '',
            'phone' => '',
            'fax' => '',
            'comment' => '',
            'telcode' => '',
            'label' => '',
            'xpress_id' => 0
        ];
        if (!empty($xsubscription['address'])) {
            $sql = new Sql($xdb);
            $select = $sql->select()->from(['A' => 'address'])->where(['A.id' => $xsubscription['address']]);
            $selectString = $sql->buildSqlString($select);
            $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
            $xaddress = $result->current();
            if (!empty($xaddress)) {
                if (!empty($xaddress['country']) && empty($country_ids[$xaddress['country']])) {
                    throw new Exception('Для адреса подписки в xpress с id = ' . $xpress_id . ' отсутствует страна в системе nx.');
                }
                $addressData = [
                    'country_id' => $country_ids[$xaddress['country']],
                    'area' => $xaddress['area'],
                    'region' => $xaddress['region'],
                    'city' => $xaddress['city'],
                    'zipcode' => $xaddress['zipcode'],
                    'address' => $xaddress['address'],
                    'phone' => $xaddress['phone'],
                    'fax' => $xaddress['fax'],
                    'comment' => $xaddress['comment'],
                    'telcode' => $xaddress['telcode'],
                    'label' => 'xpress_subscription',
                    'xpress_id' => $xaddress['id']
                ];
            }
        }

        $subscriptionData = [
            'xpress_id' => $xsubscription['id'],
            'label' => 'xpress_subscription',
            'channel_id' => $xsubscription['channel'],//!!!move channel list from xpress to nx!!!
            'price' => $xsubscription['total'],
            'discount' => $xsubscription['discount'],
            'payment_type_id' => $payment_type_id,
            'payment_date' => $xsubscription['payment_date'],
            'payment_docno' => $xsubscription['payment_docno'],
            'payment_agent_id' => $xsubscription['comagent'],//!!!move agent list from xpress to nx!!!
            'payment_sum' => $xsubscription['payment_sum'],
            'comment' => !empty($xsubscription['comment']) ? $xsubscription['comment'] : '',
            'source_id' => $xsubscription['source'],//!!!move source list from xpress to nx!!!
            'type' => $xsubscription['type'],//!!!move type list from xpress to nx!!!
            'enabled' => !$xsubscription['frozen'],
            'order_state_id' => $order_state_id,
            'email' => $email,
            'extra_emails' => $extra_emails,
            //'address_id'         => $address_id,
            'person_id' => $person_id,
            'company_id' => $company_id,
            'user_id' => $user_id,
            'subscriber_type_id' => $subscriber_type_id,
            'created' => date('Y-m-d H:i:s'),//$xsubscription['ctime'],
            'create_user_id' => $nx_user_id,
            'key' => $xsubscription['ext'],//ext//ключ!!!add field to nx!!!
            'reason' => $xsubscription['name']//name//основание!!!add field to nx!!!
        ];

        $magazinesData = [];
        foreach ($xitems as $xitem) {
            $sql = new Sql($xdb);
            $select = $sql->select()->from(['R' => 'release'], ['id'])
                ->where('R.num <= "' . $xitem['bnum'] . '"')
                ->where('R.year = "' . $xitem['byear'] . '"')
                ->where('R.issue = "' . $xitem['issue'] . '"')
                ->order('R.num ASC');
            $selectString = $sql->buildSqlString($select);
            $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
            $xitem_releases = $result->toArray();
            if (empty($xitem_releases)) {
                throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' отсутствует начальный выпуск.');
            }
            $bnum = count($xitem_releases);

            $sql = new Sql($xdb);
            $select = $sql->select()->from(['IR' => 'subscription_item_release'])->join(['R' => 'release'], 'R.id = IR.release', ['release_date', 'year', 'relmonth'])
                //->where('R.release_date IS NOT NULL')
                ->where('R.relmonth<>0')
                ->where('R.year<>"0000"')
                ->where(['IR.parent' => $xitem['id']])->order('R.release_date ASC');
            $selectString = $sql->buildSqlString($select);
            $result = $xdb->query($selectString, $xdb::QUERY_MODE_EXECUTE);
            $xitem_releases = $result->toArray();

            if (empty($xitem_releases)) {
                throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' отсутствуют выпуски с заполненными годом и месяцем выхода.');
            }
            if (count($xitem_releases) != $xitem['rqty']) {//it can be coz of in xpress rqty means count of abstract numbers but in nx rqty means count of physical issues, so such release as 07-08 for example will be counted in xpress as two, but in nx as one issue
                //throw new \Exception('Для подписки в xpress с id = ' . $xpress_id . ' отсутствуют некоторые выпуски.');
                //$warnings[] = 'Для подписки в xpress с id = ' . $xpress_id . ' отсутствуют некоторые выпуски.';
            }

            $max_stamp = 0;
            foreach ($xitem_releases as $xitem_release) {
                $cur_stamp = strtotime($xitem_release['release_date']);
                if ($cur_stamp > $max_stamp) {
                    $max_stamp = $cur_stamp;
                }
            }
            //active or not subscription? if all releases was released than not, otherwise?
            $enabled = 1;//empty($xsubscription['frozen']) ? 1 : 0; //&& (strtotime($date_end) < time())
            if ($max_stamp < time())// && count($xitem_releases)>=$xitem['rqty'])
            {
                $enabled = 0;
            }

            $delivery_type_id = $delivery_ids[$xsubscription['delivery']];
            $type = $delivery_type_id == 5 ? 'pdf' : 'paper';

            //$dates = $this->getDatesByCalendar($mag_ids[$xitem['issue']], $xitem['byear'], $bnum, $xitem['rqty']);
            //if($dates['date_start']) {
            //    $date_start = $dates['date_start'];
            //}
            //else {
            $date_start = $xitem_releases[0]['year'] . '-' . str_pad($xitem_releases[0]['relmonth'], 2, '0', STR_PAD_LEFT) . '-01';
            //}
            //if($dates['date_end']) {
            //    $date_end = $dates['date_end'];
            //}
            //else
            //{
            $date_end = $xitem_releases[count($xitem_releases) - 1]['year'] . '-' . str_pad($xitem_releases[count($xitem_releases) - 1]['relmonth'], 2, '0', STR_PAD_LEFT) . '-01';
            $end_stamp = strtotime($date_end);
            $end_next_month_stamp = mktime(date('H', $end_stamp), date('i', $end_stamp), date('s', $end_stamp), date('n', $end_stamp) + 1, date('j', $end_stamp), date('Y', $end_stamp));
            $date_end = date('Y', $end_next_month_stamp) . '-' . date('m', $end_next_month_stamp) . '-01';
            //}
            //$d1                   = new \DateTime($date_start);//since php 5.3.0
            //$d2                   = new \DateTime($date_end);
            //$diff                 = $d1->diff($d2);
            //$period               = $diff->y*12 + $diff->m;
            $period = \nx\functions\getMonthsBetweenDates($date_start, $date_end);

            $magazineData = [
                //'subscription_id'  => $subscription_id,
                'periodical_id' => $mag_ids[$xitem['issue']],
                'price' => $xitem['price'],// * $xitem['rqty'],
                'discount' => $xitem['discount'],
                'quantity' => $xitem['qty'],
                'date_start' => $date_start,
                'date_end' => $date_end,
                'completed' => 0,
                'type' => $type,
                'delivery_type_id' => $delivery_type_id,
                'enabled' => $enabled,
                'period' => $period,
                'byear' => $xitem['byear'],
                'bnum' => $bnum,
                'rqty' => $xitem['rqty'],
                'created' => date('Y-m-d H:i:s'),//$xsubscription['ctime'],
                'create_user_id' => $nx_user_id//,
                //'xpress_id'        => $xitem['id']//for item it is not unique, just for info
            ];
            $magazinesData[] = $magazineData;
        }

        $is_active = 0;
        foreach ($magazinesData as $magazineData) {
            if ($magazineData['enabled']) {
                $is_active = 1;
            }
        }
        $subscriptionData['enabled'] = $subscriptionData['enabled'] ? $is_active : 0;//auto enable by checking issues release_date
        if (!$check_mode) {
            $subscriptionData['enabled'] = $subscriptionData['enabled'] ? $is_enable : 0;//manual enable by user
        }
        $resultData['subscriptionData'] = ['subscriptionData' => $subscriptionData, 'addressData' => $addressData, 'magazinesData' => $magazinesData];

        $sql = new Sql($db);
        $select = $sql->select()->from(['O' => 'nx_order'], ['id'])->where(['xpress_id' => $xpress_id]);
        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $check_subscription = $result->toArray();

        $is_lock_subscription = 0;
        $check_subscriptions = [];
        $check_subscriptions_strict = [];
        if (!empty($check_subscription)) {
            $is_lock_subscription = 1;
            $check_subscriptions[] = $check_subscription;
            if (count($check_subscription) > 1) {
                $warnings[] = 'в nx с xpress_id = ' . $xpress_id . ' связано несколько заказов';
            }
        } else {
            foreach ($magazinesData as $magazineData) {
                /*$sql    = new Sql($db);
                $select = $sql->select()
                    ->from(array('O' => 'nx_order'), array('id'))
                    ->join(array('I' => 'nx_order_item'), 'I.order_id = O.id', array())
                    ->where(array('I.xpress_id' => $magazineData['xpress_id']))
                    ->where(array('I.item_type' => 'nx_subscription'))
                    ->group('O.id');
                $selectString        = $sql->$this->buildSqlString($select);
                $result              = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                $check_subscriptions[] = $result->toArray();*/

                if ($subscriptionData['email']) {
                    $emails = [];
                    if (!empty($subscriptionData['extra_emails'])) {
                        $emails = explode(';', $subscriptionData['extra_emails']);
                        $emails = array_map('trim', $emails);
                    }
                    $emails[] = $subscriptionData['email'];

                    $sql = new Sql($db);
                    $select = $sql->select()
                        ->from(['O' => 'nx_order'], ['id'])
                        ->join(['I' => 'nx_order_item'], 'I.order_id = O.id', [])
                        ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', [])
                        ->where(['I.item_type' => 'nx_subscription'])
                        ->group('O.id');
                    $select->where->NEST
                        ->In('O.email', $emails)->AND
                        ->equalTo('S.periodical_id', $magazineData['periodical_id'])->AND
                        ->equalTo('S.byear', $magazineData['byear'])->AND
                        ->between('S.bnum', (int)$magazineData['bnum'] - 1, (int)$magazineData['bnum'] + 1)->AND
                        ->between('S.rqty', (int)$magazineData['rqty'] - 1, (int)$magazineData['rqty'] + 1)->UNNEST;
                    $selectString = $sql->buildSqlString($select);
                    $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                    $check_subscriptions[] = $result->toArray();

                    $sql = new Sql($db);
                    $select = $sql->select()
                        ->from(['O' => 'nx_order'], ['id'])
                        ->join(['I' => 'nx_order_item'], 'I.order_id = O.id', [])
                        ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', [])
                        ->where(['I.item_type' => 'nx_subscription'])
                        ->group('O.id')
                        ->order(['O.order_state_id DESC']);
                    $select->where->NEST
                        ->greaterThanOrEqualTo('O.order_state_id', $subscriptionData['order_state_id'])->AND
                        ->equalTo('O.xpress_id', 0)->AND
                        ->equalTo('O.price', $subscriptionData['price'])->AND
                        ->In('O.email', $emails)->AND
                        ->equalTo('I.quantity', $magazineData['quantity'])->AND
                        ->equalTo('S.periodical_id', $magazineData['periodical_id'])->AND
                        ->equalTo('I.delivery_type_id', $magazineData['delivery_type_id'])->AND
                        ->equalTo('S.byear', $magazineData['byear'])->AND
                        ->equalTo('S.bnum', (int)$magazineData['bnum'])->AND
                        ->equalTo('S.rqty', (int)$magazineData['rqty'])->UNNEST;
                    $selectString = $sql->buildSqlString($select);
                    $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                    $check_subscriptions_strict[] = $result->toArray();
                }

                //if($subscriptionData['person_id'])
                foreach ($check_person_ids as $person_id) {
                    $sql = new Sql($db);
                    $select = $sql->select()
                        ->from(['O' => 'nx_order'], ['id'])
                        ->join(['I' => 'nx_order_item'], 'I.order_id = O.id', [])
                        ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', [])
                        ->where(['I.item_type' => 'nx_subscription'])
                        ->group('O.id');
                    $select->where->NEST
                        ->equalTo('O.person_id', $person_id)->AND
                        ->equalTo('S.periodical_id', $magazineData['periodical_id'])->AND
                        ->equalTo('S.byear', $magazineData['byear'])->AND
                        ->between('S.bnum', (int)$magazineData['bnum'] - 1, (int)$magazineData['bnum'] + 1)->AND
                        ->between('S.rqty', (int)$magazineData['rqty'] - 1, (int)$magazineData['rqty'] + 1)->UNNEST;
                    $selectString = $sql->buildSqlString($select);
                    $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                    $check_subscriptions[] = $result->toArray();
                }

                //if($subscriptionData['company_id'])
                foreach ($check_company_ids as $company_id) {
                    $sql = new Sql($db);
                    $select = $sql->select()
                        ->from(['O' => 'nx_order'], ['id'])
                        ->join(['I' => 'nx_order_item'], 'I.order_id = O.id', [])
                        ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', [])
                        ->where(['I.item_type' => 'nx_subscription'])
                        ->group('O.id');
                    $select->where->NEST
                        ->equalTo('O.company_id', $company_id)->AND
                        ->equalTo('S.periodical_id', $magazineData['periodical_id'])->AND
                        ->equalTo('S.byear', $magazineData['byear'])->AND
                        ->between('S.bnum', (int)$magazineData['bnum'] - 1, (int)$magazineData['bnum'] + 1)->AND
                        ->between('S.rqty', (int)$magazineData['rqty'] - 1, (int)$magazineData['rqty'] + 1)->UNNEST;
                    $selectString = $sql->buildSqlString($select);
                    $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                    $check_subscriptions[] = $result->toArray();
                }

                if (!empty($addressData['address']) && $magazineData['type'] == 'paper') {
                    $sql = new Sql($db);
                    $select = $sql->select()
                        ->from(['O' => 'nx_order'], ['id'])
                        ->join(['I' => 'nx_order_item'], 'I.order_id = O.id', [])
                        ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', [])
                        ->join(['A' => 'nx_address'], 'A.id = O.address_id')
                        ->where(['I.item_type' => 'nx_subscription'])
                        ->group('O.id');
                    $select->where->NEST
                        ->equalTo('A.address', $addressData['address'])->AND
                        ->equalTo('S.type', 'paper')->AND
                        ->equalTo('S.periodical_id', $magazineData['periodical_id'])->AND
                        ->equalTo('S.byear', $magazineData['byear'])->AND
                        ->between('S.bnum', (int)$magazineData['bnum'] - 1, (int)$magazineData['bnum'] + 1)->AND
                        ->between('S.rqty', (int)$magazineData['rqty'] - 1, (int)$magazineData['rqty'] + 1)->UNNEST;
                    $selectString = $sql->buildSqlString($select);
                    $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                    $check_subscriptions[] = $result->toArray();
                }
            }
        }

        $check_subscriptions_ids = [];
        foreach ($check_subscriptions as $subscriptions) {
            if (!empty($subscriptions)) {
                foreach ($subscriptions as $subscription) {
                    if (!in_array($subscription['id'], $check_subscriptions_ids)) {
                        $check_subscriptions_ids[] = $subscription['id'];
                    }
                }
            }
        }
        $check_subscriptions_strict_counts = [];
        foreach ($check_subscriptions_strict as $subscriptions) {
            foreach ($subscriptions as $subscription) {
                if (!isset($check_subscriptions_strict_counts[$subscription['id']])) {
                    $check_subscriptions_strict_counts[$subscription['id']] = 1;
                } else {
                    $check_subscriptions_strict_counts[$subscription['id']]++;
                }
            }
        }
        $check_subscription_strict_id = 0;
        foreach ($check_subscriptions_strict_counts as $subscription_id => $count) {
            if ($count == count($magazinesData)) {
                $check_subscription_strict_id = $subscription_id;
            }
        }

        $auto_action = '';
        if ($is_lock_subscription) {
            $auto_action = 'lock';
            if ($is_auto_mode || $check_mode) {
                $subscription_link_id = $check_subscriptions[0][0]['id'];
            }
        } else {
            if ($check_subscription_strict_id) {
                $auto_action = 'link';
                if ($is_auto_mode || $check_mode) {
                    $is_just_link_subscription = isset($data['is_just_link_subscription']) ? $data['is_just_link_subscription'] : 1;
                    $subscription_link_id = isset($data['subscription_link_id']) ? (int)$data['subscription_link_id'] : $check_subscription_strict_id;
                }
            } else {
                if (empty($check_subscriptions_ids)) {
                    $auto_action = 'add';
                } else {
                    $auto_action = 'manual';
                }
            }
        }

        if ($subscription_link_id) {
            $diff = $this->subscriptionDataDiff($subscription_link_id, $resultData);
            if (!empty($diff)) {
                $warnings[] = 'Различия полей: ' . implode(', ', $diff);
            }
        }

        $check_data = [
            'subscriptions' => !empty($check_subscriptions_ids) ? $check_subscriptions_ids : false,
            'persons' => !empty($check_person_ids) ? $check_person_ids : false,
            'user' => !empty($check_user['id']) ? [$check_user['id']] : false,
            'companies' => !empty($check_company_ids) ? $check_company_ids : false,
            'is_lock_subscription' => $is_lock_subscription,
            'is_lock_person' => $is_lock_person,
            'is_lock_company' => $is_lock_company,
            'action' => $auto_action,
            'subscription_link_id' => $subscription_link_id,
            'comment' => !empty($warnings) ? implode('<br/>', $warnings) : ''
        ];

        if ($check_mode) {
            if ($mode == 1) {
                return $resultData;
            } else if ($mode == 2) {
                return $check_data;
            } else {
                return true;
            }
        } else {
            if ($is_auto_mode && ($auto_action == 'lock' || $auto_action == 'manual' || (!empty($data['do_action']) && $auto_action != $data['do_action']))) {
                //return false;
                return $check_data;
            }
            if ($is_auto_mode) {
                if ($is_lock_person) {
                    $is_just_link_person = isset($data['is_just_link_person']) ? $data['is_just_link_person'] : 0;
                    $person_link_id = isset($data['person_link_id']) ? (int)$data['person_link_id'] : $check_person_ids[0];
                }
                if (!empty($check_company_ids))//$is_lock_company)
                {
                    $is_just_link_company = isset($data['is_just_link_company']) ? $data['is_just_link_company'] : 0;
                    $company_link_id = isset($data['company_link_id']) ? (int)$data['company_link_id'] : $check_company_ids[0];
                }
            }

            $this->adapter->isTransactional = 0;
            $this->adapter->getDriver()->getConnection()->beginTransaction();

            try {
                if (!$is_just_link_subscription || !$subscription_link_id) {
                    if (!empty($resultData['personData'])) {
                        if ($person_link_id && in_array($person_link_id, $check_person_ids))//&& $is_update_person)
                        {
                            if ($is_just_link_person) {
                                $person_id = $person_link_id;
                            } else {
                                $resultData['personData']['personData']['last_user_id'] = $resultData['personData']['personData']['create_user_id'];
                                unset($resultData['personData']['personData']['create_user_id']);
                                $person_id = $this->personModel->updatePerson($person_link_id, ['personData' => $resultData['personData']['personData'], 'addressData' => $resultData['personData']['addressData']]);
                            }
                        } else {
                            if ($is_lock_person) {
                                throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' в nx уже присутствует физ. лицо - допустимо только обновление.');
                            } else {
                                $person_id = $this->personModel->addPerson(['personData' => $resultData['personData']['personData'], 'addressData' => $resultData['personData']['addressData']]);
                            }
                        }
                        $resultData['subscriptionData']['subscriptionData']['person_id'] = $person_id;
                    }

                    if (!empty($resultData['companyData'])) {
                        if ($company_link_id && in_array($company_link_id, $check_company_ids))//$is_update_company)
                        {
                            if ($is_just_link_company) {
                                $company_id = $company_link_id;
                            } else {
                                $resultData['companyData']['companyData']['last_user_id'] = $resultData['companyData']['companyData']['create_user_id'];
                                unset($resultData['companyData']['companyData']['create_user_id']);
                                $company_id = $this->companyModel->updateCompany($company_link_id, ['companyData' => $resultData['companyData']['companyData'], 'addressData' => $resultData['companyData']['addressData'], 'legalAddressData' => $resultData['companyData']['legalAddressData']]);
                            }
                        } else {
                            if ($is_lock_company) {
                                throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' в nx уже присутствует организация - допустимо только обновление.');
                            } else {
                                $company_id = $this->companyModel->addCompany(['companyData' => $resultData['companyData']['companyData'], 'addressData' => $resultData['companyData']['addressData'], 'legalAddressData' => $resultData['companyData']['legalAddressData']]);
                            }
                        }
                        $resultData['subscriptionData']['subscriptionData']['company_id'] = $company_id;
                    }

                    if (!$person_id && !$company_id) {
                        throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' отсутствует подписчик.');
                    }
                }

                if ($subscription_link_id && in_array($subscription_link_id, $check_subscriptions_ids)) //&& $is_update_subscription)
                {
                    $resultData['subscriptionData']['subscriptionData']['last_user_id'] = $resultData['subscriptionData']['subscriptionData']['create_user_id'];
                    unset($resultData['subscriptionData']['subscriptionData']['create_user_id']);
                    if ($is_just_link_subscription) {
                        $resultData['subscriptionData'] = ['subscriptionData' => ['xpress_id' => $resultData['subscriptionData']['subscriptionData']['xpress_id']]];
                        $subscription_id = $this->updateSubscription($subscription_link_id, ['subscriptionData' => $resultData['subscriptionData']['subscriptionData']]);
                    } else {
                        $subscription_id = $this->updateSubscription($subscription_link_id, ['subscriptionData' => $resultData['subscriptionData']['subscriptionData'], 'addressData' => $resultData['subscriptionData']['addressData'], 'magazinesData' => ['magazines' => $resultData['subscriptionData']['magazinesData']]]);
                    }
                } else {
                    if ($is_lock_subscription) {
                        throw new Exception('Для подписки в xpress с id = ' . $xpress_id . ' в nx уже присутствует подписка - допустимо только обновление.');
                    } else {
                        //$this->throwExceptions = 0;
                        $subscription_id = $this->addSubscription(['subscriptionData' => $resultData['subscriptionData']['subscriptionData'], 'addressData' => $resultData['subscriptionData']['addressData'], 'magazinesData' => ['magazines' => $resultData['subscriptionData']['magazinesData']]]);
                        //$this->throwExceptions = 1;
                    }
                }

                $this->logger->log('Импорт подписки из xpress(#' . $xpress_id . ') #' . $subscription_id, 'import_subscription', $resultData, $nx_user_id);
                $this->adapter->getDriver()->getConnection()->commit();
                $this->adapter->isTransactional = 1;
                //return $subscription_id;
                return array_merge($check_data, [
                        'subscription_id' => $subscription_id,
                        'action' => 'lock',
                        'subscription_link_id' => $subscription_id
                    ]
                );
            } catch (Exception $e) {
                $this->adapter->getDriver()->getConnection()->rollback();
                $this->adapter->isTransactional = 1;
                throw $e;
                return false;
            }
        }
    }

    public function subscriptionDataDiff($subscription_id, $compare_data)
    {
        $result = [];
        $subscription = $this->getBy(['id' => $subscription_id]);
        if (empty($subscription)) {
            return $result;
        }
        $subscription = $subscription->toArray();
        //$notchecked_fields = array('label', 'created', 'last_updated', 'create_user_id', 'last_user_id', 'xpress_id', 'comment', 'address_id', 'extra_emails');
        $subscription_check_fields = [
            //'xpress_id',
            //'channel_id',
            'price',
            'discount',
            'payment_type_id',
            'payment_date',
            'payment_docno',
            'payment_agent_id',
            'payment_sum',
            //'source_id',
            'type',
            'enabled',
            'order_state_id',
            'email',
            'subscriber_type_id',
            //'key',
            //'reason'
        ];
        $address_check_field = ['address', 'city', 'country_id', 'zipcode', 'area', 'region'];
        foreach ($subscription as $field => $value) {
            $value = trim($value);
            $subscription_field_value = !empty($compare_data['subscriptionData']['subscriptionData'][$field]) ? trim($compare_data['subscriptionData']['subscriptionData'][$field]) : '';
            $address_field_value = !empty($compare_data['subscriptionData']['addressData'][$field]) ? trim($compare_data['subscriptionData']['addressData'][$field]) : '';
            if (in_array($field, $subscription_check_fields) && $subscription_field_value && $subscription_field_value != $value) {
                $result[] = !empty(\nx\Entity\SubscriptionOrder::$fields_names[$field]) ? \nx\Entity\SubscriptionOrder::$fields_names[$field] : $field;
            }
            if (in_array($field, $address_check_field) && $address_field_value && $address_field_value != $value) {
                $result[] = !empty(\nx\Entity\Address::$fields_names[$field]) ? \nx\Entity\Address::$fields_names[$field] : $field;
            }
        }

        $items = $this->getItems(['subscription_id' => $subscription_id]);
        if (empty($items)) {
            $result[] = '<span style="color: red;">пустой nx заказ</span>';
        } else {
            $item_check_fields = ['periodical_id', 'byear', 'bnum', 'rqty', 'quantity'];
            foreach ($items as $item) {
                $find = 0;
                foreach ($compare_data['subscriptionData']['magazinesData'] as $magazine) {
                    $check = 1;
                    foreach ($item_check_fields as $field) {
                        /*if($field == 'delivery_type_id' && $magazine[$field] == 1 && !in_array($item[$field], array(1,7,8,10,11)))
                        {
                            $check = 0;
                        }
                        else
                        {*/
                        if ($magazine[$field] != $item[$field]) {
                            $check = 0;
                        }
                        //}
                    }
                    if ($check) {
                        $find = 1;
                        break;
                    }
                }
                if (!$find) {
                    $result[] = '<span style="color: red;">позиции</span>';
                    break;
                }
            }
        }
        //$xpress_items = $dbModel->xpressModel->getItemsBy(array('subscription_id' => $xpress_subscription['id']));

        return $result;
    }

    public function getBy(array $params)
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        $select = $sql->select()
            ->from(['O' => $this->table])
            ->columns([
                'id',
                'price',
                'payment_type_id',
                'payment_agent_id',
                'payment_date',
                'payment_docno',
                'payment_sum',
                'email',
                'extra_emails',
                'order_state_id',
                'enabled',
                'address_id',
                'map_address_id',
                'company_id',
                'comment',
                'person_id',
                'created',
                'last_updated',
                'user_id',
                'label',
                'hash',
                'action_id',
                'action_code',
                'subscriber_type_id',
                'type',
                'xpress_id',
                'manager_id',
                'new_state'

            ])
            ->join(['A' => 'nx_address'], 'A.id = O.address_id', ['phone', 'fax', 'address', 'zipcode', 'country_id', 'area', 'region', 'address_comment' => 'comment', 'city', 'city1', 'street', 'house', 'building', 'structure', 'apartment', 'telcode'], 'left')
            ->join(['A2' => 'nx_address'], 'A2.id = O.map_address_id', [
                'map_zipcode' => 'zipcode',
                'map_area' => 'area',
                'map_region' => 'region',
                'map_city' => 'city',
                'map_city1' => 'city1',
                'map_street' => 'street',
                'map_house' => 'house',
                'map_building' => 'building',
                'map_structure' => 'structure',
                'map_apartment' => 'apartment',
                'map_is_onclaim' => 'is_onclaim',
                'map_post_box' => 'post_box',
                'map_is_enveloped' => 'is_enveloped',
                'map_address_comment' => 'comment'
            ], 'left')
            ->join(['P' => 'nx_person'], 'P.id = O.person_id', ['person_name' => 'name', 'person_id_name' => new Expression('CONCAT(P.id,". ",P.name)')], 'left')
            ->join(['C' => 'nx_company'], 'C.id = O.company_id', ['company_name' => 'name', 'company_id_name' => new Expression('CONCAT(C.id,". ",C.name)'), 'company_inn' => 'inn', 'company_kpp' => 'kpp'], 'left')
            ->join(['U' => 'User'], 'U.User_ID = O.create_user_id', ['create_user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')], 'left')
            ->join(['U2' => 'User'], 'U2.User_ID = O.last_user_id', ['last_user_name' => new Expression('IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')], 'left')
            ->join(['U3' => 'User'], 'U3.User_ID = O.user_id', ['user_name' => new Expression('CONCAT(U3.User_ID, ". ", IF(U3.LastName<>"", CONCAT_WS(" ", U3.LastName, U3.FirstName, U3.MidName), U3.Login))')], 'left')
            ->join(['ACT' => 'Message414'], 'ACT.Message_ID = O.action_id', ['action_id_name' => new Expression('CONCAT_WS(". ", ACT.Message_ID, ACT.Name)')], 'left')
            ->join(['OS' => 'nx_order_state'], 'OS.id = O.order_state_id', ['order_state_name' => 'name'])
            ->join(['PT' => 'nx_payment_type'], 'PT.id = O.payment_type_id', ['payment_type_name' => 'name'])
            ->join(['PA' => 'nx_payment_agent'], 'PA.id = O.payment_agent_id', ['payment_agent_ws1c_id' => 'ws1c_id'], 'left')
            ->join(['I' => 'nx_order_item'], 'I.order_id = O.id', ['order_item_id' => 'id'])
            ->where(['I.item_type' => 'nx_subscription'])
            ->group('O.id')
            ->limit(1);

        if ($this->publisher_id) {
            $select
                ->join(['PR' => 'nx_price'], 'PR.id = O.price_id', [])
                ->where(['PR.publisher_id' => $this->publisher_id]);
        } else {
            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }


        if (!empty($params['xpress_id'])) {
            $select->where(['O.xpress_id' => (int)$params['xpress_id']]);
        }
        if (!empty($params['id'])) {
            $select->where(['O.id' => (int)$params['id']]);
        }
        if (!empty($params['not_id'])) {
            $select->where->notEqualTo('O.id', $params['not_id']);
        }
        if (!empty($params['email'])) {
            $select->where(['O.email' => $params['email']]);
        }
        if (!empty($params['person_id'])) {
            $select->where(['O.person_id' => (int)$params['person_id']]);
        }
        if (!empty($params['company_id'])) {
            $select->where(['O.company_id' => (int)$params['company_id']]);
        }
        if (!empty($params['order_state_id'])) {
            $select->where(['O.order_state_id' => (int)$params['order_state_id']]);
        }
        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function getItems($params)
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        //$db->query('SET @osp_row_number = 0;', Adapter::QUERY_MODE_EXECUTE);

        /*$select1 = $sql->select()
            ->from(array('MI3'=>'Issue'))
            ->columns(array(
                'release_month',
                'year'
            ))
            ->where('MI3.year >= I.byear')
            ->where('MI3.MagazineID = I.periodical_id')
            ->group(new Expression('CONCAT(MI3.year,MI3.Number)'))
            ->order(array('MI3.year', 'MI3.Number'));

        $select2 = $sql->select()
                ->from(array('MI2' => $select1))
                ->columns(array(
                        'release_month',
                        'osp_row_number' => new Expression('@osp_row_number := @osp_row_number + 1')
                    )
                )
                ->having('CAST(CONCAT(MI2.year, osp_row_number) AS UNSIGNED) >= CONCAT(I.byear, I.bnum)')
                ->order(array('osp_row_number ASC'))
                ->limit('I.rqty');*/

        $select = $sql->select()
            ->from(['I' => 'nx_order_item'])
            ->columns([
                'id',
                'quantity',
                'price',
                'discount',
                'delivery_type_id'
            ])
            ->join(['S' => 'nx_subscription'], 'S.id = I.item_id', ['periodical_id', 'date_start', 'date_end', 'period', 'byear', 'bnum', 'rqty', 'type'])
            ->join(['O' => 'nx_order'], 'O.id = I.order_id', [])
            ->join(['DT' => 'nx_delivery_type'], 'DT.id = I.delivery_type_id', ['delivery_type_name' => 'name'])
            ->join(['M' => 'Edition'], 'M.Message_ID = S.periodical_id', ['periodical_name' => 'HumanizedName', 'EnglishName' => 'EnglishName'])
            //->join(array('MI'=>'Issue'), '(MI.MagazineID = I.periodical_id AND MI.year=I.byear AND MI.Number=I.bnum)', array('bmonth' => 'release_month'), 'left')
            //->join(array('MI'=>$select2), 'MI.osp_row_number = I.bnum', array('bmonth' => 'release_month'), 'left')
            ->where(['O.id' => (int)$params['subscription_id']])
            ->where(['I.item_type' => 'nx_subscription'])
            ->order('I.id ASC');

        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $items = $result->toArray();

        if (!empty($items)) {
            foreach ($items as $i => $item) {//remove H:i:s part
                $item['date_start'] = date('Y-m-d', strtotime($item['date_start']));
                $item['name'] = 'Подписка на ' . ($item['type'] == 'pdf' ? 'электронную' : 'бумажную') . ' версию журнала «' . $item['periodical_name'] . '» (' . $item['period'] . ' ' . \nx\functions\getPlural((int)$item['period'], 'месяц', 'месяца', 'месяцев') . ' - с ' . date('d.m.Y', strtotime($item['date_start'])) . '); всего - ' . $item['rqty'] . ' ' . \nx\functions\getPlural($item['rqty'], 'номер', 'номера', 'номеров') . '.';
                $item['unit_price'] = round($item['price'] * (1 - $item['discount'] / 100.0), 2);
                $item['rqty_price'] = $item['price'] * $item['rqty'];
                $item['quantity_price'] = $item['rqty_price'] * $item['quantity'];
                $item['discount_price'] = round($item['rqty_price'] * (1 - $item['discount'] / 100.0), 2);
                $item['full_price'] = $item['discount_price'] * $item['quantity'];
                $items[$i] = $item;
            }
        }

        return $items;
    }

    public function updateSubscription($id, $data)
    {
        if (empty($id) || empty($data)) {
            return false;
        }
        $data = applyFunctionToData($data, 'trim');
        $subscription = $this->getByID($id);

        if (empty($subscription->id)) {
            return false;
        }

        $isOrderBecomeActive = $this->isOrderBecomeActive($subscription, $data['subscriptionData']);

        $isTransactional = $this->isTransactional();
        if ($isTransactional) {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try {
            if (!empty($data['subscriptionData']) && isset($data['subscriptionData']['company_id'])) {
                if (!empty($data['subscriptionData']['company_id'])) {
                    $data['subscriptionData']['subscriber_type_id'] = 2;
                } else {
                    $data['subscriptionData']['subscriber_type_id'] = 1;
                    unset($data['subscriptionData']['company_id']);
                }
            }

            $price = 0;
            if (!empty($data['magazinesData']['magazines'])) {
                $is_100_discount = 1;
                $items = $this->getItems(['subscription_id' => $subscription->id]);
                $this->itemModel->delete(['order_id' => $subscription->id]);
                $country_id = !empty($data['addressData']['country_id']) ? (int)$data['addressData']['country_id'] : $subscription->country_id;
                foreach ($data['magazinesData']['magazines'] as $magazine) {
                    /*if(in_array($magazine['delivery_type_id'], array(self::DELIVERY_POST, self::DELIVERY_COURIER, self::DELIVERY_AVIA)) && empty($data['addressData']['address']))
                    {
                        throw new \Exception('Не указан адрес для почтовой подписки');
                    }*/
                    /*if($magazine['delivery_type_id'] == self::DELIVERY_PDF && empty($data['subscriptionData']['email']))
                    {
                        throw new \Exception('Не указан email для PDF подписки');
                    }*/
                    $item_data = $this->expandDataForNewItem($magazine, ['country_id' => $country_id, 'customer_type_id' => (!empty($data['subscriptionData']['subscriber_type_id']) ? $data['subscriptionData']['subscriber_type_id'] : $subscription['subscriber_type_id'])]);
                    $item_price = $item_data['quantity'] * $item_data['rqty'] * $item_data['price'];
                    if (!empty($item_data['discount'])) {
                        $item_price = round($item_price - $item_price * $item_data['discount'] / 100.0, 2);
                    }
                    $price += $item_price;
                    if (empty($item_data['discount']) || $item_data['discount'] != 100) {
                        $is_100_discount = 0;
                    }
                    if (!empty($items)) {
                        $replace_item = array_shift($items);
                        $item_data['last_user_id'] = $data['subscriptionData']['last_user_id'];
                        $item_data['id'] = $replace_item['id'];
                    }
                    $item_data['create_user_id'] = $data['subscriptionData']['last_user_id'];
                    //$item_data['created']         = date('Y-m-d H:i:s');
                    $item_data['order_id'] = $subscription->id;
                    $this->itemModel->addItem($item_data);
                }
                $this->itemReleaseModel->updateItemsIssues($subscription->id);
                if (!$price && $is_100_discount) {
                    $data['subscriptionData']['payment_type_id'] = 0;//не оплачивается
                    if (empty($data['subscriptionData']['type'])) {
                        $data['subscriptionData']['type'] = 1;//призовая
                    }
                }
            }

            if (!empty($data['addressData'])) {
                $insert_address = 1;
                if (!empty($subscription->address_id)) {
                    $address = $this->addressModel->getByID($subscription->address_id);
                    if (!empty($address)) {
                        $insert_address = 0;
                        $this->addressModel->update($data['addressData'], ['id' => $subscription->address_id]);
                    }
                }
                if ($insert_address) {
                    $data['addressData']['label'] = 'subscription';
                    $this->addressModel->insert($data['addressData']);
                    $address_id = $this->addressModel->lastInsertValue;
                    $data['subscriptionData']['address_id'] = $address_id;
                }
            }

            if (!empty($data['mapAddressData'])) {
                $insert_address = 1;
                if (!empty($subscription->map_address_id)) {
                    $address = $this->addressModel->getByID($subscription->map_address_id);
                    if (!empty($address)) {
                        $insert_address = 0;
                        $this->addressModel->update($data['mapAddressData'], ['id' => $subscription->map_address_id]);
                    }
                }
                if ($insert_address) {
                    $data['mapAddressData']['country_id'] = 165;//МАП адрес только для РФ
                    $data['mapAddressData']['label'] = 'subscription';
                    $this->addressModel->insert($data['mapAddressData']);
                    $data['subscriptionData']['map_address_id'] = $this->addressModel->lastInsertValue;
                }
            }

            if (!empty($data['tagsData']['tags'])) {
                $tags = $this->tagEntityModel->select(['entity_id' => TagEntity::ENTITY_ORDER, 'row_id' => $id]);
                $ids = [];
                if (!empty($tags)) {
                    foreach ($tags as $tag) {
                        $ids[] = $tag['tag_id'];
                    }
                }
                $newids = array_diff($data['tagsData']['tags'], $ids);
                $delids = array_diff($ids, $data['tagsData']['tags']);
                if (!empty($newids)) {
                    foreach ($newids as $newid) {
                        if (!empty($newid)) {
                            $this->tagEntityModel->insert(['entity_id' => TagEntity::ENTITY_ORDER, 'row_id' => $id, 'tag_id' => $newid]);
                        }
                    }
                }
                if (!empty($delids)) {
                    foreach ($delids as $delid) {
                        $this->tagEntityModel->delete(['entity_id' => TagEntity::ENTITY_ORDER, 'row_id' => $id, 'tag_id' => $delid]);
                    }
                }
            }

            if (!empty($data['subscriptionData'])) {
                if (!empty($data['subscriptionData']['xpress_id'])) {
                    $check = $this->getBy(['xpress_id' => $data['subscriptionData']['xpress_id'], 'not_id' => $subscription->id]);
                    if (!empty($check)) {
                        throw new Exception('xpress_id уже существует в базе подписок');
                    }
                }
                $action = [];
                //$action_id = !empty($data['subscriptionData']['action_id']) ? $data['subscriptionData']['action_id'] : (!empty($subscription['action_id']) ? $subscription['action_id'] : '');
                //$action_code = !empty($data['subscriptionData']['action_code']) ? $data['subscriptionData']['action_code'] : (!empty($subscription['action_code']) ? $subscription['action_code'] : '');
                if (empty($data['magazinesData']['magazines']) && !empty($data['subscriptionData']['action_id'])) {//cannt change action id without magazines data
                    unset($data['subscriptionData']['action_id']);
                }
                if (!empty($data['magazinesData']['magazines']) && !empty($data['subscriptionData']['action_id']))// || !empty($data['subscriptionData']['action_code'])))
                {
                    /*$action = $this->actionModel->getByID($data['subscriptionData']['action_id']);
                    if((!empty($action['code']) || !empty($action['codes'])))
                    {
                        if(empty($data['subscriptionData']['action_code']))
                        {
                            throw new \Exception('Требуется указать промокод для акции с id = ' . (int)$data['subscriptionData']['action_id']);
                        }
                        else
                        {
                            $input_code = preg_replace('#[^\w-]#is','',$data['subscriptionData']['action_code']);
                            $check      = $this->getActions(array('code'=>$input_code, 'id'=>$data['subscriptionData']['action_id']));
                            if(empty($check))
                            {
                                throw new \Exception('Указан неверный промокод = ' . $input_code . ' для акции с id = ' . (int)$data['subscriptionData']['action_id']);
                            }
                        }
                    }*/
                    $action = $this->actionService->getAction($this->actionService->prepareData($data['magazinesData']['magazines']), '', (int)$data['subscriptionData']['action_id'], $this->actionService->defaultCcID, 'no_code');
                    if (empty($action)) {
                        unset($data['subscriptionData']['action_id']);
                    } else {
                        $data['subscriptionData']['action_id'] = $action['id'];
                    }
                }
                /*if(!$price && !empty($data['subscriptionData']['action_id']))
                {
                    $items = $this->getItems(array('subscription_id' => $subscription['id']));
                    if(!empty($items))
                    {
                        foreach($items as $item)
                        {
                            $price += $item['price'];
                        }
                    }
                }*/
                if (!empty($data['magazinesData']['magazines'])) {
                    /*if(!empty($action))
                    {//action discount already applyed as item discount
                        $price = round($price-$price*$action['discount']/100.0,2);
                    }*/
                    if (empty($data['subscriptionData']['label']) || $data['subscriptionData']['label'] != 'xpress_subscription') {//coz while import from xpress price must be assigned from xpress and not by calculation
                        $data['subscriptionData']['price'] = $price;
                    }
                }

                unset($data['subscriptionData']['label']);
                unset($data['subscriptionData']['http_referer']);
                unset($data['subscriptionData']['create_user_id']);
                unset($data['subscriptionData']['created']);
                if (empty($data['subscriptionData']['last_user_id'])) {
                    unset($data['subscriptionData']['last_user_id']);
                }
                $this->update($data['subscriptionData'], ['id' => $subscription->id]);
                //if($subscription->order_state_id!=3 && $data['subscriptionData']['order_state_id']==3 && empty($subscription->user_id) && empty($data['subscriptionData']['user_id']))
                //{
                //    $this->tryToRegisterUser(array('id' => $subscription->id));
                //}
            }
            $this->logger->log('Обновление подписки #' . $subscription->id, 'update_subscription', $data, !empty($data['subscriptionData']['last_user_id']) ? $data['subscriptionData']['last_user_id'] : 0);
            if ($isTransactional) {
                $this->adapter->getDriver()->getConnection()->commit();
                if ($isOrderBecomeActive) {
                    $this->getEvents()->trigger(self::EVENT_HAS_BECOME_ACTIVE, $this, ['orderId' => intval($id)]);
                }
            }
            return $subscription->id;
        } catch (Exception $e) {
            if ($isTransactional) {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
            return false;
        }
    }

    /**
     * Календарь подписки
     * @param array $params
     * @return SubscriptionOrder
     */
    /*public function getCalendars(array $params = array())
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(array('I' => 'Message150'))
            ->columns(array(
                'Magazine_ID',
                'Month',
                'NumMagazines',
                'DateFinal'
            ))
            ->join(array('M' => 'Message151'), 'I.Calendar = M.Message_ID', array('year'))
            ->where('M.Checked = 1')
            ->order(array('Magazine_ID','M.year ASC','I.Month ASC'));
        if($params['magazineID'])
        {
            $select->where(array('I.Magazine_ID = ?' => (int)$params['magazineID']));
        }

        $selectString = $sql->$this->buildSqlString($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        return $result->toArray();
    }*/
    public function getByID($id)
    {
        return $this->getBy(['id' => $id]);
    }

    public function expandDataForNewItem($data, array $params = [])//$country_id = 0)
    {
        if (empty($data['periodical_id']) || empty($data['delivery_type_id'])) {
            throw new Exception('Subscription update magazines data is empty 1.');
        }
        if ((empty($data['date_start']) || empty($data['period'])) && (empty($data['rqty']) || empty($data['bnum']) || empty($data['byear']))) {
            throw new Exception('Subscription update magazines data is empty 2.');
        }
        if (!empty($data['date_start']) && !empty($data['period']) && (empty($data['bnum']) || empty($data['byear']) || empty($data['rqty']))) {
            //list($data['bnum'], $data['byear']) = $this->getBeginNumberByCalendar($data['periodical_id'], $data['date_start'], $data['period']);
            $counts = $this->getCountsByIssues($data['periodical_id'], $data['date_start'], $data['period']);
            if (empty($data['byear'])) {
                $data['byear'] = $counts['byear'];
            }
            if (empty($data['bnum']) && $counts['bnum']) {
                $data['bnum'] = $counts['bnum'];
            }
            if (empty($data['rqty']) && $counts['rqty']) {
                $data['rqty'] = $counts['rqty'];
            }
        }
        /*if(!empty($data['date_start']) && !empty($data['period']) && empty($data['rqty']))
        {
            $data['rqty'] = $this->getCountByCalendar($data['periodical_id'], $data['date_start'], $data['period']);
        }*/
        if (empty($data['bnum']) || empty($data['rqty'])) {
            throw new Exception('Subscription update magazines count data is empty.');
        }
        $data['type'] = $this->getTypeByDeliveryTypeID($data['delivery_type_id']);
        if (!empty($data['date_start']) && !empty($data['period'])) {
            $dateEnd = new DateTime($data['date_start']);
            $dateEnd->modify('+' . $data['period'] . ' month')->modify('-1 day');

            $dateEnd = $dateEnd->format('Y-m-d');

            $data['date_end'] = $dateEnd;
//            $startstamp       = strtotime($data['date_start']);
//            $data['date_end'] = date('Y-m-d', mktime(0, 0, 0, date('n', $startstamp) + $data['period'], date('j', $startstamp), date('Y',$startstamp)));
        }
        if (empty($data['price']) && !empty($params['customer_type_id'])) {
            $zone_id = $this->getZoneByCountry(!empty($params['country_id']) ? $params['country_id'] : $this->defaultCountryID);
            $data['price'] = $this->getMagazinePrice($data['periodical_id'], $data['delivery_type_id'], $zone_id, $params['customer_type_id']);//$data['rqty'] *
        }
        /*if( !empty($data['rqty']) && !empty($data['bnum']) && !empty($data['byear']) && (empty($data['date_start']) || empty($data['date_end'])) )
        {
            $dates = $this->getDatesByCalendar($data['periodical_id'], $data['byear'], $data['bnum'], $data['rqty']);
            if($dates['date_start']) {
                $data['date_start'] = $dates['date_start'];
            }
            if($dates['date_end']) {
                $data['date_end'] = $dates['date_end'];
            }
        }*/
        /*if(empty($data['discount'])) {
            $data['discount'] = 0;
        }
        if(!isset($data['enabled'])) {
            $data['enabled'] = 1;
        }
        if(!isset($data['completed'])) {
            $data['completed'] = 0;
        }*/
        if (!isset($data['quantity'])) {
            $data['quantity'] = 1;
        }/*
        if(!isset($data['date_start'])) {
            $data['date_start'] = 'NULL';
        }
        if(!isset($data['date_end'])) {
            $data['date_end'] = 'NULL';
        }
        if(!isset($data['period'])) {
            $data['period'] = 0;
        }
        //if(!isset($data['m133_id'])) {
        //    $data['m133_id'] = 0;
        //}
        //if(!isset($data['xpress_id'])) {
        //    $data['xpress_id'] = 0;
        //}
        if(!isset($data['last_user_id'])) {
            $data['last_user_id'] = 0;
        }*/
        //if(empty($data['price']))
        //{
        //    throw new \Exception('Subscription item price is empty.');
        //}
        return $data;
    }

    /*public function getPriceList($dateStart = '')
    {
        if(empty($dateStart) && !empty($this->currentPrice))
        {
            return $this->currentPrice;
        }
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from('Message144')
            ->columns(array(
                'id' => 'Message_ID'
            ))
            ->where('DateStart < ' . (empty($dateStart) ? 'NOW()' : '"' . $dateStart . '"'))
            ->where('Checked = 1')
            ->order('DateStart DESC')
            ->limit(1);
        $selectString = $sql->$this->buildSqlString($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $price        = $result->current();

        $priceID = $price['id'];

        $select = $sql->select()
            ->from('Message130')
            ->columns(array(
                    'min' => 'Minimal',
                    'max' => 'Maximal',
                    'MagazineID',
                    'Price',
                    'ZoneID',
                    'SubscriptionDeliveryTypeID'
                )
            )
            ->where(array('PriceID = ?' => $priceID))
            //->where('Price <> 0')
            ->order('MagazineID');
        $selectString = $sql->$this->buildSqlString($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $priceData    = $result->toArray();

        $currentPrice = array();
        foreach ($priceData as $price)
        {
            $currentPrice['priceID'] = $priceID;
            $currentPrice['magazine'][$price['MagazineID']]['eDelivery'] = !empty($currentPrice['magazine'][$price['MagazineID']]['eDelivery']) ? True : False;
            $currentPrice['magazine'][$price['MagazineID']]['pDelivery'] = !empty($currentPrice['magazine'][$price['MagazineID']]['pDelivery']) ? True : False;
            $currentPrice['magazine'][$price['MagazineID']]['period']    = array(
                'min' => $price['min'],
                'max' => $price['max']
            );
            if ($price['SubscriptionDeliveryTypeID'] == 5 && !empty($price['Price']))
            {
                $currentPrice['magazine'][$price['MagazineID']]['eDelivery'] = True;
            }
            if ($price['SubscriptionDeliveryTypeID'] == 1 && !empty($price['Price']))
            {
                $currentPrice['magazine'][$price['MagazineID']]['pDelivery'] = True;
            }
            $currentPrice['magazine'][$price['MagazineID']]['deliveryType'][$price['SubscriptionDeliveryTypeID']]['zone'][$price['ZoneID']] = $price;
        }
        if(empty($dateStart) && !empty($currentPrice))
        {
            $this->currentPrice = $currentPrice;
        }
        return $currentPrice;
    }*/



    public function getCountsByIssues($magazineID, $startDate, $period)
    {
        $start_stamp = strtotime($startDate);
        $start_year = date('Y', $start_stamp);
        $start_month = date('n', $start_stamp);
        $end_stamp = mktime(0, 0, 0, date('n', $start_stamp) + $period - 1, date('j', $start_stamp), date('Y', $start_stamp));
        $end_year = date('Y', $end_stamp);
        $end_month = date('n', $end_stamp);

        $issues = $this->itemReleaseModel->getIssues(['byear' => $start_year, 'periodical_id' => $magazineID, 'order_by' => ['I.year', 'I.release_month']]);

        $count = 0;
        $rqty = 0;
        $bnum = 0;
        $byear = $start_year;
        if (!empty($issues)) {
            $issues_by_year = [];
            foreach ($issues as $issue) {
                $issues_by_year[$issue['year']][] = $issue;
            }
            if (empty($issues_by_year[$start_year])) {
                $issues_by_year[$start_year] = end($issues_by_year);
            }
            if (empty($issues_by_year[$start_year + 1])) {
                $issues_by_year[$start_year + 1] = $issues_by_year[$start_year];
            }
            if (empty($issues_by_year[$start_year + 2])) {
                $issues_by_year[$start_year + 2] = $issues_by_year[$start_year + 1];
            }

            $break = 0;
            foreach ($issues_by_year as $year => $issues) {
                foreach ($issues as $issue) {
                    $count++;
                    if ($year > $end_year || ($year == $end_year && $issue['release_month'] > $end_month)) {
                        $break = 1;
                        break;
                    }
                    if ($year == $start_year && $issue['release_month'] < $start_month) {
                        continue;
                    }
                    if (!$bnum) {
                        if ($year > $start_year) {
                            $byear = $year;
                            $bnum = 1;
                        } else {
                            $bnum = $count;
                        }
                    }
                    $rqty++;
                }
                if ($break) {
                    break;
                }
            }
        }
        return ['byear' => $byear, 'bnum' => $bnum, 'rqty' => $rqty];
    }

    public function getTypeByDeliveryTypeID($delivery_type_id)
    {
        if ($delivery_type_id == 5) {
            return 'pdf';
        } else {
            return 'paper';
        }
    }

    /**
     * Отдает зону доставки по ID страны
     * @param int $countryID
     * @return int
     */
    public function getZoneByCountry($countryID)
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        // $select = $sql->select()
        //     ->from('Message127')
        //     ->columns(array('id' => 'ZoneID'))
        //     ->where(array('CountryID = ?' => $countryID));
        $select = $sql->select()
            ->from('nx_zone_country')
            ->columns(['id' => 'zone_id'])
            ->where(['country_id = ?' => $countryID]);

        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $zone = $result->current();
        return !empty($zone['id']) ? intval($zone['id']) : $this->defaultZoneID;
    }

    public function getMagazinePrice($periodical_id, $delivery_type_id, $zone_id, $customer_type_id = 1)
    {
        //map to netcat price list usage
        $delivery_map = [
            '1' => 1,//почта->почта
            '2' => 2,//офис->офис
            '3' => 3,//курьер->курьер
            '4' => 3,//курьер-Электрический->курьер
            '5' => 5,//pdf->pdf
            '6' => 3,//машина->курьер?
            '7' => 1,//почта гор->почта
            '8' => 1,//почта ДЗ->почта
            '9' => 1,//почта ДЗ авиа->почта
            '10' => 1,//почта РФ->почта
            '11' => 1//почта СНГ->почта
        ];
        //$priceList = $this->getPriceList();
        //$price = !empty($priceList['magazine'][$periodical_id]['deliveryType'][$delivery_map[$delivery_type_id]]['zone'][$zone_id]['Price']) ? (float)$priceList['magazine'][$periodical_id]['deliveryType'][$delivery_map[$delivery_type_id]]['zone'][$zone_id]['Price'] : 0;
        $priceList = $this->priceModel->getMagazinePriceList();
        $price = !empty($priceList['customerType'][$customer_type_id]['magazine'][$periodical_id]['deliveryType'][$delivery_map[$delivery_type_id]]['zone'][$zone_id]['Price']) ? (float)$priceList['customerType'][$customer_type_id]['magazine'][$periodical_id]['deliveryType'][$delivery_map[$delivery_type_id]]['zone'][$zone_id]['Price'] : 0;
        return $price;
    }

    public function addSubscription($data)
    {
        // $logFile = '/var/www/nx_git/application/logs/subscription_add.log';
        // $writer = new \Zend\Log\Writer\Stream($logFile);
        // $logger = new \Zend\Log\Logger();
        // $logger->addWriter($writer);
        // $logger->debug("Begin of addSubscription\n");

        // $logger->debug(print_r($data,true));


        if (empty($data['subscriptionData'])) {
            return false;
        }
        $data = applyFunctionToData($data, 'trim');

        if (empty($data['magazinesData']['magazines'])) {
            throw new Exception('Не указаны издания для подписки');
            return false;
        }

        if (empty($data['subscriptionData']['person_id']) && empty($data['subscriptionData']['company_id'])) {
            throw new Exception('Не указан подписчик');
            return false;
        }

        $isTransactional = $this->isTransactional();
        if ($isTransactional) {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try {
            if (!empty($data['subscriptionData'])) {
                if (!empty($data['subscriptionData']['company_id'])) {
                    $data['subscriptionData']['subscriber_type_id'] = 2;
                } else {
                    $data['subscriptionData']['subscriber_type_id'] = 1;
//                    $data['subscriptionData']['company_id'] = null;
                    unset($data['subscriptionData']['company_id']);
                }
            }

            $price = 0;
            $country_id = !empty($data['addressData']['country_id']) ? (int)$data['addressData']['country_id'] : $this->defaultCountryID;
            $items_data = [];
            $is_100_discount = 1;
            foreach ($data['magazinesData']['magazines'] as $magazine) {
                if (!empty($magazine['date_start'])) {
                    $magazine['date_start'] = date('Y-m', strtotime($magazine['date_start'])) . '-01';
                }
                unset($magazine['id']);
                $item_data = $this->expandDataForNewItem($magazine, ['country_id' => $country_id, 'customer_type_id' => $data['subscriptionData']['subscriber_type_id']]);
                $item_price = $item_data['quantity'] * $item_data['rqty'] * $item_data['price'];
                if (!empty($item_data['discount'])) {
                    $item_price = round($item_price - $item_price * $item_data['discount'] / 100.0, 2);
                }
                if (empty($item_data['discount']) || $item_data['discount'] != 100) {
                    $is_100_discount = 0;
                }
                $price += $item_price;
                $item_data['create_user_id'] = $data['subscriptionData']['create_user_id'];
                $item_data['created'] = date('Y-m-d H:i:s');
                $items_data[] = $item_data;
            }

            if (!empty($data['addressData'])) {
                if (empty($data['addressData']['country_id'])) {
                    $data['addressData']['country_id'] = Address::COUNTRY_ID_RUSSIA;
                }
                $data['addressData']['label'] = 'subscription';
                $this->addressModel->insert($data['addressData']);
                $address_id = $this->addressModel->lastInsertValue;
                $data['subscriptionData']['address_id'] = $address_id;
            }

            if (!empty($data['mapAddressData'])) {
                $data['mapAddressData']['country_id'] = Address::COUNTRY_ID_RUSSIA;//МАП адрес только для РФ
                $data['mapAddressData']['label'] = 'subscription';
                $this->addressModel->insert($data['mapAddressData']);
                $data['subscriptionData']['map_address_id'] = $this->addressModel->lastInsertValue;
            }

            if (!empty($data['subscriptionData'])) {
                if (empty($data['subscriptionData']['price']) || empty($data['subscriptionData']['label']) || $data['subscriptionData']['label'] != 'xpress_subscription') {
                    //todo include action into price calculation?
                    //if(!empty($data['orderData']['client_price']) && !\nx\functions\isFloatEqual($data['orderData']['client_price'], $price))
                    //{
                    //    throw new \Exception('Не совпадение клиентской и серверной цены.');
                    //}
                    $data['subscriptionData']['price'] = $price;
                }
                if (!$price && $is_100_discount) {
                    $data['subscriptionData']['payment_type_id'] = 0;//не оплачивается
                    if (empty($data['subscriptionData']['type'])) {
                        $data['subscriptionData']['type'] = 1;//призовая
                    }
                }
                /*if(empty($data['subscriptionData']['order_state_id']))
                {???
                    if(empty($data['subscriptionData']['price']) && !empty($data['subscriptionData']['action_id']))
                    {
                        $data['subscriptionData']['order_state_id'] = 3;
                    }
                    else
                    {
                        $data['subscriptionData']['order_state_id'] = 2;
                    }
                }*/
                if (empty($data['subscriptionData']['label'])) {
                    $data['subscriptionData']['label'] = 'nx_subscription';
                }
                if (empty($data['subscriptionData']['payment_agent_id'])) {
                    $data['subscriptionData']['payment_agent_id'] = 47;
                }
                $data['subscriptionData']['shop_id'] = 1;
                $data['subscriptionData']['created'] = date('Y-m-d H:i:s');
                if (!isset($data['subscriptionData']['price_id'])) {
                    $priceList = $this->priceModel->getMagazinePriceList();
                    $data['subscriptionData']['price_id'] = $priceList['customerType'][$data['subscriptionData']['subscriber_type_id']]['priceID'];
                }
                $data['subscriptionData']['http_referer'] = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';


                // $data['subscriptionData']['company_id'] = null;

                // $logger->debug('before inserting');
                // $logger->debug(print_r($data,true));

                $this->insert($data['subscriptionData']);
                $subscription_id = $this->lastInsertValue;

                $this->update(
                    ['hash' => $this->calculateOrderHash($subscription_id)],
                    ['id' => $subscription_id]
                );
            }

            foreach ($items_data as $item_data) {
                $item_data['order_id'] = $subscription_id;
                $this->itemModel->addItem($item_data);
            }

            $this->itemReleaseModel->updateItemsIssues($subscription_id);

            if (!empty($data['tagsData']['tags'])) {
                foreach ($data['tagsData']['tags'] as $tagid) {
                    if (!empty($tagid)) {
                        $this->tagEntityModel->insert(['entity_id' => TagEntity::ENTITY_ORDER, 'row_id' => $subscription_id, 'tag_id' => $tagid]);
                    }
                }
            }

            $this->logger->log('Добавление подписки #' . $subscription_id, 'add_subscription', $data, !empty($data['subscriptionData']['create_user_id']) ? $data['subscriptionData']['create_user_id'] : 0);
            if ($isTransactional) {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $subscription_id;
        } catch (Exception $e) {
            if ($isTransactional) {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
            return false;
        }
    }

    /**
     * Расчет хэша для заказа
     * @param int $subscriptionID id заказа
     * @return string
     */
    public function calculateOrderHash($subscriptionID)
    {
        return md5($subscriptionID . (!empty($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'nx.osp.ru'));
    }

    public function doXpressImport($mode = 2, $action = '')
    {
        $data = [
            'counts' => [
                'total' => 0,
                'lock' => 0,
                'link' => 0,
                'add' => 0,
                'manual' => 0
            ],
            'ids' => [
                'lock' => [],
                'link' => [],
                'add' => [],
                'manual' => []
            ]
        ];

        $subscriptions = $this->xpressModel->getSubscriptions(['is_present' => 1, 'is_payed' => 1, 'not_source' => 18]);
        if ($subscriptions->count()) {
            ini_set("max_execution_time", "90");
            $data['counts']['total'] = $subscriptions->count();
            foreach ($subscriptions as $i => $subscription) {
                //if($i<100) continue;
                $result = $this->importFromXpress([
                    'xdb' => $this->xpressModel->getAdapter(),
                    'xpress_id' => $subscription->id,
                    'mode' => $mode,
                    'do_action' => $action
                ]);
                //if($i>1000) break;
                $data['counts'][$result['action']]++;
                $subscription_data = ['id' => $subscription->id, 'comment' => $result['comment']];
                if ($result['subscription_link_id']) {
                    $subscription_data['link_id'] = $result['subscription_link_id'];
                }
                $data['ids'][$result['action']][] = $subscription_data;
            }
        }

        if (!empty($data['counts']['total'])) {
            file_put_contents('/var/www/nx.osp.ru/htdocs/application/data/cache/xpressImportStats.txt', serialize($data));
        }

        return $data;
    }

    public function intersectionCheck($data, $subscription_id = 0)
    {
        $email = $data['subscriptionData']['email'];
        $person_id = $data['subscriptionData']['person_id'];
        $company_id = $data['subscriptionData']['company_id'];

        $subscriber_issues_ids_by_type = [];
        $subscriber_issues_by_id = [];
        $whom_condition = '';
        if ($email) {
            $whom_condition = 'O.email = "' . $email . '"';
        }
        if ($person_id || $company_id) {
            $whom_condition = '(' . ($whom_condition ? $whom_condition . ' OR ' : '') . ($person_id ? 'O.person_id = "' . (int)$person_id : 'O.company_id = "' . (int)$company_id) . '")';
        }
        if ($whom_condition) {
            $db = $this->adapter;
            $sql = new Sql($db);
            $select = $sql->select()
                ->from(['II' => 'nx_order_item_release'])
                ->columns(['release_id'])
                ->join(['OI' => 'nx_order_item'], 'OI.id = II.item_id', ['order_id'])
                ->join(['S' => 'nx_subscription'], 'S.id = OI.item_id', ['type'])
                ->join(['O' => 'nx_order'], 'O.id = OI.order_id', [])
                ->where('(O.order_state_id = 3 OR O.type <> 0)')
                ->where(['OI.item_type' => 'nx_subscription'])
                ->where($whom_condition)
                ->group('II.release_id');
            if ($subscription_id) {
                $select->where->notEqualTo('O.id', $subscription_id);
            }
            $selectString = $sql->buildSqlString($select);
            $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
            $subscriber_issues = $result->toArray();
            if (!empty($subscriber_issues)) {
                foreach ($subscriber_issues as $subscriber_issue) {
                    if (!isset($subscriber_issues_ids_by_type[$subscriber_issue['type']])) {
                        $subscriber_issues_ids_by_type[$subscriber_issue['type']] = [$subscriber_issue['release_id']];
                    } else {
                        $subscriber_issues_ids_by_type[$subscriber_issue['type']][] = $subscriber_issue['release_id'];
                    }
                    $subscriber_issues_by_id[$subscriber_issue['release_id']] = $subscriber_issue;
                }
            }
        }

        if (empty($subscriber_issues_ids_by_type)) {
            return [];
        }

        $result = [];
        foreach ($data['magazinesData']['magazines'] as $magazine) {
            $item_data = $this->expandDataForNewItem($magazine);
            $item_issues = $this->itemReleaseModel->getIssues([
                'periodical_id' => $item_data['periodical_id'],
                'byear' => $item_data['byear'],
                'bnum' => $item_data['bnum'],
                'rqty' => $item_data['rqty']
            ]);
            if (!empty($item_issues)) {
                foreach ($item_issues as $item_issue) {
                    if (!empty($subscriber_issues_ids_by_type[$item_data['type']]) && in_array($item_issue['id'], $subscriber_issues_ids_by_type[$item_data['type']])) {
                        $periodical = $this->getSubscribleMagazines(['id' => $item_data['periodical_id']]);
                        $result = [
                            'subscription_id' => $subscriber_issues_by_id[$item_issue['id']]['order_id'],
                            'periodical_name' => $periodical[$item_data['periodical_id']]['name']
                        ];
                        return $result;
                    }
                }
            }
        }
        return $result;
    }

    public function getSubscribleMagazines(array $params = [])
    {
        $db = $this->aecms_adapter;
        $sql = new Sql($db);
        $select = $sql->select()
            ->from(['A' => 'Edition'])
            ->columns(
                [
                    '*',
                    'id' => 'Message_ID',
                    'name' => 'EnglishName'
                ]
            )
            //->where(array('Checked' => 1))
            ->where(['deleted' => 0]);
        if (!empty($params['id'])) {
            $select->where(['Message_ID' => (int)$params['id']]);
        }
        if (!empty($params['add_ids'])) {
            $select->where->NEST->equalTo('A.subscribe', 1)->OR->in('A.Message_ID', $params['add_ids'])->UNNEST;
        } else {
            $select->where(['A.subscribe' => 1]);
        }
        $selectString = $sql->buildSqlString($select);
        $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $result = $result->toArray();
        if (!empty($result)) {
            $magazines_by_id = [];
            foreach ($result as $magazine) {
                $magazines_by_id[$magazine['id']] = $magazine;
            }
            $result = $magazines_by_id;
        }
        return $result;
    }

    private function isOrderBecomeActive(\nx\Entity\SubscriptionOrder $prevData, array $newData): bool
    {
        $setFree = intval($prevData->payment_type_id) != 0 && intval($newData['payment_type_id']) == 0;
        $setPaid = intval($prevData->order_state_id) != 3 && intval($newData['order_state_id']) == 3;
        return $setFree || $setPaid;
    }
}
