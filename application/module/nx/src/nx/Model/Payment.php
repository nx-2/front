<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Payment extends AbstractTable
{
    public $companyModel,$publisherModel, $orderModel, $transactionModel, $ws1c_client;
    public $subscriptionService, $orderService, $companyService;//subscriptionItemService

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_payment', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Payment));
    }

    public function getList($offset, $limit, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['P'=>$this->table])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS P.id AS id'),
                'ws1c_id',
                'order_id',
                'transaction_id',
                'sum',
                'date' => new Expression('DATE_FORMAT(P.date, "%Y-%m-%d")'),
                'year',
                'docno',
                'purpose',
                'result',
                'result_comment',
                'request_data',
                'comment',
                'payment_agent_id',
                'contractor_id',
                'contractor_name_inn' => new Expression('IF(P.contractor_id, C.name, CONCAT(P.contractor_name,IF(P.contractor_inn,CONCAT(" ИНН ",P.contractor_inn),"")))'),
                'contractor_name',
                'contractor_inn',
                'contractor_kpp',
                'status',
                'enabled',
                'label',
                'created',
                'last_updated'//,
                //'enabled'
            ])
            ->join(['PA' => 'nx_payment_agent'], 'PA.id = P.payment_agent_id', ['payment_agent_name' => 'name'], 'left')
            ->join(['C' => 'nx_company'], 'C.id = P.contractor_id', [], 'left')
            ->join(['O' => 'nx_order'], 'P.order_id = O.id', [], 'left')
            ->join(['PR' => 'nx_price'], 'O.price_id = PR.id', [], 'left')
            ->group('P.id')
            ->order('P.id DESC')
            ->limit($limit)
            ->offset($offset);

        if ($this->publisher_id) {
            $select
//                ->join(array('PR'=>'nx_price'), 'PR.id = O.price_id', array())
//                ->where(array('PR.publisher_id' => $this->publisher_id));
                ->where('(PR.publisher_id = ' . $this->publisher_id . ' OR P.publisher_id = ' . $this->publisher_id . ')');
        } else {
            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }

        $is_status_success = 0;
        $is_ws1c_payment   = 0;
        $is_filter_enabled = 0;
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                switch($filter['property'])
                {
                    case 'contractor_name_inn':
                        $select->where->NEST
                            ->like('C.name', '%' . $filter['value'] . '%')->OR
                            ->like('P.contractor_name', '%' . $filter['value'] . '%')->OR
                            ->like('P.contractor_inn', '%' . $filter['value'] . '%')->UNNEST;
                        break;
                    case 'status':
                        if($filter['value'] > 0) {
                            $is_status_success = 1;
                        }
                        $select->where->equalTo('P.'.$filter['property'], $filter['value']);
                        break;
                    case 'status_greater':
                        if($filter['value'] >= 0) {
                            $is_status_success = 1;
                        }
                        $select->where->greaterThan('P.status', $filter['value']);
                        break;
                    case 'year':
                    case 'enabled':
                        $is_filter_enabled = 1;
                        $select->where->equalTo('P.'.$filter['property'], $filter['value']);
                        break;
                    case 'created':
                    case 'sum':
                    case 'date':
                    case 'id':
                    case 'transaction_id':
                    case 'order_id':
                    case 'ws1c_id':
                    case 'purpose':
                    case 'comment':
                    case 'docno':
                        $select->where->like('P.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'label':
                        if($filter['value'] == 'ws1c_payment')
                        {
                            $is_ws1c_payment = 1;
                        }
                        $select->where->like('P.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'payment_agent_name':
                        $select->where->like('PA.name', '%' . $filter['value'] . '%');
                        break;
                }
            }
        }
        if(!$is_status_success && $is_ws1c_payment)
        {
            $select->where('P.status < 1');
        }
        /*if(!$is_filter_enabled && $is_ws1c_payment)
        {
            $select->where('P.enabled = 1');
        }*/

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        if($resultSet->count())
        {
            $order_ids = [];
            foreach($resultSet as $row)
            {
                if(!empty($row->order_id)) {
                    $order_ids[] = $row->order_id;
                }
                $request = $row->request_data ? unserialize($row->request_data) : [];
                $result  = $row->result ? unserialize($row->result) : [];
                $row->status_comment = $row->result_comment;
                /*if(!empty($result['IsError']))
                {
                    if(!empty($result['Comment']))
                    {
                        $row->status_comment = $result['Comment'];
                    }
                    if(!empty($result['Decoding']))
                    {
                        $row->status_comment = $result['Decoding'];
                    }
                }
                if(!empty($result['error']))
                {
                    $row->status_comment = $result['error'];
                }*/
                if($row->status>0 && !empty($request['PaymentRowSum']))
                {
                    if(!empty($request['PaymentRowSum']['NDS']))
                    {
                        $sums = [$request['PaymentRowSum']];
                    }
                    else
                    {
                        $sums = $request['PaymentRowSum'];
                    }
                    if(!empty($sums))
                    {
                        $row->status_comment .= '<br/>';
                        foreach($sums as $sum)
                        {
                            $row->status_comment .= $sum['PaymentSum'] . '/' . $sum['NDS'] . '%; ';
                        }
                        $row->status_comment = substr($row->status_comment, 0, -2);
                    }
                }
            }
            $orders_by_id = [];
            if(!empty($order_ids))
            {
                $orders = $this->subscriptionService->mapper->getSubscriptions(['ids' => $order_ids]);
                if($orders->count()) {
                    foreach($orders as $order) {
                        $orders_by_id[$order->id] = $order;
                    }
                }
            }
            foreach($resultSet as $row)
            {
                $is_match_contractor = 1;
                $row->contractor_match_comment = '';
                if(!empty($row->order_id) && !empty($orders_by_id[$row->order_id]->company_id))
                {
                    if(!empty($row->contractor_inn) && $row->contractor_inn!=trim($orders_by_id[$row->order_id]->company_inn)) {
                        $is_match_contractor = 0;
                    }//$this->logger->log_console(array($is_match_contractor, $row->contractor_inn, $orders_by_id[$row->order_id]->company_inn));
                    if(!empty($row->contractor_kpp) && $row->contractor_kpp!=trim($orders_by_id[$row->order_id]->company_kpp)) {
                        $is_match_contractor = 0;
                    }//$this->logger->log_console(array($is_match_contractor, $row->contractor_kpp, $orders_by_id[$row->order_id]->company_kpp));
                    if(!$is_match_contractor) {
                        $row->contractor_match_comment = 'не совпадают рек-ты ' . //$row->contractor_name . ', ' . $row->contractor_inn . ', ' . $row->contractor_kpp . ' != ' .
                            htmlspecialchars($orders_by_id[$row->order_id]->company_name, ENT_QUOTES) . ', ' . $orders_by_id[$row->order_id]->company_inn . ', ' . $orders_by_id[$row->order_id]->company_kpp;
                    }
                }
            }
        }

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function savePaymentFromWs1c($params)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        if(empty($params['payments']) || !is_array($params['payments']))
        {
            throw new \Exception('paramaters empty or not array');
        }

        $results = [];

        foreach($params['payments'] as $payment)
        {
            if(empty($payment['ws1c_id']))
            {
                $results[] = [
                    'ws1c_id' => '',
                    'year'    => $payment['year'],
                    'result'  => 'empty ws1c_id'
                ];
                continue;
            }
            if( empty($payment['publisher_id'])  ) {
                $results[] = [
                    'ws1c_id' => $payment['ws1c_id'],
                    'year'    => $payment['year'],
                    'result'  => 'empty publisher_id',
                ];
                continue;
            }
            $publisherId = intval($payment['publisher_id']);
            $publisher = $this->publisherModel->getOne($publisherId);
            if ( !$publisher || empty($publisher) ) {
                $results[] = [
                    'ws1c_id' => $payment['ws1c_id'],
                    'year'    => $payment['year'],
                    'result'  => 'publisher with id=' . $publisherId . ' not found',
                ];
                continue;
            }
//            if (!boolval($publisher['1c_integration_verified']) ) {
//                $results[] = array(
//                    'ws1c_id' => $payment['ws1c_id'],
//                    'year'    => $payment['year'],
//                    'result'  => 'integration with 1c is not verified for publisher ' . $publisherId,
//                );
//                continue;
//            }
            if(empty($payment['year']))
            {
                $results[] = [
                    'ws1c_id' => $payment['ws1c_id'],
                    'year'    => $payment['year'],
                    'result'  => 'empty year'
                ];
                continue;
            }
            if(!in_array($payment['year'], [date('Y'), date('Y')-1]))
            {
                $results[] = [
                    'ws1c_id' => $payment['ws1c_id'],
                    'year'    => $payment['year'],
                    'result'  => 'invalid year'
                ];
                continue;
            }
            $check = $this->getBy(['ws1c_id' => $payment['ws1c_id'], 'year' => $payment['year']]);
            if(empty($check))
            {
                if(!empty($payment['deleted']))
                {
                    $results[] = [
                        'ws1c_id' => $payment['ws1c_id'],
                        'year'    => $payment['year'],
                        'result'  => 'not exists'
                    ];
                    continue;
                }
                if(empty($payment['payment_date']))
                {
                    $results[] = [
                        'ws1c_id' => $payment['ws1c_id'],
                        'year'    => $payment['year'],
                        'result'  => 'empty payment date'
                    ];
                    continue;
                }
                $year  = date('Y', strtotime($payment['payment_date']));
                $month = date('n', strtotime($payment['payment_date']));
                $day   = date('j', strtotime($payment['payment_date']));
                if(!checkdate($month, $day, $year) || ($year!=date('Y') && $year!=date('Y')-1))
                {
                    $results[] = [
                        'ws1c_id' => $payment['ws1c_id'],
                        'year'    => $payment['year'],
                        'result'  => 'invalid payment date'
                    ];
                    continue;
                }
                //process organization
                if(empty($payment['organization']['ws1c_id']))
                {
                    $results[] = [
                        'ws1c_id' => $payment['ws1c_id'],
                        'year'    => $payment['year'],
                        'result'  => 'empty organization'
                    ];
                    continue;
                }
                $select       = $sql->select()->from(['PA'=>'nx_payment_agent'])->where(['ws1c_id' => $payment['organization']['ws1c_id']]);
                $selectString = $sql->getSqlStringForSqlObject($select);
                $result       = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
                $result       = $result->current();
                if(empty($result['id']))
                {
                    $results[] = [
                        'ws1c_id' => $payment['ws1c_id'],
                        'year'    => $payment['year'],
                        'result'  => 'organization not found'
                    ];
                    continue;
                }
                $data['payment_agent_id'] = $result['id'];

                //process contractor
                /*if(!empty($payment['contractor']['inn']))
                {
                    $company = $this->companyModel->getBy(array('inn' => $payment['contractor']['inn']));
                    if(!empty($company->id))
                    {
                        $data['contractor_id'] = $company->id;
                    }
                }*/
                $data['contractor_name'] = $payment['contractor']['name'];
                $data['contractor_inn']  = $payment['contractor']['inn'];
                $data['contractor_kpp']  = !empty($payment['contractor']['kpp']) ? $payment['contractor']['kpp'] : '';

                $data['ws1c_id']      = $payment['ws1c_id'];
                $data['year']         = $payment['year'];
                $data['date']         = date('Y-m-d H:i:s', strtotime($payment['payment_date']));
                $data['sum']          = $payment['payment_sum'];
                $data['docno']        = $payment['payment_docno'];
                $data['purpose']      = $payment['payment_purpose'];
                $data['comment']      = $payment['contractor_by_bank'] . ($payment['contractor_by_bank'] && $payment['payment_comment'] ? '; ' : '') . $payment['payment_comment'];
                $data['request_data'] = serialize($payment);
                $data['created']      = date('Y-m-d H:i:s');
                $data['label']        = 'ws1c_payment';
                $data['publisher_id'] = $payment['publisher_id'];
                try
                {
                    $this->insert($data);
                    $results[] = [
                        'ws1c_id' => $payment['ws1c_id'],
                        'year'    => $payment['year'],
                        'result'  => 'success'
                    ];
                }
                catch(\Exception $e)
                {
                    $results[] = [
                        'ws1c_id' => $payment['ws1c_id'],
                        'year'    => $payment['year'],
                        'result'  => $e->getMessage()
                    ];
                    //throw $e;
                    continue;
                }
            }
            else
            {//обновление?
                if(!empty($payment['deleted']) && $check->status!=-3)
                {
                    $data = [
                        'status'         => -3,
                        'enabled'        => 0,
                        'result_comment' => 'удалено запросом из 1С'
                    ];

                    //$cur_result = !empty($check['result']) ? unserialize($check['result']) : '';
                    //$data['result'] = serialize(array_merge(array('error' => 'deleted by 1c request'), array('old_result' => $cur_result)));

                    $cur_request = !empty($check->request_data) ? unserialize($check->request_data) : '';
                    $data['request_data'] = serialize(array_merge($payment, ['old_request' => $cur_request]));

                    $this->update($data, ['id' => $check->id]);
                    $results[] = [
                        'ws1c_id' => $payment['ws1c_id'],
                        'year'    => $payment['year'],
                        'result'  => 'success'
                    ];
                }
                else
                {
                    $results[] = [
                        'ws1c_id' => $payment['ws1c_id'],
                        'year'    => $payment['year'],
                        'result'  => 'exists'
                    ];
                }
            }
        }

        return $results;
    }

    public function savePaymentFromTransactions(array $params)
    {
        if(empty($params['ids']))
        {
            throw new \Exception(__METHOD__ . ': empty transactions ids');
        }
        $transactions = $this->transactionModel->getTransactions(['id' => $params['ids']])->toArray();

        $results = [];
        foreach($transactions as $transaction)
        {
            if(empty($transaction['order_id']))
            {
                $results[] = [
                    'id'     => $transaction['id'],
                    'result' => 'empty order id'
                ];
                continue;
            }
            if(empty($transaction['sys_status']) || $transaction['sys_status']!=2)
            {
                $results[] = [
                    'id'     => $transaction['id'],
                    'result' => 'unpayed'
                ];
                continue;
            }
            if(empty($transaction['sum']) || $transaction['sum']==0)
            {
                $results[] = [
                    'id'     => $transaction['id'],
                    'result' => 'zero sum'
                ];
                continue;
            }
            $check = $this->getBy(['transaction_id' => $transaction['id']]);
            if(!empty($check))
            {
                $results[] = [
                    'id'     => $transaction['id'],
                    'result' => 'exists'
                ];
                continue;
            }
            $order = $this->orderService->mapper->getOrderBy([
                'columns' => ['id', 'company_id', 'payment_docno', 'price_id'],
                'id'      => $transaction['order_id']
            ]);
            if(empty($order))
            {
                $results[] = [
                    'id'     => $transaction['id'],
                    'result' => 'empty order'
                ];
                continue;
            }
            $ordersOwner = $this->orderModel->getOwner($order->id);

            if(!$ordersOwner || empty($ordersOwner)) {
                $results[] = [
                    'id'     => $transaction['id'],
                    'result' => 'Unknown order`s owner (publisher)',
                ];
                continue;
            }
            $publisherId = $ordersOwner['id'];

            $data['created']          = date('Y-m-d H:i:s');
            $data['date']             = $transaction['date'];
            $data['year']             = date('Y', strtotime($transaction['date']));
            $data['sum']              = $transaction['sum'];
            $data['transaction_id']   = $transaction['id'];
            $data['payment_agent_id'] = 47;//ООО ОС
            $data['label']            = 'transaction_payment';
            $data['order_id']         = $order->id;
            $data['publisher_id']     = $publisherId;
            //$data['contractor_id']    = $order->company_id;//0 если физ. лицо
            $data['docno']            = $order->payment_docno;

            $this->insert($data);
            $results[] = [
                'id'     => $transaction['id'],
                'result' => 'success'
            ];
        }
        return $results;
    }

    public function getBy(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['P'=>$this->table])
            ->columns([
                'id',
                'ws1c_id',
                'order_id',
                'sum',
                'date',
                'year',
                'docno',
                'purpose',
                'comment',
                'payment_agent_id',
                'contractor_id',
                'contractor_name',
                'contractor_inn',
                'contractor_kpp',
                'result',
                'result_comment',
                'request_data',
                'status',
                'enabled',
                'created',
                'last_updated',
                'publisher_id',
            ]);
        if(!empty($params['ws1c_id']))
        {
            $select->where(['P.ws1c_id' => $params['ws1c_id']]);
        }
        if(!empty($params['publisher_id']))
        {
            $select->where(['P.publisher_id' => intval($params['publisher_id'])]);
        }
        if(!empty($params['transaction_id']))
        {
            $select->where(['P.transaction_id' => (int)$params['transaction_id']]);
        }
        if(!empty($params['id']))
        {
            $select->where(['P.id' => (int)$params['id']]);
        }
        if(!empty($params['year']))
        {
            $select->where->literal('YEAR(P.date) = ?', (int)$params['year']);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function getPayments(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $publisherId = $this->publisher_id;
        $select = $sql->select()
            ->from(['P'=>$this->table])
            ->columns([
                'id',
                'ws1c_id',
                'order_id',
                'transaction_id',
                'sum',
                'date',
                'year',
                'docno',
                'purpose',
                'comment',
                'payment_agent_id',
                'contractor_id',
                'result',
                'result_comment',
                'request_data',
                'label',
                'enabled',
                'status',
                'created',
                'last_updated',
                'publisher_id'
            ])
            ->join(['PA' => 'nx_payment_agent'], 'PA.id = P.payment_agent_id', ['payment_agent_ws1c_id' => 'ws1c_id'])
            ->join(['C' => 'nx_company'], 'C.id = P.contractor_id', ['contractor_inn' => 'inn', 'contractor_kpp' => 'kpp', 'contractor_name' => 'name'], 'left')
            //->join(array('O' => 'nx_order'), 'O.id = P.order_id', array())
            ->group('P.id');
        if (!empty($publisherId)) {
            $select->where(['P.publisher_id' => $publisherId]);
        } else {
//            $select->where(' 0=1 '); //Ничего не выводить вообще
        }

        if(!empty($params['order'])) {
            $select->order($params['order']);
        } else {
            $select->order('P.id DESC');
        }
        if(!empty($params['id'])) {
            $select->where(['P.id' => $params['id']]);
        }
        if(isset($params['status'])) {
            $select->where(['P.status' => (int)$params['status']]);
        }
        if(isset($params['transaction_id'])) {
            $select->where(['P.transaction_id' => (int)$params['transaction_id']]);
        }
        if(!empty($params['label'])) {
            $select->where(['P.label' => $params['label']]);
        }
        if(isset($params['is_transaction_id'])) {
            if((int)$params['is_transaction_id']) {
                $select->where('P.transaction_id IS NOT NULL AND P.transaction_id<>0');
            } else {
                $select->where('(P.transaction_id IS NULL OR P.transaction_id=0)');
            }
        }
        if(!empty($params['is_order_id'])) {
            $select->where('P.order_id IS NOT NULL AND P.order_id<>0');
        }
        if(!empty($params['isset_ws1c_id'])) {
            $select->where('P.ws1c_id<>""');
        }
        if(!empty($params['isset_sum'])) {
            $select->where->notEqualTo('P.sum', 0);
        }
        if(isset($params['enabled'])) {
            $select->where(['P.enabled' => (int)$params['enabled']]);
        }
        if(!empty($params['limit'])) {
            $select->limit($params['limit']);
        }

        if(!empty($params['updated_end'])) {
            $select->where->lessThanOrEqualTo('P.last_updated', $params['updated_end']);
        }
//        $selectString = $sql->getSqlStringForSqlObject($select);
        @$selectString = $sql->buildSqlString($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function sendTransactionTo1c(array $params = [])
    {
        //$cur_time    = time();
        //$updated_end = date('Y-m-d', mktime(0, 0, 0, date('n', $cur_time), date('j', $cur_time)-1, date('Y', $cur_time)));
        $by = ['status' => 0, 'is_transaction_id' => 1, 'is_order_id' => 1, 'isset_sum' => 1, 'label' => 'transaction_payment', 'limit' => 25];//, 'order' => 'P.last_updated ASC');
        if (!empty($params['ids'])) {
            $by['id'] = $params['ids'];
        }
        $payments = $this->getPayments($by);
        $payments = $payments->toArray();

        $payments_by_transaction = [];
        $transaction_ids = [];
        foreach($payments as $i => $payment) {
            $transaction_ids[] = $payment['transaction_id'];
            $payments_by_transaction[$payment['transaction_id']] = $payment;
        }
        $transactions = $this->transactionModel->getTransactions(['id' => $transaction_ids, 'any_publisher' => true]);
        $transactions = $transactions->toArray();
        $transactions_by_id = [];
        foreach($transactions as $transaction) {
            $transactions_by_id[$transaction['id']] = $transaction;
        }

        $payments_data = [];
        foreach($payments as $i => $payment) {
            $payment_data = [];

            $order = $this->orderService->mapper->getOrderBy(['id' => $payment['order_id'], 'columns' => ['id', 'company_id','price_id']]);
            //$subscription = $this->subscriptionService->mapper->getBy(array('id' => $payment['order_id']));
            if(empty($order)) {
                $this->update(['status' => -1, 'result_comment' => 'не найден заказ с id = ' . $payment['order_id'], 'result' => serialize(['error' => 'no order'])], ['id' => $payment['id']]);
                continue;
            }

            $publisher = $this->orderModel->getOwner($order->id);

            if(!$publisher || empty($publisher)) {
                $this->update(['status' => -1, 'result_comment' => 'Неизвестен владелец платежа (издатель)', 'result' => serialize(['error' => 'no publisher'])], ['id' => $payment['id']]);
                continue;
            }

            if( !isset($publisher['1c_integration_verified']) || !$publisher['1c_integration_verified'] ) {
                $this->update(['status' => -1, 'result_comment' => 'Интеграция с 1с для издателя ' . $publisher['id'] . ' не прошла тестирование.', 'result' => serialize(['error' => '1c integration is not verified'])], ['id' => $payment['id']]);
                continue;
            }
            $soapOptions = isset($publisher['integration_1c']) ? $publisher['integration_1c'] : []; // данные для подключения к 1с веб сервису издателя, которому принадлежит этот заказ

            if(empty($payment['publisher_id'])) {
                $this->update(['publisher_id' => intval($publisher['id'])], ['id' => $payment['id']]);
//                continue;
            }

            //$items = $this->subscriptionService->mapper->getItems(array('subscription_id'=>$subscription->id));
            $items = $this->orderService->mapper->itemModel->getItemsBy([
                'order_id'     => $order->id,
                'columns_load' => [
                    'full_price',
                    'item_data' => ['nx_subscription' => ['type']]
                ],

            ]);
            if(empty($items)) {
                $this->update(['status' => -1, 'result_comment' => 'отсутствуют позиции у заказа с id = ' . $payment['order_id'], 'result' => serialize(['error' => 'no items'])], ['id' => $payment['id']]);
                continue;
            }

            $total_value = 0;
            $paper_value = 0;
            $pdf_value   = 0;
            $is_pdf      = 0;
            $is_paper    = 0;
            foreach($items as $item) {
                $value = $item->full_price;
                if($value) {
                    if($item->item_type=='nx_subscription') {
                        if($item->item_data->type=='paper') {
                            $is_paper = 1;
                            $paper_value+=$value;
                        }
                        if($item->item_data->type=='pdf') {
                            $is_pdf = 1;
                            $pdf_value+=$value;
                        }
                    }
                    if($item->item_type=='Issue') {
                        $is_pdf = 1;
                        $pdf_value+=$value;
                    }
                }
                /*$value = $item['full_price'];
                if($item['type']=='paper' && $value)
                {
                    $is_paper = 1;
                    $paper_value+=$value;
                }
                if($item['type']=='pdf' && $value)
                {
                    $is_pdf = 1;
                    $pdf_value+=$value;
                }*/
            }
            $total_value = $paper_value + $pdf_value;
            if (!\nx\functions\isFloatEqual((float)$total_value, (float)$payment['sum'])) {
                $this->update(['status' => -1, 'result_comment' => 'сумма заказа (' . $total_value . ') не совпадает с суммой платежа (' . $payment['sum'] . ')', 'result' => serialize(['error' => 'payment (' . $payment['sum'] . ') and order (' . $total_value . ') sums mismatch'])], ['id' => $payment['id']]);
                continue;
            }
            $payment_sum = [];
            if($is_pdf && $is_paper) {
                $payment_sum[] = ['PaymentSum' => $pdf_value, 'NDS' => \nx\NDS_SERVICE];
                $payment_sum[] = ['PaymentSum' => $paper_value, 'NDS' => \nx\NDS_PRODUCT];
            } else {
                if($is_pdf) {
                    $payment_sum = ['PaymentSum' => $pdf_value, 'NDS' => \nx\NDS_SERVICE];
                }
                if($is_paper) {
                    $payment_sum = ['PaymentSum' => $paper_value, 'NDS' => \nx\NDS_PRODUCT];
                }
            }
            if(empty($payment_sum)) {
                $this->update(['status' => -1, 'result_comment' => 'неизвестный ндс для позиции заказа', 'result' => serialize(['error' => 'empty payment sum'])], ['id' => $payment['id']]);
                continue;
            }

            if(empty($payment['payment_agent_ws1c_id'])) {
                $this->update(['status' => -1, 'result_comment' => 'неопределен платежный агент', 'result' => serialize(['error' => 'empty payment_agent_ws1c_id'])], ['id' => $payment['id']]);
                continue;
            }
            $organization = ['ID' => $payment['payment_agent_ws1c_id']];
            if(empty($transactions_by_id[$payment['transaction_id']])) {//log error? check for inn, kpp, name
                $this->update(['status' => -1, 'result_comment' => 'не найдена транзакция  с id = ' . $payment['transaction_id'], 'result' => serialize(['error' => 'empty transaction'])], ['id' => $payment['id']]);
                continue;
            }
            $acquirer = [
                'INN'  => $transactions_by_id[$payment['transaction_id']]['system_inn'],//'7750005725',//
                'KPP'  => $transactions_by_id[$payment['transaction_id']]['system_kpp'],//'775001001',//
                'Name' => $transactions_by_id[$payment['transaction_id']]['system_desc']//'Яндекс'//
            ];
            //if(empty($payment['contractor_id']))
            if(empty($order->company_id)) {
                $contractor = [
                    'INN'  => '0000000000',
                    'KPP'  => '000000000',
                    'Name' => ''
                ];
            } else {
                $contractor = $this->companyModel->getCompanyBy(['id' => $order->company_id, 'columns' => ['inn','kpp','name']]);
                if(empty($contractor->inn))
                {//log error? check for inn, kpp, name
                    $this->update(['status' => -1, 'result_comment' => 'отсутствует ИНН у контрагента с id = ' . $order->company_id, 'result' => serialize(['error' => 'empty contractor inn'])], ['id' => $payment['id']]);
                    continue;
                }
                $contractor = [
                    'INN'  => $contractor->inn,
                    'KPP'  => $contractor->kpp ? $contractor->kpp : '000000000',
                    'Name' => $contractor->name
                ];
            }
            if(empty($payment['date']) || $payment['date']=='0000-00-00 00:00:00') {
                $this->update(['status' => -1, 'result_comment' => 'отутствует дата платежа', 'result' => serialize(['error' => 'empty date'])], ['id' => $payment['id']]);
                continue;
            }
            $document_date = date('Y-m-d', strtotime($payment['date']));
            $payment_data = [
                'Organization'  => $organization,//array('ID' => 'ТакойНет'),//тот на кого платеж
                'Acquirer'      => $acquirer,//тот через кого платеж
                'Contractor'    => $contractor,//array('INN' => '0000000000', 'KPP' => '000000000', 'Name' => 'test'),//тот кто оплатил
                'PaymentRowSum' => $payment_sum,
                'IDTransaction' => $payment['transaction_id'],
                'document_date' => $document_date,
            ];
//            $payments_data[] = $payment_data;
            $payments_data[ $publisher['id'] ] [] = $payment_data; //разбросать по издателям

            if (!isset($payments_data[ $publisher['id'] ] ['soapOptions']))
                $payments_data[ $publisher['id'] ] ['soapOptions'] = $soapOptions; //рядом с данными о платежах временно сохраним данные для подключения к 1с
        }
        $payments_data_by_transaction = [];
//        foreach($payments_data as $payment_data) {
        foreach($payments_data as $publisherId => $payment_data) {
            foreach($payment_data as $pd) {
                if (isset($pd['IDTransaction']))
                    $payments_data_by_transaction[$pd['IDTransaction']] = $pd;
            }
//            $payments_data_by_transaction[$payment_data['IDTransaction']] = $payment_data;
        }

        if(!empty($payments_data)) {
            foreach($payments_data as $publisherId => &$payment_data) {
                $soapClientOptions = $payment_data['soapOptions']; // данные для подключения к 1с этого издателя
                unset($payment_data['soapOptions']); //данные хранились временно, поэтому снесем их

                try {
    //                $client = $this->ws1c_client;
                    $client = new  \nx\Service\Ws1c(2, $soapClientOptions);
                    if (!$client) {
                        continue; // в случае неудачного создания экземпляра soap клиента не пытемся ничего никуда отправлять для этого издателя
                        //TODO: Нужно как-то логировать такие случаи
                    }

                    $result = $client->PaymentFromNxTo1C(
                        [
                            'Payments' => $payment_data
                        ]
                    );
                    $result = $result->return->ResponseRow;
                    if(is_object($result))
                    {
                        $result = [$result];
                    }
                    foreach($result as $item)
                    {
                        $item = (array)$item;
                        if(empty($item['IDTransaction'])) {
                            continue;
                        }
                        $cur_result = $payments_by_transaction[$item['IDTransaction']]['result'];
                        if($cur_result) {
                            $cur_result = unserialize($cur_result);
                        }
                        if(!(int)$item['IsError'] || empty($cur_result['Decoding']) || $item['Decoding'] != $cur_result['Decoding']) {
                            $data['request_data']   = serialize($payments_data_by_transaction[$item['IDTransaction']]);
                            $data['status']         = $item['IsError'] ? -1 : 1;//sended
                            $data['result']         = serialize($item);
                            $data['result_comment'] = $item['Decoding'];
                            $this->update($data, ['transaction_id' => $item['IDTransaction']]);
                        } else {
                            if(time() - strtotime($payments_by_transaction[$item['IDTransaction']]['last_updated']) > 7*24*60*60) {//fail by time
                                $this->update(['status' => -2, 'result_comment' => 'fail by time'], ['transaction_id' => $item['IDTransaction']]);
                            }
                        }
                    }
                } catch(\Exception $e) {
                    var_dump($e->getMessage());
                }

            }
        }
    }

    public function sendNdsTo1c(array $params = [])
    {
        $results = [];
        $filters = ['status' => 0, 'is_transaction_id' => 0, 'is_order_id' => 1, 'isset_sum' => 1, 'isset_ws1c_id' => 1, 'label' => 'ws1c_payment', 'enabled' => 1];
        if(!empty($params['id'])) {
            $filters['id'] = (int)$params['id'];
        }
        $payments = $this->getPayments($filters);


        $payments = $payments->toArray();

        $payments_by_ws1c = [];
        foreach ($payments as $i => $payment) {
            $payments_by_ws1c[$payment['year'] . '_' . $payment['ws1c_id']] = $payment;
        }

        $payments_data = [];
        //$payments_data_by_ws1c2 = array();
        foreach ($payments as $i => $payment) {
            $payment_data = [];

            $order = $this->orderService->mapper->getOrderBy(['id' => $payment['order_id'], 'columns' => ['id', 'shop_id', 'company_id']]);
            //$subscription = $this->subscriptionService->mapper->getBy(array('id' => $payment['order_id']));
            if (empty($order)) {
                $results[$payment['id']] = 'не найден заказ с id = ' . $payment['order_id'];
                $this->update(['status' => -1, 'result_comment' => $results[$payment['id']], 'result' => serialize(['error' => $results[$payment['id']]])], ['id' => $payment['id']]);
                continue;
            }

            $publisher = $this->orderModel->getOwner($order->id);
            if (!$publisher || empty($publisher)) {
                $results[$payment['id']] = 'Издатель-владелец заказа ' . $payment['order_id'] . ' не найден.';
                $this->update(['status' => -1, 'result_comment' => $results[$payment['id']], 'result' => serialize(['error' => $results[$payment['id']]])], ['id' => $payment['id']]);
                continue;
            }

            $integrationVerified = isset($publisher['1c_integration_verified']) ? $publisher['1c_integration_verified'] : false;
            if (!$integrationVerified) {
                $results[$payment['id']] = 'Интеграция с 1с сервисом издателя ' . $publisher['name'] . ' не прошло тестирование.';
                $this->update(['status' => -1, 'result_comment' => $results[$payment['id']], 'result' => serialize(['error' => $results[$payment['id']]])], ['id' => $payment['id']]);
                continue;
            }

            $soapOptions = isset($publisher['integration_1c']) ? $publisher['integration_1c'] : false;
            if (!$soapOptions) {
                $results[$payment['id']] = 'Данные для подключения к веб сервису 1с для издателя ' . $publisher['name'] . ' не найдены.';
                $this->update(['status' => -1, 'result_comment' => $results[$payment['id']], 'result' => serialize(['error' => $results[$payment['id']]])], ['id' => $payment['id']]);
                continue;
            }

            //$items = $this->subscriptionService->mapper->getItems(array('subscription_id'=>$subscription->id));
            $items = $this->orderService->mapper->itemModel->getItemsBy([
                'order_id'     => $order->id,
                'columns_load' => [
                    'full_price',
                    'item_data' => ['nx_subscription' => ['type']]
                ],
            ]);
            if (empty($items)) {
                $results[$payment['id']] = 'отсутствуют позиции у заказа с id = ' . $payment['order_id'];
                $this->update(['status' => -1, 'result_comment' => $results[$payment['id']], 'result' => serialize(['error' => $results[$payment['id']]])], ['id' => $payment['id']]);
                continue;
            }

            $total_value  = 0;
            $paper_value  = 0;
            $pdf_value    = 0;
            $is_pdf       = 0;
            $is_paper     = 0;
            foreach($items as $item) {
                $value = $item->full_price;
                if($value) {
                    if($item->item_type=='nx_subscription') {
                        if($item->item_data->type=='paper') {
                            $is_paper = 1;
                            $paper_value+=$value;
                        }
                        if($item->item_data->type=='pdf') {
                            $is_pdf = 1;
                            $pdf_value+=$value;
                        }
                    }
                    if($item->item_type=='Issue') {
                        $is_pdf = 1;
                        $pdf_value+=$value;
                    }
                }
            }
            $total_value = $paper_value + $pdf_value;
            $is_match_sums = 1;
            if(!\nx\functions\isFloatEqual((float)$total_value, (float)$payment['sum'])) {
                //$results[$payment['id']] = 'сумма заказа (' . $total_value . ') не совпадает с суммой платежа (' . $payment['sum'] . ')';
                //$this->update(array('status' => -1, 'result' => serialize(array('error' => $results[$payment['id']]))),array('id' => $payment['id']));
                //continue;
                $is_match_sums = 0;
            }
            $payment_sum = [];
            if($is_pdf && $is_paper) {
                $k = $pdf_value / $paper_value;
                $pdf_sum   = round($payment['sum'] / (1 + 1/$k), 2);
                $paper_sum = $payment['sum'] - $pdf_sum;
                $payment_sum[] = ['PaymentSum' => $pdf_sum, 'NDS' => \nx\NDS_SERVICE];
                $payment_sum[] = ['PaymentSum' => $paper_sum, 'NDS' => \nx\NDS_PRODUCT];
            } else {
                if($is_pdf) {
                    $payment_sum = ['PaymentSum' => $payment['sum'], 'NDS' => \nx\NDS_SERVICE];
                }
                if($is_paper) {
                    $payment_sum = ['PaymentSum' => $payment['sum'], 'NDS' => \nx\NDS_PRODUCT];
                }
            }
            if(empty($payment_sum)) {
                $results[$payment['id']] = 'неизвестный ндс для позиции заказа';
                $this->update(['status' => -1, 'result_comment' => $results[$payment['id']], 'result' => serialize(['error' => $results[$payment['id']]])], ['id' => $payment['id']]);
                continue;
            }

            $year = $payment['year'];
            if(!in_array($year, [date('Y'), date('Y')-1])) {
                $results[$payment['id']] = 'год платежа не актуален';
                $this->update(['status' => -1, 'result_comment' => $results[$payment['id']], 'result' => serialize(['error' => $results[$payment['id']]])], ['id' => $payment['id']]);
                continue;
            }

            if(!empty($order->company_id)) {
                $company = $this->companyModel->getCompanyBy(['id' => $order->company_id, 'columns' => ['id','inn','kpp','name'],
                    'columns_load' => [
                        'addr'  => ['phone', 'address', 'zipcode', 'country_id', 'area', 'region', 'city', 'telcode'],
                        'laddr' => ['phone', 'address', 'zipcode', 'country_id', 'area', 'region', 'city', 'telcode']
                    ]
                ]);

                if(empty($company->id)) {
                    $results[$payment['id']] = 'NX: не найдена компания у заказа с id = ' . $payment['order_id'];
                    $this->update(['status' => -1, 'result_comment' => $results[$payment['id']], 'result' => serialize(['error' => $results[$payment['id']]])], ['id' => $payment['id']]);
                    continue;
                }

                $contractor = [
                    'INN'  => $company->inn,
                    'KPP'  => $company->kpp ? $company->kpp : '',//'000000000'
                    'Name' => $company->name
                ];

                $phone   = !empty($company->addr->phone) ? $company->addr->phone : ( !empty($company->laddr->phone) ? $company->laddr->phone : '' );
                $telcode = !empty($company->addr->telcode) ? $company->addr->telcode : ( !empty($company->laddr->telcode) ? $company->laddr->telcode : '' );
                $phone   = [
                    'Code'   => $telcode,
                    'Number' => $phone
                ];

                if(empty($company->addr->address) && empty($company->laddr->address)) {
                    $results[$payment['id']] = 'NX: не заполнен адрес у компании с id = ' . $order->company_id;
                    $this->update(['status' => -1, 'result_comment' => $results[$payment['id']], 'result' => serialize(['error' => $results[$payment['id']]])], ['id' => $payment['id']]);
                    continue;
                }
                $address_fields = ['zipcode', 'area', 'region', 'city', 'city1', 'address'];
                $actual_address = '';
                foreach($address_fields as $field) {
                    $actual_address .= (!empty($company->addr->$field) ? $company->addr->$field . ', ' : '');
                }
                $actual_address = mb_substr($actual_address, 0, -2);

                $legal_address = '';
                foreach($address_fields as $field) {
                    $legal_address .= (!empty($company->laddr->$field) ? $company->laddr->$field . ', ' : '');
                }
                $legal_address = mb_substr($legal_address, 0, -2);

                if(empty($company->addr->address)) {
                    $actual_address = $legal_address;
                }
                if(empty($company->laddr->address)) {
                    $legal_address = $actual_address;
                }
            } else {
                $contractor = [
                    'INN'  => '0000000000',
                    'KPP'  => '000000000',
                    'Name' => ''
                ];
                $legal_address = '';
                $actual_address = '';
                $phone   = [
                    'Code'   => '',
                    'Number' => ''
                ];
            }

            $contact = [
                'Contractor' => $contractor,
                'LegalAddr'  => $legal_address,
                'ActualAddr' => $actual_address,
                'Phone'      => $phone
            ];

            $payment_data = [
                'IDDocument'    => $payment['ws1c_id'],
                'Year'          => $year,
                'PaymentRowSum' => $payment_sum,
                'Contact'       => $contact
            ];

//            $payments_data[] = $payment_data;
            $payments_data[$publisher['id']][] = $payment_data;
            if (!isset($payments_data[$publisher['id']]['soapOptions']))
                $payments_data[ $publisher['id'] ] ['soapOptions'] = $soapOptions;

            $payments_by_ws1c[$payment_data['Year'] . '_' . $payment_data['IDDocument']]['is_match_sums'] = $is_match_sums;
            $payments_by_ws1c[$payment_data['Year'] . '_' . $payment_data['IDDocument']]['total_value']   = $total_value;
            $payments_by_ws1c[$payment_data['Year'] . '_' . $payment_data['IDDocument']]['order']         = $order;
            //$payments_by_ws1c[$payment_data['Year'] . '_' . $payment_data['IDDocument']]['pdf_item_ids']  = $pdf_item_ids;
        }
        $payments_data_by_ws1c = [];
        foreach($payments_data as $publisherId => $payment_data) {
            foreach($payment_data as $pd) {
                if ($pd['Year'] && $pd['IDDocument'] )
                    $payments_data_by_ws1c[$pd['Year'] . '_' . $pd['IDDocument']] = $pd;
            }
        }
        if(!empty($payments_data)) {
            foreach($payments_data as $publisherId => &$payment_data) {
                $soapClientOptions = $payment_data['soapOptions'];
                unset($payments_data['soapOptions']);

                try {
//                    $client = $this->ws1c_client;
                    $client = new \nx\Service\Ws1c( 2, $soapClientOptions);
                    if (!$client) {
                        continue;
                    }
                    $this->logger->log_console($payment_data);
                    $result = $client->PaymentOrderFromNx(
                        [
                            'PaymentOrder' => $payment_data
                        ]
                    );
                    $this->logger->log_console($result);
                    $result = $result->return->ResponsePaymentOrderRow;
                    if(is_object($result)) {
                        $result = [$result];
                    }
                    $stopStatuses = [//-1 = nx error, -2 = fail by time, -3 = delete by 1c request
                        'NoDoc'              => -4,
                        'DocNotSubscription' => -5,
                        'DocDeleted'         => -6
                    ];
                    foreach($result as $item) {
                        $item = (array)$item;
                        if(empty($item['Number']) || empty($item['Year'])) {
                            continue;
                        }
                        $cur_id = $payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['id'];
                        if(!$payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['is_match_sums']) {
                            $item['Comment'] .= ' Суммы не совпадают (' . $payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['total_value'] . ').';
                        }
                        $results[$cur_id] = $item['Comment'];
                        $cur_status = $payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['status'];
                        $cur_result = $payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['result'];
                        if(!empty($cur_result)) {
                            $cur_result = unserialize($cur_result);
                        }
                        $cur_request    = $payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['request_data'];
                        $cur_request    = !empty($cur_request) ? unserialize($cur_request) : '';
                        $success_status = ($payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['is_match_sums'] ? 1 : 2);
                        $new_status     = $item['IsError'] ? (in_array($item['Status'], array_keys($stopStatuses)) ? $stopStatuses[$item['Status']] : 0) : $success_status;//sended
                        if(!(int)$item['IsError'] || !isset($cur_result['Status']) || $item['Status'] != $cur_result['Status'] || $new_status != $cur_status) {
                            $request_data = $payments_data_by_ws1c[$item['Year'] . '_' . $item['Number']];
                            if(!empty($cur_request)) {
                                $request_data = array_merge($request_data, ['old_request' => $cur_request]);
                            }
                            $data['request_data']   = serialize($request_data);
                            $data['status']         = $new_status;
                            $data['result']         = serialize($item);
                            $data['result_comment'] = $item['Comment'];
                            if($new_status==-6) {
                                $data['enabled'] = 0;
                            }
                            $this->update($data, ['year' => $item['Year'], 'ws1c_id' => $item['Number']]);
                        } else {
                            if(time() - strtotime($payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['last_updated']) > 7*24*60*60) {//fail by time
                                $this->update(['status' => -2, 'result_comment' => 'fail by time'], ['year' => $item['Year'], 'ws1c_id' => $item['Number']]);
                            }
                        }
                        if(!(int)$item['IsError']) {
                            $order_data = ['orderData' => [
                                'payment_sum'      => $payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['sum'],
                                'payment_agent_id' => $payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['payment_agent_id'],
                                'payment_date'     => $payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['date'],
                                'order_state_id'   => 3,//оплачено
                                'enabled'          => 1
                            ]];
                            $this->orderService->mapper->updateOrder($payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['order_id'], $order_data);
                            $template_id = 6;
                            if($payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['order']->shop_id == 2) {
                                $template_id = 10;
                            }
                            $this->orderService->getAndSendMailMessage($payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['order_id'], $template_id);
    //                        $this->subscriptionService->tryToRegisterUser(array('id' => $payments_by_ws1c[$item['Year'] . '_' . $item['Number']]['order_id'], 'from' => 'nx_payment_1c'));
                        }
                    }
                } catch(\Exception $e) {
                    //var_dump($e->getMessage(), $payments_data);
                    throw $e;
                    //return false;
                }
            }
        }
        return $results;
    }

    public function updatePayment($data, $where)
    {
        $payment = $this->getBy($where);
        if(empty($payment))
        {
            return ['error' => 'платеж не найден'];
        }

        if(!empty($data['order_id']))
        {
            $order = $this->orderService->mapper->getOrderBy(['id' => $data['order_id'], 'columns' => ['id', 'company_id']]);
            if(empty($order))
            {
                return ['error' => 'заказа с id = ' . $data['order_id'] . ' не существует'];
            }
            if(!empty($order->company_id))
            {
                $company = $this->companyModel->getByID($order->company_id);
                if(empty($company) || empty($company->inn) || empty($company->name) || ( empty($company->address) && empty($company->la_address) ) )
                {
                    return ['error' => 'не заполнены данные (ИНН, название, адрес) об юр.лице id = ' . $order->company_id];
                }
                if(empty($company->ws1c_synced))
                {
                    $this->companyService->sendTo1c(['ids' => [$order->company_id]]);
                }
            }
            /*if($payment->sum!=$order->price)
            {
                return array('error' => 'cумма заказа = ' . $order->price . ' не совпадает с суммой платежа = ' . $payment->sum);
            }*/
        }

        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            $this->update($data, $where);

            $this->logger->log('Обновление платежа #' . $where['id'], 'update_payment', $data, $data['last_user_id']);

            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return ['error' => ''];
        }
        catch(\Exception $e)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
        }
    }

    public function checkMatchOrderSum($payment_id, $order_id)
    {
        $payment = $this->getBy(['id' => $payment_id]);
        if(empty($payment))
        {
            return '';
        }

        //$items = $this->subscriptionService->mapper->getItems(array('subscription_id'=>$order_id));
        $items = $this->orderService->mapper->itemModel->getItemsBy([
                'order_id'     => $order_id,
                'columns_load' => [
                    'full_price'
                ],
            ]
        );

        $total_value = 0;
        if(!empty($items))
        {
            foreach($items as $item)
            {
                $total_value += $item->full_price;
            }
        }
        if(\nx\functions\isFloatEqual((float)$total_value, (float)$payment->sum))
        {
            return '';
        }
        return 'Сумма заказа = ' . $total_value . ' не совпадает с суммой платежа = ' . $payment->sum . '.';
    }

    public function syncCompany(array $params)
    {
        $payment = $this->getBy(['id' => $params['payment_id']]);
        if(empty($payment->order_id)) {
            return false;
        }
        $order   = $this->subscriptionService->mapper->getBy(['id' => $payment->order_id]);
        if(empty($order->company_id)) {
            return false;
        }
        $data = ['companyData' => [
            'inn' => $payment->contractor_inn,
            'kpp' => $payment->contractor_kpp,
            'last_user_id' => !empty($params['user_id']) ? $params['user_id'] : 0
        ]];
        return $this->companyModel->updateCompany($order->company_id, $data);
    }
}
