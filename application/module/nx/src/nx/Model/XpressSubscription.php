<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class XpressSubscription extends AbstractTable
{
    //public $cache;
    //public $addressModel, $personModel, $companyModel, $itemModel, $logger, $actionModel, $nxdb;

    private $data = [];

    public function __construct(Adapter $adapter)
    {
        //$this->table   = 'subscription';
        //$this->adapter = $adapter;
        parent::__construct('subscription', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\XpressSubscription));
    }

    public function getXpressList($offset, $limit, $params = [])
    {
        //$cache = $this->getCachedData(__FUNCTION__, func_get_args());
        //if ($cache) return $cache;

        $columns = [
            new Expression('SQL_CALC_FOUND_ROWS S.id AS id'),
            'total',
            'payment_sum',
            'payment_date',
            'is_payed' => new Expression('IF(S.payment_sum >= S.total OR S.type<>0, 1, 0)'),
            'delivery',
            'manager'
        ];
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['S'=>'subscription'])
            ->columns($columns)
            ->join(['I'=>'subscription_item'], 'I.subscription = S.id', [])
            ->join(['M'=>'issue'], 'M.id = I.issue', ['periodical_names' => new Expression('GROUP_CONCAT(DISTINCT M.sname)')])
            ->where(['M.id' => [
                '1',//мир пк + dvd
                '2', //computeworld
                '3', //классный журнал
                '4', //lan
                '5', //nets
                '6', //os
                '7', //publish
                '9', //win
                '14', //lv
                '20', //cio
                '112', //stuff
                '127', //понимашка
                '130' //whf
                ]]
            )
            ->group('S.id');

        $is_filter_name    = 0;
        //$is_filter_present = 0;
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(!empty($filter['field']))
                {
                    $filter['property'] = $filter['field'];
                }
                $filter['comparison'] = !empty($filter['comparison']) ? $filter['comparison'] : "";

                if(empty($filter['value']) && !in_array($filter['property'], ['is_payed', 'is_present']))
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'delivery':
                        $select->where->equalTo('S.'.$filter['property'], $filter['value']);
                        break;
                    case 'id':
                    case 'total':
                    case 'payment_sum':
                    case 'payment_date':
                    case 'manager':
                        $select->where->like('S.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'name':
                        $is_filter_name = 1;
                        $select
                            ->join(['P'=>'person'],'P.id = S.customer_id', [], 'left')
                            ->join(['E'=>'employee'],'E.id = S.customer_id', [], 'left')
                            ->join(['C'=>'company'],'C.id = S.company', [], 'left');
                        $columns['name'] = new Expression('CONCAT(IF(S.customer_class=1, "", IFNULL(C.name,"")), " ", IF(S.customer_class=1, IFNULL(P.name,""), IF(S.customer_class=2, "", IFNULL(E.name,""))))');
                        $select->columns($columns);
                        $select->where->literal('CONVERT(CONCAT(IF(S.customer_class=1, "", IFNULL(C.name,"")), " ", IF(S.customer_class=1, IFNULL(P.name,""), IF(S.customer_class=2, "", IFNULL(E.name,"")))) USING utf8) LIKE ?', '%' . $filter['value'] . '%');
                        break;
                    case 'periodical_id':
                        $select->where->equalTo('M.id', $filter['value']);
                        break;
                    /*case 'is_active':
                        $select
                            ->join(array('IR'=>'subscription_item_release'),'IR.parent = I.id', array())
                            ->join(array('R'=>'release'),'R.id = IR.release', array())
                            ->having->literal('(S.payment_sum ' . ( $filter['value'] ? '>=' : '<') . ' S.total ' . ( $filter['value'] ? 'AND' : 'OR') . ' MAX(R.release_date) ' . ( $filter['value'] ? '>' : '<=') . ' NOW())');
                        $columns['is_active'] = new Expression('IF((S.payment_sum >= S.total) AND (MAX(R.release_date) > NOW()), 1, 0)');
                        $select->columns($columns);
                        break;*/
                    case 'is_present':
                        //$is_filter_present = 1;
                        $select
                            ->join(['IR'=>'subscription_item_release'],'IR.parent = I.id', [])
                            ->join(['R'=>'release'],'R.id = IR.release', ['num', 'release_date'])
                            //->having->literal('R.release_date = MAX(R.release_date)')
                            ->having->literal('MAX(R.release_date) ' . ( $filter['value'] ? '>' : '<=') . ' NOW()');
                        //$select->group('R.id');
                        //$columns['is_present'] = new Expression('IF(MAX(R.release_date) > NOW(), 1, 0)');
                        //$columns['last_release'] = new Expression('R.num');
                        $select->columns($columns);
                        break;
                    case 'is_payed':
                        if($filter['value'])
                        {
                            $select->where->NEST->literal('S.payment_sum >= S.total')->OR->notEqualTo('S.type', 0)->UNNEST;
                        }
                        else
                        {
                            $select->where->NEST->literal('S.payment_sum < S.total')->AND->equalTo('S.type', 0)->UNNEST;
                        }
                        break;
                }
            }
        }
        //if(!$is_filter_active)
        //{
            $select
                ->limit($limit)
                ->offset($offset);
        //}

        $select->order('S.id DESC');

        if(!empty($params['ids']))
        {
            $select->where->In('S.id', $params['ids']);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        $subscriptions = $resultSet;//->toArray();
        $subscriptions->buffer();

        if($subscriptions->count() && !$is_filter_name)
        {//get subscriber name only for cur page subscriptions
            $filtered_subscriptions = [];
            $subscriptions_ids      = array_map(create_function('$u', 'return (int)$u["id"];'), $subscriptions->toArray());
            $sql                    = new Sql($db);
            $select                 = $sql->select()
                ->from(['S'=>'subscription'])
                ->columns([
                    'id',
                    'name' => new Expression('CONCAT(IF(S.customer_class=1, "", IFNULL(C.name,"")), " ", IF(S.customer_class=1, IFNULL(P.name,""), IF(S.customer_class=2, "", IFNULL(E.name,""))))'),
                ])
                ->join(['P'=>'person'],'P.id = S.customer_id', [], 'left')
                ->join(['E'=>'employee'],'E.id = S.customer_id', [], 'left')
                ->join(['C'=>'company'],'C.id = S.company', [], 'left')
                ->where(['S.id' => $subscriptions_ids])
                ->group('S.id');
            $selectString = $sql->getSqlStringForSqlObject($select);
            $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
            $names        = $result->toArray();
            if(!empty($names))
            {
                $names_by_subscription = [];
                foreach($names as $name)
                {
                    $names_by_subscription[$name['id']] = $name;
                }
                foreach($subscriptions as $i => $subscription)
                {
                    if(!empty($names_by_subscription[$subscription->id]))
                    {
                        $subscription->name = $names_by_subscription[$subscription->id]['name'];
                        $filtered_subscriptions[] = $subscription->toArray();
                    }
                }
            }
            $subscriptions->initialize($filtered_subscriptions);
        }

        if($subscriptions->count())// && !$is_filter_present)
        {//get is_present property only for cur page subscriptions
            $filtered_subscriptions = [];
            $subscriptions_ids      = array_map(create_function('$u', 'return (int)$u["id"];'), $subscriptions->toArray());
            $sql                    = new Sql($db);
            $select                 = $sql->select()
                ->from(['S'=>'subscription'])
                ->columns([
                    'id',
                    //'maxdate' => new Expression('MAX(R.release_date)')
                ])
                ->join(['I'=>'subscription_item'],'I.subscription = S.id', [])
                ->join(['IR'=>'subscription_item_release'],'IR.parent = I.id', [])
                ->join(['R'=>'release'],'R.id = IR.release', ['issue', 'year', 'num', 'release_date'])
                ->join(['M'=>'issue'], 'M.id = R.issue', ['sname'])
                ->where(['S.id' => $subscriptions_ids]);
                //->having(array('R.release_date' => new Expression('MAX(R.release_date)')))
                //->group('S.id');
            $selectString = $sql->getSqlStringForSqlObject($select);
            $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

            $releases = $result->toArray();
            if(!empty($releases))
            {
                $releases_by_subscription = [];
                foreach($releases as $release)
                {
                    if(empty($releases_by_subscription[$release['id']]))
                    {
                        $releases_by_subscription[$release['id']] = $release;
                    }
                    else if(strtotime($release['release_date']) > strtotime($releases_by_subscription[$release['id']]['release_date']))
                    {
                        $releases_by_subscription[$release['id']] = $release;
                    }
                }
                foreach($subscriptions as $i => $subscription)
                {
                    if(!empty($releases_by_subscription[$subscription->id]))
                    {
                        $last_release = $releases_by_subscription[$subscription->id];
                        //$subscriptions[$i]['maxdate']    = $dates_by_subscription[$subscription->id]['maxdate'];
                        $subscription->last_release = $last_release['sname'] . ' #' . $last_release['num'] . ' ' . $last_release['release_date'] . '';
                        $subscription->is_present   = strtotime($last_release['release_date']) > time() ? 1 : 0;
                        $filtered_subscriptions[] = $subscription->toArray();
                    }
                }
            }
            $subscriptions->initialize($filtered_subscriptions);
        }

        $result = [
            'items' => $subscriptions,
            'total' => $count['count']
        ];

        //if(!empty($result)) $this->setCache($result, __FUNCTION__, func_get_args());

        return $result;
    }

    public function getSubscriptions(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['S'=>'subscription'])
            ->columns([
                'id'
            ])
            ->group('S.id');
            //->limit(10);
        if(isset($params['is_present']))
        {
            $select
                ->join(['I'=>'subscription_item'], 'I.subscription = S.id', [])
                ->join(['IR'=>'subscription_item_release'],'IR.parent = I.id', [])
                ->join(['R'=>'release'],'R.id = IR.release', [])
                ->having->literal('MAX(R.release_date) ' . ( $params['is_present'] ? '>' : '<=') . ' NOW()');
        }
        if(isset($params['is_payed']))
        {
            if($params['is_payed'])
            {
                $select->where->NEST->literal('S.payment_sum >= S.total')->OR->notEqualTo('S.type', 0)->UNNEST;
            }
            else
            {
                $select->where->NEST->literal('S.payment_sum < S.total')->AND->equalTo('S.type', 0)->UNNEST;
            }
        }
        if(!empty($params['not_source']))
        {
            $select->where->notEqualTo('S.source', (int)$params['not_source']);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getBy(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['S'=>'subscription'])
            ->columns([
                'id',
                'total',
                'payment_sum',
                'payment_date',
                'payment_docno',
                'is_payed'      => new Expression('IF(S.payment_sum >= S.total OR S.type<>0, 1, 0)'),
                'delivery',
                //'name'        => new Expression('CONCAT(IF(S.customer_class=1, "", IFNULL(C.name,"")), " ", IF(S.customer_class=1, IFNULL(P.name,""), IFNULL(E.name,"")))'),
                'person_name'   => new Expression('IF(S.customer_class=1, P.name, IF(S.customer_class=2, "", E.name))'),
                'company_name'  => new Expression('C.name'),
                'person_email'  => new Expression('IF(S.customer_class=1, P.email, IF(S.customer_class=2, "", E.email))'),
                'company_email' => new Expression('C.email'),
                'ctime',
                'comment',
                'payment_name'  => new Expression('IF(S.payment, IF(S.payment=1, "банк", "нал."), "не оплачивается")')
                //'email'       => new Expression('IF()')
            ])
            ->join(['A'=>'address'], 'A.id = S.address', ['area', 'city', 'zipcode', 'address'])
            ->join(['P'=>'person'],'P.id = S.customer_id', [], 'left')
            ->join(['E'=>'employee'],'E.id = S.customer_id', [], 'left')
            ->join(['C'=>'company'],'C.id = S.company', [], 'left')
            ->join(['D'=>'delivery'], 'D.id = S.delivery', ['delivery_name' => 'name']);
        if(!empty($params['id']))
        {
            $select->where(['S.id' => (int)$params['id']]);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function getItemsBy(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['I'=>'subscription_item'])
            ->columns([
                'id',
                'periodical_id' => 'issue',
                'byear',
                'bnum',
                'rqty',
                'qty',
                'bmonth' => new Expression('IF(R.relmonth,R.relmonth,R2.relmonth)')
            ])
            ->join(['S'=>'subscription'], 'S.id = I.subscription', [])
            ->join(['M'=>'issue'], 'M.id = I.issue', ['periodical_name' => 'sname'])
            ->join(['R'=>'release'], '(R.issue = I.issue AND R.year = I.byear AND R.num = I.bnum)', [], 'left')
            ->join(['R2'=>'release'], '(R2.issue = I.issue AND R2.year = I.byear AND R2.mnum = I.bnum)', [], 'left')
            ->order('I.id ASC');
        if(!empty($params['subscription_id']))
        {
            $select->where(['S.id' => (int)$params['subscription_id']]);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $items = $result->toArray();

        return $items;
    }

    public function exportXpressToCsv()
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['I'=>'subscription_item'])
            ->columns([
                'id',
                'subscription_id' => new Expression('S.id'),
                'name'    => new Expression('CONCAT(IF(S.customer_class=1, "", IFNULL(C.name,"")), " ", IF(S.customer_class=1, IFNULL(P.name,""), IF(S.customer_class=2, "", IFNULL(E.name,""))))'),
                'country' => new Expression('CO.name'),
                'area'    => new Expression('A.area'),
                'region'  => new Expression('A.region'),
                'city'    => new Expression('A.city'),
                'zipcode' => new Expression('A.zipcode'),
                'address' => new Expression('A.address')
            ])
            ->join(['S'=>'subscription'], 'S.id = I.subscription', [])
            ->join(['M'=>'issue'], 'M.id = I.issue', ['sname'])
            ->join(['IR'=>'subscription_item_release'],'IR.parent = I.id', [])
            ->join(['R'=>'release'],'R.id = IR.release', [])
            ->join(['P'=>'person'],'P.id = S.customer_id', [], 'left')
            ->join(['E'=>'employee'],'E.id = S.customer_id', [], 'left')
            ->join(['C'=>'company'],'C.id = S.company', [], 'left')
            ->join(['A' => 'address'],'A.id = S.address', [])
            ->join(['CO' => 'country'],'CO.id = A.country', [], 'left')
            ->where(['M.id' => [
                //'1',//мир пк + dvd
                //'2', //computeworld
                '3', //классный журнал
                //'4', //lan
                //'5', //nets
                //'6', //os
                //'7', //publish
                //'9', //win
                //'14', //lv
                //'20', //cio
                //'112', //stuff
                '127', //понимашка
                //'130' //whf
                ]]
            )
            ->group('I.id');
        //$select->where(array('S.delivery' => array('5', '20')));
        //$select->where->equalTo('S.delivery', '20');
        //$select->where->literal('YEAR(S.ctime)=2013 AND MONTH(S.ctime)=7');
        //$select->where->literal('S.payment_date IS NOT NULL AND YEAR(S.payment_date)>=2013 AND MONTH(S.payment_date)>=7 AND DAY(S.payment_date)>=18');
        $select->where->literal('R.year = 2013');
        $select->where->literal('(S.payment_sum >= S.total OR S.type<>0)');
        //$select->having->literal('MAX(R.release_date) > NOW()');
        $select->order('M.id DESC, I.id DESC');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $subscriptions = $result->toArray();

        if(!empty($subscriptions))
        {
            foreach($subscriptions as $i => $subscription)
            {
                for($j=0;$j<6;$j++)
                {
                    $subscriptions[$i]['addr' . $j] = '';
                }
                $addr = explode(',', $subscriptions[$i]['address']);
                foreach($addr as $j=>$addr_part)
                {
                    $subscriptions[$i]['addr' . $j] = $addr_part;
                }
            }
        }

        if(!empty($subscriptions))
        {
            $filtered_subscriptions = [];
            $subscriptions_ids      = array_map(create_function('$u', 'return (int)$u["id"];'), $subscriptions);
            $sql                    = new Sql($db);
            $select                 = $sql->select()
                ->from(['IR'=>'subscription_item_release'])
                ->columns(['parent'])
                ->join(['R'=>'release'],'R.id = IR.release', ['issue', 'year', 'num', 'release_date', 'relmonth'])
                //->join(array('M'=>'issue'), 'M.id = R.issue', array('sname'))
                ->where(['IR.parent' => $subscriptions_ids]);
            $select->where->literal('R.year = 2013');

            $selectString = $sql->getSqlStringForSqlObject($select);
            $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

            $releases = $result->toArray();
            if(!empty($releases))
            {
                $releases_by_subscription = [];
                foreach($releases as $release)
                {
                    if(empty($releases_by_subscription[$release['parent']]))
                    {
                        $releases_by_subscription[$release['parent']] = [$release];
                    }
                    else
                    {
                        $releases_by_subscription[$release['parent']][] = $release;
                    }
                }
                foreach($subscriptions as $i => $subscription)
                {
                    if(!empty($releases_by_subscription[$subscription['id']]))
                    {
                        for($j=1;$j<=12;$j++)
                        {
                            $subscriptions[$i]['qty' . $j] = 0;
                        }
                        foreach($releases_by_subscription[$subscription['id']] as $release)
                        {
                            $subscriptions[$i]['qty' . $release['relmonth']]++;
                        }
                        $filtered_subscriptions[] = $subscriptions[$i];
                    }
                }
            }
            $subscriptions = $filtered_subscriptions;
        }

        $result = $subscriptions;

        return $result;
    }
}
