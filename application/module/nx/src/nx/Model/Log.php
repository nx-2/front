<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Http\PhpEnvironment\RemoteAddress;

class Log extends AbstractTable
{
    //from zf source code
    const EMERG  = 0;
    const ALERT  = 1;
    const CRIT   = 2;
    const ERR    = 3;
    const WARN   = 4;
    const NOTICE = 5;
    const INFO   = 6;
    const DEBUG  = 7;

    private $user_session, $remoteAddress;
    public $userModel;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_log', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Log));

        $log_writer_mapping = [
            'timestamp' => 'created',
            'priority'  => 'priority',
            'message'   => 'title',
            'extra'     => [
                'data'    => 'data',
                'label'   => 'label',
                'user_id' => 'user_id',
                'ip'      => 'ip',
                'referer' => 'referer',
                'trace'   => 'trace'
            ]
        ];
        $log = new \Zend\Log\Logger();
        $writer = new \Zend\Log\Writer\Db($adapter, 'nx_log', $log_writer_mapping);
        $log->addWriter($writer);
        $this->logger = $log;
        $this->user_session = new \Zend\Session\Container('user');
        $this->remoteAddress = new RemoteAddress();
    }

    public function log($title, $label, $data = '', $user_id = 0, $priority = self::INFO)
    {
        $user_id = $user_id ? $user_id : $this->user_session->user_id;
        /*if(empty($user_id))
        {
            throw new \Exception('Логирование события с пустым user_id');
        }*/
        $this->logger->log($priority, $title, [
            'data'    => $data ? serialize($data) : '',
            'label'   => $label,
            'user_id' => $user_id ? $user_id : 0,
            'ip'      => ip2long($this->remoteAddress->getIpAddress()),
            'referer' => !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            'trace'   => $this->getBacktrace()
        ]);
    }

    public function log_console($data)
    {
        if(php_sapi_name()!='cli') {
            \ChromePhp::log($data);
        }
    }

    public function getBacktrace()
    {
        $trace = '';
        foreach (debug_backtrace() as $k => $v)
        {
            if (!$k)
            { //skip getBacktrace function call
                continue;
            }
            $trace .= '#' . $k . ' ' . (isset($v['file']) ? $v['file'] . '(' . $v['line'] . '): ' : '') . (isset($v['class']) ? $v['class'] . '->' : '') . $v['function'] . "\n";
        }
        return $trace;
    }

    public function loadUser($items)
    {
        $user_ids = [];
        foreach($items as $i => $item) {
            $user_ids[] = $item->user_id;
        }
        if(!empty($user_ids)) {
            /*$users = $this->userModel->select(function(\Zend\Db\Sql\Select $select) use($user_ids) {
                $select
                    ->columns(array('id' => 'User_ID', 'last_name' => 'LastName', 'first_name' => 'FirstName', 'mid_name' => 'MidName', 'login' => 'Login'))
                    ->where(array('User_ID' => $user_ids));
            });*/
            $users = $this->userModel->select(['User_ID' => $user_ids]);
            $users_by_ids = [];
            foreach($users as $user) {
                $users_by_ids[$user->id] = $user;
            }
            foreach($items as $item) {
                if($item->user_id && isset($users_by_ids[$item->user_id])) {
                    $user = $users_by_ids[$item->user_id];
                    $item->user_name = $user->last_name ? $user->last_name . ' ' . $user->first_name . ' ' . $user->mid_name : $user->login;
                }
            }
        }
    }

    public function getList($offset, $limit, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['L'=>$this->table])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS L.id AS id'),
                'user_id',
                'title',
                'created'
            ])
            ->order('L.created DESC')
            ->limit($limit)
            ->offset($offset);
        $filtered = [];
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(empty($filter['value']))
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'created':
                    case 'title':
                        $select->where->like('L.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'user_name':
                        $filtered['user_name'] = true;
                        $select->join(['U'=>'User'],'U.User_ID = L.user_id', ['user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')]);
                        $select->where->NEST
                            ->like('U.Login', '%' . $filter['value'] . '%')->OR
                            ->like('U.FirstName', '%' . $filter['value'] . '%')->OR
                            ->like('U.LastName', '%' . $filter['value'] . '%')->OR
                            ->like('U.MidName', '%' . $filter['value'] . '%')
                            ->UNNEST;
                        break;
                }
            }
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        if(empty($filtered['user_name'])) {
            $this->loadUser($resultSet);
        }

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function getBy(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['L'=>$this->table])
            ->columns([
                'id',
                'data',
                'ip',
                'referer',
                'trace',
                'created',
                'title',
                'priority',
                'label',
                'user_id'
            ])
            ->limit(1);
        if(!empty($params['id']))
        {
            $select->where(['L.id' => $params['id']]);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        $result = $resultSet->current();
        if(!empty($result->data))
        {
            $result->data = unserialize($result->data);
        }

        return $result;
    }
}
