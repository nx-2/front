<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class OrderItem extends AbstractTable
{
    public $issueModel, $priceModel;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_order_item', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\OrderItem));
    }

    public function getItemBy($params)
    {
        $params['current'] = true;
        return $this->getItemsBy($params);
    }

    public function getItemsBy(array $params)
    {
        $join_map = [//alias => table, condition, dependent_table
            'S' => ['nx_subscription', 'S.id = TBL.item_id', '', 'id', 'item_id', function($item){ return $item->item_type=='nx_subscription'; }],
            'O' => ['nx_order', 'O.id = TBL.order_id', '', 'id', 'order_id']
        ];
        $columns_map = [
            'item_data'         => ['columns' => ['item_id', 'item_type']],
            'subscription_rqty' => ['S', 'rqty'],
            'full_price'        => ['columns' => ['price', 'quantity', 'discount', 'item_type', 'item_id', 'id', 'subscription_rqty']],
            'order'             => ['table' => 'O', 'columns' => ['order_id']],
            'sum_total_price'   => 'SUM(TBL.quantity * (TBL.price - TBL.price * TBL.discount / 100.0))',
            'count'             => 'COUNT(*)',
            'count_qty'         => 'SUM(TBL.quantity)',
            'ids'               => ['column' => 'GROUP_CONCAT(TBL.id)'],
            'order_ids'         => ['column' => 'GROUP_CONCAT(O.id)', 'joins' => ['O']],
        ];
        $sorter_f = function($sorter, &$select)
        {
            switch($sorter['property'])
            {
                case 'id':
                    $select->order('TBL.'.$sorter['property'] . ' ' . $sorter['direction']);
                    break;
            }
        };
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                case 'order_id':
                case 'prolong_state':
                case 'item_type':
                case 'item_id':
                    if (!empty($filter['xoperator']) && $filter['xoperator']=='!=') {
                        $select->where->notEqualTo('TBL.'.$filter['property'], $filter['value']);
                        //$select->where->addPredicate(new \Zend\Db\Sql\Predicate\NotIn('O.'.$filter['property'], $filter['value']));
                    }
                    else {
                        $select->where(['TBL.'.$filter['property'] => $filter['value']]);
                    }
                    break;
                case 'subscription_date_end_gt':
                    $joins['S'] = 'inner';
                    $select->where->greaterThan('S.date_end', date('Y-m-d H:i:s', strtotime($filter['value'])));
                    break;
                case 'periodical_id':
                    $joins['S'] = 'inner';
                    $select->where(['S.'.$filter['property'] => $filter['value']]);
                    break;
                case 'person_id':
                case 'company_id':
                case 'order_state_id':
                    $joins['O'] = 'inner';
                    $select->where(['O.'.$filter['property'] => $filter['value']]);
                    break;
                case 'order_enabled':
                    $joins['O'] = 'inner';
                    $select->where(['O.enabled' => (int)$filter['value']]);
                    break;
                case 'payment_date_start':
                    $select->where->greaterThanOrEqualTo('O.payment_date', $filter['value']);
                    break;
                case 'payment_date_end':
                    $select->where->lessThanOrEqualTo('O.payment_date', $filter['value']);
                    break;
            }
        };
        $result_f = function(&$resultSet, $columns_names, $join_map, $params)
        {
            if(array_search('item_data', $columns_names)!==false)
            {//use item_type_map = array('item_type' => 'item_table')?
                foreach($params['columns_load']['item_data'] as $item_type => $cols)
                {
                    $l = [];
                    foreach($join_map as $cur) {
                        if($cur[0] == $item_type) {
                            $l = $cur;
                        }
                    }
                    $filterSet = $resultSet;
                    if(!empty($l[5])) {
                        //$filterSet = new ResultSetFilterIterator($resultSet, $l[5]);
                        $filterSet = array_filter($resultSet, $l[5]);
                    }
                    $this->loadExtra($cols, $l[0], $l[3], $filterSet, $l[4], 'item_data');
                }
            }
            if(array_search('full_price', $columns_names)!==false)
            {
                foreach($resultSet as $row) {
                    $item_price = $row->price;
                    if($row->item_type=='nx_subscription') {
                        $item_price *= $row->subscription_rqty;
                    }
                    $row->full_price = round($item_price* (1 - $row->discount/100.0),2) * $row->quantity;
                }
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'sorter_func' => $sorter_f,
            'filter_func' => $filter_f,
            'result_func' => $result_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        return $result;
    }

    public function getPrice($data)
    {
    	$item_data = $data['item_data'];
    	$zoneID    = $data['delivery']['zone_id'];
        $price     = 0;
        if(empty($item_data['item_type'])) {
        	throw new \Exception('не указан тип позиции заказа');
        }
        switch($item_data['item_type'])
        {
            case 'Issue':
                $currentPrice = $this->priceModel->getMagazinePriceList();//$this->issueModel->getIssuePriceList();
               	$item         = $this->issueModel->getIssue(['id' => $item_data['item_id']]);
                if(!empty($item))
                {
                    $price += $currentPrice['customerType'][1]['magazine'][$item['periodical_id']]['deliveryType'][5]['zone'][$zoneID]['Price'];
                }
                break;
            default:
            	throw new \Exception('неизвестный тип позиции заказа');
            	break;
        }
        return $price;
    }

    public function getStats($params = [])
    {
        $by = [
            'columns'      => [
                'count', 'count_qty', 'item_id', 'item_type', 'sum_total_price'//, 'ids', 'order_ids'
            ],
            'operator_not_item_type' => 'nx_subscription',
            'order_state_id' => 3,
            //'columns_left' => array('by_channel_id'),
            'filters'      => $params['filters'],
            'order'        => 'TBL.item_id',
            'group'        => ['TBL.item_id', 'TBL.item_type'],
            'as_array'     => true
            //'debug' => 1
        ];
        $items = $this->getItemsBy($by);
        $results = [];
        if(!empty($items))
        {
            $issues_ids = [];
            $issues_by_id = [];
            foreach($items as $item)
            {
                if($item['item_type'] == 'Issue')
                {
                    $issues_ids[] = $item['item_id'];
                }
            }
            if(!empty($issues_ids))
            {
                $issues = $this->issueModel->getIssuesBy(['columns' => ['id', 'title', 'release_month', 'year', 'periodical_name'], 'id' => $issues_ids]);
                if(!empty($issues))
                {
                    foreach($issues as $issue)
                    {
                        $issues_by_id[$issue['id']] = $issue;
                    }
                }
            }
            foreach($items as $item)
            {
                if($item['item_type'] == 'Issue' && !empty($issues_by_id[$item['item_id']]))
                {
                    $item['name'] = $issues_by_id[$item['item_id']]['periodical_name'] . ' ' . $issues_by_id[$item['item_id']]['year'];
                    $item['issue_title'] = $issues_by_id[$item['item_id']]['title'];
                }
                $results[] = $item;
            }
        }
        return $results;
    }
}
