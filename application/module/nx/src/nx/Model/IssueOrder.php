<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class IssueOrder extends AbstractTable
{
    const OSP_ROOT = '/var/www/osp.ru/htdocs';

    public $personModel, $subscriptionModel, $itemModel, $priceModel;
    public $defaultCountryID = 165, $defaultZoneID = 4;
    public $currentPrice;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_order', $adapter);
    }

    public function getList($offset = 0, $limit = 100, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['O'=>$this->table])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS O.id AS id'),
                'created'         => 'created',
                //'name'            => 'name',
                'order_state_id'  => 'order_state_id',
                'price'           => 'price',
                'payment_type_id' => 'payment_type_id',
                'item_name'       => new Expression('CONCAT(M.EnglishName, " ", I.Name)'),
                'old_order_id',
                'type',
                'email',
                'payment_date'
            ])
            ->join(['P'=>'nx_person'], 'P.id = O.person_id', ['name'])
            ->join(['OS'=>'nx_order_state'], 'OS.id = O.order_state_id', ['order_state_name'=>'name'])
            ->join(['PT'=>'nx_payment_type'], 'PT.id = O.payment_type_id', ['payment_type_name'=>'name'])
            ->join(['OI'=>'nx_order_item'], 'OI.order_id = O.id', [])
            ->join(['I'=>'Issue'],'I.Message_ID = OI.item_id', ['issue_name' => 'Name', 'issue_title' => 'issue_title'])
            ->join(['M'=>'Edition'],'M.Message_ID = I.MagazineID', ['periodical_name' => 'EnglishName'])
            ->where(['OI.item_type' => 'Issue'])
            ->order('O.id DESC')
            ->limit($limit)
            ->offset($offset);

            if ($this->publisher_id) {
                $select
                ->join(['PR'=>'nx_price'], 'PR.id = O.price_id', [])
                ->where(['PR.publisher_id' => $this->publisher_id]);
            } else {
                $select->where('0'); //не выводить ничего если не знаем какой издатель
            }


        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(empty($filter['value']))
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'payment_date':
                    case 'created':
                    case 'price':
                    case 'id':
                    case 'email':
                    //case 'name':
                        $select->where->like('O.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'payment_type_id':
                    case 'order_state_id':
                        $select->where->equalTo('O.'.$filter['property'], '' . $filter['value'] . '');
                        break;
                    case 'item_name':
                        $select->where->NEST->like('I.Name', '%' . $filter['value'] . '%')->OR->like('M.EnglishName', '%' . $filter['value'] . '%')->UNNEST;
                        break;
                    case 'periodical_id':
                        $select->where(['M.Message_ID' => $filter['value']]);
                        break;
                    case 'issue_name':
                        $select->where->like('I.Name', '%' . $filter['value'] . '%');
                        break;
                    case 'issue_title':
                        $select->where->like('I.issue_title', '%' . $filter['value'] . '%');
                        break;
                    case 'name':
                        $select->where->like('P.name', '%' . $filter['value'] . '%');
                        break;
                    case 'created_start':
                        $select->where->greaterThanOrEqualTo('O.created', $filter['value']);
                        break;
                    case 'created_end':
                        $dt = explode(' ', $filter['value']);
                        $select->where->lessThanOrEqualTo('O.created', $filter['value'] . (empty($dt[1]) ? ' 23:59:59' : ''));
                        break;
                    case 'payment_date_start':
                        $select->where->greaterThanOrEqualTo('O.payment_date', $filter['value']);
                        break;
                    case 'payment_date_end':
                        $select->where->lessThanOrEqualTo('O.payment_date', $filter['value']);
                        break;
                }
            }
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        return [
            'items' => $result->toArray(),
            'total' => $count['count']
        ];
    }

    public function getOrders($params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['O'=>$this->table])
            ->columns([
                'price'
            ])
            ->join(['OI'=>'nx_order_item'], 'OI.order_id = O.id', [])
            ->where(['OI.item_type' => 'Issue'])
            ->order('O.id DESC');

        if ($this->publisher_id) {
            $select
            ->join(['PR'=>'nx_price'], 'PR.id = O.price_id', [])
            ->where(['PR.publisher_id' => $this->publisher_id]);
        } else {
            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }

        if(!empty($params['created_start']))
        {
            $select->where->greaterThanOrEqualTo('O.created', $params['created_start']);
        }
        if(!empty($params['created_end']))
        {
            $select->where->lessThanOrEqualTo('O.created', $params['created_end']);
        }
        if(!empty($params['order_state_id']))
        {
            $select->where->equalTo('O.order_state_id', (int)$params['order_state_id']);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        return $result->toArray();
    }

    public function getByID($id)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['O'=>'nx_order'])
            ->columns([
                'id',
                'shop_id',
                'price',
                'payment_type_id',
                'email',
                'order_state_id',
                'enabled',
                'comment',
                'person_id',
                'created',
                'last_updated',
                'user_id',
                'label',
                'type',
                'payment_date'
            ])
            ->join(['P'=>'nx_person'], 'P.id = O.person_id', ['person_name' => 'name', 'person_id_name' => new Expression('CONCAT(P.id,". ",P.name)')], 'left')
            ->join(['U'=>'User'], 'U.User_ID = O.create_user_id', ['create_user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')], 'left')
            ->join(['U2'=>'User'], 'U2.User_ID = O.last_user_id', ['last_user_name' => new Expression('IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')], 'left')
            ->join(['U3'=>'User'], 'U3.User_ID = O.user_id', ['user_name' => new Expression('CONCAT(U3.User_ID, ". ", IF(U3.LastName<>"", CONCAT_WS(" ", U3.LastName, U3.FirstName, U3.MidName), U3.Login))')], 'left')
            ->where(['O.id' => $id]);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        return $result->current();
    }

    public function updateOrder($id, $data)
    {
        if(empty($id) || empty($data))
        {
            return false;
        }

        $order = $this->getByID($id);

        if(empty($order['id']))
        {
            return false;
        }
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(!empty($data['itemsData']))
            {
                $curitems = $this->getItems(['order_id' => $order['id']]);
                $this->itemModel->delete(['order_id' => $order['id']]);
                foreach($data['itemsData'] as $item_table => $items)
                {
                    if(empty($item_table))
                    {
                        throw new \Exception('Не указан тип позиций заказа при обновлении');
                    }
                    foreach($items as $item_id => $qty)
                    {
                        if(empty($item_id) || empty($qty))
                        {
                            throw new \Exception('Не указан id или количество позиций заказа при обновлении');
                        }
                        $item_data = [
                            'order_id'         => $order_id,
                            'item_id'          => (int)$item_id,
                            'item_type'        => $item_table,
                            'quantity'         => (int)$qty,
                            'delivery_type_id' => 5,
                            'created'          => date('Y-m-d H:i:s'),
                            'create_user_id'   => !empty($data['orderData']['create_user_id']) ? $data['orderData']['create_user_id'] : 0
                        ];
                        if(!empty($curitems))
                        {
                            $replace_item = array_shift($curitems);
                            $item_data['last_user_id'] = $data['orderData']['create_user_id'];
                            $item_data['id'] = $replace_item['id'];
                            //$this->itemModel->update($item_data, array('id' => $replace_item['id']));
                        }
                        $this->itemModel->insert($item_data);
                    }
                }
                $price = $this->getOrderPrice($data);
                $data['orderData']['price'] = $price;
            }
            if(!empty($data['orderData']))
            {
                $this->update($data['orderData'], ['id' => $order['id']]);
            }
            if(!empty($data['orderData']['last_user_id']))
            {
                $this->logger->log('Обновление заказа #' . $order['id'], 'update_order', $data, $data['orderData']['last_user_id']);
            }
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $order['id'];
        }
        catch(\Exception $e)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
            return false;
        }
    }

    public function addOrder($data)
    {
        if(empty($data['orderData']))
        {
            return false;
        }

        if(empty($data['itemsData']))
        {
            throw new \Exception('Не указаны позиции заказа');
            return false;
        }

        if(empty($data['orderData']['person_id']))
        {
            throw new \Exception('Не указан заказчик');
            return false;
        }

        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            $price = $this->getOrderPrice($data);
            if(empty($price))
            {
                throw new \Exception('Ошибка при вычислении стоимости заказа');
            }
            $data['orderData']['price'] = $price;
            if(empty($data['orderData']['label']))
            {
                $data['orderData']['label'] = 'nx_order';
            }
            $data['orderData']['shop_id'] = 2;
            $data['orderData']['created'] = date('Y-m-d H:i:s');
            $data['orderData']['http_referer'] = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
            $this->insert($data['orderData']);
            $order_id = $this->lastInsertValue;
            if(empty($order_id))
            {
                throw new \Exception('Ошибка при создании заказа');
            }
            $this->update(
                ['hash' => $this->calculateOrderHash($order_id)],
                ['id'   => $order_id]
            );

            foreach($data['itemsData'] as $item_table => $items)
            {
                if(!empty($items) && empty($item_table))
                {
                    throw new \Exception('Не указан тип позиций заказа');
                    return false;
                }
                foreach($items as $item_id => $qty)
                {
                    $item_data = [
                        'order_id'         => $order_id,
                        'item_id'          => (int)$item_id,
                        'item_type'        => $item_table,
                        'quantity'         => (int)$qty,
                        'price'            => $price,
                        'delivery_type_id' => 5,
                        'created'          => date('Y-m-d H:i:s'),
                        'create_user_id'   => !empty($data['orderData']['create_user_id']) ? $data['orderData']['create_user_id'] : 0
                    ];
                    $this->itemModel->insert($item_data);
                }
            }
            if(!empty($data['orderData']['create_user_id']))
            {
                $this->logger->log('Добавление заказа #' . $order_id, 'add_order', $data, $data['orderData']['create_user_id']);
            }
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $order_id;
        }
        catch(\Exception $e)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
            return false;
        }
    }

    public function getOrderPrice($data)
    {
        $db    = $this->adapter;
        $price = 0;
        foreach($data['itemsData'] as $ItemTable => $items)
        {
            switch($ItemTable)
            {
                /*case 'aecms_document':
                    $docs = $this->getAecmsDocuments(array('inIDS'=>array_keys($items)));
                    foreach($docs as $doc) $price += $doc['Issue_ArticlePrice'] ? $doc['Issue_ArticlePrice'] : $doc['Magazine_ArticlePrice'];
                    break;*/
                case 'Issue':
                    //if($data['label'] == 'store_issue_pdf')
                    //{//заказ на /store/ - берем цену pdf выпуска
                    $currentPrice = $this->priceModel->getMagazinePriceList();//$this->subscriptionModel->getPriceList();
                    $zoneID       = $this->subscriptionModel->getZoneByCountry($this->defaultCountryID);
                    //$issues       = $this->subscriptionModel->getIssueData(array('inIDS' => array_keys($items), 'limit' => 1000));
                    $sql          = new Sql($db);
                    $select       = $sql->select()->from($ItemTable)->where('Message_ID IN ("'.implode('","', array_keys($items)).'")');
                    $selectString = $sql->getSqlStringForSqlObject($select);
                    $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                    $issues       = $result->toArray();
                    if(!empty($issues))
                    {
                        foreach($issues as $issue)
                        {
                            $price += $currentPrice['customerType'][1]['magazine'][$issue['MagazineID']]['deliveryType'][5]['zone'][$zoneID]['Price'];
                        }
                    }
                    //}
                    break;
            }
        }
        return $price;
    }

    public function calculateOrderHash($id)
    {
        return md5($id . $_SERVER['SERVER_NAME']);
    }

    public function getItems($params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['I'=>'nx_order_item'])
            ->columns([
                'id',
                'order_id',
                'item_id',
                'item_type'
            ])
            ->where(['I.order_id' => (int)$params['order_id']])
            ->where(['I.item_type' => 'Issue'])
            ->order('I.id ASC');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $items = $result->toArray();

        return $items;
    }

    public function getItemsPDFIssue($params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['OI'=>'nx_order_item'])
            ->columns([
                'id',
                'order_id',
                'item_id',
                'item_type'
            ])
            ->join(['F'=>'aecms_file_storage'], 'F.issue_id = OI.item_id', ['file_name', 'file_path'])
            ->join(['I' => 'Issue'], 'F.issue_id = I.Message_ID', ['issue_title', 'cover_image' => 'Picture'])
            ->join(['M' => 'Edition'], 'I.MagazineID = M.Message_ID', ['periodical_name' => 'HumanizedName'])
            ->where(['OI.order_id' => (int)$params['order_id']])
            ->where('OI.item_type = "Issue"')
            ->where('F.file_type = 2')
            ->where('F.isArchive = 1')
            ->where('F.deleted   = 0')
            ->order('OI.id ASC');

        if ($this->publisher_id) {
            $select
            ->join(['PE'=>'publisher_editions'], 'PE.magazine_id = M.Message_ID', [])
            ->where(['PE.publisher_id' => $this->publisher_id]);
        } else {
            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }


        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $items = $result->toArray();

        return $items;
    }
}
