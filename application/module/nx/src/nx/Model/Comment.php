<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Comment extends AbstractTable
{
	const ENTITY_ORDER = 1;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_comment', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Comment));
    }

    public function getCommentBy($params)
    {
        $params['current'] = true;
        return $this->getCommentsBy($params);
    }

    public function getCommentsBy(array $params)
    {
        $join_map = [
        	'T' => ['nx_tag', 'T.id = TBL.tag_id']
        ];
        $columns_map = [
        	'tag_name' => ['T', 'name']
        ];
        $sorter_f = function($sorter, &$select)
        {
            switch($sorter['property'])
            {
                case 'id':
                    $select->order('TBL.'.$sorter['property'] . ' ' . $sorter['direction']);
                    break;
            }
        };
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                case 'row_id':
                case 'entity_id':
                case 'tag_id':
                    $select->where(['TBL.'.$filter['property'] => $filter['value']]);
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'sorter_func' => $sorter_f,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        return $result;
    }

    public function addComment($data)
    {
        if(empty($data))
        {
            return false;
        }
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            $data['created'] = date('Y-m-d H:i:s');
            $data['comment'] = trim($data['comment']);
            if(empty($data['comment']))
            {
                return false;
            }

            $this->insert($data);
            $comment_id = $this->lastInsertValue;
            $this->logger->log('Добавление комментария #' . $comment_id, 'add_comment', $data, !empty($data['create_user_id']) ? $data['create_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $comment_id;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }

    public function updateComment($id, $data)
    {
        if(empty($id) || empty($data))
        {
            return false;
        }

        $comment = $this->getCommentBy(['id' => $id]);

        if(empty($comment->id))
        {
            return false;
        }
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(isset($data['comment'])) {
                $data['comment'] = trim($data['comment']);
                if(empty($data['comment'])) {
                    unset($data['comment']);
                }
            }
            unset($data['create_user_id']);
            unset($data['created']);
            unset($data['row_id']);
            unset($data['entity_id']);
            $this->update($data, ['id' => $comment->id]);

            $this->logger->log('Обновление комментария #' . $comment->id, 'update_comment', $data, !empty($data['last_user_id']) ? $data['last_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $comment->id;
        }
        catch(\Exception $e)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
        }
    }

    public function deleteComment($id, $data)
    {
    	if(empty($id))
        {
            return false;
        }
        $comment = $this->getCommentBy(['id' => $id]);
        if(empty($comment->id))
        {
            return false;
        }
        try
        {
            $this->delete(['id' => $comment->id]);
            $this->logger->log('Удаление комментария #' . $comment->id, 'delete_comment', $data, !empty($data['user_id']) ? $data['user_id'] : 0);
            return true;
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }
}
