<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class ViewState extends AbstractTable
{
    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_view_state', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\ViewState));
    }

    public function getViewStates(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['S'=>$this->table])
            ->columns([
                'id',
                'name',
                'data'
            ])
            ->group('S.id')
            ->order('S.id DESC');
        if(!empty($params['create_user_id']))
        {
            $select->where->equalTo('S.create_user_id', (int)$params['create_user_id']);
        }
        if(!empty($params['viewid']))
        {
            $select->where->equalTo('S.viewid', $params['viewid']);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        //$items = $resultSet->count() ? $resultSet->toArray() : array();

        foreach($resultSet as $item)
        {
            if(!empty($item->data)) {
                $item->data = unserialize($item->data);
            }
        }

        return $resultSet;
    }

    public function getBy(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['S'=>$this->table])
            ->columns([
                'id',
                'name',
                'data',
                'viewid'
            ])
            ->limit(1);
        if(!empty($params['id']))
        {
            $select->where(['S.id' => (int)$params['id']]);
        }
        if(!empty($params['data']))
        {
            $select->where(['S.data' => $params['data']]);
        }
        if(!empty($params['viewid']))
        {
            $select->where(['S.viewid' => $params['viewid']]);
        }
        if(!empty($params['create_user_id']))
        {
            $select->where(['S.create_user_id' => (int)$params['create_user_id']]);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        $item = $resultSet->current();
        if(!empty($item->data)) {
            $item->data = unserialize($item->data);
        }

        return $item;
    }

    public function addViewState(array $data)
    {
    	if(empty($data['name']) || empty($data['data']))
    	{
    		throw new \Exception("Ошибка при добавлении");
    	}
    	if(!empty($data['data']['filter']))
    	{
    		$data['data']['filter'] = $this->prepareDataFilter($data['data']['filter']);
    		if(empty($data['data']['filter'])) {
    			throw new \Exception("Ошибка при разборе фильтров");
    		}
    	}
        if(!empty($data['data']['sort']))
        {
            $data['data']['sort'] = $this->prepareDataSort($data['data']['sort']);
            if(empty($data['data']['sort'])) {
                throw new \Exception("Ошибка при разборе сортировок");
            }
        }
    	$data['data'] = serialize($data['data']);
        $check = $this->getBy($data);
        if(!empty($check))
        {
            //throw new \Exception("Состояние уже было сохранено.");
            return false;
        }
    	$this->insert($data);
    	return $this->lastInsertValue;
    }

    public function prepareDataFilter($data)
    {//for extjs4.2 just validate filter data format: [{property:..., value:...},{...}]
    	$filters = $data;
    	if(empty($filters) || !is_array($filters))
    	{
    		return false;
    	}
    	foreach($filters as $filter)
    	{
            if(is_object($filter))
            {
                $filter = (array)$filter;
            }
    		if(!isset($filter['property']) || !isset($filter['value']))
    		{
    			return false;
    		}
    	}
    	$result = $data;
    	return $result;
    }

    public function prepareDataSort($data)
    {//for extjs4.2 just validate sort data format: [{property:..., direction:...},{...}]
        $sorters = $data;
        if(empty($sorters) || !is_array($sorters))
        {
            return false;
        }
        foreach($sorters as $sorter)
        {
            if(is_object($sorter))
            {
                $sorter = (array)$sorter;
            }
            if(!isset($sorter['property']) || !isset($sorter['direction']))
            {
                return false;
            }
        }
        $result = $data;
        return $result;
    }
}
