<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Shipment extends AbstractTable
{
    public static $item_type = [
        'nx_order_item_release' => 1,
    ];

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_shipment', $adapter);//, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Shipment));
    }

    public function getShipmentBy($params)
    {
        $params['current'] = true;
        return $this->getShipmentsBy($params);
    }

    public function getShipmentsBy(array $params)
    {
        $join_map = [
            'SC' => ['nx_ship_channel', 'SC.id = TBL.channel_id'],
            'U'  => ['User', 'U.User_ID = TBL.create_user_id'],
            //'F'  => array('nx_file_storage', 'F.entity_id = TBL.id')
        ];
        $columns_map = [
            'channel_id'       => ['SC', 'id'],
            'channel_name'     => ['SC', 'name'],
            'create_user_name' => ['column' => 'IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)', 'joins' => ['U']],
            //'file_hash'        => array('F', 'hash')
        ];
        $sorter_f = function($sorter, &$select)
        {
            switch($sorter['property'])
            {
                case 'id':
                    $select->order('TBL.'.$sorter['property'] . ' ' . $sorter['direction']);
                    break;
            }
        };
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                    $select->where(['TBL.'.$filter['property'] => $filter['value']]);
                    break;
                // case 'file_type':
                //     if(!empty($filter['xoperator']) && $filter['xoperator'] == 'or_null')
                //     {
                //         $select->where->NEST->equalTo('F.type', $filter['value'])->OR->isNull('F.type')->UNNEST;
                //     }
                //     break;
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'as_array'    => true,
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'sorter_func' => $sorter_f,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        return $result;
    }

    public function getShipmentItemsBy(array $params)
    {
        $join_map = [
            'S'  => ['nx_shipment', 'S.id = TBL.shipment_id'],
            'SC' => ['nx_ship_channel', 'SC.id = S.channel_id', 'S'],
            'U'  => ['User', 'U.User_ID = TBL.cancel_user_id'],
            'U2' => ['User', 'U2.User_ID = S.create_user_id', 'S'],
            'IR' => ['nx_order_item_release', 'IR.id = TBL.item_id'],
            'OI' => ['nx_order_item', 'OI.id = IR.item_id', 'IR'],
            'O'  => ['nx_order', 'O.id = OI.order_id', 'OI'],
            'I'  => ['Issue', 'I.Message_ID = IR.release_id', 'IR'],
            'M'  => ['Edition', 'M.Message_ID = I.MagazineID', 'I'],
            'A'  => ['nx_address', 'A.id = O.map_address_id', 'O'],
            'P'  => ['nx_person', 'P.id = O.person_id', 'O'],
            'C'  => ['nx_company', 'C.id = O.company_id', 'O']
        ];
        $columns_map = [
            'channel_name'     => ['SC', 'name'],
            'shipment_date'    => ['S', 'date'],
            'shipment_created' => ['S', 'created'],
            'create_user_name' => ['column' => 'IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)', 'joins' => ['U2']],
            'cancel_user_name' => ['column' => 'IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)', 'joins' => ['U']],
            'issue_name'       => ['column' => 'CONCAT(M.EnglishName, " ", I.year, "-", LPAD(I.release_month, 2, "0"), " ", I.issue_title)', 'joins' => ['M', 'I']],
            'release_date'     => ['I', 'PublicDate'],
            'item_release_id'  => ['IR', 'id'],
            'customer_name'    => ['joins' => ['C','P','O'], 'column' => 'IF(O.subscriber_type_id=2, CONCAT(IFNULL(C.name,""), " (", IFNULL(P.name,"") , ")"), P.name)'],
            'zipcode'          => ['A', 'zipcode'],
            'email'            => ['O', 'email'],
            'order_id'         => ['OI', 'order_id'],
            'periodical_name'  => ['M', 'EnglishName'],
            'issue_title'      => ['I', 'issue_title'],
        ];
        $sorter_f = function($sorter, &$select)
        {
            switch($sorter['property'])
            {
                case 'id':
                    $select->order('TBL.'.$sorter['property'] . ' ' . $sorter['direction']);
                    break;
            }
        };
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                case 'item_type':
                    $select->where(['TBL.'.$filter['property'] => $filter['value']]);
                    break;
                case 'item_release_id':
                    $select->where(['IR.id' => $filter['value']]);
                    break;
                case 'ship_count':
                    $select->where(['IR.'.$filter['property'] => $filter['value']]);
                    break;
                case 'order_id':
                    if(empty($joins['I'])) {
                        $joins['O'] = 'inner';
                    }
                    $select->where(['O.id' => $filter['value']]);
                    break;
                case 'is_reclamation':
                    $select->where->notEqualTo('TBL.cancel_user_id', 0);
                    // $select->where->NEST
                    //     ->NEST->equalTo('IR.ship_count', 1)->AND->notEqualTo('TBL.cancel_user_id', 0)->UNNEST->OR
                    //     ->NEST->greaterThan('IR.ship_count', 1)->UNNEST->UNNEST;
                    break;
                case 'periodical_id':
                    if(empty($joins['I'])) {
                        $joins['I'] = 'inner';
                    }
                    $select->where(['I.MagazineID' => $filter['value']]);
                    break;
                case 'issue_title':
                    $select->where->like('I.issue_title', '%' . $filter['value'] . '%');
                    break;
                case 'customer_name':
                    if(empty($joins['P'])) {
                        $joins['P'] = 'left';
                    }
                    if(empty($joins['C'])) {
                        $joins['C'] = 'left';
                    }
                    $select->where->NEST
                        ->like('P.name', '%' . $filter['value'] . '%')->OR
                        ->like('C.name', '%' . $filter['value'] . '%')->UNNEST;
                    break;
                case 'email':
                    $select->where->like('O.'.$filter['property'], '%' . $filter['value'] . '%');
                    break;
                case 'shipment_created':
                    $select->where->like('S.created', '%' . $filter['value'] . '%');
                    break;
                case 'release_date':
                    $select->where->like('I.PublicDate', '%' . $filter['value'] . '%');
                    break;
                case 'cancel_date':
                    $select->where->like('TBL.cancel_date', '%' . $filter['value'] . '%');
                    break;
                case 'channel_id':
                    if(empty($joins['I'])) {
                        $joins['S'] = 'inner';
                    }
                    $select->where(['S.channel_id' => (int)$filter['value']]);
                    break;
                case 'zipcode':
                    $select->where->like('A.zipcode', '%' . $filter['value'] . '%');
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'table'       => 'nx_shipment_item',
            'as_array'    => true,
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'sorter_func' => $sorter_f,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        return $result;
    }
}
