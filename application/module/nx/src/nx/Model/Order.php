<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Order extends AbstractTable
{
    //const OSP_ROOT = '/var/www/osp.ru/htdocs';

    public $personModel, $subscriptionModel, $itemModel, $fileStorageModel, $publisherModel;
    public $defaultCountryID = 165, $defaultZoneID = 4;
    //public $currentPrice;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_order', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Order));
    }

    public function getOrderBy($params)
    {
        $params['current'] = true;
        return $this->getOrdersBy($params);
    }

    public function getOrdersBy(array $params)
    {
        $join_map = [//alias => table, condition, dependent_table
            'OI' => ['nx_order_item', 'OI.order_id = TBL.id'],
            'I'  => ['Issue', 'I.Message_ID = OI.item_id', 'OI'],
            'M'  => ['Edition', 'M.Message_ID = I.MagazineID', 'I'],
            'P'  => ['nx_person', 'P.id = TBL.person_id'],
            'C'  => ['nx_company', 'C.id = TBL.company_id'],
            'S'  => ['nx_subscription', 'S.id = OI.item_id', 'OI'],
        ];
        $columns_map = [
            'sum_total_price'   => 'SUM(TBL.price)',
            'count'             => 'COUNT(TBL.id)',
            'periodical_name'   => ['M', 'EnglishName'],
            'created_yearmonth' => 'DATE_FORMAT(TBL.created, "%Y%m")',
            'person'            => ['table' => 'P', 'columns' => ['person_id']],
            'company'           => ['table' => 'C', 'columns' => ['company_id']]
        ];
        $sorter_f = function($sorter, &$select)
        {
            switch($sorter['property'])
            {
                case 'id':
                    $select->order('TBL.'.$sorter['property'] . ' ' . $sorter['direction']);
                    break;
            }
        };
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                case 'order_state_id':
                case 'shop_id':
                case 'user_id':
                    $select->where(['TBL.'.$filter['property'] => $filter['value']]);
                    break;
                case 'c_yearmonth_start':
                    $select->where->expression('DATE_FORMAT(TBL.created,"%Y%m") >= ?', $filter['value']);
                    break;
                case 'c_yearmonth_end':
                    $select->where->expression('DATE_FORMAT(TBL.created,"%Y%m") <= ?', $filter['value']);
                    break;
                case 'item_type':
                    $select->where(['OI.'.$filter['property'] => $filter['value']]);
                    break;
                case 'periodical_id':
                    $joins['S'] = 'inner';
                    $select->where(['S.'.$filter['property'] => $filter['value']]);
                    break;
                case 'is_present':
                    $joins['S'] = 'inner';
                    $select->having->literal('MAX(S.date_end) ' . ( $filter['value'] ? '>' : '<=') . ' NOW()');
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'sorter_func' => $sorter_f,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        return $result;
    }

    public function addOrder($data)
    {
        if(empty($data['orderData']))
        {
            return false;
        }
        if(empty($data['itemsData']))
        {
            throw new \Exception('Не указаны позиции заказа');
        }
        if(empty($data['orderData']['person_id']))
        {
            throw new \Exception('Не указан заказчик');
        }
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
        	$items_data = [];
        	$price = 0;
        	foreach($data['itemsData'] as $item_table => $items)
            {
                if(!empty($items) && empty($item_table))
                {
                    throw new \Exception('Не указан тип позиций заказа');
                }
                foreach($items as $item_id => $qty)
                {
                	if(empty($item_id)) {
                		throw new \Exception('Ошибка при задании позиции заказа');
                	}
                    $item_data = [
                        //'order_id'         => $order_id,
                        'item_id'          => (int)$item_id,
                        'item_type'        => $item_table,
                        'quantity'         => (int)$qty,
                        //'price'            => $price,
                        'delivery_type_id' => 5,
                        'created'          => date('Y-m-d H:i:s'),
                        'create_user_id'   => !empty($data['orderData']['create_user_id']) ? $data['orderData']['create_user_id'] : 0
                    ];
                    $item_data['price'] = $this->itemModel->getPrice(['item_data' => $item_data, 'delivery' => ['zone_id' => $this->defaultZoneID]]);
                    $price+=$item_data['price'];
                    $items_data[] = $item_data;
                    //$this->itemModel->insert($item_data);
                }
            }

            //$price = $this->getOrderPrice($data);
            if(empty($price))
            {
                throw new \Exception('Ошибка при вычислении стоимости заказа');
            }
            $data['orderData']['price'] = $price;
            if(empty($data['orderData']['label']))
            {
                $data['orderData']['label'] = 'nx_order';
            }
            $data['orderData']['created'] = date('Y-m-d H:i:s');
            $data['orderData']['http_referer'] = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
            $this->insert($data['orderData']);
            $order_id = $this->lastInsertValue;
            if(empty($order_id))
            {
                throw new \Exception('Ошибка при создании заказа');
            }
            $this->update(
                ['hash' => $this->calculateOrderHash($order_id)],
                ['id'   => $order_id]
            );
            foreach($items_data as $item_data) {
            	$item_data['order_id'] = $order_id;
            	$this->itemModel->insert($item_data);
            }

            if(!empty($data['orderData']['create_user_id']))
            {
                $this->logger->log('Добавление заказа #' . $order_id, 'add_order', $data, $data['orderData']['create_user_id']);
            }
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $order_id;
        }
        catch(\Exception $e)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
        }
    }

    public function updateOrder($id, $data)
    {
        if(empty($id) || empty($data))
        {
            return false;
        }

        $order = $this->getOrderBy(['id' => $id]);

        if(empty($order))
        {
            return false;
        }

        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(!empty($data['itemsData']))
            {
                throw new \Exception('Временно не поддерживается');
            }

            if(!empty($data['orderData']))
            {
                $this->update($data['orderData'], ['id' => $order->id]);
            }
            $this->logger->log('Обновление заказа #' . $order->id, 'update_order', $data, !empty($data['orderData']['last_user_id']) ? $data['orderData']['last_user_id'] : 0);

            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $order->id;
        }
        catch(\Exception $e)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
        }
    }

    /*public function getOrderPrice($data)
    {
        $db    = $this->adapter;
        $price = 0;
        foreach($data['itemsData'] as $ItemTable => $items)
        {
            switch($ItemTable)
            {getItemsPDFIssue
                //case 'aecms_document':
                //    $docs = $this->getAecmsDocuments(array('inIDS'=>array_keys($items)));
                //    foreach($docs as $doc) $price += $doc['Issue_ArticlePrice'] ? $doc['Issue_ArticlePrice'] : $doc['Magazine_ArticlePrice'];
                //    break;
                case 'Issue':
                    $currentPrice = $this->subscriptionModel->getPriceList();
                    $zoneID       = $this->subscriptionModel->getZoneByCountry($this->defaultCountryID);
                    //$issues       = $this->subscriptionModel->getIssueData(array('inIDS' => array_keys($items), 'limit' => 1000));
                    $sql          = new Sql($db);
                    $select       = $sql->select()->from($ItemTable)->where('Message_ID IN ("'.implode('","', array_keys($items)).'")');
                    $selectString = $sql->getSqlStringForSqlObject($select);
                    $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                    $issues       = $result->toArray();
                    if(!empty($issues))
                    {
                        foreach($issues as $issue)
                        {
                            $price += $currentPrice['magazine'][$issue['MagazineID']]['deliveryType'][5]['zone'][$zoneID]['Price'];
                        }
                    }
                    break;
            }
        }
        return $price;
    }*/

    public function calculateOrderHash($id)
    {
        return md5($id . $_SERVER['SERVER_NAME']);
    }

    /**
     * Отдает зону доставки по ID страны
     * @param int $countryID
     * @return int
     */
    public function getZoneByCountry($countryID)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        // $select = $sql->select()
        //     ->from('Message127')
        //     ->columns(array('id' => 'ZoneID'))
        //     ->where(array('CountryID = ?' => $countryID));
        $select = $sql->select()
            ->from('nx_zone_country')
            ->columns(['id'=>'zone_id'])
            ->where(['country_id = ?'=>$countryID]);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $zone = $result->current();
        return !empty($zone['id']) ? intval($zone['id']) : $this->defaultZoneID;
    }
    public function getOwner($orderId = false)
    {
        if (!$orderId)
            return [];

        $queryString = "SELECT p.publisher_id FROM nx_order o
                        LEFT JOIN nx_price p ON o.price_id = p.id
                        WHERE o.id = " . intval($orderId);

        $publisherId = $this->adapter->query($queryString)->execute()->current();
        $publisherId = !empty($publisherId) ? intval($publisherId['publisher_id']) : false;

        if (!$publisherId)
            return [];

        return $this->publisherModel->getOne($publisherId);
    }
}
