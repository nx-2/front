<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Company extends AbstractTable
{
    public $addressModel, $personModel, $ws1c_client;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_company', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Company));
    }

    public function getList($offset, $limit, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['C'=>$this->table])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS C.id AS id'),
                'name',
                'email',
                'url',
                'inn',
                'kpp',
                'org_form',
                'shortname',
                'othernames',
                'wrongnames',
                'checked',
                'enabled',
                'created',
                'xpressid',
                'ws1c_synced'
            ])
            ->order('C.id DESC')
            ->limit($limit)
            ->offset($offset);
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(!is_array($filter) && !is_object($filter))
                {
                    continue;
                }
                if(is_array($filter))
                {
                    $filter = (object)$filter;
                }
                $allowed = ['id', 'name', 'shortname', 'othernames', 'wrongnames', 'email', 'url', 'checked', 'created', 'xpressid', 'inn', 'kpp', 'org_form', 'ws1c_synced'];
                if(!in_array($filter->property, $allowed))
                {
                    continue;
                }
                $select->where->like($filter->property, '%' . $filter->value . '%');
            }
        }
        if(!empty($params['keywords']))
        {
            $select->where->NEST
                ->like('C.name', '%' . $params['keywords'] . '%')->OR
                ->like('C.shortname', '%' . $params['keywords'] . '%')->OR
                ->like('C.othernames', '%' . $params['keywords'] . '%')->OR
                ->like('C.wrongnames', '%' . $params['keywords'] . '%')->UNNEST;
        }
        if(!empty($params['checked']))
        {
            $select->where(['C.checked' => (int)$params['checked']]);
        }
        if(!empty($params['ids']))
        {
            $select->where->In('C.id', $params['ids']);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function getCompanies($params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['C'=>$this->table])
            ->columns([
                'id',
                'name',
                'email',
                'url',
                'org_form',
                'shortname',
                'othernames',
                'wrongnames',
                'checked',
                'created',
                'xpressid'
            ])
            ->order('C.id DESC');
        if(!empty($params['name_to_sorthash']))
        {
            $select->where(['C.sorthash' => self :: createSortHash($params['name_to_sorthash'])]);
        }
        if(!empty($params['xpress_id']))
        {
            $select->where(['C.xpressid' => (int)$params['xpress_id']]);
        }
        if(!empty($params['inn']))
        {
            $select->where(['C.inn' => $params['inn']]);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getCompanyBy(array $params)
    {
        $params['current'] = true;
        return $this->getCompaniesBy($params);
    }

    public function getCompaniesBy(array $params)
    {
        $join_map = [//alias => table, condition, dependent_table
            'U'  => ['User', 'TBL.create_user_id = U.User_ID'],
            'U2' => ['User', 'TBL.last_user_id = U2.User_ID'],
            'P'  => ['nx_person', 'P.id = TBL.contact_person_id'],
            'A'  => ['nx_address', 'A.id = TBL.address_id', '', 'id', 'address_id'],
            'A2' => ['nx_address', 'A2.id = TBL.legal_address_id', '', 'id', 'legal_address_id']
        ];
        $columns_map = [
            'contact_person_name' => ['P', 'name'],
            'addr'                => ['table' => 'A', 'columns' => ['address_id']],
            'laddr'               => ['table' => 'A2', 'columns' => ['legal_address_id']],
            'legal_address'       => ['A2', 'address'],
            'create_user_name'    => ['joins' => ['U'], 'column' => 'IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)'],
            'last_user_name'      => ['joins' => ['U2'], 'column' => 'IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)']
        ];
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                case 'ws1c_synced':
                case 'create_user_id':
                    $select->where(['TBL.'.$filter['property'] => $filter['value']]);
                    break;
                case 'name':
                    $select->where->like('TBL.'.$filter['property'], '%' . $filter['value'] . '%');
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        return $result;
    }

    public function getByID($id)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['C'=>$this->table])
            ->columns([
                'id',
                'name',
                'email',
                'url',
                'org_form',
                'shortname',
                'othernames',
                'wrongnames',
                'address_id',
                'legal_address_id',
                'contact_person_id',
                'inn',
                'kpp',
                'okonh',
                'okpo',
                'pay_account',
                'corr_account',
                'bank',
                'bik',
                'checked',
                'enabled',
                'xpressid',
                'description',
                'created',
                'last_updated',
                'ws1c_synced'
            ])
            ->join(['A'=>'nx_address'],'A.id = C.address_id', ['phone', 'fax', 'address', 'zipcode', 'country_id', 'area', 'region', 'comment', 'city', 'telcode'], 'left')
            ->join(['A2'=>'nx_address'], 'A2.id = C.legal_address_id', ['la_phone' => 'phone', 'la_fax' => 'fax', 'la_address' => 'address', 'la_zipcode' => 'zipcode', 'la_country_id' => 'country_id', 'la_area' => 'area', 'la_region' => 'region', 'la_comment' => 'comment', 'la_city' => 'city', 'la_telcode' => 'telcode'], 'left')
            ->join(['U'=>'User'], 'U.User_ID = C.create_user_id', ['create_user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')], 'left')
            ->join(['U2'=>'User'], 'U2.User_ID = C.last_user_id', ['last_user_name' => new Expression('IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')], 'left')
            ->join(['P'=>'nx_person'],'P.id = C.contact_person_id', ['contact_person_name'=>'name'], 'left')
            ->where(['C.id' => $id]);
        $selectString = $sql->getSqlStringForSqlObject($select);

        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function getBy(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['C'=>$this->table])
            ->columns([
                'id',
                'name',
                'email',
                'url',
                'org_form',
                'shortname',
                'othernames',
                'wrongnames',
                'address_id',
                'legal_address_id',
                'contact_person_id',
                'inn',
                'kpp',
                'okonh',
                'okpo',
                'pay_account',
                'corr_account',
                'bank',
                'bik',
                'checked',
                'xpressid',
                'description',
                'created',
                'last_updated'
            ])
            ->join(['A'=>'nx_address'],'A.id = C.address_id', ['phone', 'fax', 'address', 'zipcode', 'country_id', 'area', 'region', 'comment', 'city', 'telcode'], 'left')
            ->join(['A2'=>'nx_address'], 'A2.id = C.legal_address_id', ['la_phone' => 'phone', 'la_fax' => 'fax', 'la_address' => 'address', 'la_zipcode' => 'zipcode', 'la_country_id' => 'country_id', 'la_area' => 'area', 'la_region' => 'region', 'la_comment' => 'comment', 'la_city' => 'city', 'la_telcode' => 'telcode'], 'left')
            ->join(['U'=>'User'], 'U.User_ID = C.create_user_id', ['create_user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')], 'left')
            ->join(['U2'=>'User'], 'U2.User_ID = C.last_user_id', ['last_user_name' => new Expression('IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')], 'left')
            ->join(['P'=>'nx_person'],'P.id = C.contact_person_id', ['contact_person_name'=>'name'], 'left')
            ->limit(1);
        if(!empty($params['name_to_sorthash']))
        {
            $select->where(['C.sorthash' => self :: createSortHash($params['name_to_sorthash'])]);
        }
        if(!empty($params['inn']))
        {
            $select->where(['C.inn' => $params['inn']]);
        }
        if(!empty($params['not_id']))
        {
            $select->where->notEqualTo('C.id', (int)$params['not_id']);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function addCompany($data)
    {
        if(empty($data['companyData']['name']))
        {
            return false;
        }
        $data = \nx\functions\applyFunctionToData($data, 'trim');
        /*if(!empty($data['companyData']['inn']) && $this->getBy(array('inn' => $data['companyData']['inn'])))
        {
           throw new \Exception('Организация с таким инн уже существует');
        }*/
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(!empty($data['addressData']))
            {
                $data['addressData']['label'] = 'company';
                $this->addressModel->insert($data['addressData']);
                $address_id = $this->addressModel->lastInsertValue;
                $data['companyData']['address_id'] = $address_id;
            }
            if(!empty($data['legalAddressData']))
            {
                $data['legalAddressData']['label'] = 'company';
                $this->addressModel->insert($data['legalAddressData']);
                $address_id = $this->addressModel->lastInsertValue;
                $data['companyData']['legal_address_id'] = $address_id;
            }
            $data['companyData']['created']  = date('Y-m-d H:i:s');
            if(empty($data['companyData']['label']))
            {
                $data['companyData']['label'] = 'company';
            }
            $data['companyData']['sorthash'] = self::createSortHash($data['companyData']['name']);
            $this->insert($data['companyData']);
            $company_id = $this->lastInsertValue;
            $this->logger->log('Добавление организации ' . $data['companyData']['name'] . ' #' . $company_id, 'add_company', $data, !empty($data['companyData']['create_user_id']) ? $data['companyData']['create_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $company_id;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
            return false;
        }
    }

    public function updateCompany($id, $data)
    {
        if(empty($id) || empty($data))
        {
            return false;
        }
        $data = \nx\functions\applyFunctionToData($data, 'trim');

        $company = $this->getByID($id);

        if(empty($company->id))
        {
            return false;
        }

        /*if(!empty($data['companyData']['inn']) && $this->getBy(array('inn' => $data['companyData']['inn'], 'not_id' => $id)))
        {
           throw new \Exception('Организация с таким инн уже существует');
        }*/
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(!empty($data['addressData']))
            {
                $insert_address = 1;
                if(!empty($company->address_id))
                {
                    $address = $this->addressModel->getByID($company->address_id);
                    if(!empty($address))
                    {
                        $insert_address = 0;
                        $this->addressModel->update($data['addressData'], ['id' => $company->address_id]);
                    }
                }
                if($insert_address)
                {
                    $data['addressData']['label'] = 'company';
                    $this->addressModel->insert($data['addressData']);
                    $address_id = $this->addressModel->lastInsertValue;
                    $data['companyData']['address_id'] = $address_id;
                }
            }

            if(!empty($data['legalAddressData']))
            {
                $insert_address = 1;
                if(!empty($company->legal_address_id))
                {
                    $address = $this->addressModel->getByID($company->legal_address_id);
                    if(!empty($address))
                    {
                        $insert_address = 0;
                        $this->addressModel->update($data['legalAddressData'], ['id' => $company->legal_address_id]);
                    }
                }
                if($insert_address)
                {
                    $data['legalAddressData']['label'] = 'company';
                    $this->addressModel->insert($data['legalAddressData']);
                    $address_id = $this->addressModel->lastInsertValue;
                    $data['companyData']['legal_address_id'] = $address_id;
                }
            }

            if(!empty($data['companyData']))
            {
                if(isset($data['companyData']['name']) && empty($data['companyData']['name'])) {
                    unset($data['companyData']['name']);
                }
                if(!empty($data['companyData']['name']))
                {
                    $data['companyData']['sorthash'] = self::createSortHash($data['companyData']['name']);
                    /*if($this->issetCompany($data['companyData']['name'], $company['id']))
                    {
                       throw new \Exception('Ошибка обновления организации #' . $company['id'] . ': название ' . $data['companyData']['name'] . ' уже существует');
                       return false;
                    }*/
                }
                unset($data['companyData']['label']);
                unset($data['companyData']['create_user_id']);
                unset($data['companyData']['created']);
                $this->update($data['companyData'], ['id' => $company->id]);
            }
            $this->logger->log('Обновление организации #' . $company->id, 'update_company', $data, !empty($data['companyData']['last_user_id']) ? $data['companyData']['last_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $company->id;
        }
        catch(\Exception $e)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
            return false;
        }
    }

    /**
     * Создает название для сортировки
     * @param string $string
     * @return string
     */
    public static function createSortHash($string)
    {
        //return mb_strtoupper(str_replace(array(' ', '*', ';', ',', '.', "'", '"', '«', '»', '\\', '/'), '', $string), 'UTF-8');
        return mb_strtoupper(preg_replace('#[^\wа-яА-ЯЁё-]#isu', '', $string), 'UTF-8');
    }

    /**
     * Существует ли организация с названием
     * @param string $companyName
     * @param int $companyID
     * @return int
     */
    public function issetCompany($companyName, $companyID = 0)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from('nx_company')
            ->columns([
                //'count' => new Expression('COUNT(*)'),
                'id'
            ])
            ->where(['sorthash = ?' => self :: createSortHash($companyName)])
            ->where(['id <> ?' => $companyID]);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $organization = $db->query($selectString, $db::QUERY_MODE_EXECUTE)->current();
        return !empty($organization['id']) ? $organization['id'] : 0;
    }

    /**
     * Отдает список объектов для комбокса
     * @param string $keywords
     * @return array
     */
    public function getObjectsForCombo($keywords = '')
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['C'=>$this->table])
            ->columns([
                'id',
                'name' => new Expression('CONCAT(C.id,". ",C.name)')
            ])
            ->where(['C.enabled' => 1]);
        if(!empty($keywords))
        {
            $select->where->NEST
                ->like('C.name', '%' . $keywords . '%')->OR
                ->like('C.shortname', '%' . $keywords . '%')->OR
                ->like('C.othernames', '%' . $keywords . '%')->OR
                ->like('C.wrongnames', '%' . $keywords . '%')->UNNEST;
        }
        $select->order('C.name ASC')->limit('150');
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $result       = array_merge([['id' => '0', 'name' => '0']], $result->toArray());
        return $result;
    }

    public function getWs1cList(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['C'=>'nx_ws1c_company'])
            ->columns([
                'id',
                'name',
                'inn',
                'kpp'
            ])
            ->order('C.name ASC');

        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                switch($filter['property'])
                {
                    case 'name':
                    case 'inn':
                    case 'kpp':
                        $select->where->like('C.' . $filter['property'], '%' . $filter['value'] . '%');
                        break;
                }
            }
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        return $result->toArray();
    }

    public function reWs1cList()
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        try
        {
            /*$options = array(
                'soap_version' => SOAP_1_1,
                'exceptions'   => true,
                'trace'        => 1,
                'cache_wsdl'   => WSDL_CACHE_NONE,
                'login'        => 'NxUser',
                'password'     => 'XVb0LJmE9h'
            );
            $client = new \SoapClient('http://teta.osp.ru:8081/complex/ws/ws1.1cws?wsdl', $options);*/
            $client = $this->ws1c_client;

            $result = $client->Contractors(
                []
            );
            $result = $result->return->Contractor;
            if(is_object($result))
            {
                $result = [$result];
            }
            if(!empty($result)) {
                $db->query('TRUNCATE TABLE nx_ws1c_company', $db::QUERY_MODE_EXECUTE);
            }
            foreach($result as $item)
            {
                $item = (array)$item;
                $values = [
                    'name' => $item['Name'],
                    'inn'  => $item['INN'],
                    'kpp'  => $item['KPP']
                ];
                $insert      = $sql->insert('nx_ws1c_company')->values($values);
                $queryString = $sql->getSqlStringForSqlObject($insert);
                $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
            }
        }
        catch(\Exception $e) {
            throw $e;
        }
    }
}
