<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Notification extends AbstractTable
{
    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_notification', $adapter);
    }

    public function getNotificationsBy(array $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['N'=>$this->table])
            ->columns([
                'id',
                'message',
                'type',
                'checked',
                'created',
                'last_updated'
            ])
            ->order('N.created DESC');

        if(!empty($params['calc_found_rows'])) {
            $select->quantifier(new Expression('SQL_CALC_FOUND_ROWS'));
        }
        if(!empty($params['limit'])) {
            $select->limit((int)$params['limit']);
        }
        if(!empty($params['offset'])) {
            $select->offset((int)$params['offset']);
        }
        if(!empty($params['current'])) {
            $select->limit(1);
        }

        $not_filters = ['columns', 'limit', 'offset', 'order', 'filters', 'group', 'calc_found_rows'];
        $filters_names = array_diff(array_keys($params), $not_filters);
        $filters = [];
        if(!empty($params['filters'])) {
            $filters = $params['filters'];
        }
        foreach($filters_names as $filter_name) {
            $find = 0;
            foreach($filters as $filter) {
                if($filter['property'] == $filter_name) {
                    $find = 1;
                }
            }
            if(!$find) {
                $filters[] = ['property' => $filter_name, 'value' => $params[$filter_name]];
            }
        }
        if(!empty($filters))
        {
            foreach($filters as $filter)
            {
                switch($filter['property'])
                {
                    case 'id':
                    case 'type':
                    case 'checked':
                        $select->where(['N.'.$filter['property'] => $filter['value']]);
                        break;
                    case 'message':
                    case 'created':
                    case 'last_updated':
                        $select->where->like('N.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                }
            }
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $items        = !empty($params['current']) ? $result->current() : $result;//->toArray();

        if(!empty($params['calc_found_rows'])) {
            $count = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();
        }
        $result = !empty($params['calc_found_rows']) ? ['items' => $items, 'total' => $count['count']] : $items;

        return $result;
    }

    public function getNotificationBy($params)
    {
        $params['current'] = true;
        return $this->getNotificationsBy($params);
    }

    public function updateNotification($data, $where)
    {
        $notification = $this->getNotificationBy($where);
        if(empty($notification))
        {
            return false;
        }
        $this->update($data, $where);
        return $notification->id;
    }
}
