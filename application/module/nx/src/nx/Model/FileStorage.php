<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

//require_once 'vendor/pclzip.lib.php';
require_once __DIR__ . '/../' . 'pclzip.lib.php';

function renamePcLibZipAddFile($p_event, &$p_header)
{
    static $repeats = [];
    //static $cnt     = 1;
    //static $uName   = '';
    //global $PCLZIP_CB_PRE_ADD_FILE_NAME;
    //if (!empty($PCLZIP_CB_PRE_ADD_FILE_NAME)) {
        //if ($uName != $PCLZIP_CB_PRE_ADD_FILE_NAME) {
            //$cnt = 1;
        //}
        //$uName = $PCLZIP_CB_PRE_ADD_FILE_NAME;
    $info = pathinfo($p_header['stored_filename']);
    if(!isset($repeats[$p_header['stored_filename']])) {
        $repeats[$p_header['stored_filename']] = 0;
    }
    $repeats[$p_header['stored_filename']]++;
    //$p_header['stored_filename'] = $PCLZIP_CB_PRE_ADD_FILE_NAME . '_' . $cnt . '.' . $info['extension'];
    //$p_header['stored_filename'] = $info['filename'] . '_' . $cnt . '.' . $info['extension'];
    $p_header['stored_filename'] = $info['filename'] . '_' . $repeats[$p_header['stored_filename']] . '.' . $info['extension'];
        //$cnt++;
    //} else {
    //    $p_header['stored_filename'] = md5($p_header['filename']) . '_' . $p_header['stored_filename'];
    //}
    return 1;
}
function zipPreExtractCallBack($p_event,&$p_header)
{
    static $cnt_extract   = 1;
    $info = pathinfo($p_header['filename']);
    $p_header['filename'] = $info['dirname'] . '/' . $cnt_extract.'.'.$info['extension'];
    $cnt_extract++;
    return 1;
}

class FileStorage extends AbstractTable
{
    const TYPE_1C_BILL = 1;
    const TYPE_1C_ACT = 2;
    const TYPE_1C_TORG12 = 3;
    const TYPE_1C_INVOICE = 4;
    const TYPE_SHIPMENT_DATA = 5;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_file_storage', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\FileStorage));
    }

    public function getList($offset, $limit, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['FS'=>$this->table])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS FS.id AS id'),
                'name'
            ]);
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(empty($filter['value']))
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'entity_type':
                    case 'entity_id':
                        $select->where->equalTo('FS.'.$filter['property'], $filter['value']);
                        break;
                    case 'name':
                    case 'id':
                        $select->where->like('FS.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'type':
                        $select->where->equalTo('FS.'.$filter['property'], $filter['value']);
                        break;
                }
            }
        }

        $select->order('FS.id DESC')
            ->limit($limit)
            ->offset($offset);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function getFiles($params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['FS'=>$this->table])
            ->columns([
                'id',
                'name',
                'hash',
                'entity_id',
                'entity_type',
                'path'
            ])
            ->order('FS.id DESC');
        if(!empty($params['id']))
        {
            $select->where(['FS.id' => $params['id']]);
        }
        if(!empty($params['path']))
        {
            $select->where(['FS.path' => $params['path']]);
        }
        if(!empty($params['hash']))
        {
            $select->where(['FS.hash' => $params['hash']]);
        }
        if(!empty($params['type']))
        {
            $select->where(['FS.type' => $params['type']]);
        }
        if(!empty($params['entity_id']))
        {
            $select->where(['FS.entity_id' => $params['entity_id']]);
        }
        if(!empty($params['entity_type']))
        {
            $select->where(['FS.entity_type' => $params['entity_type']]);
        }
        if(isset($params['label']))
        {
            $select->where(['FS.label' => $params['label']]);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getBy(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['FS'=>$this->table])
            ->columns([
                'id',
                'name',
                'path',
                'entity_type',
                'entity_id',
                'hash',
                'type',
                'deleted',
                'created',
                'last_updated',
                'create_user_id',
                'last_user_id'
            ])
            ->join(['U'=>'User'], 'U.User_ID = FS.create_user_id', ['create_user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')], 'left')
            ->join(['U2'=>'User'], 'U2.User_ID = FS.last_user_id', ['last_user_name' => new Expression('IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')], 'left')
            ->limit(1);
        if(!empty($params['id']))
        {
            $select->where(['FS.id' => (int)$params['id']]);
        }
        if(!empty($params['path']))
        {
            $select->where(['FS.path' => $params['path']]);
        }
        if(!empty($params['hash']))
        {
            $select->where(['FS.hash' => $params['hash']]);
        }
        if(!empty($params['type']))
        {
            $select->where(['FS.type' => (int)$params['type']]);
        }
        if(!empty($params['entity_id']))
        {
            $select->where(['FS.entity_id' => (int)$params['entity_id']]);
        }
        if(!empty($params['entity_type']))
        {
            $select->where(['FS.entity_type' => $params['entity_type']]);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function putContents($contents, $data)
    {
        if(empty($data['fileStorageData']['path']))
        {
            return false;
        }
        if(empty($data['fileStorageData']['name']))
        {
            $data['fileStorageData']['name'] = basename($data['fileStorageData']['path']);
        }
        if(!empty($data['fileStorageData']['create_user_id']) && empty($data['fileStorageData']['last_user_id']))
        {
            $data['fileStorageData']['last_user_id'] = $data['fileStorageData']['create_user_id'];
        }
        $data['fileStorageData']['created'] = date('Y-m-d H:i:s');
        $data['fileStorageData']['hash']    = md5($data['fileStorageData']['path'] . time());

        $check = $this->getBy(['path' => $data['fileStorageData']['path']]);

        $dir = pathinfo($data['fileStorageData']['path'], PATHINFO_DIRNAME);
        if(!file_exists($dir)) {
            \nx\functions\makedir($dir);
        }

        if(!file_put_contents($data['fileStorageData']['path'], $contents))
        {
            return false;
        }
        //chmod($data['fileStorageData']['path'], 0777);
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(empty($check->id))
            {
                $this->insert($data['fileStorageData']);
                $file_id = $this->lastInsertValue;
                if(!empty($data['fileStorageData']['create_user_id']))
                {
                    $this->logger->log('Добавление файла ' . $data['fileStorageData']['name'] . ' #' . $file_id, 'file_put_contents', $data, !empty($data['fileStorageData']['create_user_id']) ? $data['fileStorageData']['create_user_id'] : 0);
                }
            }
            else
            {
                $file_id = (int)$check->id;
                $this->update($data['fileStorageData'], ['id' => $file_id]);
                if(!empty($data['fileStorageData']['last_user_id']))
                {
                    $this->logger->log('Обновление файла ' . $data['fileStorageData']['name'] . ' #' . $file_id, 'file_put_contents', $data, !empty($data['fileStorageData']['last_user_id']) ? $data['fileStorageData']['last_user_id'] : 0);
                }
            }
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return [$file_id, $data['fileStorageData']['hash']];
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }

    public function createZipAttachment($ids, array $params = [])
    {
        $files = $this->getFiles(['id' => $ids]);
        if (!empty($files))
        {
            $files = $files->toArray();
            usort($files, function($a, $b) use($ids){
                $k1 = (int)array_search($a['id'], $ids);
                $k2 = (int)array_search($b['id'], $ids);
                if ($k1 == $k2) {
                    return 0;
                }
                return ($k1 < $k2) ? -1 : 1;
            });
            $filesToCompress = [];
            $fileUrl         = 'data/tmp/' . strtoupper(md5(time() . implode('_', $ids))) . '_object.zip';
            $filePath        = 'public/' . $fileUrl;
            $zip             = new \PclZip($filePath);
            foreach ($files as $file)
            {
                if (!file_exists($file['path'])) {
                    continue;
                }
                $filesToCompress[] = $file['path'];
            }

            /*$rename_func = '';
            if(empty($params['norename'])) {
                $rename_func = 'renamePcLibZipAddFile';
            }

            if($rename_func)
            {
                $zip->add(
                    implode(',', $filesToCompress),
                    PCLZIP_OPT_REMOVE_ALL_PATH,
                    PCLZIP_OPT_NO_COMPRESSION,
                    PCLZIP_CB_PRE_ADD, $rename_func
                );
            }
            else
            {*/
            $rename_func = '\nx\Model\renamePcLibZipAddFile';
            $result = $zip->add(
                implode(',', $filesToCompress),
                PCLZIP_OPT_REMOVE_ALL_PATH,
                PCLZIP_OPT_NO_COMPRESSION,
                PCLZIP_CB_PRE_ADD, $rename_func
            );
                //var_dump($filesToCompress, $result, $zip->errorInfo());
            //}
            if(file_exists($filePath))
            {
                return [
                    'url'  => $fileUrl,
                    'path' => $filePath
                ];
            }
            else {
                throw new \Exception($zip->errorInfo());
            }
        }
        return False;
    }

    /*public function updateContents($contents, $data)
    {
        if(empty($data['fileStorageData']['path']))
        {
            return false;
        }
        if(empty($data['fileStorageData']['name']))
        {
            $data['fileStorageData']['name'] = basename($data['fileStorageData']['path']);
        }
        $data['fileStorageData']['created'] = date('Y-m-d H:i:s');
        $data['fileStorageData']['hash']    = md5($data['fileStorageData']['path']);

        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {

            file_put_contents($data['fileStorageData']['path'], $item['Bill']);

            $this->insert($data['fileStorageData']);
            $insert_id = $this->lastInsertValue;
            $this->logger->log('Добавление файла ' . $data['fileStorageData']['name'] . ' #' . $insert_id, 'file_put_contents', $data, !empty($data['fileStorageData']['create_user_id']) ? $data['fileStorageData']['create_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $insert_id;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }*/
}
