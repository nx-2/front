<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Stdlib\Hydrator\ArraySerializable;


class Address extends AbstractTable
{
    const AHUNTER_PARSE_RESULT = 1;
    const AHUNTER_USER = 'aHunterApiToken'; // see https://ahunter.ru/
    const COUNTRY_ID_RUSSIA = 165;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_address', $adapter, null, new HydratingResultSet(new ArraySerializable, new \nx\Entity\Address));
    }

    public function getList($offset, $limit, $params = [])
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        $select = $sql->select()
            ->from(['A' => $this->table])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS A.id AS id'),
                'address',
                'phone',
                'fax'
            ])
            //->join(array('G' => 'User_Group'), 'G.User_ID = U.User_ID')
            //->where('G.PermissionGroup_ID = ' . self :: Netcat_Permission_Group_ID)
            ->order('A.id ASC')
            ->limit($limit)
            ->offset($offset);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result = $db->query($selectString)->execute();
        $count = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function getByID($id)
    {
        $db = $this->adapter;
        $sql = new Sql($db);
        $select = $sql->select()
            ->from(['A' => $this->table])
            ->columns(['id', 'address', 'phone', 'fax'])
            ->where(['A.id' => $id]);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function processAhunterRequest($query, $mode = 0)
    {
        if (empty($query) && false) // метод отключен
        {
            return false;
        }

        $cmd = 'mcheck';//check | mcheck | verify
        $format = 'xml';
        $query = mb_detect_encoding($query, 'UTF-8', true) ? iconv('utf-8', 'windows-1251', $query) : $query;
        $query = urlencode($query);
        $hash = md5($cmd . '-' . $format . '-' . $query);

        $request = $cmd . '?user=' . self::AHUNTER_USER . ';output=' . $format . ';query=' . $query;

        $db = $this->adapter;
        $sql = new Sql($db);
        $select = $sql->select()->from(['C' => 'nx_cache_ahunter'])->columns(['id', 'result'])->where(['C.hash' => $hash]);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $result = $result->current();

        if (!empty($result['result'])) {
            $output = $result['result'];
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://ahunter.ru/site/" . $request);
            //curl_setopt($ch, CURLOPT_POST, true);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, 'query='.urlencode($address));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);

            $output = iconv('windows-1251', 'utf-8', $output);

            $insert = $sql->insert('nx_cache_ahunter')->values([
                    'query' => $query,
                    'url' => $request,
                    'hash' => $hash,
                    'result' => $output,
                    'created' => date('Y-m-d H:i:s')
                ]
            );
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $result = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        }
        $this->logger->log_console($output);
        if (!empty($output) && $mode == self::AHUNTER_PARSE_RESULT) {
            $data = [];
            $output = str_replace('encoding="windows-1251"', 'encoding="utf-8"', $output);
            $xml = simplexml_load_string($output);
            $check = (int)$xml->CheckInfo->Alts;
            if (empty($check)) {
                return false;
            } else {
                if ($check == 1) {
                    $addrs = [$xml->Address];
                } else {
                    $addrs = $xml->Address;
                }
                foreach ($addrs as $addr) {
                    $fields = $addr->Field;
                    if (!empty($fields)) {
                        $cur = [];
                        foreach ($fields as $field) {
                            $field = (array)$field;
                            $attrs = $field['@attributes'];
                            $map = [
                                'Region' => 'area',
                                'District' => 'region',
                                'City' => 'city',
                                'Place' => 'city1',
                                'Street' => 'street',
                                'House' => 'house',
                                'Building' => 'building',
                                'Structure' => 'structure',
                                'Flat' => 'apartment',
                                'Zip' => 'zipcode'
                            ];
                            $type = $attrs['type'] && !in_array($attrs['level'], ['Zip', 'Flat', 'Structure', 'Building', 'House']) ? ' ' . $attrs['type'] : '';
                            /*if($map[$attrs['level']]=='house' && empty($attrs['c'])) {
                                $cur[$map[$attrs['level']]] = '';
                            }
                            else {*/
                            $cur[$map[$attrs['level']]] = $attrs['name'] . $type;
                            //}
                        }
                        $data[] = $cur;
                    }
                }
            }
            return $data;
        }

        return $output;
    }
}
