<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use ArrayObject;
use FilterIterator;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\Adapter\MemcachedOptions;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use function nx\functions\is_assoc;

class ResultSetFilterIterator extends FilterIterator
{
    private $accepter = null;

    public function __construct($iterator, $callback)
    {
        parent::__construct($iterator);
        $this->accepter = $callback;
    }

    public function accept()
    {
        $accept = call_user_func_array($this->accepter, [$this->current()]);
        return $accept;
    }
}

class AbstractTable extends TableGateway
{
    public $logger;
    public $cache;
    public $publisher_id;
    //public $throwExceptions = 1;

    //public $isTransactional = 1;
    public function __construct($table, $adapter, $features = null, $prototype = null)
    {
        if (!($this instanceof Log)) {
            $this->logger = new Log($adapter);
        }
        $this->cache = new Memcached(new MemcachedOptions([
            'ttl' => 60,
            'namespace' => 'nx_memcache_listener',
            'key_pattern' => null,
            'readable' => true,
            'writable' => true,
            'servers' => '127.0.0.1',
        ]));
        parent::__construct($table, $adapter, $features, $prototype);

        $user_session = new Container('user');
        $user_session = $user_session->offsetGet('user');

        $this->publisher_id = (!empty($user_session) && isset($user_session['publisher_id']) && !empty($user_session['publisher_id'])) ? $user_session['publisher_id'] : false;

    }

    public function isTransactional()
    {
        return $this->adapter->isTransactional;
    }

    public function getCache($functionName, $functionArgs)
    {
        $cacheObjects = $this->cache->getItem($this->getCacheKey($functionName, $functionArgs));
        return isset($cacheObjects['objects']) ? $cacheObjects['objects'] : False;
    }

    public function setCache($data, $functionName, $functionArgs, $ttl = 0)
    {
        if ($ttl) {
            $this->cache->getOptions()->setTtl($ttl);
        }
        $this->cache->setItem($this->getCacheKey($functionName, $functionArgs), ['objects' => $data]);
    }

    public function getCacheKey($functionName, $functionArgs)
    {
        $functionArgs = array_map(create_function('$f', 'return is_array($f) ? serialize($f) : $f;'), $functionArgs);
        $key = get_called_class() . '_' . $functionName . '_' . implode('_', $functionArgs);
        //$key = preg_replace('#[^\w]#isu','',$key);
        $key = md5($key);
        return $key;
    }

    public function removeCache($functionName, $functionArgs)
    {
        $this->cache->removeItem($this->getCacheKey($functionName, $functionArgs));
    }

    public function getByMap($params)
    {
        $db = $this->adapter;
        $sql = new Sql($db);

        $join_map = !empty($params['join_map']) ? $params['join_map'] : [];//array(//alias => table, condition, dependent_table, load: table_field, entity_field, filter callback
        $columns_map = !empty($params['columns_map']) ? $params['columns_map'] : [];//array(//array('table'=>,'column'=>,'joins'=>,'columns'=>,'columns_map'=>)
        $group_map = !empty($params['group_map']) ? $params['group_map'] : [];
        $order_map = !empty($params['order_map']) ? $params['order_map'] : [];
        $joins = !empty($params['joins']) ? $params['joins'] : [];
        $loads = [];


        $columns_map = $this->normalize_columns_map($columns_map);

        if (!empty($params['filters']) && !empty($params['columns_apply'])) {//move columns from 'columns_apply' paramater to 'columns[_left]' paramater if filter matched
            foreach ($params['filters'] as $filter) {
                foreach ($params['columns_apply'] as $type => $columns) {
                    $coltype = 'columns' . ($type == 'left' ? '_left' : '');
                    foreach ($columns as $i => $column) {
                        if ($column == $filter['property']) {
                            if (empty($params[$coltype])) {
                                $params[$coltype] = [$column];
                            } else if (!in_array($column, $params[$coltype])) {
                                $params[$coltype][] = $column;
                            }
                            unset($params['columns_apply'][$type][$i]);
                            break;
                        }
                    }
                }
            }
        }

        //join inner, left, load columns into single columns_db array
        list($columns_names, $columns_db) = $this->get_columns2($params, $columns_map);

        $select = $sql->select()->from(['TBL' => !empty($params['table']) ? $params['table'] : $this->table]);

        if (!empty($params['calc_found_rows'])) {
            $select->quantifier(new Expression('SQL_CALC_FOUND_ROWS'));
        }
        if (!empty($params['limit'])) {
            $select->limit((int)$params['limit']);
        }
        if (!empty($params['offset'])) {
            $select->offset((int)$params['offset']);
        }
        if (!empty($params['current'])) {
            $select->limit(1);
        }
        if (!empty($params['sorter_func']) && !empty($params['sorters'])) {
            foreach ($params['sorters'] as $sorter) {
                if (empty($sorter['direction'])) {
                    continue;
                }
                $params['sorter_func']($sorter, $select);
            }
        } else {
            if (!empty($params['order'])) {
                $select->order($params['order']);
            }
        }

        //process filters = add where, add joins if needed, del loads
        $filters = $this->get_filters($params);
        if (!empty($filters) && !empty($params['filter_func'])) {
            foreach ($filters as $filter) {
                $params['filter_func']($filter, $select, $joins);
            }
        }

        $this->retype_columns($columns_db, $joins, $columns_map);

        //process columns
        list($columns, $joins_by_cols, $loads_by_cols) = $this->process_columns2($columns_db, $columns_map);
        $joins = array_merge($joins, $joins_by_cols);
        $loads = array_merge($loads, $loads_by_cols);

        $select->columns($columns);

        //process joins
        $joins = $this->process_joins($joins, $join_map);
        foreach ($join_map as $key => $table) {
            if (!empty($joins[$key])) {
                $select->join([$key => $table[0]], $table[1], [], $joins[$key]);
            }
        }

        if (!empty($params['group'])) {
            $group_by = $params['group'];
            if (is_string($params['group']) && !empty($group_map[$params['group']])) {
                $group_by = $group_map[$params['group']];
            }
            $select->group($group_by);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);//var_dump($selectString);//die;
        if (!empty($params['debug'])) {
            var_dump($selectString);
        }
        $result = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);//)->execute();

        $resultSet = iterator_to_array($result);

        $this->process_result($resultSet, $columns_map);

        $count = count($resultSet);
        //$count = $resultSet->count();
        if ($count && !empty($params['calc_found_rows'])) {
            $count = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();
            $count = $count['count'];
        }

        $this->process_loads($loads, $join_map, $columns_map, $resultSet);

        if (!empty($params['columns_apply']) && !empty($resultSet)) {
            $ids = [];
            foreach ($resultSet as $row) {
                if (!empty($row)) {
                    $ids[] = $row['id'];
                }
            }
            if (!empty($ids)) {
                $xparams = [
                    'as_array' => true,
                    'columns_map' => (!empty($params['columns_map']) ? $params['columns_map'] : []),
                    'join_map' => (!empty($params['join_map']) ? $params['join_map'] : []),
                    'table' => (!empty($params['table']) ? $params['table'] : ''),
                    'filter_func' => (!empty($params['filter_func']) ? $params['filter_func'] : false),
                    'filters' => [['property' => 'id', 'value' => $ids]],
                    'debug' => (!empty($params['debug']) ? $params['debug'] : false),
                ];
                foreach ($params['columns_apply'] as $type => $columns) {
                    $curparams = $xparams;
                    $coltype = 'columns' . ($type == 'left' ? '_left' : '');
                    $curparams[$coltype] = $columns;
                    if (!empty($curparams['columns'])) {
                        $curparams['columns'][] = 'id';
                    } else {
                        $curparams['columns'] = ['id'];
                    }
                    $extras = $this->getByMap($curparams);
                    if (!empty($extras)) {
                        $extras_by_id = [];
                        foreach ($extras as $row) {
                            $extras_by_id[$row['id']] = $row;
                        }
                        foreach ($resultSet as $row) {
                            if (!empty($extras_by_id[$row['id']])) {
                                foreach ($extras_by_id[$row['id']] as $key => $val) {
                                    if ($key != 'id') {
                                        $row->$key = $val;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!empty($params['result_func'])) {
            $params['result_func']($resultSet, $columns_names, $join_map, $params);
        }

        if (empty($params['as_array'])) {
            $resultSet = array_map('get_object_vars', $resultSet);
            $returnSet = clone $this->resultSetPrototype;
            $returnSet->initialize($resultSet);
            $returnSet->buffer();
        } else {
            $returnSet = $resultSet;
        }

        if (!empty($params['current'])) {
            if (empty($params['as_array'])) {
                $returnSet->rewind();
                $returnSet = $returnSet->current();
            } else {
                $returnSet = !empty($returnSet[0]) ? $returnSet[0] : false;
            }
        }

        $result = !empty($params['calc_found_rows']) ? ['items' => $returnSet, 'total' => $count] : $returnSet;

        return $result;
    }

    public function normalize_columns_map($columns_map)
    {
        $result = [];
        foreach ($columns_map as $key => $map) {
            //if(empty($map)) {
            //    continue;
            //}
            $cur = [];
            if (is_array($map)) {
                if (is_assoc($map)) {
                    if (!empty($map['columns_map'])) {
                        $map['columns_map'] = $this->normalize_columns_map($map['columns_map']);
                    }
                    $cur = $map;
                } else {
                    $cur['table'] = $map[0];
                    $cur['column'] = $map[1];
                }
            } else if (!empty($map)) {
                $cur['column'] = $map;//expression
            } else {
                $cur = $map;
            }
            $result[$key] = $cur;
        }
        return $result;
    }

    public function get_columns2($params, $columns_map)
    {
        $columns_names = [];
        $columns_db = [];
        $me = $this;
        $passColumn = function ($column, $type) use (&$columns_names, &$columns_db, &$params, &$columns_map, &$me) {
            $columns_names[] = $column;
            if (isset($columns_map[$column])) {//entity join/load fields
                $c = $columns_map[$column];
                //if(isset($c['columns_map']))
                if (!empty($c['table']) && empty($c['column'])) {
                    $pkey = 'columns' . ($type != 'inner' ? '_' . $type : '');
                    $cm = $me->get_columns2([$pkey => $params[$pkey][$column]], !empty($c['columns_map']) ? $c['columns_map'] : []);
                    if (!isset($columns_db[$column])) {
                        $columns_db[$column] = $cm[1];
                    } else {
                        $columns_db[$column] = array_merge($columns_db[$column], $cm[1]);
                    }
                }
                if (!empty($c['columns'])) {
                    foreach ($c['columns'] as $col) {
                        $columns_db[$col] = $type;//!empty($columns_map[$col]) ? $type : 'inner';
                    }
                }
                if (!empty($c['column'])) {
                    $columns_db[$column] = $type;
                }
            } else {//entity table-self fields
                $columns_db[$column] = $type;//'inner';
            }
        };
        $columns_param = !empty($params['columns_left']) ? $params['columns_left'] : [];
        foreach ($columns_param as $key => $val) {
            $column = is_numeric($key) ? $val : $key;
            $passColumn($column, 'left');
        }
        $columns_param = !empty($params['columns_load']) ? $params['columns_load'] : [];
        foreach ($columns_param as $key => $val) {
            $column = is_numeric($key) ? $val : $key;
            $passColumn($column, 'load');
        }
        $columns_param = !empty($params['columns']) ? $params['columns'] : (empty($columns_names) ? ['id'] : []);
        foreach ($columns_param as $key => $val) {
            $column = is_numeric($key) ? $val : $key;
            $passColumn($column, 'inner');
        }
        return [$columns_names, $columns_db];
    }

    public function get_filters($params, array $not_filters = [])
    {
        $not_filters = array_merge($not_filters, [
            'columns', 'columns_left', 'columns_load', 'limit', 'offset', 'order', 'filters', 'group',
            'calc_found_rows', 'sorters', 'as_array', 'current', 'result_func', 'filter_func', 'sorter_func',
            'columns_map', 'join_map', 'debug'
        ]);
        $filters_names = array_diff(array_keys($params), $not_filters);
        $filters = [];
        if (!empty($params['filters'])) {
            $filters = $params['filters'];
        }
        foreach ($filters_names as $filter_name) {
            $operator_map = [
                'not' => '!=',
            ];
            $filter_op = '';
            $filter_value = $params[$filter_name];
            if (mb_substr($filter_name, 0, 9) == 'operator_') {
                $filter = explode('_', $filter_name);
                $filter_op = !empty($operator_map[$filter[1]]) ? $operator_map[$filter[1]] : '';
                $filter_name = implode('_', array_slice($filter, 2));
            }
            $find = 0;
            foreach ($filters as $filter) {
                if ($filter['property'] == $filter_name) {
                    $find = 1;
                }
            }
            if (!$find) {
                $filter = ['property' => $filter_name, 'value' => $filter_value];
                if ($filter_op) {
                    $filter['xoperator'] = $filter_op;
                }
                $filters[] = $filter;
            }
        }
        return $filters;
    }

    public function retype_columns(&$columns_db, $joins, $columns_map, $type = '')
    {
        foreach ($columns_db as $column => $type) {
            $c = !empty($columns_map[$column]) ? $columns_map[$column] : [];
            $type = (!empty($c['table']) && !empty($joins[$c['table']])) ? $joins[$c['table']] : $type;
            if ($type) {
                if (is_array($columns_db[$column])) {
                    $this->retype_columns($columns_db[$column], $joins, !empty($c['columns_map']) ? $c['columns_map'] : [], $type);
                } else {
                    $columns_db[$column] = $type;
                }
            }
        }
    }

    public function process_columns2($columns_db, $columns_map, $column_prefix = '', $table = '')
    {
        $joins = [];
        $loads = [];
        $columns = [];
        foreach ($columns_db as $column => $type) {
            if (isset($columns_map[$column])) {
                $c = $columns_map[$column];
                if (!empty($c['table']) && empty($c['column']) && is_array($type)) {
                    $cjl = $this->process_columns2($type, !empty($c['columns_map']) ? $c['columns_map'] : [], $column . '/', $c['table']);
                    $columns = array_merge($columns, $cjl[0]);
                    $joins = array_merge($joins, $cjl[1]);
                    $loads = array_merge($loads, $cjl[2]);
                } else {
                    if ($type == 'load' && !empty($c['table'])) {
                        if (!isset($loads[$c['table']])) {
                            $loads[$c['table']] = [];
                        }
                        $loads[$c['table']][$column] = $c['column'];
                    } else {
                        if (!empty($c['column'])) {
                            $columns[$column_prefix . $column] = new Expression((!empty($c['table']) ? $c['table'] . '.' : '') . $c['column']);
                        }
                        if (!empty($c['table'])) {
                            $joins[$c['table']] = $type;
                        }
                        if (!empty($c['joins'])) {
                            foreach ($c['joins'] as $join_table) {
                                $joins[$join_table] = $type;
                            }
                        }
                    }
                }
            } else {
                if (!empty($table)) {
                    if ($type == 'load') {
                        if (!isset($loads[$table])) {
                            $loads[$table] = [];
                        }
                        $loads[$table][$column] = $column;
                    } else {
                        $joins[$table] = $type;
                        $columns[$column_prefix . $column] = new Expression($table . '.' . $column);
                    }
                } else {
                    $columns[] = $column;
                }
            }
        }
        return [$columns, $joins, $loads];
    }

    public function process_joins($joins, $join_map)
    {
        foreach ($joins as $key => $join_type) {
            $joins = $this->process_join($joins, $join_map, $key, $join_type);
        }
        return $joins;
    }

    public function process_join($joins, $join_map, $key, $join_type)
    {
        if (!empty($join_map[$key][2]) && !empty($join_type) && empty($joins[$join_map[$key][2]])) {
            $joins[$join_map[$key][2]] = $join_type;
            $joins = $this->process_join($joins, $join_map, $join_map[$key][2], $join_type);
        }
        return $joins;
    }

    public function process_result($resultSet, $columns_map)
    {
        $entity_columns = [];
        foreach ($columns_map as $col => $cur) {
            if (!empty($cur['table']) && empty($cur['column'])) {
                $entity_columns[] = $col;
            }
        }
        foreach ($resultSet as $row) {
            $old = clone $row;
            foreach ($old as $key => $val) {
                $prop = explode('/', $key);
                if (in_array($prop[0], $entity_columns)) {
                    if (!isset($row->$prop[0])) {
                        $row->$prop[0] = new ArrayObject([$prop[1] => $val], ArrayObject::ARRAY_AS_PROPS);
                    } else {
                        $row->$prop[0]->$prop[1] = $val;
                    }
                    unset($row->$key);
                }
            }
        }
    }

    public function process_loads($loads, $join_map, $columns_map, $resultSet)
    {
        foreach ($loads as $table => $cols) {
            if (!empty($join_map[$table])) {
                $l = $join_map[$table];
                $filterSet = $resultSet;
                if (!empty($l[5])) {
                    //$filterSet = new ResultSetFilterIterator($resultSet, $l[5]);
                    $filterSet = array_filter($resultSet, $l[5]);
                }
                $property = '';
                foreach ($columns_map as $key => $cur) {
                    if (!empty($cur['table']) && empty($cur['column']) && $cur['table'] == $table) {
                        $property = $key;
                    }
                }
                $this->loadExtra($cols, $l[0], $l[3], $filterSet, $l[4], $property);
            }
        }
    }

    /*
    Функция осуществляет сборку запроса с помощью zf query builder на основе массива параметров
    на стадии разработки - дописывается по мере необходимости расширения возможностей составления запроса

    $params = array(

    'table'             => 'nx_shipment_item',                                    // таблица для выборки, по-умолчанию $this->table
    'debug'             => true,                                                  // var_dump($selectString);
    'offset'            => 0,                                                     //
    'limit'             => 25,                                                    //
    'order'             => 'TBL.id DESC',                                         //
    'group'             => 'TBL.id',                                              //
    'current'           => true,                                                  // возвращает первый результат запроса
    'columns'           => array('id', 'name'),                                   // колонки для выборки (inner join)
    'columns_apply'     => array('left' => array('customer_name', 'zipcode')),    // колонки для выборки для первых limit записей, с помощью повторного запроса
    'columns_left'      => array('zipcode', 'customer_name'),                     // колонки для выборки с помощью left join объединения с нужными таблицами
    'columns_load'      => array('order' => array('order_state_id')),             // колонки для выборки из другой таблицы с помощью loadExtra фции, таблица должны быть задана в columns_map
    'columns_map'       => array('email' => array('O', 'email')),                 // массив возможных колонок
    'join_map'          => array('O' => array('nx_order', 'O.id = OI.order_id')), // массив возможных join'ов
    'joins'             => array('O' => 'left', 'S' => 'inner'),                  // joins
    'calc_found_rows'   => true,                                                  // посчитать количество строк, результат будет представлен в виде массива array('items', 'total');
    'as_array'          => true,                                                  // результат как массив, если false, то как объекты this->resultSetPrototype
    'filters'           => array(array('property' => , 'value' => )),             //
    'sorters'           => array(array('property' => , 'direction' => )),         //
    'sorter_func'       => function(){},                                          // foreach(sorters) $sorter_func($sorter, $select);
    'filter_func'       => function(){},                                          // foreach(filters) $filter_func($filter, $select, $joins);
    'result_func'       => function(){},                                          //

    );

    examples:
    */

    public function loadExtra($columns, $table, $table_field, $items, $item_field, $extra_field = '')
    {
        $cols = [];
        foreach ($columns as $key => $val) {
            $cols[is_numeric($key) ? $val : $key] = $val;
        }
        $ids = [];
        foreach ($items as $item) {
            if (!empty($item->$item_field)) {
                $ids[] = $item->$item_field;
            }
        }
        if (!empty($ids)) {
            $db = $this->adapter;
            $sql = new Sql($db);
            $select = $sql->select()
                ->columns(array_merge([$table_field], array_values($cols)))
                ->from(['T' => $table])
                ->where(['T.' . $table_field => $ids]);
            $selectString = $sql->getSqlStringForSqlObject($select);//var_dump($selectString);
            $result = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $result_by_ids = [];
            foreach ($result as $cur) {
                $result_by_ids[$cur->$table_field] = $cur;
            }
            if (!$extra_field) {
                foreach ($items as $item) {
                    if (isset($result_by_ids[$item->$item_field])) {
                        foreach ($cols as $item_col => $table_col) {
                            $item->$item_col = $result_by_ids[$item->$item_field][$table_col];
                        }
                    }
                }
            } else {
                foreach ($items as $item) {
                    if (isset($result_by_ids[$item->$item_field])) {
                        $item->$extra_field = $result_by_ids[$item->$item_field];
                    }
                }
            }
        }
    }
}
