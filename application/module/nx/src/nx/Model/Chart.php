<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Chart extends AbstractTable
{
    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_chart', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Chart));
    }

    public function loadChartBy(array $params = [])
    {
        $chart = $this->getChartBy($params);

        $db  = $this->adapter;
        $sql = new Sql($db);
        $chart->data = $db->query($chart->query, $db::QUERY_MODE_EXECUTE)->toArray();

        return $chart;
    }

    public function getChartBy(array $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['C'=>$this->table])
            ->columns([
                'id',
                'name',
                'query',
                'type',
                'created',
                'last_updated'
            ])
            ->join(['U'=>'User'], 'U.User_ID = C.create_user_id', ['create_user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')], 'left')
            ->join(['U2'=>'User'], 'U2.User_ID = C.last_user_id', ['last_user_name' => new Expression('IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')], 'left')
            ->limit(1);
        if(!empty($params['id'])) {
            $select->where(['C.id' => (int)$params['id']]);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function getCharts(array $params = [])
    {
        $columns = [
            'id',
            'name'
        ];
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['C' => 'nx_chart'])
            ->columns($columns)
            ->order('id DESC');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        return $resultSet;
    }

    public function updateChart($id, $data)
    {
        if(empty($id) || empty($data))
        {
            return false;
        }
        $chart = $this->getChartBy(['id' => $id]);
        if(empty($chart->id))
        {
            return false;
        }
        try
        {
            if(!empty($data['chartData']))
            {
                unset($data['personData']['create_user_id']);
                unset($data['personData']['created']);
                $this->update($data['chartData'], ['id' => $chart->id]);
            }
            return $chart->id;
        }
        catch(\Exception $e)
        {
            throw $e;
        }
    }

    public function addChart($data)
    {
        if(empty($data['chartData']['name']) || empty($data['chartData']['query']))
        {
            return false;
        }
        try
        {
            $data['chartData']['created'] = date('Y-m-d H:i:s');

            $this->insert($data['chartData']);
            $chart_id = $this->lastInsertValue;
            return $chart_id;
        }
        catch(\Exception $E)
        {
            throw $E;
        }
    }
}
