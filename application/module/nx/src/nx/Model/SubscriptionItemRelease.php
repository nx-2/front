<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class SubscriptionItemRelease extends AbstractTable
{
    private static $ship_channels = [
        'map'           => 1,
        'courier'       => 2,
        'envelope_post' => 3,
        'envelope_dz'   => 4,
        'office'        => 7,
        //'envelope_org' => 5,
    ];

	public $subscriptionService,$fileStorageModel, $subscriptionPdfLogModel;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_order_item_release', $adapter);
    }

    public function getReleaseBy($params = [])
    {
        $by = [
            'current' => true,
            'columns' => [
                'id','item_id','release_id','shipment_id','is_doc_wait','doc_result','last_request','last_request_date',
                'subscription_id','xpress_id', 'year', 'issue_num', 'issue_title', 'type'
            ],
            'columns_left' => ['shipment_date'],
            'id' => (int)$params['id']
        ];
        $result = $this->getShippingsBy($by);
        if(!empty($result['last_request'])) {
            $result['last_request'] = unserialize($result['last_request']);
        }
        return $result;
    }

    public function getShippingsBy(array $params)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);

        $join_map = [//alias => table, condition, dependent_table
            'M'  => ['Edition', 'M.Message_ID = I.MagazineID'],
            'SH' => ['nx_shipment', 'SH.id = II.shipment_id'],
            'SC' => ['nx_ship_channel', 'SC.id = SH.channel_id', 'SH'],
            'PA' => ['nx_payment_agent', 'PA.id = O.payment_agent_id'],
            'A2' => ['nx_address', 'A2.id = O.address_id'],
            'CC' => ['Classificator_Country', 'CC.Country_ID = A2.country_id', 'A2'],
            'A'  => ['nx_address', 'A.id = O.map_address_id'],
            'P'  => ['nx_person', 'P.id = O.person_id'],
            'C'  => ['nx_company', 'C.id = O.company_id'],
            'TE' => ['nx_tag_entity', 'TE.row_id = O.id'],
        ];



        $columns_map = [//array('table'=>,'column'=>,'joins'=>,'columns'=>)
            'subscription_type'     => ['S', 'type'],//new Expression('S.type'),
            'type'                  => ['S', 'type'],
            'shipment_date'         => ['SH', 'date'],// 'nx_shipment'),
            'shipdate'              => ['SH', 'date'],
            'channel_id'            => ['SH', 'channel_id'],
            'channel_name'          => ['SC', 'name'],// array('nx_ship_channel', 'nx_shipment')),
            'periodical_name'       => ['M', 'EnglishName'],// 'Edition'),
            'issue_num'             => ['I', 'Number'],
            'year'                  => ['I', 'year'],
            'release_month'         => ['I', 'release_month'],
            'state'                 => 'IF(I.PublicDate<>"0000-00-00 00:00:00" AND I.PublicDate < NOW(), 1, 0)',
            'order_state_id'        => ['O', 'order_state_id'],
            'price'                 => ['OI', 'price'],
            'discount'              => ['OI', 'discount'],
            'count'                 => 'COUNT(*)',
            'count_pdf'             => 'SUM(IF(S.type="pdf",1,0))',
            'count_paper'           => 'SUM(IF(S.type="paper",1,0))',
            'count_pdf_full'        => 'SUM(IF(S.type="pdf",1,0) * OI.quantity)',
            'count_paper_full'      => 'SUM(IF(S.type="paper",1,0) * OI.quantity)',
            'sum_total_price'       => 'SUM(OI.quantity * (OI.price - OI.price * OI.discount / 100.0))',

            'count_post'            => 'SUM(IF(OI.delivery_type_id IN ("1","7","8","9","10","11"),1,0))',
            'sum_payed_price'       => 'SUM(IF(O.order_state_id="3", OI.quantity * (OI.price - OI.price * OI.discount / 100.0), 0))',
            'sum_payed_paper_price' => 'SUM(IF(S.type="paper" AND O.order_state_id="3", OI.quantity * (OI.price - OI.price * OI.discount / 100.0), 0))',
            'sum_payed_pdf_price'   => 'SUM(IF(S.type="pdf" AND O.order_state_id="3", OI.quantity * (OI.price - OI.price * OI.discount / 100.0), 0))',

            'payment_agent_name'    => ['PA', 'name'],
            'address'               => ['A2', 'address'],
            'country_id'            => ['A2', 'country_id'],
            'phone'                 => ['A2', 'phone'],
            'country_name'          => ['CC', 'Country_Name'],
            'is_enveloped'          => ['A', 'is_enveloped'],
            'area'                  => ['A', 'area'],
            'region'                => ['A', 'region'],
            'city'                  => ['A', 'city'],
            'city1'                 => ['A', 'city1'],
            'zipcode'               => ['A', 'zipcode'],
            'street'                => ['A', 'street'],
            'house'                 => ['A', 'house'],
            'building'              => ['A', 'building'],
            'structure'             => ['A', 'structure'],
            'apartment'             => ['A', 'apartment'],
            'comment'               => ['A', 'comment'],
            'post_box'              => ['A', 'post_box'],
            'is_onclaim'            => ['A', 'is_onclaim'],
            'person_name'           => ['P', 'name'],
            'company_name'          => ['C', 'name'],
            'name'                  => ['joins' => ['C','P'], 'column' => 'IF(O.subscriber_type_id=2, CONCAT(IFNULL(C.name,""), " (", IFNULL(P.name,"") , ")"), P.name)'],
            'email'                 => ['O', 'email'],
            'subscription_id'       => ['O', 'id'],
            'xpress_id'             => ['O', 'xpress_id'],
            'issue_title'           => ['I', 'issue_title'],
            'issue_id'              => ['I', 'Message_ID'],
            'sendtime'              => ['I', 'PublicDate'],
            'subscriber_type_id'    => ['O', 'subscriber_type_id'],
            'delivery_type_id'      => ['OI', 'delivery_type_id'],
            'order_type'            => ['O', 'type'],
            'quantity'              => ['OI', 'quantity'],
            'month'                 => ['I', 'release_month'],
            'release_month'         => ['I', 'release_month'],
            'order_id'              => ['OI', 'order_id'],
            'item_row_id'           => ['OI', 'item_id'],
            'periodical_id'         => ['I', 'MagazineID'],
            'payment_type_id'       => ['O', 'payment_type_id'],
            'payment_date'          => ['O', 'payment_date'],
            'full_map_address'      => ['columns' => ['country_id', 'area', 'region', 'city', 'city1', 'street', 'house', 'building', 'structure', 'apartment', 'comment', 'post_box', 'is_onclaim']]
        ];

        $map_date_check_sql = '( ( I.year = YEAR(CURDATE()) AND ( I.release_month = MONTH(CURDATE()) OR I.release_month = (MONTH(CURDATE()) + 1) ) ) OR ( I.year = (YEAR(CURDATE()) + 1) AND MONTH(CURDATE()) = 12 AND I.release_month = 1 ) )';
        $map_date_check_sql = '( IF(@osp_nx_map_close_day, ( II.shipment_id OR @osp_nx_map_close_day > DAY(CURDATE()) OR ( @osp_nx_map_close_day <= DAY(CURDATE()) AND !(' . $map_date_check_sql . ') ) ) , 1) )';
        $map_date_check_sql = '(' . $map_date_check_sql . ' AND ( ( I.year = YEAR(CURDATE()) AND I.release_month >= MONTH(CURDATE()) ) OR I.year > YEAR(CURDATE()) ) )';

        $channel_sql = [
            'map'           => '(' . $map_date_check_sql . ' AND OI.delivery_type_id IN ("1","7","10") AND O.type<>-1 AND O.subscriber_type_id=1 AND A2.country_id=165 AND IFNULL(A.is_enveloped,0)=0 )',
            'courier'       => '( OI.delivery_type_id IN ("3","4") AND (A2.country_id=165 OR A2.country_id=0 OR A2.country_id IS NULL) )',
            'envelope_post' => '( OI.delivery_type_id IN ("1","7","10") AND ( ( A2.country_id=0 OR A2.country_id IS NULL ) OR ( A2.country_id=165 AND (A.is_enveloped=1 OR O.subscriber_type_id=2 OR O.type=-1 OR !' . $map_date_check_sql . ') ) ) )',
            'envelope_dz'   => '( OI.delivery_type_id IN ("8","9","11") OR ( OI.delivery_type_id IN ("1","7","10") AND (A2.country_id IS NOT NULL) AND A2.country_id<>0 AND A2.country_id<>165 ) )',
            'office'        => '( OI.delivery_type_id IN ("2") )',
        ];

        foreach(self::$ship_channels as $channel_name => $channel_id) {
            if(!empty($channel_sql[$channel_name])) {
                $columns_map['count_' . $channel_name]                 = ['joins' => ['A','A2'], 'column' => 'SUM(IF(' . $channel_sql[$channel_name] . ',1,0))'];
                $columns_map['count_' . $channel_name . '_not_sended'] = ['joins' => ['A','A2'], 'column' => 'SUM(IF(' . $channel_sql[$channel_name] . ' AND II.shipment_id IS NULL,1,0))'];
                $columns_map['count_' . $channel_name . '_sended']     = ['joins' => ['SH'],     'column' => 'SUM(IF(SH.channel_id=' . $channel_id . ',1,0))'];
            }
        }
        $columns_map = $this->normalize_columns_map($columns_map);

        $joins = [];

        //process columns
        list($columns_names, $columns_db) = $this->get_columns2($params, $columns_map);

        if(in_array('count_map', $columns_names)) {
            $map_close_day = $this->subscriptionService->getMapCloseDay();
            $db->query('SET @osp_nx_map_close_day = ' . $map_close_day . ';', $db::QUERY_MODE_EXECUTE);
        }

        $select = $sql->select()
            ->from(['II'=>'nx_order_item_release'])
            ->join(['OI'=>'nx_order_item'], 'OI.id = II.item_id', ['order_id'])
            ->join(['S'=>'nx_subscription'], 'S.id = OI.item_id', [])
            ->join(['O'=>'nx_order'], 'O.id = OI.order_id', [])
            ->join(['I'=>'Issue'], 'I.Message_ID = II.release_id', [])
            ->where(['O.enabled' => 1])
            ->where('(O.order_state_id = 3 OR O.type <> 0)');

        if ($this->publisher_id) {
            $select
            ->join(['PR'=>'nx_price'], 'PR.id = O.price_id', [])
            ->where(['PR.publisher_id' => $this->publisher_id]);
        } else {
//            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }


        if (!empty($params['calc_found_rows'])) {
            $select->quantifier(new Expression('SQL_CALC_FOUND_ROWS'));
        }
        if (!empty($params['group'])) {
            $select->group($params['group']);
        } else {
            $select->group('II.id');
        }
        if (!empty($params['limit'])) {
            $select->limit((int)$params['limit']);
        }
        if (!empty($params['offset'])) {
            $select->offset((int)$params['offset']);
        }
        if (!empty($params['current'])) {
            $select->limit(1);
        }
        if (!empty($params['sorters'])) {
            foreach($params['sorters'] as $sorter) {
                if(empty($sorter['direction'])) {
                    continue;
                }
                switch($sorter['property']) {
                    case 'doc_result':
                    case 'last_request_date':
                        $select->order('II.'.$sorter['property'] . ' ' . $sorter['direction']);
                        break;
                }
            }
        } else {
            if (!empty($params['order'])) {
                $select->order($params['order']);
            } else {
                $select->order('I.year DESC, I.release_month DESC');
            }
        }

        //process filters = add where, add joins if needed, del loads
        $filters = $this->get_filters($params);
        $filter_channel_id = 0;
        if (!empty($filters)) {
            foreach ($filters as $filter) {
                if (empty($filter['value']) && in_array($filter['property'], ['shipment_date_start', 'shipment_date_end', 'payment_date_start', 'payment_date_end'])) {
                    continue;
                }
                switch ($filter['property']) {
                    case 'id':
                    case 'item_id':
                    case 'release_id':
                    case 'is_doc_wait':
                    case 'ship_count':
                        $select->where(['II.'.$filter['property'] => $filter['value']]);
                        break;
                    case 'shipment_id':
                        if ( $filter['value'] == null || strtolower($filter['value']) == 'null' ) {
                            $select->where->literal('II.' . $filter['property']. ' IS NULL');
                        } else {
                            $select->where(['II.'.$filter['property'] => $filter['value']]);
                        }
                        break;
                    case 'subscriber_type_id':
                    case 'action_id':
                    case 'order_state_id':
                    case 'payment_agent_id':
                    case 'xpress_id':
                    case 'payment_type_id':
                        $select->where(['O.'.$filter['property'] => $filter['value']]);
                        break;
                    case 'order_type':
                        if (!empty($filter['xoperator']) && $filter['xoperator']=='!=') {
                            $select->where->notEqualTo('O.type', $filter['value']);
                        } else {
                            $select->where->equalTo('O.type', $filter['value']);
                        }
                        break;
                    case 'is_ship':
                        if (!empty($filter['value'])) {
//                            $select->where->notEqualTo('II.shipment_id', 0);
                            $select->where('II.shipment_id IS NOT NULL');
                        } else {
//                            $select->where->equalTo('II.shipment_id', 0);
                            $select->where('II.shipment_id IS NULL');
                        }
                        break;
                    case 'is_shipped':
                        if (!empty($filter['value'])) {
                            $select->where->literal('SH.date <= NOW()');
                        } else {
//                            $select->where->literal('(II.shipment_id = 0 OR SH.date > NOW())');
                            $select->where->literal('(II.shipment_id IS NULL OR SH.date > NOW())');
                        }
                        break;
                    case 'is_enveloped':
                        if (empty($filter['value'])) {
                            $select->where('(A.is_enveloped = 0 OR A.is_enveloped IS NULL)');
                        } else {
                            $select->where->equalTo('A.'.$filter['property'], $filter['value']);
                        }
                        break;
                    case 'type':
                        $select->where->equalTo('S.'.$filter['property'], '' . $filter['value'] . '');
                        break;
                    case 'state':
                        $select->where(($filter['value'] ? 'I.PublicDate<>"0000-00-00 00:00:00" AND ' : '') . 'I.PublicDate ' . ($filter['value'] ? '<' : '>=') . 'NOW()');
                        break;
                    case 'email':
                        $select->where->like('O.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'name':
                        $select->where->NEST
                            ->like('P.'.$filter['property'], '%' . $filter['value'] . '%')->OR
                            ->like('C.'.$filter['property'], '%' . $filter['value'] . '%')->UNNEST;
                        break;
                    case 'subscription_id':
                        $select->where->like('O.id', '%' . $filter['value'] . '%');
                        break;
                    case 'sendtime':
                        $select->where->like('I.PublicDate', '%' . $filter['value'] . '%');
                        break;
                    case 'sendtime_start':
                        $select->where->greaterThanOrEqualTo('I.PublicDate', date('Y-m-d H:i:s', strtotime($filter['value'])));
                        break;
                    case 'sendtime_end':
                        $select->where->lessThanOrEqualTo('I.PublicDate', date('Y-m-d H:i:s', strtotime($filter['value'])));
                        break;
                    case 'periodical_id':
                        $select->where(['S.'.$filter['property'] => $filter['value']]);
                        break;
                    case 'periodical_name':
                        $select->where->like('M.EnglishName', '%' . $filter['value'] . '%');
                        break;
                    case 'issue_id':
                        $select->where->equalTo('II.release_id', $filter['value']);
                        break;
                    case 'issue_title':
                        $select->where->like('I.issue_title', '%' . $filter['value'] . '%');
                        break;
                    case 'issue_num':
                        $select->where->equalTo('I.Number', $filter['value']);
                        break;
                    case 'yearmonth':
                        $select->where->equalTo('I.year', substr($filter['value'],0,4));
                        $select->where->expression('LPAD(I.release_month, 2, "0") = ?', substr($filter['value'],4));
                        break;
                    case 'yearmonth_start':
                        $select->where->expression('CONCAT(I.year,LPAD(I.release_month, 2, "0")) >= ?', $filter['value']);
                        break;
                    case 'yearmonth_end':
                        $select->where->expression('CONCAT(I.year,LPAD(I.release_month, 2, "0")) <= ?', $filter['value']);
                        break;
                    case 'by_channel_id':
                        $filter_channel_id = (int)$filter['value'];
                        $columns_db = array_merge([
                            'is_enveloped'       => 'left',
                            'country_id'         => 'left'
                        ], $columns_db);
                        $columns_db = array_merge($columns_db, [
                            'shipment_id'        => 'inner',
                            'year'               => 'inner',
                            'month'              => 'inner',
                            'delivery_type_id'   => 'inner',
                            'subscriber_type_id' => 'inner',
                            'order_type'         => 'inner'
                        ]);
                        break;
                    case 'channel_id':
                        $select->where(['SH.channel_id' => (int)$filter['value']]);
                        break;
                    case 'month_start':
                        $select->where->greaterThanOrEqualTo('I.release_month', $filter['value']);
                        break;
                    case 'month_end':
                        $select->where->lessThanOrEqualTo('I.release_month', $filter['value']);
                        break;
                    case 'year':
                        $select->where->equalTo('I.year', $filter['value']);
                        break;
                    case 'month':
                        $select->where(['I.release_month' => $filter['value']]);
                        break;
                    case 'order_id':
                        if (!empty($filter['xoperator']) && $filter['xoperator']=='!=') {
                            $select->where->addPredicate(new \Zend\Db\Sql\Predicate\NotIn('O.id', $filter['value']));
                        } else {
                            $select->where(['O.id' => $filter['value']]);
                        }
                        break;
                    case 'doc_result':
                        if (!empty($filter['xoperator']) && $filter['xoperator']=='!=') {
                            $select->where->notEqualTo('II.'.$filter['property'], $filter['value']);
                        } else {
                            if(empty($filter['value'])) {
                                $select->where->equalTo('II.'.$filter['property'], $filter['value']);
                            } else {
                                $select->where->like('II.'.$filter['property'], '%' . $filter['value'] . '%');
                            }
                        }
                        break;
                    case 'last_request_date':
                        $select->where->like('II.'.$filter['property'], $filter['value'] . '%');
                        break;
                    case 'country_id':
                        $select->where(['A2.'.$filter['property'] => $filter['value']]);
                        break;
                    case 'delivery_type_id':
                        $select->where(['OI.'.$filter['property'] => $filter['value']]);
                        break;
                    case 'shipdate':
                         $select->where->like('SH.date', $filter['value'] . '%');
                        break;
                    case 'shipment_date_start':
                        $select->where->greaterThanOrEqualTo('SH.date', $filter['value']);
                        break;
                    case 'shipment_date_end':
                        $select->where->lessThanOrEqualTo('SH.date', $filter['value']);
                        break;
                    case 'payment_date_start':
                        $select->where->greaterThanOrEqualTo('O.payment_date', $filter['value']);
                        break;
                    case 'payment_date_end':
                        $select->where->lessThanOrEqualTo('O.payment_date', $filter['value']);
                        break;
                    case 'order_tags':
                        if(empty($filter['value'])) {
                            $joins['TE'] = 'inner';
                            $select->where->NEST->notEqualTo('TE.entity_id', \nx\Model\TagEntity::ENTITY_ORDER)->OR->isNull('TE.id')->UNNEST;
                        } else {
                            if (!empty($filter['xoperator']) && $filter['xoperator']=='OR') {
                                $joins['TE'] = 'inner';
                                $select->where(['TE.entity_id' => \nx\Model\TagEntity::ENTITY_ORDER])->where(['TE.tag_id' => $filter['value']]);
                            } else {
                                foreach($filter['value'] as $i => $tag_id) {
                                    $select->join(['TE'.$i => 'nx_tag_entity'], 'TE'.$i.'.row_id = O.id', []);
                                    $select->where(['TE'.$i.'.entity_id' => \nx\Model\TagEntity::ENTITY_ORDER])->where(['TE'.$i.'.tag_id' => $tag_id]);
                                }
                            }
                        }
                        break;
                }
            }
        }

        //process columns
        list($columns, $joins_by_cols) = $this->process_columns2($columns_db, $columns_map);
        $joins = array_merge($joins, $joins_by_cols);

        $select->columns($columns);

        //process joins
        foreach($joins as $key => $join_type) {
            if(!empty($join_map[$key][2]) && !empty($join_type)) {
                $joins[$join_map[$key][2]] = $join_type;
            }
        }
        foreach($join_map as $key => $table) {
            if(!empty($joins[$key])) {
                $select->join([$key=>$table[0]], $table[1], [], $joins[$key]);
            }
        }

        $selectString = $sql->getSqlStringForSqlObject($select);//var_dump($selectString);//die;
        if(!empty($params['debug'])) {
            var_dump($selectString);
        }
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        if(!empty($params['calc_found_rows'])) {
            $count = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();
        }

        $items = $result->toArray();

        if($filter_channel_id) {
            $filtered = [];
            foreach($items as $item) {
                if($this->checkChannel($filter_channel_id, $item)) {
                    $filtered[] = $item;
                }
            }
            $items = $filtered;
        }

        //process loads

        //process custom entity fields
        if(array_search('full_map_address', $columns_names)!==false) {
            foreach($items as $i=>$item) {
                $address_fields = ['area', 'region', 'city', 'city1', 'street', 'house', 'building', 'structure', 'apartment', 'comment'];
                $fields_prefix  = ['house' => 'д.', 'building' => 'корп.', 'structure' => 'стр.', 'apartment' => 'кв.'];
                $item['comment'] = preg_replace('#[\t\v\r\n\f;]#ius',' ',$item['comment']);
                if(!empty($item['country_id']) && $item['country_id'] != \nx\Model\Address::COUNTRY_ID_RUSSIA) {
                    array_unshift($address_fields, 'country_name');
                }
                if(!empty($item['post_box']) || !empty($item['is_onclaim'])) {
                    $item['street'] = ($item['post_box'] ? 'а/я ' . $item['post_box'] : ($item['is_onclaim'] ? 'ДО ВОСТРЕБОВАНИЯ' : ''));
                    $item['house'] = $item['building'] = $item['structure'] = $item['apartment'] = '';
                }
                $items[$i]['full_map_address'] = '';
                foreach($address_fields as $field) {
                    $items[$i]['full_map_address'] .= (!empty($item[$field]) ? (!empty($fields_prefix[$field]) ? $fields_prefix[$field] . ' ' : '') . $item[$field] . ', ' : '');
                }
                $items[$i]['full_map_address'] = mb_substr($items[$i]['full_map_address'], 0, -2);
            }
        }

        foreach($items as $i=>$item) {//clear unwanted columns
            foreach($item as $col) {
                if(!in_array($col, $columns_names)) {
                    unset($items[$i][$col]);
                }
            }
        }

        $items = !empty($params['current']) ? $items[0] : $items;

        $result = !empty($params['calc_found_rows']) ? ['items' => $items, 'total' => $count['count']] : $items;

        return $result;
    }

    public function getShippingsForTable(array $params)
    {
        $items = $this->getShippingsBy(array_merge($params, [
                'order_state_id' => 3,
                'group'          => ['M.EnglishName', 'I.year', 'I.release_month', 'I.Number'],
                'columns'        => ['state','issue_num','periodical_name','year','release_month','count_pdf','count_paper','sum_total_price']
            ]
        ));
        return $items;
    }

    public function getShippingsForMap($params = [])
    {
        $filters = [['property' => 'order_type', 'xoperator' => '!=', 'value' => '-1']];
        if(!empty($params['not_order_id'])) {
            $filters[] = ['property' => 'order_id', 'xoperator' => '!=', 'value' => $params['not_order_id']];
        }
        $by = [
            'delivery_type_id'   => [1,7,10],
            'subscriber_type_id' => 1,
            'country_id'         => 165,
            'is_enveloped'       => 0,
            'by_channel_id'      => 1,
            'year'               => $params['year'],
            'periodical_id'      => $params['periodical_id'],
            'month_start'        => $params['month_start'],
            'month_end'          => $params['month_end'],
            'columns'            => [
                'order_id','item_row_id','quantity','release_month','year','periodical_id','person_name', 'id', 'shipment_id', 'release_id', 'item_id'
            ],
            'columns_left' => ['zipcode','region','area','city','city1','street','house','building','structure','apartment','post_box','is_onclaim'],
            'filters'      => $filters,
            'order'        => 'I.year DESC, P.name, I.release_month DESC'
        ];
        if(isset($params['shipment_id'])) {
            $by['shipment_id'] = (int)$params['shipment_id'];
        }
        $items = $this->getShippingsBy($by);
        return $items;
    }

    public function getShippingsMapList(array $params)
    {
        $shippings = $this->getShippingsForMap($params);
        $issues    = $this->getIssues(['isset_release_month' => 1, 'periodical_id' => $params['periodical_id'], 'year' => $params['year']]);
        $counts    = [];
        for($j=1;$j<=12;$j++) {
            $counts[$j] = 0;
        }
        if(!empty($issues)) {
            foreach($issues as $issue) {
                $counts[$issue['release_month']]++;
            }
        }

        $items = [];
        $duplicateCriterialsList = [];

        foreach ($shippings as $shipping) {
            if (!isset($items[$shipping['periodical_id'] . '_' . $shipping['year'] . '_' . $shipping['order_id']])) {
                $init = [];
                $init = [
                    'ship_count'      => 0,
                    'is_ship'         => 0,
                    'is_partial_ship' => 0,
                    'is_partial'      => 0,
                    'periodical_id'   => $shipping['periodical_id'],
                    //'periodical_name' => $shipping['periodical_name'],
                    //'year'            => $shipping['year'],
                    'subscription_id' => $shipping['order_id'],
                    'sumqty'          => 0,//$shipping['quantity'],
                    'postcode'        => $shipping['zipcode'],
                    'region'          => $shipping['area'],
                    'area'            => $shipping['region'],
                    'city'            => $shipping['city'],
                    'city1'           => $shipping['city1']//,
                    //'street'          => $shipping['street'] . ($shipping['post_box'] ? ($shipping['street'] ? ', ' : '') . 'а/я ' . $shipping['post_box'] : '') . ($shipping['is_onclaim'] ? ($shipping['street'] || $shipping['post_box'] ? ', ' : '') . 'ДО ВОСТРЕБОВАНИЯ' : ''),
                    //'dom'             => $shipping['house'],
                    //'korp'            => $shipping['building'] . ($shipping['building'] && $shipping['structure'] ? ' стр.' : '') . $shipping['structure'],
                    //'kv'              => $shipping['apartment']
                ];


                if ((mb_strpos($shipping['area'], 'Москва') !== false || mb_strpos($shipping['area'], 'Санкт-Петербург') !== false)) {
//                if ($shipping['area'] == 'Москва г' ||$shipping['area'] == 'Санкт-Петербург г' ) {
                    $init['city'] = $shipping['area'];
                }

                if ( empty($shipping['area']) && (mb_strpos($shipping['city'], 'Москва') !==  false || mb_strpos($shipping['city'], 'Санкт-Петербург г') !== false ) ) {
                    $init['area'] = $shipping['city'];
                }

//                if ( empty($shipping['area']) && ($shipping['city'] == 'Москва г' || $shipping['city'] == 'Санкт-Петербург г') ) {
//                    $init['area'] = $shipping['city'];
//                }

                if (empty($shipping['post_box']) && empty($shipping['is_onclaim'])) {
                    $init = array_merge($init, [
                        'street'   => $shipping['street'],
                        'dom'      => $shipping['house'],
                        'korp'     => $shipping['building'] . ($shipping['building'] && $shipping['structure'] ? ' стр.' : '') . $shipping['structure'],
                        'str'      => $shipping['structure'],
                        'kv'       => $shipping['apartment'],
                        'is_post_box' => false,
                        'is_on_claim' => false,
                    ]);
                } else {
                    $init = array_merge($init, [
                        'street'   => ($shipping['post_box'] ? 'а/я ' . $shipping['post_box'] : ($shipping['is_onclaim'] ? 'ДО ВОСТРЕБОВАНИЯ' : '')),
                        'dom'      => '',
                        'korp'     => '',
                        'kv'       => '',
                        'str'      => '',
                        'is_post_box' => $shipping['post_box'] ? true : false,
                        'is_on_claim' => $shipping['is_onclaim'] ? true : false,
                    ]);
                }

                $insteadOfSpace = '%%%';
                $fullName = preg_replace("/\s+/", $insteadOfSpace, $shipping['person_name']);
                $parsedName = explode($insteadOfSpace, $fullName);

                $init['fio'] = $shipping['person_name'];

                $surname = (count($parsedName) > 0 && !empty($parsedName[0])) ? $parsedName[0] : '';
                $personName = (count($parsedName) > 1 && !empty($parsedName[1])) ? $parsedName[1] : '';
                $patronymic = (count($parsedName) > 2 && !empty($parsedName[2])) ? $parsedName[2] : '';

                $init['surname'] = $surname;
                $init['name'] = $personName;
                $init['patronymic'] = $patronymic;

                $duplicateCriteria = $init['fio'];

                if ( !array_key_exists($duplicateCriteria, $duplicateCriterialsList) ) {
                    $duplicateCriterialsList[$duplicateCriteria] =  1;
                } else {
                    $duplicateCriterialsList[$duplicateCriteria]++;
                }

                for ( $j=1; $j<=12; $j++) {
                    $init['qty' . $j] = [];
                }
                $init['qty' . $shipping['release_month']] = [$shipping['item_id'] => [$shipping['quantity']]];
                if ($shipping['shipment_id']) {
                    $init['ship_count'] = 1;
                }
                $items[$shipping['periodical_id'] . '_' . $shipping['year'] . '_' . $shipping['order_id']] = $init;
            } else {
                if ($shipping['shipment_id']) {
                    $items[$shipping['periodical_id'] . '_' . $shipping['year'] . '_' . $shipping['order_id']]['ship_count']++;
                }
                if (!in_array($shipping['item_id'],array_keys($items[$shipping['periodical_id'] . '_' . $shipping['year'] . '_' . $shipping['order_id']]['qty' . $shipping['release_month']]))) {//проверим когда в заказе несколько позиций на один и тот же журнал
                    $items[$shipping['periodical_id'] . '_' . $shipping['year'] . '_' . $shipping['order_id']]['qty' . $shipping['release_month']][$shipping['item_id']] = [$shipping['quantity']];
                    //$items[$shipping['periodical_id'] . '_' . $shipping['year'] . '_' . $shipping['order_id']]['sumqty']+=$shipping['quantity'];
                } else {//проверим когда заказ не на полный месячный комплект
                    $items[$shipping['periodical_id'] . '_' . $shipping['year'] . '_' . $shipping['order_id']]['qty' . $shipping['release_month']][$shipping['item_id']][] = $shipping['quantity'];
                }
            }
        }
        $result = [];
        foreach($items as $i => $item) {
            $ship_plan_count = 0;
            for($j=1;$j<=12;$j++) {
                $count = 0;
                foreach($item['qty' . $j] as $key => $quantity) {
                    $ship_plan_count+=$counts[$j];
                    if(count($quantity) == $counts[$j]) {
                        $count += $quantity[0];
                    } else {
                        $item['is_partial'] = 1;
                    }
                }
                $item['qty' . $j] = $count;
                $item['sumqty'] += $count;
            }

            if ($item['is_post_box']) {
                $item['deltype'] = 3;
            } elseif ($item['is_on_claim']) {
                $item['deltype'] = 2;
            } else {
                $item['deltype'] = 1;
            }

            $duplicateCriteria = $item['fio'];

            if (array_key_exists($duplicateCriteria, $duplicateCriterialsList) && $duplicateCriterialsList[$duplicateCriteria] > 1) {
                $item['has_duplicate'] = 1;
            } else {
                $item['has_duplicate'] = 0;
            }

            if ($item['ship_count']) {
                $item['is_ship'] = 1;
                if($item['ship_count'] != $ship_plan_count) {
                    $item['is_partial_ship'] = 1;
                }
            }
            if(isset($params['is_ship'])) {
                if(!empty($params['is_ship']) && (empty($item['is_ship']) || !empty($item['is_partial_ship']))) {
                    continue;
                }
                if(empty($params['is_ship']) && !empty($item['is_ship']) && empty($item['is_partial_ship'])) {
                    continue;
                }
            }
            if(isset($params['is_partial'])) {
                if(!empty($params['is_partial']) && empty($item['is_partial'])) {
                    continue;
                }
                if(empty($params['is_partial']) && !empty($item['is_partial'])) {
                    continue;
                }
            }
            $result[] = $item;
        }

        return $result;
    }

    public function getShippingsCounts($params = [])
    {
        $by = [
            'columns'      => [
                'count', 'count_paper_full', 'count_pdf', 'count_paper', 'issue_id', 'count_post', 'periodical_id', 'periodical_name', 'issue_title', 'month', 'year', 'issue_num',
                'sum_payed_price', 'sum_payed_paper_price', 'sum_payed_pdf_price'
            ],
            //'columns_left' => array('by_channel_id'),
            'filters'      => $params['filters'],
            'order'        => 'I.year DESC, I.MagazineID, I.release_month DESC',
            'group'        => 'II.release_id'
        ];
        foreach(self::$ship_channels as $channel_name => $channel_id) {
            $by['columns_left'][] = 'count_' . $channel_name;
            $by['columns_left'][] = 'count_' . $channel_name . '_sended';
            $by['columns_left'][] = 'count_' . $channel_name . '_not_sended';
        }
        $items = $this->getShippingsBy($by);
        $results = [];
        if(!empty($items)) {
            foreach($items as $item) {
                $issue_ids[] = $item['issue_id'];
            }
            $pdflog = $this->subscriptionPdfLogModel->getPdfLogByIssue(['filters' => [
                ['property' => 'issue_id', 'value' => array_unique($issue_ids)]
            ]]);
            $pdf_log_counts = [];
            if(!empty($pdflog)) {
                foreach($pdflog as $row) {
                    $pdf_log_counts[$row['issue_id']] = $row['queue'];
                }
            }
            foreach($items as $item) {
                $item['id']                = $item['issue_id'];
                $item['count_price']       = $item['sum_payed_price'];
                $item['count_paper_price'] = $item['sum_payed_paper_price'];
                $item['count_pdf_price']   = $item['sum_payed_pdf_price'];
                $item['count_pdf_log']     = !empty($pdf_log_counts[$item['issue_id']]) ? $pdf_log_counts[$item['issue_id']] : 0;
                $results[] = $item;
            }
        }
        return $results;
    }

    public function shipByMap(array $params)
    {
        $maplist = $this->getShippingsMapList(array_merge($params, ['is_partial' => 1]));
        $ids     = [];
        if(!empty($maplist)) {
            foreach($maplist as $item) {
                $ids[] = $item['subscription_id'];
            }
        }
        $shippings = $this->getShippingsForMap(array_merge($params, ['shipment_id' => 'NULL', 'not_order_id' => $ids]));//select not shipped only
//        $shippings = $this->getShippingsForMap(array_merge($params, array('shipment_id' => 0, 'not_order_id' => $ids)));//select not shipped only

//        $shippings = $this->getShippingsList(array_merge($params, array('shipment_id' => 0, 'not_order_id' => $ids)));//select not shipped only

        if(empty($shippings)) {
            return false;
        }

        $ids = [];
        foreach($shippings as $shipping) {
            $ids[] = $shipping['id'];
        }

        $shipment_id = $this->addShipment([
            'shipmentData' => [
                'channel_id'     => 1,
                'create_user_id' => $params['user_id'],
            ],
            'shipData' => [
                'ids' => $ids
            ]
        ]);
        return $shipment_id;
    }

    public function shipByShippings(array $params)
    {
        if(empty($params['channel_id'])) {
            return false;
        }
        $shippings = [];
        if(!empty($params['ids'])) {
            //$shippings = $this->getShippings(array('ids' => $params['ids'], 'shipment_id' => 0));
            $shippings = $this->getShippingsBy(['id' => $params['ids'], 'shipment_id' => 'NULL']);
        }
        //else if(!empty($params['filters'])) {
        //    $shippings = $this->getShippingsList(array('filters' => $params['filters'], 'shipment_id' => 0));
        //}
        if(empty($shippings))
        {
            return false;
        }
        $ids = [];
        foreach($shippings as $shipping)
        {
            $ids[] = $shipping['id'];
        }
        $shipment_id = $this->addShipment([
            'shipmentData' => [
                'channel_id'     => $params['channel_id'],
                'date'           => $params['date'],
                'create_user_id' => $params['user_id'],
            ],
            'shipData' => [
                'ids' => $ids
            ]
        ]);
        return $shipment_id;
    }

    public function addShipment($data)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);

        if(empty($data['shipmentData']['channel_id']))
        {
            return false;
        }

        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            $data['shipmentData']['created'] = date('Y-m-d H:i:s');
            if(empty($data['shipmentData']['date'])) {
                $data['shipmentData']['date'] = $data['shipmentData']['created'];
            }

            $query = $sql->insert('nx_shipment')->values($data['shipmentData']);
            $queryString = $sql->getSqlStringForSqlObject($query);
            $db->query($queryString, $db::QUERY_MODE_EXECUTE);

            $shipment_id = $db->getDriver()->getLastGeneratedValue();

            $this->update(['shipment_id' => $shipment_id, 'ship_count' => new Expression('ship_count + 1')], ['id' => $data['shipData']['ids']]);

            foreach($data['shipData']['ids'] as $item_id)
            {
                $values = ['shipment_id' => $shipment_id, 'item_type' => \nx\Model\Shipment::$item_type['nx_order_item_release'], 'item_id' => $item_id];
                $query = $sql->insert('nx_shipment_item')->values($values);
                $queryString = $sql->getSqlStringForSqlObject($query);
                $db->query($queryString, $db::QUERY_MODE_EXECUTE);
            }

            $report_data = $this->getShipmentCsvData($data['shipData']['ids']);
            $this->adapter->isTransactional = 0;
            $this->fileStorageModel->putContents($report_data, ['fileStorageData' => [
                'entity_type'    => 'shipment',
                'entity_id'      => $shipment_id,
                'create_user_id' => !empty($data['shipmentData']['create_user_id']) ? $data['shipmentData']['create_user_id'] : 0,
                'name'           => 'отгрузки_' . $shipment_id . '.csv',
                'path'           => 'data/shipment/' . $shipment_id . '/data.csv',
                'type'           => \nx\Model\FileStorage::TYPE_SHIPMENT_DATA,
            ]]);
            $this->adapter->isTransactional = 1;
            if(!empty($data['shipmentData']['create_user_id'])) {
                $this->logger->log('Добавление отгрузки #' . $shipment_id, 'add_shipment', $data, !empty($data['shipmentData']['create_user_id']) ? $data['shipmentData']['create_user_id'] : 0);
            }
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $shipment_id;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }

    public function getShipChannels()
    {
        $db           = $this->adapter;
        $sql          = new Sql($db);
        $select       = $sql->select()->from('nx_ship_channel')->order('id');
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $result       = $result->toArray();
        return $result;
    }

    public function cancelShip(array $params)
    {
        /** @var Adapter $db */
        $db  = $this->adapter;
        $sql = new Sql($db);
        $isTransactional = $this->isTransactional();
        if ($isTransactional) {
            $db->getDriver()->getConnection()->beginTransaction();
        }
        try {
            $shippings = $this->getShippingsBy(['id' => $params['ids'], 'columns' => ['id', 'shipment_id']]);
            $shipIdsToCheck = [];
            foreach ($shippings as $shipping) {
                $delQuery = $sql->delete()
                    ->from('nx_shipment_item')
                    ->where([
                        'item_type'   => \nx\Model\Shipment::$item_type['nx_order_item_release'],
                        'item_id'     => $shipping['id'],
                        'shipment_id' => $shipping['shipment_id']
                    ]);

                $delQueryString = $sql->buildSqlString($delQuery);
                $isDel = $db->query($delQueryString)->execute();
                if ($isDel->count() > 0) {
                    $shipIdsToCheck[] = intval($shipping['shipment_id']);
                }
                $update = $sql->update()->table('nx_order_item_release')
                    ->set(['shipment_id' => NULL])
                    ->where(['id' => intval($shipping['id'])]);

                $queryStr = $sql->buildSqlString($update);
                $updateResult = $db->query($queryStr)->execute();
            }

//            $this->update(['shipment_id' => 0], ['id' => $params['ids']]);

            $this->logger->log('Отмена отгрузки', 'cancel_shipping', $params, !empty($params['user_id']) ? $params['user_id'] : 0);
            if ($isTransactional) {
                $db->getDriver()->getConnection()->commit();
            }

            // возможно после удаления из отгрузки позиции, в ней (в отгрузке) ничего не осталось.
            // Тогда ее тоже нужно удалить т.к. не осталось ссылок на нее.
            foreach ($shipIdsToCheck as $shipment_id) {
                $queryStr = "SELECT count(id) AS found FROM nx_shipment_item WHERE shipment_id = " . $shipment_id;
                $r = $db->query($queryStr, $db::QUERY_MODE_EXECUTE)->toArray();
                $r = intval($r[0]['found']) ?? 0;
                if ($r > 0) {
                    continue;
                }
                $queryStr = "DELETE FROM nx_shipment WHERE id = " . $shipment_id;
                $d = $db->query($queryStr)->execute();
            }

            return true;
        } catch(\Exception $E) {
            if ($isTransactional) {
                $db->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }

    public function checkChannel($channel_id, $item, $params = [])
    {
        $map_close_day  = isset($params['map_close_day']) ? $params['map_close_day'] : $this->subscriptionService->getMapCloseDay();
        $map_date_check = ($item['year'] == date('Y') && ( $item['month']==date('n') || $item['month']==(date('n')+1) )) || ($item['year']==(date('Y')+1) && date('n')==12 && $item['month']==1);
        $map_date_check = $map_close_day ? $item['shipment_id'] || date('j')<$map_close_day || (date('j')>=$map_close_day && !$map_date_check) : true;
        //$map_date_check = $map_close_day ? $item['shipment_id'] || date('j')<$map_close_day : true;
        $map_date_check = $map_date_check && (($item['year'] == date('Y') && $item['month'] >= date('n')) || $item['year'] > date('Y'));//auto close prev monthes
        switch($channel_id)
        {
            case 1:
                if($map_date_check && in_array($item['delivery_type_id'], [1,7,10]) && $item['order_type']!=-1 && $item['subscriber_type_id']==1 && $item['country_id']==\nx\Model\Address::COUNTRY_ID_RUSSIA && empty($item['is_enveloped']))
                {
                    return true;
                }
                break;
            case 2:
                if(in_array($item['delivery_type_id'], [3,4]) && ($item['country_id']==\nx\Model\Address::COUNTRY_ID_RUSSIA || empty($item['country_id'])))
                {
                    return true;
                }
                break;
            case 3:
                if(in_array($item['delivery_type_id'], [1,7,10]) && (empty($item['country_id']) || ($item['country_id']==\nx\Model\Address::COUNTRY_ID_RUSSIA && (!empty($item['is_enveloped']) || $item['subscriber_type_id']==2 || $item['order_type']==-1 || !$map_date_check))))
                //if(in_array($item['delivery_type_id'], array(1,7,10)) && ($item['country_id']==\nx\Model\Address::COUNTRY_ID_RUSSIA || empty($item['country_id'])) && (!empty($item['is_enveloped']) || $item['subscriber_type_id']==2 || $item['order_type']==-1 || !$map_date_check))
                {
                    return true;
                }
                break;
            case 4:
                //if((!empty($item['is_enveloped']) || $item['subscriber_type_id']==2) && in_array($item['delivery_type_id'], array(8,9,11)))
                if(in_array($item['delivery_type_id'], [8,9,11]) || (in_array($item['delivery_type_id'], [1,7,10]) && !empty($item['country_id']) && $item['country_id']!=\nx\Model\Address::COUNTRY_ID_RUSSIA))
                {
                    return true;
                }
                break;
            case 7:
                if(in_array($item['delivery_type_id'], [2]))
                {
                    return true;
                }
                break;
            /*case 5:
                if(!empty($item['is_enveloped']) && ($item['subscriber_type_id']==2))
                {
                    return true;
                }
                break;*/
        }
        return false;
    }

    public function getStickersData(array $params)
    {
        $delivery_types = $this->subscriptionService->mapper->getDeliveryTypes();
        //$shippings = $this->getShippingsList(array('ids' => $params['ids']));
        $shippings = $this->getShippingsBy(['id' => $params['ids'],
            'columns' => [
                'periodical_name',
                'issue_title',
                'quantity',
                'delivery_type_id',
                'subscription_id',
                'subscriber_type_id',
                'payment_agent_name',
                'payment_type_id'
            ],
            'columns_left' => ['zipcode', 'full_map_address', 'name', 'phone']
        ]);
        if(empty($shippings)) {
            return '';
        }
        $data = [];
        foreach($shippings as $shipping)
        {
            $delivery_type_name = '';
            foreach($delivery_types as $type)
            {
                if($type['id'] == $shipping['delivery_type_id']) {
                    $delivery_type_name = $type['name'];
                }
            }
            $data[] = [
                'periodical_name'      => $shipping['periodical_name'],
                'issue_title'          => $shipping['issue_title'],
                'zipcode'              => $shipping['zipcode'],
                'address'              => $shipping['full_map_address'],
                'name'                 => $shipping['name'],
                'phone'                => $shipping['phone'],
                'quantity'             => $shipping['quantity'],
                'delivery_type_name'   => $delivery_type_name,
                'order_id'             => $shipping['subscription_id'],
                'payment_type_id'      => $shipping['payment_type_id'],
                'payment_agent_name'   => $shipping['payment_agent_name'],
                'subscriber_type_mark' => $shipping['subscriber_type_id'] == 2 ? '*' : ''
            ];
        }
        return $data;
    }

    public function getShipmentCsvData($ids)
    {
        $fields = ['id', 'order_id', 'periodical_name', 'year', 'issue_num', 'name', 'email', 'zipcode', 'full_map_address'];
        //$shippings = $this->getShippingsList(array('ids' => $ids));
        $shippings = $this->getShippingsBy(['id' => $ids,
            'columns' => [
                'id',
                'order_id',
                'periodical_name',
                'year',
                'issue_num',
                'email',
            ],
            'columns_left' => ['name', 'zipcode', 'full_map_address']
        ]);
        if(empty($shippings)) {
            return '';
        }
        $content = '';
        ob_start();
        if(!empty($shippings))
        {
            foreach($shippings as $shipping)
            {
                $row = '';
                foreach($fields as $field) {
                    foreach($shipping as $key=>$value) {
                        if($key==$field) {
                            $row .= $value . ';';
                        }
                    }
                }
                $row = mb_substr($row,0,-1);
                echo $row . "\n";
            }
        }
        $content = ob_get_clean();
        return $content;
    }

    public function getIssues($data)
    {
        $db      = $this->adapter;
        $columns = [
            'id'           => 'Message_ID',
            'release_date' => 'PublicDate',
            'release_month',
            'name'         => new Expression('CONCAT(M.EnglishName, " ", I.year, "-", LPAD(I.release_month, 2, "0"), " ", I.issue_title)'),
            'year'
        ];
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['I'=>'Issue'])
            ->join(['M'=>'Edition'], 'M.Message_ID = I.MagazineID', [])
            ->order(!empty($data['order_by']) ? $data['order_by'] : ['I.year', 'I.Number'])
            ->group(new Expression('CONCAT(I.MagazineID,I.year,I.Number)'));

        if ($this->publisher_id) {
            $select
            ->join(['PE'=>'publisher_editions'], 'PE.magazine_id = M.Message_ID', [])
            ->where(['PE.publisher_id' => $this->publisher_id]);
        } else {
            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }

        if(!empty($data['item_id']))
        {
            $select
                ->join(['II' => 'nx_order_item_release'], 'II.release_id = I.Message_ID', [])
                ->where(['II.item_id' => $data['item_id']]);
        }
        if(!empty($data['periodical_id']))
        {
            $select->where(['I.MagazineID' => $data['periodical_id']]);
        }
        if(!empty($data['byear']))
        {
            $select->where->greaterThanOrEqualTo('I.year', $data['byear']);
        }
        if(!empty($data['year']))
        {
            $select->where(['I.year' => $data['year']]);
        }
        if(!empty($data['isset_release_month']))
        {
            $select->where->notEqualTo('I.release_month', 0);
        }
        $select->columns($columns);
        if(!empty($data['periodical_id']) && !empty($data['byear']) && !empty($data['bnum']) && !empty($data['rqty']))
        {
            $db->query('SET @osp_row_number = 0;', Adapter::QUERY_MODE_EXECUTE);
            $select
                ->where(['I.year >= ?' => $data['byear']])
                ->where(['I.MagazineID' => $data['periodical_id']]);
                //->group(new Expression('CONCAT(I.year,I.Number)'));
            $select = $sql->select()
                ->from(['I' => $select])
                ->columns([
                        'id',
                        'year',
                        'name',
                        'release_date',
                        'release_month',
                        'osp_row_number' => new Expression('@osp_row_number := @osp_row_number + 1')
                    ]
                )
                ->having(['CAST(CONCAT(I.year, osp_row_number) AS UNSIGNED) >= ?' => $data['byear'] . $data['bnum']])
                ->order(['osp_row_number ASC'])
                ->limit($data['rqty']);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $items = $result->toArray();

        if(!empty($items) && !empty($data['ship_item_id'])) {
            $release_ids = [];
            foreach($items as $item) {
                $release_ids[] = $item['id'];
            }
            $shippings = $this->getShippingsBy(['item_id' => $data['ship_item_id'], 'release_id' => $release_ids,
                'columns'      => ['id', 'release_id', 'ship_count', 'shipment_id'],
                'columns_left' => ['channel_name', 'shipment_date']
            ]);
            if(!empty($shippings)) {
                $ship_by_ids = [];
                foreach($shippings as $shipping) {
                    $ship_by_ids[$shipping['release_id']] = $shipping;
                }
                foreach($items as $i=>$item) {
                    if(!empty($ship_by_ids[$item['id']])) {
                        $items[$i]['shipping_id']   = $ship_by_ids[$item['id']]['id'];
                        //$items[$i]['shipment_info'] = $ship_by_ids[$item['id']]['channel_name'] . ', ' . $ship_by_ids[$item['id']]['shipment_date'];
                        $items[$i]['channel_name']  = $ship_by_ids[$item['id']]['channel_name'];
                        $items[$i]['shipment_date'] = $ship_by_ids[$item['id']]['shipment_date'];
                        $items[$i]['ship_count']    = $ship_by_ids[$item['id']]['ship_count'];
                        $items[$i]['shipment_id']   = $ship_by_ids[$item['id']]['shipment_id'];
                    }
                }
            }
        }

        return $items;
    }

    public function updateItemsIssues($order_id)
    {
        if(empty($order_id)) {
            throw new \Exception('Отсутствуют входные данные.');
        }

        $db           = $this->adapter;
        $sql          = new Sql($db);
        $select       = $sql->select()->from(['O'=>'nx_order'])->where(['O.id' => $order_id]);

//        if ($this->publisher_id) {
//            $select
//            ->join(array('PR'=>'nx_price'), 'PR.id = O.price_id', array())
//            ->where(array('PR.publisher_id' => $this->publisher_id));
//        } else {
//            $select->where('0'); //не выводить ничего если не знаем какой издатель
//        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $subscription = $result->current();

        if(empty($subscription)) {
            throw new \Exception('Заказ ' . $order_id . ' не найден или принадлежит другому издателю' );
        }

        $select       = $sql->select()
            ->from(['I'=>'nx_order_item'])->columns(['id', 'item_id'])
            ->join(['S'=>'nx_subscription'], 'S.id = I.item_id', ['periodical_id', 'byear', 'bnum', 'rqty', 'date_start', 'date_end', 'period'])
            ->where(['I.order_id' => $subscription['id']])
            ->where(['I.item_type' => 'nx_subscription']);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $items        = $result->toArray();

        if(empty($items)) {
            //throw new \Exception('Для подписки в nx с id = ' . $order_id . ' отсутствуют позиции.');
            return false;
        }

        $items_ids = array_map(create_function('$u', 'return (int)$u["id"];'), $items);

        $select       = $sql->select()->from(['I'=>'nx_order_item_release'])->where(['I.item_id' => $items_ids]);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $items_issues = $result->toArray();

        $items_issues_ids = [];
        if(!empty($items_issues)) {
            foreach($items_issues as $item_issue) {
                if(empty($items_issues_ids[$item_issue['item_id']])) {
                    $items_issues_ids[$item_issue['item_id']] = [$item_issue['release_id']];
                } else {
                    $items_issues_ids[$item_issue['item_id']][] = $item_issue['release_id'];
                }
            }
        }

        foreach($items as $item) {
            if(empty($item['byear']) || empty($item['bnum']) || empty($item['rqty'])) {
                //throw new \Exception('Для подписки в nx с id = ' . $order_id . ' отсутствуют данные у позиции.');
                continue;
            }

            $db->query('SET @osp_row_number = 0;', Adapter::QUERY_MODE_EXECUTE);
            $sql    = new Sql($db);
            $select = $sql->select()
                ->from(['I' => 'Issue'])
                ->columns([
                        'id'           => 'Message_ID',
                        'year',
                        'num'          => 'Number',
                        'release_date' => 'PublicDate',
                        'release_month',
                        //'osp_row_number' => new Expression('@osp_row_number := @osp_row_number + 1')
                    ]
                )
                //->join('(SELECT @row := 0) AS R', '1=1')
                ->where(['I.year >= ?' => $item['byear']])
                ->where(['I.MagazineID' => $item['periodical_id']])
                ->group(new Expression('CONCAT(I.year,I.Number)'))
                ->order(['I.year ASC', 'I.Number ASC']);
            $select = $sql->select()
                ->from(['I' => $select])
                ->columns([
                        'id',
                        'year',
                        'num',
                        'release_date',
                        'release_month',
                        'osp_row_number' => new Expression('@osp_row_number := @osp_row_number + 1')
                    ]
                )
                ->having(['CAST(CONCAT(I.year, osp_row_number) AS UNSIGNED) >= ?' => $item['byear'] . $item['bnum']])
                ->order(['osp_row_number ASC'])
                ->limit($item['rqty']);
            $selectString = $sql->getSqlStringForSqlObject($select);
            $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
            $issues       = $result->toArray();

            if(empty($issues)) {
                //throw new \Exception('Для подписки в nx с id = ' . $subscription_id . ' отсутствуют выпуски в асурц.');
                continue;
            }

            $data       = [];
            $date_start = $issues[0]['year'] . '-' . ( !empty($issues[0]['release_month']) ? str_pad($issues[0]['release_month'], 2, '0', STR_PAD_LEFT) : date('m', strtotime($issues[0]['release_date'])) ) . '-01';
            if(strtotime($date_start)!=strtotime($item['date_start'])) {
                $data['date_start'] = $date_start;
            }

            $date_end = 0;
            $period   = 0;
            if(count($issues) == $item['rqty']) {
                $date_end             = $issues[count($issues)-1]['year'] . '-' . ( !empty($issues[count($issues)-1]['release_month']) ? str_pad($issues[count($issues)-1]['release_month'], 2, '0', STR_PAD_LEFT) : date('m', strtotime($issues[count($issues)-1]['release_date'])) ) . '-01';
                $end_stamp            = strtotime($date_end);
                $end_next_month_stamp = mktime(date('H',$end_stamp), date('i',$end_stamp), date('s',$end_stamp), date('n',$end_stamp)+1, date('j',$end_stamp), date('Y',$end_stamp));
                $date_end             = date('Y', $end_next_month_stamp) . '-' . date('m', $end_next_month_stamp) . '-01';
                if(strtotime($date_end)!=strtotime($item['date_end'])) {
                    $data['date_end'] = $date_end;
                }

                $period = \nx\functions\getMonthsBetweenDates($date_start, $date_end);
                if($period!=$item['period']) {
                    $data['period'] = $period;
                }
            }
            if(!empty($data)) {
                $update       = $sql->update('nx_subscription')->set($data)->where(['id' => $item['item_id']]);
                $selectString = $sql->getSqlStringForSqlObject($update);
                $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
            }

            $item_issues_ids = array_map(create_function('$u', 'return (int)$u["id"];'), $issues);

            $delete_ids = array_diff(!empty($items_issues_ids[$item['id']]) ? $items_issues_ids[$item['id']] : [], $item_issues_ids);
            $new_ids    = array_diff($item_issues_ids, !empty($items_issues_ids[$item['id']]) ? $items_issues_ids[$item['id']] : []);

            if (count($delete_ids)) {
                $sql          = new Sql($db);
                $delete       = $sql->delete('nx_order_item_release')->where(['item_id' => $item['id'], 'release_id' => $delete_ids]);
                $selectString = $sql->getSqlStringForSqlObject($delete);
                $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
            }
            if (count($new_ids)) {
                foreach ($new_ids as $new_id) {
                    $sql          = new Sql($db);
                    $insert       = $sql->insert('nx_order_item_release')->values(['item_id' => $item['id'], 'release_id' => $new_id]);
                    $selectString = $sql->getSqlStringForSqlObject($insert);
                    $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
                }
            }
        }

        $query       = $sql->update('nx_order_item')->set(['last_updated' => date('Y-m-d H:i:s')])->where(['id' => $items_ids]);
        $queryString = $sql->getSqlStringForSqlObject($query);
        $db->query($queryString, $db::QUERY_MODE_EXECUTE);
    }
}
