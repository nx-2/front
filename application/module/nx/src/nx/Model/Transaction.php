<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Transaction extends AbstractTable
{

    public function __construct(Adapter $adapter)
    {
        parent::__construct('transactions', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Transaction));
    }

    public function getList($offset = 0, $limit = 100, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['T'=>$this->table])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS T.id AS id'),
                'shopid',
                'transactionid',
                'system',
                'sum',
                'description',
                'person',
                'personid',
                'email',
                'date',
                'sys_date_1',
                'sys_date_2',
                'sys_status',
                'shop_date',
                'shop_status',
                'last_check',
                'buh_status'
            ])
            ->join(['S' => 'shops'], 'S.id = T.shopid', ['shop' => 'name'])
            ->join(['SYS' => 'systems'], 'SYS.id = T.system', ['system_name' => 'name'], 'left')
            ->group('T.id')
            ->order('T.id DESC')
            ->limit($limit)
            ->offset($offset);

        if ($this->publisher_id) {
            $select
            ->join(['O'=>'nx_order'], 'T.transactionid = O.id', [])
            ->join(['PR'=>'nx_price'], 'PR.id = O.price_id', [])
            ->where(['PR.publisher_id' => $this->publisher_id]);
        } else {
            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }

        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(empty($filter['value']) && $filter['property']!='sys_status' && $filter['property']!='shop_status')
                {
                    continue;
                }
                switch($filter['property'])
                {
                	case 'sys_status':
                    case 'shop_status':
                        $select->where->equalTo('T.'.$filter['property'], '' . $filter['value'] . '');
                        break;
                    case 'date':
                    case 'description':
                    case 'person':
                    case 'id':
                    case 'transactionid':
                    case 'sum':
                        $select->where->like('T.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                    case 'shop':
                        $select->where->like('S.name', '%' . $filter['value'] . '%');
                        break;
                    case 'shop_id':
                        $select->where->equalTo('S.id', $filter['value']);
                        break;
                    case 'system_name':
                        $select->where->like('SYS.name', '%' . $filter['value'] . '%');
                        break;
                }
            }
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function getTransactions($params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['T'=>$this->table])
            ->columns([
                'id',
                'shopid',
                'order_id' => 'transactionid',
                'system',
                'sum',
                'description',
                'person',
                'personid',
                'email',
                'date',
                'sys_date_1',
                'sys_date_2',
                'sys_status',
                'shop_date',
                'shop_status',
                'last_check',
                'buh_status'
            ])
            ->join(['S' => 'shops'], 'S.id = T.shopid', ['shop' => 'name'])
            ->join(['SYS' => 'systems'], 'SYS.id = T.system', ['system_name' => 'name', 'system_desc' => 'description', 'system_inn' => 'inn', 'system_kpp' => 'kpp'])
            ->order('T.id DESC');
        if(!empty($params['sys_status'])) {
            $select->where(['T.sys_status' => (int)$params['sys_status']]);//2 - оплачено
        }
        if(!empty($params['shopid'])) {
            $select->where(['T.shopid' => (int)$params['shopid']]);
        }
        if(!empty($params['system'])) {
            $select->where(['T.system' => (int)$params['system']]);
        }
        if(!empty($params['date_start'])) {
            $select->where->greaterThanOrEqualTo('T.date', date('Y-m-d H:i:s', strtotime($params['date_start'])));
        }
        if(!empty($params['period_days'])) {
            $select->where->greaterThanOrEqualTo('T.date', new Expression('SUBDATE(NOW(), ' . $params['period_days'] . ')'));
        }
        if(!empty($params['id'])) {
            $select->where(['T.id' => $params['id']]);
        }
        if(!empty($params['isset_sum'])) {
            $select->where->notEqualTo('T.sum', 0);
        }

        if ($this->publisher_id) {
            $select
            ->join(['O'=>'nx_order'], 'T.transactionid = O.id', [])
            ->join(['PR'=>'nx_price'], 'PR.id = O.price_id', [])
            ->where(['PR.publisher_id' => $this->publisher_id]);
        } else {
            if (!isset($params['any_publisher']) || (boolean)$params['any_publisher'] != true )
                $select->where('0'); //не выводить ничего если не знаем какой издатель
        }


        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet;
    }
}
