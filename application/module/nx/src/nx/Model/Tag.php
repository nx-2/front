<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Tag extends AbstractTable
{
    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_tag', $adapter);
    }

    public function getTagsBy(array $params)
    {
        $join_map = [//alias => table, condition, dependent_table
            'U'  => ['User', 'TBL.create_user_id = U.User_ID'],
            'U2' => ['User', 'TBL.last_user_id = U2.User_ID']
        ];
        $columns_map = [
            'create_user_name' => ['joins' => ['U'], 'column' => 'IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)'],
            'last_user_name'   => ['joins' => ['U2'], 'column' => 'IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)']
        ];
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                    $select->where(['TBL.'.$filter['property'] => $filter['value']]);
                    break;
                case 'name':
                    $select->where->like('TBL.'.$filter['property'], '%' . $filter['value'] . '%');
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        return $result;
    }

    /*public function getTagsBy(array $params)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);

        $join_map = array(//alias => table, condition, dependent_table, load: table_field, entity_field, filter callback
            'U'  => array('User', 'T.create_user_id = U.User_ID'),
            'U2' => array('User', 'T.last_user_id = U2.User_ID')
        );

        $columns_map = array(//array('table'=>,'column'=>,'joins'=>,'columns'=>)
            'create_user_name' => array('joins' => array('U'), 'column' => 'IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)'),
            'last_user_name'   => array('joins' => array('U2'), 'column' => 'IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')
        );
        $columns_map = $this->normalize_columns_map($columns_map);

        $joins = array();
        $loads = array();
        //process columns
        list($columns_names, $columns_db) = $this->get_columns2($params, $columns_map);

        $select = $sql->select()
            ->from(array('T'=>$this->table));

        if(!empty($params['calc_found_rows'])) {
            $select->quantifier(new Expression('SQL_CALC_FOUND_ROWS'));
        }
        if(!empty($params['limit'])) {
            $select->limit((int)$params['limit']);
        }
        if(!empty($params['offset'])) {
            $select->offset((int)$params['offset']);
        }
        if(!empty($params['current'])) {
            $select->limit(1);
        }
        if(!empty($params['sorters']))
        {
            foreach($params['sorters'] as $sorter)
            {
                if(empty($sorter['direction'])) {
                    continue;
                }
                switch($sorter['property'])
                {
                    case 'id':
                        $select->order('T.'.$sorter['property'] . ' ' . $sorter['direction']);
                        break;
                }
            }
        }
        else
        {
            if(!empty($params['order'])) {
                $select->order($params['order']);
            }
            else {
                $select->order('T.id DESC');
            }
        }

        //process filters = add where, add joins if needed, del loads
        $filters = $this->get_filters($params);
        if(!empty($filters))
        {
            foreach($filters as $filter)
            {
                switch($filter['property'])
                {
                    case 'id':
                        $select->where(array('T.'.$filter['property'] => $filter['value']));
                        break;
                    case 'name':
                        $select->where->like('T.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                }
            }
        }

        //process columns
        list($columns, $joins_by_cols, $loads_by_cols) = $this->process_columns2($columns_db, $columns_map);
        $joins = array_merge($joins, $joins_by_cols);
        $loads = array_merge($loads, $loads_by_cols);

        $select->columns($columns);

        //process joins
        foreach($joins as $key => $join_type) {
            if(!empty($join_map[$key][2]) && !empty($join_type)) {
                $joins[$join_map[$key][2]] = $join_type;
            }
        }
        foreach($join_map as $key => $table) {
            if(!empty($joins[$key])) {
                $select->join(array($key=>$table[0]), $table[1], array(), $joins[$key]);
            }
        }

        if(!empty($params['group'])) {
            $select->group($params['group']);
        }
        else {
            //$select->group('O.id');
        }

        $selectString = $sql->getSqlStringForSqlObject($select);//var_dump($selectString);//die;
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);
        $resultSet->buffer();

        $count = $resultSet->count();
        if($count && !empty($params['calc_found_rows'])) {
            $count = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();
            $count = $count['count'];
        }

        //process custom entity fields

        $resultSet = !empty($params['current']) ? $resultSet->current() : $resultSet;

        $result = !empty($params['calc_found_rows']) ? array('items' => $resultSet, 'total' => $count) : $resultSet;

        return $result;
    }*/

    public function getTagBy($params)
    {
        $params['current'] = true;
        return $this->getTagsBy($params);
    }

    public function addTag($data)
    {
        if(empty($data))
        {
            return false;
        }
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            $data['created'] = date('Y-m-d H:i:s');
            $data['name'] = trim($data['name']);
            if(empty($data['name']))
            {
                return false;
            }

            $this->insert($data);
            $tag_id = $this->lastInsertValue;
            $this->logger->log('Добавление тега ' . $data['name'] . ' #' . $tag_id, 'add_tag', $data, !empty($data['create_user_id']) ? $data['create_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $tag_id;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }

    public function updateTag($id, $data)
    {
        if(empty($id) || empty($data))
        {
            return false;
        }

        $tag = $this->getTagBy(['id' => $id]);

        if(empty($tag->id))
        {
            return false;
        }
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(isset($data['name'])) {
                $data['name'] = trim($data['name']);
                if(empty($data['name'])) {
                    unset($data['name']);
                }
            }
            unset($data['create_user_id']);
            unset($data['created']);
            $this->update($data, ['id' => $tag->id]);

            $this->logger->log('Обновление тега #' . $tag->id, 'update_tag', $data, !empty($data['last_user_id']) ? $data['last_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $tag->id;
        }
        catch(\Exception $e)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
        }
    }

    /*public function getObjectsForCombo($keywords = '')
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(array('T'=>$this->table))
            ->columns(array(
                'id',
                'name' => new Expression('CONCAT(T.id,". ",T.name)')
            ));
        if(!empty($keywords))
        {
            $select->where->like('name','%'.$keywords.'%');
        }
        $select->order('T.name ASC')->limit('150');
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        //$result       = array_merge(array(array('id' => '0', 'name' => '0')), $result->toArray());
        $result       = $result->toArray();
        return $result;
    }*/
    public function getObjectsForCombo(array $params)//$keywords = '', $ids)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['T'=>'nx_tag'])
            ->columns([
                'id',
                'name' => new Expression('CONCAT(T.id,". ",T.name)')
            ]);
        if(!empty($params['query']))
        {
            $select->where->like('name','%'.$params['query'].'%');
        }
        if(!empty($params['entity_id']))
        {
            $select->join(['TE' => 'nx_tag_entity'], 'TE.tag_id = T.id', []);
            $select->where(['TE.entity_id' => $params['entity_id']]);
        }
        $select->order('T.name ASC')->limit('150');
        $select->quantifier('DISTINCT');
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $result       = $result->toArray();

        if(!empty($params['ids']))
        {
            $sql    = new Sql($db);
            $select = $sql->select()
                ->from(['T'=>'nx_tag'])
                ->columns([
                    'id',
                    'name' => new Expression('CONCAT(T.id,". ",T.name)')
                ])
                ->where(['T.id' => $params['ids']])
                ->order('T.id ASC');
            $selectString = $sql->getSqlStringForSqlObject($select);
            $by_ids       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
            $result       = array_merge($by_ids->toArray(), $result);
        }

        return $result;
    }
}
