<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Price extends AbstractTable
{
    public $zoneModel;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_price', $adapter);
    }

    public function getPricesBy(array $params)
    {
        $join_map = [//alias => table, condition, dependent_table
            'U'  => ['User', 'TBL.create_user_id = U.User_ID'],
            'U2' => ['User', 'TBL.last_user_id = U2.User_ID']
        ];
        $columns_map = [
            'create_user_name' => ['joins' => ['U'], 'column' => 'IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)'],
            'last_user_name'   => ['joins' => ['U2'], 'column' => 'IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)']
        ];
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                case 'customer_type_id':
                case 'checked':
                case 'item_type':
                    $select->where(['TBL.'.$filter['property'] => $filter['value']]);
                    break;
                case 'date_end':
                    $select->where->lessThanOrEqualTo('TBL.date', $filter['value']);
                    break;
            }
        };
        $get_active_col = !empty($params['columns']) ? array_search('active', $params['columns']) : false;
        if($get_active_col!==false) {
            unset($params['columns'][$get_active_col]);
        }
        $result = $this->getByMap(array_merge($params, [
            'as_array'    => true,
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        if($get_active_col!==false)
        {
            $currentPrice = $this->getMagazinePriceList();
            foreach($result as $i=>$item)
            {
                if($item['id'] == $currentPrice['customerType']['1']['priceID'] || $item['id'] == $currentPrice['customerType']['2']['priceID'])
                {
                    $result[$i]['active'] = '1';
                }
                else
                {
                    $result[$i]['active'] = '0';
                }
            }
        }

        return $result;
    }

    public function getPriceBy($params)
    {
        $params['current'] = true;
        return $this->getPricesBy($params);
    }

    public function getPriceData(array $params)
    {
        $base_data = !empty($params['base_data']) ? $params['base_data'] : $this->getZeroPrice();
        $data = [];
        foreach($base_data as $row)
        {
            $data[$row['periodical_id']] = $row;
        }
        $items = $this->getItemsBy(['price_id' => $params['price_id'], 'columns' => ['item_id', 'zone_id', 'delivery_type_id', 'price']])->toArray();
        $ids   = [];
        foreach($items as $item)
        {
            if(isset($data[$item['item_id']]['price_' . $item['zone_id'] . '_' . $item['delivery_type_id']]))
            {
                $data[$item['item_id']]['price_' . $item['zone_id'] . '_' . $item['delivery_type_id']] = $item['price'];
            }
        }
        $data = array_values($data);
        return $data;
    }

    public function getPriceList(array $params)//$dateStart = '')
    {
        //if(empty($dateStart) && !empty($this->currentPrice))
        //{
        //    return $this->currentPrice;
        ///}
        $db     = $this->adapter;
        $sql    = new Sql($db);
        /*$select = $sql->select()
            ->from('nx_price')
            ->columns(array(
                'id'
            ))
            ->where('date < ' . (empty($params['dateStart']) ? 'NOW()' : '"' . $params['dateStart'] . '"'))
            ->where('checked = 1')
            ->where('item_type = 1')
            ->order('date DESC')
            ->limit(1);
        if(!empty($params['customer_type_id']))
        {
            $select->where(array('customer_type_id' => (int)$params['customer_type_id']));
        }
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $price        = $result->current();*/

        $zones = $this->zoneModel->getZonesBy(['as_array' => true, 'joins' => ['ZC' => 'inner'], 'group' => 'id']);

        $price = $this->getPriceBy(array_merge($params, ['order' => 'TBL.date DESC']));
        $priceID = $price['id'];

        $select = $sql->select()
            ->from('nx_price_item')
            ->columns([
                    'MagazineID'                 => 'item_id',
                    'Price'                      => 'price',
                    'ZoneID'                     => 'zone_id',
                    'SubscriptionDeliveryTypeID' => 'delivery_type_id'
                ]
            )
            ->where(['price_id = ?' => $priceID])
            //->where('Price <> 0')
            ->order('item_id');
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $priceData    = $result->toArray();

        $currentPrice = [];
        foreach ($priceData as $price)
        {
            $currentPrice['priceID'] = $priceID;
            $currentPrice['magazine'][$price['MagazineID']]['eDelivery'] = !empty($currentPrice['magazine'][$price['MagazineID']]['eDelivery']) ? True : False;
            $currentPrice['magazine'][$price['MagazineID']]['pDelivery'] = !empty($currentPrice['magazine'][$price['MagazineID']]['pDelivery']) ? True : False;
            //$currentPrice['magazine'][$price['MagazineID']]['period']    = array(
            //    'min' => $price['min'],
            //    'max' => $price['max']
            //);
            if ($price['SubscriptionDeliveryTypeID'] == 5 && !empty($price['Price']))
            {
                $currentPrice['magazine'][$price['MagazineID']]['eDelivery'] = True;
                foreach($zones as $zone)
                {//single pdf price for all zones
                    $currentPrice['magazine'][$price['MagazineID']]['deliveryType'][$price['SubscriptionDeliveryTypeID']]['zone'][$zone['id']] = $price;
                }
            }
            else
            {
                $currentPrice['magazine'][$price['MagazineID']]['deliveryType'][$price['SubscriptionDeliveryTypeID']]['zone'][$price['ZoneID']] = $price;
            }
            if ($price['SubscriptionDeliveryTypeID'] == 1 && !empty($price['Price']))
            {
                $currentPrice['magazine'][$price['MagazineID']]['pDelivery'] = True;
            }
            //$currentPrice['magazine'][$price['MagazineID']]['deliveryType'][$price['SubscriptionDeliveryTypeID']]['zone'][$price['ZoneID']] = $price;
        }
        //if(empty($dateStart) && !empty($currentPrice))
        //{
        //    $this->currentPrice = $currentPrice;
        //}
        return $currentPrice;
    }

    public function getMagazinePriceList()
    {
        $person_price  = $this->getPriceList(['checked' => 1, 'item_type' => 1, 'customer_type_id' => [0,1], 'date_end' => date('Y-m-d')]);
        $company_price = $this->getPriceList(['checked' => 1, 'item_type' => 1, 'customer_type_id' => [0,2], 'date_end' => date('Y-m-d')]);
        $price = ['customerType' => [
            '1' => $person_price,
            '2' => $company_price
        ]];
        return $price;
    }

    public function getItemsBy($params)
    {
        $join_map = [//alias => table, condition, dependent_table
            'U'  => ['User', 'TBL.create_user_id = U.User_ID'],
            'U2' => ['User', 'TBL.last_user_id = U2.User_ID']
        ];
        $columns_map = [
            'create_user_name' => ['joins' => ['U'], 'column' => 'IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)'],
            'last_user_name'   => ['joins' => ['U2'], 'column' => 'IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)']
        ];
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                case 'price_id':
                    $select->where(['TBL.'.$filter['property'] => $filter['value']]);
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'table'       => 'nx_price_item',
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        return $result;
    }

    public function getZeroPrice(array $params)
    {
        $zones = !empty($params['zones']) ? $params['zones'] : $this->zoneModel->getZones();

        $db           = $this->adapter;
        $sql          = new Sql($db);
        $select       = $sql->select()->from(['M'=>'Edition'])->columns(['periodical_id' => 'Message_ID', 'periodical_name' => 'HumanizedName'])->where(['deleted' => 0, 'subscribe' => 1]);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $magazines    = $result->toArray();

        $data = [];

        foreach($magazines as $magazine)
        {
            $row = [];
            $row['periodical_name'] = $magazine['periodical_name'];
            $row['periodical_id']   = $magazine['periodical_id'];
            foreach($zones as $zone)
            {
                foreach($zone['delivery_type'] as $delivery_type)
                {
                    $row['price_' . $zone['zone_id'] . '_' . $delivery_type['id']] = 0;
                }
            }
            $data[] = $row;
        }

        return $data;
    }

    public function addFromGrid(array $data)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        //var_dump($data);
        if(empty($data['priceData']['name']) || empty($data['priceData']['date']))
        {
            return false;
        }
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(empty($data['priceData']['item_type']))
            {
                $data['priceData']['item_type'] = 1;
            }
            $this->insert($data['priceData']);
            $price_id = $this->lastInsertValue;
            if(!empty($price_id) && !empty($data['itemsData']))
            {
                foreach($data['itemsData'] as $row)
                {
                    $values = ['item_id' => $row['periodical_id'], 'price_id' => $price_id];
                    foreach($row as $key => $price)
                    {
                        if(mb_substr($key, 0, 5) == 'price')
                        {
                            $key = explode('_', $key);

                            $values['zone_id']          = $key[1];
                            $values['delivery_type_id'] = $key[2];
                            $values['price']            = $price;

                            $query       = $sql->insert('nx_price_item')->values($values);
                            $queryString = $sql->getSqlStringForSqlObject($query);
                            $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
                        }
                    }
                }
            }
            $this->logger->log('Добавление прайса ' . $data['priceData']['name'] . ' #' . $price_id, 'add_price', $data, !empty($data['priceData']['create_user_id']) ? $data['priceData']['create_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $price_id;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }

    public function updateFromGrid($id, array $data)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        //var_dump($data);
        if(empty($id) || empty($data['priceData']['name']) || empty($data['priceData']['date']))
        {
            return false;
        }
        $price = $this->getPriceBy(['id' => $id]);
        if(empty($price['id']))
        {
            return false;
        }
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            $this->update($data['priceData'], ['id' => $id]);
            if(!empty($data['itemsData']))
            {
                $items    = $this->getItemsBy(['price_id' => $id, 'columns' => ['id', 'item_id', 'zone_id', 'delivery_type_id', 'price']])->toArray();
                $curitems = [];
                foreach($items as $item)
                {
                    $curitems[$item['item_id']][$item['zone_id']][$item['delivery_type_id']] = $item;
                }
                foreach($data['itemsData'] as $row)
                {
                    $values = ['item_id' => $row['periodical_id'], 'price_id' => $id];
                    foreach($row as $key => $price)
                    {
                        if(mb_substr($key, 0, 5) == 'price')
                        {
                            $key = explode('_', $key);

                            $values['zone_id']          = $key[1];
                            $values['delivery_type_id'] = $key[2];
                            $values['price']            = $price;

                            if(isset($curitems[$values['item_id']][$values['zone_id']][$values['delivery_type_id']]))
                            {
                                $query       = $sql->update('nx_price_item')->set(['price' => $values['price']])->where(['id' => $curitems[$values['item_id']][$values['zone_id']][$values['delivery_type_id']]['id']]);
                                $queryString = $sql->getSqlStringForSqlObject($query);
                                $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
                            }
                            else
                            {
                                $query       = $sql->insert('nx_price_item')->values($values);
                                $queryString = $sql->getSqlStringForSqlObject($query);
                                $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
                            }
                        }
                    }
                }
            }
            $this->logger->log('Обновление прайса ' . $data['priceData']['name'] . ' #' . $id, 'update_price', $data, !empty($data['priceData']['last_user_id']) ? $data['priceData']['last_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $id;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }
}
