<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Person extends AbstractTable
{
    public $addressModel;

    public function __construct(Adapter $adapter)
    {
        parent::__construct('nx_person', $adapter, null, new \Zend\Db\ResultSet\HydratingResultSet(new \Zend\Stdlib\Hydrator\ArraySerializable, new \nx\Entity\Person));
    }

    public function getList($offset, $limit, $params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['P'=>$this->table])
            ->columns([
                new Expression('SQL_CALC_FOUND_ROWS P.id AS id'),
                'name',
                'email',
                'phone',
                'position',
                'enabled',
                'created'
            ]);
        if(!empty($params['company_id']))
        {
            $select->where(['company_id'=>(int)$params['company_id']]);
        }
        if(!empty($params['filters']))
        {
            foreach($params['filters'] as $filter)
            {
                if(empty($filter['value']))
                {
                    continue;
                }
                switch($filter['property'])
                {
                    case 'created':
                    case 'name':
                    case 'position':
                    case 'email':
                    case 'phone':
                    case 'id':
                        $select->where->like('P.'.$filter['property'], '%' . $filter['value'] . '%');
                        break;
                }
            }
        }
        if(!empty($params['ids']))
        {
            $select->where->In('P.id', $params['ids']);
        }
            //->join(array('G' => 'User_Group'), 'G.User_ID = U.User_ID')
            //->where('G.PermissionGroup_ID = ' . self :: Netcat_Permission_Group_ID)
        $select->order('P.id DESC')
            ->limit($limit)
            ->offset($offset);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();
        $count        = $db->query('SELECT FOUND_ROWS() AS count', Adapter::QUERY_MODE_EXECUTE)->current();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return [
            'items' => $resultSet,
            'total' => $count['count']
        ];
    }

    public function getPersons($params = [])
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['P'=>$this->table])
            ->columns([
                'id',
                'name',
                'created',
                'xpress_id'
            ]);
        if(!empty($params['email']))
        {
            $select->where(['P.email'=>$params['email']]);
        }
        if(!empty($params['xpress_id']))
        {
            $select->where(['P.xpress_id'=>$params['xpress_id']]);
        }
        if(!empty($params['xpress_type']))
        {
            $select->where(['P.xpress_type'=>$params['xpress_type']]);
        }
        if(!empty($params['name']))
        {
            $select->where(['P.name'=>$params['name']]);
        }
        $select->order('P.id DESC');
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getByID($id)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['P'=>$this->table])
            ->columns([
                'id',
                'f',
                'i',
                'o',
                'name',
                'gender',
                'phone',
                'position',
                'speciality',
                'company_id',
                'email',
                'comment',
                'checked',
                'enabled',
                'created',
                'last_updated',
                'create_user_id',
                'last_user_id'
            ])
            ->join(['A'=>'nx_address'], 'A.id = P.address_id', ['address_phone' => 'phone', 'fax', 'address', 'zipcode', 'country_id', 'area', 'region', 'address_comment' => 'comment', 'city', 'telcode'], 'left')
            ->join(['U'=>'User'], 'U.User_ID = P.create_user_id', ['create_user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')], 'left')
            ->join(['U2'=>'User'], 'U2.User_ID = P.last_user_id', ['last_user_name' => new Expression('IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')], 'left')
            ->join(['C'=>'nx_company'], 'C.id = P.company_id', ['company_name' => 'name'], 'left')
            ->where(['P.id' => $id]);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    public function getBy(array $params)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['P'=>$this->table])
            ->columns([
                'id',
                'f',
                'i',
                'o',
                'name',
                'gender',
                'phone',
                'position',
                'speciality',
                'company_id',
                'email',
                'comment',
                'address_id',
                'checked',
                'created',
                'last_updated',
                'create_user_id',
                'last_user_id'
            ])
            ->join(['A'=>'nx_address'], 'A.id = P.address_id', ['address_phone' => 'phone', 'fax', 'address', 'zipcode', 'country_id', 'area', 'region', 'address_comment' => 'comment', 'city', 'telcode'], 'left')
            ->join(['U'=>'User'], 'U.User_ID = P.create_user_id', ['create_user_name' => new Expression('IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)')], 'left')
            ->join(['U2'=>'User'], 'U2.User_ID = P.last_user_id', ['last_user_name' => new Expression('IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')], 'left')
            ->join(['C'=>'nx_company'], 'C.id = P.company_id', ['company_name' => 'name'], 'left')
            ->limit(1);
        if(!empty($params['name']))
        {
            $select->where(['P.name' => $params['name']]);
        }
        if(!empty($params['email']))
        {
            $select->where(['P.email' => $params['email']]);
        }
        if(!empty($params['phone']))
        {
            $select->where(['P.phone' => $params['phone']]);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString)->execute();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($result);

        return $resultSet->current();
    }

    /**
     * Отдает список объектов для комбокса
     * @param string $keywords
     * @return array
     */
    public function getObjectsForCombo($keywords = '')
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['P'=>$this->table])
            ->columns([
                'id',
                'name' => new Expression('CONCAT(P.id,". ",P.name)')
            ])
            ->where(['P.enabled' => 1]);
        if(!empty($keywords))
        {
            $select->where->like('name','%'.$keywords.'%');
        }
        $select->order('P.name ASC')->limit('150');
        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $result       = array_merge([['id' => '0', 'name' => '0']], $result->toArray());
        return $result;
    }

    public function addPerson($data)
    {
        if(empty($data['personData']))
        {
            return false;
        }
        $data = \nx\functions\applyFunctionToData($data, 'trim');
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(!empty($data['addressData']))
            {
                $data['addressData']['label'] = 'person';
                $this->addressModel->insert($data['addressData']);
                $address_id = $this->addressModel->lastInsertValue;
                $data['personData']['address_id'] = $address_id;
            }
            $data['personData']['created'] = date('Y-m-d H:i:s');
            if(empty($data['personData']['label']))
            {
                $data['personData']['label'] = 'person';
            }
            if(empty($data['personData']['name']))
            {
                $space1 = (($data['personData']['f'] && $data['personData']['i']) || ($data['personData']['f'] && $data['personData']['o'] && !$data['personData']['i']) ? ' ' : '');
                $space2 = ($data['personData']['i'] && $data['personData']['o'] ? ' ' : '');
                $data['personData']['name'] = $data['personData']['f'] . $space1 . $data['personData']['i'] . $space2 . $data['personData']['o'];
            }
            $data['personData']['name'] = trim($data['personData']['name']);
            if(empty($data['personData']['name']))
            {
                return false;
            }
            if(!empty($data['personData']['email']))
            {
                $check = $this->getBy(['name'=>$data['personData']['name'], 'email'=>$data['personData']['email']]);
                if(!empty($check))
                {//person already exists
                    throw new \Exception('Физ. лицо с таким именем и email уже существует');
                    return false;
                }
            }

            $this->insert($data['personData']);
            $person_id = $this->lastInsertValue;
            $this->logger->log('Добавление физ. лица ' . $data['personData']['name'] . ' #' . $person_id, 'add_person', $data, !empty($data['personData']['create_user_id']) ? $data['personData']['create_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $person_id;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
            return false;
        }
    }

    public function updatePerson($id, $data)
    {
        if(empty($id) || empty($data))
        {
            return false;
        }

        $person = $this->getByID($id);

        if(empty($person->id))
        {
            return false;
        }
        $data = \nx\functions\applyFunctionToData($data, 'trim');
        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            if(!empty($data['addressData']))
            {
                $insert_address = 1;
                if(!empty($person->address_id))
                {
                    $address = $this->addressModel->getByID($person->address_id);
                    if(!empty($address))
                    {
                        $insert_address = 0;
                        $this->addressModel->update($data['addressData'], ['id' => $person->address_id]);
                    }
                }
                if($insert_address)
                {
                    $data['addressData']['label'] = 'person';
                    $this->addressModel->insert($data['addressData']);
                    $address_id = $this->addressModel->lastInsertValue;
                    $data['personData']['address_id'] = $address_id;
                }
            }

            if(!empty($data['personData']))
            {
                if(!empty($data['personData']['f']) || !empty($data['personData']['i']) || !empty($data['personData']['o']))
                {
                    $space1 = (($data['personData']['f'] && $data['personData']['i']) || ($data['personData']['f'] && $data['personData']['o'] && !$data['personData']['i']) ? ' ' : '');
                    $space2 = ($data['personData']['i'] && $data['personData']['o'] ? ' ' : '');
                    $data['personData']['name'] = $data['personData']['f'] . $space1 . $data['personData']['i'] . $space2 . $data['personData']['o'];
                }
                if(isset($data['personData']['name'])) {
                    $data['personData']['name'] = trim($data['personData']['name']);
                    if(empty($data['personData']['name'])) {
                        unset($data['personData']['name']);
                    }
                }
                unset($data['personData']['label']);
                unset($data['personData']['create_user_id']);
                unset($data['personData']['created']);
                $this->update($data['personData'], ['id' => $person->id]);
            }
            if(!empty($data['personData']['last_user_id']))
            {
                $this->logger->log('Обновление физ. лица #' . $person->id, 'update_person', $data, !empty($data['personData']['last_user_id']) ? $data['personData']['last_user_id'] : 0);
            }
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $person->id;
        }
        catch(\Exception $e)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $e;
            return false;
        }
    }
}
