<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class MailTemplates extends AbstractTableGateway
{
    protected $cfg;

    public function __construct(Adapter $adapter, $config)
    {
        $this->table   = 'aecms_mail_templates';
        $this->adapter = $adapter;
        $this->cfg     = $config;
        //$OSP_ROOT = dirname(dirname(dirname(dirname($_SERVER['DOCUMENT_ROOT'] . '../').'../').'../').'../').'/osp';
        //require_once($OSP_ROOT . '/osp_moduls/classes/MagazineSubscribe.php');
        //$this->osp_subscribe = new \MagazineSubscribe();
    }

    /**
     * достать шаблон письма
     * @param int $id
     * @return array
     **/
    public function getByID($id)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['T' => 'aecms_mail_templates'])
            ->columns([
                    'id',
                    'name',
                    'subject',
                    'text',
                    'created'
                ]
            )
            ->where(['T.id = ?' => (int)$id]);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $template = $result->current();

        return $template;
    }

    /**
     * Отдает рекламу для шаблона
     * @param (int) $templateID
     * @return (array)
     */
    public function getAdvToTemplate($templateID)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['T' => 'OSPSubscribeADVImplementsTemplate'])
            ->join(['V' => 'OSPSubscribeADV'], 'V.id = T.advID', ['id', 'title', 'contentText', 'contentHtml'])
            ->where(['T.templateID = ?' => $templateID])
            ->order('T.advID DESC')
            ->limit(1);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        return $result->toArray();
    }

    /**
     * Подставляет рекламу в шаблон
     * @param (int) $templateID
     * @param (string) $templateBody
     * @return (string)
     *
     **/
    public function parseADVToTemplate($templateID,$templateBody)
    {
        $advList   = $this->getAdvToTemplate($templateID);
        $advTop    = '';
        $advRight  = '';
        $advBottom = '';
        if(!empty($advList)) foreach ($advList as $adv) {
            switch ($adv['position']) {
                case 'top' :
                    $advTop    = !empty($adv['contentHtml']) ? $adv['contentHtml'] : htmlspecialchars($adv['contentText']);
                    break;
                case 'right' :
                    $advRight  = !empty($adv['contentHtml']) ? $adv['contentHtml'] : htmlspecialchars($adv['contentText']);
                    break;
                case 'bottom' :
                    $advBottom = !empty($adv['contentHtml']) ? $adv['contentHtml'] : htmlspecialchars($adv['contentText']);
                    break;
            }
        }

        $replacedData  = [
            '%%ADV_TOP%%'     => !empty($advTop) ?  '<hr/>На правах рекламы:<br/>' . $advTop . '<hr/>' : '',
            '%%ADV_SIDE%%'    => !empty($advRight) ?  'На правах рекламы:<br/>' . $advRight . '<hr/>' : '',
            '%%ADV_BOTTOM%%'  => !empty($advBottom) ?  '<hr/>На правах рекламы:<br/>' . $advBottom : ''
        ];
        return str_replace(array_keys($replacedData), array_values($replacedData), $templateBody);
    }

    /**
     * достать шаблон письма и вставить значения
     * @param int $templateID
     * @param array $templateData
     * @return array
     **/
    public function getParseTemplate($templateID, $templateData)
    {
        $subject = $text = '';

        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['T' => 'aecms_mail_templates'])
            ->columns([
                    'subject',
                    'text'
                ]
            )
            ->where(['T.id = ?' => (int)$templateID])
            ->query()->fetch();

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $template     = $result->current();

        if(!empty($template))
        {
            $text    = str_replace(array_keys($templateData), array_values($templateData), $template['text']);
            $subject = str_replace(array_keys($templateData), array_values($templateData), $template['subject']);
        }

        $text = $this->parseADVToTemplate($templateID, $text);

        return ['mailSubject' => $subject, 'mailText' => $text];
    }

    /**
     * достать шаблон письма и вставить значения и отправить через БД
     * @param int $templateID
     * @param array $templateData
     * @param array $mailData
     * @return array
     **/
    /*public function getParseAndSendTemplate($templateID, $templateData, $mailData)
    {
        $subject = $text = '';

        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(array('T' => 'aecms_mail_templates'))
            ->columns(array(
                    'subject',
                    'text'
                )
            )
            ->where(array('T.id = ?' => (int)$templateID))
            ->query()->fetch();

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $template     = $result->current();

        if(!empty($template) && !empty($mailData['to_email']))
        {
            $text    = str_replace(array_keys($templateData), array_values($templateData), $template['text']);
            $subject = str_replace(array_keys($templateData), array_values($templateData), $template['subject']);
            $text    = $this->parseADVToTemplate($templateID, $text);

            if($text && $subject)
            {
                $this->db->insert('aecms_mail_send', array(
                        'subject'  => $subject,
                        'plain'    => '',
                        'html'     => $text,
                        'priority' => !empty($mailData['priority']) ? $mailData['priority'] : 1
                     )
                );
                $mailID = $this->db->lastInsertId();
                if(!empty($mailData['mailID'])) $mailID = $mailData['mailID'];
                $this->db->insert('aecms_mail_send_queue', array(
                        'mailID'        => $mailID,
                        'fromEmail'     => !empty($mailData['from']) ? $mailData['from'] : 'info@osp.ru',
                        'toEmail'       => $mailData['to_email'],
                        'toName'        => !empty($mailData['to_name']) ? $mailData['to_name'] : '',
                        'queuePriority' => 1,
                        'sendAsHtml'    => 1
                     )
                );
                return true;
            }
        }

        return false;
    }*/

    /**
     * отправка письма через базу по крону
     * @param array $mailData
     * @return void
     * @author
     **/
    public function create($mailData)
    {
        if(empty($mailData['to_email']) || (empty($mailData['subject']) && empty($mailData['text'])))
        {
            throw new \Exception('Недостаточно данных для отправки письма');
        }
        $mail_data = ['mail' => [
            'to_email'   => $mailData['to_email'],
            'cc'         => !empty($mailData['cc']) ? $mailData['cc'] : '',
            'to_name'    => !empty($mailData['to_name']) ? $mailData['to_name'] : '',
            'from_email' => !empty($mailData['from']) ? $mailData['from'] : '',
            'subject'    => $mailData['subject'],
            'body'       => !empty($mailData['text']) ? $mailData['text'] : '',
            'priority'   => !empty($mailData['priority']) ? $mailData['priority'] : 1
        ]];

        $client    = new \Zend\Http\Client($this->cfg['mail_service']['send_db']);

        $timestamp = time();
        $token     = md5($this->cfg['mail_service']['apikey'] . $timestamp);
        // $client->setOptions(array('sslverifypeer' => false));

        $client->setOptions(['sslcapath' => '/etc/ssl/certs']);
        $client->setParameterGet(['token' => $token, 'timestamp' => $timestamp]);
        $client->setMethod('POST')->setParameterPost($mail_data);
        $response = $client->send();//var_dump($this->cfg['mail_service']['create'], $response->toString(), $client->getLastRawRequest());
        if ($response->isSuccess()) {
            $data = \nx\functions\objectToArray(json_decode($response->getBody()));
        }
        return !empty($data['id']) ? $data['id'] : 0;
        /*$db  = $this->adapter;
        $sql = new Sql($db);

        $insert = $sql->insert('aecms_mail_send')->values(array(
                'subject'  => $mailData['subject'],
                'plain'    => '',
                'html'     => $mailData['text'],
                'priority' => !empty($mailData['priority']) ? $mailData['priority'] : 1
             )
        );
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);

        $mailID = $db->getDriver()->getLastGeneratedValue();

        $insert = $sql->insert('aecms_mail_send_queue')->values(array(
                'mailID'        => $mailID,
                'fromEmail'     => !empty($mailData['from']) ? $mailData['from'] : 'info@osp.ru',
                'toEmail'       => $mailData['to_email'],
                'toName'        => !empty($mailData['to_name']) ? $mailData['to_name'] : '',
                'queuePriority' => 1,
                'sendAsHtml'    => 1
             )
        );
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        return $db->getDriver()->getLastGeneratedValue();*/
    }

    /**
     * Отправка письма без регистрации в системе
     * @param string $to
     * @param string $subject
     * @param string $body
     */
    public static function sendWithoutRegistry($to, $subject, $body, $from = '')
    {
        //$transport = new \Zend\Mail\Transport\Sendmail();
        $cfgs = ['config/autoload/global.php', 'config/autoload/local.php'];
        foreach($cfgs as $i => $cfg) {
            if(!file_exists($cfg)) {
                unset($cfgs[$i]);
            }
        }
        $cfg = \Zend\Config\Factory::fromFiles($cfgs);

        $mail_data = ['mail' => [
            'to_email'   => $to,
            'from_email' => !empty($from) ? $from : 'noreply@osp.ru',
            'subject'    => $subject,
            'body'       => $body
        ]];

        $client    = new \Zend\Http\Client($cfg['nx']['mail_service']['send']);
        $timestamp = time();
        $token     = md5($cfg['nx']['mail_service']['apikey'] . $timestamp);
        // $client->setOptions(array('sslverifypeer' => false));
        $client->setOptions(['sslcapath' => '/etc/ssl/certs']);

        $client->setParameterGet(['token' => $token, 'timestamp' => $timestamp]);
        $client->setMethod('POST')->setParameterPost($mail_data);
        $response = $client->send();//var_dump($response->toString(), $client->getLastRawRequest());
        //if ($response->isSuccess()) {
        //    $data = \nx\functions\objectToArray(json_decode($response->getBody()));
        //}

        /*$transport = new \Zend\Mail\Transport\Smtp();
        $options = new \Zend\Mail\Transport\SmtpOptions(array('host' => 'stove.osp.ru'));
        $transport->setOptions($options);
        $mail = new \Zend\Mail\Message();

        $html = new \Zend\Mime\Part($body);
        $html->type    = "text/html";
        $html->charset = "UTF-8";
        $body = new \Zend\Mime\Message();
        $body->addPart($html);

        $mail->setBody($body);
        $mail->setEncoding('UTF-8');
        $mail->setFrom($from ? $from : 'noreply@osp.ru');
        $mail->setTo($to);
        $mail->setSubject($subject);

        $transport->send($mail);*/
    }
}
