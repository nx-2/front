<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Zone extends AbstractTable
{
    public function __construct(Adapter $adapter)
    {
        parent::__construct('Classificator_Zone', $adapter);
    }

    public function getZonesBy($params)
    {
        $join_map = [//alias => table, condition, dependent_table
            'ZD' => ['nx_zone_delivery', 'ZD.zone_id = TBL.id'],
            'ZC' => ['nx_zone_country', 'ZC.zone_id = TBL.id']
        ];
        $columns_map = [
            'delivery_type_ids' => ['column' => 'GROUP_CONCAT(DISTINCT ZD.delivery_type_id)', 'joins' => ['ZD']],
            'country_ids'       => ['column' => 'GROUP_CONCAT(DISTINCT ZC.country_id)', 'joins' => ['ZC']]
        ];
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                    $select->where(['TBL.id' => $filter['value']]);
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'table'       => 'nx_zone',
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        ]));
        return $result;
    }

    public function getZoneBy($params)
    {
        $params['current'] = true;
        return $this->getZonesBy($params);
    }

    public function getZones()
    {
        $db  = $this->adapter;
        $sql = new Sql($db);

        $select         = $sql->select()->from(['C'=>'nx_delivery_type']);
        $selectString   = $sql->getSqlStringForSqlObject($select);
        $result         = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $delivery_types = $result->toArray();

        $delivery_by_id = [];
        foreach($delivery_types as $delivery_type)
        {
            $delivery_by_id[$delivery_type['id']] = $delivery_type;
        }

        $select        = $sql->select()->from(['ZD' => 'nx_zone_delivery'])
            ->columns([
                'zone_id',
                'delivery_type_ids' => new Expression('GROUP_CONCAT(DISTINCT ZD.delivery_type_id)')
            ])
            ->join(['Z' => 'nx_zone'], 'ZD.zone_id = Z.id', ['zone_name' => 'name'])
            ->group('ZD.zone_id');
        $selectString  = $sql->getSqlStringForSqlObject($select);
        $result        = $db->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $zones         = $result->toArray();

        foreach($zones as $i => $zone)
        {
            $zones[$i]['delivery_type_ids'] = explode(',', $zone['delivery_type_ids']);

            foreach($zones[$i]['delivery_type_ids'] as $j => $delivery_type_id)
            {
                $zones[$i]['delivery_type'][$j] = ['id' => $delivery_type_id, 'name' => $delivery_by_id[$delivery_type_id]['name']];
            }
        }

        return $zones;
    }

    /*public function getZoneCountriesBy($params)
    {
        $join_map = array(//alias => table, condition, dependent_table
            //'U'  => array('User', 'TBL.create_user_id = U.User_ID'),
            //'U2' => array('User', 'TBL.last_user_id = U2.User_ID')
            'C' => array('Classificator_Country', 'TBL.country_id = C.Country_ID'),
            'Z' => array('nx_zone', 'TBL.zone_id = Z.id')
        );
        $columns_map = array(
            //'create_user_name' => array('joins' => array('U'), 'column' => 'IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)'),
            //'last_user_name'   => array('joins' => array('U2'), 'column' => 'IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')
            'zone_name'    => array('Z', 'name'),
            'country_id'   => array('C', 'Country_ID'),
            'country_name' => array('C', 'Country_Name'),

        );
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                    $select->where(array('TBL.id' => $filter['value']));
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, array(
            'table'       => 'nx_zone_country',
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        )));
        return $result;
    }

    public function getZoneCountryBy($params)
    {
        $params['current'] = true;
        return $this->getZoneCountriesBy($params);
    }

    public function addZoneCountry(array $data)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        if(empty($data['zone_id']) || empty($data['country_id']))
        {
            return false;
        }
        $values      = array('zone_id' => $data['zone_id'], 'country_id' => $data['country_id']);
        $query       = $sql->insert('nx_zone_country')->values($values);
        $queryString = $sql->getSqlStringForSqlObject($query);
        $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
        $insert_id   = $db->getDriver()->getLastGeneratedValue();
        return $insert_id;
    }

    public function updateZoneCountry($id, array $data)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        if(empty($data['zone_id']) || empty($data['country_id']))
        {
            return false;
        }
        $zone_country = $this->getZoneCountryBy(array('id' => $id));
        if(empty($zone_country['id']))
        {
            return false;
        }
        $values      = array('zone_id' => $data['zone_id'], 'country_id' => $data['country_id']);
        $query       = $sql->update('nx_zone_country')->set($values)->where(array('id' => $id));
        $queryString = $sql->getSqlStringForSqlObject($query);
        $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
        return $id;
    }

    public function deleteZoneCountry($id)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        $query       = $sql->delete('nx_zone_country')->where(array('id' => $id));
        $queryString = $sql->getSqlStringForSqlObject($query);
        $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
        return $result;
    }

    public function getZoneDeliveriesBy($params)
    {
        $join_map = array(
            //'U'  => array('User', 'TBL.create_user_id = U.User_ID'),
            //'U2' => array('User', 'TBL.last_user_id = U2.User_ID')
            'D' => array('nx_delivery_type', 'TBL.delivery_type_id = D.id'),
            'Z' => array('nx_zone', 'TBL.zone_id = Z.id')
        );
        $columns_map = array(
            //'create_user_name' => array('joins' => array('U'), 'column' => 'IF(U.LastName<>"", CONCAT_WS(" ", U.LastName, U.FirstName, U.MidName), U.Login)'),
            //'last_user_name'   => array('joins' => array('U2'), 'column' => 'IF(U2.LastName<>"", CONCAT_WS(" ", U2.LastName, U2.FirstName, U2.MidName), U2.Login)')
            'zone_name'          => array('Z', 'name'),
            'delivery_type_name' => array('D', 'name'),
            'delivery_names'     => array('column' => 'GROUP_CONCAT(D.name)', 'joins' => array('D'))
        );
        $group_map = array(
            'zone_id' => 'TBL.zone_id'
        );
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                    $select->where(array('TBL.id' => $filter['value']));
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, array(
            'table'       => 'nx_zone_delivery',
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'filter_func' => $filter_f,
            'group_map'   => $group_map,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.id DESC'
        )));
        return $result;
    }

    public function getZoneDeliveryBy($params)
    {
        $params['current'] = true;
        return $this->getZoneDeliveriesBy($params);
    }

    public function updateZoneDelivery($id, array $data)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        if(empty($data['zone_id']) || empty($data['delivery_type_id']))
        {
            return false;
        }
        $zone_delivery = $this->getZoneDeliveryBy(array('id' => $id));
        if(empty($zone_delivery['id']))
        {
            return false;
        }
        $values      = array('zone_id' => $data['zone_id'], 'delivery_type_id' => $data['delivery_type_id']);
        $query       = $sql->update('nx_zone_delivery')->set($values)->where(array('id' => $id));
        $queryString = $sql->getSqlStringForSqlObject($query);
        $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
        return $id;
    }*/

    public function addZone(array $data)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        $data['name'] = trim($data['name']);
        if(empty($data['name']))
        {
            return false;
        }

        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            $query       = $sql->insert('nx_zone')->values(['name' => $data['name']]);
            $queryString = $sql->getSqlStringForSqlObject($query);
            $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);
            $zone_id     = $db->getDriver()->getLastGeneratedValue();

            if($zone_id)
            {
                $zoneDeliveryModel = new \Zend\Db\TableGateway\TableGateway('nx_zone_delivery', $db);
                foreach($data['delivery_type_id'] as $delivery_type_id)
                {
                    $delivery_type_id = trim($delivery_type_id);
                    if(!empty($delivery_type_id))
                    {
                        $zoneDeliveryModel->insert(['zone_id' => $zone_id, 'delivery_type_id' => $delivery_type_id]);
                    }
                }

                $zoneCountryModel = new \Zend\Db\TableGateway\TableGateway('nx_zone_country', $db);
                foreach($data['country_id'] as $country_id)
                {
                    $country_id = trim($country_id);
                    if(!empty($country_id))
                    {
                        $zoneCountryModel->insert(['zone_id' => $zone_id, 'country_id' => $country_id]);
                    }
                }
            }

            $this->logger->log('Добавление зоны ' . $data['name'] . ' #' . $zone_id, 'add_zone', $data, !empty($data['create_user_id']) ? $data['create_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $zone_id;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }

    public function updateZone($id, array $data)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);
        $data['name'] = trim($data['name']);
        if(empty($data['name']))
        {
            return false;
        }
        $zone = $this->getZoneBy(['id' => $id]);
        if(empty($zone['id']))
        {
            return false;
        }

        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            $query       = $sql->update('nx_zone')->set(['name' => $data['name']])->where(['id' => $zone['id']]);
            $queryString = $sql->getSqlStringForSqlObject($query);
            $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);

            $zoneDeliveryModel = new \Zend\Db\TableGateway\TableGateway('nx_zone_delivery', $db);
            $deliveries = $zoneDeliveryModel->select(['zone_id' => $zone['id']]);
            $ids = [];
            if(!empty($deliveries))
            {
                foreach($deliveries as $delivery)
                {
                    $ids[] = $delivery['delivery_type_id'];
                }
            }
            $newids = array_diff($data['delivery_type_id'], $ids);
            $delids = array_diff($ids, $data['delivery_type_id']);
            if(!empty($newids))
            {
                foreach($newids as $newid)
                {
                    if(!empty($newid))
                    {
                        $zoneDeliveryModel->insert(['zone_id' => $zone['id'], 'delivery_type_id' => $newid]);
                    }
                }
            }
            if(!empty($delids))
            {
                foreach($delids as $delid)
                {
                    $zoneDeliveryModel->delete(['zone_id' => $zone['id'], 'delivery_type_id' => $delid]);
                }
            }

            $zoneCountryModel = new \Zend\Db\TableGateway\TableGateway('nx_zone_country', $db);
            $countries = $zoneCountryModel->select(['zone_id' => $zone['id']]);
            $ids = [];
            if(!empty($countries))
            {
                foreach($countries as $country)
                {
                    $ids[] = $country['country_id'];
                }
            }
            $newids = array_diff($data['country_id'], $ids);
            $delids = array_diff($ids, $data['country_id']);
            if(!empty($newids))
            {
                foreach($newids as $newid)
                {
                    if(!empty($newid))
                    {
                        $zoneCountryModel->insert(['zone_id' => $zone['id'], 'country_id' => $newid]);
                    }
                }
            }
            if(!empty($delids))
            {
                foreach($delids as $delid)
                {
                    $zoneCountryModel->delete(['zone_id' => $zone['id'], 'country_id' => $delid]);
                }
            }

            $this->logger->log('Обновление зоны ' . $data['name'] . ' #' . $zone['id'], 'update_zone', $data, !empty($data['last_user_id']) ? $data['last_user_id'] : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $zone['id'];
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }

    public function deleteZone($id, $user_id = 0)
    {
        $db  = $this->adapter;
        $sql = new Sql($db);

        $zone = $this->getZoneBy(['id' => $id]);
        if(empty($zone['id']))
        {
            return false;
        }

        $isTransactional = $this->isTransactional();
        if($isTransactional)
        {
            $this->adapter->getDriver()->getConnection()->beginTransaction();
        }
        try
        {
            $query       = $sql->delete('nx_zone_country')->where(['zone_id' => $id]);
            $queryString = $sql->getSqlStringForSqlObject($query);
            $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);

            $query       = $sql->delete('nx_zone_delivery')->where(['zone_id' => $id]);
            $queryString = $sql->getSqlStringForSqlObject($query);
            $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);

            $query       = $sql->delete('nx_price_item')->where(['zone_id' => $id]);
            $queryString = $sql->getSqlStringForSqlObject($query);
            $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);

            $query       = $sql->delete('nx_zone')->where(['id' => $id]);
            $queryString = $sql->getSqlStringForSqlObject($query);
            $result      = $db->query($queryString, $db::QUERY_MODE_EXECUTE);

            $this->logger->log('Удаление зоны #' . $id, 'delete_zone', $data, !empty($user_id) ? $user_id : 0);
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->commit();
            }
            return $result;
        }
        catch(\Exception $E)
        {
            if($isTransactional)
            {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
            throw $E;
        }
    }
}
