<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Issue extends AbstractTable
{
    public $currentIssuePrice;
    //public $nx_api_url;
    public function __construct(Adapter $adapter)
    {
        parent::__construct('Issue', $adapter);
    }

    public function getIssuesForCombo($keywords = '', $periodical_id)
    {
        $db     = $this->adapter;
        $sql    = new Sql($db);
        $select = $sql->select()
            ->from(['I'=>'Issue'])
            ->columns([
                'id'   => 'Message_ID',
                'name' => new Expression('CONCAT(M.EnglishName," ",I.issue_title)')
            ])
            ->join(['M' => 'Edition'],'M.Message_ID = I.MagazineID', [])
            ->join(['F' => 'aecms_file_storage'], 'F.issue_id = I.Message_ID', [])
        	->where(['M.Message_ID' => $periodical_id])
        	->where('F.file_type = 2')
			->where('F.isArchive = 1')
			->where('F.deleted   = 0')
			->where('I.status_id = 2')
        	->order(['I.year DESC', 'I.Number DESC'])->limit('150');
        if(!empty($keywords)) {
        	$select->where(['I.issue_title LIKE ?' => '%'.$keywords.'%']);
        }

        if ($this->publisher_id) {
            $select
            ->join(['PE'=>'publisher_edition'], 'PE.magazine_id = M.Message_ID', [])
            ->where(['PE.publisher_id' => $this->publisher_id]);
        } else {
            $select->where('0'); //не выводить ничего если не знаем какой издатель
        }


        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $result       = $result->toArray();
        return $result;
    }

    public function getIssue(array $params)
    {
		$db     = $this->adapter;
		$sql    = new Sql($db);
		$select = $sql->select()->from('Issue')->columns([
			'id'            => 'Message_ID',
			'periodical_id' => 'MagazineID'
        ])->limit(1);
        if(!empty($params['id'])) {
        	$select->where(['Message_ID' => $params['id']]);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $result       = $db->query($selectString, $db::QUERY_MODE_EXECUTE);
        $issue        = $result->current();
        return $issue;
    }

    public function getIssueBy($params)
    {
        $params['current'] = true;
        return $this->getIssuesBy($params);
    }

    public function getIssuesBy(array $params)
    {
        $join_map = [//alias => table, condition, dependent_table
            'M' => ['Edition', 'M.Message_ID = TBL.MagazineID']
        ];
        $columns_map = [
            'id'   => ['TBL', 'Message_ID'],
            //'name' => array('column' => 'CONCAT(M.EnglishName, " ", TBL.year)', 'joins' => array('M')),
            'periodical_name' => ['M', 'EnglishName'],
            'title' => ['TBL', 'issue_title']
        ];
        $sorter_f = function($sorter, &$select)
        {
            switch($sorter['property'])
            {
                case 'id':
                    $select->order('TBL.Message_ID' . ' ' . $sorter['direction']);
                    break;
            }
        };
        $filter_f = function($filter, &$select, &$joins)
        {
            switch($filter['property'])
            {
                case 'id':
                    $select->where(['TBL.Message_ID' => $filter['value']]);
                    break;
            }
        };
        $result = $this->getByMap(array_merge($params, [
            'join_map'    => $join_map,
            'columns_map' => $columns_map,
            'sorter_func' => $sorter_f,
            'filter_func' => $filter_f,
            'order'       => !empty($params['order']) ? $params['order'] : 'TBL.Message_ID DESC'
        ]));
        return $result;
    }
}
