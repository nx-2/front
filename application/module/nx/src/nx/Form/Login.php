<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Form;

use Zend\Form\Form;

class Login extends Form
{
    public function init()
    {
        $this->setMethod('post')
            ->loadDefaultDecorators()
            ->addDecorator('FormErrors');

        $this->addElement(
            'text',
            'username',
            [
                'filters' => [
                    ['StringTrim']
                ],
                'validators' => [
                    'EmailAddress'
                ],
                'required' => true,
                'label' => 'Email'
            ]
        );
        $this->addElement(
            'submit',
            'login',
            [
                'ignore' => true,
                'label' => 'Login'
            ]
        );
    }
}
