<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\functions;

// склонение счетных существительных
function getPlural($n, $nomSingular, $genSingular, $genPlural)
{
	switch(true)
	{
		case ($n % 10 == 1 && $n % 100 != 11):
			return $nomSingular;
			break;
		case ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20)):
			return $genSingular;
			break;
		default:
			return $genPlural;
	}
}

// отдает расшифровку числительных
function getNumberInWords($number, $male=true)
{
	$number      =(int)$number;
	$numUnits    = $number % 10;
	$numTens     = (int)(($number % 100 - $numUnits) / 10);
	$numHundreds = (int)(($number % 1000 - $numTens - $numUnits) / 100);

	$_UNITS_MALE   = ['', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'];
	$_UNITS_FEMALE = ['', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'];
	$_TENS         = ['', 'десять', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'];
	$_TENS_PLUS    = ['', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'];
	$_HUNDREDS     = ['', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот'];

	$numberInWords= [$_HUNDREDS[$numHundreds]];

	if($numTens==1 && $numUnits>0)
	{
		$numberInWords[]=$_TENS_PLUS[$numUnits];
	}
	else
	{
		$numberInWords[]=$_TENS[$numTens];
		if($male)
		{
			$numberInWords[]=$_UNITS_MALE[$numUnits];
		}
		else
		{
			$numberInWords[]=$_UNITS_FEMALE[$numUnits];
		}
	}

	return ' '.trim(implode(' ', $numberInWords));
}

// отдает расшифровку суммы в рублях
function getRoubleSumInWords($sum)
{
	// получить сумму копеек
	$kops = intval($sum * 100) % 100;

	// получить сумму рублей
	$sum = (int)$sum;

	if($sum==0 && $kops==0)
	{
		return 'ноль рублей';
	}

	$roubles   = $sum % 1000;
	$thousands = (int)(($sum % 1000000 - $roubles) / 1000);
	$millions  = (int)(($sum % 1000000000 - $thousands - $roubles) / 1000000);

	$result= [];

	if($millions)
	{
		$result[] = getNumberInWords($millions).' '.getPlural($millions, 'миллион', 'миллиона', 'миллионов');
	}

	if($thousands)
	{
		$result[] = getNumberInWords($thousands, false).' '.getPlural($thousands, 'тысяча', 'тысячи', 'тысяч');
	}

	if($roubles==0)
	{
		$result[] = 'рублей';
	}
	else
	{
		$result[] = getNumberInWords($roubles).' '.getPlural($roubles, 'рубль', 'рубля', 'рублей');
	}

	if($kops)
	{
		$result[] = getNumberInWords($kops, false).' '.getPlural($kops, 'копейка', 'копейки', 'копеек');
	}

	return trim(implode(' ', $result));
}

function getMonthsBetweenDates($startDate, $endDate)
{
    $retval = false;

    $splitStart = explode('-', $startDate);
    $splitEnd   = explode('-', $endDate);

    if (is_array($splitStart) && is_array($splitEnd))
    {
		$diffYears  = (int)$splitEnd[0] - (int)$splitStart[0];
		$diffMonths = (int)$splitEnd[1] - (int)$splitStart[1];
		$diffDays   = (int)$splitEnd[2] - (int)$splitStart[2];

        $retval = ($diffDays >= 0) ? $diffMonths : $diffMonths - 1;
        $retval += $diffYears * 12;
    }
    return $retval;
}

function isFloatEqual($x, $y, $precision=0.0000001)
{
	return (abs($x - $y) <= $precision);
}

function applyFunctionToData($data, $function)
{
	if(!is_array($data))
	{
		return $function($data);
	}
	$result = [];
	foreach($data as $i => $cur)
	{
		$result[$i] = applyFunctionToData($cur, $function);
	}
	return $result;
}

function isRunning($pid)
{
    try
    {
        $result = shell_exec(sprintf("ps %d", $pid));
        if( count(preg_split("/\n/", $result)) > 2){
            return true;
        }
    }
    catch(Exception $e){}

    return false;
}

function objectToArray($obj)
{
	if (is_object($obj)) $obj = get_object_vars($obj);
	if (is_array($obj)) return array_map(__FUNCTION__, $obj);
	else return $obj;
}

/**
 * Создание папки
 * @param string $path
 * @return boolean
 */
function makedir($path)
{
	if (!is_dir($path)) {
        $mask = umask(0);
        mkdir($path, 0777, True);
        umask($mask);
    }
    return is_dir($path);
}

function is_assoc($array) {
	return (bool)count(array_filter(array_keys($array), 'is_string'));
}
