<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class Log extends AbstractEntity
{
    public static $fields_names = [
        'id' => 'id',
        'title' => 'событие',
        'created' => 'дата',
        'user_id' => 'пользователь',
        'user_name' => 'пользователь',
        'data' => 'данные',
        'trace' => 'trace',
        'referer' => 'referer',
        'ip' => 'ip',
        'priority' => 'приоритет',
        'label' => 'метка'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            $result[$field] = $this->$field;
        }
        return $result;
    }
}
