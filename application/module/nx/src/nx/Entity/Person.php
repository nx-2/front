<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class Person extends AbstractEntity
{
    public static $fields_names = [
        'id' => 'id',
        'f' => 'фамилия',
        'i' => 'имя',
        'o' => 'отчество',
        'name' => 'фио',
        'gender' => 'пол',
        'phone' => 'телефон',
        'position' => 'должность',
        'speciality' => 'специализция',
        'company_id' => 'организация',
        'company_name' => 'организация',
        'email' => 'email',
        'comment' => 'комментарий',
        'address_id' => 'адрес',
        'checked' => 'проверено',
        'enabled' => 'включено',
        'country_id' => 'страна',
        'area' => 'область',
        'region' => 'район',
        'city' => 'город',
        'zipcode' => 'индекс',
        'address' => 'адрес',
        'address_phone' => 'телефон',
        'fax' => 'факс',
        'telcode' => 'тел. код',
        'address_comment' => 'комментарий',
        'xpress_id' => 'xpress id',
        'xpress_type' => 'xpress тип',
        'created' => 'создание',
        'create_user_id' => 'создал',
        'last_user_id' => 'обновил',
        'last_updated' => 'обновление',
        'create_user_name' => 'создал',
        'last_user_name' => 'обновил'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            $result[$field] = $this->$field;
        }
        return $result;
    }
}
