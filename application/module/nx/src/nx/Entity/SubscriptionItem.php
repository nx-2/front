<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class SubscriptionItem extends AbstractEntity
{
    public static $fields_names = [
        'xpress_id' => 'xpress_id',
        'id' => 'id',
        'order_id' => 'заказ',
        'email' => 'email',
        'order_type' => 'тип',
        'order_state_id' => 'состояние',
        'order_state_name' => 'состояние',
        'subscription_id' => 'подписка',
        'periodical_id' => 'издание',
        'periodical_name' => 'издание',
        'periodical_title' => 'издание',
        'price' => 'цена',
        'discount' => 'скидка',
        'payment_type_id' => 'способ оплаты',
        'payment_type_name' => 'способ оплаты',
        'payment_date' => 'дата оплаты заказа',
        'delivery_type_id' => 'способ доставки',
        'delivery_type_name' => 'способ доставки',
        'date_start' => 'дата начала',
        'date_end' => 'дата окончания',
        'period' => 'период',
        'byear' => 'год',
        'bnum' => 'выпуск',
        'rqty' => 'номеров',
        'type' => 'тип',
        'enabled' => 'включено',
        'quantity' => 'кол-во',
        'completed' => 'выполнено',
        'is_present' => 'актуально?',
        'order_label' => 'метка заказа',
        'order_price' => 'стоимость заказа',
        'order_date' => 'дата заказа/оплаты',
        'name' => 'подписчик',
        'person_id' => 'физ. лицо',
        'person_name' => 'физ. лицо',
        'person_id_name' => 'физ. лицо',
        'company_id' => 'организация',
        'company_name' => 'организация',
        'company_id_name' => 'организация',
        'company_inn' => 'ИНН юр. лица',
        'company_kpp' => 'КПП юр. лица',
        'subscriber_type_id' => 'тип подписчика',
        'created' => 'создание',
        'create_user_id' => 'создал',
        'last_user_id' => 'обновил',
        'last_updated' => 'обновление',
        'prolong_state' => 'статус продления',
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            $result[$field] = $this->$field;
        }
        return $result;
    }

    public function fillName()
    {
        $this->name = $this->subscriber_type_id == 2 ? $this->company_name . ' (' . $this->person_name . ')' : $this->person_name;
    }
}
