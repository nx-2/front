<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class Publisher extends AbstractEntity
{
    public static $fields_names = [
        'id' => '',
        'company_id' => 'id',
        'contact_person_id' => 'Контактное лицо',
        'logo' => 'Лого',
        'description' => 'Описание',
        'name' => 'Название',
        'distribution' => 'Распространение',
        'audience' => 'Аудитория',
        'official_email' => 'email для переписки',
        'created' => 'Создан',
        'last_updated' => 'Обновлен',
        'enabled' => 'Активен',
        'create_user_id' => 'Создан пользователем',
        'last_user_id' => 'Обновлен пользователем',
        '1c_integration_verified' => 'Проверка 1с интеграции',
        'last_verification_date' => 'Дата последней проверки',
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            $result[$field] = $this->$field;
        }
        return $result;
    }
}
