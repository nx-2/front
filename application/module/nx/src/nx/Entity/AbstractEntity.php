<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

use Zend\Stdlib\ArraySerializableInterface;

abstract class AbstractEntity implements ArraySerializableInterface
{
    public function toArray()
    {
        return $this->getArrayCopy();
    }
}