<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class Company extends AbstractEntity
{
    public static $fields_names = [
        'id' => 'id',
        'name' => 'название',
        'email' => 'email',
        'url' => 'адрес сайта',
        'org_form' => 'организационно-правовая форма',
        'shortname' => 'короткое название',
        'othernames' => 'другие названия',
        'wrongnames' => 'неправильные названия',
        'address_id' => 'адрес',
        'legal_address_id' => 'юридический адрес',
        'contact_person_id' => 'контактное лицо',
        'contact_person_name' => 'контактное лицо',
        'inn' => 'инн',
        'kpp' => 'кпп',
        'okonh' => 'оконх',
        'okpo' => 'окпо',
        'pay_account' => 'рассчетный счет',
        'corr_account' => 'корр счет',
        'bank' => 'банк',
        'bik' => 'бик',
        'description' => 'описание',
        'checked' => 'проверена',
        'enabled' => 'включена',
        'country_id' => 'страна',
        'area' => 'область',
        'region' => 'район',
        'city' => 'город',
        'zipcode' => 'индекс',
        'address' => 'адрес',
        'phone' => 'телефон',
        'fax' => 'факс',
        'telcode' => 'тел. код',
        'comment' => 'комментарий',
        'la_country_id' => 'страна',
        'la_area' => 'область',
        'la_region' => 'район',
        'la_city' => 'город',
        'la_zipcode' => 'индекс',
        'la_address' => 'адрес',
        'la_phone' => 'телефон',
        'la_fax' => 'факс',
        'la_telcode' => 'тел. код',
        'la_comment' => 'комментарий',
        'addr' => 'адрес',
        'laddr' => 'юридический адрес',
        'xpressid' => 'id в системе xpress',
        'created' => 'создание',
        'create_user_id' => 'создал',
        'last_user_id' => 'обновил',
        'last_updated' => 'обновление',
        'create_user_name' => 'создал',
        'last_user_name' => 'обновил',
        'ws1c_synced' => 'статус синхронизации с 1С'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            $result[$field] = $this->$field;
        }
        return $result;
    }
}
