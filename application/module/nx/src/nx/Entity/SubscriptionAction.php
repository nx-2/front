<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class SubscriptionAction extends AbstractEntity
{

    public static $fields = [
        'id' => ['id', 'Message_ID'],
        'create_user_id' => ['создал', 'User_ID'],
        'checked' => ['включена?', 'Checked'],
        'created' => ['создание', 'Created'],
        'name' => ['название', 'Name'],
        'text' => ['описание', 'Text'],
        'date_begin' => ['дана начала', 'DateBegin'],
        'date_finish' => ['дата конца', 'DateFinish'],
        'discount' => ['скидка', 'Discount'],
        'discount_table' => ['издания для проверки', 'DiscountTable'],
        'code' => ['код', 'Code'],
        'codes' => ['одноразовые коды', 'Codes'],
        'promocodes' => 'коды в md5',
        'email' => ['email менеджера', 'MailFrom'],
        'is_grouped' => ['скидка на комплект?', 'isGrouped'],
        'last_user_id' => ['обновил', 'LastUser_ID'],
        'last_updated' => ['обновление', 'LastUpdated'],
        'create_user_name' => 'создал',
        'last_user_name' => 'обновил',
        'count' => 'количество подписок по акции',
        'count_pay' => 'количество оплаченных подписок по акции',
        'page_id' => 'id страницы скидки'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields as $field => $info) {
            if (is_array($info)) {
                $this->$field = (isset($data[$field])) ? $data[$field] : (isset($data[$info[1]]) ? $data[$info[1]] : null);
            } else {
                $this->$field = (isset($data[$field])) ? $data[$field] : null;
            }
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields as $field => $info) {
            $result[$field] = $this->$field;
        }
        return $result;
    }

    public function prepare_from_db()
    {
        $this->discount_table = unserialize($this->discount_table);
        $this->codes = explode("\r\n", trim($this->codes));
        $this->fill_promocodes();
    }

    public function fill_promocodes()
    {
        $codes = [];
        if ($this->code || $this->codes) {
            if ($this->code) {
                $codes[] = md5($this->code);
            }
            if ($this->codes) {
                foreach ($this->codes as $codekey => $code) {
                    $codes[] = md5($code);
                }
            }
        }
        $this->promocodes = $codes;
    }
}
