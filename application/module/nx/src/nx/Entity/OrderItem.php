<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class OrderItem extends AbstractEntity
{
    public static $fields = [
        'xpress_id' => 'xpress_id',
        'id' => 'id',
        'order_id' => 'подписка',
        'item_id' => 'id позиции',
        'item_type' => 'тип позиции',
        'price' => 'цена',
        'discount' => 'скидка',
        'delivery_type_id' => 'способ оплаты',
        'delivery_type_name' => 'способ оплаты',
        'enabled' => 'включено',
        'quantity' => 'кол-во',
        'completed' => 'выполнено',
        'prolong_state' => 'статус продления',
        'created' => 'создание',
        'create_user_id' => 'создал',
        'last_user_id' => 'обновил',
        'last_updated' => 'обновление',
        //extra fields
        'full_price' => 'стоимость',
        'subscription_rqty' => 'количество выпусков по подписке',
        'item_data' => 'данные из таблицы товара',
        'order' => 'данные заказа'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields as $field => $name) {
            if ($this->$field !== null) {
                $result[$field] = $this->$field;
            }
        }
        return $result;
    }
}
