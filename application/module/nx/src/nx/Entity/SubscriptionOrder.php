<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class SubscriptionOrder extends AbstractEntity
{
    public static $fields_names = [
        'id' => 'id',
        'xpress_id' => 'xpress_id',
        'label' => 'метка',
        'channel_id' => 'канал',
        'price' => 'стоимость',
        'discount' => 'скидка',
        'payment_type_id' => 'способ оплаты',
        'payment_type_name' => 'способ оплаты',
        'payment_date' => 'дата оплаты',
        'payment_docno' => 'номер док. оплаты',
        'payment_agent_id' => 'платежный агент',
        'payment_agent_ws1c_id' => 'платежный агент в 1С',
        'payment_sum' => 'сумма оплаты',
        'comment' => 'комментарий',
        'address_comment' => 'комментарий',
        'source_id' => 'источник',
        'type' => 'тип',//['-1', 'рассылка'], ['0', 'платная'], ['1', 'призовая'], ['2', 'агентства']
        'enabled' => 'включено',
        //'checked'               => 'показывать',
        'order_state_id' => 'состояние',//['0', '-'], ['1', 'оформляется'], ['2', 'готов'], ['3', 'оплачено']
        'order_state_name' => 'состояние',
        'email' => 'email',
        'extra_emails' => 'доп. email',
        'address_id' => 'адрес',
        'map_address_id' => 'адрес для МАП',
        'person_id' => 'физ. лицо',
        'person_name' => 'физ. лицо',
        'person_id_name' => 'физ. лицо',
        'company_id' => 'организация',
        'company_name' => 'организация',
        'company_id_name' => 'организация',
        'company_inn' => 'ИНН юр. лица',
        'company_kpp' => 'КПП юр. лица',
        'user_id' => 'пользователь',
        'user_name' => 'пользователь',
        'subscriber_type_id' => 'тип подписчика',
        'key' => 'ключ',
        'manager_id' => 'менеджер',
        'reason' => 'основание',
        'created' => 'создание',
        'create_user_id' => 'создал',
        'last_user_id' => 'обновил',
        'last_updated' => 'обновление',
        'create_user_name' => 'создал',
        'last_user_name' => 'обновил',
        'is_present' => 'актуальна',
        'name' => 'подписчик',
        'periodical_names' => 'издания',
        'delivery_type_names' => 'способ доставки',
        'delivery_types' => 'способ доставки',
        'date' => 'дата',
        'date_start_sub' => 'дата начала',
        'country_id' => 'страна',
        'country_name' => 'страна',
        'area' => 'область',
        'region' => 'район',
        'city' => 'город',
        'city1' => 'населенный пункт',
        'zipcode' => 'индекс',
        'address' => 'адрес',
        'street' => 'улица',
        'house' => 'дом',
        'building' => 'корпус',
        'structure' => 'строение',
        'apartment' => 'квартира',
        'phone' => 'телефон',
        'fax' => 'факс',
        'telcode' => 'тел. код',
        'action_id_name' => 'акция',
        'action_id' => 'акция',
        'action_code' => 'промокод',
        'hash' => 'хеш',
        'map_area' => 'регион',
        'map_region' => 'район',
        'map_city' => 'город',
        'map_city1' => 'город1',
        'map_zipcode' => 'индекс',
        'map_street' => 'улица',
        'map_house' => 'дом',
        'map_building' => 'корпус',
        'map_structure' => 'строение',
        'map_apartment' => 'квартира',
        'map_is_onclaim' => 'до востребования',
        'map_post_box' => 'а/я',
        'map_is_enveloped' => 'конвертом',
        'map_address_comment' => 'комментарий',
        //'is_shipped'            => 'отгружено',
        'new_state' => 'новый заказ?',
        'full_map_address' => 'разобранный адрес',
        'map_address' => 'разобранный адрес',
        'order_item_id' => 'id подзаказа',
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            if ($this->$field !== null) {
                $result[$field] = $this->$field;
            }
        }
        return $result;
    }

    public function fillName()
    {
        $this->name = $this->subscriber_type_id == 2 ? $this->company_name . ' (' . $this->person_name . ')' : $this->person_name;
    }
}
