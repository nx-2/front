<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class Order extends AbstractEntity
{
    public static $fields = [
        'id' => 'id',
        'shop_id' => 'магазин',
        'channel_id' => 'канал',
        'price' => 'стоимость',
        'discount' => 'скидка',
        'payment_type_id' => 'способ оплаты',
        'payment_date' => 'дата оплаты',
        'payment_docno' => 'номер док. оплаты',
        'payment_agent_id' => 'платежный агент',
        'payment_sum' => 'сумма оплаты',
        'comment' => 'комментарий',
        'reason' => 'основание',
        'key' => 'ключ',
        'source_id' => 'источник',
        'type' => 'тип',
        'enabled' => 'включено',
        'manager_id' => 'менеджер',
        'http_referer' => 'http referer',
        'label' => 'метка',
        'epay_result' => 'онлайн оплата',
        'hash' => 'хеш',
        'order_state_id' => 'состояние',
        'action_id' => 'акция',
        'action_code' => 'промокод',
        'email' => 'email',
        'extra_emails' => 'доп. email',
        'address_id' => 'адрес',
        'map_address_id' => 'адрес по КЛАДР',
        'person_id' => 'физ. лицо',
        'company_id' => 'организация',
        'user_id' => 'пользователь',
        'subscriber_type_id' => 'тип подписчика',
        'created' => 'создание',
        'create_user_id' => 'создал',
        'last_user_id' => 'обновил',
        'last_updated' => 'обновление',
        'xpress_id' => 'xpress_id',
        'checked' => 'показывать',
        //extra fields
        'is_present' => 'актуальна',
        'name' => 'подписчик',
        'date' => 'дата',
        'person' => 'физ. лицо',
        'company' => 'юр. лицо'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields as $field => $name) {
            if ($this->$field !== null) {
                $result[$field] = $this->$field;
            }
        }
        return $result;
    }
}
