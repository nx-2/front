<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class Comment extends AbstractEntity
{
    public static $fields_names = [
        'id' => 'id',
        'comment' => 'комментарий',
        'entity_id' => 'id сущности',
        'row_id' => 'id объекта',
        'tag_id' => 'тег',
        'created' => 'создание',
        'create_user_id' => 'создал',
        'last_user_id' => 'обновил',
        'last_updated' => 'обновление',
        //extra fields
        'tag_name' => 'тег',
        'create_user_name' => 'создал',
        'last_user_name' => 'обновил',
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            if ($this->$field !== null) {
                $result[$field] = $this->$field;
            }
        }
        return $result;
    }
}
