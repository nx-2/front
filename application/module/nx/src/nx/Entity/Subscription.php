<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class Subscription extends AbstractEntity
{
    public static $fields_names = [
        'id' => 'id',
        'periodical_id' => 'издание',
        'periodical_name' => 'издание',
        'date_start' => 'дата начала',
        'date_end' => 'дата окончания',
        'period' => 'период',
        'byear' => 'год',
        'bnum' => 'выпуск',
        'rqty' => 'номеров',
        'type' => 'тип',
        'enabled' => 'включено',
        'created' => 'создание',
        'create_user_id' => 'создал',
        'last_user_id' => 'обновил',
        'last_updated' => 'обновление'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            $result[$field] = $this->$field;
        }
        return $result;
    }
}
