<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class Payment extends AbstractEntity
{
    public static $fields_names = [
        'id' => 'id',
        'order_id' => 'заказ',
        'transaction_id' => 'транзакция',
        'ws1c_id' => '1C id',
        'payment_agent_id' => 'организация',
        'payment_agent_name' => 'организация',
        'payment_agent_ws1c_id' => 'организация в 1С',
        'contractor_id' => 'контрагент',
        'contractor_name' => 'контрагент',
        'contractor_name_inn' => 'контрагент',
        'contractor_inn' => 'ИНН контрагента',
        'contractor_kpp' => 'КПП контрагента',
        'contractor_match_comment' => 'контрагент совпадает?',
        'docno' => 'номер поступления',
        'sum' => 'сумма',
        'date' => 'дата поступления',
        'year' => 'год платежа в 1с',
        'purpose' => 'назначение платежа',
        'comment' => 'комментарий',
        'status' => 'статус',
        'status_comment' => 'комментарий статуса',
        'result' => 'ответ сервиса',
        'result_comment' => 'комментарий',
        'request_data' => 'запрос',
        'enabled' => 'включен?',
        'label' => 'метка',
        'created' => 'создание',
        'create_user_id' => 'создал',
        'last_user_id' => 'обновил',
        'last_updated' => 'обновление',
        'create_user_name' => 'создал',
        'last_user_name' => 'обновил',
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            $result[$field] = $this->$field;
        }
        return $result;
    }
}
