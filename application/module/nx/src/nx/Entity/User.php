<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;
//use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="User")
 * @ORM\Entity
 */
class User extends AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="User_ID", type="integer")
     */
    //public $id;

    /** @ORM\Column(type="string") */
    /*public $email;
    public $login;
    public $full_name;
    public $last_name;
    public $first_name;
    public $mid_name;
    public $checked;
    public $publisher_id;
    public $roles;*/

    public static $fields = [
        'id' => ['id', 'User_ID'],
        'email' => ['email', 'Email'],
        'login' => ['логин', 'Login'],
        'full_name' => ['полное имя', 'FullName'],
        'last_name' => ['фамилия', 'LastName'],
        'first_name' => ['имя', 'FirstName'],
        'mid_name' => ['отчество', 'MidName'],
        'checked' => ['проверен', 'Checked'],
        'publisher_id' => ['Издатель', 'publisher_id'],
        'roles' => 'роли'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields as $field => $info) {
            if (is_array($info)) {
                $this->$field = (isset($data[$field])) ? $data[$field] : (isset($data[$info[1]]) ? $data[$info[1]] : null);
            } else {
                $this->$field = (isset($data[$field])) ? $data[$field] : null;
            }
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields as $field => $info) {
            $result[$field] = $this->$field;
        }
        return $result;
    }
}
