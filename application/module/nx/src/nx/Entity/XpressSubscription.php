<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class XpressSubscription extends AbstractEntity
{
    public static $fields_names = [
        'id' => 'id',
        'name' => 'подписчик',
        'total' => 'сумма',
        'payment_sum' => 'оплачено',
        'payment_date' => 'дата оплаты',
        'payment_docno' => 'номер док. оплаты',
        'payment_name' => 'способ оплаты',
        'is_payed' => 'оплачено',
        'delivery' => 'доставка',
        'delivery_name' => 'доставка',
        'periodical_names' => 'издания',
        'last_release' => 'последний выпуск',
        'is_present' => 'актуально',
        'person_name' => 'физ. лицо',
        'company_name' => 'организация',
        'person_email' => 'email',
        'company_email' => 'email',
        'ctime' => 'создано',
        'comment' => 'комментарий',
        'area' => 'область',
        'city' => 'город',
        'zipcode' => 'индекс',
        'address' => 'адрес',
        'manager' => 'менеджер'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            $result[$field] = $this->$field;
        }
        return $result;
    }
}
