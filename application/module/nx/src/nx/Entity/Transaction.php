<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class Transaction extends AbstractEntity
{

    public static $fields_names = [
        'id' => 'id',
        'transactionid' => 'заказ',
        'order_id' => 'заказ',
        'system' => 'платежная система',
        'sum' => 'сумма',
        'description' => 'описание',
        'person' => 'плательщик',
        'email' => 'email',
        'date' => 'дата создания',
        'sys_status' => 'статус ответа системы',
        'shop_date' => 'дата передачи магазину',
        'shop_status' => 'статус ответа магазина',
        'last_check' => '',
        'buh_status' => '',
        'shop' => 'магазин',
        'system_name' => 'платежная система',
        'system_desc' => 'платежная система',
        'system_inn' => 'инн платежной системы',
        'system_kpp' => 'кпп платежной системы'
    ];

    public function exchangeArray(array $data)
    {
        foreach (self::$fields_names as $field => $name) {
            $this->$field = (isset($data[$field])) ? $data[$field] : null;
        }
    }

    public function getArrayCopy()
    {
        $result = [];
        foreach (self::$fields_names as $field => $name) {
            $result[$field] = $this->$field;
        }
        return $result;
    }
}
