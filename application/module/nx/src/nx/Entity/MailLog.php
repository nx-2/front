<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Entity;

class MailLog extends AbstractEntity
{

    public $id;
    public $email;
    public $created;
    public $message;
    public $subject;
    public $from;
    public $mail_id;

    public function exchangeArray(array $data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->email = (isset($data['email'])) ? $data['email'] : null;
        $this->created = (isset($data['created'])) ? $data['created'] : null;
        $this->message = (isset($data['message'])) ? $data['message'] : null;
        $this->subject = (isset($data['subject'])) ? $data['subject'] : null;
        $this->from = (isset($data['from'])) ? $data['from'] : null;
        $this->mail_id = (isset($data['mail_id'])) ? $data['mail_id'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'created' => $this->created,
            'message' => $this->message,
            'subject' => $this->subject,
            'from' => $this->from,
            'mail_id' => $this->mail_id
        ];
    }
}
