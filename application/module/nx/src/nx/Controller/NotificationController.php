<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Feed\Writer\Feed;
use Zend\View\Model\FeedModel;

class NotificationController extends AbstractController
{
    const Model = 'nx\Model\Notification';

    private $configs;

    public function __construct(array $configs)
    {
        $this->configs = $configs;
    }

    public function indexAction()
    {
        $model   = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $model->getNotificationsBy([
                'calc_found_rows' => true,
                'offset'          => (int)$this->params()->fromQuery('start', 0),
                'limit'           => (int)$this->params()->fromQuery('limit', 25),
                'filters'         => $filters
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items']->toArray(),
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function rssAction()
    {
        $model = $this->getModel();
        $items = $model->getNotificationsBy([
                'limit' => 30,
            ]
        );
        //header('Content-Type: text/xml; charset=utf-8');
        //$manager = Writer::getExtensionManager();
        \Zend\Feed\Writer\Writer::reset();
        $feed = new Feed();
        $feed->setTitle('NX2 Системные сообщения');
        //$feed->setFeedLink('http://nx.osp.ru/notification/rss', 'rss');
        /*$feed->addAuthor(array(
            'name'  => 'admin',
            'email' => 'contact@ourdomain.com',
            'uri'   => 'http://www.ourdomain.com',
             ));*/
        $feed->setDescription('NX2 Системные сообщения');
        $link = $this->configs['schema'] . $this->configs['http_host'] . '/#notification/index';
        $feed->setLink($link);
        $feed->setDateModified(time());
        $feed->setLanguage('ru');
        //$feed->setGenerator(array());
        $last_mod_date = null;
//        if(!empty($items))
        if($items->count() > 0)
        {
            foreach($items as $row)
            {
                if(!$last_mod_date) {
                    $last_mod_date = $row->created;
                }

                $entry = $feed->createEntry();
                $entry->setTitle($row->message);
                $entry->setLink('http://nx.osp.ru/#notification/index/show/' . $row->id);
                $entry->setDescription($row->message);

                $entry->setDateModified(strtotime($row->created));
                $entry->setDateCreated(strtotime($row->created));

                $feed->addEntry($entry);
            }
            $feed->setDateModified(strtotime($last_mod_date));
        }

        $feed->export('rss');

        $feedmodel = new FeedModel();
        $feedmodel->setFeed($feed);

        return $feedmodel;
    }

    public function updateAction()
    {
        $dbModel = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'notificationData' => [
                'checked' => 'int'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');
        if(!empty($data) && $id)
        {
            $dbModel->updateNotification($data['notificationData'], ['id' => $id]);
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);
        return $viewModel;
    }
}
