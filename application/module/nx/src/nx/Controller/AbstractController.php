<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class AbstractController extends AbstractActionController
{
    public $user = [];

    public function getService()
    {//since php 5.3 late static binding
        return $this->getServiceLocator()->get(static::Service);
    }
    public function getModel()
    {
        return $this->getServiceLocator()->get(static::Model);
    }
    public function getJsonParams($name, $default, $source = 'GET')
    {
        $param = $this->params()->{'from' . ($source=='POST' ? 'Post' : 'Query')}($name, $default);
        if(!empty($param))
        {
            $param = json_decode($param);
            $param = \nx\functions\objectToArray($param);
        }
        return $param;
    }
    public function processPostFields($group_fields)
    {
        $post = $this->params()->fromPost();
        $data = [];
        foreach($group_fields as $group => $fields)
        {
            foreach($fields as $post_field => $type)
            {
                if(isset($post[$post_field]))
                {
                    $data_field = $post_field;
                    if(is_array($type))
                    {
                        $data_field = $type[0];
                        $type       = $type[1];
                    }
                    $data[$group][$data_field] = $post[$post_field];
                    if($type=='int')
                    {
                        $data[$group][$data_field] = (int)$data[$group][$data_field];
                    }
                }
            }
        }
        return $data;
    }
}
