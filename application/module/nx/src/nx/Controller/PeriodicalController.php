<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class PeriodicalController extends AbstractController
{

    public function indexAction()
    {
    	$sm              = $this->getServiceLocator();
        $periodicalModel = $sm->get('nx\Model\Periodical');
        $periodicals     = $periodicalModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25)
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $periodicals['items'],
            'total'   => $periodicals['total']
        ]);

        return $viewModel;
    }

}
