<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ZoneController extends AbstractController
{
    const Model = 'nx\Model\Zone';

    public function indexAction()
    {
        $model   = $this->getModel();
        $items   = $model->getZonesBy([
            'columns' => ['id','name'],
        ])->toArray();

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items,
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $model   = $this->getModel();
        $item    = $model->getZoneBy([
            'columns'      => ['id','name'],
            'columns_left' => ['country_ids', 'delivery_type_ids'],
            'id'           => (int)$this->params()->fromPost('id'),
            'group'        => 'TBL.id',
            //'debug' => 1
        ]);
        $item['country_id[]'] = explode(',', $item['country_ids']);
        $item['delivery_type_id[]'] = explode(',', $item['delivery_type_ids']);

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item,
        ]);

        return $viewModel;
    }

    public function saveAction()
    {
        $model = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'zoneData' => [
                'name'             => 'string',
                'country_id'       => 'array',
                'delivery_type_id' => 'array',
            ],
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id', 0);

        if(!empty($id))
        {
            $data['zoneData']['last_user_id'] = $user_session->user_id;
            $model->updateZone($id, $data['zoneData']);
        }
        else
        {
            $data['zoneData']['create_user_id'] = $user_session->user_id;
            $model->addZone($data['zoneData']);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function deleteAction()
    {
        $user_session = new \Zend\Session\Container('user');

        $model = $this->getModel();
        $id    = $this->params()->fromPost('id', 0);

        if($id) {
            $result = $model->deleteZone($id, $user_session->user_id);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    /*public function zoneDeliveryAction()
    {
        $model   = $this->getModel();
        $items   = $model->getZoneDeliveriesBy(array(
            'columns' => array('id', 'zone_name', 'delivery_type_name'),
            //'columns' => array('id', 'zone_name', 'delivery_names'),
            //'group'   => 'zone_id'
        ))->toArray();
        //$sm = $this->getServiceLocator();
        //$subscriptionModel = $sm->get('nx\Model\Subscription');
        //$delivery_types = $subscriptionModel->getDeliveryTypes(true);

        $viewModel =  new JsonModel(array(
            'success' => true,
            'items'   => $items,
        ));

        return $viewModel;
    }

    public function zoneDeliveryGetAction()
    {
        $model   = $this->getModel();
        $item    = $model->getZoneDeliveryBy(array(
            'columns' => array('id', 'zone_id', 'delivery_type_id'),
            'id'      => (int)$this->params()->fromPost('id')
        ));
        $item['zone_id'] = (int)$item['zone_id'];
        //$item['delivery_type_id[]'] = explode(',', $item['delivery_type_id']);
        //$item['delivery_type_id'] = explode(',', $item['delivery_type_id']);

        $viewModel =  new JsonModel(array(
            'success' => true,
            'data'    => $item,
        ));

        return $viewModel;
    }

    public function zoneDeliverySaveAction()
    {
        $model = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = array(
            'zonedeliveryData' => array(
                'zone_id'          => 'int',
                'delivery_type_id' => 'array',
            ),
        );

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id', 0);

        if(!empty($id))
        {
            $data['zonedeliveryData']['last_user_id'] = $user_session->user_id;
            $model->updateZoneDelivery($id, $data['zonedeliveryData']);
        }

        $viewModel =  new JsonModel(array(
            'success' => true,
        ));

        return $viewModel;
    }

    public function zoneCountryAction()
    {
        $model   = $this->getModel();
        $items   = $model->getZoneCountriesBy(array(
            'columns' => array('id', 'zone_name', 'country_name'),
        ));

        $viewModel =  new JsonModel(array(
            'success' => true,
            'items'   => $items->toArray(),
        ));

        return $viewModel;
    }

    public function getZonesForComboAction()
    {
        $model = $this->getModel();

        $viewModel =  new JsonModel(array(
            'success' => true,
            'objects' => $model->getZonesBy(array('columns' => array('id', 'name')))->toArray()
        ));

        return $viewModel;
    }

    public function zoneCountrySaveAction()
    {
        $model = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = array(
            'zonecountryData' => array(
                'zone_id'    => 'int',
                'country_id' => 'int',
            ),
        );

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id', 0);

        if(!empty($id))
        {
            $data['zonecountryData']['last_user_id'] = $user_session->user_id;
            $model->updateZoneCountry($id, $data['zonecountryData']);
        }
        else
        {
            $data['zonecountryData']['create_user_id'] = $user_session->user_id;
            $model->addZoneCountry($data['zonecountryData']);
        }

        $viewModel =  new JsonModel(array(
            'success' => true,
        ));

        return $viewModel;
    }

    public function zoneCountryGetAction()
    {
        $model   = $this->getModel();
        $item    = $model->getZoneCountryBy(array(
            'columns' => array('id', 'zone_id', 'country_id'),
            'id'      => (int)$this->params()->fromPost('id')
        ));
        $item['zone_id'] = (int)$item['zone_id'];

        $viewModel =  new JsonModel(array(
            'success' => true,
            'data'    => $item,
        ));

        return $viewModel;
    }

    public function zoneCountryDeleteAction()
    {
        $model = $this->getModel();
        $id    = $this->params()->fromPost('id', 0);;

        if($id) {
            $result = $model->deleteZoneCountry($id);
        }

        $viewModel =  new JsonModel(array(
            'success' => true,
        ));

        return $viewModel;
    }*/
}
