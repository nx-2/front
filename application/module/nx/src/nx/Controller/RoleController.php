<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class RoleController extends AbstractController
{
    const Model = 'nx\Model\Role';

    public function indexAction()
    {
    	$dbModel = $this->getModel();
        $items   = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25)
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items']->count() ? $items['items']->toArray() : [],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $dbModel = $this->getModel();
        $item    = $dbModel->getByID((int)$this->params()->fromPost('id'));

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item->toArray(),
        ]);

        return $viewModel;
    }

    public function addAction()
    {
        $dbModel = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'roleData' => [
                'name' => 'string',
                'code' => 'string'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        if(!empty($data['roleData']))
        {
            $data['roleData']['create_user_id'] = $user_session->user_id;
            $dbModel->addRole($data);
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function updateAction()
    {
        $dbModel = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'roleData' => [
                'name' => 'string',
                'code' => 'string'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');
        if(!empty($data) && $id)
        {
            $data['roleData']['last_user_id'] = $user_session->user_id;
            $dbModel->updateRole($id, $data);
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }
}
