<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class FileStorageController extends AbstractController
{
    const Model = 'nx\Model\FileStorage';

    public function indexAction()
    {
        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items']->count() ? $items['items']->toArray() : [],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function downloadAction()
    {
        $dbModel = $this->getModel();
        $id      = $this->params()->fromRoute('id', 0);
        $hash    = $this->params()->fromRoute('hash', '');

        if($id || $hash)
        {
            $file_info = $dbModel->getBy(['id' => $id, 'hash' => $hash]);
            if(!empty($file_info))
            {
                $file_info = $file_info->toArray();
            }
            if(!empty($file_info['path']) && file_exists($file_info['path']))
            {
                $file = $file_info['path'];

                $ext = pathinfo($file, PATHINFO_EXTENSION);

                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime  = finfo_file($finfo, $file);
                $mime  = $mime ? $mime : 'application/octet-stream';
                finfo_close($finfo);

                $inline_mime = [];//'application/pdf');

                header('Content-Transfer-Encoding: binary');
                header('Content-type: ' . $mime);
                header('Content-Length: ' . filesize($file));
                if(in_array($mime, $inline_mime))
                {
                    header('Content-Disposition: inline; filename="' . $file_info['name'] . '"');
                    header('Accept-Ranges: bytes');
                }
                else
                {
                    header('Content-Description: File Transfer');
                    header('Pragma: public');
                    header('Content-Disposition: attachment; filename="' . $file_info['name'] . '"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                }
                ob_clean();
                flush();
                readfile($file);
                die;
            }
            else
            {
                die('файл не найден');
            }
        }
    }

    public function createZipAction()
    {
        $dbModel = $this->getModel();
        $ids     = $this->params()->fromPost('ids', '');
        $link    = '';
        if(!empty($ids))
        {
            $ids = explode(',', $ids);
            $zip = $dbModel->createZipAttachment($ids);
            //$link = $zip['path'];
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $zip
        ]);

        return $viewModel;
    }
}
