<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use api;
use Exception;
use nx\Service\PublishAdmin;
use nx\Service\Ws1c;
use nx\ws1c\Controller;
use Zend\Db\Sql\Sql;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Soap\AutoDiscover;
use Zend\Soap\Client;
use Zend\Soap\Server;
use Zend\Soap\Server\DocumentLiteralWrapper;
use Zend\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

//use nx\api as api;
class IndexController extends AbstractController
{
    public function indexAction()
    {
        $sm = $this->getServiceLocator();

        $this->layout('layout/ext-layout');
        $viewModel = new ViewModel([
            'sm' => $sm
        ]);
        $viewModel->setTemplate('nx/index/ext-index.phtml');
        return $viewModel;
    }

    public function testAction()
    {
        $sm = $this->getServiceLocator();
        $config = $sm->get('Config');
        /** @var PublishAdmin $publishAdminService */
        $publishAdminService = $sm->get('nx\Service\PublishAdmin');
        $token = $publishAdminService->buildAuthToken();

        $client = new Client('http://' . $config['nx']['http_host'] . '/ws1c?wsdl', [
            'soap_version' => SOAP_1_1,
            'login' => 'ws1c',
            'password' => 'someString',
            'cache_wsdl' => WSDL_CACHE_NONE,
        ]);
        try {
            var_dump($client->getLastResponse());
            var_dump($client->getLastRequest());
            var_dump($result);
        } catch (Exception $e) {
            var_dump($e->getMessage());
            var_dump($client->getLastResponse());
            var_dump($client->getLastRequest());
        }

        die('1');
    }

    public function apiAction()
    {
        $sm = $this->getServiceLocator();
        $cfg = $sm->get('config');

        $keys = [$cfg['nx']['passport_service']['token']];

        $allow = 0;
        foreach ($keys as $key) {
            if ($this->getRequest()->getPost('apikey') == md5($key)) {
                $allow = 1;
            }
        }

        if ($allow) {
            require_once(dirname(__DIR__) . '/api.php');
            $method = $this->getRequest()->getPost('method', '');
            if ($method) {
                $params = $this->getRequest()->getPost('params', '');
                $api = new api($this->getServiceLocator());
                try {
                    $result = $api->{$method}($params);
                } catch (Exception $e) {
                    $sm->get('nx\Model\Log')
                        ->log(
                            'api request error',
                            'api_request_error',
                            [
                                'request' => [
                                    'method' => $method,
                                    'params' => $params
                                ],
                                'response' => $e->getMessage(),
                                'trace' => $e->__toString()
                            ],
                            0
                        );
                    throw $e;
                }
            }

            $viewModel = new JsonModel([
                'success' => true,
                'result' => $result
            ]);

            return $viewModel;

        }

        return $this->getResponse()->setContent('');
    }

    public function ws1cAction()
    {
        $sm = $this->getServiceLocator();
        $config = $sm->get('Config');
        require_once('module/nx/src/nx/ws1c.php');

        if (isset($_GET['wsdl'])) {
            $autodiscover = new AutoDiscover(new ArrayOfTypeSequence());
            $autodiscover
                ->setClass(new Controller())
                ->setUri($config['nx']['schema'] . $config['nx']['http_host'] . '/ws1c')
                ->setServiceName('nx_ws1c');
            $autodiscover
                ->setBindingStyle(['style' => 'document'])
                ->setOperationBodyStyle(['use' => 'literal']);
            $wsdl = $autodiscover->generate();
            header('Content-type: application/xml');
            $xml = $wsdl->toXml();
            $xml = str_replace('<xsd:schema', '<xsd:schema attributeFormDefault="unqualified" elementFormDefault="qualified"', $xml);

            echo $xml;
        } else {
            $allowed = [
                'ws1c' => 'someString'
            ];

            $allow = 0;
            foreach ($allowed as $user => $password) {
                if (
                    $this->getRequest()->getServer('PHP_AUTH_USER') == $user &&
                    $this->getRequest()->getServer('PHP_AUTH_PW') == $password
                ) {
                    $allow = 1;
                }
            }
            if (!$allow) {
                header('WWW-Authenticate: Basic realm="My Realm"');
                header('HTTP/1.0 401 Unauthorized');
                echo 'wrong auth';
                exit;
            } else {
                try {
                    $server = new Server($config['nx']['schema'] . $config['nx']['http_host'] . '/ws1c?wsdl=2', [
                        'uri' => $config['nx']['schema'] . $config['nx']['http_host'] . '/ws1c',
                        'soap_version' => SOAP_1_1,
                        'cache_wsdl' => WSDL_CACHE_NONE,
                    ]);
                    $server->setWSDLCache(false);
                    $server->setObject(new DocumentLiteralWrapper(new Controller($sm)));
                    $server->setReturnResponse(true);
                    $server->handle();
                    $request = $server->getLastRequest();
                    $response = $server->getResponse();

                    $sm->get('nx\Model\Log')->log('ws1c запрос', 'ws1c_request', ['request' => $request, 'response' => $response], 0);

                    echo $response;
                } catch (SOAPFault $f) {
                    echo $f->faultstring;
                }
            }
        }
        return $this->getResponse();
    }

    public function exportDataAction()
    {
        $data = $this->params()->fromPost('data', '');
        if ($data) {
            $filename = $this->params()->fromPost('filename', 'Экспорт');

            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=nx_" . $filename . ".csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $data;
        }
        die;
    }

    public function getUrlContentAction()
    {
        $url = $this->params()->fromPost('url', '');

        $content = '';
        if ($url) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $content = curl_exec($ch);
            curl_close($ch);
        }

        $viewModel = new JsonModel([
            'success' => true,
            'data' => $content,
        ]);

        return $viewModel;
    }

    public function check1CAction()
    {
        $sm = $this->getServiceLocator();
        $ws1c = new Ws1c(3);

        $viewModel = new JsonModel([
            'success' => true,
            'check' => $ws1c->client ? true : false,
        ]);

        return $viewModel;
    }
}
