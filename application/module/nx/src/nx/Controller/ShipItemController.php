<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ShipItemController extends AbstractController
{
    const Model = 'nx\Model\Shipment';

    public function reclamationAction()
    {
        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $dbModel->getShipmentItemsBy(
            [
                // 'columns' => array('id', 'shipment_created', 'channel_name', 'release_date', 'issue_name', 'cancel_date', 'create_user_name', 'cancel_user_name',
                //     'item_release_id', 'order_id', 'periodical_name', 'issue_title', 'email'
                // ),
                // 'columns_left' => array('zipcode', 'customer_name'),
                'columns' => ['id', 'cancel_date', 'item_release_id'],
                'columns_apply'   => [
                    'left' => ['customer_name', 'zipcode'],
                    ['shipment_created', 'channel_name', 'release_date', 'issue_name', 'create_user_name', 'cancel_user_name', 'order_id', 'periodical_name', 'issue_title', 'email']
                ],
                'calc_found_rows' => true,
                'offset'          => (int)$this->params()->fromQuery('start', 0),
                'limit'           => (int)$this->params()->fromQuery('limit', 25),
                'is_reclamation'  => 1,
                'filters'         => $filters,
                //'debug' => 1
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items'],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

}
