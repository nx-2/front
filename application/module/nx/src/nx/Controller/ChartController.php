<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ChartController extends AbstractController
{
    const Service = 'nx\Service\Chart';

    public function indexAction()
    {
        $service = $this->getService();

        $items = $service->mapper->getCharts();

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items->count() ? $items->toArray() : [],
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $service = $this->getService();
        $item    = $service->mapper->getChartBy(['id' => (int)$this->params()->fromPost('id')]);

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item->toArray(),
        ]);

        return $viewModel;
    }

    public function loadAction()
    {
        $service = $this->getService();
        $item    = $service->mapper->loadChartBy(['id' => (int)$this->params()->fromPost('id')]);

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item->toArray(),
        ]);

        return $viewModel;
    }

    public function updateAction()
    {
        $service = $this->getService();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'chartData' => [
                'name'  => 'string',
                'query' => 'text',
                'type'  => 'int'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');
        if(!empty($data) && $id)
        {
            $data['chartData']['last_user_id'] = $user_session->user_id;
            $service->mapper->updateChart($id, $data);
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function addAction()
    {
        $service = $this->getService();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'chartData' => [
                'name'  => 'string',
                'query' => 'text',
                'type'  => 'int'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        if(!empty($data['chartData']))
        {
            $data['chartData']['create_user_id'] = $user_session->user_id;
            $service->mapper->addChart($data);
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function deleteAction()
    {
        $service = $this->getService();
        $id      = $this->params()->fromPost('id', 0);;

        if($id) {
            $result = $service->mapper->delete(['id' => $id]);
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result
        ]);

        return $viewModel;
    }
}
