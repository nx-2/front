<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
//use Zend\Db\Adapter\Adapter;

use nx\Model\User;
    //Zend\Session\Container as SessionContainer,
    //Zend\Permissions\Acl\Acl,
    //Zend\Permissions\Acl\Role\GenericRole as Role,
    //Zend\Permissions\Acl\Resource\GenericResource as Resource;

//use Zend\ServiceManager\ServiceManagerAwareInterface;
//use Zend\ServiceManager\ServiceManager;
class AuthPlugin extends AbstractPlugin
{
    /*protected $sesscontainer ;

    private function getSessContainer()
    {
        if (!$this->sesscontainer) {
            $this->sesscontainer = new SessionContainer('zftutorial');
        }
        return $this->sesscontainer;
    }*/

    public function doAuthorization($e)
    {
        //session_start();

        //$session_id = '';
        //if(!empty($_COOKIE['PHP_AUTH_SID']))
        //{
            //$session_id = $_COOKIE['PHP_AUTH_SID'];//netcat session
        //}
        //if(empty($session_id))
        //{
        //$session_id = session_id();//self session
        //}
        //Zend_Registry :: set ('sessionId', $sessionId);

        //var_dump($e);
        $application    = $e->getApplication();
        $sm             = $application->getServiceManager();
        //$db             = $sm->get('nx\Db\Adapter');
        //$request        = $application->getRequest();
        //$router         = $sm->get('router');
        $sessionManager = $sm->get('Zend\Session\SessionManager');
        $session_id     = $sessionManager->getId();

        $controller     = $e->getTarget();
        $controllerName = $e->getRouteMatch()->getParam('controller');//$router->params('controller');
        $actionName     = $e->getRouteMatch()->getParam('action');
        //$controllerName = $request->getControllerName();
        //var_dump($controllerName,$router,$e->getRouteMatch());die;

        $allowedControllers = ['nx\Controller\Login', 'nx\Controller\Index', 'nx\Controller\Notification'];//allow index controller because of soap/rest/api
        $allowedActions     = [
            'nx\Controller\Index' => ['api', 'ws1c', 'test'],
            'nx\Controller\Notification' => ['rss']
        ];

        $user_model    = $sm->get('nx\Model\User');
        $session_model = $sm->get('nx\Model\Session');

        $user    = [];
        $session = $session_model->select(['Session_ID' => $session_id])->current();

        if(!empty($session)) {
            $user = $user_model->getByID($session['User_ID']);
        }
        $isAdmin = 0;
        if(!empty($user)) {
            $session = new \Zend\Session\Container('user');
            $session->user_id = $user->id;
            $session->user    = $user->toArray();
            $session_time     = time() + 24 * 3600;// продлеваем
            $session_model->update(['SessionTime' => $session_time, 'Catalogue_ID' => 0], ['Session_ID' => $session['Session_ID']]);

            $allowedActions = [
                'nx\Controller\User'    => ['get-objects-for-combo'],
                'nx\Controller\MailLog' => ['send-mail-message'],
                'nx\Controller\Tag'     => ['get-objects-for-combo'],
            ];
            $allowedControllers = array_merge($allowedControllers, [
                'nx\Controller\ViewState',
                'nx\Controller\FileStorage',
                'nx\Controller\Subscription',
                'nx\Controller\Periodical',
                'nx\Controller\Address',
                'nx\Controller\Company',
                'nx\Controller\Person',
                'nx\Controller\Order',
                'nx\Controller\OrderItem',
                'nx\Controller\User',
                'nx\Controller\Ship',
                'nx\Controller\ShipItem',
                'nx\Controller\Action',
                'nx\Controller\MailLog',
                'nx\Controller\Tag',
                'nx\Controller\Comment',
            ]);
            $perm = $sm->get('nx\Perm');
            if($perm->hasRole(User::ROLE_ACCOUNTANT)) {
                $allowedControllers[] = 'nx\Controller\Transaction';
                $allowedControllers[] = 'nx\Controller\Payment';
            }
            if($perm->hasRole(User::ROLE_SUBSCRIBE_MANAGER)) {
                $allowedControllers = array_merge($allowedControllers, [
                    'nx\Controller\Transaction',
                    'nx\Controller\Payment',
                    'nx\Controller\Price',
                ]);
            }
            $isAdmin = $perm->hasRole(User::ROLE_ADMIN) || $perm->hasRole(User::ROLE_PUBLISHER);
            if($isAdmin) {
                $allowedActions = []; //empty means all allowed
                $allowedControllers = []; //empty means all allowed
            }
            $controller->user = $user->toArray();
        }

        if((!empty($allowedControllers) && !in_array($controllerName, $allowedControllers)) || (!empty($allowedActions[$controllerName]) && !in_array($actionName, $allowedActions[$controllerName])))
        {
            if($e->getRequest()->isXmlHttpRequest())
            {
                $response = [
                    'success' => True,
                    'data'    => []
                ];
                if(!empty($user))
                {
                    $response['systemMessage'] = [
                        'title'   => 'Ошибка доступа.',
                        'message' => 'Ваши действия запротоколированы.',
                        'icon'    => 'login-big-ico'
                    ];
                }
                else
                {
                    $response['redirect'] = '/login';
                }
                echo json_encode($response);
                exit;
            }
            else
            {
                $controller->plugin('redirect')->toUrl('/login');
            }
            $e->stopPropagation();
        }

        //setcookie('PHP_AUTH_SID',  $session_id,  $session_time, "/", 'aecms.ru');
        //setcookie('PHP_AUTH_SID',  $session_id,  $session_time, "/", 'osp.ru');
        $controller->session_id = $session_id;
        //$user = $db->query('SELECT * FROM User WHERE Login = "pechorin"', Adapter::QUERY_MODE_EXECUTE)->current();
        //var_dump($user);

        //$this->redirect()->toRoute('index');
        //header('Location: /login', 301);
        //die;
        /*//setting ACL...
        $acl = new Acl();
        //add role ..
        $acl->addRole(new Role('anonymous'));
        $acl->addRole(new Role('user'),  'anonymous');
        $acl->addRole(new Role('admin'), 'user');

        $acl->addResource(new Resource('nx'));
        $acl->addResource(new Resource('Login'));

        $acl->deny('anonymous', 'nx', 'view');
        $acl->allow('anonymous', 'Login', 'view');

        $acl->allow('user',
            array('nx'),
            array('view')
        );

        //admin is child of user, can publish, edit, and view too !
        $acl->allow('admin',
            array('nx'),
            array('publish', 'edit')
        );

        $controller = $e->getTarget();
        $controllerClass = get_class($controller);
        $namespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));

        $role = (! $this->getSessContainer()->role ) ? 'anonymous' : $this->getSessContainer()->role;
        if ( ! $acl->isAllowed($role, $namespace, 'view'))
        {
            $router = $e->getRouter();
            $url    = $router->assemble(array(), array('name' => 'Login/auth'));

            $response = $e->getResponse();
            $response->setStatusCode(302);
            //redirect to login route...

            $response->getHeaders()->addHeaderLine('Location', $url);
            $e->stopPropagation();
        }
        */
    }
}
