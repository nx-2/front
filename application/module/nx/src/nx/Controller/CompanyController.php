<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class CompanyController extends AbstractController
{
    const Model = 'nx\Model\Company';
    const Service = 'nx\Service\Company';

    public function indexAction()
    {
        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters,
                'ids' => json_decode($this->params()->fromQuery('json_ids', '')),
            ]
        );

        $viewModel = new JsonModel([
            'success' => true,
            'items' => $items['items']->count() ? $items['items']->toArray() : [],
            'total' => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $dbModel = $this->getModel();
        $item = $dbModel->getByID((int)$this->params()->fromPost('id'));

        $this->layout('layout/ajax-layout');
        $viewModel = new JsonModel([
            'success' => true,
            'data' => $item,
        ]);

        return $viewModel;
    }

    public function updateAction()
    {
        $dbModel = $this->getModel();
        $user_session = new Container('user');

        $group_fields = [
            'companyData' => [
                'checked' => 'int',
                'enabled' => 'int',
                'name' => 'string',
                'shortname' => 'string',
                'othernames' => 'string',
                'wrongnames' => 'string',
                'url' => 'string',
                'org_form' => 'string',
                'email' => 'email',
                'contact_person_id' => 'int',
                'description' => 'text',
                'inn' => 'string',
                'kpp' => 'string',
                'okonh' => 'string',
                'okpo' => 'string',
                'bank' => 'string',
                'bik' => 'string',
                'pay_account' => 'string',
                'corr_account' => 'string'
            ],
            'addressData' => [
                'country_id' => 'int',
                'address' => 'string',
                'fax' => 'string',
                'zipcode' => 'string',
                'phone' => 'string',
                'telcode' => 'string',
                'region' => 'string',
                'area' => 'string',
                'city' => 'string',
                'comment' => 'string'
            ],
            'legalAddressData' => [
                'la_country_id' => ['country_id', 'int'],
                'la_address' => ['address', 'string'],
                'la_fax' => ['fax', 'string'],
                'la_zipcode' => ['zipcode', 'string'],
                'la_phone' => ['phone', 'string'],
                'la_telcode' => ['telcode', 'string'],
                'la_region' => ['region', 'string'],
                'la_area' => ['area', 'string'],
                'la_city' => ['city', 'string'],
                'la_comment' => ['comment', 'string']
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');
        if (!empty($data) && $id) {
            $data['companyData']['last_user_id'] = $user_session->user_id;
            $dbModel->updateCompany($id, $data);
        }

        $this->layout('layout/ajax-layout');
        $viewModel = new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function addAction()
    {
        $dbModel = $this->getModel();
        $user_session = new Container('user');

        $group_fields = [
            'companyData' => [
                'checked' => 'int',
                'name' => 'string',
                'shortname' => 'string',
                'othernames' => 'string',
                'wrongnames' => 'string',
                'url' => 'string',
                'org_form' => 'string',
                'email' => 'email',
                'contact_person_id' => 'int',
                'description' => 'text',
                'inn' => 'string',
                'kpp' => 'string',
                'okonh' => 'string',
                'okpo' => 'string',
                'bank' => 'string',
                'bik' => 'string',
                'pay_account' => 'string',
                'corr_account' => 'string'
            ],
            'addressData' => [
                'country_id' => 'int',
                'address' => 'string',
                'fax' => 'string',
                'zipcode' => 'string',
                'phone' => 'string',
                'telcode' => 'string',
                'region' => 'string',
                'area' => 'string',
                'city' => 'string',
                'comment' => 'string'
            ],
            'legalAddressData' => [
                'la_country_id' => ['country_id', 'int'],
                'la_address' => ['address', 'string'],
                'la_fax' => ['fax', 'string'],
                'la_zipcode' => ['zipcode', 'string'],
                'la_phone' => ['phone', 'string'],
                'la_telcode' => ['telcode', 'string'],
                'la_region' => ['region', 'string'],
                'la_area' => ['area', 'string'],
                'la_city' => ['city', 'string'],
                'la_comment' => ['comment', 'string']
            ]
        ];

        $data = $this->processPostFields($group_fields);

        if (!empty($data['companyData']['name'])) {
            $data['companyData']['label'] = 'nx_company';
            $data['companyData']['create_user_id'] = $user_session->user_id;
            $dbModel->addCompany($data);
        }

        $this->layout('layout/ajax-layout');
        $viewModel = new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function getObjectsForComboAction()
    {
        $dbModel = $this->getModel();

        $viewModel = new JsonModel([
            'success' => true,
            'objects' => $dbModel->getObjectsForCombo($this->params()->fromQuery('query', ''))
        ]);

        return $viewModel;
    }

    public function ws1cAction()
    {
        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');

        $items = $dbModel->getWs1cList(['filters' => $filters]);

        $viewModel = new JsonModel([
            'success' => true,
            'items' => $items,
        ]);

        return $viewModel;
    }

    public function reWs1cAction()
    {
        $dbModel = $this->getModel();

        $dbModel->reWs1cList();

        $viewModel = new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function sendWs1cAction()
    {
        $service = $this->getService();
        $ids = $this->params()->fromPost('ids', '');
        $ids = explode(',', $ids);
        $result = [];
        if (!empty($ids)) {
            $results = $service->sendTo1c(['ids' => $ids]);
            if (!empty($results)) {
                foreach ($results as $key => $val) {
                    $result[] = [
                        'id' => $key,
                        'result' => (!empty($val['error']) ? $val['error'] : ''),
                    ];
                }
            }
        }
        $viewModel = new JsonModel([
            'success' => true,
            'result' => $result
        ]);
        return $viewModel;
    }

    public function resetWs1cAction()
    {
        $dbModel = $this->getModel();
        $id = $this->params()->fromPost('id');
        $ids = explode(',', $id);
        if (!empty($ids)) {
            $dbModel->update(['ws1c_synced' => 0], ['id' => $ids]);
        }
        $viewModel = new JsonModel([
            'success' => true
        ]);
        return $viewModel;
    }
}
