<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class TagController extends AbstractController
{
    const Model = 'nx\Model\Tag';

    public function indexAction()
    {
        $model   = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $model->getTagsBy([
            'columns'         => ['id','name'],
            'calc_found_rows' => true,
            'offset'          => (int)$this->params()->fromQuery('start', 0),
            'limit'           => (int)$this->params()->fromQuery('limit', 25),
            'filters'         => $filters
        ]);

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items']->count() ? $items['items']->toArray() : [],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $model   = $this->getModel();
        $item    = $model->getTagBy([
            'columns'      => ['id','name', 'created', 'last_updated'],
            'columns_left' => ['create_user_name', 'last_user_name'],
            'id'           => (int)$this->params()->fromPost('id')
        ]);

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item,
        ]);

        return $viewModel;
    }

    public function addAction()
    {
        $model = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'tagData' => [
                'name'  => 'string',
            ]
        ];

        $data = $this->processPostFields($group_fields);

        if(!empty($data['tagData']))
        {
            $data['tagData']['create_user_id'] = $user_session->user_id;
            $model->addTag($data['tagData']);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function updateAction()
    {
        $model = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'tagData' => [
                'name'  => 'string',
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');
        if(!empty($data) && $id)
        {
            $data['tagData']['last_user_id'] = $user_session->user_id;
            $model->updateTag($id, $data['tagData']);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function getObjectsForComboAction()
    {
        $dbModel = $this->getModel();
        //$ids     = $this->params()->fromQuery('ids', array());

        $ids = [];
        $id = $this->params()->fromQuery('id', '');
        if(!empty($id))
        {
            $ids = explode(',',trim($id));
            $ids = array_map('trim', $ids);
        }

        $entity_id = $this->params()->fromQuery('entity_id', 0);

        $viewModel =  new JsonModel([
            'success' => true,
            'objects' => $dbModel->getObjectsForCombo([
                'query'     => $this->params()->fromQuery('query', ''),
                'ids'       => $ids,
                'entity_id' => $entity_id
            ])
        ]);

        return $viewModel;
    }
}
