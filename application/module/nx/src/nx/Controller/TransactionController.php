<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class TransactionController extends AbstractController
{
    const Model = 'nx\Model\Transaction';

    public function indexAction()
    {
    	$dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items']->count() ? $items['items']->toArray() : [],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function sendToPaymentAction()
    {
        $sm = $this->getServiceLocator();
        $paymentModel = $sm->get('nx\Model\Payment');

        $result = [];
        $ids = $this->params()->fromPost('ids', []);
        //$ids = explode(',', $ids);
        if(!empty($ids)) {
            $result = $paymentModel->savePaymentFromTransactions(['ids' => $ids]);
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result,
        ]);

        return $viewModel;
    }
}
