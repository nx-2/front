<?php

namespace nx\Controller\factory;

use nx\Controller\NotificationController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class NotificationControllerFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sm = $serviceLocator->getServiceLocator();
        $configs = $sm->get('config');
        $controller  = new NotificationController($configs['nx']);
        return $controller;
    }
}