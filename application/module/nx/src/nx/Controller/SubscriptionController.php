<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Exception;
use nx\Model\Address;
use nx\Model\TagEntity;
use Zend\Http\Client;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\View\Renderer\PhpRenderer;
use function nx\functions\isRunning;
use function nx\functions\objectToArray;

//use Zend\Json\Json;

class SubscriptionController extends AbstractController
{
    const Model = 'nx\Model\Subscription';
    const Service = 'nx\Service\Subscription';

    public function indexAction()
    {
        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $sorters = $this->getJsonParams('sort', '');
        $items = $dbModel->getSubscriptionList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters,
                'sorters' => $sorters,
                'by' => $this->params()->fromQuery('by', ''),
                'ids' => json_decode($this->params()->fromQuery('json_ids', '')),
                'show_all' => $this->params()->fromQuery('show_all', 0)
            ]
        );

        $viewModel = new JsonModel([
            'success' => true,
            'items' => $items['items']->count() ? $items['items']->toArray() : [],
            'total' => $items['total']
        ]);

        //$this->layout()->setVariables(array('someVariable' => $this->params()->fromRoute('ad', 'empty')));

        //if($this->getRequest()->isXmlHttpRequest()) {
        //    $this->layout('layout/ajax-layout');
        //}
        //$viewModel->setTerminal(true);//To disable layout
        //$viewModel->setTemplate('mymodule/newview.phtml');
        return $viewModel;
        //echo 'test';
        //return $this->response;
        //To disable layout and view, return a response object
        //return $this->getResponse()->setContent(Json::encode(array(
        //    'success' => true
        //)));
        //return false; To disable your view return false
    }

    public function itemsAction()
    {
        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items = $dbModel->itemModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters,
                //'show_all' => $this->params()->fromQuery('show_all', 0)
            ]
        );

        $viewModel = new JsonModel([
            'success' => true,
            'items' => $items['items']->count() ? $items['items']->toArray() : [],
            'total' => $items['total']
        ]);

        return $viewModel;
    }

    public function pdflogAction()
    {
        $sm = $this->getServiceLocator();
        $dbModel = $sm->get('nx\Model\SubscriptionPdfLog');
        $filters = $this->getJsonParams('filter', '');
        $items = $dbModel->getPdfLog(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters
            ]
        );

        $viewModel = new JsonModel([
            'success' => true,
            'items' => $items['items'],
            'total' => $items['total']
        ]);

        return $viewModel;
    }

    public function pdflogbyissueAction()
    {
        $sm = $this->getServiceLocator();
        $dbModel = $sm->get('nx\Model\SubscriptionPdfLog');
        $filters = $this->getJsonParams('filter', '');
        $items = $dbModel->getPdfLogByIssue(
            [
                'offset' => (int)$this->params()->fromQuery('start', 0),
                'limit' => (int)$this->params()->fromQuery('limit', 25),
                'filters' => $filters,
                'calc_found_rows' => true
            ]
        );

        $viewModel = new JsonModel([
            'success' => true,
            'items' => $items['items'],
            'total' => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $sm = $this->getServiceLocator();
        $dbModel = $sm->get(self::Model);
        $itemReleaseModel = $sm->get('nx\Model\SubscriptionItemRelease');
        $subscription_id = (int)$this->params()->fromPost('id');
        //$item    = $dbModel->getByID($subscription_id);
        $items = $dbModel->getItems([
            'subscription_id' => $subscription_id
        ]);
        $maxdate = 0;
        /*if(!empty($items))
        {
            foreach($items as $item)
            {
                if(strtotime($item['date_end']) > $maxdate)
                {
                    $maxdate = strtotime($item['date_end']);
                }
            }
            if($maxdate>time())
            {*/
        $itemReleaseModel->updateItemsIssues($subscription_id);
        //    }
        //}

        $item = $dbModel->getByID($subscription_id);
        $item = $item->toArray();
        $items = $dbModel->getItems([
            'subscription_id' => $subscription_id
        ]);
        $tags = $dbModel->tagEntityModel->select(['entity_id' => TagEntity::ENTITY_ORDER, 'row_id' => $subscription_id]);
        $ids = [];
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                $ids[] = $tag['tag_id'];
            }
        }
        $ids = array_map('intval', $ids);
        $item['tags[]'] = $ids;
        //$item->is_shipped = $dbModel->isShipped($subscription_id);

        $this->layout('layout/ajax-layout');
        $viewModel = new JsonModel([
            'success' => true,
            'data' => $item,//array_merge($item->toArray(), array('tags[]' => array(1808, 4825), 'payment_type_id_id[]' => array(1808, 4825) )),
            'items' => $items
        ]);

        return $viewModel;
    }

    public function updateAction()
    {
        $sm = $this->getServiceLocator();
        $dbModel = $sm->get(self::Model);
        $user_session = new Container('user');

        $group_fields = [
            'subscriptionData' => [
                'enabled' => 'int',
                'order_state_id' => 'int',
                'comment' => 'string',
                'email' => 'email',
                'extra_emails' => 'string',
                'payment_type_id' => 'int',
                'payment_agent_id' => 'int',
                'payment_date' => 'date',
                'payment_docno' => 'string',
                'payment_sum' => 'float',
                'person_id' => 'int',
                'company_id' => 'int',
                'user_id' => 'int',
                'action_id' => 'int',
                'action_code' => 'string',
                'price' => 'float',
                'type' => 'int',
                //'checked'          => 'int',
                'xpress_id' => 'int',
                'manager_id' => 'int',
                'new_state' => 'int'
            ],
            'addressData' => [
                'country_id' => 'int',
                'address' => 'string',
                'fax' => 'string',
                'zipcode' => 'string',
                'phone' => 'string',
                'telcode' => 'string',
                'region' => 'string',
                'area' => 'string',
                'city' => 'string',
                'city1' => 'string',
                'street' => 'string',
                'house' => 'string',
                'building' => 'string',
                'structure' => 'string',
                'apartment' => 'string',
                'address_comment' => ['comment', 'string']
            ],
            'mapAddressData' => [
                'map_zipcode' => ['zipcode', 'string'],
                'map_region' => ['region', 'string'],
                'map_area' => ['area', 'string'],
                'map_city' => ['city', 'string'],
                'map_city1' => ['city1', 'string'],
                'map_street' => ['street', 'string'],
                'map_house' => ['house', 'string'],
                'map_building' => ['building', 'string'],
                'map_structure' => ['structure', 'string'],
                'map_apartment' => ['apartment', 'string'],
                'map_is_onclaim' => ['is_onclaim', 'int'],
                'map_post_box' => ['post_box', 'int'],
                'map_is_enveloped' => ['is_enveloped', 'int'],
                'map_address_comment' => ['comment', 'string']
            ],
            'magazinesData' => [
                'magazines' => 'array',
                'is_change_magazines' => 'int'
            ],
            'tagsData' => [
                'tags' => 'array'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $result = ['success' => true];

        $id = (int)$this->params()->fromPost('id');
        if (!empty($data) && $id) {
            $data['subscriptionData']['last_user_id'] = $user_session->user_id;
            if (empty($data['magazinesData']['is_change_magazines']) && /*empty($data['subscriptionData']['action_code']) && empty($data['subscriptionData']['action_id']) &&*/ !empty($data['magazinesData']['magazines'])) {//action_code and id are not changeable yet, otherwise need some checks for code and id was changed
                unset($data['magazinesData']['magazines']);
            }
            $confirmed = $this->params()->fromPost('nx_ui_confirmed');
            if (!$confirmed && !empty($data['magazinesData']['magazines'])) {
                $check = $dbModel->intersectionCheck($data, $id);
                if (!empty($check['subscription_id'])) {
                    $result['confirmMessage'] = 'Подписка на ' . $check['periodical_name'] . ' пересекается с подпиской #' . $check['subscription_id'] . '. Вы уверены?';
                }
            }
            if ($confirmed || empty($result['confirmMessage'])) {
                $dbModel->updateSubscription($id, $data);
            }
            /*
            $subscription = $dbModel->getByID($id);
            send payment letter if confirmed by button click
            if($confirm && $oldState!=3 && $newState==3)
            {
                $mailTemplatesModel = $sm->get('nx\Model\MailTemplates');
                $mailTemplatesModel->getParseAndSendTemplate(6,
                    array(
                        '%%USER_NAME%%'   => $subscription['person_name'],
                        '%%ORDER_ID%%'    => $subscription['id'],
                        '%%ORDER_PRICE%%' => $subscription['price'],
                        '%%ORDER_DATE%%'  => date('Y-m-d', strtotime($subscription['created']))
                    ),
                    array(
                        'priority' => 5,
                        'from'     => 'xpress@osp.ru',
                        'to_email' => $subscription['email'],
                        'to_name'  => $subscription['person_name']
                    )
                );
            }
            */
            /*maybe by button click?

            if(empty($subscription['user_id']) && $subscription['order_state_id']==3)
            {
                $userModel = $sm->get('nx\Model\User');
                //register user
                $userModel->registerBySubscription($subscription);
            }*/
        }

        $this->layout('layout/ajax-layout');

        $viewModel = new JsonModel($result);

        return $viewModel;
    }

    public function addAction()
    {
        $dbModel = $this->getModel();
        $user_session = new Container('user');

        $group_fields = [
            'subscriptionData' => [
                'enabled' => 'int',
                'order_state_id' => 'int',
                'comment' => 'string',
                'email' => 'email',
                'extra_emails' => 'string',
                'payment_type_id' => 'int',
                'payment_agent_id' => 'int',
                'payment_date' => 'date',
                'payment_docno' => 'string',
                'payment_sum' => 'float',
                'person_id' => 'int',
                'company_id' => 'int',
                'user_id' => 'int',
                'type' => 'int',
                'price' => 'float',
                'xpress_id' => 'int',
                'manager_id' => 'int',
                'new_state' => 'int'
            ],
            'addressData' => [
                'country_id' => 'int',
                'address' => 'string',
                'fax' => 'string',
                'zipcode' => 'string',
                'phone' => 'string',
                'telcode' => 'string',
                'region' => 'string',
                'area' => 'string',
                'city' => 'string',
                'city1' => 'string',
                'street' => 'string',
                'house' => 'string',
                'building' => 'string',
                'structure' => 'string',
                'apartment' => 'string',
                'address_comment' => ['comment', 'string']
            ],
            'mapAddressData' => [
                'map_zipcode' => ['zipcode', 'string'],
                'map_region' => ['region', 'string'],
                'map_area' => ['area', 'string'],
                'map_city' => ['city', 'string'],
                'map_city1' => ['city1', 'string'],
                'map_street' => ['street', 'string'],
                'map_house' => ['house', 'string'],
                'map_building' => ['building', 'string'],
                'map_structure' => ['structure', 'string'],
                'map_apartment' => ['apartment', 'string'],
                'map_is_onclaim' => ['is_onclaim', 'int'],
                'map_post_box' => ['post_box', 'int'],
                'map_is_enveloped' => ['is_enveloped', 'int'],
                'map_address_comment' => ['comment', 'string']
            ],
            'magazinesData' => [
                'magazines' => 'array'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $result = ['success' => true];

        if (!empty($data)) {
            $data['subscriptionData']['create_user_id'] = $user_session->user_id;
            $confirmed = $this->params()->fromPost('nx_ui_confirmed');
            if (!$confirmed && !empty($data['magazinesData']['magazines'])) {
                $check = $dbModel->intersectionCheck($data);
                if (!empty($check['subscription_id'])) {
                    $result['confirmMessage'] = 'Подписка на ' . $check['periodical_name'] . ' пересекается с подпиской #' . $check['subscription_id'] . '. Вы уверены?';
                }
            }
            if ($confirmed || empty($result['confirmMessage'])) {
                $dbModel->addSubscription($data);
            }
        }

        /*if(!empty($data['subscription']))
        {
            $data['subscription']['last_user_id'] = $user_session->user_id;
            $dbModel->update(
                $data['subscription'],
                array(
                    'id' => (int)$this->params()->fromPost('id')
                )
            );
        }*/

        $this->layout('layout/ajax-layout');
        $viewModel = new JsonModel($result);

        return $viewModel;
    }

    public function getDataFromCsvAction()
    {
        ini_set("max_execution_time", "1800");
        $dbModel = $this->getModel();
        $user_session = new Container('user');

        $group_fields = [
            'subscriptionData' => [
                'label' => 'string',
                'order_state_id' => 'int',
                'payment_type_id' => 'int',
                'type' => 'int',
            ],
            'mapAddressData' => [
                'map_is_enveloped' => ['is_enveloped', 'int']
            ],
            'magazinesData' => [
                'magazines' => 'array'
            ],
            'serviceData' => [
                'pass_rows' => 'string',
                'csv_map' => 'array'
            ],
            'tagsData' => [
                'tags' => 'array'
            ]
        ];

        $post = $this->processPostFields($group_fields);

        $result = ['success' => true];

        if (empty($post['magazinesData']['magazines'])) {
            $result['systemMessage'] = ['title' => 'не выбраны издания!'];
            return new JsonModel($result);
        }

        $data = [];

        $file = $this->params()->fromFiles('file');
        $file = !empty($file['tmp_name']) ? $file['tmp_name'] : '';
        if (empty($file)) {
            $result['systemMessage'] = ['title' => 'не выбран файл csv!'];
            return new JsonModel($result);
        }

        $pass_rows = explode(',', $post['serviceData']['pass_rows']);
        $handle = fopen($file, "r");
        $rownum = 0;

        $itemsData = $post['magazinesData']['magazines'];
        $tagsData = $post['tagsData'];
        $subscriptionData = $post['subscriptionData'];
        $subscriptionData['label'] .= '_nx_csv_subscription';
        $subscriptionData = array_merge($subscriptionData, [
            'created' => date('Y-m-d H:i:s'),
            'subscriber_type_id' => 1,
            'create_user_id' => $user_session->user_id
        ]);

        $csvmap = $post['serviceData']['csv_map'];//var_dump($csvmap);
        while (($buffer = fgetcsv($handle, 4096)) !== false) {
            $rownum++;
            if (in_array($rownum, $pass_rows)) {
                continue;
            }
            //echo $rownum . ' ' . $buffer;
            //$buffer = explode(';', $buffer);
            foreach ($buffer as $i => $val) {
                $buffer[$i] = trim($val);
            }

            $personData = [];
            $addressData = [];
            $orderData = [];

            foreach ($csvmap as $map) {
                $col = explode('_', $map['column_db']);
                if ($col[0] == 'address') {
                    $addressData[$col[1]] = $buffer[$map['column_num'] - 1];
                }
                if ($col[0] == 'person') {
                    $personData[$col[1]] = $buffer[$map['column_num'] - 1];
                }
                if ($col[0] == 'order') {
                    $orderData[$col[1]] = $buffer[$map['column_num'] - 1];
                }
            }
            $subscriptionData = array_merge($subscriptionData, $orderData);

            if (empty($personData['name'])) //&& (!empty($personData['f']) || !empty($personData['i']) || !empty($personData['o'])))
            {
                $personData['name'] =
                    (!empty($personData['f']) ? $personData['f'] : '') .
                    (!empty($personData['f']) && !empty($personData['i']) ? ' ' : '') .
                    (!empty($personData['i']) ? $personData['i'] : '') .
                    (!empty($personData['i']) && !empty($personData['o']) ? ' ' : '') .
                    (!empty($personData['o']) ? $personData['o'] : '');
            } else {
                $fio = explode(' ', $personData['name'] . '   ');
                if (empty($personData['f'])) {
                    $personData['f'] = $fio[0];
                }
                if (empty($personData['i'])) {
                    $personData['i'] = $fio[1];
                }
                if (empty($personData['o'])) {
                    $personData['o'] = $fio[2];
                }
            }

            if (!empty($addressData)) {
                $addressData['country_id'] = !empty($addressData['country_id']) ? $addressData['country_id'] : Address::COUNTRY_ID_RUSSIA;
                if (empty($addressData['address'])) {
                    $addressData['address'] =
                        (!empty($addressData['street']) ? 'ул. ' . $addressData['street'] . ', ' : '') .
                        (!empty($addressData['house']) ? $addressData['house'] : '') .
                        (!empty($addressData['building']) ? ', корп. ' . $addressData['building'] : '') .
                        (!empty($addressData['apartment']) ? ', кв. ' . $addressData['apartment'] : '');
                }
            }

            $mapAddressData = [];
            if (!empty($addressData['address'])) {
                $ahunter_query =
                    (!empty($addressData['zipcode']) ? $addressData['zipcode'] . ', ' : '') .
                    (!empty($addressData['area']) ? $addressData['area'] . ', ' : '') .
                    (!empty($addressData['city']) ? $addressData['city'] . ', ' : '') .
                    $addressData['address'];
                $mapdata = $dbModel->addressModel->processAhunterRequest($ahunter_query, Address::AHUNTER_PARSE_RESULT);
                if (!empty($mapdata)) {
                    $mapAddressData = $mapdata[0];
                }
                $mapAddressData = array_merge($mapAddressData, $post['mapAddressData']);
            }

            $subscriptionFullData = [
                'subscriptionData' => $subscriptionData,
                'addressData' => $addressData,
                'mapAddressData' => $mapAddressData,
                'itemsData' => $itemsData,
                'tagsData' => $tagsData
            ];
            $personData = ['personData' => $personData];
            $params = ['subscriptionData' => $subscriptionFullData, 'personData' => $personData];
            $data[] = $params;
            //break;
        }

        if (!empty($data)) {
            $result['data'] = $data;
        }

        /*$adapter = new \Zend\File\Transfer\Adapter\Http();
        $adapter->setDestination('data/assets');
        if ($adapter->receive($File['name'])) {
            $profile->exchangeArray($form->getData());
            echo 'Profile Name '.$profile->profilename.' upload '.$profile->fileupload;
        }*/

        $viewModel = new JsonModel($result);
        return $viewModel;
    }

    public function addByDataAction()
    {
        ini_set("max_execution_time", "1800");
        $sm = $this->getServiceLocator();
        $config = $sm->get('config');
        $post = $this->params()->fromPost();
        $data = json_decode($post['data']);
        $data = objectToArray($data);
        $nx_client = new Client($config['nx']['schema'] . $config['nx']['http_host'] . '/index/api');
        $nx_client->setOptions(['sslcapath' => '/etc/ssl/certs']);
        $nx_client->setMethod('POST');
        //$nx_client->setHeaders('Referer: ' . $_SERVER['HTTP_REFERER']);
        $nx_client->setEncType(Client::ENC_URLENCODED);
        $nx_client->setParameterPost([
            'apikey' => md5($config['nx']['passport_service']['token']),
            'method' => 'subscription_add'
        ]);
        foreach ($data as $params) {
            $nx_client->getRequest()->getPost()->set('params', $params);
            $result = $nx_client->send();
            //break;
        }
        $viewModel = new JsonModel(['success' => true]);
        return $viewModel;
    }

    public function getMailMessageAction()
    {
        $sm = $this->getServiceLocator();
        $dbModel = $sm->get(self::Model);
        $service = $this->getService();
        $mailTemplatesModel = $sm->get('nx\Model\MailTemplates');
        $subscription_id = (int)$this->params()->fromPost('entity_id', 0);
        $template_id = (int)$this->params()->fromPost('template_id', 0);
        $subscription = $dbModel->getByID($subscription_id);
        $mailTemplate = $mailTemplatesModel->getByID($template_id);
        $mailText = '';
        $mailSubject = '';

        $mail = $service->getMailMessage($subscription_id, $template_id);
        $mailText = $mail['message'];
        $mailSubject = $mail['subject'];

        $viewModel = new JsonModel([
            'success' => true,
            'data' => ['message' => $mailText, 'subject' => $mailSubject],
        ]);

        return $viewModel;
    }

    public function xpressAction()
    {
        $sm = $this->getServiceLocator();
        $dbModel = $sm->get(self::Model);
        $dbModel->xpressdb = $sm->get('nx\xpress\Adapter');
        //$dbModel->cache = $sm->get('Memcached');
        $filters = $this->getJsonParams('filter', '');
        $items = $dbModel->xpressModel->getXpressList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters
            ]
        );

        $viewModel = new JsonModel([
            'success' => true,
            'items' => $items['items']->count() ? $items['items']->toArray() : [],
            'total' => $items['total']
        ]);

        return $viewModel;
    }

    public function importXpressAction()
    {
        $dbModel = $this->getModel();
        $user_session = new Container('user');

        $xpress_id = (int)$this->params()->fromQuery('xpress_id', 0);
        $step = (int)$this->params()->fromQuery('step', 1);
        $subscription_link_id = (int)$this->params()->fromQuery('subscription_link_id', 0);
        $person_link_id = (int)$this->params()->fromQuery('person_link_id', 0);
        $company_link_id = (int)$this->params()->fromQuery('company_link_id', 0);
        $is_just_link_subscription = (int)$this->params()->fromQuery('is_just_link_subscription', 0);
        $is_just_link_person = (int)$this->params()->fromQuery('is_just_link_person', 0);
        $is_just_link_company = (int)$this->params()->fromQuery('is_just_link_company', 0);
        //is_auto_link_user
        //is_active
        //add flag to merge data, not only add or update
        //echo '<pre>';
        //var_dump($xpress_id);
        if ($xpress_id) {
            $result = $dbModel->importFromXpress([
                'xdb' => $dbModel->xpressModel->getAdapter(),
                'xpress_id' => $xpress_id,
                'nx_user_id' => $user_session->user_id,
                'mode' => $step == 3 ? 3 : ($step == 2 ? 2 : 1),
                'subscription_link_id' => $subscription_link_id,
                'person_link_id' => $person_link_id,
                'company_link_id' => $company_link_id,
                'is_just_link_subscription' => $is_just_link_subscription,
                'is_just_link_person' => $is_just_link_person,
                'is_just_link_company' => $is_just_link_company
            ]);
        }

        if ($step == 1) {
            $orderStates = $dbModel->getOrderStates();
            $paymentTypes = $dbModel->getPaymentTypes();
            $close_magazines = [2, 17];//nets, stuff
            $magazines = $dbModel->getSubscribleMagazines(['add_ids' => $close_magazines]);
            $result_person_fio = !empty($result['personData']['personData']) ? $result['personData']['personData']['f'] . ' ' . $result['personData']['personData']['i'] . ' ' . $result['personData']['personData']['o'] : '';
            $res = [];
            $res['Свойства']['Физ. лицо'] = !empty($result['personData']['personData']) ? $result['personData']['personData']['name'] : $result_person_fio;
            $res['Свойства']['Организация'] = !empty($result['companyData']['companyData']) ? $result['companyData']['companyData']['name'] : '';
            $res['Свойства']['Email'] = $result['subscriptionData']['subscriptionData']['email'];
            $res['Свойства']['Состояние'] = $orderStates[$result['subscriptionData']['subscriptionData']['order_state_id']]['name'];
            $res['Свойства']['Способ оплаты'] = !empty($result['subscriptionData']['subscriptionData']['payment_type_id']) ? $paymentTypes[$result['subscriptionData']['subscriptionData']['payment_type_id']]['name'] : 'не оплачивается';
            $res['Свойства']['Сумма'] = $result['subscriptionData']['subscriptionData']['price'];
            $res['Свойства']['Включена?'] = $result['subscriptionData']['subscriptionData']['enabled'] ? 'да' : 'нет';
            $res['Свойства']['Комментарий'] = $result['subscriptionData']['subscriptionData']['comment'];
            foreach ($result['subscriptionData']['magazinesData'] as $magazine) {
                if (empty($magazines[$magazine['periodical_id']]) || (in_array($magazine['periodical_id'], $close_magazines) && $magazine['byear'] > 2013)) {
                    throw new Exception('в nx отсутствует издание' . $magazine['periodical_id'] . ' для подписки, либо подписка закрыта');
                }
                $res['Свойства']['Издания'][] = [
                    'издание' => $magazines[$magazine['periodical_id']]['name'],
                    'тип' => $magazine['type'],
                    'год' => $magazine['byear'],
                    'выпуск' => $magazine['bnum'],
                    'номеров' => $magazine['rqty']
                ];
            }
            $res['Тех. данные'] = $result;
            $result = $res;
        }

        $viewModel = new JsonModel([
            'result' => $result,
            'success' => true
        ]);

        return $viewModel;
    }

    public function importXpressListAction()
    {
        $dbModel = $this->getModel();
        $user_session = new Container('user');

        $xpress_ids = $this->params()->fromPost('ids', '');
        //var_dump($xpress_ids);

        if ($xpress_ids) {
            $subscriptions = $dbModel->xpressModel->getXpressList(0, 5000, ['ids' => explode(',', $xpress_ids)]);
        }
        /*else
        {
            $subscriptions = $dbModel->getXpressList(0,2,array('filters' => array(
                array('property' => 'is_present', 'value' => 1),
                array('property' => 'is_payed', 'value' => 1),
            )));
        }*/

        $items = [];
        if ($subscriptions['items']->count()) {
            ini_set("max_execution_time", "900");
            $subscriptions = $subscriptions['items']->toArray();
            foreach ($subscriptions as $subscription) {
                $result = $dbModel->importFromXpress([
                    'xdb' => $dbModel->xpressModel->getAdapter(),
                    'xpress_id' => $subscription['id'],
                    'mode' => 2,
                ]);

                $actions = [
                    'lock' => 'xpress_id занят',
                    'link' => 'связь с подпиской ',
                    'add' => 'добавление',
                    'manual' => 'вручную'
                ];

                $items[] = [
                    'xpress_id' => $subscription['id'],
                    'action' => $result['action'],
                    'action_name' => $actions[$result['action']] . ($result['subscription_link_id'] ? ' #' . $result['subscription_link_id'] : ''),
                    'link_id' => $result['subscription_link_id'] ? $result['subscription_link_id'] : 0,
                    'comment' => $result['comment']
                ];
            }
        }
        /*$action = array();
        foreach ($items as $key => $row) {
            $action[$key]  = $row['action'];
        }
        array_multisort($action, SORT_DESC, $items);
        echo '<table border="1"><tr><td>#</td><td>xpress_id</td><td>action</td></tr>';
        foreach($items as $i => $item)
        {
            ?>
            <tr>
                <td><?=($i+1)?></td>
                <td><?=$item['xpress_id']?></td>
                <td><?=$item['action']?></td>
            </tr>
            <?
        }
        echo '</table>';
        die;
        return false;*/
        $viewModel = new JsonModel([
            'items' => $items,
            'success' => true
        ]);

        return $viewModel;
    }

    public function importXpressAllAction()
    {
        $dbModel = $this->getModel();

        $pid_file = '/var/www/nx.osp.ru/htdocs/application/data/cache/xpressImportStatsPid.txt';
        $cmd = "/usr/local/zend/bin/php /var/www/nx.osp.ru/htdocs/application/schedule/xpress_import.php > /dev/null 2>&1 & echo $! > " . $pid_file;
        exec($cmd);

        $viewModel = new JsonModel([
            'success' => true
        ]);

        return $viewModel;
    }

    public function getIssuesAction()
    {
        $sm = $this->getServiceLocator();
        $dbModel = $sm->get('nx\Model\SubscriptionItemRelease');
        $items = $dbModel->getIssues(
            [
                'ship_item_id' => $this->params()->fromQuery('ship_item_id', 0),
                'item_id' => $this->params()->fromQuery('item_id', 0),
                'periodical_id' => $this->params()->fromQuery('periodical_id', 0),
                'byear' => $this->params()->fromQuery('byear', 0),
                'bnum' => $this->params()->fromQuery('bnum', 0),
                'rqty' => $this->params()->fromQuery('rqty', 0),
            ]
        );

        $viewModel = new JsonModel([
            'success' => true,
            'items' => $items
        ]);

        return $viewModel;
    }

    public function getDataForNewItemAction()
    {
        $dbModel = $this->getModel();
        $data = [
            'periodical_id' => $this->params()->fromQuery('periodical_id', 0),
            'delivery_type_id' => $this->params()->fromQuery('delivery_type_id', 0),
            'date_start' => $this->params()->fromQuery('date_start', ''),
            'period' => $this->params()->fromQuery('period', 0),
        ];
        $customer_type_id = (int)$this->params()->fromQuery('company_id', 0) ? 2 : 1;
        $country_id = (int)$this->params()->fromQuery('country_id', 0);
        $country_id = $country_id ? $country_id : Address::COUNTRY_ID_RUSSIA;
        if ($data['periodical_id'] && $data['delivery_type_id'] && $data['date_start'] && $data['period']) {
            $data = $dbModel->expandDataForNewItem($data, ['customer_type_id' => $customer_type_id, 'country_id' => $country_id]);
        }

        $viewModel = new JsonModel([
            'success' => true,
            'data' => $data
        ]);

        return $viewModel;
    }

    public function exportXpressCsvAction()
    {
        $dbModel = $this->getModel();

        $subscriptions = $dbModel->xpressModel->exportXpressToCsv();

        $content = '';
        ob_start();
        if (!empty($subscriptions)) {
            $row = '';
            foreach ($subscriptions[0] as $col => $val) {
                $row .= $col . ';';
            }
            $row = substr($row, 0, -1);
            echo $row . "\n";
            foreach ($subscriptions as $subscription) {
                $row = '';
                foreach ($subscription as $value) {
                    $row .= $value . ';';
                }
                $row = substr($row, 0, -1);
                echo $row . "\n";
            }
        }
        $content = ob_get_clean();

        if ($content) {
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=xpress.csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $content;
            //$viewModel = new ViewModel();
            //$viewModel->setTerminal(true);
            //return false;
            die;
        } else {
            $viewModel = new JsonModel([
                'result' => $result,
                'success' => true
            ]);

            return $viewModel;
        }
    }

    public function compareXpressNxAction()
    {
        $sm = $this->getServiceLocator();
        $dbModel = $sm->get(self::Model);
        $itemReleaseModel = $sm->get('nx\Model\SubscriptionItemRelease');
        $xpress_id = $this->params()->fromQuery('xpress_id', 0);
        $subscription_id = $this->params()->fromQuery('subscription_id', 0);

        $xpress_subscription = $dbModel->xpressModel->getBy(['id' => $xpress_id]);
        $xpress_items = [];
        $xpress_items_list = [];
        if (!empty($xpress_subscription->id)) {
            $xpress_items = $dbModel->xpressModel->getItemsBy(['subscription_id' => $xpress_subscription->id]);
            if (!empty($xpress_items)) {
                foreach ($xpress_items as $item) {
                    $xpress_items_list[] = [
                        'издание' => $item['periodical_name'],
                        'год' => $item['byear'],
                        'нач. номер' => $item['bnum'] . ' (МВ = ' . $item['bmonth'] . ')',
                        'выпусков' => $item['rqty'],
                        'кол-во' => $item['qty']
                    ];
                }
            }
        }
        $subscription = $subscription_id ? $dbModel->getBy(['id' => $subscription_id]) : $dbModel->getBy(['xpress_id' => $xpress_id]);
        $items = [];
        $items_list = [];
        if (!empty($subscription->id)) {
            $items = $dbModel->getItems(['subscription_id' => $subscription->id]);
            if (!empty($items)) {
                foreach ($items as $item) {
                    $item_issues = $itemReleaseModel->getIssues(['periodical_id' => $item['periodical_id'], 'byear' => $item['byear'], 'bnum' => $item['bnum'], 'rqty' => $item['rqty']]);
                    $bmonth = !empty($item_issues[0]['release_month']) ? $item_issues[0]['release_month'] : 0;
                    $items_list[] = [
                        'издание' => $item['periodical_name'],
                        'год' => $item['byear'],
                        'нач. номер' => $item['bnum'] . ' (МВ = ' . $bmonth . ')',
                        'выпусков' => $item['rqty'],
                        'кол-во' => $item['quantity'],
                        'способ доставки' => $item['delivery_type_name']
                    ];
                }
            }
        }

        $result = [
            'xpress' => [
                'свойства' => [
                    'id' => $xpress_subscription->id,
                    'сумма' => $xpress_subscription->total,
                    'способ оплаты' => $xpress_subscription->payment_name,
                    'оплачено' => $xpress_subscription->payment_sum,
                    'номер док. оплаты' => $xpress_subscription->payment_docno,
                    'дата оплаты' => $xpress_subscription->payment_date,
                    'физ. лицо' => $xpress_subscription->person_name,
                    'организация' => $xpress_subscription->company_name,
                    'комментарий' => $xpress_subscription->comment,
                    'email физ. лица' => $xpress_subscription->person_email,
                    'email организации' => $xpress_subscription->company_email,
                    'адрес' => $xpress_subscription->area . ' ' . $xpress_subscription->city . ' ' . $xpress_subscription->address,
                    'дата создания' => $xpress_subscription->ctime,
                    'способ доставки' => $xpress_subscription->delivery_name,

                ],
                'позиции' => $xpress_items_list,
                'тех. данные' => [$xpress_subscription->toArray(), $xpress_items]
            ],
            'nx' => [
                'свойства' => [
                    'id' => $subscription->id,
                    'сумма' => $subscription->price,
                    'способ оплаты' => $subscription->payment_type_name,
                    'оплачено' => $subscription->payment_sum,
                    'номер док. оплаты' => $subscription->payment_docno,
                    'дата оплаты' => $subscription->payment_date,
                    'физ. лицо' => $subscription->person_name,
                    'организация' => $subscription->company_name,
                    'комментарий' => $subscription->comment,
                    'email' => $subscription->email,
                    'адрес' => $subscription->area . ' ' . $subscription->city . ' ' . $subscription->address,
                    'дата создания' => $subscription->created,
                    'состояние' => $subscription->order_state_name,
                    'xpress_id' => $subscription->xpress_id,
                ],
                'позиции' => $items_list,
                'тех. данные' => [$subscription->toArray(), $items]
            ]
        ];

        $viewModel = new JsonModel([
            'success' => true,
            'result' => $result
        ]);

        return $viewModel;
    }

    public function getXpressImportStatsAction()
    {
        $dbModel = $this->getModel();
        $data = [
            'counts' => [
                'total' => 0,
                'lock' => 0,
                'link' => 0,
                'add' => 0,
                'manual' => 0
            ],
            'ids' => [
                'lock' => [],
                'link' => [],
                'add' => [],
                'manual' => []
            ],
            'mtime' => 0,
            'running' => 0
        ];

        $cache_file = '/var/www/nx.osp.ru/htdocs/application/data/cache/xpressImportStats.txt';
        $cache = file_exists($cache_file) ? file_get_contents($cache_file) : '';
        if (!empty($cache)) {
            $data = unserialize($cache);
        }
        $data['mtime'] = date('Y-m-d H:i:s', filemtime($cache_file));

        $pid_file = '/var/www/nx.osp.ru/htdocs/application/data/cache/xpressImportStatsPid.txt';
        $pid = file_exists($pid_file) ? file_get_contents($pid_file) : '';
        $data['running'] = 0;
        if (!empty($pid)) {
            $data['running'] = (int)isRunning($pid);
        }


        $viewModel = new JsonModel([
            'success' => true,
            'data' => $data
        ]);

        return $viewModel;
    }

    public function getBillAction()
    {
        $sm = $this->getServiceLocator();
        $service = $sm->get('nx\Service\Order');
        $id = (int)$this->params()->fromPost('id');
        $signed = (bool)$this->params()->fromPost('signed', false);
        $user_session = new Container('user');
        $result = $service->get1CDocsForOrder(['id' => $id, 'user_id' => $user_session->user_id, 'bill' => true, 'signed' => $signed]);
        $viewModel = new JsonModel([
            'success' => true,
            'result' => $result
        ]);
        return $viewModel;
    }

    public function getDocAction()
    {
        $dbModel = $this->getModel();
        $id = (int)$this->params()->fromPost('id');
        $user_session = new Container('user');
        $result = $dbModel->getDoc_1c($id, ['user_id' => $user_session->user_id]);
        $viewModel = new JsonModel([
            'success' => true,
            'result' => $result
        ]);
        return $viewModel;
    }

    public function toggleMapCloseAction()
    {
        $sm = $this->getServiceLocator();
        $service = $sm->get('nx\Service\Subscription');
        $state = $this->params()->fromPost('state', 1);
        $result = $service->toggleMapCloseDay($state);
        $viewModel = new JsonModel([
            'success' => true,
            'result' => $result
        ]);
        return $viewModel;
    }

    public function checkMapCloseAction()
    {
        $sm = $this->getServiceLocator();
        $service = $sm->get('nx\Service\Subscription');
        $result = $service->getMapCloseDay();
        $viewModel = new JsonModel([
            'success' => true,
            'result' => $result ? true : false
        ]);
        return $viewModel;
    }

    public function registerAction()
    {
        $service = $this->getService();
        $id = (int)$this->params()->fromPost('id');
        $result = $service->tryToRegisterUser(['id' => $id, 'from' => 'nx_subscription_action']);
        $viewModel = new JsonModel([
            'success' => true,
            'result' => json_decode($result)
        ]);
        return $viewModel;
    }
}
