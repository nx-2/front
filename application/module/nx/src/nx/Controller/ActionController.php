<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ActionController extends AbstractController
{
    const Service = 'nx\Service\Action';

    public function indexAction()
    {
        $service = $this->getService();
        $filters = $this->getJsonParams('filter', '');
        $items   = $service->mapper->getActions([
                'filters'         => $filters,
                'load'            => ['order', 'page'],
                'calc_found_rows' => 1,
                'offset'          => (int)$this->params()->fromQuery('start', 0),
                'limit'           => (int)$this->params()->fromQuery('limit', 25)
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items']->count() ? $items['items']->toArray() : [],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $service = $this->getService();
        $item    = $service->mapper->getActionBy(['id' => (int)$this->params()->fromPost('id')]);
        $item    = $item->toArray();

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item,
        ]);

        return $viewModel;
    }
}
