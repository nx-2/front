<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
//use Zend\Db\Adapter\Adapter;
use Zend\Http\PhpEnvironment\RemoteAddress;
//use Zend\Session\Validator\RemoteAddr;
use Zend\Db\Sql\Predicate\Expression;
//use nx\Form\Login as LoginForm;

class LoginController extends AbstractController
{
    public function indexAction()
    {
        $sm            = $this->getServiceLocator();
        $message       = '';
        $login         = $this->params()->fromPost('login', '');
        $password      = $this->params()->fromPost('password', '');
        $user_model    = $sm->get('nx\Model\User');
        $session_model = $sm->get('nx\Model\Session');
        $remoteAddress = new RemoteAddress();
        if (!empty($login) && !empty($password))
        {

            $user = $user_model->findByCredentials($login,$password);
            if (!empty($user) && !empty($this->session_id))
            {
                $session_model->delete(['Session_ID'=>$this->session_id]);

                $session_model->insert([
                    'Session_ID'   => $this->session_id,
                    'User_ID'      => $user->id,
                    'SessionStart' => time(),
                    'SessionTime'  => time() + 24 * 3600,
                    'Catalogue_ID' => 0,
                    'LoginSave'    => 1,
                    'UserIP'       => ip2long($remoteAddress->getIpAddress())
                ]);

                $request_target = $this->params()->fromPost('request_target', '');

                $this->plugin('redirect')->toUrl('/' . $request_target);
                return false;

            }
            else
            {
                $message = 'Пользователя с такими данными не существует!';
            }
        }
        else
        {
//            $message = 'Введите логин и пароль!';
            $message = '';
        }

        $viewModel = new ViewModel([
            'message'    => $message
        ]);
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function logoutAction()
    {
        $sm            = $this->getServiceLocator();
        $user_session  = new \Zend\Session\Container('user');
        $session_model = $sm->get('nx\Model\Session');
        session_destroy();
        $session_model->delete(['Session_ID'=>$this->session_id]);
        $session_model->delete(['User_ID'=>$user_session->user_id]);
        $this->plugin('redirect')->toUrl('/');
        return false;
    }
}
