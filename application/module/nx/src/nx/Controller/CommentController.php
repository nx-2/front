<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class CommentController extends AbstractController
{
    const Model = 'nx\Model\Comment';

    public function indexAction()
    {
        $model   = $this->getModel();
        $filters = $this->getJsonParams('filter', []);
        $filters[] = ['property' => 'entity_id', 'value' => $this->params()->fromQuery('entity_id', 0)];
        $filters[] = ['property' => 'row_id', 'value' => $this->params()->fromQuery('row_id', 0)];
        $items   = $model->getCommentsBy([
            'as_array'     => true,
            'columns'      => ['id','comment', 'entity_id', 'row_id', 'created', 'tag_id'],
            'columns_left' => ['tag_name'],
            'filters'      => $filters
        ]);
        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items,
            'total'   => count($items)
        ]);

        return $viewModel;
    }

    public function addAction()
    {
        $model = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'commentData' => [
                'comment'   => 'string',
                'entity_id' => 'int',
                'row_id'    => 'int',
                'tag_id'    => 'int'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        if(!empty($data['commentData']))
        {
            $data['commentData']['create_user_id'] = $user_session->user_id;
            $model->addComment($data['commentData']);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function updateAction()
    {
        $model = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'commentData' => [
                'comment' => 'string',
                'tag_id'  => 'int'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');
        if(!empty($data) && $id)
        {
            $data['commentData']['last_user_id'] = $user_session->user_id;
            $model->updateComment($id, $data['commentData']);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function deleteAction()
    {
        $model = $this->getModel();
        $user_session = new \Zend\Session\Container('user');
        $id = (int)$this->params()->fromPost('id');

        if($id)
        {
            $model->deleteComment($id, ['user_id' => $user_session->user_id]);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }
}
