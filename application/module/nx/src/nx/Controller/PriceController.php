<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class PriceController extends AbstractController
{
    const Model = 'nx\Model\Price';

    public function indexAction()
    {
        $model   = $this->getModel();
        $items   = $model->getPricesBy([
            'columns'  => ['id', 'name', 'date', 'item_type', 'customer_type_id', 'checked', 'active'],
        ]);//->toArray();

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items,
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $model = $this->getModel();
        $id    = (int)$this->params()->fromPost('id', 0);
        if(empty($id))
        {
            $last = $model->getPriceBy([]);
            $id   = !empty($last['id']) ? $last['id'] : 0;
            $price['date'] = date('Y-m-d');
        }
        else
        {
            $price = $model->getPriceBy(['id' => $id, 'columns' => ['id', 'name', 'date', 'customer_type_id', 'checked']]);
        }
        $zones = $model->zoneModel->getZones();
        $data  = $model->getZeroPrice(['zones' => $zones]);
        if(!empty($id))
        {
            $data = $model->getPriceData(['price_id' => $id, 'base_data' => $data]);
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => ['zones' => $zones, 'data' => $data, 'price' => $price],
        ]);

        return $viewModel;
    }

    public function saveFromGridAction()
    {
        $model = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'priceData' => [
                'name'             => 'string',
                'date'             => 'date',
                'customer_type_id' => 'int',
                'checked'          => 'bool'
            ],
        ];

        $data = $this->processPostFields($group_fields);
        $data['itemsData'] = $this->getJsonParams('data', '', 'POST');

        $id = (int)$this->params()->fromPost('id', 0);

        if(!empty($id))
        {
            $data['priceData']['last_user_id'] = $user_session->user_id;
            $model->updateFromGrid($id, $data);
        }
        else
        {
            $data['priceData']['create_user_id'] = $user_session->user_id;
            $model->addFromGrid($data);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }
}
