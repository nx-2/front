<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ShipController extends AbstractController
{
    const Model = 'nx\Model\SubscriptionItemRelease';
    const Service = 'nx\Service\SubscriptionItemRelease';

    public function shipmentAction()
    {
        $sm      = $this->getServiceLocator();
        $dbModel = $sm->get('nx\Model\Shipment');
        $filters = $this->getJsonParams('filter', '');
        //$filters[] = array('property' => 'file_type', 'value' => \nx\Model\FileStorage::TYPE_SHIPMENT_DATA, 'xoperator' => 'or_null');
        $items   = $dbModel->getShipmentsBy(
            [
                'columns'         => ['id', 'date', 'created', 'channel_id', 'channel_name'],
                'columns_left'    => ['create_user_name'],//, 'file_hash'),
                'calc_found_rows' => true,
                'offset'          => (int)$this->params()->fromQuery('start', 0),
                'limit'           => (int)$this->params()->fromQuery('limit', 25),
                'filters'         => $filters,
                //'debug' => 1
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items'],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function resetShipmentAction() {
        $dbModel = $this->getModel();
        $id      = (int)$this->params()->fromPost('id');
        $user_session = new \Zend\Session\Container('user');

        $dbModel->cancelShip(['ids' => $id, 'user_id'=>$user_session->user_id]);

        $viewModel =  new JsonModel([
            'success' => true
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $service = $this->getService();
        $item    = $service->mapper->getReleaseBy(['id' => (int)$this->params()->fromPost('id')]);
        //$item    = $item->toArray();

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item,
        ]);

        return $viewModel;
    }

    public function shipAction()
    {
        $dbModel      = $this->getModel();
        $user_session = new \Zend\Session\Container('user');
        $shipids      = $this->params()->fromPost('ids', '');
        $ids          = [];
        if($shipids) {
            $ids = explode(',', $shipids);
        }

        $channel_id = $this->params()->fromPost('channel_id', 0);

        $result = false;
        if($channel_id && !empty($ids))
        {
            $result = $dbModel->shipByShippings([
                'date'       => $this->params()->fromPost('date', ''),
                'channel_id' => $channel_id,
                'ids'        => $ids,
                'user_id'    => $user_session->user_id
            ]);
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result
        ]);

        return $viewModel;
    }

    public function shipByMapAction()
    {
        $dbModel      = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $result  = $dbModel->shipByMap([
            'user_id'        => $user_session->user_id,
            'periodical_id'  => $this->params()->fromPost('periodical_id'),
            'year'           => $this->params()->fromPost('year'),
            'month_start'    => $this->params()->fromPost('month_start'),
            'month_end'      => $this->params()->fromPost('month_end')
        ]);

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result
        ]);

        return $viewModel;
    }

    public function cancelShipAction()
    {
        $user_session = new \Zend\Session\Container('user');
        $dbModel      = $this->getModel();
        $shipids      = $this->params()->fromPost('ids', '');
        $ids          = [];
        if($shipids) {
            $ids = explode(',', $shipids);
        }
        $result = false;
        if(!empty($ids)) {
            $result = $dbModel->cancelShip(['ids' => $ids, 'user_id' => $user_session->user_id]);
        }
        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result
        ]);
        return $viewModel;
    }

    public function shippingsAction()
    {

        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $sorters = $this->getJsonParams('sort', '');
        $limit = (int)$this->params()->fromQuery('limit', 25);
        $items = $dbModel->getShippingsBy([
                'columns' => [
                    'id', 'periodical_name', 'issue_title', 'email', 'subscription_id', 'subscriber_type_id', 'payment_date', 'price', 'discount', 'doc_result', 'payment_agent_name',
                    'year', 'delivery_type_id', 'month', 'shipment_id', 'order_type', 'payment_type_id', 'last_request_date', 'xpress_id', 'quantity', 'ship_count', 'type'
                ],
                'columns_left'    => ['zipcode', 'full_map_address', 'name', 'country_id', 'is_enveloped', 'shipdate'],
                'offset'          => (int)$this->params()->fromQuery('start', 0),
                'limit'           => $limit,
                'filters'         => $filters,
                'sorters'         => $sorters,
                'calc_found_rows' => $limit==99999 ? 0 : 1
            ]
        );
        $result = [
            'success' => true
        ];
        if(isset($items['total'])) {
            $result['items'] = $items['items'];
            $result['total'] = $items['total'];
        }
        else {
            $result['items'] = $items;
        }

        $viewModel = new JsonModel($result);

        return $viewModel;
    }

    public function shiptomapAction()
    {
        $dbModel = $this->getModel();
        $items   = $dbModel->getShippingsMapList(
            [
                'periodical_id' => $this->params()->fromQuery('periodical_id'),
                'year'          => $this->params()->fromQuery('year'),
                'month_start'   => $this->params()->fromQuery('month_start'),
                'month_end'     => $this->params()->fromQuery('month_end'),
                'is_ship'       => $this->params()->fromQuery('is_ship', null)
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items
        ]);

        return $viewModel;
    }

    public function shiptableAction()
    {
        $service = $this->getService();
        $filters = $this->getJsonParams('filter', '');
        $items = $service->getShippingsTable([
                'filters' => $filters
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items'],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function shipcountsAction()
    {
        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');

        $counts = $dbModel->getShippingsCounts(
            [
                'periodical_name' => $this->params()->fromQuery('periodical_name'),
                'issue_num'       => $this->params()->fromQuery('issue_num'),
                'yearmonth'       => $this->params()->fromQuery('yearmonth'),
                'filters'         => $filters
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $counts
        ]);

        return $viewModel;
    }

    public function getShipDocAction()
    {
        $sm      = $this->getServiceLocator();
        $service = $this->getService();
        $user_session = new \Zend\Session\Container('user');
        $perm    = $sm->get('nx\Perm');

        $shipids = $this->params()->fromPost('ids', '');
        $signed  = $this->params()->fromPost('signed', false);
        $signed  = filter_var($signed, FILTER_VALIDATE_BOOLEAN);

        $ids = [];
        if($shipids) {
            $ids = explode(',', $shipids);
        }
        $publisherId = isset($this->user['publisher_id']) ? $this->user['publisher_id'] : false;
        $result = [];
        if(!empty($ids)) {
            if(count($ids) == 1 && $perm->hasRole(\nx\Model\User::ROLE_DEVELOPER)) {
                $results = $service->get1CDocsForRelease(['ids' => $ids, 'user_id' => $user_session->user_id, 'signed' => $signed, 'publisher_id' => $publisherId]);
            } else {
                $results = $service->get1CDocsForReleases(['ids' => $ids, 'user_id' => $user_session->user_id, 'signed' => $signed, 'publisher_id' => $publisherId]);
            }
            if(!empty($results)) {
                foreach($results as $key => $val) {
                    $result[] = [
                        'id'     => $key,
                        'result' => (!empty($val['links']) ? $val['links'] . ' ' : '') . (!empty($val['error']) ? $val['error'] :  ''),
                        'files'  => !empty($val['file_ids']) ? $val['file_ids'] : []
                    ];
                }
            }
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result
        ]);

        return $viewModel;
    }

    public function updateShipDocAction()
    {
        $dbModel = $this->getModel();
        $service = $this->getService();
        $shipids = $this->params()->fromPost('ids', '');
        $ids     = [];
        if($shipids) {
            $ids = explode(',', $shipids);
        }
        if(!empty($ids)) {
            $data     = $service->get1CDocsData_Release(['ids' => $ids, 'prefix' => 'release']);
            $data_res = $data['results'];
            if(!empty($data_res)) {
                foreach($data_res as $release_id => $result) {
                    if(!empty($result['error'])) {
                        unset($ids[array_search($release_id, $ids)]);
                    }
                }
            }
            if(!empty($ids)) {
                $dbModel->update(['is_doc_wait' => 1], ['id' => $ids]);
            }
        }
        $viewModel =  new JsonModel([
            'success' => true
        ]);

        return $viewModel;
    }

    public function getStickersDataAction()
    {
        $dbModel = $this->getModel();
        $shipids  = $this->params()->fromPost('ids', '');
        $ids      = [];
        if($shipids) {
            $ids = explode(',', $shipids);
        }
        $data = '';
        if(!empty($ids))
        {
            $data = $dbModel->getStickersData(['ids' => $ids]);
        }
        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $data
        ]);
        return $viewModel;
    }
}
