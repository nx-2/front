<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use nx\Model\MailTemplates;
use nx\Service\PublishAdmin;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;

class MailLogController extends AbstractController
{
    const Model = 'nx\Model\MailLog';

    public function indexAction()
    {
        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters,
                'from' => $this->params()->fromQuery('from', '')
            ]
        );

        $viewModel = new JsonModel([
            'success' => true,
            'items' => $items['items']->count() ? $items['items']->toArray() : [],
            'total' => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $dbModel = $this->getModel();
        $item = $dbModel->getBy([
            'id' => (int)$this->params()->fromPost('id'),
            'from' => $this->params()->fromPost('from')
        ]);

        $this->layout('layout/ajax-layout');
        $viewModel = new JsonModel([
            'success' => true,
            'data' => $item->toArray()
        ]);

        return $viewModel;
    }

    public function sendMailMessageAction()
    {
        $sm = $this->getServiceLocator();
        $dbModel = $sm->get(self::Model);
        $email = $this->params()->fromPost('email', '');
        $email_cc = $this->params()->fromPost('email_cc', '');
        $mailSubject = $this->params()->fromPost('subject', '');
        $mailText = $this->params()->fromPost('message', '');
        $debug = $this->params()->fromPost('debug', false);
        $entityName = $this->params()->fromPost('entity_name', '');
        $entityId = $this->params()->fromPost('entity_id', NULL);

        //$entity_name     = $this->params()->fromPost('entity_name', '');
        $log_message = $this->params()->fromPost('log_message', '');
        //$entity_id       = $this->params()->fromPost('entity_id', '');
        $user_session = new Container('user');
        //$subscription_id = (int)$this->params()->fromPost('subscription_id', 0);
        //$subscription    = $dbModel->getByID($subscription_id);

        if (!empty($mailText) && !empty($mailSubject)) {
            /** @var MailTemplates $mailTemplatesModel */
            $mailTemplatesModel = $sm->get('nx\Model\MailTemplates');

            $mailData = [
                'to_email' => $debug ? $user_session->user['email'] : $email,
                'cc' => $email_cc,
                'from' => $user_session->user['email'],
                'subject' => $mailSubject,
                'text' => $mailText,
                'entity_name' => $entityName,
                'entity_id' => $entityId,
            ];

//            if($debug) {
//                $mailTemplatesModel::sendWithoutRegistry($mailData['to_email'], $mailData['subject'], $mailData['text'], $mailData['from']);
//            } else {
//                $mailTemplatesModel->create($mailData);
//            }

            /** @var PublishAdmin $publishAdminService */
            $publishAdminService = $sm->get('nx\Service\PublishAdmin');
            $url = '/guarded/email/send';
            $response = $publishAdminService->sendRequest($url, 'POST', $mailData);

            if (!$debug) {
                $logger = $sm->get('nx\Model\Log');
                $logger->log($log_message, 'remind_subscription', $mailData, $user_session->user_id);
            }
        }

        $viewModel = new JsonModel([
            'success' => true
        ]);

        return $viewModel;
    }
}
