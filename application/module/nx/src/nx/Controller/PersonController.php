<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class PersonController extends AbstractController
{
    const Model = 'nx\Model\Person';

    public function indexAction()
    {
    	$dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters,
                'ids'     => json_decode($this->params()->fromQuery('json_ids', '')),
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items']->count() ? $items['items']->toArray() : [],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $dbModel = $this->getModel();
        $item    = $dbModel->getByID((int)$this->params()->fromPost('id'));

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item,
        ]);

        return $viewModel;
    }

    public function updateAction()
    {
        $dbModel = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'personData' => [
                'checked'         => 'int',
                'enabled'         => 'int',
                'f'               => 'string',
                'i'               => 'string',
                'o'               => 'string',
                'email'           => 'email',
                'gender'          => 'string',
                'position'        => 'string',
                'speciality'      => 'string',
                'company_id'      => 'int',
                'phone'           => 'string',
                'comment'         => 'string'
            ],
            'addressData' => [
                'country_id'      => 'int',
                'address'         => 'string',
                'fax'             => 'string',
                'zipcode'         => 'string',
                'address_phone'   => ['phone', 'string'],
                'telcode'         => 'string',
                'region'          => 'string',
                'area'            => 'string',
                'city'            => 'string',
                'address_comment' => ['comment', 'string']
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');
        if(!empty($data) && $id)
        {
            $data['personData']['last_user_id'] = $user_session->user_id;
            $dbModel->updatePerson($id, $data);
            //if($result)
            //{
            //    $sm->get('Zend\Log')->info('update person ' . $result);
            //}
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    /**
     * Данные для комбоксов
     * @param undefined
     * @return json
     */
    public function getObjectsForComboAction() {
        $dbModel = $this->getModel();

        $viewModel =  new JsonModel([
            'success' => true,
            'objects' => $dbModel->getObjectsForCombo($this->params()->fromQuery('query', ''))
        ]);

        return $viewModel;
    }

    public function addAction()
    {
        $dbModel = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'personData' => [
                'checked'         => 'int',
                'f'               => 'string',
                'i'               => 'string',
                'o'               => 'string',
                'email'           => 'email',
                'gender'          => 'int',
                'position'        => 'string',
                'speciality'      => 'string',
                'company_id'      => 'int',
                'phone'           => 'string',
                'comment'         => 'string'
            ],
            'addressData' => [
                'country_id' => 'int',
                'address'    => 'string',
                'fax'        => 'string',
                'zipcode'    => 'string',
                'phone'      => 'string',
                'telcode'    => 'string',
                'region'     => 'string',
                'area'       => 'string',
                'city'       => 'string',
                'comment'    => 'string'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        if(!empty($data['personData']))
        {
            $data['personData']['create_user_id'] = $user_session->user_id;
            $dbModel->addPerson($data);
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }
}
