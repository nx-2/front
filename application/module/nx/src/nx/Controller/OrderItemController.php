<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class OrderItemController extends AbstractController
{
    const Model = 'nx\Model\OrderItem';
    const Service = 'nx\Service\OrderItem';

    /*public function indexAction()
    {
        $model   = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $model->getItemsBy(array(
            'columns'         => array('id', 'name'),
            'calc_found_rows' => true,
            'offset'          => (int)$this->params()->fromQuery('start', 0),
            'limit'           => (int)$this->params()->fromQuery('limit', 25),
            'filters'         => $filters
        ));

        $viewModel =  new JsonModel(array(
            'success' => true,
            'items'   => $items['items']->count() ? $items['items']->toArray() : array(),
            'total'   => $items['total']
        ));

        return $viewModel;
    }*/

    public function updateProlongStateAction()
    {
        $sm      = $this->getServiceLocator();
        $service = $sm->get('nx\Service\Order');//$this->getService();
        $model   = $this->getModel();

        $ids     = $this->params()->fromPost('ids', '');
        $ids     = explode(',', $ids);
        $result  = [];
        if(!empty($ids))
        {
            $check_ids = [];
            foreach($ids as $id)
            {
                $check = $service->isProlonged($id);
                if(!isset($check_ids[$check]))
                {
                    $check_ids[$check] = [];
                }
                $check_ids[$check][] = $id;
            }
            foreach($check_ids as $state => $ids)
            {
                $model->update(['prolong_state' => $state], ['id' => $ids]);
            }
        }

        $viewModel = new JsonModel([
            'success' => true,
            'result'  => $result
        ]);
        return $viewModel;
    }

    public function updateAction()
    {
        $dbModel = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'itemData' => [
                'prolong_state' => 'int'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');

        if(!empty($data) && $id)
        {
            $data['itemData']['last_user_id'] = $user_session->user_id;
            $result = $dbModel->update($data['itemData'], ['id' => $id]);
        }

        $this->layout('layout/ajax-layout');
        $viewModel = new JsonModel(['success' => true]);

        return $viewModel;
    }

    public function statsAction()
    {
        $sm      = $this->getServiceLocator();
        $dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');

        $stats = $dbModel->getStats(
            [
                'filters' => $filters
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $stats
        ]);

        return $viewModel;
    }

    public function getShipDocAction()
    {
        $sm      = $this->getServiceLocator();
        //$service = $this->getService();
        $service = $sm->get('nx\Service\SubscriptionItem');
        $user_session = new \Zend\Session\Container('user');
        $perm    = $sm->get('nx\Perm');

        $ids    = $this->params()->fromPost('ids', '');
        $signed = $this->params()->fromPost('signed', false);
        $signed = filter_var($signed, FILTER_VALIDATE_BOOLEAN);

        $ids = explode(',', $ids);

        $result = [];
        if(!empty($ids))
        {
            $results = $service->get1CDocsForSubscriptionItems(['ids' => $ids, 'user_id' => $user_session->user_id, 'signed' => $signed]);
            if(!empty($results)) {
                foreach($results as $key => $val) {
                    $result[] = [
                        'id'     => $key,
                        'result' => (!empty($val['links']) ? $val['links'] . ' ' : '') . (!empty($val['error']) ? $val['error'] :  ''),
                        'files'  => !empty($val['file_ids']) ? $val['file_ids'] : []
                    ];
                }
            }
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result
        ]);

        return $viewModel;
    }

    public function updateShipDocAction()
    {
        $sm      = $this->getServiceLocator();
        $service = $sm->get('nx\Service\SubscriptionItem');
        $user_session = new \Zend\Session\Container('user');
        $perm    = $sm->get('nx\Perm');

        $ids    = $this->params()->fromPost('ids', '');
        $signed = $this->params()->fromPost('signed', false);
        $signed = filter_var($signed, FILTER_VALIDATE_BOOLEAN);

        $ids = explode(',', $ids);

        $publisherId = isset($this->user['publisher_id']) ? $this->user['publisher_id'] : false;

        $result = [];
        if(!empty($ids)) {
            $results = $service->request1CDocsForSubscriptionItems(['ids' => $ids, 'user_id' => $user_session->user_id, 'signed' => $signed, 'publisher_id' => $publisherId]);
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $results
        ]);

        return $viewModel;
    }
}
