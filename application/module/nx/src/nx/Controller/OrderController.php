<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class OrderController extends AbstractController
{
    const Model = 'nx\Model\IssueOrder';
    const Service = 'nx\Service\Order';

    public function indexAction()
    {
    	$dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items'],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $dbModel = $this->getModel();
        $item    = $dbModel->getByID((int)$this->params()->fromPost('id'));

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item,
        ]);

        return $viewModel;
    }

    public function addAction()
    {
        $sm           = $this->getServiceLocator();
        $service      = $sm->get('nx\Service\Order');
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'orderData' => [
                'enabled'         => 'int',
                'order_state_id'  => 'int',
                'comment'         => 'string',
                'email'           => 'email',
                'payment_type_id' => 'int',
                'person_id'       => 'int',
                'company_id'      => 'int',
                'user_id'         => 'int',
                'type'            => 'int',
                'payment_date'    => 'date'
            ],
            'itemsData' => [
                'issue_id' => 'int'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        if(!empty($data))
        {
            $data['itemsData'] = ['Issue' => [$data['itemsData']['issue_id'] => 1]];
            $data['orderData']['shop_id'] = 2;
            $data['orderData']['create_user_id'] = $user_session->user_id;
            $service->mapper->addOrder($data);
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function updateAction()
    {
        $dbModel = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'orderData' => [
                'enabled'         => 'int',
                'order_state_id'  => 'int',
                'comment'         => 'string',
                'email'           => 'email',
                'payment_type_id' => 'int',
                'person_id'       => 'int',
                'company_id'      => 'int',
                'user_id'         => 'int',
                'type'            => 'int',
                'payment_date'    => 'date'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');
        if(!empty($data) && $id)
        {
            $data['orderData']['last_user_id'] = $user_session->user_id;
            $dbModel->updateOrder($id, $data);
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function getLastMonthIncomeAction()
    {
        $dbModel = $this->getModel();

        $cur_month_start       = date('Y-m-01');
        $cur_month_start_stamp = strtotime($cur_month_start);
        $prev_month_start      = date('Y-m-d', mktime(0, 0, 0, date('n', $cur_month_start_stamp)-1, date('j', $cur_month_start_stamp), date('Y',$cur_month_start_stamp)));
        $prev_month_end        = date('Y-m-d H:i:s', mktime(23, 59, 59, date('n', $cur_month_start_stamp), date('j', $cur_month_start_stamp)-1, date('Y',$cur_month_start_stamp)));

        $orders1 = $dbModel->getOrders(['order_state_id'=>3, 'created_start'=>$cur_month_start]);
        $orders2 = $dbModel->getOrders(['order_state_id'=>3, 'created_start'=>$prev_month_start, 'created_end'=>$prev_month_end]);

        $price1 = $price2 = 0;
        if(!empty($orders1))
        {
            foreach($orders1 as $order)
            {
                $price1+=$order['price'];
            }
        }
        if(!empty($orders2))
        {
            foreach($orders2 as $order)
            {
                $price2+=$order['price'];
            }
        }

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'data'    => [
                'income_last_month' => $price1,
                'income_prev_month' => $price2
            ],
            'success' => true,
        ]);

        return $viewModel;
    }

    public function getMessageAfterPayAction()
    {
        $sm           = $this->getServiceLocator();
        $dbModel      = $sm->get(self::Model);
        //$user_session = new \Zend\Session\Container('user');
        $order_id     = (int)$this->params()->fromPost('entity_id');

        $service            = $this->getService();
        //$mailTemplatesModel = $sm->get('nx\Model\MailTemplates');
        $template_id        = (int)$this->params()->fromPost('template_id', 0);
        $order              = $dbModel->getByID($order_id);
        //$mailTemplate       = $mailTemplatesModel->getByID($template_id);
        //$mailText           = '';
        //$mailSubject        = '';
        //$template_id = 10;
        if($template_id==10 && $order['order_state_id']!=3) {
            $template_id = 15;
        }
        $mail        = $service->getMailMessage($order_id, $template_id);
        $mailText    = $mail['message'];
        $mailSubject = $mail['subject'];

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => ['message' => $mailText, 'subject' => $mailSubject],
        ]);

        return $viewModel;

        /*$mailText = '';
        if(!empty($order_id))
        {
            $order = $dbModel->getByID($order_id);
            if(!empty($order))
            {
                $items = $dbModel->getItemsPDFIssue(array('order_id' => $order_id));

                if(!empty($items))
                {
                    $links_content = '';
                    foreach($items as $item)
                    {
                        $path = $dbModel::OSP_ROOT . $item['file_path'];
                        if(file_exists($path))
                        {
                            $simlink = '/download/' . md5($order['email'] . 'store' . $order['id'] . $item['file_path']) . '.pdf';
                            if(!file_exists($dbModel::OSP_ROOT . $simlink))
                            {
                                system('ln -s ' .
                                   escapeshellarg($path) . ' ' .
                                   escapeshellarg($dbModel::OSP_ROOT . $simlink)
                                );
                            }
                            $links_content .= '
                                <tr>
                                  <td style="vertical-align:top;">
                                    <table cellpadding="0" cellspacing="0" style="width:570px;">
                                      <tr>
                                        <td style="padding-top:30px; vertical-align:top; font-size:14px; padding-left:20px; font-family:Arial; line-height:20px !important;"><span style="font-weight:bold;">'.$item['periodical_name'].' '.$item['issue_title'].'</span></td>
                                      </tr>
                                      <tr>
                                        <td style="vertical-align:top; padding-top:13px; padding-left:20px;"><a href="http://www.osp.ru' . $simlink . '"><img src="http://www.osp.ru/images/mail/download.jpg" alt="download" style="border:none;" /></a></td>
                                      </tr>
                                      <tr>
                                        <td style="font-size:14px; font-family:Arial; padding-left:20px; color:#000; line-height:20px; padding-top:18px;">Или воспользуйтесь ссылкой:<br><a href="http://www.osp.ru' . $simlink . '" style="color:#0073b5;">http://www.osp.ru' . $simlink . '</a></td>
                                      </tr>
                                    </table>
                                  </td>
                                  <td style="width:108px;"><img src="http://www.osp.ru/data/'.$item['cover_image'].'" style="border:none;" alt="title" /></td>
                                </tr>
                            ';
                        }
                        else
                        {
                            throw new \Exception('Файл PDF для заказа не существует!');
                        }
                        //$this->db->insert('aecms_simlink', array('simlink' => $simlink, 'realpath' => $item['file_path']));
                    }

                    if(!empty($links_content))
                    {
                        $links_content = '<table>' . $links_content . '</table>';
                        $mailTemplatesModel = $sm->get('nx\Model\MailTemplates');
                        $templateID = 10;//osp
                        if($order['order_state_id']!=3) {
                            $templateID = 15;
                        }
                        $mailTemplate = $mailTemplatesModel->getByID($templateID);
                        $replacement = array(
                            '%%USER_NAME%%'     => $order['person_name'],
                            '%%ORDER_DATE%%'    => $order['created'],
                            '%%ORDER_PRICE%%'   => $order['price'],
                            '%%ORDER_ID%%'      => $order['id'],
                            '%%ORDER_CONTENT%%' => $links_content
                        );
                        $mailText    = str_replace(array_keys($replacement), array_values($replacement), $mailTemplate['text']);
                        $mailSubject = str_replace(array_keys($replacement), array_values($replacement), $mailTemplate['subject']);
                        $mailText    = $mailTemplatesModel->parseADVToTemplate($templateID, $mailText); // вставим рекламу
                    }
                }
            }
        }

        $viewModel =  new JsonModel(array(
            'success' => true,
            'data'    => array('message' => $mailText, 'subject' => $mailSubject),
        ));

        return $viewModel;*/
    }

    public function getIssuesForComboAction() {
        $sm    = $this->getServiceLocator();
        $model = $sm->get('nx\Model\Issue');

        $viewModel =  new JsonModel([
            'success' => true,
            'objects' => $model->getIssuesForCombo(
                $this->params()->fromQuery('query', ''),
                $this->params()->fromQuery('periodical_id')
            )
        ]);

        return $viewModel;
    }

    public function stattableAction()
    {
        $service = $this->getService();
        $filters = $this->getJsonParams('filter', '');
        $items = $service->getStatTable([
                'filters' => $filters
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items'],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }
}
