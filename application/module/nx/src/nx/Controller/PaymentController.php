<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class PaymentController extends AbstractController
{
    const Model = 'nx\Model\Payment';

    public function indexAction()
    {
    	$dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters
            ]
        );
        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items']->count() ? $items['items']->toArray() : [],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $dbModel = $this->getModel();
        $item    = $dbModel->getBy(['id' => (int)$this->params()->fromPost('id')]);
        $item    = $item->toArray();
        if(!empty($item))
        {
            $item['result'] = !empty($item['result']) ? unserialize($item['result']) : '';
            $item['request_data'] = !empty($item['request_data']) ? unserialize($item['request_data']) : '';
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item,
        ]);

        return $viewModel;
    }

    public function reAction()
    {
        $dbModel = $this->getModel();
        $id      = $this->params()->fromPost('id');
        $ids     = explode(',',$id);
        //$item    = $dbModel->getBy(array('id' => $id));
        //$item    = $item->toArray();

        //$result = array();
        //if(!empty($item['id']) && $item['status']!=0)
        if(!empty($ids))
        {
            $dbModel->update(['status' => 0], ['id' => $ids]);
            //$dbModel->sendNdsTo1c(array('id' => $id));
            //$result = $dbModel->getBy(array('id' => $id))->toArray();
            //$result = unserialize($result['result']);
        }

        $viewModel =  new JsonModel([
            'success' => true//,
            //'data'    => $result
        ]);

        return $viewModel;
    }


    public function updateAction()
    {
        $dbModel = $this->getModel();
        $user_session = new \Zend\Session\Container('user');

        $group_fields = [
            'paymentData' => [
                //'ws1c_id'         => 'int',
                //'transaction_id' => 'int',
                //'docno'          => 'string',
                //'sum'            => 'float',
                //'purpose'        => 'text',
                //'date'           => 'date',
                'order_id'       => 'int',
                'enabled'        => 'int',
                //'user_id'        => 'int'
            ]
        ];

        $data = $this->processPostFields($group_fields);

        $id = (int)$this->params()->fromPost('id');

        $results = ['success' => true];
        $result = [];
        if(!empty($data) && $id)
        {
            $data['paymentData']['last_user_id'] = $user_session->user_id;
            $confirmed = $this->params()->fromPost('nx_ui_confirmed');
            if(!$confirmed && !empty($data['paymentData']['order_id']))
            {
                $check = $dbModel->checkMatchOrderSum($id, $data['paymentData']['order_id']);
                if($check)
                {
                    $results['confirmMessage'] = $check . ' Вы уверены?';
                }
            }
            if($confirmed || empty($results['confirmMessage']))
            {
                $result = $dbModel->updatePayment($data['paymentData'], ['id' => $id]);
            }
        }

        if(!empty($result['error']))
        {
            $results = ['systemMessage' => ['title' => '', 'message' => $result['error']]];
        }

        $this->layout('layout/ajax-layout');
        $viewModel = new JsonModel($results);

        return $viewModel;
    }

    public function sendTransactionAction()
    {
        $dbModel = $this->getModel();
        $ids = $this->params()->fromPost('ids', '');
        $ids = explode(',', $ids);
        if(!empty($ids)) {
            $dbModel->sendTransactionTo1c(['ids' => $ids]);
        }
        $viewModel =  new JsonModel([
            'success' => true
        ]);

        return $viewModel;
    }

    public function sendNdsAction()
    {
        $dbModel = $this->getModel();
        $results = $dbModel->sendNdsTo1c();
        $viewModel =  new JsonModel([
            'result'  => $results,
            'success' => true
        ]);

        return $viewModel;
    }

    public function syncCompanyAction()
    {
        $dbModel = $this->getModel();
        $id      = (int)$this->params()->fromPost('id');
        $user_session = new \Zend\Session\Container('user');

        $result = $dbModel->syncCompany(['payment_id' => $id, 'user_id' => $user_session->user_id]);

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result
        ]);

        return $viewModel;
    }
}
