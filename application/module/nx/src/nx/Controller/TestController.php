<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TestController extends AbstractController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function testAction()
    {
    	$sm = $this->getServiceLocator();
    	return new ViewModel();
    }
}
