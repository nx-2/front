<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ViewStateController extends AbstractController
{
    const Model = 'nx\Model\ViewState';

    public function indexAction()
    {
        $user_session = new \Zend\Session\Container('user');
        $dbModel      = $this->getModel();
        $viewid       = $this->params()->fromQuery('viewid', '');

        if(empty($viewid)) {
            throw new \Exception('empty viewid!');
        }

        $items = $dbModel->getViewStates(['create_user_id' => $user_session->user_id, 'viewid' => $viewid]);

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items->count() ? $items->toArray() : [],
        ]);

        return $viewModel;
    }

    public function addAction()
    {
        $user_session = new \Zend\Session\Container('user');
        $sm           = $this->getServiceLocator();
        $dbModel      = $sm->get(self::Model);
        $filters      = $this->params()->fromPost('filter', '');
        $sorters      = $this->params()->fromPost('sort', '');
        $columns      = $this->params()->fromPost('columns', '');
        $name         = $this->params()->fromPost('name', '');
        $viewid       = $this->params()->fromPost('viewid', '');

        $data = [];
        if(!empty($filters)) {
            $filters = json_decode($filters);
            foreach($filters as $i=>$filter)
            {
                if(is_object($filter))
                {
                    $filters[$i] = (array)$filter;
                }
            }
            $data['filter'] = $filters;
        }
        if(!empty($sorters)) {
            $sorters = json_decode($sorters);
            foreach($sorters as $i=>$sorter)
            {
                if(is_object($sorter))
                {
                    $sorters[$i] = (array)$sorter;
                }
            }
            $data['sort'] = $sorters;
        }
        if(!empty($columns)) {
            $columns = json_decode($columns);
            foreach($columns as $i=>$column) {
                if(is_object($column)) {
                    $columns[$i] = (array)$column;
                }
            }
            $data['columns'] = $columns;
        }

        if(!$name) {
            $name = 'state_' . date('Ymd_His');
        }

        $stateData    = [
            'create_user_id' => $user_session->user_id,
            'name'           => $name,
            'viewid'         => $viewid,
            'data'           => $data
        ];
        if(!empty($data) && $viewid)
        {
            $result = $dbModel->addViewState($stateData);
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $sm      = $this->getServiceLocator();
        $dbModel = $sm->get(self::Model);
        $item    = $dbModel->getBy(['id' => $this->params()->fromQuery('id')]);

        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item,
        ]);

        return $viewModel;
    }

    public function deleteAction()
    {
        $sm      = $this->getServiceLocator();
        $dbModel = $sm->get(self::Model);
        $id      = $this->params()->fromPost('id', 0);;

        if($id) {
            $result = $dbModel->delete(['id' => $id]);
        }

        $viewModel =  new JsonModel([
            'success' => true,
            'result'  => $result
        ]);

        return $viewModel;
    }
}
