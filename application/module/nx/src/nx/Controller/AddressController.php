<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class AddressController extends AbstractController
{
    const Model = 'nx\Model\Address';

    public function indexAction()
    {
    	$dbModel = $this->getModel();
        $items   = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25)
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items'],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $dbModel = $this->getModel();
        $item    = $dbModel->getByID((int)$this->params()->fromPost('id'));

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item,
        ]);

        return $viewModel;
    }

    public function updateAction()
    {
        $dbModel = $this->getModel();

        $dbModel->update([
                'address'      => $this->params()->fromPost('name'),
                'phone'     => $this->params()->fromPost('email'),
                'fax'       => $this->params()->fromPost('url')
        ],
            [
                'id' => (int)$this->params()->fromPost('id')
            ]
        );

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function parseAction()
    {
        $dbModel = $this->getModel();
        $area    = $this->params()->fromPost('area', '');
        $region  = $this->params()->fromPost('region', '');
        $city    = $this->params()->fromPost('city', '');
        $address = $this->params()->fromPost('address', '');
        $data    = [];

        $result  = ['success' => true];
        if($address)
        {
            $query = ($area ? $area . ' ' : '') . ($region ? $region . ' ' : '') . ($city ? $city . ' ' : '') . $address;

            $output = $dbModel->processAhunterRequest($query, \nx\Model\Address::AHUNTER_PARSE_RESULT);
            if($output===false)
            {
                $result['systemMessage'] = [
                    'title'   => '',
                    'message' => 'К сожалению, не удалось разобрать адрес.'
                ];
            }
            else
            {
                $data = $output;
            }
        }

        $result['data'] = $data;
        $viewModel = new JsonModel($result);

        return $viewModel;
    }

    /*public function importMapfileToDbAction()
    {
        $sm      = $this->getServiceLocator();
        $dbModel = $sm->get('nx\Model\Address');
        $subscriptionModel = $sm->get('nx\Model\Subscription');
        $db = $sm->get('nx\Db\Adapter');

        $handle = @fopen("data/map11.csv", "r");
        if($handle)
        {
            while(($buffer = fgets($handle, 4096)) !== false)
            {
                echo $buffer;
                $buffer = explode(';', $buffer);
                $xpress_id = (int)$buffer[31];
                if(empty($xpress_id))
                {
                    continue;
                }
                foreach($buffer as $i => $val)
                {
                    $buffer[$i] = trim($val);
                }
                $street     = $buffer[25];
                $post_box   = '';
                $is_onclaim = 0;
                if(mb_stripos($street, 'до востребования')!==false)
                {
                    $street     = trim(trim(str_ireplace('до востребования', '', $street)),',;');
                    $is_onclaim = 1;
                }
                if(mb_stripos($street, 'а/я')!==false)
                {
                    $post_box = preg_replace('#[^\d]#is', '', str_ireplace('а/я', '', $street));
                    $street   = '';
                }
                $data = array(
                    'zipcode'    => $buffer[20],
                    'area'       => $buffer[21],
                    'region'     => $buffer[22],
                    'city'       => $buffer[23],
                    'city1'      => $buffer[24],
                    'street'     => $street,
                    'house'      => $buffer[26],
                    'building'   => $buffer[27],
                    'apartment'  => $buffer[28],
                    'post_box'   => $post_box,
                    'is_onclaim' => $is_onclaim
                );var_dump($data);

                $subscription = $subscriptionModel->getBy(array('xpress_id' => $xpress_id));

                if(!empty($subscription->id))
                {var_dump($subscription->id);
                    $subscriptionModel->updateSubscription($subscription->id, array('mapAddressData' => $data));
                }
            }
            fclose($handle);
        }
        return $this->getResponse();
    }*/
}
