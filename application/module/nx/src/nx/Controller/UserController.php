<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class UserController extends AbstractController
{
    const Model = 'nx\Model\User';

    public function indexAction()
    {
    	$dbModel = $this->getModel();
        $filters = $this->getJsonParams('filter', '');
        $items   = $dbModel->getList(
            (int)$this->params()->fromQuery('start', 0),
            (int)$this->params()->fromQuery('limit', 25),
            [
                'filters' => $filters
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items['items']->count() ? $items['items']->toArray() : [],
            'total'   => $items['total']
        ]);

        return $viewModel;
    }

    public function getAction()
    {
        $dbModel = $this->getModel();
        $item    = $dbModel->getByID((int)$this->params()->fromPost('id'));

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel([
            'success' => true,
            'data'    => $item->toArray(),
        ]);

        return $viewModel;
    }

    /*function updateAction()
    {
        $sm      = $this->getServiceLocator();
        $dbModel = $sm->get('nx\Model\User');

        $dbModel->update(array(
                'Login'     => $this->params()->fromPost('Login'),
                'Email'     => $this->params()->fromPost('Email'),
                'LastName'  => $this->params()->fromPost('LastName'),
                'FirstName' => $this->params()->fromPost('FirstName'),
                'MidName'   => $this->params()->fromPost('MidName')
            ),
            array(
                'User_ID' => (int)$this->params()->fromPost('id')
            )
        );

        $this->layout('layout/ajax-layout');
        $viewModel =  new JsonModel(array(
            'success' => true,
        ));

        return $viewModel;
    }*/

    /**
     * Данные для комбоксов
     * @param undefined
     * @return json
     */
    public function getObjectsForComboAction()
    {
        $dbModel = $this->getModel();

        $viewModel =  new JsonModel([
            'success' => true,
            'objects' => $dbModel->getObjectsForCombo($this->params()->fromQuery('query', ''))
        ]);

        return $viewModel;
    }

    public function getRolesAction()
    {
        $dbModel = $this->getModel();

        $items   = $dbModel->getRoles(
            [
                'user_id' => (int)$this->params()->fromQuery('user_id', 0)
            ]
        );

        $viewModel =  new JsonModel([
            'success' => true,
            'items'   => $items
        ]);

        return $viewModel;
    }

    public function addRoleAction()
    {
        $dbModel = $this->getModel();

        $user_id = (int)$this->params()->fromPost('user_id');
        $role_id = (int)$this->params()->fromPost('role_id');

        if(!empty($user_id) && !empty($role_id))
        {
            $dbModel->addUserRole($user_id, $role_id);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    public function deleteRoleAction()
    {
        $dbModel = $this->getModel();

        $user_id = (int)$this->params()->fromPost('user_id');
        $role_id = (int)$this->params()->fromPost('role_id');

        if(!empty($user_id) && !empty($role_id))
        {
            $dbModel->deleteUserRole($user_id, $role_id);
        }

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }

    /**
     * Залогинивание под другим пользователем
     * @param undefined
     * @return void
     */
    public function authFakeUserAction()
    {
        $sm   = $this->getServiceLocator();
        $perm = $sm->get('nx\Perm');
        if($perm->hasRole(\nx\Model\User::ROLE_ADMIN))
        {
            $session_model = $sm->get('nx\Model\Session');
            $remoteAddress = new \Zend\Http\PhpEnvironment\RemoteAddress();
            $user_id = (int)$this->params()->fromRoute('user_id', 0);
            $session_id = $this->session_id;
            $session_model->delete(['Session_ID'=>$session_id]);
            $session_model->delete(['User_ID' => $user_id]);
            $session_model->insert([
                'Session_ID'   => $session_id,
                'User_ID'      => $user_id,
                'SessionStart' => time(),
                'SessionTime'  => time() + 24 * 3600,
                'Catalogue_ID' => 0,
                'LoginSave'    => 1,
                'UserIP'       => ip2long($remoteAddress->getIpAddress())
            ]);
        }

        $this->plugin('redirect')->toUrl('/');
        return false;
    }

    public function doctrineAction()
    {
        $sm   = $this->getServiceLocator();
        $em = $sm->get('Doctrine\ORM\EntityManager');

        $em->getConfiguration()->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());

        $users = $em->getRepository('nx\Entity\User')->findAll();
        var_dump($users);

        $viewModel =  new JsonModel([
            'success' => true,
        ]);

        return $viewModel;
    }
}
