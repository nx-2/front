<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Service;
use Firebase\JWT\JWT;
use Zend\Http\Headers;

/**
 * @property \Zend\Db\Adapter\Adapter $dbAdapter
 * @property array $config
 * @property array $userData
 */
class PublishAdmin
{
    const JWT_PROP_GROUP_KEY = 'jwt';
    const JWT_ALG = 'HS256';

    private $config = [];
    private $userData = [];
    private $dbAdapter;

    public function __construct(array $config, array $userData, \Zend\Db\Adapter\Adapter $dbAdapter)
    {
        $this->config = $config;
        $this->userData = $userData;
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * @throws \Exception
     * @return array
     * @param string $url
     * @param string $method
     * @param array $data
     */
    public function sendRequest(string $url, string $method, array $data = [])
    {
        $availableMethodsList = ['GET', 'POST', 'PUT'];
        $method = strtoupper($method);
        if (!in_array($method, $availableMethodsList)) {
            $method = 'GET';
        }

        $host = $this->config['apiUrl'] ?? '';
        $addr = $host . $url;
        $client    = new \Zend\Http\Client($addr);

        $request = $client->getRequest();

//        $a = new \Zend\Http\Client\Adapter\Curl();
//        $client->setAdapter($a);

        $token = $this->buildAuthToken();
        $authHeader = new \Zend\Http\Header\Authorization('Bearer ' . $token);
        $contentTypeHeader = new \Zend\Http\Header\ContentType('application/x-www-form-urlencoded');

        $headers = new \Zend\Http\Headers();
        $headers->addHeader($authHeader);
        $headers->addHeader($contentTypeHeader);

        $client->setHeaders($headers);

        // $client->setOptions(array('sslverifypeer' => false));
        $client->setOptions(['sslcapath' => '/etc/ssl/certs']);
        $client->setMethod($method);
        if (in_array($method, ['POST', 'PUT'])) {
            $client->setParameterPost($data);
        }
        try {
//            $request = $client->getRequest();
            $response = $client->send();//var_dump($this->cfg['mail_service']['create'], $response->toString(), $client->getLastRawRequest());
            if ($response->isSuccess()) {
                $contentTypeString = strtolower($response->getHeaders()->get('Content-Type')->getFieldValue());
                $body = $response->getBody();
                $isJson = strpos($contentTypeString,'application/json') !== false;
                if ($isJson) {
                    $data = json_decode($body, true);
                } else {
                    $data = $body;
                }
            } else {
                throw new \Exception($response->getStatusCode() . ' ' . $response->getContent());
            }
        } catch (\Exception $e) {
            throw $e;
        }
        return $data;
    }

    public function buildAuthToken(): string
    {
        $propertyGroupKey = self::JWT_PROP_GROUP_KEY;
        $publisherId = intval($this->userData['publisher_id'] ?? 0);

        $sqlString = <<<SQL
            SELECT pt.name, pt.key, pp.value 
            FROM publisher_property pp
            LEFT JOIN publisher_property_type pt ON pt.id = pp.property_type_id
            LEFT JOIN publisher_property_group pg ON pg.id = pt.group_id
            WHERE pg.key = '$propertyGroupKey'
            AND pp.publisher_id = $publisherId
SQL;
        $secretStringData = $this->dbAdapter->query($sqlString, $this->dbAdapter::QUERY_MODE_EXECUTE)->toArray();
        if (empty($secretStringData)) {
            return '';
        }

        $secretString = $secretStringData[0]['value'] ?? '';
        if (empty($secretString)) {
            return '';
        }
        $payload = [
            'uId' => $this->userData['id'] ?? 0,
            'uLogin' => $this->userData['login'] ?? '',
            'pId' => $publisherId,
            'iat' => time()
        ];

        $token = JWT::encode($payload, $secretString, self::JWT_ALG);
        return $token;
    }


}