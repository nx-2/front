<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Service;

use Exception;
use SoapClient;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Ws1c
{
    public $client;

    public function __construct($timeout = 0, $options = [])
    {
        $defaultOptions = [
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
        ];
        $url = (is_array($options) && isset($options['url'])) ? $options['url'] : false;

        if (!$url) {
            $this->client = false;
            return false;
        }

        unset($options['url']);

        $options = array_merge($defaultOptions, $options);
        if (!empty($timeout)) {
            $options['connection_timeout'] = $timeout;
            $old = ini_set('default_socket_timeout', $timeout);
        }
        try {
            $client = new SoapClient($url, $options);
        } catch (Exception $e) {
            if (!empty($timeout)) {
                ini_set('default_socket_timeout', $old);
            }
            $this->client = false;
            return false;
        }
        if (!empty($timeout)) {
            ini_set('default_socket_timeout', $old);
        }
        $this->client = $client;
    }

    public function __call($name, $arguments)
    {
        //return $this->client->{$name}($arguments);
        return call_user_func_array([$this->client, $name], $arguments);
    }

    public function __set($key, $val)
    {
        $this->client->$key = $val;
    }

    public function __get($key)
    {
        return $this->client->$key;
    }
}
