<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Service;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;

class Action extends AbstractService
{
    public $defaultCcID = 1094;

    public function prepareData($data)
    {
        $result = [];
        if (!empty($data)) {
            foreach ($data as $i => $cur) {
                if ($cur['delivery_type_id'] == 5) {
                    $cur['pdfMatter'] = 1;
                    $cur['printMatter'] = 0;
                } else {
                    $cur['pdfMatter'] = 0;
                    $cur['printMatter'] = 1;
                }
                if (empty($result)) {
                    $result = [$cur];
                } else {
                    $joined = 0;
                    foreach ($result as $j => $curj) {
                        if ($cur['periodical_id'] == $curj['periodical_id'] && $cur['period'] == $curj['period'] && $cur['date_start'] = $curj['date_start'] && $cur['pdfMatter'] && empty($curj['pdfMatter'])) {
                            $result[$j]['pdfMatter'] = 1;
                            $joined = 1;
                            break;
                        }
                        if ($cur['periodical_id'] == $curj['periodical_id'] && $cur['period'] == $curj['period'] && $cur['date_start'] = $curj['date_start'] && $cur['printMatter'] && empty($curj['printMatter'])) {
                            $result[$j]['printMatter'] = 1;
                            $joined = 1;
                            break;
                        }
                    }
                    if (!$joined) {
                        $result[] = $cur;
                    }
                }
            }
        }
        return $result;
    }

    public function getAction($selection, $code = '', $actionID = 0, $cc = 0, $mode = '')
    {
        if (!$cc) {
            $cc = $this->defaultCcID;
        }
        $actions = $this->mapper->getActions(['id' => $actionID, 'cc' => $cc, 'is_current' => 1, 'checked' => 1]);
        $actions = $actions->count() ? $actions->toArray() : [];
        $actnum = -1;
        $selected_ids = array_map(create_function('$u', 'return (int)$u["periodical_id"];'), $selection);
        for ($i = 0; $i < count($actions); $i++) {
            $count = 0;
            $check = 1;
            $action = $actions[$i];
            $dtable = $action['discount_table'];
            $modeOR = $action['is_grouped'] == '1' ? false : true;
            $bind = [];
            //проверяем условия акции с указанными журналами id!=-1 && id!=0
            for ($j = 0; $j < count($dtable); $j++) {
                $row = $dtable[$j];
                if ($row[0] == -1 || $row[0] == 0) {
                    continue;
                }
                $count++;
                if (!in_array($row[0], $selected_ids)) {
                    $check = 0;
                    if ($modeOR) {
                        continue;
                    } else {
                        break;
                    }
                } else {
                    if (!isset($row[1])) {
                        $row[1] = 0;
                    }
                    if (!isset($row[2])) {
                        $row[2] = 0;
                    }
                    $rowcheck = 0;
                    foreach ($selection as $prop => $selectionrow) {
                        if (!(
                            (($row[1] || $row[2]) && ($selectionrow['printMatter'] != $row[1]) && ($row[1] || $selectionrow['printMatter'])) ||
                            (($row[2] || $row[1]) && ($selectionrow['pdfMatter'] != $row[2]) && ($row[2] || $selectionrow['pdfMatter'])) ||
                            //|| ($selection[$j]['pdfMatter'] && $selection[$j]['printMatter'])
                            ($row[3] && (int)$row[3] && $selectionrow['period'] != $row[3])
                        )
                        ) {
                            $rowcheck = 1;
                            $bind[$prop] = 1;
                            break;
                        }
                    }
                    if (!$rowcheck && !$modeOR) {
                        $check = 0;
                        break;
                    }
                    if (!$rowcheck && $modeOR) {
                        $check = 0;
                    }
                    if ($rowcheck && $modeOR) {
                        $check = 1;
                        break;
                    }
                }
            }
            /*$dtmagazines = array();
            for($j=0; $j<count($dtable); $j++)
            {
                $row = $dtable[$j];
                if($row[0] == -1 || $row[0] == 0) continue;
                if(!is_array($dtmagazines[$row[0]])) $dtmagazines[$row[0]] = array();
                $dtmagazines[$row[0]][] = $row;
            }*/

            //проверяем условия акции с указанными журналами id!=-1 && id!=0
            /*foreach($dtmagazines as $j=>$rows)
            {
                $count++;
                if(!in_array($j,$selected_ids)) { $check=0; if($modeOR) continue; else break; }
                else
                {
                    $bind[$j] = 1; //временный флаг, о том что строку просмотрели

                    $rowcheck=0;
                    for($k=0; $k<count($rows); $k++)
                    {
                        $inrow = $rows[$k];
                        if(!(
                                (
                                    (($inrow[1] || $inrow[2]) && ($selection[$j]['printMatter']!=$inrow[1]) && ($inrow[1] || $selection[$j]['printMatter'])) &&
                                    (($inrow[2] || $inrow[1]) && ($selection[$j]['pdfMatter']!=$inrow[2]) && ($inrow[2] || $selection[$j]['pdfMatter']))
                                )
                                //|| ($selection[$j]['pdfMatter'] && $selection[$j]['printMatter'])
                                || ($inrow[3] && (int)$inrow[3] && $selection[$j]['period']!=$inrow[3])
                            )
                        ) { $rowcheck=1; break; }
                    }

                    if(!$rowcheck && !$modeOR) { $check=0; break; }
                    if(!$rowcheck && $modeOR) { $check=0; }
                    if($rowcheck && $modeOR) {$check=1; break;}
                }
            }*/
            if (!$modeOR) {
                for ($j = 0; $j < count($dtable); $j++) //проверяем условия с любым журналом id=0
                {
                    $row = $dtable[$j];
                    if ($row[0] != 0) {
                        continue;
                    }
                    $count++;
                    $anycheck = 0;
                    foreach ($selection as $prop => $selectionrow) {
                        if ((!$bind[$prop]) &&
                            ((!$row[1] && !$row[2]) || $selection[$prop]['printMatter'] == $row[1] || (!$row[1] && !$selection[$prop]['printMatter'])) &&
                            ((!$row[2] && !$row[1]) || $selection[$prop]['pdfMatter'] == $row[2] || (!$row[2] && !$selection[$prop]['pdfMatter'])) &&
                            (!$row[3] || !(int)$row[3] || $selection[$prop]['period'] == $row[3])
                        ) {
                            $anycheck = 1;
                            $bind[$prop] = 1;
                            break;
                        }
                    }
                    if (!$anycheck) {
                        $check = 0;
                        break;
                    }
                }
            }
            $selcount = 0;
            foreach ($selection as $cur) {
                if ((int)$cur['period'] && ($cur['printMatter'] || $cur['pdfMatter'])) {
                    $selcount++;
                }
            }
            if ($modeOR) {
                if ($selcount > 1) $check = 0;
            } else {
                if ($count != $selcount) {
                    $check = 0;
                }
            }
            if ($check) {
                if ($action['promocodes'] && $mode != 'no_code') {
                    if ($code) {
                        for ($j = 0; $j < count($action['promocodes']); $j++) {
                            if ($action['promocodes'][$j] == md5($code)) {
                                $actnum = $i;
                                break;
                            }
                        }
                    }
                    if ($actnum == -1) {
                        $check = 0;
                    } else {
                        break;
                    }
                } else {
                    $actnum = $i;
                }
            }
        }
        if ($actnum == -1) {
            return false;
        } else {
            return $actions[$actnum];
        }
    }

}
