<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Service;

use Exception;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
use function nx\functions\objectToArray;

//use Zend\Db\RowGateway\RowGateway;

class Company extends AbstractService
{
    public $ws1c_client;

    public function sendTo1c(array $params = [])
    {
        //$cur_time    = time();
        //$updated_end = date('Y-m-d', mktime(0, 0, 0, date('n', $cur_time), date('j', $cur_time)-1, date('Y', $cur_time)));
        $by = ['ws1c_synced' => 0, 'limit' => 25, 'columns' => ['id', 'inn', 'kpp', 'name'],
            'columns_load' => [
                'addr' => ['phone', 'address', 'zipcode', 'country_id', 'area', 'region', 'city', 'telcode'],
                'laddr' => ['phone', 'address', 'zipcode', 'country_id', 'area', 'region', 'city', 'telcode']
            ]
        ];//, 'order' => 'P.last_updated ASC');
        $by['id'] = $params['ids'];
        $companies = $this->mapper->getCompaniesBy($by);
        $results = [];
        $companies_data = [];
        foreach ($companies as $i => $company) {
            $company_data = [];

            if (empty($company->inn)) {
                $results[$company->id] = ['error' => 'NX: не заполнен ИНН'];
                continue;
            }
            if (empty($company->name)) {
                $results[$company->id] = ['error' => 'NX: не заполнено название'];
                continue;
            }
            $contractor = [
                'INN' => $company->inn,
                'KPP' => $company->kpp ? $company->kpp : '',//'000000000'
                'Name' => $company->name
            ];

            $phone = !empty($company->addr->phone) ? $company->addr->phone : (!empty($company->laddr->phone) ? $company->laddr->phone : '');
            $telcode = !empty($company->addr->telcode) ? $company->addr->telcode : (!empty($company->laddr->telcode) ? $company->laddr->telcode : '');
            $phone = [
                'Code' => $telcode,
                'Number' => $phone
            ];

            //address process
            if (empty($company->addr->address) && empty($company->laddr->address)) {
                $results[$company->id] = ['error' => 'NX: не заполнен адрес'];
                continue;
            }
            $address_fields = ['zipcode', 'area', 'region', 'city', 'city1', 'address'];
            //if(!empty($company->country_id) && $company->country_id != \nx\Model\Address::COUNTRY_ID_RUSSIA) {
            //    array_unshift($address_fields, 'country_name');
            //}
            $actual_address = '';
            foreach ($address_fields as $field) {
                $actual_address .= (!empty($company->addr->$field) ? $company->addr->$field . ', ' : '');
            }
            $actual_address = mb_substr($actual_address, 0, -2);

            //$address_fields = array('la_zipcode', 'la_area', 'la_region', 'la_city', 'la_city1', 'la_address');
            //if(!empty($company->country_id) && $company->country_id != \nx\Model\Address::COUNTRY_ID_RUSSIA) {
            //    array_unshift($address_fields, 'country_name');
            //}
            $legal_address = '';
            foreach ($address_fields as $field) {
                $legal_address .= (!empty($company->laddr->$field) ? $company->laddr->$field . ', ' : '');
            }
            $legal_address = mb_substr($legal_address, 0, -2);

            if (empty($company->addr->address)) {
                $actual_address = $legal_address;
            }
            if (empty($company->laddr->address)) {
                $legal_address = $actual_address;
            }

            $company_data = [
                'Contractor' => $contractor,
                'LegalAddr' => $legal_address,
                'ActualAddr' => $actual_address,
                'Phone' => $phone
            ];
            $companies_data[] = $company_data;
            $companies_data_by[$company_data['Contractor']['INN'] . '_' . $company_data['Contractor']['KPP']] = $company;
        }
        if (!empty($companies_data)) {
            try {
                /*$options = array(
                    'soap_version' => SOAP_1_1,
                    'exceptions'   => true,
                    'trace'        => 1,
                    'cache_wsdl'   => WSDL_CACHE_NONE,
                    //'login'      => 'NxUser',
                    //'password'   => 'XVb0LJmE9h'
                    'login'        => 'test',
                    'password'     => '0'
                );
                $client = new \SoapClient('http://teta.osp.ru:8081/complexTest/ws/ws1.1cws?wsdl', $options);*/
                $client = $this->ws1c_client;

                $this->mapper->logger->log_console($companies_data);
                $result = $client->Contacts(
                    [
                        'ContactsList' => $companies_data
                    ]
                );
                $this->mapper->logger->log_console($result);
                $result = $result->return->КонтактОтвет;
                if (is_object($result)) {
                    $result = [$result];
                }
                $result = objectToArray($result);
                foreach ($result as $item) {
                    $company = $companies_data_by[$item['Contractor']['INN'] . '_' . $item['Contractor']['KPP']];
                    if (!(int)$item['IsError']) {
                        $results[$company->id] = ['error' => ''];
                        $data['ws1c_synced'] = 1;
                        $this->mapper->update($data, ['id' => $company->id]);
                    } else {
                        $results[$company->id] = ['error' => '1C: ' . $item['Decoding']];
                    }
                }
            } catch (Exception $e) {
                var_dump($e->getMessage());
                throw $e;
            }
        }
        //return array('results' => $results, 'sales_data' => $sales_data);
        return $results;
    }
}
