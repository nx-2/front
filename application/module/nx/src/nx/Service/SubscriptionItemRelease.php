<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Service;

use nx\Model\FileStorage;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
use const nx\NDS_PRODUCT;
use const nx\NDS_SERVICE;

class SubscriptionItemRelease extends AbstractService
{
    public $orderService, $subscriptionMapper;

    public function get1CFilesForReleases($ids, $signed)
    {
        $results = [];
        foreach ($ids as $id) {
            $results[$id] = ['links' => '', 'file_ids' => [], 'request_doc' => 0];
        }
        //$shippings = $this->mapper->getShippings(array('ids' => $ids));
        $shippings = $this->mapper->getShippingsBy(['id' => $ids, 'columns' => ['id', 'subscription_type']]);
        $paper_ids = $pdf_ids = [];
        $shippings_by_rid = [];
        if (!empty($shippings)) {
            foreach ($shippings as $shipping) {
                $shippings_by_rid[$shipping['id']] = $shipping;
                if ($shipping['subscription_type'] == 'pdf') {
                    $pdf_ids[] = $shipping['id'];
                }
                if ($shipping['subscription_type'] == 'paper') {
                    $paper_ids[] = $shipping['id'];
                }
            }
            $label = $signed ? 'signed' : '';
            $files = $this->mapper->fileStorageModel->getFiles(['entity_type' => 'order_item_release', 'entity_id' => $ids, 'type' => FileStorage::TYPE_1C_INVOICE, 'label' => $label]);
            $invoice_by_rid = [];
            if (!empty($files)) {
                foreach ($files as $file) {
                    $invoices_by_rid[$file->entity_id] = $file;
                    //$results[$file->entity_id]['links'].='<a href="/file-storage/download/hash/' . $file->hash . '" target="_blank">счет-фактура</a>, ';
                }
            }
            $torg12_by_rid = [];
            if (!empty($paper_ids)) {
                $files = $this->mapper->fileStorageModel->getFiles(['entity_type' => 'order_item_release', 'entity_id' => $paper_ids, 'type' => FileStorage::TYPE_1C_TORG12, 'label' => $label]);
                if (!empty($files)) {
                    foreach ($files as $file) {
                        $torg12_by_rid[$file->entity_id] = $file;
                        //$results[$file->entity_id]['links'].='<a href="/file-storage/download/hash/' . $file->hash . '" target="_blank">тов. накладная</a>, ';
                    }
                }
            }
            $act_by_rid = [];
            if (!empty($pdf_ids)) {
                $files = $this->mapper->fileStorageModel->getFiles(['entity_type' => 'order_item_release', 'entity_id' => $pdf_ids, 'type' => FileStorage::TYPE_1C_ACT, 'label' => $label]);
                if (!empty($files)) {
                    foreach ($files as $file) {
                        $act_by_rid[$file->entity_id] = $file;
                        //$results[$file->entity_id]['links'].='<a href="/file-storage/download/hash/' . $file->hash . '" target="_blank">акт</a>, ';
                    }
                }
            }
            foreach ($ids as $id) {
                $request_doc = 0;
                if (!empty($invoices_by_rid[$id]->hash)) {
                    $results[$id]['file_ids'][] = $invoices_by_rid[$id]->id;
                    $results[$id]['links'] .= '<a href="/file-storage/download/hash/' . $invoices_by_rid[$id]->hash . '" target="_blank">счет-фактура</a>, ';
                } else {
                    $request_doc = 1;
                }
                if ($shippings_by_rid[$id]['subscription_type'] == 'pdf') {
                    if (!empty($act_by_rid[$id]->hash)) {
                        $results[$id]['file_ids'][] = $act_by_rid[$id]->id;
                        $results[$id]['links'] .= '<a href="/file-storage/download/hash/' . $act_by_rid[$id]->hash . '" target="_blank">акт</a>, ';
                    } else {
                        $request_doc = 1;
                    }
                }
                if ($shippings_by_rid[$id]['subscription_type'] == 'paper') {
                    if (!empty($torg12_by_rid[$id]->hash)) {
                        $results[$id]['file_ids'][] = $torg12_by_rid[$id]->id;
                        $results[$id]['links'] .= '<a href="/file-storage/download/hash/' . $torg12_by_rid[$id]->hash . '" target="_blank">накладная</a>, ';
                    } else {
                        $request_doc = 1;
                    }
                }
                $results[$id]['request_doc'] = $request_doc;
                /*if($request_doc)
                {
                    $results[$id]['links']='в очереди на док-ты, ' . $results[$id]['links'];
                    $this->itemReleaseModel->update(array('is_doc_wait' => 1),array('id' => $id));
                }*/
                $results[$id]['links'] = mb_substr($results[$id]['links'], 0, -2);
            }
        }
        return $results;
    }

    public function get1CDocsForReleases($params)
    {
        $results = [];
        $user_id = !empty($params['user_id']) ? (int)$params['user_id'] : 0;
        $ids = $params['ids'];
        //$shippings = $this->mapper->getShippings(array('ids' => $ids));
        $shippings = $this->mapper->getShippingsBy(['id' => $ids, 'columns' => ['id', 'doc_result', 'is_doc_wait']]);
        $shippings_by_id = [];
        if (empty($shippings)) {
            return $results;
        }
        foreach ($shippings as $shipping) {
            $shippings_by_id[$shipping['id']] = $shipping;
        }

        $signed = isset($params['signed']) ? (bool)$params['signed'] : false;
        $files_res = $this->get1CFilesForReleases($ids, $signed);
        if (!empty($files_res)) {
            foreach ($files_res as $release_id => $result) {
                $results[$release_id] = $result;
                if (empty($result['request_doc'])) {
                    unset($ids[array_search($release_id, $ids)]);
                }
            }
        }
        if (!empty($ids)) {
            //return $results;
            $data_res = $this->get1CDocsData_Release(['ids' => $ids, 'prefix' => 'release']);
            $data_res = $data_res['results'];
            if (!empty($data_res)) {
                foreach ($data_res as $release_id => $result) {
                    if (!empty($result['error'])) {
                        $results[$release_id]['error'] = $result['error'];
                        unset($ids[array_search($release_id, $ids)]);
                    }
                }
            }
        }
        if (!empty($ids)) {
            //return $results;
            foreach ($ids as $release_id) {
                if (!empty($shippings_by_id[$release_id]['doc_result'])) {
                    $results[$release_id]['error'] = $shippings_by_id[$release_id]['doc_result'];
                    unset($ids[array_search($release_id, $ids)]);
                }
            }
        }
        if (!empty($ids)) {
            $this->mapper->update(['is_doc_wait' => 1], ['id' => $ids]);
            foreach ($ids as $release_id) {
                $shippings_by_id[$release_id]['is_doc_wait'] = 1;
                //$results[$release_id]['links'] = 'в очереди на док-ты' . ($results[$release_id]['links'] ? ', ' : '') . $results[$release_id]['links'];
            }
        }
        foreach ($results as $release_id => $result) {
            if (!empty($shippings_by_id[$release_id]['is_doc_wait'])) {
                $results[$release_id]['links'] = 'в очереди на док-ты' . ($results[$release_id]['links'] ? ', ' : '') . $results[$release_id]['links'];
            }
        }

        return $results;
    }

    public function get1CDocsData_Release(array $params)
    {
        $results = [];
        $ids = $params['ids'];
        if (empty($ids)) {
            return false;
        }
        $sales_data = [];
        foreach ($ids as $release_id) {
            $release = $this->mapper->getReleaseBy(['id' => $release_id]);
            if (empty($release['id'])) {
                $results[$release_id] = ['error' => 'Отгрузка с id = ' . $release_id . ' не найдена'];
                continue;
            }
            // if(empty($release['shipment_id'])) {
            //     $results[$release_id] = array('error' => 'Отгрузка с id = ' . $release_id . ' не была сформирована');
            //     continue;
            // }
            //$results[$release_id]['doc_result'] = $release['doc_result'];
            $subscription_id = $release['subscription_id'];
            $subscription = $this->subscriptionMapper->getBy(['id' => $subscription_id]);
            if (empty($subscription->id)) {
                $results[$release_id] = ['error' => 'Заказ с id = ' . $subscription_id . ' не найден'];
                continue;
            }
            if ($subscription->subscriber_type_id != 2) {
                $results[$release_id] = ['error' => 'Заказ с id = ' . $subscription_id . ' не на юр. лицо'];
                continue;
            }
            if ($subscription->type != 0) {
                $results[$release_id] = ['error' => 'Заказ с id = ' . $subscription_id . ' не оплачивается'];
                continue;
            }
            $items = $this->subscriptionMapper->getItems(['subscription_id' => $subscription_id]);
            if (empty($items)) {
                $results[$release_id] = ['error' => 'Не найдены позиции у заказа с id = ' . $subscription_id];
                continue;
            }
            if (!empty($release['type']) && $release['type'] == 'pdf') {
                $releases = $this->subscriptionMapper->itemReleaseModel->getShippingsBy(['item_id' => $release['item_id']]);
                if (!empty($releases)) {
                    $release_ids = [];
                    foreach ($releases as $cur) {
                        $release_ids[] = $cur['id'];
                    }
                    $files = $this->subscriptionMapper->fileStorageModel->getFiles(['entity_id' => $release_ids, 'entity_type' => 'order_item_release', 'type' => [1, 2, 3, 4]]);
                    $check = $files->count();
                    if (empty($check)) {
                        $results[$release_id] = ['error' => 'Позиция с id = ' . $release['item_id'] . ' должна отгружаться как pdf услуга на период, а не поштучно'];
                        continue;
                    }
                }
            }
            $doc_sale_rows = [];
            $is_pdf = false;
            foreach ($items as $item) {
                $name = '';
                if ($item['id'] != $release['item_id']) {
                    continue;
                }
                if (!in_array($item['type'], ['paper', 'pdf'])) {
                    continue;
                }

                /*        $text_info='версия журнала';
                        if($item['EnglishName']=='Classmag' || $item['EnglishName']=='ponymashka' ){
                            $text_info='Газета';
                        }*/
                $is_pdf = $item['type'] == 'pdf';
                $issue_title = explode('/', trim(trim($release['issue_title']), '#'));
                $issue_title = $issue_title[0];
                $quantity = $item['quantity'];
                $name = ($item['type'] == 'paper' ? 'Бумажная' : 'Электронная') . ' версия журнала «' . $item['periodical_name'] . '» ' . $release['year'] . ' ' . $issue_title;
                $price = $item['unit_price'];
                // if($item['type'] == 'pdf') {
                //     $name  = 'Доступ к электронной версии «'.$item['periodical_name'].'» с '.date('d.m.Y', strtotime($item['date_start'])).' на '.$item['period'].'мес.';
                //     $price = $item['discount_price'];
                // }
                $nds = $item['type'] == 'paper' ? NDS_PRODUCT : NDS_SERVICE;
                $doc_sale_row = [
                    'Nomenclature' => $name,//название товара
                    'Quantity' => $quantity,
                    'Price' => $price,
                    'NDS' => $nds,
                    'periodical_name' => $item['periodical_name'],
                    'year' => $release['year'],
                    'issue_num' => $issue_title, //$release['issue_num']
                    'SeasonTicketDataStart' => $item['date_start'],
                    'SeasonTicketPeriod' => $item['type'] == 'pdf' ? $item['period'] : 0,
                ];
                $doc_sale_rows[] = $doc_sale_row;
            }
            if (empty($doc_sale_rows)) {
                $results[$release_id] = ['error' => 'Не определены позиции заказа для отправки в 1с'];
                continue;
            }
            if (count($doc_sale_rows) == 1) {
                $doc_sale_rows = $doc_sale_rows[0];
            }
            if (empty($subscription->payment_agent_ws1c_id)) {
                $results[$release_id] = ['error' => 'У заказа неизвестный 1c id платежного агента'];
                continue;
            }
            $payment_agent_ws1c_id = $subscription->payment_agent_id == 2 ? '000000002' : $subscription->payment_agent_ws1c_id; //ZAO to OOO
            $organization = ['ID' => $payment_agent_ws1c_id];
            //if($subscription->subscriber_type_id==2)
            //{
            if (empty($subscription->company_inn) || empty($subscription->company_name)) {// || empty($subscription->company_kpp)) {
                $results[$release_id] = ['error' => 'Не заполнены данные юр. лица в NX'];
                continue;
            }
            $contractor = [
                'INN' => $subscription->company_inn,
                'KPP' => !empty($subscription->company_kpp) ? $subscription->company_kpp : '',
                'Name' => $subscription->company_name
            ];
            //}
            /*else
            {
                if(empty($subscription->person_name)) {
                    $results[$release_id] = array('error' => 'Не заполнены данные физ. лица в NX');
                    continue;
                }
                $contractor = array(
                    'INN'  => '0000000000',
                    'KPP'  => '000000000',
                    'Name' => $subscription->person_name
                );
            }*/
            if (empty($subscription->payment_date) || $subscription->payment_date == '0000-00-00') {
                $results[$release_id] = ['error' => 'Не проставлена дата оплаты у заказа в NX'];
                continue;
            }
            $payment_date = date('Y-m-d', strtotime($subscription->payment_date));
            if (empty($release['shipment_date']) || $release['shipment_date'] == '0000-00-00 00:00:00') {
                $results[$release_id] = ['error' => 'Не проставлена дата реализации у отгрузки в NX'];
                continue;
            }
            $shipment_date = date('Y-m-d\TH:i:s', strtotime($release['shipment_date']));
            $sale_id_prefix = !empty($params['prefix']) ? $params['prefix'] : 'release';
            $sale = [
                'document_date' => $is_pdf ? date('Y-m-d\TH:i:s') : $shipment_date,
                'SaleId' => $sale_id_prefix . $release_id,
                'Organization' => $organization,
                'Contractor' => $contractor,
                'DocSaleRow' => $doc_sale_rows,
                'Update' => true,
                'payment_date' => $payment_date,
                'OnlyBill' => false
            ];
            $sales_data[] = $sale;
        }
        return ['results' => $results, 'sales_data' => $sales_data];
    }

    public function get1CDocsForRelease($params)
    {
        $results = [];
        $sales_data = [];
        $user_id = !empty($params['user_id']) ? (int)$params['user_id'] : 0;
        $publisherId = isset($params['publisher_id']) ? $params['publisher_id'] : false;
        $data = $this->get1CDocsData_Release(['ids' => $params['ids'], 'prefix' => 'release']);
        if ($data) {
            $results = $data['results'];
            $sales_data = $data['sales_data'];
        }
        if (!empty($sales_data)) {
            return $this->orderService->request1CDocs([
                'bill' => true,
                'selling' => true,
                'invoice' => true,
                'signed' => isset($params['signed']) ? (bool)$params['signed'] : false,
                'type' => 'order_item_release',
                'prefix' => 'release',
                'data' => $sales_data,
                'user_id' => $user_id,
                'publisher_id' => $publisherId,
            ]);
        }
        return $results;
    }

    public function getShippingsTable($params = [])
    {
        $cache = $this->mapper->getCache(__FUNCTION__, func_get_args());
        if ($cache) {
            return $cache;
        }

        $shippings = $this->mapper->getShippingsForTable($params);

        $items = [];

        $shippings_by_yearmonth = [];
        foreach ($shippings as $ship) {
            $yearmonth = $ship['year'] . str_pad($ship['release_month'], 2, '0', STR_PAD_LEFT);
            if (!isset($shippings_by_yearmonth[$yearmonth])) {
                $shippings_by_yearmonth[$yearmonth] = [$ship];
            } else {
                $shippings_by_yearmonth[$yearmonth][] = $ship;
            }
        }
        foreach ($shippings_by_yearmonth as $yearmonth => $shippings) {
            $row = ['yearmonth' => $yearmonth];
            foreach ($shippings as $i => $ship) {
                $row[$ship['periodical_name']][$ship['issue_num']]['state'] = (int)$ship['state'];
                $row[$ship['periodical_name']][$ship['issue_num']]['types']['paper'] = (int)$ship['count_paper'];
                $row[$ship['periodical_name']][$ship['issue_num']]['types']['pdf'] = (int)$ship['count_pdf'];
                $row[$ship['periodical_name']][$ship['issue_num']]['total_price'] = (float)$ship['sum_total_price'];
            }
            $items[] = $row;
        }

        $result = [
            'items' => $items,
            'total' => count($items)
        ];

        $this->mapper->setCache($result, __FUNCTION__, func_get_args(), 300);

        return $result;
    }
}
