<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Service;

use Exception;
use nx\Model\FileStorage;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
use function nx\functions\getPlural;
use function nx\functions\makedir;
use const nx\NDS_PRODUCT;
use const nx\NDS_SERVICE;

//use Zend\Db\RowGateway\RowGateway;

class Order extends AbstractService
{
    public $ws1c_client, $subscriptionMapper, $mailTemplatesModel, $issueOrderMapper, $publisherModel;

    public function get1CDocsData_Order(array $params)
    {
        $sales_data = [];
        $id = $params['id'];
        $subscription = $this->subscriptionMapper->getBy(['id' => $id]); //$subscription - это заказ на самом деле. Т.е. экземпляр модели Order (не Subscription !!!)
        if (empty($subscription->id)) {
            throw new Exception('Заказ с id = ' . $id . ' не найден');
        }
        if ($subscription->subscriber_type_id != 2) {
            throw new Exception('Заказ не на юр. лицо' . $subscription->subscriber_type_id);
        }
        if ($subscription->type != 0) {
            throw new Exception('Заказ не оплачивается' . $subscription->type);
        }
        $items = $this->subscriptionMapper->getItems(['subscription_id' => $id]);
        if (empty($items)) {
            throw new Exception('Заказ пуст');
        }
        $doc_sale_rows = [];
        foreach ($items as $item) {
            $name = '';
            if (!in_array($item['type'], ['paper', 'pdf'])) {
                continue;
            }
            $quantity = $item['quantity'];
            $name = 'Подписка на ' . ($item['type'] == 'paper' ? 'бумажную' : 'электронную') . ' версию журнала «' . $item['periodical_name'] . '» (' . $item['period'] . ' ' . getPlural((int)$item['period'], 'месяц', 'месяца', 'месяцев') . ' - с ' . date('d.m.Y', strtotime($item['date_start'])) . '); всего - ' . $item['rqty'] . ' ' . getPlural($item['rqty'], 'номер', 'номера', 'номеров') . '.';
            $price = $item['discount_price'];
            $nds = $item['type'] == 'paper' ? NDS_PRODUCT : NDS_SERVICE;
            $doc_sale_row = [
                'Nomenclature' => $name,//название товара
                'Quantity' => $quantity,
                'Price' => $price,
                'NDS' => $nds,
                'periodical_name' => '',
                'year' => '',
                'issue_num' => '',
                'SeasonTicketDataStart' => '',
                'SeasonTicketPeriod' => '',
            ];
            $doc_sale_rows[] = $doc_sale_row;
        }
        if (empty($doc_sale_rows)) {
            throw new Exception('Неопределены позиции заказа для отправки в 1с');
        }
        if (count($doc_sale_rows) == 1) {
            $doc_sale_rows = $doc_sale_rows[0];
        }
        if (empty($subscription->payment_agent_ws1c_id)) {
            throw new Exception('У заказа неизвестный 1c id платежного агента');
        }
        $payment_agent_ws1c_id = $subscription->payment_agent_id == 2 ? '000000002' : $subscription->payment_agent_ws1c_id; //ZAO to OOO
        $organization = ['ID' => $payment_agent_ws1c_id];
        if (empty($subscription->company_inn) || empty($subscription->company_name)) {// || empty($subscription->company_kpp)) {
            throw new Exception('Не заполнены данные юр. лица');
        }
        $contractor = [
            'INN' => $subscription->company_inn,
            'KPP' => !empty($subscription->company_kpp) ? $subscription->company_kpp : '',
            'Name' => $subscription->company_name
        ];
        if (empty($subscription->payment_date) || $subscription->payment_date == '0000-00-00') {
            throw new Exception('У заказа не проставлена дата оплаты');
        }
        $payment_date = date('Y-m-d', strtotime($subscription->payment_date));
        $sale_id_prefix = !empty($params['prefix']) ? $params['prefix'] : 'order';
        $sale = [
            'document_date' => $payment_date,
            'SaleId' => $sale_id_prefix . $id,
            'Organization' => $organization,
            'Contractor' => $contractor,
            'DocSaleRow' => $doc_sale_rows,
            'Update' => true,
            'payment_date' => $payment_date,
            'OnlyBill' => true
        ];
        $sales_data[] = $sale;
        return $sales_data;
    }

    public function get1CDocsForOrder(array $params)
    {
        $user_id = !empty($params['user_id']) ? (int)$params['user_id'] : 0;
        $sales_data = $this->get1CDocsData_Order(['id' => $params['id'], 'prefix' => 'order']);
        if (!empty($sales_data)) {
            return $this->request1CDocs([
                'bill' => isset($params['bill']) ? (bool)$params['bill'] : false,
                'selling' => isset($params['selling']) ? (bool)$params['selling'] : false,
                'invoice' => isset($params['invoice']) ? (bool)$params['invoice'] : false,
                'signed' => isset($params['signed']) ? (bool)$params['signed'] : false,
                'type' => 'order',
                'prefix' => 'order',
                'data' => $sales_data,
                'user_id' => $user_id,
                'order_id' => $params['id'],
            ]);
        }
        return false;
    }

    public function request1CDocs($params)
    {
        ini_set("max_execution_time", "90");
        $results = [];
        if (empty($params['data']) || empty($params['type']) || empty($params['prefix'])) {
            return false;
        }
        try {
            $sales_data = $params['data'];
            $sale_id_prefix = $params['prefix'];
            $entity_type = $params['type'];

            //ожидаем в параметрах либо ИД издателя либо ИД заказа. Это нужно, чтобы связываться с определенным 1с, соответствующим данному издателю (если это возможно)
            $publisher = false;
            if (isset($params['publisher_id'])) {
                $publisherId = intval($params['publisher_id']);
                unset($params['publisher_id']);
                $publisher = $this->publisherModel->getOne($publisherId);

                if (isset($params['order_id']))
                    unset($params['order_id']);

            }

            if ((!$publisher || empty($publisher)) && isset($params['order_id'])) {
                $orderId = intval($params['order_id']);
                unset($params['order_id']);
                $publisher = $this->mapper->getOwner($orderId);
            }

            $integrationVerified = isset($publisher['1c_integration_verified']) ? $publisher['1c_integration_verified'] : false;
            $soapOptions = isset($publisher['integration_1c']) ? $publisher['integration_1c'] : false;
            if (!$publisher || empty($publisher) || !$integrationVerified || !$soapOptions) {
                $client = false;
            } else {
                $client = new Ws1c(2, $soapOptions);
            }

//            $client = $this->ws1c_client;

            $sale_params = [
                'Bill' => isset($params['bill']) ? (bool)$params['bill'] : false,
                'Selling' => isset($params['selling']) ? (bool)$params['selling'] : false,
                'Invoice' => isset($params['invoice']) ? (bool)$params['invoice'] : false,
                'Format' => !empty($params['format']) ? $params['format'] : '',//htm
                'Signed' => isset($params['signed']) ? (bool)$params['signed'] : false
            ];
            $request = [
                'Sale' => [
                    'DocSale' => $sales_data,
                    'PrintParams' => $sale_params
                ]
            ];
            $this->mapper->logger->log_console($request);//print_r($request);die;
            if ($client && empty($client->client)) {
                foreach ($sales_data as $item) {
                    $entity_id = str_replace($sale_id_prefix, '', $item['SaleId']);
                    $results[$entity_id] = ['error' => '', 'links' => '', 'request' => ''];
                    $results[$entity_id]['error'] = 'ws1c was not responded';
                    $results[$entity_id]['request'] = $request;
                }
            } else {
                $result = $client->SaleFromNX($request);
                $this->mapper->logger->log_console($result);
                $result = $result->return->ResponseSaleRow;
                if (is_object($result)) {
                    $result = [$result];
                }
                foreach ($result as $item) {
                    $item = (array)$item;
                    if (!empty($item['SaleId'])) {
                        //$entity_type = 'order_item_release';
                        $entity_id = str_replace($sale_id_prefix, '', $item['SaleId']);
                        $results[$entity_id] = ['error' => '', 'links' => '', 'request' => ''];
                        if ($item['IsError'] || $item['Komm'] == 'InXpress') {//save request only for errors to optimize table disk space
                            $results[$entity_id]['error'] = $item['Komm'];
                            $results[$entity_id]['request'] = $request;
                            continue;
                        }

                        $dir = 'data/' . $entity_type . '/' . $entity_id;
                        makedir($dir);
                        $label = $sale_params['Signed'] ? 'signed' : '';
                        $fileStorageData = [
                            'entity_type' => $entity_type,
                            'entity_id' => $entity_id,
                            'create_user_id' => !empty($params['user_id']) ? (int)$params['user_id'] : 0,
                            'label' => $label
                        ];
                        if (!empty($item['Bill'])) {
                            $data = [
                                'fileStorageData' => array_merge($fileStorageData, [
                                    'name' => 'счет_' . $entity_id . '.pdf',
                                    'path' => $dir . '/bill' . ($label ? '_' . $label : '') . '.pdf',
                                    'type' => FileStorage::TYPE_1C_BILL,
                                ])
                            ];
                            list($results[$entity_id]['bill_file_id'], $results[$entity_id]['bill_file_hash']) = $this->mapper->fileStorageModel->putContents($item['Bill'], $data);
                            $results[$entity_id]['links'] .= '<a href="/file-storage/download/hash/' . $results[$entity_id]['bill_file_hash'] . '" target="_blank">счет</a>, ';
                        }
                        if (!empty($item['Act'])) {
                            $data = [
                                'fileStorageData' => array_merge($fileStorageData, [
                                    'name' => 'акт_' . $entity_id . '.pdf',
                                    'path' => $dir . '/act' . ($label ? '_' . $label : '') . '.pdf',
                                    'type' => FileStorage::TYPE_1C_ACT,
                                ])
                            ];
                            list($results[$entity_id]['act_file_id'], $results[$entity_id]['act_file_hash']) = $this->mapper->fileStorageModel->putContents($item['Act'], $data);
                            $results[$entity_id]['links'] .= '<a href="/file-storage/download/hash/' . $results[$entity_id]['act_file_hash'] . '" target="_blank">акт</a>, ';
                        }
                        if (!empty($item['Torg12'])) {
                            $data = [
                                'fileStorageData' => array_merge($fileStorageData, [
                                    'name' => 'товарная_накладная_' . $entity_id . '.pdf',
                                    'path' => $dir . '/torg12' . ($label ? '_' . $label : '') . '.pdf',
                                    'type' => FileStorage::TYPE_1C_TORG12,
                                ])
                            ];
                            list($results[$entity_id]['torg12_file_id'], $results[$entity_id]['torg12_file_hash']) = $this->mapper->fileStorageModel->putContents($item['Torg12'], $data);
                            $results[$entity_id]['links'] .= '<a href="/file-storage/download/hash/' . $results[$entity_id]['torg12_file_hash'] . '" target="_blank">накладная</a>, ';
                        }
                        if (!empty($item['Invoice'])) {
                            $data = [
                                'fileStorageData' => array_merge($fileStorageData, [
                                    'name' => 'счет-фактура_' . $entity_id . '.pdf',
                                    'path' => $dir . '/invoice' . ($label ? '_' . $label : '') . '.pdf',
                                    'type' => FileStorage::TYPE_1C_INVOICE,
                                ])
                            ];
                            list($results[$entity_id]['invoice_file_id'], $results[$entity_id]['invoice_file_hash']) = $this->mapper->fileStorageModel->putContents($item['Invoice'], $data);
                            $results[$entity_id]['links'] .= '<a href="/file-storage/download/hash/' . $results[$entity_id]['invoice_file_hash'] . '" target="_blank">счет-фактура</a>, ';
                        }
                        $results[$entity_id]['links'] = mb_substr($results[$entity_id]['links'], 0, -2);
                    }
                }
            }
        } catch (Exception $e) {
            var_dump($e->getMessage(), $request);
            //throw $e;
            throw new Exception($e->getMessage() . serialize($request['Sale']['DocSale'][0]), $e->getCode(), $e);
        }
        return $results;
    }

    public function getStatTable($params = [])
    {
        //$cache = $this->mapper->getCache(__FUNCTION__, func_get_args());
        //if ($cache) {
        //    return $cache;
        //}

        //$shippings = $this->mapper->getShippingsForTable($params);
        $orders = $this->mapper->getOrdersBy(array_merge($params, [
                'as_array' => true,
                'order_state_id' => 3,
                'item_type' => 'Issue',
                'group' => ['M.EnglishName', new \Zend\Db\Sql\Expression('DATE_FORMAT(TBL.created, "%Y%m")')],// 'I.year', 'I.release_month', 'I.Number'),
                ///'columns'     => array('issue_num','periodical_name','year','release_month','count','sum_total_price')
                'columns' => ['created_yearmonth', 'periodical_name', 'count', 'sum_total_price']
            ]
        ));

        $items = [];

        $orders_by_yearmonth = [];
        foreach ($orders as $order) {
            //$yearmonth = $ship['year'] . str_pad($ship['release_month'], 2, '0', STR_PAD_LEFT);
            //$yearmonth = date('Ym', strtotime($order['created']));
            $yearmonth = $order['created_yearmonth'];
            if (!isset($orders_by_yearmonth[$yearmonth])) {
                $orders_by_yearmonth[$yearmonth] = [$order];
            } else {
                $orders_by_yearmonth[$yearmonth][] = $order;
            }
        }
        foreach ($orders_by_yearmonth as $yearmonth => $orders) {
            $row = ['yearmonth' => $yearmonth];
            foreach ($orders as $i => $order) {
                $row[$order['periodical_name']]['count'] = (int)$order['count'];
                $row[$order['periodical_name']]['total_price'] = (float)$order['sum_total_price'];
            }
            $items[] = $row;
        }

        $result = [
            'items' => $items,
            'total' => count($items)
        ];

        //$this->mapper->setCache($result, __FUNCTION__, func_get_args(), 300);

        return $result;
    }

    public function isProlonged($order_item_id)
    {
        $item = $this->mapper->itemModel->getItemBy(['id' => $order_item_id, 'columns' => ['order_id', 'prolong_state'], 'columns_load' => [
            'item_data' => ['nx_subscription' => ['date_end', 'periodical_id']]
        ]
        ]);
        if (empty($item)) {
            return false;
        }
        $order = $this->mapper->getOrderBy(['id' => $item->order_id, 'columns' => ['person_id', 'company_id']]);
        if (empty($order) || (empty($order->person_id) && empty($order->company_id))) {
            return false;
        }
        //if($subscription->order_state_id!=3) {
        //    return false;
        //}
        $check = false;
        if (!empty($order->person_id)) {
            $check = $this->mapper->itemModel->getItemBy([
                'operator_not_id' => $order_item_id,
                'person_id' => $order->person_id,
                'subscription_date_end_gt' => $item->item_data->date_end,
                'item_type' => 'nx_subscription',
                'periodical_id' => $item->item_data->periodical_id,
                'columns_load' => ['order' => ['order_state_id']],
                'order' => 'O.order_state_id DESC',
                'order_enabled' => 1
            ]);
        }
        if (!empty($order->company_id) && empty($check)) {
            $check = $this->mapper->itemModel->getItemBy([
                'operator_not_id' => $order_item_id,
                'company_id' => $order->company_id,
                'subscription_date_end_gt' => $item->item_data->date_end,
                'item_type' => 'nx_subscription',
                'periodical_id' => $item->item_data->periodical_id,
                'columns_load' => ['order' => ['order_state_id']],
                'order' => 'O.order_state_id DESC',
                'order_enabled' => 1
            ]);
        }
        if (empty($check) && $item->prolong_state <= 0) {
            return -1;
        }
        if ($item->prolong_state == 2 || (!empty($check) && $check->order->order_state_id == 3)) {
            return 2;
        }
        return 1;
    }

    public function getMailMessage($order_id, $template_id)
    {
        $order = $this->mapper->getOrderBy(['id' => $order_id, 'columns' => ['person' => ['name'], 'id', 'payment_date', 'payment_sum', 'email', 'shop_id']]);
        $mailTemplate = $this->mailTemplatesModel->getByID($template_id);
        $mailText = '';
        $mailSubject = '';

        if (!empty($order) && !empty($mailTemplate)) {
            $mailText = $mailTemplate['text'];
            $mailSubject = $mailTemplate['subject'];
            $replacement = [];

            if ($template_id == 6 || $template_id == 8 || $template_id == 10 || $template_id == 15) {
                $replacement = [
                    '%%USER_NAME%%' => $order->person->name,
                    '%%ORDER_DATE%%' => $order->payment_date,
                    '%%ORDER_ID%%' => $order->id,
                    '%%ORDER_PRICE%%' => $order->payment_sum
                ];
            }

            if ($template_id == 10 || $template_id == 15)// && $order->shop_id == 2)
            {
                $items = $this->issueOrderMapper->getItemsPDFIssue(['order_id' => $order_id]);
                if (!empty($items)) {
                    $content = '';
                    foreach ($items as $item) {
                        $osp_root = '/var/www/osp.ru/htdocs';
                        $path = $osp_root . $item['file_path'];
                        if (file_exists($path)) {
                            $simlink = '/download/' . md5($order->email . 'store' . $order->id . $item['file_path']) . '.pdf';
                            if (!file_exists($osp_root . $simlink)) {
                                system('ln -s ' .
                                    escapeshellarg($path) . ' ' .
                                    escapeshellarg($osp_root . $simlink)
                                );
                            }
                            $content .= '
                                <tr>
                                  <td style="vertical-align:top;">
                                    <table cellpadding="0" cellspacing="0" style="width:570px;"> 
                                      <tr>
                                        <td style="padding-top:30px; vertical-align:top; font-size:14px; padding-left:20px; font-family:Arial; line-height:20px !important;"><span style="font-weight:bold;">' . $item['periodical_name'] . ' ' . $item['issue_title'] . '</span></td>
                                      </tr>
                                      <tr>
                                        <td style="vertical-align:top; padding-top:13px; padding-left:20px;"><a href="http://www.osp.ru' . $simlink . '"><img src="http://www.osp.ru/images/mail/download.jpg" alt="download" style="border:none;" /></a></td>
                                      </tr>
                                      <tr>
                                        <td style="font-size:14px; font-family:Arial; padding-left:20px; color:#000; line-height:20px; padding-top:18px;">Или воспользуйтесь ссылкой:<br><a href="http://www.osp.ru' . $simlink . '" style="color:#0073b5;">http://www.osp.ru' . $simlink . '</a></td>
                                      </tr>
                                    </table>
                                  </td>
                                  <td style="width:108px;"><img src="http://www.osp.ru/data/' . $item['cover_image'] . '" style="border:none;" alt="title" /></td>
                                </tr>
                            ';
                        } else {
                            throw new Exception('Файл PDF для заказа не существует!');
                        }
                    }
                    $replacement['%%ORDER_CONTENT%%'] = $content;
                }
            }

            $mailText = str_replace(array_keys($replacement), array_values($replacement), $mailTemplate['text']);
            $mailSubject = str_replace(array_keys($replacement), array_values($replacement), $mailTemplate['subject']);

            $mailText = $this->mailTemplatesModel->parseADVToTemplate($template_id, $mailText);
        }

        return ['message' => $mailText, 'subject' => $mailSubject];
    }

    public function getAndSendMailMessage($order_id, $template_id)
    {
        $order = $this->mapper->getOrderBy(['id' => $order_id, 'columns' => ['email', 'id']]);
        if ($order->email) {
            $mailData = $this->getMailMessage($order_id, $template_id);
            $mailData = [
                'to_email' => $order->email,
                'from' => 'xpress@osp.ru',
                'subject' => $mailData['subject'],
                'text' => $mailData['message']
            ];
            return $this->mailTemplatesModel->create($mailData);
        }
        return false;
    }
}
