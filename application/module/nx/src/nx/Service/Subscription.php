<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Service;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
//use Zend\Db\RowGateway\RowGateway;

class Subscription extends AbstractService
{
    private $map_close_day = 0;

    public $renderer;

    public function getMapCloseDay()
    {
        /*$cache = $this->getCache(__FUNCTION__, func_get_args());
        if ($cache!==false) {
            return $cache;
        }*/
        if($this->map_close_day) {
            return $this->map_close_day;
        }
        $result     = 0;
        $cache_file = 'data/cache/mapCloseTime.txt';
        $cache      = file_exists($cache_file) ? file_get_contents($cache_file) : '';
        if (!empty($cache))
        {
            $data = unserialize($cache);
            if(!empty($data['time']) && date('n') == date('n',$data['time'])) {
                $result = date('j', $data['time']);
            }
        }
        if($result) {
            $this->map_close_day = $result;
        }
        //$this->setCache($result, __FUNCTION__, func_get_args(), 300);
        return $result;
    }

    public function toggleMapCloseDay($state)
    {
        //$this->removeCache('getMapCloseDay', array());
        $this->map_close_day = 0;
        $result     = 0;
        $cache_file = 'data/cache/mapCloseTime.txt';
        $data       = [];
        if($state) {
            $data['time'] = time();
        }
        if(file_put_contents($cache_file, serialize($data))) {
            return true;
        }
        return false;
    }

    /**
     * Проверяем по известному номеру заказа, является ли он новым
     * "Новым" считаем заказ, который создан неизвестным нам ранее контрагентом. Проверяем по другим оплаченным заказа с таким же email
     * @param integer $order_id
     * @return bool|int
     */
    public function isNew($order_id)
    {
        $subscription = $this->mapper->getBy(['id' => $order_id]); //$subscription это заказ на самом деле (Не подписка!!)
        if (empty($subscription)) {
            return false;
        }
        if (empty($subscription->email)) {
            return false;
        }
        if ($subscription->order_state_id!=3) { // 3 = оплачен
            return false;
        }
        //пытаемся найти другие оплаченные заказы с таким же email
        $check = $this->mapper->getBy(['not_id' => $subscription->id, 'email' => $subscription->email, 'order_state_id' => 3]);
        if (!empty($check)) {
            return -1;
        }
        return 1;
    }

    public function getMailMessage($subscription_id, $template_id)
    {
        $subscription       = $this->mapper->getByID($subscription_id);
        $mailTemplate       = $this->mapper->mailTemplatesModel->getByID($template_id);
        $mailText           = '';
        $mailSubject        = '';

        if(!empty($subscription) && !empty($mailTemplate))
        {
            $mailText    = $mailTemplate['text'];
            $mailSubject = $mailTemplate['subject'];
            if($template_id == 1 || $template_id == 2 || $template_id == 14 || $template_id == 161)
            {
                $items      = $this->mapper->getItems(['subscription_id'=>$subscription_id]);
                $action     = [];
                $actionText = '';
                if(!empty($subscription->action_id))
                {
                    $action = $this->mapper->actionService->mapper->getActionBy(['id' => $subscription->action_id]);
                    if(!empty($action))
                    {
                        $actionText = "Скидка предоставляется участникам акции : " . $action->name;
                    }
                }

                $items_data = [];
                foreach($items as $item)
                {
                    $items_data[] = [
                        'name'     => $item['name'] . ' ' . $actionText,
                        'quantity' => $item['quantity'],
                        'price'    => sprintf('%01.2f', $item['rqty_price']),
                        'value'    => !empty($item['discount']) ? "<span style='text-decoration:line-through;'>".sprintf('%01.2f', $item['quantity_price'])."</span><br/><b style='font-size:16px'>".sprintf('%01.2f', $item['full_price'])."</b>" : sprintf('%01.2f', $item['full_price'])
                    ];
                }
                /*$renderer = new \Zend\View\Renderer\PhpRenderer();
                $resolver = new \Zend\View\Resolver\TemplateMapResolver(array(
                    'nx/subscription/remind.phtml' => 'module/nx/view/nx/subscription/remind.phtml'
                ));
                $renderer->setResolver($resolver);*/
                $model = new \Zend\View\Model\ViewModel(['items' => $items_data, 'subscription' => $subscription]);
                $model->setTemplate('nx/subscription/remind.phtml');
                $itemsTable = $this->renderer->render($model);
            }
            if($template_id == 1 || $template_id == 2)
            {
                $replacement = [
                    '%%name%%'          => $subscription->person_name,
                    '%%order_hash%%'    => $subscription->hash,
                    '%%order_id%%'      => $subscription->id,
                    '%%order_content%%' => $itemsTable,
                    '%%LINK_PAY%%'      => 'https://passport.osp.ru/sales/repeat/' . $subscription->hash,
                ];
            }
            else if($template_id == 14)
            {
                $replacement = [
                    '%%NAME%%'          => $subscription->person_name,
                    '%%MAGAZINE_NAME%%' => $items[0]['periodical_name'],
                    '%%LINK_BILL%%'     => 'https://www.osp.ru/subscribe/paydoc/?hash=' . $subscription->hash,
                    '%%LINK_PAY%%'      => 'https://passport.osp.ru/sales/repeat/' . $subscription->hash,
                    '%%LINK_CABINET%%'  => 'https://passport.osp.ru/profile/subscription/',
                    '%%ORDER_CONTENT%%' => $itemsTable
                ];
            }
            else if($template_id == 161)
            {
                $replacement = [
                    '%%NAME%%'          => $subscription->person_name,
                    '%%MAGAZINE_NAME%%' => $items[0]['periodical_name'],
                    '%%LINK_BILL%%'     => 'https://www.osp.ru/subscribe/paydoc/?hash=' . $subscription->hash,
                    '%%LINK_PAY%%'      => 'https://passport.osp.ru/sales/repeat/' . $subscription->hash,
                    '%%LINK_CABINET%%'  => 'https://passport.osp.ru/profile/subscription/',
                    '%%ORDER_CONTENT%%' => $itemsTable
                ];
            }
            else if($template_id == 9)
            {
                $items = $this->mapper->getItems(['subscription_id'=>$subscription_id]);
                $days  = 99999;
                foreach($items as $item)
                {
                    $cur_days = floor((strtotime($item['date_end']) - time()) / (60 * 60 * 24));
                    if($cur_days < $days)
                    {
                        $days = $cur_days;
                        $replacement = [
                            '%%LINK%%'          => 'https://passport.osp.ru/profile/renewal/' . $subscription->order_item_id,
                            '%%EXP_TIME%%'      => $days,
                            '%%MAGAZINE_NAME%%' => $item['periodical_name']
                        ];
                    }
                }
                $replacement = array_merge($replacement, [
                    '%%NAME%%' => $subscription->person_name
                ]);
            }
            else //if($template_id == 6 || $template_id == 8)
            {
                $items      = $this->mapper->getItems(['subscription_id'=>$subscription_id]);
                $subscribeUrl = '';
                if(!empty($items)){
                    $subscriptionUrlsMap = [
                            'cw'	=> 'https://www.osp.ru/subscribe/cw/',
                            'cio'  	=> 'https://www.osp.ru/subscribe/cio/',
                            'lan'  	=> 'https://www.osp.ru/subscribe/lan/',
                            'win2000'  	=> 'https://www.osp.ru/subscribe/win2000/',
                            'os'  	=> 'https://www.osp.ru/subscribe/os/',
                            'pcworld'  	=> 'https://www.osp.ru/subscribe/pcworld/',
                            'publish' 	=> 'https://www.publish.ru/subscription',
                            'whf' 	=> 'https://www.osp.ru/subscribe/whf/',
                            'lvrach' 	=> 'http://www.lvrach.ru/subscribe/',
                            'Classmag' 	=> 'http://www.classmag.ru/subscribe',
                            'ponymashka' => 'http://www.ponymashka.ru/subscribe',
                    ];
                    $magazineName = $items[0]['EnglishName'];
                    $subscribeUrl = isset($subscriptionUrlsMap[$magazineName]) ? $subscriptionUrlsMap[$magazineName] : '';
                }

                $replacement = [
                    '%%USER_NAME%%'   => $subscription->person_name,
                    '%%ORDER_DATE%%'  => $subscription->payment_date,
                    '%%ORDER_ID%%'    => $subscription->id,
                    '%%ORDER_PRICE%%' => $subscription->payment_sum,
                    '%%SUBSCRIBE_URL%%' => $subscribeUrl,
                ];
            }

            $mailText    = str_replace(array_keys($replacement), array_values($replacement), $mailTemplate['text']);
            $mailSubject = str_replace(array_keys($replacement), array_values($replacement), $mailTemplate['subject']);

            $mailText = $this->mapper->mailTemplatesModel->parseADVToTemplate($template_id, $mailText);
        }

        return ['message' => $mailText, 'subject' => $mailSubject];
    }

    public function getAndSendMailMessage($subscription_id, $template_id)
    {
        $subscription = $this->mapper->getByID($subscription_id);
        if($subscription->email)
        {
            $mailData = $this->getMailMessage($subscription_id, $template_id);
            $mailData = [
                'to_email' => $subscription->email,
                'from'     => 'xpress@osp.ru',
                'subject'  => $mailData['subject'],
                'text'     => $mailData['message']
            ];
            return $this->mapper->mailTemplatesModel->create($mailData);
        }
        return false;
    }

    public function tryToRegisterUser(array $params)
    {
        $subscription = $this->mapper->getByID($params['id']);

        if(!empty($subscription->user_id) || empty($subscription->person_id) || empty($subscription->email)) {
            return false;
        }

        $person = $this->mapper->personModel->getByID($subscription->person_id);

        $password = \nx\Service\Passport::generate_password(8);

        $fio = explode(' ', trim($person->name) . '   ');

        $regdata = [
            'LastName'   => $person->f ? $person->f : $fio[0],
            'FirstName'  => $person->i ? $person->i : $fio[1],
            'MidName'    => $person->o ? $person->o : $fio[2],
            'Password'   => $password,
            'Login'      => $subscription->email,
            'Email'      => $subscription->email,//'pechorin@osp.ru',//$subscription->email
            'send_email' => true,
        ];
        if(!empty($params['from'])) {
            $regdata['come_from'] = $params['from'];
        }

        return $this->passportService->registerUser($regdata);
    }
}
