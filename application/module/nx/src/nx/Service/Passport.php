<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


//passport.osp.ru
namespace nx\Service;
class Passport extends AbstractService
{
	private $client, $url, $token;

	public function __construct($cfg)
	{
		$this->url = $cfg['api_url'];
        $this->token = $cfg['token'] ?? '';
	}

	public function registerUser($data)
	{
		$client = new \Zend\Http\Client($this->url . 'create');
        $client->setOptions(['sslcapath' => '/etc/ssl/certs']);

        $client->setMethod('POST');
		//$params = array('subscriptionData' => $subscriptionData, 'personData' => $personData);var_dump($params);
        $client->getRequest()->getPost()->fromArray($data);
        $timestamp = time();
        $token     = md5($this->token . $timestamp);
        $client->setParameterGet(['token' => $token, 'timestamp' => $timestamp]);
    	//var_dump($data);//die;
        $result = $client->send();
        //var_dump($result->getBody());
        return $result->getBody();
	}

	public function request()
	{

	}

	public static function generate_password($number)
	{
	    $arr = ['a','b','c','d','e','f',

	                 'g','h','i','j','k','l',

	                 'm','n','o','p','r','s',

	                 't','u','v','x','y','z',

	                 'A','B','C','D','E','F',

	                 'G','H','I','J','K','L',

	                 'M','N','O','P','R','S',

	                 'T','U','V','X','Y','Z',

	                 '1','2','3','4','5','6',

	                 '7','8','9','0'];

	    $pass = "";
	    for($i = 0; $i < $number; $i++)
	    {
	    	// Вычисляем случайный индекс массива
	    	$index = rand(0, count($arr) - 1);
	    	$pass .= $arr[$index];
	    }

	    return $pass;
	}
}
