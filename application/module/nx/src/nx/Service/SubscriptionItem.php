<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


namespace nx\Service;

use nx\Model\FileStorage;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Expression;
use const nx\NDS_SERVICE;

//use Zend\Db\RowGateway\RowGateway;

class SubscriptionItem extends AbstractService
{
    public $subscriptionMapper, $orderService;

    public function get1CDocsData_SubscriptionItem(array $params)
    {
        $results = [];
        $ids = $params['ids'];
        if (empty($ids)) {
            return false;
        }
        $sales_data = [];
        foreach ($ids as $item_id) {
            $item = $this->mapper->getItemByParams(['filters' => [['property' => 'id', 'value' => $item_id, 'xoperator' => 'eq']]]);
            if (empty($item['id'])) {
                $results[$item_id] = ['error' => 'Позиция с id = ' . $item_id . ' не найдена'];
                continue;
            }
            if (empty($item['type']) || $item['type'] != 'pdf') {
                $results[$item_id] = ['error' => 'Позиция с id = ' . $item_id . ' не является pdf услугой'];
                continue;
            }
            $subscription_id = $item['order_id'];
            $subscription = $this->subscriptionMapper->getBy(['id' => $subscription_id]);
            if (empty($subscription->id)) {
                $results[$item_id] = ['error' => 'Заказ с id = ' . $subscription_id . ' не найден'];
                continue;
            }
            if ($subscription->subscriber_type_id != 2) {
                $results[$item_id] = ['error' => 'Заказ с id = ' . $subscription_id . ' не на юр. лицо'];
                continue;
            }
            if ($subscription->type != 0) {
                $results[$item_id] = ['error' => 'Заказ с id = ' . $subscription_id . ' не оплачивается'];
                continue;
            }
            $releases = $this->subscriptionMapper->itemReleaseModel->getShippingsBy(['item_id' => $item_id]);
            if (!empty($releases)) {
                $release_ids = [];
                foreach ($releases as $release) {
                    $release_ids[] = $release['id'];
                }
                $files = $this->subscriptionMapper->fileStorageModel->getFiles(['entity_id' => $release_ids, 'entity_type' => 'order_item_release', 'type' => [1, 2, 3, 4]]);
                $check = $files->count();
                if (!empty($check)) {
                    $results[$item_id] = ['error' => 'Позиция с id = ' . $item_id . ' уже отгружается поштучно'];
                    continue;
                }
            }

            $doc_sale_rows = [];
            $quantity = $item['quantity'];
            $price = round($item['rqty'] * $item['price'] * (1 - $item['discount'] / 100.0), 2);
            $name = 'Доступ к электронной версии «' . $item['periodical_title'] . '» с ' . date('d.m.Y', strtotime($item['date_start'])) . ' на ' . $item['period'] . 'мес.';
            $nds = NDS_SERVICE;
            $doc_sale_row = [
                'Nomenclature' => $name,
                'Quantity' => $quantity,
                'Price' => $price,
                'NDS' => $nds,
                'periodical_name' => $item['periodical_title'],
                'year' => '',
                'issue_num' => '',
                'SeasonTicketDataStart' => $item['date_start'],
                'SeasonTicketPeriod' => $item['period'],
            ];
            $doc_sale_rows[] = $doc_sale_row;

            if (empty($doc_sale_rows)) {
                $results[$item_id] = ['error' => 'Не определены позиции заказа для отправки в 1с'];
                continue;
            }
            if (count($doc_sale_rows) == 1) {
                $doc_sale_rows = $doc_sale_rows[0];
            }
            if (empty($subscription->payment_agent_ws1c_id)) {
                $results[$item_id] = ['error' => 'У заказа неизвестный 1c id платежного агента'];
                continue;
            }
            $payment_agent_ws1c_id = $subscription->payment_agent_id == 2 ? '000000002' : $subscription->payment_agent_ws1c_id; //ZAO to OOO
            $organization = ['ID' => $payment_agent_ws1c_id];

            if (empty($subscription->company_inn) || empty($subscription->company_name)) {
                $results[$item_id] = ['error' => 'Не заполнены данные юр. лица в NX'];
                continue;
            }
            $contractor = [
                'INN' => $subscription->company_inn,
                'KPP' => !empty($subscription->company_kpp) ? $subscription->company_kpp : '',
                'Name' => $subscription->company_name
            ];

            if (empty($subscription->payment_date) || $subscription->payment_date == '0000-00-00') {
                $results[$item_id] = ['error' => 'Не проставлена дата оплаты у заказа в NX'];
                continue;
            }
            $payment_date = date('Y-m-d', strtotime($subscription->payment_date));
            $sale_id_prefix = !empty($params['prefix']) ? $params['prefix'] : 'order_item';
            $sale = [
                'document_date' => date('Y-m-d\TH:i:s'),//$payment_date,
                'SaleId' => $sale_id_prefix . $item_id,
                'Organization' => $organization,
                'Contractor' => $contractor,
                'DocSaleRow' => $doc_sale_rows,
                'Update' => true,
                'payment_date' => $payment_date,
                'OnlyBill' => false
            ];
            $sales_data[] = $sale;
        }
        return ['results' => $results, 'sales_data' => $sales_data];
    }

    public function request1CDocsForSubscriptionItems($params)
    {
        $results = [];
        $sales_data = [];
        $user_id = !empty($params['user_id']) ? (int)$params['user_id'] : 0;

        $data = $this->get1CDocsData_SubscriptionItem(['ids' => $params['ids'], 'prefix' => 'order_item']);
        if ($data) {
            $results = $data['results'];
            $sales_data = $data['sales_data'];
        }
        if (!empty($sales_data)) {
            return $this->orderService->request1CDocs([
                //'bill'    => true,
                'selling' => true,
                'invoice' => true,
                'signed' => isset($params['signed']) ? (bool)$params['signed'] : false,
                'type' => 'order_item',
                'prefix' => 'order_item',
                'data' => $sales_data,
                'user_id' => $user_id,
                'publisher_id' => $params['publisher_id'],
            ]);
        }
        return $results;
    }

    public function get1CFilesForSubscriptionItems($ids, $signed)
    {
        $results = [];
        foreach ($ids as $id) {
            $results[$id] = ['links' => '', 'file_ids' => [], 'request_doc' => 0];
        }

        $label = $signed ? 'signed' : '';
        $files = $this->orderService->mapper->fileStorageModel->getFiles(['entity_type' => 'order_item', 'entity_id' => $ids, 'type' => FileStorage::TYPE_1C_INVOICE, 'label' => $label]);
        $invoice_by_rid = [];
        if (!empty($files)) {
            foreach ($files as $file) {
                $invoices_by_rid[$file->entity_id] = $file;
            }
        }
        $act_by_rid = [];
        $files = $this->orderService->mapper->fileStorageModel->getFiles(['entity_type' => 'order_item', 'entity_id' => $ids, 'type' => FileStorage::TYPE_1C_ACT, 'label' => $label]);
        if (!empty($files)) {
            foreach ($files as $file) {
                $act_by_rid[$file->entity_id] = $file;
            }
        }

        foreach ($ids as $id) {
            $request_doc = 0;
            if (!empty($invoices_by_rid[$id]->hash)) {
                $results[$id]['file_ids'][] = $invoices_by_rid[$id]->id;
                $results[$id]['links'] .= '<a href="/file-storage/download/hash/' . $invoices_by_rid[$id]->hash . '" target="_blank">счет-фактура</a>, ';
            } else {
                $request_doc = 1;
            }

            if (!empty($act_by_rid[$id]->hash)) {
                $results[$id]['file_ids'][] = $act_by_rid[$id]->id;
                $results[$id]['links'] .= '<a href="/file-storage/download/hash/' . $act_by_rid[$id]->hash . '" target="_blank">акт</a>, ';
            } else {
                $request_doc = 1;
            }

            $results[$id]['request_doc'] = $request_doc;
            $results[$id]['links'] = mb_substr($results[$id]['links'], 0, -2);
        }

        return $results;
    }

    public function get1CDocsForSubscriptionItems($params)
    {
        $results = [];
        $user_id = !empty($params['user_id']) ? (int)$params['user_id'] : 0;
        $ids = $params['ids'];

        $signed = isset($params['signed']) ? (bool)$params['signed'] : false;
        $files_res = $this->get1CFilesForSubscriptionItems($ids, $signed);
        if (!empty($files_res)) {
            foreach ($files_res as $item_id => $result) {
                $results[$item_id] = $result;
                if (empty($result['request_doc'])) {
                    unset($ids[array_search($item_id, $ids)]);
                }
            }
        }
        if (!empty($ids)) {
            $data_res = $this->get1CDocsData_SubscriptionItem(['ids' => $ids, 'prefix' => 'order_item']);
            $data_res = $data_res['results'];
            if (!empty($data_res)) {
                foreach ($data_res as $item_id => $result) {
                    if (!empty($result['error'])) {
                        $results[$item_id]['error'] = $result['error'];
                        unset($ids[array_search($item_id, $ids)]);
                    }
                }
            }
        }
        if (!empty($ids)) {
            foreach ($ids as $item_id) {
                $results[$item_id]['links'] = '';
            }
        }

        return $results;
    }
}
