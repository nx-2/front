<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */

namespace nx\ws1c;

class organization
{
    /**
     * @var string
     * */
    public $ws1c_id;
    /**
     * @var string
     * */
    //public $name;
}

class contractor
{
    /**
     * @var string
     * */
    public $inn;
    /**
     * @var string
     * */
    public $kpp;
    /**
     * @var string
     * */
    public $name;
}

class paymentToNxIn
{
    /**
     * @var string
     * */
    public $ws1c_id;
    /**
     * @var int
     * */
    public $year;
    /**
     * @var string
     * */
    public $payment_date;
    /**
     * @var string
     * */
    public $payment_docno;
    /**
     * @var \nx\ws1c\organization
     * */
    public $organization;
    /**
     * @var \nx\ws1c\contractor
     * */
    public $contractor;
    /**
     * @var string
     * */
    public $contractor_by_bank;
    /**
     * @var float
     * */
    public $payment_sum;
    /**
     * @var string
     * */
    public $payment_purpose;
    /**
     * @var string
     * */
    public $payment_comment;
    /**
     * @var bool
     * */
    public $deleted;
    /**
     * @var int
     * */
    public $publisher_id;
}

class paymentToNxOut
{
    /**
     * @var string
     * */
    public $ws1c_id;
    /**
     * @var int
     * */
    public $year;
    /**
     * @var string
     * */
    public $result;
}

class Controller
{
	private $sm;

    function __construct($sm = null)
    {
        if($sm) {
            $this->sm = $sm;
        }
    }

    /**
     * save payments from 1C to NX
     * @param \nx\ws1c\paymentToNxIn[] $payments
     * @return \nx\ws1c\paymentToNxOut[]
     */
    public function paymentToNx($ArrayOfPaymentToNxIn)
    {
        //ob_start();
        //var_dump($ArrayOfPaymentToNxIn);
        //$params = ob_get_clean(); //file_put_contents('/home/pechorin/www/nx/test.txt', array($output, serialize($params)));
        try
        {
            $payments = $ArrayOfPaymentToNxIn->item;
            if(!is_array($payments))
            {
                $payments = [(array)$payments];
            }
            $payments = \nx\functions\objectToArray($payments);
            $result = $this->sm->get('nx\Model\Payment')->savePaymentFromWs1c(['payments' => $payments]);
            return $result;
        }
        catch(\Exception $e)
        {
            $result = [['ws1c_id' => '', 'year' => 0, 'result' => $e->getMessage()]];
            return $result;// . $e->getTraceAsString();
        }
    }
}
