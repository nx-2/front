<?php

return [
    'router' => [
        'routes' => [
            '*' => [
                'type' => 'Segment',
                //'type' => 'regex',
                'options' => [
                    'route' => '/:controller[/:action]',//[/]
                    //'regex' => '/<controller>/(?<action>)?',
                        /* OR add something like this to include the module path */
                        // 'route' => '/support/[:controller[/:action]]',
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        '__NAMESPACE__' => 'nx\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ],
                    //'spec' => '/%controller%/%action%',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'wildcard' => [
                        'type' => 'Wildcard'
                    ]
                ]
            ],
            'ws1c' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route'    => '/ws1c',
                    'defaults' => [
                        'controller' => 'nx\Controller\Index',
                        'action'     => 'ws1c',
                    ],
                ],
            ],
            'home' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'nx\Controller\Index',
                        'action'     => 'index',
                    ],
                ],
                /*
                'may_terminate' => true,
                'child_routes' => array(
                    'control' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '[/:controller[/:action]][.html]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z]?[a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z]?[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                //'__NAMESPACE__' => 'nx\Controller',
                                'controller' => 'Index',
                                'action'     => 'index',
                                'module'     => 'nx',
                            ),
                        ),
                    ),
                ),
                */
            ],
            /*'test' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/test',
                    'defaults' => array(
                        'controller' => 'nx\Controller\Index',
                        'action'     => 'test',
                    ),
                ),
            ),*/
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            /*'nx' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/nx',
                    'defaults' => array(
                        '__NAMESPACE__' => 'nx\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),*/
        ],
    ],
    'service_manager' => [
        'factories' => [
            'LazyServiceFactory' => 'Zend\ServiceManager\Proxy\LazyServiceFactoryFactory',
            'translator'         => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ],
        'invokables' => [
            'nx\ws1c_client' => '\nx\Service\Ws1c'
        ],
        'delegators' => [
            'nx\ws1c_client' => [
                'LazyServiceFactory'
            ],
        ],
    ],
    'lazy_services' => [
        'class_map' => [
            'nx\ws1c_client' => '\nx\Service\Ws1c'
        ],
    ],
    'translator' => [
        'locale' => 'en_US',
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ],
        ],
    ],
    'controllers' => [
        'invokables' => [
            'nx\Controller\Index'        => 'nx\Controller\IndexController',
            'nx\Controller\Test'         => 'nx\Controller\TestController',
            'nx\Controller\Subscription' => 'nx\Controller\SubscriptionController',
            'nx\Controller\Login'        => 'nx\Controller\LoginController',
            'nx\Controller\Periodical'   => 'nx\Controller\PeriodicalController',
            'nx\Controller\Address'      => 'nx\Controller\AddressController',
            'nx\Controller\Company'      => 'nx\Controller\CompanyController',
            'nx\Controller\User'         => 'nx\Controller\UserController',
            'nx\Controller\Person'       => 'nx\Controller\PersonController',
            'nx\Controller\Log'          => 'nx\Controller\LogController',
            'nx\Controller\Role'         => 'nx\Controller\RoleController',
            'nx\Controller\Transaction'  => 'nx\Controller\TransactionController',
            'nx\Controller\Order'        => 'nx\Controller\OrderController',
            'nx\Controller\MailLog'      => 'nx\Controller\MailLogController',
            'nx\Controller\Payment'      => 'nx\Controller\PaymentController',
            'nx\Controller\FileStorage'  => 'nx\Controller\FileStorageController',
            'nx\Controller\ViewState'    => 'nx\Controller\ViewStateController',
            'nx\Controller\Ship'         => 'nx\Controller\ShipController',
            'nx\Controller\ShipItem'     => 'nx\Controller\ShipItemController',
            'nx\Controller\Action'       => 'nx\Controller\ActionController',
            'nx\Controller\Chart'        => 'nx\Controller\ChartController',
//            'nx\Controller\Notification' => 'nx\Controller\NotificationController',
            'nx\Controller\Tag'          => 'nx\Controller\TagController',
            'nx\Controller\OrderItem'    => 'nx\Controller\OrderItemController',
            'nx\Controller\Comment'      => 'nx\Controller\CommentController',
            'nx\Controller\Price'        => 'nx\Controller\PriceController',
            'nx\Controller\Zone'         => 'nx\Controller\ZoneController',
        ],
        'factories' => [
            'nx\Controller\Notification' => \nx\Controller\factory\NotificationControllerFactory::class,
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/json',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'nx/index/index'          => __DIR__ . '/../view/nx/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
            'ViewFeedStrategy'
        ],
    ], /*
    'view_helpers' => array(
        'factories' => array(
            'showmessages' => function($sm) {
                $helper = new ModuleName\Helper\MessageShower();
                // do stuff with $sm or the $helper
                return $helper;
            },
        ),
        'invokables' => array(
            //'selectmenu' => 'ModuleName\Helper\SelectMenu',
            //'prettyurl'  => 'ModuleName\Helper\PrettyUrl',
        ),
    ),*/
    'controller_plugins' => [
        'invokables' => [
            'AuthPlugin' => 'nx\Controller\Plugin\AuthPlugin'
        ],
    ],
    // Doctrine config
    'doctrine' => [
        'driver' => [
            'nx_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/nx/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'nx\Entity' => 'nx_driver'
                ]
            ]
        ]
    ],

    /*'di' => array(
        'instance' => array(
            'Zend\View\Resolver\AggregateResolver' => array(
                'injections' => array(
                    'Zend\View\Resolver\TemplateMapResolver',
                    'Zend\View\Resolver\TemplatePathStack',
                ),
            ),
            'Zend\View\Resolver\TemplateMapResolver' => array(
                'parameters' => array(
                    'map'  => array(
                        'subscription/remind' => __DIR__ . '/../view/nx/subscription/remind.phtml',
                        //'index/index' => __DIR__ . '/view/index/index.phtml',
                    ),
                ),
            ),
            'Zend\View\Resolver\TemplatePathStack' => array(
                'parameters' => array(
                    'paths'  => array(
                        __DIR__ . '/../view/',
                        //'elsewhere'   => $someOtherPath,
                    ),
                ),
            ),
            'Zend\View\Renderer\PhpRenderer' => array(
                'parameters' => array(
                    'resolver' => 'Zend\View\Resolver\AggregateResolver',
                ),
            ),
        ),
    ),*/
];
