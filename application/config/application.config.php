<?php
return [
    'modules' => [
        //'ZendDeveloperTools',
        //'DoctrineModule',
        //'DoctrineORMModule',
        'nx',
    ],
    'module_listener_options' => [
        'config_glob_paths'    => [
            'config/autoload/{,*.}{global,local}.php',
            //'config/autoload/{,*.}' . (getenv('APPLICATION_ENV') ?: 'production') . '.php',
        ],
        'module_paths' => [
            //'/var/www/nx.osp.ru/htdocs/application/module'
            './module',
            //'./vendor',
        ],
    ],
];
