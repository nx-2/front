<?php
/**
 * @link      https://gitlab.com/nx-2/front
 * @copyright Copyright © 2017, 2018, 2022 Dimeo Ltd. under the terms of the GNU GPL, Version 3.0 (https://www.dimeo.ru/)
 * @license   https://gitlab.com/nx-2/front/-/blob/master/LICENSE.md
 * @author    A. Shchepetov
 */


ini_set('soap.wsdl_cache', 0);
ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);


$initSqlCmd = "SET NAMES 'UTF8'";

$cfg = [
    'nx' => [
        'http_host'        => getenv('THE_HOST'),
        'schema' => getenv('THE_SCHEME'),
        'passport_service' => [
            'api_url' => getenv('PASSPORT_HOST') . getenv('PASSPORT_API_URL'),
            'token' => getenv('NX_PASSPORT_API_TOKEN')
        ],
        'mail_service'     => [
            'apikey'  => getenv('NX_PASSPORT_API_TOKEN'),
            'send_db' => getenv('MAPI_HOST') . getenv('MAPI_SEND_DB_URL'),
            'send'    => getenv('MAPI_HOST') . getenv('MAPI_SEND_URL')
        ],
        'adminUrl' => getenv('ADMIN_URL') !== false ? getenv('ADMIN_URL') : 'https://nx2-admin.dimeo.ru/',
        'apiUrl' => getenv('API_URL') !== false ? getenv('API_URL') : 'https://nx2-api.dimeo.ru/'
    ],
    'session' => [
        //'remember_me_seconds'     => 2419200,
        'use_cookies'               => true,
        'use_trans_sid'             => false,
        'cookie_domain'             => '.dimeo.ru',
        'name'                      => 'nxospru',
        //'cookie_path'             => '/',
        //'hash_bits_per_character' => 5,
        //'gc_maxlifetime'          => 1800,
        //'gc_probability'          => 1
        //'cookie_httponly'         => true,
    ],
    'service_manager' => [
        'factories' => [
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'nx\Db\Adapter' => function($sm) {
                $config = $sm->get('Config');
                return new \nx\DbAdapter($config['db']);
            },
            'nx\xpress\Adapter' => function($sm) {
                $initSqlCmd = "SET NAMES 'UTF8'";
                return new \Zend\Db\Adapter\Adapter([
                    'driver'         => 'pdo',
                    'dsn'            => 'mysql:dbname=xpress;host=xpress.osp.ru;port=3306',
                    'username'       => 'pechorin',
                    'password'       => '9brfxXT292tr',
                    'driver_options' => [PDO::MYSQL_ATTR_INIT_COMMAND => $initSqlCmd],
                ]);
            },
            'nx\aecms\Adapter' => function($sm) {
                $initSqlCmd = "SET NAMES 'UTF8'";
                return new \Zend\Db\Adapter\Adapter([
                    'driver'         => 'pdo',
                    'dsn'            => 'mysql:dbname=' . getenv('MYSQL_OSP_DBNAME') . ';host=' . getenv('MYSQL_OSP_HOST') . ';port=' . getenv('MYSQL_OSP_PORT'),
                    'username'       => getenv('MYSQL_OSP_USER'),
                    'password'       => getenv('MYSQL_OSP_PWD'),
                    'driver_options' => [PDO::MYSQL_ATTR_INIT_COMMAND => $initSqlCmd],
                ]);
            },
            'nx\osp\Adapter' => function($sm) {
                return new \Zend\Db\Adapter\Adapter([
                    'driver'         => 'pdo',
                    'dsn'            => 'mysql:dbname=' . getenv('MYSQL_OSP_DBNAME') . ';host=' . getenv('MYSQL_OSP_HOST') . ';port=' . getenv('MYSQL_OSP_PORT'),
                    'username'       => getenv('MYSQL_OSP_USER'),
                    'password'       => getenv('MYSQL_OSP_PWD'),
                    'driver_options' => [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"],
                ]);
            },
        ],
    ],
    'db' => [
        'driver'         => 'pdo',
        'dsn'            => 'mysql:dbname=' . getenv('MYSQL_OSP_DBNAME') . ';host=' . getenv('MYSQL_OSP_HOST') . ';port=' . getenv('MYSQL_OSP_PORT'),
        'username'       => getenv('MYSQL_OSP_USER'),
        'password'       => getenv('MYSQL_OSP_PWD'),
        'driver_options' => [PDO::MYSQL_ATTR_INIT_COMMAND => $initSqlCmd],
    ],
];
return $cfg;
